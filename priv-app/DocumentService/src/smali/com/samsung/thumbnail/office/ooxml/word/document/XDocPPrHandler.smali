.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XDocPPrHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocKeepLinesHandler$IXKeepLines;
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocKeepNextHandler$IXKeepNext;
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPageBreakBeforeHandler$IXPgBrBeforePara;
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$IXRPrObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$IXPPrObserver;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrShade;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrNumbering;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrIndentation;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrSpacing;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrJustify;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrStyleId;
    }
.end annotation


# instance fields
.field protected charProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

.field private pPrObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$IXPPrObserver;

.field protected paraProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$IXPPrObserver;)V
    .locals 37
    .param p1, "pPrObserver"    # Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$IXPPrObserver;

    .prologue
    .line 43
    const-string/jumbo v36, "pPr"

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 45
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;->pPrObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$IXPPrObserver;

    .line 47
    const/16 v36, 0xd

    move/from16 v0, v36

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    move-object/from16 v24, v0

    .line 49
    .local v24, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v31, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrStyleId;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrStyleId;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;)V

    .line 50
    .local v31, "styleId":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrStyleId;
    new-instance v32, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;

    const-string/jumbo v36, "pStyle"

    move-object/from16 v0, v32

    move-object/from16 v1, v36

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 52
    .local v32, "styleIdHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;
    new-instance v33, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v36, "pStyle"

    move-object/from16 v0, v33

    move-object/from16 v1, v36

    move-object/from16 v2, v32

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 54
    .local v33, "styleIdSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v36, 0x0

    aput-object v33, v24, v36

    .line 57
    new-instance v16, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrNumbering;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrNumbering;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;)V

    .line 58
    .local v16, "numbering":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrNumbering;
    new-instance v14, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler;

    move-object/from16 v0, v16

    invoke-direct {v14, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/properties/IXDocPropery;)V

    .line 59
    .local v14, "numPrHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler;
    new-instance v15, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v36, "numPr"

    move-object/from16 v0, v36

    invoke-direct {v15, v0, v14}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 61
    .local v15, "numSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v36, 0x1

    aput-object v15, v24, v36

    .line 64
    new-instance v17, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocParaBorderHandler;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocParaBorderHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;)V

    .line 66
    .local v17, "paraBorderHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocParaBorderHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v36, "pBdr"

    move-object/from16 v0, v36

    move-object/from16 v1, v17

    invoke-direct {v3, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 68
    .local v3, "borderSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v36, 0x2

    aput-object v3, v24, v36

    .line 71
    new-instance v25, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrShade;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrShade;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;)V

    .line 72
    .local v25, "shade":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrShade;
    new-instance v26, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocShadeHandler;

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocShadeHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 73
    .local v26, "shadeHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocShadeHandler;
    new-instance v27, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v36, "shd"

    move-object/from16 v0, v27

    move-object/from16 v1, v36

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 75
    .local v27, "shadeSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v36, 0x3

    aput-object v27, v24, v36

    .line 78
    new-instance v29, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrSpacing;

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrSpacing;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;)V

    .line 79
    .local v29, "spacing":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrSpacing;
    new-instance v30, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSpacingHandler;

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSpacingHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/properties/IXDocPropery;)V

    .line 80
    .local v30, "spacingHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSpacingHandler;
    new-instance v28, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v36, "spacing"

    move-object/from16 v0, v28

    move-object/from16 v1, v36

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 82
    .local v28, "spaceSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v36, 0x4

    aput-object v28, v24, v36

    .line 85
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrIndentation;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrIndentation;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;)V

    .line 86
    .local v5, "indent":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrIndentation;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocIndentationHandler;

    invoke-direct {v6, v5}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocIndentationHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/properties/IXDocPropery;)V

    .line 88
    .local v6, "indentationHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocIndentationHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v36, "ind"

    move-object/from16 v0, v36

    invoke-direct {v4, v0, v6}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 90
    .local v4, "indSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v36, 0x5

    aput-object v4, v24, v36

    .line 93
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrJustify;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrJustify;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;)V

    .line 94
    .local v8, "justify":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrJustify;
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;

    const-string/jumbo v36, "jc"

    move-object/from16 v0, v36

    invoke-direct {v9, v0, v8}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 96
    .local v9, "justifyHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v36, "jc"

    move-object/from16 v0, v36

    invoke-direct {v7, v0, v9}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 98
    .local v7, "jcSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v36, 0x6

    aput-object v7, v24, v36

    .line 101
    new-instance v20, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$IXRPrObserver;)V

    .line 102
    .local v20, "rPrHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;
    new-instance v21, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v36, "rPr"

    move-object/from16 v0, v21

    move-object/from16 v1, v36

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 104
    .local v21, "rPrSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v36, 0x7

    aput-object v21, v24, v36

    .line 107
    new-instance v22, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;

    const/16 v36, 0x15

    move-object/from16 v0, v22

    move/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;-><init>(I)V

    .line 109
    .local v22, "sectPrHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;
    new-instance v23, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v36, "sectPr"

    move-object/from16 v0, v23

    move-object/from16 v1, v36

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 111
    .local v23, "sectPrSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v36, 0x8

    aput-object v23, v24, v36

    .line 114
    new-instance v34, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabsHandler;

    invoke-direct/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabsHandler;-><init>()V

    .line 115
    .local v34, "tabsHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabsHandler;
    new-instance v35, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v36, "tabs"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move-object/from16 v2, v34

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 117
    .local v35, "tabsSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v36, 0x9

    aput-object v35, v24, v36

    .line 119
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPageBreakBeforeHandler;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPageBreakBeforeHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPageBreakBeforeHandler$IXPgBrBeforePara;)V

    .line 121
    .local v18, "pgBrBefHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPageBreakBeforeHandler;
    new-instance v19, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v36, "pageBreakBefore"

    move-object/from16 v0, v19

    move-object/from16 v1, v36

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 123
    .local v19, "pgBrBefSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v36, 0xa

    aput-object v19, v24, v36

    .line 125
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocKeepLinesHandler;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocKeepLinesHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocKeepLinesHandler$IXKeepLines;)V

    .line 126
    .local v10, "keepLinesHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocKeepLinesHandler;
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v36, "keepLines"

    move-object/from16 v0, v36

    invoke-direct {v11, v0, v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 128
    .local v11, "keepLinesSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v36, 0xb

    aput-object v11, v24, v36

    .line 130
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocKeepNextHandler;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocKeepNextHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocKeepNextHandler$IXKeepNext;)V

    .line 131
    .local v12, "keepNextHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocKeepNextHandler;
    new-instance v13, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v36, "keepNext"

    move-object/from16 v0, v36

    invoke-direct {v13, v0, v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 133
    .local v13, "keepNxtSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v36, 0xc

    aput-object v13, v24, v36

    .line 135
    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 136
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 164
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 165
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;->pPrObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$IXPPrObserver;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;->paraProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;->charProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-interface {v0, v1, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$IXPPrObserver;->onPPrFinished(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V

    .line 166
    return-void
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 155
    invoke-virtual {p0, p2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 156
    .local v0, "elementName":Ljava/lang/String;
    const-string/jumbo v1, "contextualSpacing"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 157
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;->paraProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->setContextualSpacing()V

    .line 159
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 160
    return-void
.end method

.method public onRPrFinsihed(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p1, "charProperties"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .param p2, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 244
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;->charProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .line 245
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;->paraProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->setParaCharProperties(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V

    .line 246
    return-void
.end method

.method public setBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V
    .locals 1
    .param p1, "borderType"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    .param p2, "border"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .prologue
    .line 250
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;->paraProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->setBorders(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    .line 251
    return-void
.end method

.method public setKeepLinesStatus(Z)V
    .locals 1
    .param p1, "keepLinesStatus"    # Z

    .prologue
    .line 265
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;->paraProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->setKeepLinesStatus(Z)V

    .line 266
    return-void
.end method

.method public setKeepNxtStatus(Z)V
    .locals 1
    .param p1, "keepNxtStatus"    # Z

    .prologue
    .line 270
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;->paraProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->setKeepNxtStatus(Z)V

    .line 271
    return-void
.end method

.method public setPgBrBefStatus(Z)V
    .locals 1
    .param p1, "pgBrBefStatus"    # Z

    .prologue
    .line 260
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;->paraProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->setPgBrBefStatus(Z)V

    .line 261
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 141
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;->paraProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    .line 142
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;->charProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .line 143
    return-void
.end method
