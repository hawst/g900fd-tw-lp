.class public Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;
.super Ljava/lang/Object;
.source "EMR_Ellipse.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;


# instance fields
.field private Size:I

.field private Type:I

.field private emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

.field fileContent:[B

.field private leftTopX:I

.field private leftTopY:I

.field private offset:I

.field private pen:I

.field private rightBottomX:I

.field private rightBottomY:I


# direct methods
.method public constructor <init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V
    .locals 0
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I
    .param p3, "emrGlobals"    # Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .line 65
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->fileContent:[B

    .line 66
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->offset:I

    .line 67
    return-void
.end method


# virtual methods
.method public getLeftTopX()I
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->leftTopX:I

    return v0
.end method

.method public getLeftTopY()I
    .locals 1

    .prologue
    .line 151
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->leftTopY:I

    return v0
.end method

.method public getPen()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->pen:I

    return v0
.end method

.method public getRightBottomX()I
    .locals 1

    .prologue
    .line 170
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->rightBottomX:I

    return v0
.end method

.method public getRightBottomY()I
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->rightBottomY:I

    return v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->Size:I

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->Type:I

    return v0
.end method

.method public prepare()V
    .locals 0

    .prologue
    .line 339
    return-void
.end method

.method public readEmfEllipse()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x0

    .line 206
    const/4 v2, 0x0

    .line 207
    .local v2, "i":I
    const/4 v8, 0x4

    new-array v3, v8, [B

    .line 209
    .local v3, "intConvert":[B
    const/4 v2, 0x0

    :goto_0
    if-gt v2, v12, :cond_0

    .line 210
    rsub-int/lit8 v8, v2, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->offset:I

    add-int/2addr v10, v2

    aget-byte v9, v9, v10

    aput-byte v9, v3, v8

    .line 209
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 211
    :cond_0
    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 212
    .local v1, "Type":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->offset:I

    add-int/2addr v8, v2

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->offset:I

    .line 213
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->setType(I)V

    .line 215
    const/4 v2, 0x0

    :goto_1
    if-gt v2, v12, :cond_1

    .line 216
    rsub-int/lit8 v8, v2, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->offset:I

    add-int/2addr v10, v2

    aget-byte v9, v9, v10

    aput-byte v9, v3, v8

    .line 215
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 217
    :cond_1
    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 218
    .local v0, "Size":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->offset:I

    add-int/2addr v8, v2

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->offset:I

    .line 219
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->setSize(I)V

    .line 221
    const/4 v2, 0x0

    :goto_2
    if-gt v2, v12, :cond_2

    .line 222
    rsub-int/lit8 v8, v2, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->offset:I

    add-int/2addr v10, v2

    aget-byte v9, v9, v10

    aput-byte v9, v3, v8

    .line 221
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 223
    :cond_2
    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v4

    .line 224
    .local v4, "leftTopX":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->offset:I

    add-int/2addr v8, v2

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->offset:I

    .line 225
    invoke-virtual {p0, v4}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->setLeftTopX(I)V

    .line 227
    const/4 v2, 0x0

    :goto_3
    if-gt v2, v12, :cond_3

    .line 228
    rsub-int/lit8 v8, v2, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->offset:I

    add-int/2addr v10, v2

    aget-byte v9, v9, v10

    aput-byte v9, v3, v8

    .line 227
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 229
    :cond_3
    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v5

    .line 230
    .local v5, "leftTopY":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->offset:I

    add-int/2addr v8, v2

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->offset:I

    .line 231
    invoke-virtual {p0, v5}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->setLeftTopY(I)V

    .line 233
    const/4 v2, 0x0

    :goto_4
    if-gt v2, v12, :cond_4

    .line 234
    rsub-int/lit8 v8, v2, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->offset:I

    add-int/2addr v10, v2

    aget-byte v9, v9, v10

    aput-byte v9, v3, v8

    .line 233
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 235
    :cond_4
    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v6

    .line 236
    .local v6, "rightBottomX":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->offset:I

    add-int/2addr v8, v2

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->offset:I

    .line 237
    invoke-virtual {p0, v6}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->setRightBottomX(I)V

    .line 239
    const/4 v2, 0x0

    :goto_5
    if-gt v2, v12, :cond_5

    .line 240
    rsub-int/lit8 v8, v2, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->offset:I

    add-int/2addr v10, v2

    aget-byte v9, v9, v10

    aput-byte v9, v3, v8

    .line 239
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 241
    :cond_5
    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v7

    .line 242
    .local v7, "rightBottomY":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->offset:I

    add-int/2addr v8, v2

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->offset:I

    .line 243
    invoke-virtual {p0, v7}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->setRightBottomY(I)V

    .line 245
    return-void
.end method

.method public render(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 255
    const/4 v3, 0x0

    .line 256
    .local v3, "i":I
    new-instance v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;-><init>()V

    .line 257
    .local v1, "PaintEllipsePenPropertiesObject":Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;-><init>()V

    .line 258
    .local v0, "PaintEllipseBrushPropertiesObject":Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;
    sget-object v6, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v6}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 259
    const/4 v4, 0x0

    .line 260
    .local v4, "localRectF":Landroid/graphics/RectF;
    new-instance v4, Landroid/graphics/RectF;

    .end local v4    # "localRectF":Landroid/graphics/RectF;
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->getLeftTopX()I

    move-result v6

    int-to-float v6, v6

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float/2addr v6, v7

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->getLeftTopY()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float/2addr v7, v8

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->getRightBottomX()I

    move-result v8

    int-to-float v8, v8

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v9, v9, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float/2addr v8, v9

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->getRightBottomY()I

    move-result v9

    int-to-float v9, v9

    iget-object v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v10, v10, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float/2addr v9, v10

    invoke-direct {v4, v6, v7, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 264
    .restart local v4    # "localRectF":Landroid/graphics/RectF;
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v6, v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 266
    .local v2, "emrSize":I
    if-lez v2, :cond_1

    .line 267
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    .line 268
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v7, v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandleBrush:I

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v6, v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->gethandleNumber()I

    move-result v6

    if-ne v7, v6, :cond_8

    .line 273
    :cond_0
    if-ge v3, v2, :cond_1

    .line 274
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v6, v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->getColor()I

    move-result v6

    invoke-static {v6}, Lcom/samsung/thumbnail/office/emf/Util/toColorAndroid;->getColor(I)I

    move-result v5

    .line 277
    .local v5, "myColor":I
    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 278
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual {p1, v4, v6}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 283
    .end local v5    # "myColor":I
    :cond_1
    if-eq v3, v2, :cond_2

    if-nez v2, :cond_3

    .line 285
    :cond_2
    const/4 v6, 0x5

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v6, v7, :cond_9

    .line 301
    :cond_3
    :goto_1
    if-lez v2, :cond_5

    .line 302
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v2, :cond_4

    .line 303
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v7, v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandlePen:I

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v6, v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->gethandleNumber()I

    move-result v6

    if-ne v7, v6, :cond_b

    .line 308
    :cond_4
    if-ge v3, v2, :cond_5

    .line 309
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v6, v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getPenStyle()I

    move-result v6

    invoke-static {v6}, Lcom/samsung/thumbnail/office/emf/Util/toStyleAndroid;->getStyle(I)Landroid/graphics/Paint$Style;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 312
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v6, v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getColor()I

    move-result v6

    invoke-static {v6}, Lcom/samsung/thumbnail/office/emf/Util/toColorAndroid;->getColor(I)I

    move-result v6

    invoke-virtual {v1, v6}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 315
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual {p1, v4, v6}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 320
    :cond_5
    if-eq v3, v2, :cond_6

    if-nez v2, :cond_7

    .line 322
    :cond_6
    const/16 v6, 0x8

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v6, v7, :cond_c

    .line 329
    :cond_7
    :goto_3
    return-void

    .line 267
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 287
    :cond_9
    const/4 v6, 0x4

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v6, v7, :cond_a

    .line 289
    const/high16 v6, -0x1000000

    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 290
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual {p1, v4, v6}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 292
    :cond_a
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v6, v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-nez v6, :cond_3

    .line 294
    const/4 v6, -0x1

    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 295
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual {p1, v4, v6}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 302
    :cond_b
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    .line 325
    :cond_c
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual {p1, v4, v6}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_3
.end method

.method public setLeftTopX(I)V
    .locals 0
    .param p1, "leftTopX"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->leftTopX:I

    .line 143
    return-void
.end method

.method public setLeftTopY(I)V
    .locals 0
    .param p1, "leftTopY"    # I

    .prologue
    .line 161
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->leftTopY:I

    .line 162
    return-void
.end method

.method public setPen(I)V
    .locals 0
    .param p1, "pn"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->pen:I

    .line 86
    return-void
.end method

.method public setRightBottomX(I)V
    .locals 0
    .param p1, "rightBottomX"    # I

    .prologue
    .line 180
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->rightBottomX:I

    .line 181
    return-void
.end method

.method public setRightBottomY(I)V
    .locals 0
    .param p1, "rightBottomY"    # I

    .prologue
    .line 199
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->rightBottomY:I

    .line 200
    return-void
.end method

.method public setSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 123
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->Size:I

    .line 124
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 104
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->Type:I

    .line 105
    return-void
.end method
