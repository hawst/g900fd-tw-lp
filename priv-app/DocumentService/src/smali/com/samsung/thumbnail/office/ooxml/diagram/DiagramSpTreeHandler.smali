.class public Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpTreeHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DiagramSpTreeHandler.java"


# direct methods
.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V
    .locals 5
    .param p1, "ID"    # I
    .param p2, "xwpfDiagram"    # Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;
    .param p3, "theme"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    .param p4, "docType"    # Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    .prologue
    const/4 v4, 0x0

    .line 21
    const-string/jumbo v3, "spTree"

    invoke-direct {p0, p1, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 23
    const/4 v3, 0x1

    new-array v1, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 25
    .local v1, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    if-ne p4, v3, :cond_1

    .line 26
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;

    invoke-direct {v2, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 28
    .local v2, "sphandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;

    const-string/jumbo v3, "sp"

    invoke-direct {v0, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 30
    .local v0, "dsp1Seq":Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;
    aput-object v0, v1, v4

    .line 38
    .end local v0    # "dsp1Seq":Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;
    .end local v2    # "sphandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;
    :cond_0
    :goto_0
    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpTreeHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 39
    return-void

    .line 31
    :cond_1
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPTX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    if-ne p4, v3, :cond_0

    .line 32
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;

    invoke-direct {v2, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;)V

    .line 33
    .local v2, "sphandler":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;

    const-string/jumbo v3, "sp"

    invoke-direct {v0, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 35
    .restart local v0    # "dsp1Seq":Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;
    aput-object v0, v1, v4

    goto :goto_0
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 44
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 45
    return-void
.end method
