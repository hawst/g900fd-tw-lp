.class public Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblGridHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XDocTblGridHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblGridHandler$ITblGridObserver;
    }
.end annotation


# instance fields
.field private gridCellArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mGridObserver:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblGridHandler$ITblGridObserver;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblGridHandler$ITblGridObserver;)V
    .locals 1
    .param p1, "gridObserver"    # Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblGridHandler$ITblGridObserver;

    .prologue
    .line 18
    const-string/jumbo v0, "tblGrid"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 19
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblGridHandler;->mGridObserver:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblGridHandler$ITblGridObserver;

    .line 21
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblGridHandler;->mGridObserver:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblGridHandler$ITblGridObserver;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblGridHandler;->gridCellArray:Ljava/util/ArrayList;

    invoke-interface {v0, p1, v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblGridHandler$ITblGridObserver;->setTblGridProp(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/util/ArrayList;)V

    .line 43
    return-void
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 33
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;

    const-string/jumbo v1, "w"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblGridHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;-><init>(Ljava/lang/String;)V

    .line 35
    .local v0, "pos":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblGridHandler;->gridCellArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;->getValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 26
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblGridHandler;->gridCellArray:Ljava/util/ArrayList;

    .line 28
    return-void
.end method
