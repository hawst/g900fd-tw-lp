.class public Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;
.super Ljava/lang/Object;
.source "ReadRawFiles.java"


# static fields
.field static actHeight:F

.field static actWidth:F

.field static height:F

.field static width:F


# instance fields
.field public EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

.field public EMR_EofObject:Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;

.field public EMR_SetMapModeObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;

.field public EMR_SetStretchBltModeObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;

.field public EMR_SetViewPortExtExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortExtEx;

.field public EMR_SetWindowExtExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetWindowExtEx;

.field public EMR_StretchdibitsObject:Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;

.field public EmfHeaderObject:Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;

.field public EmrRecordTypeObject:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;

.field private drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

.field private emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

.field fileContent:[B

.field private mRecordType:I

.field private mapFlag:Z

.field public offset:I


# direct methods
.method public constructor <init>([B)V
    .locals 2
    .param p1, "stream"    # [B

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EmrRecordTypeObject:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;

    .line 48
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EmfHeaderObject:Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;

    .line 51
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetStretchBltModeObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;

    .line 54
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_StretchdibitsObject:Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;

    .line 57
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_EofObject:Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;

    .line 63
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetViewPortExtExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortExtEx;

    .line 66
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetMapModeObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;

    .line 76
    iput v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->mRecordType:I

    .line 79
    iput v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 82
    iput-boolean v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->mapFlag:Z

    .line 85
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    .line 106
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    .line 107
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    .line 108
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .line 109
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->initialise()V

    .line 110
    return-void
.end method

.method public constructor <init>([BFF)V
    .locals 2
    .param p1, "stream"    # [B
    .param p2, "width"    # F
    .param p3, "height"    # F

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EmrRecordTypeObject:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;

    .line 48
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EmfHeaderObject:Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;

    .line 51
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetStretchBltModeObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;

    .line 54
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_StretchdibitsObject:Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;

    .line 57
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_EofObject:Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;

    .line 63
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetViewPortExtExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortExtEx;

    .line 66
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetMapModeObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;

    .line 76
    iput v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->mRecordType:I

    .line 79
    iput v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 82
    iput-boolean v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->mapFlag:Z

    .line 85
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    .line 115
    sput p2, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->actWidth:F

    .line 116
    sput p3, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->actHeight:F

    .line 117
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    .line 118
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    .line 119
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .line 120
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->initialise()V

    .line 121
    return-void
.end method

.method public static getResizedHeight(F)F
    .locals 2
    .param p0, "heightParam"    # F

    .prologue
    .line 706
    sget v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->actHeight:F

    mul-float/2addr v0, p0

    sget v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->height:F

    div-float/2addr v0, v1

    return v0
.end method

.method public static getResizedWidth(F)F
    .locals 2
    .param p0, "widthParam"    # F

    .prologue
    .line 702
    sget v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->actWidth:F

    mul-float/2addr v0, p0

    sget v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->width:F

    div-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method public drawImage()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 137
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    .line 138
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->onDraw(Landroid/graphics/Canvas;)V

    .line 139
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->read()Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method public getEmfHeaderObject()Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;
    .locals 1

    .prologue
    .line 715
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EmfHeaderObject:Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;

    return-object v0
.end method

.method public initialise()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 146
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iput-object v5, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    .line 147
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iput v4, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    .line 148
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iput v4, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    .line 149
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iput v1, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandlePen:I

    .line 150
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iput v1, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandleBrush:I

    .line 151
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iput v1, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandleFont:I

    .line 152
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iput v1, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    .line 153
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iput-boolean v2, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->mapMode:Z

    .line 154
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iput-boolean v2, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->stockObject:Z

    .line 155
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iput v3, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->currentX:F

    .line 156
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iput v3, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->currentY:F

    .line 157
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iput-object v5, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    .line 158
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iput v2, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->pathStart:I

    .line 159
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iput-boolean v2, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->endPath:Z

    .line 172
    return-void
.end method

.method public read()Landroid/graphics/Bitmap;
    .locals 42

    .prologue
    .line 180
    const/16 v35, 0x0

    .line 181
    .local v35, "bitmap":Landroid/graphics/Bitmap;
    const/16 v38, 0x0

    .line 182
    .local v38, "numberOfRecord":I
    new-instance v39, Lcom/samsung/thumbnail/office/emf/EmfGlobals/DummyClass;

    invoke-direct/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/DummyClass;-><init>()V

    move-object/from16 v0, v39

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    .line 183
    const/16 v39, 0x0

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    new-instance v40, Ljava/util/ArrayList;

    invoke-direct/range {v40 .. v40}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v40

    move-object/from16 v1, v39

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    .line 186
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v39, v0

    if-nez v39, :cond_0

    .line 187
    const/16 v39, 0x0

    .line 697
    :goto_0
    return-object v39

    .line 190
    :cond_0
    new-instance v39, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v41, v0

    invoke-direct/range {v39 .. v41}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;-><init>([BI)V

    move-object/from16 v0, v39

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EmrRecordTypeObject:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;

    .line 191
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EmrRecordTypeObject:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;->getRecordType()I

    move-result v39

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->mRecordType:I

    .line 193
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->mRecordType:I

    move/from16 v39, v0

    packed-switch v39, :pswitch_data_0

    .line 673
    :pswitch_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EmrRecordTypeObject:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 677
    :cond_1
    :goto_1
    const/16 v39, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-boolean v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->mapMode:Z

    move/from16 v40, v0

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetWindowExtExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetWindowExtEx;

    move-object/from16 v39, v0

    if-eqz v39, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetViewPortExtExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortExtEx;

    move-object/from16 v39, v0

    if-eqz v39, :cond_4

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->mapFlag:Z

    move/from16 v39, v0

    if-nez v39, :cond_4

    .line 679
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetWindowExtExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetWindowExtEx;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetWindowExtEx;->getXextent()I

    move-result v40

    move/from16 v0, v40

    int-to-float v0, v0

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetViewPortExtExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortExtEx;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortExtEx;->getXextent()I

    move-result v41

    move/from16 v0, v41

    int-to-float v0, v0

    move/from16 v41, v0

    div-float v40, v40, v41

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    .line 681
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetWindowExtExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetWindowExtEx;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetWindowExtEx;->getYextent()I

    move-result v40

    move/from16 v0, v40

    int-to-float v0, v0

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetViewPortExtExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortExtEx;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortExtEx;->getYextent()I

    move-result v41

    move/from16 v0, v41

    int-to-float v0, v0

    move/from16 v41, v0

    div-float v40, v40, v41

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    .line 683
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    move/from16 v39, v0

    const/high16 v40, 0x3f800000    # 1.0f

    cmpg-float v39, v39, v40

    if-gez v39, :cond_2

    .line 684
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    const/high16 v40, 0x3f800000    # 1.0f

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    .line 685
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    move/from16 v39, v0

    const/high16 v40, 0x3f800000    # 1.0f

    cmpg-float v39, v39, v40

    if-gez v39, :cond_3

    .line 686
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    const/high16 v40, 0x3f800000    # 1.0f

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    .line 687
    :cond_3
    const/16 v39, 0x1

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->mapFlag:Z

    .line 690
    :cond_4
    add-int/lit8 v38, v38, 0x1

    .line 694
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EmfHeaderObject:Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;

    move-object/from16 v39, v0

    if-eqz v39, :cond_5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->mRecordType:I

    move/from16 v39, v0

    const/16 v40, 0xe

    move/from16 v0, v39

    move/from16 v1, v40

    if-eq v0, v1, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EmfHeaderObject:Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->getNumOfRecords()I

    move-result v39

    move/from16 v0, v38

    move/from16 v1, v39

    if-lt v0, v1, :cond_0

    :cond_5
    move-object/from16 v39, v35

    .line 697
    goto/16 :goto_0

    .line 196
    :pswitch_1
    new-instance v39, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v41, v0

    invoke-direct/range {v39 .. v41}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;-><init>([BI)V

    move-object/from16 v0, v39

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EmfHeaderObject:Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;

    .line 197
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EmfHeaderObject:Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->readEmfHeader()V

    .line 199
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EmfHeaderObject:Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 201
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EmfHeaderObject:Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->getBoundsBottom()I

    move-result v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EmfHeaderObject:Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->getBoundsLeft()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    int-to-float v0, v0

    move/from16 v39, v0

    sput v39, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->height:F

    .line 203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EmfHeaderObject:Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->getBoundsTop()I

    move-result v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EmfHeaderObject:Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->getBoundsRight()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    int-to-float v0, v0

    move/from16 v39, v0

    sput v39, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->width:F

    .line 208
    sget v39, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->width:F

    invoke-static/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v39

    move/from16 v0, v39

    float-to-int v0, v0

    move/from16 v39, v0

    sget v40, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->height:F

    invoke-static/range {v40 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v40

    move/from16 v0, v40

    float-to-int v0, v0

    move/from16 v40, v0

    sget-object v41, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static/range {v39 .. v41}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v35

    .line 213
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 214
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    invoke-virtual/range {v39 .. v40}, Landroid/graphics/Canvas;->drawColor(I)V

    goto/16 :goto_1

    .line 218
    :pswitch_2
    new-instance v32, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;-><init>([BI)V

    .line 220
    .local v32, "EMR_SetViewPortOrgexObject":Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->readEmfSetViewPortOrgex()V

    .line 221
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    goto/16 :goto_1

    .line 225
    .end local v32    # "EMR_SetViewPortOrgexObject":Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;
    :pswitch_3
    new-instance v33, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetWindowOrgex;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, v33

    move-object/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetWindowOrgex;-><init>([BI)V

    .line 227
    .local v33, "EMR_SetWindowOrgexObject":Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetWindowOrgex;
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetWindowOrgex;->readEmfSetWindowOrgex()V

    .line 228
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetWindowOrgex;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    goto/16 :goto_1

    .line 232
    .end local v33    # "EMR_SetWindowOrgexObject":Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetWindowOrgex;
    :pswitch_4
    new-instance v39, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v41, v0

    invoke-direct/range {v39 .. v41}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;-><init>([BI)V

    move-object/from16 v0, v39

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetMapModeObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;

    .line 233
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetMapModeObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->readEmfSetMapMode()V

    .line 235
    sget-object v39, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;->MM_ANISOTROPIC:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;->getMapModeType()I

    move-result v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetMapModeObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->getMapMode()I

    move-result v40

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_6

    .line 237
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    const/16 v40, 0x1

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput-boolean v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->mapMode:Z

    .line 240
    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetMapModeObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    goto/16 :goto_1

    .line 239
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput-boolean v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->mapMode:Z

    goto :goto_2

    .line 244
    :pswitch_5
    new-instance v39, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortExtEx;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v41, v0

    invoke-direct/range {v39 .. v41}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortExtEx;-><init>([BI)V

    move-object/from16 v0, v39

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetViewPortExtExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortExtEx;

    .line 246
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetViewPortExtExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortExtEx;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortExtEx;->readEmfSetViewPortExtEx()V

    .line 248
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetViewPortExtExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortExtEx;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortExtEx;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    goto/16 :goto_1

    .line 253
    :pswitch_6
    new-instance v39, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetWindowExtEx;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v41, v0

    invoke-direct/range {v39 .. v41}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetWindowExtEx;-><init>([BI)V

    move-object/from16 v0, v39

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetWindowExtExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetWindowExtEx;

    .line 255
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetWindowExtExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetWindowExtEx;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetWindowExtEx;->readEmfSetWindowExtEx()V

    .line 257
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetWindowExtExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetWindowExtEx;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetWindowExtEx;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    goto/16 :goto_1

    .line 262
    :pswitch_7
    new-instance v30, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetBkMode;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetBkMode;-><init>([BI)V

    .line 264
    .local v30, "EMR_SetBkModeObject":Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetBkMode;
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetBkMode;->readEmf_SelectObject()V

    .line 266
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetBkMode;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    goto/16 :goto_1

    .line 269
    .end local v30    # "EMR_SetBkModeObject":Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetBkMode;
    :pswitch_8
    new-instance v14, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, v39

    move/from16 v1, v40

    invoke-direct {v14, v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;-><init>([BI)V

    .line 271
    .local v14, "EMR_ModifyWorldTransformObject":Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;
    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->readEmfModifyWorldTransform()V

    .line 272
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 274
    sget-object v39, Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;->MWT_LEFTMULTIPLY:Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;->getModifyWorldTransformModeType()I

    move-result v39

    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->getmodifyWorldTransformMode()I

    move-result v40

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_1

    .line 277
    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->getxFormM11()F

    move-result v39

    const/high16 v40, 0x3f800000    # 1.0f

    cmpg-float v39, v39, v40

    if-gez v39, :cond_7

    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->getxFormM22()F

    move-result v39

    const/high16 v40, 0x3f800000    # 1.0f

    cmpg-float v39, v39, v40

    if-gez v39, :cond_7

    .line 279
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    const/high16 v40, 0x3f800000    # 1.0f

    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->getxFormM11()F

    move-result v41

    div-float v40, v40, v41

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    .line 281
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    const/high16 v40, 0x3f800000    # 1.0f

    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->getxFormM22()F

    move-result v41

    div-float v40, v40, v41

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    goto/16 :goto_1

    .line 284
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    const/high16 v40, 0x3f800000    # 1.0f

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    .line 285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    const/high16 v40, 0x3f800000    # 1.0f

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    goto/16 :goto_1

    .line 291
    .end local v14    # "EMR_ModifyWorldTransformObject":Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;
    :pswitch_9
    new-instance v29, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;-><init>([BI)V

    .line 293
    .local v29, "EMR_SelectObjectInstance":Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->readEmf_SelectObject(Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V

    .line 294
    const/16 v39, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-boolean v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->stockObject:Z

    move/from16 v40, v0

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_9

    .line 295
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->getIhObject()I

    move-result v40

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    .line 319
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    goto/16 :goto_1

    .line 298
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Ljava/util/ArrayList;->size()I

    move-result v36

    .line 299
    .local v36, "emrSize":I
    const/16 v37, 0x0

    .local v37, "i":I
    :goto_3
    move/from16 v0, v37

    move/from16 v1, v36

    if-ge v0, v1, :cond_8

    .line 300
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->getIhObject()I

    move-result v40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->gethandleNumber()I

    move-result v39

    move/from16 v0, v40

    move/from16 v1, v39

    if-ne v0, v1, :cond_a

    .line 302
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->getObjectType()I

    move-result v39

    const/16 v40, 0x27

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_b

    .line 304
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->getIhObject()I

    move-result v40

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandleBrush:I

    .line 299
    :cond_a
    :goto_4
    add-int/lit8 v37, v37, 0x1

    goto :goto_3

    .line 307
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->getObjectType()I

    move-result v39

    const/16 v40, 0x26

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_c

    .line 309
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->getIhObject()I

    move-result v40

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandlePen:I

    goto :goto_4

    .line 312
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->getObjectType()I

    move-result v39

    const/16 v40, 0x52

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_a

    .line 314
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->getIhObject()I

    move-result v40

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandleFont:I

    goto :goto_4

    .line 324
    .end local v29    # "EMR_SelectObjectInstance":Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;
    .end local v36    # "emrSize":I
    .end local v37    # "i":I
    :pswitch_a
    new-instance v7, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_DeleteObject;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, v39

    move/from16 v1, v40

    invoke-direct {v7, v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_DeleteObject;-><init>([BI)V

    .line 326
    .local v7, "EMR_DeleteObjectInstance":Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_DeleteObject;
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_DeleteObject;->readEmf_SelectObject()V

    .line 328
    const/16 v37, 0x0

    .restart local v37    # "i":I
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Ljava/util/ArrayList;->size()I

    move-result v39

    move/from16 v0, v37

    move/from16 v1, v39

    if-ge v0, v1, :cond_11

    .line 329
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_DeleteObject;->getIhObject()I

    move-result v40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->gethandleNumber()I

    move-result v39

    move/from16 v0, v40

    move/from16 v1, v39

    if-ne v0, v1, :cond_e

    .line 331
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->getObjectType()I

    move-result v39

    const/16 v40, 0x27

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_d

    .line 332
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_DeleteObject;->getIhObject()I

    move-result v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandleBrush:I

    move/from16 v40, v0

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_f

    .line 333
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    const/16 v40, -0x1

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandleBrush:I

    .line 345
    :cond_d
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 328
    :cond_e
    add-int/lit8 v37, v37, 0x1

    goto/16 :goto_5

    .line 335
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->getObjectType()I

    move-result v39

    const/16 v40, 0x26

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_d

    .line 337
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_DeleteObject;->getIhObject()I

    move-result v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandlePen:I

    move/from16 v40, v0

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_10

    .line 338
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    const/16 v40, -0x1

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandlePen:I

    goto :goto_6

    .line 340
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->getObjectType()I

    move-result v39

    const/16 v40, 0x52

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_d

    .line 342
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_DeleteObject;->getIhObject()I

    move-result v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandleFont:I

    move/from16 v40, v0

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_d

    .line 343
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    const/16 v40, -0x1

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandleFont:I

    goto/16 :goto_6

    .line 349
    :cond_11
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_DeleteObject;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    goto/16 :goto_1

    .line 352
    .end local v7    # "EMR_DeleteObjectInstance":Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_DeleteObject;
    .end local v37    # "i":I
    :pswitch_b
    new-instance v4, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v41, v0

    move-object/from16 v0, v39

    move/from16 v1, v40

    move-object/from16 v2, v41

    invoke-direct {v4, v0, v1, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;-><init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V

    .line 354
    .local v4, "EMR_ArcObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->readEmfArc()V

    .line 355
    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    .line 357
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 358
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->prepare()V

    .line 359
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v40, v0

    invoke-interface/range {v39 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->render(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 363
    .end local v4    # "EMR_ArcObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    move-object/from16 v39, v0

    if-eqz v39, :cond_12

    .line 364
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    move-object/from16 v0, v40

    move-object/from16 v1, v39

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    .line 365
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    const/16 v40, 0x1

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->pathStart:I

    .line 366
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput-boolean v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->endPath:Z

    .line 367
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    new-instance v40, Landroid/graphics/Path;

    invoke-direct/range {v40 .. v40}, Landroid/graphics/Path;-><init>()V

    move-object/from16 v0, v40

    move-object/from16 v1, v39

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    .line 368
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EmrRecordTypeObject:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    goto/16 :goto_1

    .line 373
    :pswitch_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Landroid/graphics/Path;->close()V

    .line 374
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    const/16 v40, 0x1

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput-boolean v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->endPath:Z

    .line 375
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EmrRecordTypeObject:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    goto/16 :goto_1

    .line 379
    :pswitch_e
    new-instance v16, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v41, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v39

    move/from16 v2, v40

    move-object/from16 v3, v41

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;-><init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V

    .line 381
    .local v16, "EMR_PieObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->readEmfPie()V

    .line 382
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    .line 384
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 385
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->prepare()V

    .line 386
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v40, v0

    invoke-interface/range {v39 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->render(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 389
    .end local v16    # "EMR_PieObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;
    :pswitch_f
    new-instance v28, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;-><init>([BI)V

    .line 391
    .local v28, "EMR_RoundRectObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;
    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->readEmfRoundRect()V

    .line 392
    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    .line 394
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 395
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->prepare()V

    .line 396
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v40, v0

    invoke-interface/range {v39 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->render(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 401
    .end local v28    # "EMR_RoundRectObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;
    :pswitch_10
    new-instance v27, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v41, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v39

    move/from16 v2, v40

    move-object/from16 v3, v41

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;-><init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V

    .line 403
    .local v27, "EMR_RectangleObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->readEmfrectangle()V

    .line 404
    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    .line 406
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 407
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->prepare()V

    .line 408
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v40, v0

    invoke-interface/range {v39 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->render(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 411
    .end local v27    # "EMR_RectangleObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;
    :pswitch_11
    new-instance v12, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v41, v0

    move-object/from16 v0, v39

    move/from16 v1, v40

    move-object/from16 v2, v41

    invoke-direct {v12, v0, v1, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;-><init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V

    .line 413
    .local v12, "EMR_FillPathObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->readEmfFillPath()V

    .line 414
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    .line 416
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 417
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->prepare()V

    .line 418
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v40, v0

    invoke-interface/range {v39 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->render(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 422
    .end local v12    # "EMR_FillPathObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;
    :pswitch_12
    new-instance v34, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v41, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v39

    move/from16 v2, v40

    move-object/from16 v3, v41

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;-><init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V

    .line 424
    .local v34, "EMR_StrokePathObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;
    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->readEmfStrokePath()V

    .line 425
    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    .line 427
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 428
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->prepare()V

    .line 429
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v40, v0

    invoke-interface/range {v39 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->render(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 433
    .end local v34    # "EMR_StrokePathObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;
    :pswitch_13
    new-instance v5, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, v39

    move/from16 v1, v40

    invoke-direct {v5, v0, v1}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;-><init>([BI)V

    .line 435
    .local v5, "EMR_BrushIndirectObject":Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->readEmfCreateBrushIndirect()V

    .line 436
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Ljava/util/ArrayList;->size()I

    move-result v40

    move-object/from16 v0, v39

    move/from16 v1, v40

    invoke-virtual {v0, v1, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 439
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->getSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    goto/16 :goto_1

    .line 444
    .end local v5    # "EMR_BrushIndirectObject":Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;
    :pswitch_14
    new-instance v6, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, v39

    move/from16 v1, v40

    invoke-direct {v6, v0, v1}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;-><init>([BI)V

    .line 446
    .local v6, "EMR_CreatePenObject":Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->readEmfCreatePen()V

    .line 447
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Ljava/util/ArrayList;->size()I

    move-result v40

    move-object/from16 v0, v39

    move/from16 v1, v40

    invoke-virtual {v0, v1, v6}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 450
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    goto/16 :goto_1

    .line 455
    .end local v6    # "EMR_CreatePenObject":Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;
    :pswitch_15
    new-instance v31, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v39

    move/from16 v2, v40

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;-><init>([BI)V

    .line 457
    .local v31, "EMR_SetTextAlignObject":Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->readEmfSetTextAlign()V

    .line 458
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v39

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EMR_TextAlignObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;

    .line 459
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    goto/16 :goto_1

    .line 464
    .end local v31    # "EMR_SetTextAlignObject":Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;
    :pswitch_16
    new-instance v10, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v41, v0

    move-object/from16 v0, v39

    move/from16 v1, v40

    move-object/from16 v2, v41

    invoke-direct {v10, v0, v1, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;-><init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V

    .line 466
    .local v10, "EMR_ExtTextOutAObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->readEmfExtTextOutW()V

    .line 467
    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    .line 468
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->prepare()V

    .line 469
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v40, v0

    invoke-interface/range {v39 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->render(Landroid/graphics/Canvas;)V

    .line 470
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->getSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    goto/16 :goto_1

    .line 475
    .end local v10    # "EMR_ExtTextOutAObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;
    :pswitch_17
    new-instance v11, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutW;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v41, v0

    move-object/from16 v0, v39

    move/from16 v1, v40

    move-object/from16 v2, v41

    invoke-direct {v11, v0, v1, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutW;-><init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V

    .line 477
    .local v11, "EMR_ExtTextOutWObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutW;
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutW;->readEmfExtTextOutW()V

    .line 478
    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    .line 480
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->prepare()V

    .line 481
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v40, v0

    invoke-interface/range {v39 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->render(Landroid/graphics/Canvas;)V

    .line 482
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutW;->getSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    goto/16 :goto_1

    .line 487
    .end local v11    # "EMR_ExtTextOutWObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutW;
    :pswitch_18
    new-instance v9, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, v39

    move/from16 v1, v40

    invoke-direct {v9, v0, v1}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;-><init>([BI)V

    .line 489
    .local v9, "EMR_ExtCreateFontIndirectWObject":Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->readEmfExtCreateFontIndirectW()V

    .line 491
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Ljava/util/ArrayList;->size()I

    move-result v40

    move-object/from16 v0, v39

    move/from16 v1, v40

    invoke-virtual {v0, v1, v9}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 494
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->getSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    goto/16 :goto_1

    .line 499
    .end local v9    # "EMR_ExtCreateFontIndirectWObject":Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;
    :pswitch_19
    new-instance v20, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v41, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v39

    move/from16 v2, v40

    move-object/from16 v3, v41

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier;-><init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V

    .line 501
    .local v20, "EMR_PolybeizierObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier;->readEmfPolybeizier()V

    .line 502
    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    .line 504
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier;->getSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 505
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->prepare()V

    .line 506
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v40, v0

    invoke-interface/range {v39 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->render(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 511
    .end local v20    # "EMR_PolybeizierObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier;
    :pswitch_1a
    new-instance v19, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v41, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v39

    move/from16 v2, v40

    move-object/from16 v3, v41

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;-><init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V

    .line 513
    .local v19, "EMR_Polybeizier16Object":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->readEmfPolybeizier16()V

    .line 514
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    .line 516
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->getSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 517
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->prepare()V

    .line 518
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v40, v0

    invoke-interface/range {v39 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->render(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 521
    .end local v19    # "EMR_Polybeizier16Object":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;
    :pswitch_1b
    new-instance v21, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v41, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v39

    move/from16 v2, v40

    move-object/from16 v3, v41

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;-><init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V

    .line 523
    .local v21, "EMR_PolybezierTo16Object":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->readPolybezierTo16()V

    .line 524
    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    .line 526
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 527
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->prepare()V

    .line 528
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v40, v0

    invoke-interface/range {v39 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->render(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 532
    .end local v21    # "EMR_PolybezierTo16Object":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;
    :pswitch_1c
    new-instance v22, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v41, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v39

    move/from16 v2, v40

    move-object/from16 v3, v41

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;-><init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V

    .line 534
    .local v22, "EMR_Polygon16Object":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->readEmfPolygon16()V

    .line 535
    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    .line 537
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 538
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->prepare()V

    .line 539
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v40, v0

    invoke-interface/range {v39 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->render(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 544
    .end local v22    # "EMR_Polygon16Object":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;
    :pswitch_1d
    new-instance v23, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v41, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v39

    move/from16 v2, v40

    move-object/from16 v3, v41

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;-><init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V

    .line 546
    .local v23, "EMR_PolygonObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->readEmfPolygon()V

    .line 547
    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    .line 549
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 550
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->prepare()V

    .line 551
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v40, v0

    invoke-interface/range {v39 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->render(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 555
    .end local v23    # "EMR_PolygonObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;
    :pswitch_1e
    new-instance v17, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v41, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v39

    move/from16 v2, v40

    move-object/from16 v3, v41

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;-><init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V

    .line 557
    .local v17, "EMR_PolyPolygon16Object":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->readEmfPolyPolygon16()V

    .line 558
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    .line 560
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 561
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->prepare()V

    .line 562
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v40, v0

    invoke-interface/range {v39 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->render(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 566
    .end local v17    # "EMR_PolyPolygon16Object":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;
    :pswitch_1f
    new-instance v24, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v41, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    move/from16 v2, v40

    move-object/from16 v3, v41

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;-><init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V

    .line 568
    .local v24, "EMR_Polyline16Object":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->readpolyline16()V

    .line 569
    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    .line 571
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 572
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->prepare()V

    .line 573
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v40, v0

    invoke-interface/range {v39 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->render(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 576
    .end local v24    # "EMR_Polyline16Object":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;
    :pswitch_20
    new-instance v26, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v41, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v39

    move/from16 v2, v40

    move-object/from16 v3, v41

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;-><init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V

    .line 578
    .local v26, "EMR_PolylineTo16Object":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->readPolylineTo16()V

    .line 579
    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    .line 581
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 582
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->prepare()V

    .line 583
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v40, v0

    invoke-interface/range {v39 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->render(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 587
    .end local v26    # "EMR_PolylineTo16Object":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;
    :pswitch_21
    new-instance v18, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v41, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v39

    move/from16 v2, v40

    move-object/from16 v3, v41

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;-><init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V

    .line 589
    .local v18, "EMR_PolyPolyline16Object":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->readPolyPolyline16()V

    .line 590
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    .line 592
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 593
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->prepare()V

    .line 594
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v40, v0

    invoke-interface/range {v39 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->render(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 599
    .end local v18    # "EMR_PolyPolyline16Object":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;
    :pswitch_22
    new-instance v25, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v41, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v39

    move/from16 v2, v40

    move-object/from16 v3, v41

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;-><init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V

    .line 601
    .local v25, "EMR_PolylineObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->readpolyline()V

    .line 602
    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    .line 604
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 605
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->prepare()V

    .line 606
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v40, v0

    invoke-interface/range {v39 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->render(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 611
    .end local v25    # "EMR_PolylineObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;
    :pswitch_23
    new-instance v15, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, v39

    move/from16 v1, v40

    invoke-direct {v15, v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;-><init>([BI)V

    .line 613
    .local v15, "EMR_MoveToExObject":Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    invoke-virtual {v15, v0}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->readEmfMoveToEx(Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V

    .line 614
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iput-object v15, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->MoveToExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;

    .line 616
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    goto/16 :goto_1

    .line 621
    .end local v15    # "EMR_MoveToExObject":Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;
    :pswitch_24
    new-instance v13, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v41, v0

    move-object/from16 v0, v39

    move/from16 v1, v40

    move-object/from16 v2, v41

    invoke-direct {v13, v0, v1, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;-><init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V

    .line 623
    .local v13, "EMR_LineToObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;
    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->readLineTo()V

    .line 624
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    .line 626
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 627
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->prepare()V

    .line 628
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v40, v0

    invoke-interface/range {v39 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->render(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 634
    .end local v13    # "EMR_LineToObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;
    :pswitch_25
    new-instance v39, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v41, v0

    invoke-direct/range {v39 .. v41}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;-><init>([BI)V

    move-object/from16 v0, v39

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetStretchBltModeObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;

    .line 636
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetStretchBltModeObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->readSetStretchBltMode()V

    .line 638
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_SetStretchBltModeObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    goto/16 :goto_1

    .line 643
    :pswitch_26
    new-instance v8, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v41, v0

    move-object/from16 v0, v39

    move/from16 v1, v40

    move-object/from16 v2, v41

    invoke-direct {v8, v0, v1, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;-><init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V

    .line 645
    .local v8, "EMR_EllipseObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->readEmfEllipse()V

    .line 646
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    .line 648
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;->getSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 649
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->prepare()V

    .line 650
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v40, v0

    invoke-interface/range {v39 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->render(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 655
    .end local v8    # "EMR_EllipseObject":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Ellipse;
    :pswitch_27
    new-instance v39, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v41, v0

    invoke-direct/range {v39 .. v41}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;-><init>([BI)V

    move-object/from16 v0, v39

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_StretchdibitsObject:Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;

    .line 657
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_StretchdibitsObject:Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->readEmfStretchdibits()V

    .line 658
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_StretchdibitsObject:Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    .line 660
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_StretchdibitsObject:Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->getSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    .line 661
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->prepare()V

    .line 662
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMF:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v40, v0

    invoke-interface/range {v39 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;->render(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 667
    :pswitch_28
    new-instance v39, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->fileContent:[B

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v41, v0

    invoke-direct/range {v39 .. v41}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;-><init>([BI)V

    move-object/from16 v0, v39

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_EofObject:Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;

    .line 668
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_EofObject:Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->readEMR_Eof()V

    .line 669
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->EMR_EofObject:Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->getRecordSize()I

    move-result v40

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->offset:I

    goto/16 :goto_1

    .line 193
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_19
        :pswitch_1d
        :pswitch_22
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_3
        :pswitch_5
        :pswitch_2
        :pswitch_0
        :pswitch_28
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_25
        :pswitch_15
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_23
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_14
        :pswitch_13
        :pswitch_a
        :pswitch_0
        :pswitch_26
        :pswitch_10
        :pswitch_f
        :pswitch_b
        :pswitch_0
        :pswitch_e
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_24
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_11
        :pswitch_0
        :pswitch_12
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_27
        :pswitch_18
        :pswitch_16
        :pswitch_17
        :pswitch_1a
        :pswitch_1c
        :pswitch_1f
        :pswitch_1b
        :pswitch_20
        :pswitch_21
        :pswitch_1e
    .end packed-switch
.end method

.method public setActualHeight(F)V
    .locals 0
    .param p1, "height"    # F

    .prologue
    .line 128
    sput p1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->actHeight:F

    .line 129
    return-void
.end method

.method public setActualWidth(F)V
    .locals 0
    .param p1, "width"    # F

    .prologue
    .line 124
    sput p1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->actWidth:F

    .line 125
    return-void
.end method
