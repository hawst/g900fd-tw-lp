.class public Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;
.super Ljava/lang/Object;
.source "EMR_Globals.java"


# instance fields
.field public EMR_TextAlignObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;

.field public EmrObjectArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;",
            ">;"
        }
    .end annotation
.end field

.field public MoveToExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;

.field public beginEndPath:Landroid/graphics/Path;

.field public currentX:F

.field public currentY:F

.field public endPath:Z

.field public mapMode:Z

.field public pathStart:I

.field public selectedHandleBrush:I

.field public selectedHandleFont:I

.field public selectedHandlePen:I

.field public selectedStockObject:I

.field public stockObject:Z

.field public xScale:F

.field public yScale:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMoveToExObject()Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->MoveToExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;

    return-object v0
.end method

.method public setMoveToExObject(Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;)V
    .locals 0
    .param p1, "moveToExObject"    # Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->MoveToExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;

    .line 35
    return-void
.end method
