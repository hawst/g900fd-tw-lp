.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStylesListHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "TableStylesListHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStyleHandler$ITableStyleConsumer;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/16 v4, 0x1f

    .line 22
    const-string/jumbo v3, "tblStyleLst"

    invoke-direct {p0, v4, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 24
    const/4 v3, 0x1

    new-array v0, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 26
    .local v0, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStyleHandler;

    invoke-direct {v2, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStyleHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStyleHandler$ITableStyleConsumer;)V

    .line 33
    .local v2, "tableStyleHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStyleHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    const-string/jumbo v3, "tblStyle"

    invoke-direct {v1, v4, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 36
    .local v1, "tableStHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    const/4 v3, 0x0

    aput-object v1, v0, v3

    .line 38
    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStylesListHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 39
    return-void
.end method


# virtual methods
.method public handleStyle(Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 0
    .param p1, "tblStyle"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;
    .param p2, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 45
    return-void
.end method
