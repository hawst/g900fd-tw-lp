.class public Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;
.super Lorg/apache/poi/POIXMLDocumentPart;
.source "XWPFPictureData.java"


# static fields
.field protected static final RELATIONS:[Lorg/apache/poi/POIXMLRelation;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 50
    const/16 v0, 0x9

    new-array v0, v0, [Lorg/apache/poi/POIXMLRelation;

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;->RELATIONS:[Lorg/apache/poi/POIXMLRelation;

    .line 51
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;->RELATIONS:[Lorg/apache/poi/POIXMLRelation;

    const/4 v1, 0x2

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->IMAGE_EMF:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    aput-object v2, v0, v1

    .line 52
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;->RELATIONS:[Lorg/apache/poi/POIXMLRelation;

    const/4 v1, 0x3

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->IMAGE_WMF:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    aput-object v2, v0, v1

    .line 53
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;->RELATIONS:[Lorg/apache/poi/POIXMLRelation;

    const/4 v1, 0x4

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->IMAGE_PICT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    aput-object v2, v0, v1

    .line 54
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;->RELATIONS:[Lorg/apache/poi/POIXMLRelation;

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->IMAGE_JPEG:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    aput-object v2, v0, v1

    .line 55
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;->RELATIONS:[Lorg/apache/poi/POIXMLRelation;

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->IMAGE_PNG:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    aput-object v2, v0, v1

    .line 56
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;->RELATIONS:[Lorg/apache/poi/POIXMLRelation;

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->IMAGE_DIB:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    aput-object v2, v0, v1

    .line 57
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;->RELATIONS:[Lorg/apache/poi/POIXMLRelation;

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->IMAGE_GIF:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    aput-object v2, v0, v1

    .line 58
    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lorg/apache/poi/POIXMLDocumentPart;-><init>()V

    .line 66
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V
    .locals 0
    .param p1, "part"    # Lorg/apache/poi/openxml4j/opc/PackagePart;
    .param p2, "rel"    # Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/POIXMLDocumentPart;-><init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V

    .line 82
    return-void
.end method


# virtual methods
.method public getData()[B
    .locals 4

    .prologue
    .line 99
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/poi/util/IOUtils;->toByteArray(Ljava/io/InputStream;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 103
    :goto_0
    return-object v1

    .line 100
    :catch_0
    move-exception v0

    .line 101
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v1, "DocumentService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getPartName()Lorg/apache/poi/openxml4j/opc/PackagePartName;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->getName()Ljava/lang/String;

    move-result-object v0

    .line 113
    .local v0, "name":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 114
    const/4 v1, 0x0

    .line 115
    :goto_0
    return-object v1

    :cond_0
    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getPictureType()I
    .locals 3

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getContentType()Ljava/lang/String;

    move-result-object v0

    .line 140
    .local v0, "contentType":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;->RELATIONS:[Lorg/apache/poi/POIXMLRelation;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 141
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;->RELATIONS:[Lorg/apache/poi/POIXMLRelation;

    aget-object v2, v2, v1

    if-nez v2, :cond_1

    .line 140
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 144
    :cond_1
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;->RELATIONS:[Lorg/apache/poi/POIXMLRelation;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lorg/apache/poi/POIXMLRelation;->getContentType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 148
    .end local v1    # "i":I
    :goto_1
    return v1

    .restart local v1    # "i":I
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public suggestFileExtension()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getPartName()Lorg/apache/poi/openxml4j/opc/PackagePartName;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->getExtension()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
