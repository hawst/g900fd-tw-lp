.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocGroupHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XDocGroupHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 25
    const/16 v0, 0x28

    const-string/jumbo v1, "group"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 26
    return-void
.end method

.method private init()V
    .locals 29

    .prologue
    .line 29
    const/16 v27, 0xb

    move/from16 v0, v27

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    move-object/from16 v20, v0

    .line 31
    .local v20, "seqDescriptors":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v26, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/WrapHandler;

    const-string/jumbo v27, "wrap"

    invoke-direct/range {v26 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/WrapHandler;-><init>(Ljava/lang/String;)V

    .line 32
    .local v26, "wrapHandler":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/WrapHandler;
    new-instance v25, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;

    const/16 v27, 0x29

    const-string/jumbo v28, "wrap"

    move-object/from16 v0, v25

    move/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v26

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 35
    .local v25, "wrapDes":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;
    const/16 v27, 0x0

    aput-object v25, v20, v27

    .line 37
    new-instance v22, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeHandler;

    invoke-direct/range {v22 .. v22}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeHandler;-><init>()V

    .line 38
    .local v22, "shapeHandler":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeHandler;
    new-instance v21, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;

    const-string/jumbo v27, "shape"

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 40
    .local v21, "shapeDes":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;
    const/16 v27, 0x1

    aput-object v21, v20, v27

    .line 42
    new-instance v17, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/RectHandler;

    const-string/jumbo v27, "rect"

    move-object/from16 v0, v17

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/RectHandler;-><init>(Ljava/lang/String;)V

    .line 43
    .local v17, "rectHandler":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/RectHandler;
    new-instance v16, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;

    const-string/jumbo v27, "rect"

    move-object/from16 v0, v16

    move-object/from16 v1, v27

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 45
    .local v16, "rectDes":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;
    const/16 v27, 0x2

    aput-object v16, v20, v27

    .line 47
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ArcHandler;

    invoke-direct {v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ArcHandler;-><init>()V

    .line 48
    .local v5, "arcHandler":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ArcHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;

    const-string/jumbo v27, "arc"

    move-object/from16 v0, v27

    invoke-direct {v4, v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 50
    .local v4, "arcDes":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;
    const/16 v27, 0x3

    aput-object v4, v20, v27

    .line 52
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/LineHandler;

    invoke-direct {v11}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/LineHandler;-><init>()V

    .line 53
    .local v11, "lineHandler":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/LineHandler;
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;

    const-string/jumbo v27, "line"

    move-object/from16 v0, v27

    invoke-direct {v10, v0, v11}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 55
    .local v10, "lineDes":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;
    const/16 v27, 0x4

    aput-object v10, v20, v27

    .line 57
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/CurveHandler;

    invoke-direct {v7}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/CurveHandler;-><init>()V

    .line 58
    .local v7, "curveHandler":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/CurveHandler;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;

    const-string/jumbo v27, "curve"

    move-object/from16 v0, v27

    invoke-direct {v6, v0, v7}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 60
    .local v6, "curveDes":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;
    const/16 v27, 0x5

    aput-object v6, v20, v27

    .line 62
    new-instance v15, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/PolylineHandler;

    invoke-direct {v15}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/PolylineHandler;-><init>()V

    .line 63
    .local v15, "polylineHandler":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/PolylineHandler;
    new-instance v14, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;

    const-string/jumbo v27, "polyline"

    move-object/from16 v0, v27

    invoke-direct {v14, v0, v15}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 65
    .local v14, "polylineDes":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;
    const/16 v27, 0x6

    aput-object v14, v20, v27

    .line 67
    new-instance v19, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/RectHandler;

    const-string/jumbo v27, "roundrect"

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/RectHandler;-><init>(Ljava/lang/String;)V

    .line 68
    .local v19, "roundRectHandler":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/RectHandler;
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;

    const-string/jumbo v27, "roundrect"

    move-object/from16 v0, v18

    move-object/from16 v1, v27

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 70
    .local v18, "roundRectDes":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;
    const/16 v27, 0x7

    aput-object v18, v20, v27

    .line 72
    new-instance v24, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeTypeHandler;

    invoke-direct/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeTypeHandler;-><init>()V

    .line 73
    .local v24, "shapeTypeHandler":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeTypeHandler;
    new-instance v23, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;

    const-string/jumbo v27, "shapetype"

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 75
    .local v23, "shapeTypeDes":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;
    const/16 v27, 0x8

    aput-object v23, v20, v27

    .line 77
    new-instance v13, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/OvalHandler;

    invoke-direct {v13}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/OvalHandler;-><init>()V

    .line 78
    .local v13, "ovalHandler":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/OvalHandler;
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;

    const-string/jumbo v27, "oval"

    move-object/from16 v0, v27

    invoke-direct {v12, v0, v13}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 80
    .local v12, "ovalDes":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;
    const/16 v27, 0x9

    aput-object v12, v20, v27

    .line 82
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocGroupHandler;

    invoke-direct {v9}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocGroupHandler;-><init>()V

    .line 83
    .local v9, "groupHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocGroupHandler;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;

    const-string/jumbo v27, "group"

    move-object/from16 v0, v27

    invoke-direct {v8, v0, v9}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 85
    .local v8, "groupDes":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;
    const/16 v27, 0xa

    aput-object v8, v20, v27

    .line 87
    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocGroupHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 88
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 111
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 112
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->endGroupStyle()V

    .line 113
    return-void
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 94
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 95
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 100
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 101
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocGroupHandler;->init()V

    .line 103
    const-string/jumbo v1, "style"

    invoke-virtual {p0, p3, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocGroupHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 104
    .local v0, "style":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 105
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->addGroupStyle(Ljava/lang/String;)V

    .line 107
    :cond_0
    return-void
.end method
