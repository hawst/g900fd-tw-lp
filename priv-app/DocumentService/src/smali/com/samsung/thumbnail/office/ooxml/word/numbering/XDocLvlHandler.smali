.class public Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XDocLvlHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$IXPPrObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$IXRPrObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$IXDocLvlConsumer;,
        Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$PStyle;,
        Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlJc;,
        Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$PicBulletId;,
        Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlText;,
        Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlIsLgl;,
        Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlRestart;,
        Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlNumFmt;,
        Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlStart;
    }
.end annotation


# instance fields
.field private numLvl:Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;

.field private observer:Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$IXDocLvlConsumer;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$IXDocLvlConsumer;)V
    .locals 33
    .param p1, "observer"    # Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$IXDocLvlConsumer;

    .prologue
    .line 39
    const-string/jumbo v32, "lvl"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 40
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;->observer:Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$IXDocLvlConsumer;

    .line 42
    const/16 v32, 0xa

    move/from16 v0, v32

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    move-object/from16 v25, v0

    .line 44
    .local v25, "seqDes":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v26, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlStart;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlStart;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;)V

    .line 45
    .local v26, "start":Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlStart;
    new-instance v27, Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;

    const-string/jumbo v32, "start"

    move-object/from16 v0, v27

    move-object/from16 v1, v32

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 47
    .local v27, "startHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;
    new-instance v28, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v32, "start"

    move-object/from16 v0, v28

    move-object/from16 v1, v32

    move-object/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 49
    .local v28, "startSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v32, 0x0

    aput-object v28, v25, v32

    .line 51
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlNumFmt;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlNumFmt;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;)V

    .line 52
    .local v9, "numFmt":Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlNumFmt;
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;

    const-string/jumbo v32, "numFmt"

    move-object/from16 v0, v32

    invoke-direct {v10, v0, v9}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 54
    .local v10, "numFmtHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v32, "numFmt"

    move-object/from16 v0, v32

    invoke-direct {v11, v0, v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 56
    .local v11, "numFmtSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v32, 0x1

    aput-object v11, v25, v32

    .line 58
    new-instance v22, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlRestart;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlRestart;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;)V

    .line 59
    .local v22, "restart":Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlRestart;
    new-instance v23, Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;

    const-string/jumbo v32, "lvlRestart"

    move-object/from16 v0, v23

    move-object/from16 v1, v32

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 61
    .local v23, "restartHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;
    new-instance v24, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v32, "lvlRestart"

    move-object/from16 v0, v24

    move-object/from16 v1, v32

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 63
    .local v24, "restartSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v32, 0x2

    aput-object v24, v25, v32

    .line 65
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlIsLgl;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlIsLgl;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;)V

    .line 66
    .local v3, "isLgl":Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlIsLgl;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;

    const-string/jumbo v32, "isLgl"

    move-object/from16 v0, v32

    invoke-direct {v4, v0, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 68
    .local v4, "isLglHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v32, "isLgl"

    move-object/from16 v0, v32

    invoke-direct {v5, v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 70
    .local v5, "isLglSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v32, 0x3

    aput-object v5, v25, v32

    .line 72
    new-instance v29, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlText;

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlText;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;)V

    .line 73
    .local v29, "text":Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlText;
    new-instance v30, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;

    const-string/jumbo v32, "lvlText"

    move-object/from16 v0, v30

    move-object/from16 v1, v32

    move-object/from16 v2, v29

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 75
    .local v30, "textHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;
    new-instance v31, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v32, "lvlText"

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 77
    .local v31, "textSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v32, 0x4

    aput-object v31, v25, v32

    .line 79
    new-instance v17, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$PicBulletId;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$PicBulletId;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;)V

    .line 80
    .local v17, "picBulletId":Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$PicBulletId;
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;

    const-string/jumbo v32, "lvlPicBulletId"

    move-object/from16 v0, v18

    move-object/from16 v1, v32

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 82
    .local v18, "picBulletIdHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;
    new-instance v19, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v32, "lvlPicBulletId"

    move-object/from16 v0, v19

    move-object/from16 v1, v32

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 84
    .local v19, "picBulletIdSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v32, 0x5

    aput-object v19, v25, v32

    .line 86
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlJc;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlJc;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;)V

    .line 87
    .local v6, "jc":Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlJc;
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;

    const-string/jumbo v32, "lvlJc"

    move-object/from16 v0, v32

    invoke-direct {v7, v0, v6}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 89
    .local v7, "jcHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v32, "lvlJc"

    move-object/from16 v0, v32

    invoke-direct {v8, v0, v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 91
    .local v8, "jcSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v32, 0x6

    aput-object v8, v25, v32

    .line 93
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$IXPPrObserver;)V

    .line 94
    .local v12, "pPrHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;
    new-instance v13, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v32, "pPr"

    move-object/from16 v0, v32

    invoke-direct {v13, v0, v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 96
    .local v13, "pPrSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v32, 0x7

    aput-object v13, v25, v32

    .line 98
    new-instance v20, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$IXRPrObserver;)V

    .line 99
    .local v20, "rPrHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;
    new-instance v21, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v32, "rPr"

    move-object/from16 v0, v21

    move-object/from16 v1, v32

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 101
    .local v21, "rPrSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v32, 0x8

    aput-object v21, v25, v32

    .line 103
    new-instance v14, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$PStyle;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$PStyle;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;)V

    .line 104
    .local v14, "pStyle":Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$PStyle;
    new-instance v15, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;

    const-string/jumbo v32, "pStyle"

    move-object/from16 v0, v32

    invoke-direct {v15, v0, v14}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 106
    .local v15, "pStyleHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;
    new-instance v16, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v32, "pStyle"

    move-object/from16 v0, v16

    move-object/from16 v1, v32

    invoke-direct {v0, v1, v15}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 108
    .local v16, "pStyleSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v32, 0x9

    aput-object v16, v25, v32

    .line 110
    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 111
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;)Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;->numLvl:Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;

    return-object v0
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 127
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;->observer:Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$IXDocLvlConsumer;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;->numLvl:Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;

    invoke-interface {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$IXDocLvlConsumer;->consumeLvl(Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;)V

    .line 130
    return-void
.end method

.method public onPPrFinished(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p1, "paraProperties"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    .param p2, "charProperties"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .param p3, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 223
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;->numLvl:Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->setParaProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V

    .line 224
    return-void
.end method

.method public onRPrFinsihed(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p1, "charProperties"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .param p2, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 229
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;->numLvl:Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->setCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V

    .line 230
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 116
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 117
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;->numLvl:Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;

    .line 119
    const-string/jumbo v1, "ilvl"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 120
    .local v0, "val":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 121
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;->numLvl:Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->setILvl(I)V

    .line 123
    :cond_0
    return-void
.end method
