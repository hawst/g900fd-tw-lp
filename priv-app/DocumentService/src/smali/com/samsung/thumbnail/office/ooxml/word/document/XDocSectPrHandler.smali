.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XDocSectPrHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgBorderHandler$IPgBorParam;
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler$IPgMargin;
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgSzHandler$IPgSize;
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrColsHandler$ICols;
.implements Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XDocHdrFdrHandler$IHdrFdrRef;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler$SectPrType;
    }
.end annotation


# instance fields
.field private sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

.field private section:I


# direct methods
.method public constructor <init>(I)V
    .locals 22
    .param p1, "section"    # I

    .prologue
    .line 38
    const-string/jumbo v21, "sectPr"

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 40
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->section:I

    .line 41
    const/16 v21, 0x8

    move/from16 v0, v21

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    move-object/from16 v17, v0

    .line 43
    .local v17, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler$SectPrType;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler$SectPrType;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;)V

    .line 44
    .local v18, "type":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler$SectPrType;
    new-instance v19, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;

    const-string/jumbo v21, "type"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 46
    .local v19, "typeHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;
    new-instance v20, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v21, "type"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 48
    .local v20, "typeSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v21, 0x0

    aput-object v20, v17, v21

    .line 50
    new-instance v15, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgSzHandler;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgSzHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgSzHandler$IPgSize;)V

    .line 51
    .local v15, "pgSzHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgSzHandler;
    new-instance v16, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v21, "pgSz"

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v15}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 53
    .local v16, "pgSzSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v21, 0x1

    aput-object v16, v17, v21

    .line 55
    new-instance v13, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler$IPgMargin;)V

    .line 56
    .local v13, "pgMarHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler;
    new-instance v14, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v21, "pgMar"

    move-object/from16 v0, v21

    invoke-direct {v14, v0, v13}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 58
    .local v14, "pgMarSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v21, 0x2

    aput-object v14, v17, v21

    .line 60
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrColsHandler;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrColsHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrColsHandler$ICols;)V

    .line 61
    .local v3, "colsHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrColsHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v21, "cols"

    move-object/from16 v0, v21

    invoke-direct {v4, v0, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 63
    .local v4, "colsSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v21, 0x3

    aput-object v4, v17, v21

    .line 65
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XHeaderRefHandler;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XHeaderRefHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XDocHdrFdrHandler$IHdrFdrRef;)V

    .line 66
    .local v9, "hdrRefHandler":Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XHeaderRefHandler;
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v21, "headerReference"

    move-object/from16 v0, v21

    invoke-direct {v10, v0, v9}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 68
    .local v10, "hdrRefSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v21, 0x4

    aput-object v10, v17, v21

    .line 70
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XFooterRefHandler;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XFooterRefHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XDocHdrFdrHandler$IHdrFdrRef;)V

    .line 71
    .local v7, "fdrRefHandler":Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XFooterRefHandler;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v21, "footerReference"

    move-object/from16 v0, v21

    invoke-direct {v8, v0, v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 73
    .local v8, "fdrRefSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v21, 0x5

    aput-object v8, v17, v21

    .line 75
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgBorderHandler;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v11, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgBorderHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgBorderHandler$IPgBorParam;Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;)V

    .line 76
    .local v11, "pgBorHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgBorderHandler;
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v21, "pgBorders"

    move-object/from16 v0, v21

    invoke-direct {v12, v0, v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 78
    .local v12, "pgBorSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v21, 0x6

    aput-object v12, v17, v21

    .line 80
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocGridHandler;

    invoke-direct {v5}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocGridHandler;-><init>()V

    .line 81
    .local v5, "docGridHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocGridHandler;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v21, "docGrid"

    move-object/from16 v0, v21

    invoke-direct {v6, v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 83
    .local v6, "docGridSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v21, 0x7

    aput-object v6, v17, v21

    .line 85
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 86
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    return-object v0
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 108
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 109
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-interface {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->addSectPr(Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;)V

    .line 110
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-virtual {p1, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->setSectPr(Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;)V

    .line 111
    return-void
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 99
    const-string/jumbo v0, "w:titlePg"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->setTitlePagePresence(Z)V

    .line 102
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 104
    return-void
.end method

.method public setBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V
    .locals 1
    .param p1, "borderType"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    .param p2, "border"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .prologue
    .line 202
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->setBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    .line 203
    return-void
.end method

.method public setBotom(I)V
    .locals 1
    .param p1, "bottom"    # I

    .prologue
    .line 156
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->setBottom(I)V

    .line 157
    return-void
.end method

.method public setColumnCount(I)V
    .locals 1
    .param p1, "num"    # I

    .prologue
    .line 141
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->setColsCount(I)V

    .line 142
    return-void
.end method

.method public setColumnSpace(I)V
    .locals 1
    .param p1, "colSpace"    # I

    .prologue
    .line 146
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->setColsSpace(I)V

    .line 147
    return-void
.end method

.method public setDisplay(Ljava/lang/String;)V
    .locals 1
    .param p1, "display"    # Ljava/lang/String;

    .prologue
    .line 190
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->setPgBorDisplay(Ljava/lang/String;)V

    .line 192
    :cond_0
    return-void
.end method

.method public setFooter(F)V
    .locals 1
    .param p1, "footer"    # F

    .prologue
    .line 176
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->setFooter(F)V

    .line 177
    return-void
.end method

.method public setHdrFdrRef(ZLcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;Ljava/lang/String;)V
    .locals 1
    .param p1, "header"    # Z
    .param p2, "type"    # Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;
    .param p3, "rID"    # Ljava/lang/String;

    .prologue
    .line 181
    if-eqz p1, :cond_0

    .line 182
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-virtual {v0, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->addHeaderRef(Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;Ljava/lang/String;)V

    .line 186
    :goto_0
    return-void

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-virtual {v0, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->addFooterRef(Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setHeader(F)V
    .locals 1
    .param p1, "header"    # F

    .prologue
    .line 171
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->setHeader(F)V

    .line 172
    return-void
.end method

.method public setLandscape(Z)V
    .locals 1
    .param p1, "val"    # Z

    .prologue
    .line 126
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->setLandscape(Z)V

    .line 127
    return-void
.end method

.method public setLeft(F)V
    .locals 1
    .param p1, "left"    # F

    .prologue
    .line 161
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->setLeft(F)V

    .line 162
    return-void
.end method

.method public setOffsetVal(Ljava/lang/String;)V
    .locals 1
    .param p1, "offsetVal"    # Ljava/lang/String;

    .prologue
    .line 196
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->setPgBorOffset(Ljava/lang/String;)V

    .line 198
    :cond_0
    return-void
.end method

.method public setPgHeight(F)V
    .locals 1
    .param p1, "height"    # F

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->setPgHeight(F)V

    .line 132
    return-void
.end method

.method public setPgWidth(F)V
    .locals 1
    .param p1, "width"    # F

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->setPgWidth(F)V

    .line 137
    return-void
.end method

.method public setRight(F)V
    .locals 1
    .param p1, "right"    # F

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->setRight(F)V

    .line 167
    return-void
.end method

.method public setTop(I)V
    .locals 1
    .param p1, "top"    # I

    .prologue
    .line 151
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->setTop(I)V

    .line 152
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 91
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 92
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    .line 93
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    iget v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->section:I

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->setSecType(I)V

    .line 94
    return-void
.end method
