.class public Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelParser;
.super Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;
.source "DiagramDataModelParser.java"


# instance fields
.field dataModel:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;)V
    .locals 0
    .param p1, "dataModel"    # Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;-><init>(Ljava/lang/Object;)V

    .line 18
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelParser;->dataModel:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

    .line 19
    return-void
.end method


# virtual methods
.method public createRootElementHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    .locals 3

    .prologue
    .line 23
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelHandler;

    const/16 v1, 0xca

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelParser;->dataModel:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;)V

    return-object v0
.end method

.method public getMainNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .locals 1

    .prologue
    .line 29
    const/16 v0, 0xca

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v0

    return-object v0
.end method
