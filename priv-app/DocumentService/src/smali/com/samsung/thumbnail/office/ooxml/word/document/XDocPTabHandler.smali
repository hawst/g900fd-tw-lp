.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPTabHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XDocPTabHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPTabHandler$IPTabContObserver;
    }
.end annotation


# instance fields
.field mPTabContentObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPTabHandler$IPTabContObserver;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPTabHandler$IPTabContObserver;)V
    .locals 1
    .param p1, "pTabContObserver"    # Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPTabHandler$IPTabContObserver;

    .prologue
    .line 15
    const-string/jumbo v0, "ptab"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 16
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPTabHandler;->mPTabContentObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPTabHandler$IPTabContObserver;

    .line 17
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 22
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    const-string/jumbo v1, "alignment"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPTabHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;-><init>(Ljava/lang/String;)V

    .line 24
    .local v0, "alignment":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    if-eqz v0, :cond_0

    .line 25
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPTabHandler;->mPTabContentObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPTabHandler$IPTabContObserver;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPTabHandler$IPTabContObserver;->setPTabContent(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 27
    :cond_0
    return-void
.end method
