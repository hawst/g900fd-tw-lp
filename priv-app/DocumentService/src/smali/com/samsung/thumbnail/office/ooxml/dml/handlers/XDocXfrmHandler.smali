.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XDocXfrmHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XDocXfrmHandler.java"


# instance fields
.field private xfrm:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IXfrmHandler;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IXfrmHandler;)V
    .locals 2
    .param p1, "xfrm"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IXfrmHandler;

    .prologue
    .line 22
    const/16 v0, 0x1f

    const-string/jumbo v1, "xfrm"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 23
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XDocXfrmHandler;->xfrm:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IXfrmHandler;

    .line 24
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 29
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 31
    const-string/jumbo v2, "rot"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XDocXfrmHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 32
    .local v0, "rot":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 33
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;

    invoke-direct {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;-><init>(Ljava/lang/String;)V

    .line 34
    .local v1, "rotation":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XDocXfrmHandler;->xfrm:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IXfrmHandler;

    if-eqz v2, :cond_0

    .line 35
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XDocXfrmHandler;->xfrm:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IXfrmHandler;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;->getValue()I

    move-result v3

    invoke-interface {v2, p1, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IXfrmHandler;->setRotation(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;I)V

    .line 38
    .end local v1    # "rotation":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;
    :cond_0
    return-void
.end method
