.class public abstract Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
.source "XExcelStreamParser.java"


# static fields
.field protected static excelNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 19
    const/4 v5, 0x5

    new-array v5, v5, [Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    sput-object v5, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;->excelNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    .line 20
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v5, "http://schemas.openxmlformats.org/spreadsheetml/2006/main"

    const/16 v6, 0xc8

    invoke-direct {v4, v5, v6}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 23
    .local v4, "wordMLMain":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;->excelNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    .line 24
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v5, "http://schemas.openxmlformats.org/officeDocument/2006/relationships"

    const/16 v6, 0xa

    invoke-direct {v3, v5, v6}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 26
    .local v3, "relationship":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;->excelNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const/4 v6, 0x1

    aput-object v3, v5, v6

    .line 28
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v5, "http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing"

    const/16 v6, 0xc9

    invoke-direct {v1, v5, v6}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 31
    .local v1, "drawingML":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;->excelNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const/4 v6, 0x2

    aput-object v1, v5, v6

    .line 33
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v5, "http://schemas.openxmlformats.org/drawingml/2006/main"

    const/16 v6, 0x1f

    invoke-direct {v2, v5, v6}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 35
    .local v2, "drawingimgML":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;->excelNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const/4 v6, 0x3

    aput-object v2, v5, v6

    .line 37
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v5, "http://schemas.openxmlformats.org/drawingml/2006/chart"

    const/16 v6, 0x23

    invoke-direct {v0, v5, v6}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 39
    .local v0, "chartML":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;->excelNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const/4 v6, 0x4

    aput-object v0, v5, v6

    .line 41
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)V
    .locals 1
    .param p1, "chartobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;-><init>(Ljava/lang/Object;)V

    .line 45
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;->excelNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;->setPredefinedNameSpaces([Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)V
    .locals 1
    .param p1, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;-><init>(Ljava/lang/Object;)V

    .line 56
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;->excelNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;->setPredefinedNameSpaces([Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;)V
    .locals 1
    .param p1, "sharedstringobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;-><init>(Ljava/lang/Object;)V

    .line 50
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;->excelNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;->setPredefinedNameSpaces([Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;)V
    .locals 1
    .param p1, "styleobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;-><init>(Ljava/lang/Object;)V

    .line 77
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;->excelNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;->setPredefinedNameSpaces([Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 78
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;)V
    .locals 1
    .param p1, "workbookobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;-><init>(Ljava/lang/Object;)V

    .line 72
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;->excelNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;->setPredefinedNameSpaces([Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 73
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;)V
    .locals 1
    .param p1, "worksheetobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;-><init>(Ljava/lang/Object;)V

    .line 82
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;->excelNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;->setPredefinedNameSpaces([Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 83
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/excel/table/XWPFExcelTableParser;)V
    .locals 1
    .param p1, "tableobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/table/XWPFExcelTableParser;

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;-><init>(Ljava/lang/Object;)V

    .line 62
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;->excelNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;->setPredefinedNameSpaces([Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 63
    return-void
.end method
