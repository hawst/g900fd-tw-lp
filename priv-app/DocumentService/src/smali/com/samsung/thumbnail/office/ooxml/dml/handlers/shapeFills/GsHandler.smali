.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler;
.super Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;
.source "GsHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;,
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$ColorConsumer;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V
    .locals 6
    .param p1, "consumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;
    .param p2, "shInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "theme"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .prologue
    .line 16
    const/16 v1, 0x1f

    const-string/jumbo v2, "gs"

    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$ColorConsumer;

    invoke-direct {v3, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$ColorConsumer;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;)V

    move-object v0, p0

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 18
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 23
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 24
    const-string/jumbo v4, "pos"

    invoke-virtual {p0, p3, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    .line 25
    .local v2, "pos":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 26
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler;->colorConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$ColorConsumer;

    .line 27
    .local v0, "colorConsumer":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$ColorConsumer;
    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    .line 28
    .local v1, "p":F
    const v4, 0x47c35000    # 100000.0f

    div-float v3, v1, v4

    .line 29
    .local v3, "position":F
    iput v3, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$ColorConsumer;->pos:F

    .line 31
    .end local v0    # "colorConsumer":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$ColorConsumer;
    .end local v1    # "p":F
    .end local v3    # "position":F
    :cond_0
    return-void
.end method
