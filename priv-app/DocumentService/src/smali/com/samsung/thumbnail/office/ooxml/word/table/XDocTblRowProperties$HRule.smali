.class public final enum Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;
.super Ljava/lang/Enum;
.source "XDocTblRowProperties.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HRule"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

.field public static final enum ATLEAST:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

.field public static final enum AUTO:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

.field public static final enum EXACT:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 36
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

    const-string/jumbo v1, "AUTO"

    const-string/jumbo v2, "auto"

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;->AUTO:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

    const-string/jumbo v1, "EXACT"

    const-string/jumbo v2, "exact"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;->EXACT:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

    const-string/jumbo v1, "ATLEAST"

    const-string/jumbo v2, "atLeast"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;->ATLEAST:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

    .line 35
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;->AUTO:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;->EXACT:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;->ATLEAST:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;->value:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public static convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;->values()[Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 50
    .local v2, "inst":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 54
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;
    :goto_1
    return-object v2

    .line 49
    .restart local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 54
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 35
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

    return-object v0
.end method


# virtual methods
.method public xmlValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;->value:Ljava/lang/String;

    return-object v0
.end method
