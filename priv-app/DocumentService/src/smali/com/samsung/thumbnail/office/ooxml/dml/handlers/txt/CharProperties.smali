.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;
.super Ljava/lang/Object;
.source "CharProperties.java"


# instance fields
.field public baseline:I

.field public bold:Z

.field public fontFamily:Ljava/lang/String;

.field public fontSize:I

.field public fontTypeface:Ljava/lang/String;

.field public italic:Z

.field public strike:I

.field public underline:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;)V
    .locals 0
    .param p1, "charProp"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    if-nez p1, :cond_0

    .line 22
    :goto_0
    return-void

    .line 21
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->assign(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;)V

    goto :goto_0
.end method


# virtual methods
.method public assign(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;)V
    .locals 1
    .param p1, "charProp"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;

    .prologue
    .line 109
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->bold:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->bold:Z

    .line 110
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->italic:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->italic:Z

    .line 111
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->underline:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->underline:Z

    .line 112
    return-void
.end method

.method public getBaseLine()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->baseline:I

    return v0
.end method

.method public getBold()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->bold:Z

    return v0
.end method

.method public getFontFamily()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->fontFamily:Ljava/lang/String;

    return-object v0
.end method

.method public getFontFontTypeFace()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->fontTypeface:Ljava/lang/String;

    return-object v0
.end method

.method public getFontSize()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->fontSize:I

    return v0
.end method

.method public getItalic()Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->italic:Z

    return v0
.end method

.method public getStrike()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->strike:I

    return v0
.end method

.method public getUnderLine()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->underline:Z

    return v0
.end method

.method public mergeCharProp(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;)V
    .locals 2
    .param p1, "charProp"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;

    .prologue
    .line 93
    if-nez p1, :cond_0

    .line 106
    :goto_0
    return-void

    .line 96
    :cond_0
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;

    invoke-direct {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;)V

    .line 98
    .local v0, "localCharProp":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;
    iget-boolean v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->bold:Z

    if-eqz v1, :cond_1

    .line 99
    iget-boolean v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->bold:Z

    iput-boolean v1, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->bold:Z

    .line 100
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->italic:Z

    if-eqz v1, :cond_2

    .line 101
    iget-boolean v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->italic:Z

    iput-boolean v1, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->italic:Z

    .line 102
    :cond_2
    iget-boolean v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->underline:Z

    if-eqz v1, :cond_3

    .line 103
    iget-boolean v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->underline:Z

    iput-boolean v1, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->underline:Z

    .line 105
    :cond_3
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->assign(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;)V

    goto :goto_0
.end method

.method public setBaseLine(I)V
    .locals 0
    .param p1, "baseline"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->baseline:I

    .line 30
    return-void
.end method

.method public setBold(Z)V
    .locals 0
    .param p1, "bold"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->bold:Z

    .line 46
    return-void
.end method

.method public setFontFamily(Ljava/lang/String;)V
    .locals 0
    .param p1, "fontFamily"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->fontFamily:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public setFontFontTypeFace(Ljava/lang/String;)V
    .locals 0
    .param p1, "fontTypeface"    # Ljava/lang/String;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->fontTypeface:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public setFontSize(I)V
    .locals 0
    .param p1, "fontSize"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->fontSize:I

    .line 34
    return-void
.end method

.method public setItalic(Z)V
    .locals 0
    .param p1, "italic"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->italic:Z

    .line 50
    return-void
.end method

.method public setStrike(I)V
    .locals 0
    .param p1, "strike"    # I

    .prologue
    .line 37
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->strike:I

    .line 38
    return-void
.end method

.method public setUnderLine(Z)V
    .locals 0
    .param p1, "underline"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/CharProperties;->underline:Z

    .line 54
    return-void
.end method
