.class Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler$XTblIndentation;
.super Ljava/lang/Object;
.source "XDocTblPrExHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler$IXTblWOberver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "XTblIndentation"
.end annotation


# instance fields
.field private tblCellMargin:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;

.field final synthetic this$0:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;


# direct methods
.method constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler$XTblIndentation;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setElement(Ljava/lang/String;)V
    .locals 2
    .param p1, "element"    # Ljava/lang/String;

    .prologue
    .line 121
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler$XTblIndentation;->tblCellMargin:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;

    .line 122
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler$XTblIndentation;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;->tableProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler$XTblIndentation;->tblCellMargin:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->setTblInd(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;)V

    .line 123
    return-void
.end method

.method public setTblWidth(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;I)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "width"    # I

    .prologue
    .line 112
    return-void
.end method

.method public setWidthType(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "widthType"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    .prologue
    .line 117
    return-void
.end method
