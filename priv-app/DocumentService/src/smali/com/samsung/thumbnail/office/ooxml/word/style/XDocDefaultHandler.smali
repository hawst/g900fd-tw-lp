.class public Lcom/samsung/thumbnail/office/ooxml/word/style/XDocDefaultHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XDocDefaultHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$IXPPrObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$IXRPrObserver;


# instance fields
.field protected styles:Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;)V
    .locals 8
    .param p1, "styles"    # Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;

    .prologue
    .line 25
    const-string/jumbo v7, "docDefaults"

    invoke-direct {p0, v7}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 27
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocDefaultHandler;->styles:Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;

    .line 28
    const/4 v7, 0x2

    new-array v6, v7, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 30
    .local v6, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;

    invoke-direct {v4, p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$IXRPrObserver;)V

    .line 31
    .local v4, "rPrHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/OOXMLSingleElementHandler;

    const-string/jumbo v7, "rPrDefault"

    invoke-direct {v3, v7, v4}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSingleElementHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 33
    .local v3, "rPrDefHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLSingleElementHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v7, "rPrDefault"

    invoke-direct {v5, v7, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 35
    .local v5, "rPrSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v7, 0x0

    aput-object v5, v6, v7

    .line 36
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;

    invoke-direct {v1, p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$IXPPrObserver;)V

    .line 37
    .local v1, "pPrHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLSingleElementHandler;

    const-string/jumbo v7, "pPrDefault"

    invoke-direct {v0, v7, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSingleElementHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 39
    .local v0, "pPrDefHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLSingleElementHandler;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v7, "pPrDefault"

    invoke-direct {v2, v7, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 41
    .local v2, "pPrSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v7, 0x1

    aput-object v2, v6, v7

    .line 43
    iput-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocDefaultHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 49
    return-void
.end method


# virtual methods
.method public onPPrFinished(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p1, "paraProperties"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    .param p2, "charProperties"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .param p3, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocDefaultHandler;->styles:Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;

    invoke-interface {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;->setDefaultParaProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V

    .line 55
    return-void
.end method

.method public onRPrFinsihed(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p1, "charProperties"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .param p2, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocDefaultHandler;->styles:Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;

    invoke-interface {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;->setDefaultCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V

    .line 61
    return-void
.end method
