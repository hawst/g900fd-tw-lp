.class public Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;
.super Ljava/lang/Object;
.source "XSSFExcelStyle.java"


# instance fields
.field private applynumberformatval:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private checkstyle:Ljava/lang/String;

.field private excelVar:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

.field private excellXfid:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private excellborderId:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private excellbottomborderrgbcolor:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private excellbottomborderstyle:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private excellbottomborderthemecolor:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private excellfillid:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private excellfontid:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private excellleftborderrgbcolor:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private excellleftborderstyle:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private excellleftborderthemecolor:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private excellrgtborderrgbcolor:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private excellrgtborderstyle:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private excellrgtborderthemecolor:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private excellstylename:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private excelltopborderrgbcolor:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private excelltopborderstyle:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private excelltopborderthemecolor:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private excellwraptextval:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private extextbold:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private extextcolorrgb:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private extextcolortheme:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private extextcolortint:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private extextfontname:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private extextitalic:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private extextsize:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private extextstrike:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private extextunderline:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private fills:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/excel/IFill;",
            ">;"
        }
    .end annotation
.end field

.field private fontidcnt:I

.field private horizontalalign:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private numFmtId:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private numformatcode:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private shrinkToFit:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private textAlignmentIndent:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private textverticalalign:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private verticalalign:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;)V
    .locals 1
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "mContext"    # Landroid/content/Context;
    .param p3, "excelVar"    # Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextbold:Ljava/util/ArrayList;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextunderline:Ljava/util/ArrayList;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextitalic:Ljava/util/ArrayList;

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextsize:Ljava/util/ArrayList;

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextcolorrgb:Ljava/util/ArrayList;

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextcolortheme:Ljava/util/ArrayList;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextcolortint:Ljava/util/ArrayList;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextfontname:Ljava/util/ArrayList;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellfontid:Ljava/util/ArrayList;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->numFmtId:Ljava/util/ArrayList;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellborderId:Ljava/util/ArrayList;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellfillid:Ljava/util/ArrayList;

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->horizontalalign:Ljava/util/ArrayList;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->verticalalign:Ljava/util/ArrayList;

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellstylename:Ljava/util/Map;

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->numformatcode:Ljava/util/Map;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellXfid:Ljava/util/ArrayList;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->applynumberformatval:Ljava/util/ArrayList;

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellwraptextval:Ljava/util/ArrayList;

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellleftborderstyle:Ljava/util/ArrayList;

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellleftborderrgbcolor:Ljava/util/ArrayList;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellleftborderthemecolor:Ljava/util/ArrayList;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellrgtborderstyle:Ljava/util/ArrayList;

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellrgtborderrgbcolor:Ljava/util/ArrayList;

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellrgtborderthemecolor:Ljava/util/ArrayList;

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excelltopborderstyle:Ljava/util/ArrayList;

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excelltopborderrgbcolor:Ljava/util/ArrayList;

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excelltopborderthemecolor:Ljava/util/ArrayList;

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellbottomborderstyle:Ljava/util/ArrayList;

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellbottomborderrgbcolor:Ljava/util/ArrayList;

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellbottomborderthemecolor:Ljava/util/ArrayList;

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->checkstyle:Ljava/lang/String;

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextstrike:Ljava/util/ArrayList;

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->textverticalalign:Ljava/util/ArrayList;

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->shrinkToFit:Ljava/util/ArrayList;

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->textAlignmentIndent:Ljava/util/ArrayList;

    .line 109
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excelVar:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    .line 111
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->onStyleRead(Ljava/io/InputStream;)V

    .line 112
    return-void
.end method

.method private parseExcel(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "is"    # Ljava/io/InputStream;

    .prologue
    .line 120
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleSheetParser;

    invoke-direct {v0, p0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleSheetParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;)V

    .line 121
    .local v0, "excelParser":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleSheetParser;
    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleSheetParser;->parse(Ljava/io/InputStream;)V

    .line 122
    return-void
.end method


# virtual methods
.method public addAlingmentHorizontal(Ljava/lang/String;I)V
    .locals 1
    .param p1, "excellalignmentval"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 282
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->horizontalalign:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 283
    return-void
.end method

.method public addAlingmentVertical(Ljava/lang/String;I)V
    .locals 1
    .param p1, "excellalignmentval"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 298
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->verticalalign:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 299
    return-void
.end method

.method public addApplynumberformatval(Ljava/lang/String;I)V
    .locals 1
    .param p1, "numfrmt"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 274
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->applynumberformatval:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 275
    return-void
.end method

.method public addExFontidcount(I)V
    .locals 0
    .param p1, "fontidcnt"    # I

    .prologue
    .line 331
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->fontidcnt:I

    .line 332
    return-void
.end method

.method public addExStyleCellBottomBorderRGBColor(Ljava/lang/String;I)V
    .locals 1
    .param p1, "excellbordercolor"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 447
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellbottomborderrgbcolor:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 448
    return-void
.end method

.method public addExStyleCellBottomBorderThemeColor(Ljava/lang/String;I)V
    .locals 1
    .param p1, "excellbordercolor"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 456
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellbottomborderthemecolor:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 457
    return-void
.end method

.method public addExStyleCellBottomBorderstyle(Ljava/lang/String;I)V
    .locals 1
    .param p1, "excellborderstyle"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 438
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellbottomborderstyle:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 439
    return-void
.end method

.method public addExStyleCellLeftBorderRGBColor(Ljava/lang/String;I)V
    .locals 1
    .param p1, "excellbordercolor"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 358
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellleftborderrgbcolor:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 359
    return-void
.end method

.method public addExStyleCellLeftBorderThemeColor(Ljava/lang/String;I)V
    .locals 1
    .param p1, "excellbordercolor"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 367
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellleftborderthemecolor:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 368
    return-void
.end method

.method public addExStyleCellLeftBorderstyle(Ljava/lang/String;I)V
    .locals 1
    .param p1, "excellborderstyle"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 349
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellleftborderstyle:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 350
    return-void
.end method

.method public addExStyleCellRightBorderRGBColor(Ljava/lang/String;I)V
    .locals 1
    .param p1, "excellbordercolor"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 388
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellrgtborderrgbcolor:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 389
    return-void
.end method

.method public addExStyleCellRightBorderThemeColor(Ljava/lang/String;I)V
    .locals 1
    .param p1, "excellbordercolor"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 397
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellrgtborderthemecolor:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 398
    return-void
.end method

.method public addExStyleCellRightBorderstyle(Ljava/lang/String;I)V
    .locals 1
    .param p1, "excellborderstyle"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 379
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellrgtborderstyle:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 380
    return-void
.end method

.method public addExStyleCellStyleName(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "clrSchma"    # Ljava/lang/String;
    .param p2, "val"    # Ljava/lang/String;

    .prologue
    .line 310
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellstylename:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    return-void
.end method

.method public addExStyleCellTopBorderRGBColor(Ljava/lang/String;I)V
    .locals 1
    .param p1, "excellbordercolor"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 417
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excelltopborderrgbcolor:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 418
    return-void
.end method

.method public addExStyleCellTopBorderThemeColor(Ljava/lang/String;I)V
    .locals 1
    .param p1, "excellbordercolor"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 426
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excelltopborderthemecolor:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 427
    return-void
.end method

.method public addExStyleCellTopBorderstyle(Ljava/lang/String;I)V
    .locals 1
    .param p1, "excellborderstyle"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 408
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excelltopborderstyle:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 409
    return-void
.end method

.method public addExStyleCellXFBorderId(Ljava/lang/String;I)V
    .locals 1
    .param p1, "excellborderId"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 339
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellborderId:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 340
    return-void
.end method

.method public addExStyleCellXFFillid(Ljava/lang/String;I)V
    .locals 1
    .param p1, "excellfillid"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 258
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellfillid:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 259
    return-void
.end method

.method public addExStyleCellXFFontid(Ljava/lang/String;I)V
    .locals 1
    .param p1, "excellfontid"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 250
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellfontid:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 251
    return-void
.end method

.method public addExStyleCellXFNumFmtId(Ljava/lang/String;I)V
    .locals 1
    .param p1, "numFmtId"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 242
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->numFmtId:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 243
    return-void
.end method

.method public addExStyleCellXFWraptext(Ljava/lang/String;I)V
    .locals 1
    .param p1, "excellwraptextval"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 323
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellwraptextval:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 324
    return-void
.end method

.method public addExStyleCellXFid(Ljava/lang/String;I)V
    .locals 1
    .param p1, "excellXfid"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 266
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellXfid:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 267
    return-void
.end method

.method public addExStyleFontBOLD(Ljava/lang/String;I)V
    .locals 1
    .param p1, "bold"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 151
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextbold:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 152
    return-void
.end method

.method public addExStyleTextColorRGB(Ljava/lang/String;I)V
    .locals 1
    .param p1, "extextcolorrgb"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 210
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextcolorrgb:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 211
    return-void
.end method

.method public addExStyleTextColorTheme(Ljava/lang/String;I)V
    .locals 1
    .param p1, "extextcolortheme"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 214
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextcolortheme:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 215
    return-void
.end method

.method public addExStyleTextColorTint(Ljava/lang/String;I)V
    .locals 1
    .param p1, "extextcolortheme"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 222
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextcolortint:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 223
    return-void
.end method

.method public addExStyleTextFontname(Ljava/lang/String;I)V
    .locals 1
    .param p1, "extextfontname"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 234
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextfontname:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 235
    return-void
.end method

.method public addExStyleTextItalic(Ljava/lang/String;I)V
    .locals 1
    .param p1, "extextitalic"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 167
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextitalic:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 168
    return-void
.end method

.method public addExStyleTextSize(Ljava/lang/String;I)V
    .locals 1
    .param p1, "extextsize"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 202
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextsize:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 203
    return-void
.end method

.method public addExStyleTextStrikethrough(Ljava/lang/String;I)V
    .locals 1
    .param p1, "extextstrike"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 194
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextstrike:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 195
    return-void
.end method

.method public addExStyleTextUnderline(Ljava/lang/String;I)V
    .locals 1
    .param p1, "extextunderline"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 159
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextunderline:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 160
    return-void
.end method

.method public addFill(Lcom/samsung/thumbnail/office/ooxml/excel/IFill;)V
    .locals 1
    .param p1, "fill"    # Lcom/samsung/thumbnail/office/ooxml/excel/IFill;

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->fills:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->fills:Ljava/util/ArrayList;

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->fills:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    return-void
.end method

.method public addShrinkToFont(Ljava/lang/String;I)V
    .locals 1
    .param p1, "val"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 286
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->shrinkToFit:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 287
    return-void
.end method

.method public addStyleTextAlignmentIndent(Ljava/lang/String;I)V
    .locals 1
    .param p1, "excellalignmentindent"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 186
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->textAlignmentIndent:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 187
    return-void
.end method

.method public addStyleTextVerticalAlign(Ljava/lang/String;I)V
    .locals 1
    .param p1, "excellalignmentval"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 177
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->textverticalalign:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 178
    return-void
.end method

.method public addstylenumfmtcode(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "clrSchma"    # Ljava/lang/String;
    .param p2, "val"    # Ljava/lang/String;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->numformatcode:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    return-void
.end method

.method public getAlingmentHorizontal()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 278
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->horizontalalign:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getAlingmentVertical()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 294
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->verticalalign:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getApplynumberformatval()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 270
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->applynumberformatval:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExFontidcount()I
    .locals 1

    .prologue
    .line 327
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->fontidcnt:I

    return v0
.end method

.method public getExSharedStringTextBOLD()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextbold:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleCellBottomBorderRGBColor()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 442
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellbottomborderrgbcolor:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleCellBottomBorderThemeColor()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 451
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellbottomborderthemecolor:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleCellBottomBorderstyle()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 433
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellbottomborderstyle:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleCellLeftBorderRGBColor()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 353
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellleftborderrgbcolor:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleCellLeftBorderThemeColor()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 362
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellleftborderthemecolor:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleCellLeftBorderstyle()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 344
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellleftborderstyle:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleCellRightBorderRGBColor()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 383
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellrgtborderrgbcolor:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleCellRightBorderThemeColor()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 392
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellrgtborderthemecolor:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleCellRightBorderstyle()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 374
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellrgtborderstyle:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleCellStyleName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 314
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellstylename:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getExStyleCellTopBorderRGBColor()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 412
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excelltopborderrgbcolor:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleCellTopBorderThemeColor()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 421
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excelltopborderthemecolor:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleCellTopBorderstyle()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 404
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excelltopborderstyle:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleCellXFBorderId()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 335
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellborderId:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleCellXFFillid()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellfillid:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleCellXFFontid()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 246
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellfontid:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleCellXFNumFmtId()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 238
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->numFmtId:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleCellXFWraptext()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 319
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellwraptextval:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleCellXFid()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 262
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excellXfid:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleTextColorRGB()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 206
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextcolorrgb:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleTextColorTheme()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextcolortheme:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleTextColorTint()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextcolortint:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleTextFontname()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 230
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextfontname:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleTextItalic()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextitalic:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleTextSize()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 198
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextsize:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleTextStrikethrough()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 190
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextstrike:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExStyleTextUnderline()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->extextunderline:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->excelVar:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    return-object v0
.end method

.method public getFill(I)Lcom/samsung/thumbnail/office/ooxml/excel/IFill;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 140
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->fills:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->fills:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/IFill;

    .line 143
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFills()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/excel/IFill;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->fills:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getShrinkToFit()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 290
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->shrinkToFit:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getStyle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->checkstyle:Ljava/lang/String;

    return-object v0
.end method

.method public getStyleTextAlignmentIndent()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->textAlignmentIndent:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getStyleTextVerticalAlign()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->textverticalalign:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getstylenumfmtcode(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 306
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->numformatcode:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method protected onStyleRead(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 125
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->parseExcel(Ljava/io/InputStream;)V

    .line 126
    return-void
.end method

.method public setStyle(Ljava/lang/String;)V
    .locals 0
    .param p1, "stylestr"    # Ljava/lang/String;

    .prologue
    .line 462
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->checkstyle:Ljava/lang/String;

    .line 463
    return-void
.end method
