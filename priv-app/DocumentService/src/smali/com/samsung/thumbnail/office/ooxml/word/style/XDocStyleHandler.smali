.class public Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XDocStyleHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$IXPPrObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$IXRPrObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler$ITblStylePrObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler$ITblPropObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$ITblCellPropObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler$StyleLink;,
        Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler$StyleBasedOn;
    }
.end annotation


# instance fields
.field protected style:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

.field protected styles:Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;)V
    .locals 1
    .param p1, "styles"    # Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;

    .prologue
    .line 43
    const-string/jumbo v0, "style"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 44
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;->styles:Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;

    .line 45
    return-void
.end method

.method private init()V
    .locals 21

    .prologue
    .line 48
    const/16 v20, 0x7

    move/from16 v0, v20

    new-array v13, v0, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 50
    .local v13, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler$StyleBasedOn;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler$StyleBasedOn;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;)V

    .line 51
    .local v3, "basedOn":Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler$StyleBasedOn;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;

    const-string/jumbo v20, "basedOn"

    move-object/from16 v0, v20

    invoke-direct {v4, v0, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 53
    .local v4, "basedOnHand":Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v20, "basedOn"

    move-object/from16 v0, v20

    invoke-direct {v5, v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 55
    .local v5, "basedSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v20, 0x0

    aput-object v5, v13, v20

    .line 57
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler$StyleLink;

    invoke-direct {v6}, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler$StyleLink;-><init>()V

    .line 58
    .local v6, "link":Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler$StyleLink;
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;

    const-string/jumbo v20, "link"

    move-object/from16 v0, v20

    invoke-direct {v7, v0, v6}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 60
    .local v7, "linkOnHand":Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v20, "link"

    move-object/from16 v0, v20

    invoke-direct {v8, v0, v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 62
    .local v8, "linkSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v20, 0x1

    aput-object v8, v13, v20

    .line 64
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$IXPPrObserver;)V

    .line 65
    .local v9, "pPrHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v20, "pPr"

    move-object/from16 v0, v20

    invoke-direct {v10, v0, v9}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 67
    .local v10, "pPrSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v20, 0x2

    aput-object v10, v13, v20

    .line 69
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$IXRPrObserver;)V

    .line 70
    .local v11, "rPrHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v20, "rPr"

    move-object/from16 v0, v20

    invoke-direct {v12, v0, v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 72
    .local v12, "rPrSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v20, 0x3

    aput-object v12, v13, v20

    .line 74
    new-instance v14, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler$ITblPropObserver;)V

    .line 75
    .local v14, "tablePr":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;
    new-instance v15, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v20, "tblPr"

    move-object/from16 v0, v20

    invoke-direct {v15, v0, v14}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 77
    .local v15, "tblPropSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v20, 0x4

    aput-object v15, v13, v20

    .line 79
    new-instance v16, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler$ITblStylePrObserver;)V

    .line 80
    .local v16, "tblStylePr":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler;
    new-instance v17, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v20, "tblStylePr"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 82
    .local v17, "tblStylePrSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v20, 0x5

    aput-object v17, v13, v20

    .line 84
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$ITblCellPropObserver;)V

    .line 85
    .local v18, "tblTCPr":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;
    new-instance v19, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v20, "tcPr"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 87
    .local v19, "tblTCPrSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v20, 0x6

    aput-object v19, v13, v20

    .line 89
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 90
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 116
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;->style:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;->styles:Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;->style:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    invoke-interface {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;->addStyle(Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;)V

    .line 119
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;->style:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    .line 121
    :cond_0
    return-void
.end method

.method public onPPrFinished(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p1, "paraProperties"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    .param p2, "charProperties"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .param p3, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;->style:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->setParaProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V

    .line 133
    return-void
.end method

.method public onRPrFinsihed(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p1, "charProperties"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .param p2, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;->style:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->setCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V

    .line 127
    return-void
.end method

.method public setTblCellProp(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "cellProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    .prologue
    .line 171
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;->style:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->setTblCellProp(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;)V

    .line 173
    return-void
.end method

.method public setTblProp(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "tblProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;->style:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->setTblProp(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;)V

    .line 160
    return-void
.end method

.method public setTblStylePr(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "type"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;
    .param p3, "style"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;->style:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    invoke-virtual {v0, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->setTableStyleProp(Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;)V

    .line 166
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 6
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 95
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 96
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;->init()V

    .line 98
    const-string/jumbo v4, "type"

    invoke-virtual {p0, p3, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v3

    .line 99
    .local v3, "type":Ljava/lang/String;
    const-string/jumbo v4, "styleId"

    invoke-virtual {p0, p3, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    .line 101
    .local v2, "styleId":Ljava/lang/String;
    if-eqz v3, :cond_0

    if-nez v2, :cond_1

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    invoke-direct {v4, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;->style:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    .line 107
    const-string/jumbo v4, "default"

    invoke-virtual {p0, p3, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 108
    .local v1, "def":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 109
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;-><init>(Ljava/lang/String;)V

    .line 110
    .local v0, "bool":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;->style:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;->getValue()Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->setDefault(Z)V

    goto :goto_0
.end method
