.class public Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;
.super Ljava/lang/Object;
.source "HSLColorUtil.java"


# instance fields
.field private alphaVal:F

.field private hslVal:[F

.field private rgbColor:Lorg/apache/poi/java/awt/Color;


# direct methods
.method public constructor <init>(FFF)V
    .locals 1
    .param p1, "hue"    # F
    .param p2, "sat"    # F
    .param p3, "lum"    # F

    .prologue
    .line 52
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;-><init>(FFFF)V

    .line 53
    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 2
    .param p1, "hue"    # F
    .param p2, "sat"    # F
    .param p3, "lum"    # F
    .param p4, "alpha"    # F

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    const/4 v0, 0x3

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput p1, v0, v1

    const/4 v1, 0x1

    aput p2, v0, v1

    const/4 v1, 0x2

    aput p3, v0, v1

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    .line 69
    iput p4, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->alphaVal:F

    .line 70
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    invoke-static {v0, p4}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->fromHSLToRGB([FF)Lorg/apache/poi/java/awt/Color;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->rgbColor:Lorg/apache/poi/java/awt/Color;

    .line 71
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/java/awt/Color;)V
    .locals 2
    .param p1, "rgbClr"    # Lorg/apache/poi/java/awt/Color;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->rgbColor:Lorg/apache/poi/java/awt/Color;

    .line 36
    invoke-static {p1}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->fromRGBToHSL(Lorg/apache/poi/java/awt/Color;)[F

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    .line 37
    invoke-virtual {p1}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x437f0000    # 255.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->alphaVal:F

    .line 38
    return-void
.end method

.method public constructor <init>([F)V
    .locals 1
    .param p1, "hsl"    # [F

    .prologue
    .line 81
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, p1, v0}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;-><init>([FF)V

    .line 82
    return-void
.end method

.method public constructor <init>([FF)V
    .locals 1
    .param p1, "hsl"    # [F
    .param p2, "alpha"    # F

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    .line 95
    iput p2, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->alphaVal:F

    .line 96
    invoke-static {p1, p2}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->fromHSLToRGB([FF)Lorg/apache/poi/java/awt/Color;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->rgbColor:Lorg/apache/poi/java/awt/Color;

    .line 97
    return-void
.end method

.method private static convertHueToRGB(FFF)F
    .locals 4
    .param p0, "a"    # F
    .param p1, "b"    # F
    .param p2, "hue"    # F

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v2, 0x40c00000    # 6.0f

    const/high16 v1, 0x3f800000    # 1.0f

    .line 394
    const/4 v0, 0x0

    cmpg-float v0, p2, v0

    if-gez v0, :cond_0

    .line 395
    add-float/2addr p2, v1

    .line 397
    :cond_0
    cmpl-float v0, p2, v1

    if-lez v0, :cond_1

    .line 398
    sub-float/2addr p2, v1

    .line 400
    :cond_1
    mul-float v0, v2, p2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_3

    .line 401
    sub-float v0, p1, p0

    mul-float/2addr v0, v2

    mul-float/2addr v0, p2

    add-float p1, p0, v0

    .line 412
    .end local p1    # "b":F
    :cond_2
    :goto_0
    return p1

    .line 404
    .restart local p1    # "b":F
    :cond_3
    mul-float v0, v3, p2

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_2

    .line 408
    const/high16 v0, 0x40400000    # 3.0f

    mul-float/2addr v0, p2

    cmpg-float v0, v0, v3

    if-gez v0, :cond_4

    .line 409
    sub-float v0, p1, p0

    mul-float/2addr v0, v2

    const v1, 0x3f2aaaab

    sub-float/2addr v1, p2

    mul-float/2addr v0, v1

    add-float p1, p0, v0

    goto :goto_0

    :cond_4
    move p1, p0

    .line 412
    goto :goto_0
.end method

.method public static fromHSLToRGB(FFF)Lorg/apache/poi/java/awt/Color;
    .locals 1
    .param p0, "hue"    # F
    .param p1, "sat"    # F
    .param p2, "lum"    # F

    .prologue
    .line 333
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p0, p1, p2, v0}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->fromHSLToRGB(FFFF)Lorg/apache/poi/java/awt/Color;

    move-result-object v0

    return-object v0
.end method

.method public static fromHSLToRGB(FFFF)Lorg/apache/poi/java/awt/Color;
    .locals 10
    .param p0, "hue"    # F
    .param p1, "sat"    # F
    .param p2, "lum"    # F
    .param p3, "alpha"    # F

    .prologue
    .line 351
    const/4 v6, 0x0

    cmpg-float v6, p1, v6

    if-ltz v6, :cond_0

    const/high16 v6, 0x42c80000    # 100.0f

    cmpl-float v6, p1, v6

    if-lez v6, :cond_1

    .line 352
    :cond_0
    const-string/jumbo v2, "Color parameter outside of expected range - Saturation"

    .line 353
    .local v2, "message":Ljava/lang/String;
    new-instance v6, Ljava/lang/IllegalArgumentException;

    invoke-direct {v6, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 356
    .end local v2    # "message":Ljava/lang/String;
    :cond_1
    const/4 v6, 0x0

    cmpg-float v6, p2, v6

    if-ltz v6, :cond_2

    const/high16 v6, 0x42c80000    # 100.0f

    cmpl-float v6, p2, v6

    if-lez v6, :cond_3

    .line 357
    :cond_2
    const-string/jumbo v2, "Color parameter outside of expected range - Luminance"

    .line 358
    .restart local v2    # "message":Ljava/lang/String;
    new-instance v6, Ljava/lang/IllegalArgumentException;

    invoke-direct {v6, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 361
    .end local v2    # "message":Ljava/lang/String;
    :cond_3
    const/4 v6, 0x0

    cmpg-float v6, p3, v6

    if-ltz v6, :cond_4

    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v6, p3, v6

    if-lez v6, :cond_5

    .line 362
    :cond_4
    const-string/jumbo v2, "Color parameter outside of expected range - Alpha"

    .line 363
    .restart local v2    # "message":Ljava/lang/String;
    new-instance v6, Ljava/lang/IllegalArgumentException;

    invoke-direct {v6, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 368
    .end local v2    # "message":Ljava/lang/String;
    :cond_5
    const/high16 v6, 0x43b40000    # 360.0f

    rem-float/2addr p0, v6

    .line 369
    const/high16 v6, 0x43b40000    # 360.0f

    div-float/2addr p0, v6

    .line 370
    const/high16 v6, 0x42c80000    # 100.0f

    div-float/2addr p1, v6

    .line 371
    const/high16 v6, 0x42c80000    # 100.0f

    div-float/2addr p2, v6

    .line 373
    const/4 v4, 0x0

    .line 375
    .local v4, "q":F
    float-to-double v6, p2

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    cmpg-double v6, v6, v8

    if-gez v6, :cond_6

    .line 376
    const/high16 v6, 0x3f800000    # 1.0f

    add-float/2addr v6, p1

    mul-float v4, p2, v6

    .line 380
    :goto_0
    const/high16 v6, 0x40000000    # 2.0f

    mul-float/2addr v6, p2

    sub-float v3, v6, v4

    .line 382
    .local v3, "p":F
    const/4 v6, 0x0

    const v7, 0x3eaaaaab

    add-float/2addr v7, p0

    invoke-static {v3, v4, v7}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->convertHueToRGB(FFF)F

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    move-result v5

    .line 383
    .local v5, "red":F
    const/4 v6, 0x0

    invoke-static {v3, v4, p0}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->convertHueToRGB(FFF)F

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 384
    .local v1, "green":F
    const/4 v6, 0x0

    const v7, 0x3eaaaaab

    sub-float v7, p0, v7

    invoke-static {v3, v4, v7}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->convertHueToRGB(FFF)F

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 386
    .local v0, "blue":F
    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    .line 387
    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static {v1, v6}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 388
    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static {v0, v6}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 390
    new-instance v6, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v6, v5, v1, v0, p3}, Lorg/apache/poi/java/awt/Color;-><init>(FFFF)V

    return-object v6

    .line 378
    .end local v0    # "blue":F
    .end local v1    # "green":F
    .end local v3    # "p":F
    .end local v5    # "red":F
    :cond_6
    add-float v6, p2, p1

    mul-float v7, p1, p2

    sub-float v4, v6, v7

    goto :goto_0
.end method

.method public static fromHSLToRGB([F)Lorg/apache/poi/java/awt/Color;
    .locals 1
    .param p0, "hslVal"    # [F

    .prologue
    .line 301
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->fromHSLToRGB([FF)Lorg/apache/poi/java/awt/Color;

    move-result-object v0

    return-object v0
.end method

.method public static fromHSLToRGB([FF)Lorg/apache/poi/java/awt/Color;
    .locals 3
    .param p0, "hslVal"    # [F
    .param p1, "alpha"    # F

    .prologue
    .line 317
    const/4 v0, 0x0

    aget v0, p0, v0

    const/4 v1, 0x1

    aget v1, p0, v1

    const/4 v2, 0x2

    aget v2, p0, v2

    invoke-static {v0, v1, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->fromHSLToRGB(FFFF)Lorg/apache/poi/java/awt/Color;

    move-result-object v0

    return-object v0
.end method

.method public static fromRGBToHSL(Lorg/apache/poi/java/awt/Color;)[F
    .locals 15
    .param p0, "clr"    # Lorg/apache/poi/java/awt/Color;

    .prologue
    const/4 v14, 0x0

    const/high16 v13, 0x43b40000    # 360.0f

    const/high16 v12, 0x42c80000    # 100.0f

    const/high16 v11, 0x40000000    # 2.0f

    const/high16 v10, 0x42700000    # 60.0f

    .line 248
    const/4 v9, 0x0

    invoke-virtual {p0, v9}, Lorg/apache/poi/java/awt/Color;->getRGBColorComponents([F)[F

    move-result-object v7

    .line 249
    .local v7, "rgbValues":[F
    aget v6, v7, v14

    .line 250
    .local v6, "red":F
    const/4 v9, 0x1

    aget v1, v7, v9

    .line 251
    .local v1, "green":F
    const/4 v9, 0x2

    aget v0, v7, v9

    .line 254
    .local v0, "blue":F
    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v9

    invoke-static {v6, v9}, Ljava/lang/Math;->min(FF)F

    move-result v5

    .line 255
    .local v5, "minVal":F
    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v9

    invoke-static {v6, v9}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 259
    .local v4, "maxVal":F
    const/4 v2, 0x0

    .line 261
    .local v2, "hue":F
    cmpl-float v9, v4, v5

    if-nez v9, :cond_1

    .line 262
    const/4 v2, 0x0

    .line 272
    :cond_0
    :goto_0
    add-float v9, v4, v5

    div-float v3, v9, v11

    .line 277
    .local v3, "l":F
    const/4 v8, 0x0

    .line 279
    .local v8, "s":F
    cmpl-float v9, v4, v5

    if-nez v9, :cond_4

    .line 280
    const/4 v8, 0x0

    .line 286
    :goto_1
    const/4 v9, 0x3

    new-array v9, v9, [F

    aput v2, v9, v14

    const/4 v10, 0x1

    mul-float v11, v8, v12

    aput v11, v9, v10

    const/4 v10, 0x2

    mul-float v11, v3, v12

    aput v11, v9, v10

    return-object v9

    .line 263
    .end local v3    # "l":F
    .end local v8    # "s":F
    :cond_1
    cmpl-float v9, v4, v6

    if-nez v9, :cond_2

    .line 264
    sub-float v9, v1, v0

    mul-float/2addr v9, v10

    sub-float v10, v4, v5

    div-float/2addr v9, v10

    add-float/2addr v9, v13

    rem-float v2, v9, v13

    goto :goto_0

    .line 265
    :cond_2
    cmpl-float v9, v4, v1

    if-nez v9, :cond_3

    .line 266
    sub-float v9, v0, v6

    mul-float/2addr v9, v10

    sub-float v10, v4, v5

    div-float/2addr v9, v10

    const/high16 v10, 0x42f00000    # 120.0f

    add-float v2, v9, v10

    goto :goto_0

    .line 267
    :cond_3
    cmpl-float v9, v4, v0

    if-nez v9, :cond_0

    .line 268
    sub-float v9, v6, v1

    mul-float/2addr v9, v10

    sub-float v10, v4, v5

    div-float/2addr v9, v10

    const/high16 v10, 0x43700000    # 240.0f

    add-float v2, v9, v10

    goto :goto_0

    .line 281
    .restart local v3    # "l":F
    .restart local v8    # "s":F
    :cond_4
    const/high16 v9, 0x3f000000    # 0.5f

    cmpg-float v9, v3, v9

    if-gtz v9, :cond_5

    .line 282
    sub-float v9, v4, v5

    add-float v10, v4, v5

    div-float v8, v9, v10

    goto :goto_1

    .line 284
    :cond_5
    sub-float v9, v4, v5

    sub-float v10, v11, v4

    sub-float/2addr v10, v5

    div-float v8, v9, v10

    goto :goto_1
.end method


# virtual methods
.method public adjustHueValue(F)Lorg/apache/poi/java/awt/Color;
    .locals 3
    .param p1, "deg"    # F

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    const/4 v2, 0x2

    aget v1, v1, v2

    iget v2, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->alphaVal:F

    invoke-static {p1, v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->fromHSLToRGB(FFFF)Lorg/apache/poi/java/awt/Color;

    move-result-object v0

    return-object v0
.end method

.method public adjustLuminanceValue(F)Lorg/apache/poi/java/awt/Color;
    .locals 3
    .param p1, "percentage"    # F

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    iget v2, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->alphaVal:F

    invoke-static {v0, v1, p1, v2}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->fromHSLToRGB(FFFF)Lorg/apache/poi/java/awt/Color;

    move-result-object v0

    return-object v0
.end method

.method public adjustSaturationValue(F)Lorg/apache/poi/java/awt/Color;
    .locals 3
    .param p1, "percentage"    # F

    .prologue
    .line 132
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    const/4 v2, 0x2

    aget v1, v1, v2

    iget v2, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->alphaVal:F

    invoke-static {v0, p1, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->fromHSLToRGB(FFFF)Lorg/apache/poi/java/awt/Color;

    move-result-object v0

    return-object v0
.end method

.method public adjustShadeValue(F)Lorg/apache/poi/java/awt/Color;
    .locals 5
    .param p1, "percentage"    # F

    .prologue
    const/high16 v3, 0x42c80000    # 100.0f

    .line 145
    sub-float v2, v3, p1

    div-float v1, v2, v3

    .line 146
    .local v1, "multiplier":F
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    mul-float/2addr v3, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 148
    .local v0, "l":F
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    iget v4, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->alphaVal:F

    invoke-static {v2, v3, v0, v4}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->fromHSLToRGB(FFFF)Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    return-object v2
.end method

.method public adjustTone(F)Lorg/apache/poi/java/awt/Color;
    .locals 5
    .param p1, "percentage"    # F

    .prologue
    const/high16 v4, 0x42c80000    # 100.0f

    .line 161
    add-float v2, v4, p1

    div-float v1, v2, v4

    .line 162
    .local v1, "multiplier":F
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    mul-float/2addr v2, v1

    invoke-static {v4, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 164
    .local v0, "l":F
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    iget v4, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->alphaVal:F

    invoke-static {v2, v3, v0, v4}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->fromHSLToRGB(FFFF)Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    return-object v2
.end method

.method public getAlphaVal()F
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->alphaVal:F

    return v0
.end method

.method public getComplementaryRGB()Lorg/apache/poi/java/awt/Color;
    .locals 4

    .prologue
    .line 184
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    const/high16 v2, 0x43340000    # 180.0f

    add-float/2addr v1, v2

    const/high16 v2, 0x43b40000    # 360.0f

    rem-float v0, v1, v2

    .line 185
    .local v0, "hue":F
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->fromHSLToRGB(FFF)Lorg/apache/poi/java/awt/Color;

    move-result-object v1

    return-object v1
.end method

.method public getHSLValues()[F
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    return-object v0
.end method

.method public getHueValue()F
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public getLuminanceValue()F
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    const/4 v1, 0x2

    aget v0, v0, v1

    return v0
.end method

.method public getRGBValue()Lorg/apache/poi/java/awt/Color;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->rgbColor:Lorg/apache/poi/java/awt/Color;

    return-object v0
.end method

.method public getSaturationValue()F
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 234
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "HSLColorUtil[h="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ",s="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ",l="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->hslVal:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ",alpha="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->alphaVal:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 237
    .local v0, "toString":Ljava/lang/String;
    return-object v0
.end method
