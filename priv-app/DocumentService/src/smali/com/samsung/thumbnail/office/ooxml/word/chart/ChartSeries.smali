.class public Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;
.super Ljava/lang/Object;
.source "ChartSeries.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    }
.end annotation


# static fields
.field public static final NUMERIC_CATCH:I = 0xb

.field public static final SER_CAT:Ljava/lang/String; = "cat"

.field public static final SER_SIZE:Ljava/lang/String; = "size"

.field public static final SER_TEXT:Ljava/lang/String; = "tx"

.field public static final SER_VAL:Ljava/lang/String; = "val"

.field public static final STRING_CATCH:I = 0xa


# instance fields
.field private order:I

.field private seriesVals:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->seriesVals:Ljava/util/HashMap;

    .line 27
    return-void
.end method


# virtual methods
.method public getOrder()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->order:I

    return v0
.end method

.method public getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    .locals 1
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->seriesVals:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    return-object v0
.end method

.method public getSeriesVal()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->seriesVals:Ljava/util/HashMap;

    return-object v0
.end method

.method public putSeriesVal(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;)V
    .locals 1
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "series"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->seriesVals:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    return-void
.end method

.method public setOrder(I)V
    .locals 0
    .param p1, "order"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->order:I

    .line 43
    return-void
.end method
