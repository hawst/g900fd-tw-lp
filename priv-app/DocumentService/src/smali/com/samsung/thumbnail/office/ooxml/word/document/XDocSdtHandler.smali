.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XDocSdtHandler.java"


# instance fields
.field private type:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 18
    const-string/jumbo v0, "sdt"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 19
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;->type:I

    .line 20
    return-void
.end method

.method private init()V
    .locals 6

    .prologue
    .line 23
    const/4 v5, 0x2

    new-array v4, v5, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 25
    .local v4, "seqDescriptors":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtContentHandler;

    iget v5, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;->type:I

    invoke-direct {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtContentHandler;-><init>(I)V

    .line 27
    .local v0, "sdtContentHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtContentHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v5, "sdtContent"

    invoke-direct {v1, v5, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 29
    .local v1, "sdtContentSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v5, 0x0

    aput-object v1, v4, v5

    .line 31
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtPrHandler;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtPrHandler;-><init>()V

    .line 32
    .local v2, "sdtPrHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtPrHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v5, "sdtPr"

    invoke-direct {v3, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 34
    .local v3, "sdtPrSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v5, 0x1

    aput-object v3, v4, v5

    .line 36
    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 37
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 42
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 43
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;->init()V

    .line 44
    return-void
.end method
