.class public Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPPrHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XDocTblPPrHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPPrHandler$ITblPPrObserver;
    }
.end annotation


# instance fields
.field mObserver:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPPrHandler$ITblPPrObserver;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPPrHandler$ITblPPrObserver;)V
    .locals 1
    .param p1, "observer"    # Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPPrHandler$ITblPPrObserver;

    .prologue
    .line 14
    const-string/jumbo v0, "tblpPr"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 15
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPPrHandler;->mObserver:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPPrHandler$ITblPPrObserver;

    .line 16
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 21
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 23
    const-string/jumbo v2, "tblpX"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPPrHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 24
    .local v0, "xPos":Ljava/lang/String;
    const-string/jumbo v2, "tblpY"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPPrHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 26
    .local v1, "yPos":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 27
    const-string/jumbo v2, "tblpXSpec"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPPrHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 30
    :cond_0
    if-nez v1, :cond_1

    .line 31
    const-string/jumbo v2, "tblpYSpec"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPPrHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 34
    :cond_1
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPPrHandler;->mObserver:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPPrHandler$ITblPPrObserver;

    invoke-interface {v2, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPPrHandler$ITblPPrObserver;->setTblPosition(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    return-void
.end method
