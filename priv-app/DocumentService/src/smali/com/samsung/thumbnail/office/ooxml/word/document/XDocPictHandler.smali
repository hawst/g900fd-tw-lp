.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPictHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XDocPictHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    const-string/jumbo v0, "pict"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 26
    return-void
.end method

.method private init()V
    .locals 25

    .prologue
    .line 29
    const/16 v24, 0xa

    move/from16 v0, v24

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    move-object/from16 v19, v0

    .line 31
    .local v19, "seqDescriptors":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v21, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeHandler;

    invoke-direct/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeHandler;-><init>()V

    .line 32
    .local v21, "shapeHandler":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeHandler;
    new-instance v20, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;

    const-string/jumbo v24, "shape"

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 34
    .local v20, "shapeDes":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;
    const/16 v24, 0x0

    aput-object v20, v19, v24

    .line 36
    new-instance v16, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/RectHandler;

    const-string/jumbo v24, "rect"

    move-object/from16 v0, v16

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/RectHandler;-><init>(Ljava/lang/String;)V

    .line 37
    .local v16, "rectHandler":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/RectHandler;
    new-instance v15, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;

    const-string/jumbo v24, "rect"

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    invoke-direct {v15, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 39
    .local v15, "rectDes":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;
    const/16 v24, 0x1

    aput-object v15, v19, v24

    .line 41
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ArcHandler;

    invoke-direct {v4}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ArcHandler;-><init>()V

    .line 42
    .local v4, "arcHandler":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ArcHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;

    const-string/jumbo v24, "arc"

    move-object/from16 v0, v24

    invoke-direct {v3, v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 44
    .local v3, "arcDes":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;
    const/16 v24, 0x2

    aput-object v3, v19, v24

    .line 46
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/LineHandler;

    invoke-direct {v10}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/LineHandler;-><init>()V

    .line 47
    .local v10, "lineHandler":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/LineHandler;
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;

    const-string/jumbo v24, "line"

    move-object/from16 v0, v24

    invoke-direct {v9, v0, v10}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 49
    .local v9, "lineDes":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;
    const/16 v24, 0x3

    aput-object v9, v19, v24

    .line 51
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/CurveHandler;

    invoke-direct {v6}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/CurveHandler;-><init>()V

    .line 52
    .local v6, "curveHandler":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/CurveHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;

    const-string/jumbo v24, "curve"

    move-object/from16 v0, v24

    invoke-direct {v5, v0, v6}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 54
    .local v5, "curveDes":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;
    const/16 v24, 0x4

    aput-object v5, v19, v24

    .line 56
    new-instance v14, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/PolylineHandler;

    invoke-direct {v14}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/PolylineHandler;-><init>()V

    .line 57
    .local v14, "polylineHandler":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/PolylineHandler;
    new-instance v13, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;

    const-string/jumbo v24, "polyline"

    move-object/from16 v0, v24

    invoke-direct {v13, v0, v14}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 59
    .local v13, "polylineDes":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;
    const/16 v24, 0x5

    aput-object v13, v19, v24

    .line 61
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/RectHandler;

    const-string/jumbo v24, "roundrect"

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/RectHandler;-><init>(Ljava/lang/String;)V

    .line 62
    .local v18, "roundRectHandler":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/RectHandler;
    new-instance v17, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;

    const-string/jumbo v24, "roundrect"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 64
    .local v17, "roundRectDes":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;
    const/16 v24, 0x6

    aput-object v17, v19, v24

    .line 66
    new-instance v23, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeTypeHandler;

    invoke-direct/range {v23 .. v23}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeTypeHandler;-><init>()V

    .line 67
    .local v23, "shapeTypeHandler":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeTypeHandler;
    new-instance v22, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;

    const-string/jumbo v24, "shapetype"

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 69
    .local v22, "shapeTypeDes":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;
    const/16 v24, 0x7

    aput-object v22, v19, v24

    .line 71
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/OvalHandler;

    invoke-direct {v12}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/OvalHandler;-><init>()V

    .line 72
    .local v12, "ovalHandler":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/OvalHandler;
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;

    const-string/jumbo v24, "oval"

    move-object/from16 v0, v24

    invoke-direct {v11, v0, v12}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 74
    .local v11, "ovalDes":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;
    const/16 v24, 0x8

    aput-object v11, v19, v24

    .line 76
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocGroupHandler;

    invoke-direct {v8}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocGroupHandler;-><init>()V

    .line 77
    .local v8, "groupHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocGroupHandler;
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;

    const-string/jumbo v24, "group"

    move-object/from16 v0, v24

    invoke-direct {v7, v0, v8}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 79
    .local v7, "groupDes":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;
    const/16 v24, 0x9

    aput-object v7, v19, v24

    .line 81
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPictHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 82
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 99
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 100
    return-void
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 94
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 95
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 87
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 88
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPictHandler;->init()V

    .line 89
    return-void
.end method
