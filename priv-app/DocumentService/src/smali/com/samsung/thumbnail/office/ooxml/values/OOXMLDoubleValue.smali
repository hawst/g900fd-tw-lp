.class public Lcom/samsung/thumbnail/office/ooxml/values/OOXMLDoubleValue;
.super Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;
.source "OOXMLDoubleValue.java"


# instance fields
.field private doubleVal:D


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;-><init>()V

    .line 11
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLDoubleValue;->parseString(Ljava/lang/String;)D

    .line 12
    return-void
.end method

.method private parseString(Ljava/lang/String;)D
    .locals 2
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 19
    const-wide/16 v0, 0x0

    return-wide v0
.end method


# virtual methods
.method public getValue()D
    .locals 2

    .prologue
    .line 15
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLDoubleValue;->doubleVal:D

    return-wide v0
.end method
