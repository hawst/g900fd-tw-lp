.class public Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCShade;
.super Ljava/lang/Object;
.source "XDocTblTCPrHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TCShade"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCShade;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V
    .locals 3
    .param p1, "value"    # Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;

    .prologue
    .line 113
    instance-of v2, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadeValue;

    if-eqz v2, :cond_0

    move-object v1, p1

    .line 114
    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadeValue;

    .line 115
    .local v1, "shadeValue":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadeValue;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 116
    .local v0, "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadeValue;->getValue()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->getFill()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 117
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCShade;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;

    # getter for: Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;->tblCellProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;
    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;->access$000(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;)Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->setShade(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    .line 119
    .end local v0    # "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .end local v1    # "shadeValue":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadeValue;
    :cond_0
    return-void
.end method
