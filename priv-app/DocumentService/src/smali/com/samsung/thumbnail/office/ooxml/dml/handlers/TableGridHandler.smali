.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableGridHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "TableGridHandler.java"


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;)V
    .locals 3
    .param p1, "table"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    .prologue
    .line 11
    const/16 v1, 0x1f

    const-string/jumbo v2, "tblGrid"

    invoke-direct {p0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 13
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableGridColHandler;

    invoke-direct {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableGridColHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;)V

    .line 14
    .local v0, "gridColHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableGridColHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableGridHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "gridCol"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    return-void
.end method
