.class public Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumCacheHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DMLChartNumCacheHandler.java"


# instance fields
.field private curElement:Ljava/lang/String;

.field private ptConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;)V
    .locals 2
    .param p1, "ptConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;

    .prologue
    .line 26
    const/16 v0, 0x23

    const-string/jumbo v1, "numCache"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 27
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumCacheHandler;->ptConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;

    .line 28
    return-void
.end method

.method private init()V
    .locals 6

    .prologue
    .line 31
    const/4 v5, 0x2

    new-array v4, v5, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 33
    .local v4, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtHandler;

    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumCacheHandler;->ptConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;

    invoke-direct {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;)V

    .line 34
    .local v0, "ptHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v5, "pt"

    invoke-direct {v1, v5, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 36
    .local v1, "ptSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/4 v5, 0x0

    aput-object v1, v4, v5

    .line 38
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtCountHandler;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtCountHandler;-><init>()V

    .line 39
    .local v2, "ptcntHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtCountHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v5, "ptCount"

    invoke-direct {v3, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 41
    .local v3, "ptcntSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/4 v5, 0x1

    aput-object v3, v4, v5

    .line 43
    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumCacheHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 44
    return-void
.end method


# virtual methods
.method public charactersHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;[CII)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "content"    # [C
    .param p3, "start"    # I
    .param p4, "end"    # I

    .prologue
    .line 56
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2, p3, p4}, Ljava/lang/String;-><init>([CII)V

    .line 59
    .local v0, "val":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumCacheHandler;->curElement:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumCacheHandler;->curElement:Ljava/lang/String;

    const-string/jumbo v2, "formatCode"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    if-eqz v1, :cond_1

    .line 61
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumCacheHandler;->ptConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;

    invoke-interface {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;->setFormatCode(Ljava/lang/String;)V

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumCacheHandler;->curElement:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumCacheHandler;->curElement:Ljava/lang/String;

    const-string/jumbo v2, "formatCode"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    if-eqz v1, :cond_0

    .line 67
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumCacheHandler;->ptConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;

    invoke-interface {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;->setFormatCode(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 75
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumCacheHandler;->getExpectedNS(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v1

    invoke-static {p2, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumCacheHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)Ljava/lang/String;

    move-result-object v0

    .line 77
    .local v0, "elementName":Ljava/lang/String;
    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumCacheHandler;->curElement:Ljava/lang/String;

    .line 79
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumCacheHandler;->curElement:Ljava/lang/String;

    const-string/jumbo v2, "formatCode"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 80
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 83
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    if-eqz v1, :cond_1

    .line 88
    :goto_0
    return-void

    .line 87
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    const-string/jumbo v2, "Num"

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->setExChartVAl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 49
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 50
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumCacheHandler;->init()V

    .line 51
    return-void
.end method
