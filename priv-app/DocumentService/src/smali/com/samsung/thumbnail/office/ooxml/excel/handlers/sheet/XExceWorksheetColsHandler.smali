.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExceWorksheetColsHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExceWorksheetColsHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 16
    const-string/jumbo v1, "cols"

    invoke-direct {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 17
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetcolHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetcolHandler;-><init>()V

    .line 18
    .local v0, "sheetcolHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetcolHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExceWorksheetColsHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "col"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 4
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 24
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v0

    .line 26
    .local v0, "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tableWidth:F

    sget v3, Lcom/samsung/index/parser/ThumbnailDocumentParser;->MAX_EXCEL_TABLE_WIDTH:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    .line 27
    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tableWidth:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    .line 29
    .local v1, "width":Ljava/lang/Float;
    :goto_0
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v2

    const/high16 v3, 0x42800000    # 64.0f

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    add-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    .line 30
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v2

    sget v3, Lcom/samsung/index/parser/ThumbnailDocumentParser;->MAX_EXCEL_TABLE_WIDTH:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 31
    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->maxCol:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->maxCol:I

    goto :goto_0

    .line 34
    :cond_0
    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->maxCol:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->maxCol:I

    .line 35
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v2

    iput v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tableWidth:F

    .line 41
    .end local v1    # "width":Ljava/lang/Float;
    :cond_1
    return-void
.end method
