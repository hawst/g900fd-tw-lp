.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/SpacingAfterHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "SpacingAfterHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/SpacingAfterHandler$ISpacingAfterConsumer;
    }
.end annotation


# instance fields
.field private spcAfterConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/SpacingAfterHandler$ISpacingAfterConsumer;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/SpacingAfterHandler$ISpacingAfterConsumer;)V
    .locals 2
    .param p1, "spcAfterConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/SpacingAfterHandler$ISpacingAfterConsumer;

    .prologue
    .line 20
    const/16 v0, 0x1f

    const-string/jumbo v1, "spcAft"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 21
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/SpacingAfterHandler;->spcAfterConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/SpacingAfterHandler$ISpacingAfterConsumer;

    .line 22
    return-void
.end method


# virtual methods
.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 27
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 29
    invoke-virtual {p0, p2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/SpacingAfterHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 30
    .local v0, "elementName":Ljava/lang/String;
    const-string/jumbo v3, "spcPcts"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string/jumbo v3, "spcPct"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 32
    :cond_0
    const-string/jumbo v3, "val"

    invoke-virtual {p0, p3, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/SpacingAfterHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 33
    .local v2, "val":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 34
    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    .line 35
    .local v1, "spacing":F
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/SpacingAfterHandler;->spcAfterConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/SpacingAfterHandler$ISpacingAfterConsumer;

    const/high16 v4, 0x447a0000    # 1000.0f

    div-float v4, v1, v4

    invoke-interface {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/SpacingAfterHandler$ISpacingAfterConsumer;->consumeSpacingAfter(F)V

    .line 38
    .end local v1    # "spacing":F
    .end local v2    # "val":Ljava/lang/String;
    :cond_1
    return-void
.end method
