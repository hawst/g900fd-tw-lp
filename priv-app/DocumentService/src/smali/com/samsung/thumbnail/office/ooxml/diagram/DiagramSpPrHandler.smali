.class public Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DiagramSpPrHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$ICustGeomHandler;,
        Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IGradFillHandler;,
        Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$ISolidFillHandler;,
        Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IPstgeomHandler;,
        Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IXfrmHandlerDgm;,
        Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$ISpPrHandler;
    }
.end annotation


# instance fields
.field fillColor:Ljava/lang/String;

.field private fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

.field private gradFill:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IGradFillHandler;

.field private solidFill:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$ISolidFillHandler;

.field theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;


# direct methods
.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V
    .locals 19
    .param p1, "ID"    # I
    .param p2, "dsp"    # Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;
    .param p3, "theme"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .prologue
    .line 39
    const-string/jumbo v18, "spPr"

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 41
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .line 43
    const/16 v18, 0x7

    move/from16 v0, v18

    new-array v13, v0, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 45
    .local v13, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v16, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramXfrmHandler;

    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramXfrmHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IXfrmHandlerDgm;)V

    .line 47
    .local v16, "xfrmHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramXfrmHandler;
    new-instance v17, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v18, "xfrm"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 49
    .local v17, "xfrmSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v18, 0x0

    aput-object v17, v13, v18

    .line 51
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprPstgeomHandler;

    const/16 v18, 0x1f

    move/from16 v0, v18

    move-object/from16 v1, p2

    invoke-direct {v11, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprPstgeomHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IPstgeomHandler;)V

    .line 53
    .local v11, "pstgeomHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprPstgeomHandler;
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v18, "prstGeom"

    move-object/from16 v0, v18

    invoke-direct {v12, v0, v11}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 56
    .local v12, "pstgeomSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v18, 0x1

    aput-object v12, v13, v18

    .line 58
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler;->solidFill:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$ISolidFillHandler;

    .line 59
    new-instance v14, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprSolidFillHandler;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprSolidFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;)V

    .line 61
    .local v14, "solidFillHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprSolidFillHandler;
    new-instance v15, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v18, "solidFill"

    move-object/from16 v0, v18

    invoke-direct {v15, v0, v14}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 63
    .local v15, "solidFillSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v18, 0x2

    aput-object v15, v13, v18

    .line 65
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler;

    const/16 v18, 0x1f

    move/from16 v0, v18

    move-object/from16 v1, p2

    invoke-direct {v3, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;)V

    .line 67
    .local v3, "blipFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v18, "blipFill"

    move-object/from16 v0, v18

    invoke-direct {v4, v0, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 69
    .local v4, "blipSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v18, 0x3

    aput-object v4, v13, v18

    .line 71
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;

    const/16 v18, 0x1f

    move/from16 v0, v18

    move-object/from16 v1, p2

    invoke-direct {v6, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$ICustGeomHandler;)V

    .line 73
    .local v6, "custgeoHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v18, "custGeom"

    move-object/from16 v0, v18

    invoke-direct {v5, v0, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 75
    .local v5, "custSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v18, 0x4

    aput-object v5, v13, v18

    .line 77
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprLineHandler;

    const-string/jumbo v18, "ln"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-direct {v9, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprLineHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprLineHandler$ILineFillHandler;Ljava/lang/String;)V

    .line 79
    .local v9, "lnHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprLineHandler;
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v18, "ln"

    move-object/from16 v0, v18

    invoke-direct {v10, v0, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 81
    .local v10, "lnSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v18, 0x5

    aput-object v10, v13, v18

    .line 83
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler;->gradFill:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IGradFillHandler;

    .line 84
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramGradFillHandler;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v7, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramGradFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 86
    .local v7, "gradFillHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramGradFillHandler;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v18, "gradFill"

    move-object/from16 v0, v18

    invoke-direct {v8, v0, v7}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 88
    .local v8, "gradFillSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v18, 0x6

    aput-object v8, v13, v18

    .line 90
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 92
    return-void
.end method


# virtual methods
.method public consumeFill(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 0
    .param p1, "fillProperties"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .line 148
    return-void
.end method

.method public consumeGS(Ljava/lang/String;FLjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 3
    .param p1, "element"    # Ljava/lang/String;
    .param p2, "pos"    # F
    .param p3, "val"    # Ljava/lang/String;
    .param p4, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 153
    invoke-static {p3}, Lcom/samsung/thumbnail/util/Utils;->replaceThemeColorKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler;->fillColor:Ljava/lang/String;

    .line 156
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x23

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler;->fillColor:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    :goto_0
    return-void

    .line 157
    :catch_0
    move-exception v0

    .line 158
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler;->fillColor:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler;->fillColor:Ljava/lang/String;

    goto :goto_0
.end method

.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 112
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler;->solidFill:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$ISolidFillHandler;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-interface {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$ISolidFillHandler;->setShapeFillProperty(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V

    .line 114
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler;->fillColor:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler;->gradFill:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IGradFillHandler;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler;->fillColor:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IGradFillHandler;->setGradFillProperty(Ljava/lang/String;)V

    .line 117
    :cond_0
    return-void
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 107
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 108
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 97
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 98
    return-void
.end method
