.class public Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFFont;
.super Ljava/lang/Object;
.source "WMFFont.java"


# instance fields
.field public charset:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "charset"    # I

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFFont;->charset:I

    .line 19
    return-void
.end method

.method public constructor <init>(IIIIIII)V
    .locals 0
    .param p1, "charset"    # I
    .param p2, "underline"    # I
    .param p3, "strikeOut"    # I
    .param p4, "italic"    # I
    .param p5, "weight"    # I
    .param p6, "orient"    # I
    .param p7, "escape"    # I

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFFont;->charset:I

    .line 31
    return-void
.end method
