.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillStopHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "GradientFillStopHandler.java"


# instance fields
.field private gradientFill:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;

.field private stop:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;)V
    .locals 1
    .param p1, "gradientFill"    # Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;

    .prologue
    .line 20
    const-string/jumbo v0, "stop"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 22
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillStopHandler;->gradientFill:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;

    .line 23
    return-void
.end method

.method private init()V
    .locals 3

    .prologue
    .line 26
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillStopHandler;->stop:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;

    .line 27
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/FillFGColorHandler;

    const-string/jumbo v1, "color"

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillStopHandler;->stop:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/FillFGColorHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;)V

    .line 29
    .local v0, "fgcolorhandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/FillFGColorHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillStopHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "color"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillStopHandler;->gradientFill:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillStopHandler;->stop:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->addStop(Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;)V

    .line 48
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 35
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 36
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillStopHandler;->init()V

    .line 37
    const-string/jumbo v1, "position"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillStopHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 39
    .local v0, "position":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 40
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillStopHandler;->stop:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;->setPosition(D)V

    .line 42
    :cond_0
    return-void
.end method
