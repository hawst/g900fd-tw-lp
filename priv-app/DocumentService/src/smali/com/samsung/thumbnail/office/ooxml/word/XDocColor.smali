.class public Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
.super Ljava/lang/Object;
.source "XDocColor.java"


# instance fields
.field protected color:Ljava/lang/String;

.field protected isAuto:Z

.field protected isSchemeClr:Z

.field protected lumMod:Ljava/lang/String;

.field protected lumOff:Ljava/lang/String;

.field protected pos:F

.field protected satMod:Ljava/lang/String;

.field protected satOff:Ljava/lang/String;

.field protected shade:Ljava/lang/String;

.field protected themeColor:Ljava/lang/String;

.field protected tint:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-string/jumbo v0, "255"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->shade:Ljava/lang/String;

    .line 26
    const-string/jumbo v0, "0"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->tint:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public static calcColorTint(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "hexString"    # Ljava/lang/String;
    .param p1, "tint"    # Ljava/lang/String;

    .prologue
    const/high16 v10, 0x437f0000    # 255.0f

    const/high16 v9, 0x3f800000    # 1.0f

    .line 238
    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    const v8, 0x47c35000    # 100000.0f

    div-float v6, v7, v8

    .line 239
    .local v6, "tnt":F
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "#"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    .line 240
    .local v5, "rgbColor":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v4

    .line 241
    .local v4, "red":I
    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v0

    .line 242
    .local v0, "blue":I
    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v3

    .line 244
    .local v3, "green":I
    int-to-float v7, v4

    mul-float/2addr v7, v6

    sub-float v8, v9, v6

    mul-float/2addr v8, v10

    add-float/2addr v7, v8

    float-to-int v4, v7

    .line 245
    int-to-float v7, v3

    mul-float/2addr v7, v6

    sub-float v8, v9, v6

    mul-float/2addr v8, v10

    add-float/2addr v7, v8

    float-to-int v3, v7

    .line 246
    int-to-float v7, v0

    mul-float/2addr v7, v6

    sub-float v8, v9, v6

    mul-float/2addr v8, v10

    add-float/2addr v7, v8

    float-to-int v0, v7

    .line 247
    new-instance v1, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v1, v4, v3, v0}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    .line 248
    .local v1, "clr":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v1}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    const v8, 0xffffff

    and-int/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    .line 249
    .local v2, "color":Ljava/lang/String;
    return-object v2
.end method

.method public static calcLuminousColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;
    .locals 11
    .param p0, "hexString"    # Ljava/lang/String;
    .param p1, "lumMod"    # Ljava/lang/Double;

    .prologue
    const/4 v10, 0x2

    .line 152
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "#"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v3

    .line 154
    .local v3, "rgbColor":Lorg/apache/poi/java/awt/Color;
    invoke-static {v3}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->fromRGBToHSL(Lorg/apache/poi/java/awt/Color;)[F

    move-result-object v0

    .line 155
    .local v0, "a":[F
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    const-wide v8, 0x40f86a0000000000L    # 100000.0

    div-double v4, v6, v8

    .line 156
    .local v4, "lValue":D
    aget v6, v0, v10

    float-to-double v6, v6

    mul-double/2addr v6, v4

    double-to-float v6, v6

    aput v6, v0, v10

    .line 157
    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->fromHSLToRGB([F)Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    .line 158
    .local v2, "hslRGBColor":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v2}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v6

    const v7, 0xffffff

    and-int/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    .line 159
    .local v1, "color":Ljava/lang/String;
    return-object v1
.end method

.method public static calcSaturationColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;
    .locals 11
    .param p0, "hexString"    # Ljava/lang/String;
    .param p1, "satMod"    # Ljava/lang/Double;

    .prologue
    const/4 v10, 0x1

    .line 200
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "#"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v3

    .line 202
    .local v3, "rgbColor":Lorg/apache/poi/java/awt/Color;
    invoke-static {v3}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->fromRGBToHSL(Lorg/apache/poi/java/awt/Color;)[F

    move-result-object v0

    .line 203
    .local v0, "a":[F
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    const-wide v8, 0x412e848000000000L    # 1000000.0

    div-double v4, v6, v8

    .line 204
    .local v4, "lValue":D
    aget v6, v0, v10

    float-to-double v6, v6

    mul-double/2addr v6, v4

    double-to-float v6, v6

    aput v6, v0, v10

    .line 205
    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->fromHSLToRGB([F)Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    .line 206
    .local v2, "hslRGBColor":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v2}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v6

    const v7, 0xffffff

    and-int/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    .line 207
    .local v1, "color":Ljava/lang/String;
    return-object v1
.end method

.method public static calculateColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;
    .locals 11
    .param p0, "hexString"    # Ljava/lang/String;
    .param p1, "lumMod"    # Ljava/lang/Double;

    .prologue
    const/4 v10, 0x2

    .line 133
    invoke-static {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->hexToRGB(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v3

    .line 135
    .local v3, "rgbColor":Lorg/apache/poi/java/awt/Color;
    invoke-static {v3}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->fromRGBToHSL(Lorg/apache/poi/java/awt/Color;)[F

    move-result-object v0

    .line 136
    .local v0, "a":[F
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    const-wide v8, 0x40f86a0000000000L    # 100000.0

    div-double v4, v6, v8

    .line 137
    .local v4, "lValue":D
    aget v6, v0, v10

    float-to-double v6, v6

    mul-double/2addr v6, v4

    double-to-float v6, v6

    aput v6, v0, v10

    .line 138
    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->fromHSLToRGB([F)Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    .line 139
    .local v2, "hslRGBColor":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v2}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v6

    const v7, 0xffffff

    and-int/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    .line 140
    .local v1, "color":Ljava/lang/String;
    return-object v1
.end method

.method public static calculateFinalLumValue(DF)D
    .locals 10
    .param p0, "tint"    # D
    .param p2, "lum"    # F

    .prologue
    const-wide v8, 0x406fe00000000000L    # 255.0

    const-wide/16 v4, 0x0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    .line 270
    cmpl-double v2, p0, v4

    if-nez v2, :cond_0

    .line 271
    float-to-double v0, p2

    .line 279
    :goto_0
    return-wide v0

    .line 273
    :cond_0
    const-wide/16 v0, 0x0

    .line 274
    .local v0, "lum1":D
    cmpg-double v2, p0, v4

    if-gez v2, :cond_1

    .line 275
    float-to-double v2, p2

    add-double v4, v6, p0

    mul-double v0, v2, v4

    goto :goto_0

    .line 277
    :cond_1
    float-to-double v2, p2

    sub-double v4, v6, p0

    mul-double/2addr v2, v4

    sub-double v4, v6, p0

    mul-double/2addr v4, v8

    sub-double v4, v8, v4

    add-double v0, v2, v4

    goto :goto_0
.end method

.method public static calculateShadeColor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "hexString"    # Ljava/lang/String;
    .param p1, "shade"    # Ljava/lang/String;

    .prologue
    .line 219
    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    const v8, 0x47c35000    # 100000.0f

    div-float v6, v7, v8

    .line 220
    .local v6, "shd":F
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "#"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    .line 221
    .local v5, "rgbColor":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v4

    .line 222
    .local v4, "red":I
    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v0

    .line 223
    .local v0, "blue":I
    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v3

    .line 224
    .local v3, "green":I
    new-instance v1, Lorg/apache/poi/java/awt/Color;

    int-to-float v7, v4

    mul-float/2addr v7, v6

    float-to-int v7, v7

    int-to-float v8, v3

    mul-float/2addr v8, v6

    float-to-int v8, v8

    int-to-float v9, v0

    mul-float/2addr v9, v6

    float-to-int v9, v9

    invoke-direct {v1, v7, v8, v9}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    .line 226
    .local v1, "clr":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v1}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    const v8, 0xffffff

    and-int/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    .line 227
    .local v2, "color":Ljava/lang/String;
    return-object v2
.end method

.method public static displayRgbfromThemeandtint(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;
    .locals 14
    .param p0, "themergbclr"    # Ljava/lang/String;
    .param p1, "tint"    # Ljava/lang/String;

    .prologue
    const/4 v13, 0x2

    const/high16 v12, 0x42c80000    # 100.0f

    .line 285
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "#"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v4

    .line 287
    .local v4, "rgbColr":Lorg/apache/poi/java/awt/Color;
    invoke-static {v4}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->fromRGBToHSL(Lorg/apache/poi/java/awt/Color;)[F

    move-result-object v0

    .line 288
    .local v0, "a":[F
    invoke-static {p1}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    .line 290
    .local v6, "tintval":D
    aget v5, v0, v13

    const/high16 v8, 0x437f0000    # 255.0f

    mul-float/2addr v5, v8

    div-float/2addr v5, v12

    invoke-static {v6, v7, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calculateFinalLumValue(DF)D

    move-result-wide v8

    const-wide v10, 0x406fe00000000000L    # 255.0

    div-double v2, v8, v10

    .line 295
    .local v2, "calculateFinalLumValue":D
    double-to-float v5, v2

    mul-float/2addr v5, v12

    aput v5, v0, v13

    .line 298
    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/util/HSLColorUtil;->fromHSLToRGB([F)Lorg/apache/poi/java/awt/Color;

    move-result-object v1

    .line 300
    .local v1, "output":Lorg/apache/poi/java/awt/Color;
    return-object v1
.end method

.method public static hexToRGB(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;
    .locals 6
    .param p0, "colorStr"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x4

    const/4 v2, 0x2

    const/16 v4, 0x10

    .line 125
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x6

    invoke-virtual {p0, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    return-object v0
.end method

.method public static modulateLuminanace(Lorg/apache/poi/java/awt/Color;DI)Lorg/apache/poi/java/awt/Color;
    .locals 9
    .param p0, "c"    # Lorg/apache/poi/java/awt/Color;
    .param p1, "lumMod"    # D
    .param p3, "lumOff"    # I

    .prologue
    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    .line 172
    if-lez p3, :cond_0

    .line 173
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v1

    rsub-int v1, v1, 0xff

    int-to-double v2, v1

    sub-double v4, v6, p1

    mul-double/2addr v2, v4

    div-double/2addr v2, v6

    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v1

    int-to-double v4, v1

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v2

    rsub-int v2, v2, 0xff

    mul-int/2addr v2, p3

    int-to-double v2, v2

    div-double/2addr v2, v6

    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v4

    int-to-double v4, v4

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v3

    rsub-int v3, v3, 0xff

    mul-int/2addr v3, p3

    int-to-double v4, v3

    div-double/2addr v4, v6

    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v3

    int-to-double v6, v3

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v3, v4

    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/apache/poi/java/awt/Color;-><init>(IIII)V

    .line 187
    .local v0, "color":Lorg/apache/poi/java/awt/Color;
    :goto_0
    return-object v0

    .line 180
    .end local v0    # "color":Lorg/apache/poi/java/awt/Color;
    :cond_0
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v1

    int-to-double v2, v1

    mul-double/2addr v2, p1

    div-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v2, p1

    div-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v3

    int-to-double v4, v3

    mul-double/2addr v4, p1

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v3, v4

    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/apache/poi/java/awt/Color;-><init>(IIII)V

    .restart local v0    # "color":Lorg/apache/poi/java/awt/Color;
    goto :goto_0
.end method


# virtual methods
.method public getColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->color:Ljava/lang/String;

    return-object v0
.end method

.method public getLumMod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->lumMod:Ljava/lang/String;

    return-object v0
.end method

.method public getLumOff()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->lumOff:Ljava/lang/String;

    return-object v0
.end method

.method public getPosition()F
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->pos:F

    return v0
.end method

.method public getSatMod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->satMod:Ljava/lang/String;

    return-object v0
.end method

.method public getSatOff()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->satOff:Ljava/lang/String;

    return-object v0
.end method

.method public getShade()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->shade:Ljava/lang/String;

    return-object v0
.end method

.method public getThemeColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->themeColor:Ljava/lang/String;

    return-object v0
.end method

.method public getTint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->tint:Ljava/lang/String;

    return-object v0
.end method

.method public isAuto()Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->isAuto:Z

    return v0
.end method

.method public isSchemeClr()Z
    .locals 1

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->isSchemeClr:Z

    return v0
.end method

.method public setAuto(Z)V
    .locals 0
    .param p1, "auto"    # Z

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->isAuto:Z

    .line 31
    return-void
.end method

.method public setColor(Ljava/lang/String;)V
    .locals 0
    .param p1, "color"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->color:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public setLumMod(Ljava/lang/String;)V
    .locals 0
    .param p1, "lumMod"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->lumMod:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public setLumOff(Ljava/lang/String;)V
    .locals 0
    .param p1, "lumOff"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->lumOff:Ljava/lang/String;

    .line 55
    return-void
.end method

.method public setPosition(F)V
    .locals 0
    .param p1, "pos"    # F

    .prologue
    .line 66
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->pos:F

    .line 67
    return-void
.end method

.method public setSatMod(Ljava/lang/String;)V
    .locals 0
    .param p1, "satMod"    # Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->satMod:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public setSatOff(Ljava/lang/String;)V
    .locals 0
    .param p1, "satOff"    # Ljava/lang/String;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->satOff:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public setSchemeClr(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->isSchemeClr:Z

    .line 111
    return-void
.end method

.method public setShade(Ljava/lang/String;)V
    .locals 0
    .param p1, "shade"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->shade:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public setThemeColor(Ljava/lang/String;)V
    .locals 0
    .param p1, "colorName"    # Ljava/lang/String;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->themeColor:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public setTint(Ljava/lang/String;)V
    .locals 0
    .param p1, "tint"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->tint:Ljava/lang/String;

    .line 47
    return-void
.end method
