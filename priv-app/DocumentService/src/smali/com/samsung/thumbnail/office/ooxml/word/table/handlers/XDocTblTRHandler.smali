.class public Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XDocTblTRHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler$ITblRowPropObserver;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    .line 20
    const-string/jumbo v3, "tr"

    invoke-direct {p0, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 22
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler;

    invoke-direct {v2, p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler$ITblRowPropObserver;)V

    .line 23
    .local v2, "trPrHandler":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v4, "trPr"

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCHandler;-><init>()V

    .line 26
    .local v1, "tcHandler":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCHandler;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v4, "tc"

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;

    const/16 v3, 0x66

    invoke-direct {v0, v3}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;-><init>(I)V

    .line 29
    .local v0, "sdtHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v4, "sdt"

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 47
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->endRow()V

    .line 48
    return-void
.end method

.method public setTblRowProp(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "rowProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;

    .prologue
    .line 53
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->setTblRowProperty(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;)V

    .line 54
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 35
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->startRow()V

    .line 36
    return-void
.end method
