.class public final Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPicture;
.super Ljava/lang/Object;
.source "XSSFPicture.java"


# instance fields
.field protected height:I

.field protected relId:Ljava/lang/String;

.field private vmlStyle:Z

.field protected width:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPicture;->vmlStyle:Z

    .line 53
    return-void
.end method


# virtual methods
.method public getHeight()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPicture;->height:I

    return v0
.end method

.method public getRelId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPicture;->relId:Ljava/lang/String;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPicture;->width:I

    return v0
.end method

.method public isVMLType()Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPicture;->vmlStyle:Z

    return v0
.end method

.method public setExtent(II)V
    .locals 0
    .param p1, "cx"    # I
    .param p2, "cy"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPicture;->width:I

    .line 65
    iput p2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPicture;->height:I

    .line 66
    return-void
.end method

.method public setImageRelId(Ljava/lang/String;)V
    .locals 0
    .param p1, "relId"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPicture;->relId:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public setVMLStyle(Ljava/lang/String;)V
    .locals 7
    .param p1, "style"    # Ljava/lang/String;

    .prologue
    .line 81
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPicture;->vmlStyle:Z

    .line 83
    move-object v2, p1

    .line 85
    .local v2, "val":Ljava/lang/String;
    :try_start_0
    const-string/jumbo v4, "width:"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 86
    const/16 v4, 0x3a

    invoke-virtual {v2, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    const/16 v5, 0x3b

    invoke-virtual {v2, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 88
    .local v3, "wid":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 90
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPicture;->width:I

    .line 92
    const/16 v4, 0x3b

    invoke-virtual {v2, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 93
    invoke-virtual {p0, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPicture;->setVMLStyle(Ljava/lang/String;)V

    .line 112
    .end local v3    # "wid":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    const-string/jumbo v4, "height:"

    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 97
    const/16 v4, 0x3a

    invoke-virtual {v2, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    const/16 v5, 0x3b

    invoke-virtual {v2, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 99
    .local v1, "hei":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 102
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPicture;->height:I

    .line 104
    const/16 v4, 0x3b

    invoke-virtual {v2, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 106
    invoke-virtual {p0, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPicture;->setVMLStyle(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 109
    .end local v1    # "hei":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 110
    .local v0, "ex":Ljava/lang/Exception;
    const-string/jumbo v4, "XSSFPicture"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception in setting Picture Style : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
