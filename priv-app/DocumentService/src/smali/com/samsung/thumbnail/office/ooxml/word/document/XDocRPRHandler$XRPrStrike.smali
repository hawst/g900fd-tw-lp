.class Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrStrike;
.super Ljava/lang/Object;
.source "XDocRPRHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "XRPrStrike"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;


# direct methods
.method constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;)V
    .locals 0

    .prologue
    .line 313
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrStrike;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V
    .locals 3
    .param p1, "value"    # Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;

    .prologue
    .line 317
    instance-of v1, p1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 318
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;

    .line 319
    .local v0, "strike":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrStrike;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;

    iget-object v1, v1, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;->charProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;->getValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setStrike(Z)V

    .line 321
    .end local v0    # "strike":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;
    :cond_0
    return-void
.end method
