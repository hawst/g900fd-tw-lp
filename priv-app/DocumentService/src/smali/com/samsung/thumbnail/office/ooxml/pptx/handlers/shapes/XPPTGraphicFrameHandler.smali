.class public Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicFrameHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XPPTGraphicFrameHandler.java"


# instance fields
.field ID:I

.field private graphicFrame:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;

.field private shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "ID"    # I

    .prologue
    .line 28
    const-string/jumbo v0, "graphicFrame"

    invoke-direct {p0, p1, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 29
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicFrameHandler;->ID:I

    .line 30
    return-void
.end method

.method private init()V
    .locals 13

    .prologue
    const/16 v12, 0x1f

    const/16 v11, 0x33

    .line 33
    const/4 v8, 0x3

    new-array v5, v8, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 35
    .local v5, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualShapePropHandler;

    iget v8, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicFrameHandler;->ID:I

    const-string/jumbo v9, "nvGraphicFramePr"

    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicFrameHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-direct {v3, v8, v9, v10}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualShapePropHandler;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 37
    .local v3, "nonVisualPropHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualShapePropHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;

    const-string/jumbo v8, "nvGraphicFramePr"

    invoke-direct {v4, v11, v8, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 40
    .local v4, "nvGraphicFrame":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;
    const/4 v8, 0x0

    aput-object v4, v5, v8

    .line 42
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler;

    iget-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicFrameHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-direct {v6, v11, v8}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 44
    .local v6, "xfrmHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler;
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;

    const-string/jumbo v8, "xfrm"

    invoke-direct {v7, v11, v8, v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 47
    .local v7, "xfrmSeq":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;
    const/4 v8, 0x1

    aput-object v7, v5, v8

    .line 49
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicHandler;

    iget-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicFrameHandler;->graphicFrame:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;

    invoke-direct {v0, v8}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;)V

    .line 51
    .local v0, "graphicDataHandler":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/OOXMLSingleElementHandler;

    const-string/jumbo v8, "graphic"

    invoke-direct {v1, v12, v8, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSingleElementHandler;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 55
    .local v1, "graphicDataSingleHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLSingleElementHandler;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    const-string/jumbo v8, "graphic"

    invoke-direct {v2, v12, v8, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 58
    .local v2, "graphicHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    const/4 v8, 0x2

    aput-object v2, v5, v8

    .line 60
    iput-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicFrameHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 61
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 66
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 68
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    .line 69
    .local v0, "slide":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicFrameHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 70
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicFrameHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-direct {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicFrameHandler;->graphicFrame:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;

    .line 71
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicFrameHandler;->graphicFrame:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->addShapes(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;)V

    .line 72
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicFrameHandler;->init()V

    .line 73
    return-void
.end method
