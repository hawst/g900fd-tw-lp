.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPageBreakBeforeHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;
.source "XDocPageBreakBeforeHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPageBreakBeforeHandler$IXPgBrBeforePara;
    }
.end annotation


# instance fields
.field pgBrBefCallback:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPageBreakBeforeHandler$IXPgBrBeforePara;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPageBreakBeforeHandler$IXPgBrBeforePara;)V
    .locals 1
    .param p1, "pgBrBefCallback"    # Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPageBreakBeforeHandler$IXPgBrBeforePara;

    .prologue
    .line 22
    const-string/jumbo v0, "pageBreakBefore"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;-><init>(Ljava/lang/String;)V

    .line 23
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPageBreakBeforeHandler;->pgBrBefCallback:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPageBreakBeforeHandler$IXPgBrBeforePara;

    .line 24
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 29
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 30
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPageBreakBeforeHandler;->pgBrBefCallback:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPageBreakBeforeHandler$IXPgBrBeforePara;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPageBreakBeforeHandler$IXPgBrBeforePara;->setPgBrBefStatus(Z)V

    .line 31
    return-void
.end method
