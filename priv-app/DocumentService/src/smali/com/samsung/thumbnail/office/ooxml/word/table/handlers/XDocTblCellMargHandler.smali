.class public Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblCellMargHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XDocTblCellMargHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler$IXTblWOberver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblCellMargHandler$ITblCellMargin;
    }
.end annotation


# instance fields
.field private observer:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblCellMargHandler$ITblCellMargin;

.field private tblCellMargin:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblCellMargHandler$ITblCellMargin;)V
    .locals 1
    .param p1, "observer"    # Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblCellMargHandler$ITblCellMargin;

    .prologue
    .line 24
    const-string/jumbo v0, "tblCellMar"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 25
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblCellMargHandler;->observer:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblCellMargHandler$ITblCellMargin;

    .line 26
    return-void
.end method


# virtual methods
.method protected init()V
    .locals 10

    .prologue
    .line 29
    const/4 v9, 0x4

    new-array v6, v9, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 31
    .local v6, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;

    const-string/jumbo v9, "top"

    invoke-direct {v8, v9, p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler$IXTblWOberver;)V

    .line 33
    .local v8, "topWidth":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v9, "top"

    invoke-direct {v7, v9, v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 35
    .local v7, "topSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v9, 0x0

    aput-object v7, v6, v9

    .line 37
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;

    const-string/jumbo v9, "left"

    invoke-direct {v2, v9, p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler$IXTblWOberver;)V

    .line 39
    .local v2, "leftBorder":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v9, "left"

    invoke-direct {v3, v9, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 41
    .local v3, "leftSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v9, 0x1

    aput-object v3, v6, v9

    .line 43
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;

    const-string/jumbo v9, "bottom"

    invoke-direct {v0, v9, p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler$IXTblWOberver;)V

    .line 45
    .local v0, "bottomBorder":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v9, "bottom"

    invoke-direct {v1, v9, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 47
    .local v1, "bottomSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v9, 0x2

    aput-object v1, v6, v9

    .line 49
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;

    const-string/jumbo v9, "right"

    invoke-direct {v4, v9, p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler$IXTblWOberver;)V

    .line 51
    .local v4, "rightBorder":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v9, "right"

    invoke-direct {v5, v9, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 53
    .local v5, "rightSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v9, 0x3

    aput-object v5, v6, v9

    .line 55
    iput-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblCellMargHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 56
    return-void
.end method

.method public setElement(Ljava/lang/String;)V
    .locals 2
    .param p1, "element"    # Ljava/lang/String;

    .prologue
    .line 77
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblCellMargHandler;->tblCellMargin:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;

    .line 79
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblCellMargHandler;->observer:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblCellMargHandler$ITblCellMargin;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblCellMargHandler;->tblCellMargin:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;

    invoke-interface {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblCellMargHandler$ITblCellMargin;->setTblCellMargin(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;)V

    .line 80
    return-void
.end method

.method public setTblWidth(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;I)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "width"    # I

    .prologue
    .line 68
    return-void
.end method

.method public setWidthType(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "widthType"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    .prologue
    .line 73
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 61
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 62
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblCellMargHandler;->init()V

    .line 63
    return-void
.end method
