.class public Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;
.super Ljava/lang/Object;
.source "XDocNumLvl.java"


# instance fields
.field private charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

.field private iLvl:I

.field private isLgl:Z

.field private jc:Ljava/lang/String;

.field private numFmt:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field private paraProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

.field private picBulletId:I

.field private restart:I

.field private start:I

.field private style:Ljava/lang/String;

.field private text:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    return-object v0
.end method

.method public getILvl()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->iLvl:I

    return v0
.end method

.method public getIsLgl()Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->isLgl:Z

    return v0
.end method

.method public getJc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->jc:Ljava/lang/String;

    return-object v0
.end method

.method public getNumFmt()Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->numFmt:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    return-object v0
.end method

.method public getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->paraProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    return-object v0
.end method

.method public getPicBulletId()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->picBulletId:I

    return v0
.end method

.method public getRestart()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->restart:I

    return v0
.end method

.method public getStart()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->start:I

    return v0
.end method

.method public getStyle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->style:Ljava/lang/String;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->text:Ljava/lang/String;

    return-object v0
.end method

.method public setCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V
    .locals 0
    .param p1, "charProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .line 62
    return-void
.end method

.method public setILvl(I)V
    .locals 0
    .param p1, "iLvl"    # I

    .prologue
    .line 37
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->iLvl:I

    .line 38
    return-void
.end method

.method public setIsLgl(Z)V
    .locals 0
    .param p1, "isLgl"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->isLgl:Z

    .line 46
    return-void
.end method

.method public setJc(Ljava/lang/String;)V
    .locals 0
    .param p1, "jc"    # Ljava/lang/String;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->jc:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public setNumFmt(Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;)V
    .locals 0
    .param p1, "numFmt"    # Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->numFmt:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 34
    return-void
.end method

.method public setParaProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V
    .locals 0
    .param p1, "paraProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->paraProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    .line 66
    return-void
.end method

.method public setPicBulletId(I)V
    .locals 0
    .param p1, "picBulletId"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->picBulletId:I

    .line 54
    return-void
.end method

.method public setRestart(I)V
    .locals 0
    .param p1, "restart"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->restart:I

    .line 42
    return-void
.end method

.method public setStart(I)V
    .locals 0
    .param p1, "start"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->start:I

    .line 30
    return-void
.end method

.method public setStyle(Ljava/lang/String;)V
    .locals 0
    .param p1, "style"    # Ljava/lang/String;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->style:Ljava/lang/String;

    .line 70
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->text:Ljava/lang/String;

    .line 50
    return-void
.end method
