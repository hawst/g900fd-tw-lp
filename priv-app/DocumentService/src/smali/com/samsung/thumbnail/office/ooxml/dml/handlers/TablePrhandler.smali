.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TablePrhandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "TablePrhandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableStyleIdHandler$IStyleIdConsumer;


# instance fields
.field private styleId:Ljava/lang/String;

.field private table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;)V
    .locals 2
    .param p1, "table"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    .prologue
    .line 25
    const/16 v0, 0x1f

    const-string/jumbo v1, "tblPr"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 26
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TablePrhandler;->table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    .line 27
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    .line 68
    const/4 v3, 0x1

    new-array v0, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 70
    .local v0, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableStyleIdHandler;

    invoke-direct {v2, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableStyleIdHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableStyleIdHandler$IStyleIdConsumer;)V

    .line 71
    .local v2, "tblStyleIdHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableStyleIdHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v3, "tableStyleId"

    invoke-direct {v1, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 73
    .local v1, "styleId":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v3, 0x0

    aput-object v1, v0, v3

    .line 75
    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TablePrhandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 76
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 79
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TablePrhandler;->table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TablePrhandler;->styleId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->setStyleId(Ljava/lang/String;)V

    .line 81
    return-void
.end method

.method public setStyleId(Ljava/lang/String;)V
    .locals 0
    .param p1, "styleId"    # Ljava/lang/String;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TablePrhandler;->styleId:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 8
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 32
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 34
    const-string/jumbo v6, "firstRow"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TablePrhandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v3

    .line 36
    .local v3, "firstRow":Ljava/lang/String;
    const-string/jumbo v6, "firstCol"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TablePrhandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    .line 38
    .local v2, "firstCol":Ljava/lang/String;
    const-string/jumbo v6, "lastRow"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TablePrhandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v5

    .line 39
    .local v5, "lastRow":Ljava/lang/String;
    const-string/jumbo v6, "lastCol"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TablePrhandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v4

    .line 40
    .local v4, "lastCol":Ljava/lang/String;
    const-string/jumbo v6, "bandRow"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TablePrhandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 41
    .local v1, "bandRow":Ljava/lang/String;
    const-string/jumbo v6, "bandCol"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TablePrhandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 43
    .local v0, "bandCol":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 45
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TablePrhandler;->table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->setFirstRow(I)V

    .line 47
    :cond_0
    if-eqz v2, :cond_1

    .line 48
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TablePrhandler;->table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->setFirstCol(I)V

    .line 50
    :cond_1
    if-eqz v5, :cond_2

    .line 51
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TablePrhandler;->table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->setLastRow(I)V

    .line 53
    :cond_2
    if-eqz v4, :cond_3

    .line 54
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TablePrhandler;->table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->setLastCol(I)V

    .line 57
    :cond_3
    if-eqz v0, :cond_4

    .line 58
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TablePrhandler;->table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->setBandCol(I)V

    .line 60
    :cond_4
    if-eqz v1, :cond_5

    .line 61
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TablePrhandler;->table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->setBandRow(I)V

    .line 63
    :cond_5
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TablePrhandler;->init()V

    .line 64
    return-void
.end method
