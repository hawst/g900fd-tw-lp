.class public Lcom/samsung/thumbnail/office/ooxml/OOXMLTextOnlyHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedTagNoChildrenHandler;
.source "OOXMLTextOnlyHandler.java"


# instance fields
.field private text:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "nsId"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedTagNoChildrenHandler;-><init>(ILjava/lang/String;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "elementName"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedTagNoChildrenHandler;-><init>(Ljava/lang/String;)V

    .line 15
    return-void
.end method


# virtual methods
.method public charactersHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;[CII)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "content"    # [C
    .param p3, "start"    # I
    .param p4, "end"    # I

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLTextOnlyHandler;->text:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2, p3, p4}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 25
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 30
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedTagNoChildrenHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 32
    .local v0, "sbTxt":Ljava/lang/StringBuilder;
    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLTextOnlyHandler;->text:Ljava/lang/StringBuilder;

    .line 33
    return-void
.end method
