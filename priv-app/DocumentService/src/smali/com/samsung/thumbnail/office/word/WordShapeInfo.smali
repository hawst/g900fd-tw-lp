.class public Lcom/samsung/thumbnail/office/word/WordShapeInfo;
.super Ljava/lang/Object;
.source "WordShapeInfo.java"


# instance fields
.field private escherOptRecord:Lorg/apache/poi/ddf/EscherOptRecord;

.field private escherSpRecord:Lorg/apache/poi/ddf/EscherSpRecord;

.field private filledState:Z

.field private fspa:Lorg/apache/poi/hwpf/model/FSPA;

.field private isHorizontalLine:Z

.field private stringText:Ljava/lang/String;

.field private strokeStatus:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object v0, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->escherOptRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    .line 16
    iput-object v0, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->escherSpRecord:Lorg/apache/poi/ddf/EscherSpRecord;

    .line 17
    iput-object v0, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->fspa:Lorg/apache/poi/hwpf/model/FSPA;

    .line 18
    iput-boolean v1, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->filledState:Z

    .line 19
    iput-boolean v1, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->strokeStatus:Z

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->isHorizontalLine:Z

    .line 43
    return-void
.end method

.method private getEscherProperty(S)I
    .locals 4
    .param p1, "propId"    # S

    .prologue
    const/4 v2, 0x0

    .line 103
    iget-object v3, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->escherOptRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    invoke-static {v3, p1}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v0

    .line 104
    .local v0, "prop":Lorg/apache/poi/ddf/EscherProperty;
    if-nez v0, :cond_1

    .line 108
    :cond_0
    :goto_0
    return v2

    .line 106
    :cond_1
    iget-object v3, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->escherOptRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    invoke-static {v3, p1}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 108
    .local v1, "propSimple":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v2

    goto :goto_0
.end method

.method private static getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;
    .locals 3
    .param p0, "opt"    # Lorg/apache/poi/ddf/EscherOptRecord;
    .param p1, "propId"    # I

    .prologue
    .line 114
    if-eqz p0, :cond_1

    .line 115
    invoke-virtual {p0}, Lorg/apache/poi/ddf/EscherOptRecord;->getEscherProperties()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 116
    .local v0, "iterator":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 117
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherProperty;

    .line 118
    .local v1, "prop":Lorg/apache/poi/ddf/EscherProperty;
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherProperty;->getPropertyNumber()S

    move-result v2

    if-ne v2, p1, :cond_0

    .line 121
    .end local v0    # "iterator":Ljava/util/Iterator;
    .end local v1    # "prop":Lorg/apache/poi/ddf/EscherProperty;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getAdjustmentValue(I)I
    .locals 2
    .param p1, "idx"    # I

    .prologue
    .line 95
    if-ltz p1, :cond_0

    const/16 v0, 0x9

    if-le p1, v0, :cond_1

    .line 96
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "The index of an adjustment value must be in the [0, 9] range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 99
    :cond_1
    add-int/lit16 v0, p1, 0x147

    int-to-short v0, v0

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getEscherProperty(S)I

    move-result v0

    return v0
.end method

.method public getEscherOptRecord()Lorg/apache/poi/ddf/EscherOptRecord;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->escherOptRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    return-object v0
.end method

.method public getEscherSpRecord()Lorg/apache/poi/ddf/EscherSpRecord;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->escherSpRecord:Lorg/apache/poi/ddf/EscherSpRecord;

    return-object v0
.end method

.method public getFilledStatus()Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->filledState:Z

    return v0
.end method

.method public getHeight()F
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->fspa:Lorg/apache/poi/hwpf/model/FSPA;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FSPA;->getYaBottom()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->fspa:Lorg/apache/poi/hwpf/model/FSPA;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/FSPA;->getYaTop()I

    move-result v1

    sub-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x48

    int-to-float v0, v0

    const/high16 v1, 0x44100000    # 576.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public getLeft()F
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->fspa:Lorg/apache/poi/hwpf/model/FSPA;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FSPA;->getXaLeft()I

    move-result v0

    mul-int/lit8 v0, v0, 0x48

    int-to-float v0, v0

    const/high16 v1, 0x44100000    # 576.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public getLineColor()Lorg/apache/poi/java/awt/Color;
    .locals 7

    .prologue
    .line 147
    iget-object v4, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->escherOptRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    const/16 v5, 0x1c0

    invoke-static {v4, v5}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 154
    .local v1, "p1":Lorg/apache/poi/ddf/EscherSimpleProperty;
    const/4 v0, 0x0

    .line 156
    .local v0, "clr":Lorg/apache/poi/java/awt/Color;
    if-nez v1, :cond_0

    const/4 v2, 0x0

    .line 157
    .local v2, "rgb":I
    :goto_0
    new-instance v3, Lorg/apache/poi/java/awt/Color;

    const/4 v4, 0x1

    invoke-direct {v3, v2, v4}, Lorg/apache/poi/java/awt/Color;-><init>(IZ)V

    .line 158
    .local v3, "tmp":Lorg/apache/poi/java/awt/Color;
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    .end local v0    # "clr":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v4

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v5

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v6

    invoke-direct {v0, v4, v5, v6}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    .line 160
    .restart local v0    # "clr":Lorg/apache/poi/java/awt/Color;
    return-object v0

    .line 156
    .end local v2    # "rgb":I
    .end local v3    # "tmp":Lorg/apache/poi/java/awt/Color;
    :cond_0
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v2

    goto :goto_0
.end method

.method public getShapeColor()Lorg/apache/poi/java/awt/Color;
    .locals 9

    .prologue
    .line 125
    iget-object v6, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->escherOptRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    const/16 v7, 0x181

    invoke-static {v6, v7}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 131
    .local v2, "p1":Lorg/apache/poi/ddf/EscherSimpleProperty;
    iget-object v6, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->escherOptRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    const/16 v7, 0x182

    invoke-static {v6, v7}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 135
    .local v3, "p3":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-nez v3, :cond_1

    const/16 v0, 0xff

    .line 137
    .local v0, "alpha":I
    :goto_0
    const/4 v1, 0x0

    .line 138
    .local v1, "clr":Lorg/apache/poi/java/awt/Color;
    if-eqz v2, :cond_0

    .line 139
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v4

    .line 140
    .local v4, "rgb":I
    new-instance v5, Lorg/apache/poi/java/awt/Color;

    const/4 v6, 0x1

    invoke-direct {v5, v4, v6}, Lorg/apache/poi/java/awt/Color;-><init>(IZ)V

    .line 141
    .local v5, "tmp":Lorg/apache/poi/java/awt/Color;
    new-instance v1, Lorg/apache/poi/java/awt/Color;

    .end local v1    # "clr":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v6

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v7

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v8

    invoke-direct {v1, v6, v7, v8, v0}, Lorg/apache/poi/java/awt/Color;-><init>(IIII)V

    .line 143
    .end local v4    # "rgb":I
    .end local v5    # "tmp":Lorg/apache/poi/java/awt/Color;
    .restart local v1    # "clr":Lorg/apache/poi/java/awt/Color;
    :cond_0
    return-object v1

    .line 135
    .end local v0    # "alpha":I
    .end local v1    # "clr":Lorg/apache/poi/java/awt/Color;
    :cond_1
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v6

    shr-int/lit8 v6, v6, 0x8

    and-int/lit16 v0, v6, 0xff

    goto :goto_0
.end method

.method public getShapeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->isHorizontalLine:Z

    if-eqz v0, :cond_0

    .line 89
    const-string/jumbo v0, "line"

    .line 91
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->escherSpRecord:Lorg/apache/poi/ddf/EscherSpRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSpRecord;->getOptions()S

    move-result v0

    shr-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Lorg/apache/poi/hslf/model/ShapeTypes;->typeName(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getShapeText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->stringText:Ljava/lang/String;

    return-object v0
.end method

.method public getStrokeStatus()Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->strokeStatus:Z

    return v0
.end method

.method public getTop()F
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->fspa:Lorg/apache/poi/hwpf/model/FSPA;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FSPA;->getYaTop()I

    move-result v0

    mul-int/lit8 v0, v0, 0x48

    int-to-float v0, v0

    const/high16 v1, 0x44100000    # 576.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public getWidth()F
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->fspa:Lorg/apache/poi/hwpf/model/FSPA;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FSPA;->getXaRight()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->fspa:Lorg/apache/poi/hwpf/model/FSPA;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/FSPA;->getXaLeft()I

    move-result v1

    sub-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x48

    int-to-float v0, v0

    const/high16 v1, 0x44100000    # 576.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public isHorizontalLine()Z
    .locals 1

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->isHorizontalLine:Z

    return v0
.end method

.method public setEscherOptRecord(Lorg/apache/poi/ddf/EscherOptRecord;)V
    .locals 0
    .param p1, "escherOptRecord"    # Lorg/apache/poi/ddf/EscherOptRecord;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->escherOptRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    .line 51
    return-void
.end method

.method public setFSPA(Lorg/apache/poi/hwpf/model/FSPA;)V
    .locals 0
    .param p1, "fspa"    # Lorg/apache/poi/hwpf/model/FSPA;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->fspa:Lorg/apache/poi/hwpf/model/FSPA;

    .line 55
    return-void
.end method

.method public setFilledStatus(Ljava/lang/String;)V
    .locals 1
    .param p1, "status"    # Ljava/lang/String;

    .prologue
    .line 33
    if-eqz p1, :cond_0

    const-string/jumbo v0, "f"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->filledState:Z

    .line 35
    :cond_0
    return-void
.end method

.method public setHorizontalLine(Z)V
    .locals 0
    .param p1, "isHorizontalLine"    # Z

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->isHorizontalLine:Z

    .line 80
    return-void
.end method

.method public setShapeText(Ljava/lang/String;)V
    .locals 0
    .param p1, "stringText"    # Ljava/lang/String;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->stringText:Ljava/lang/String;

    .line 180
    return-void
.end method

.method public setSpRecord(Lorg/apache/poi/ddf/EscherSpRecord;)V
    .locals 0
    .param p1, "spRecord"    # Lorg/apache/poi/ddf/EscherSpRecord;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->escherSpRecord:Lorg/apache/poi/ddf/EscherSpRecord;

    .line 47
    return-void
.end method

.method public setStrokeStatus(Ljava/lang/String;)V
    .locals 1
    .param p1, "status"    # Ljava/lang/String;

    .prologue
    .line 24
    if-eqz p1, :cond_0

    const-string/jumbo v0, "f"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->strokeStatus:Z

    .line 26
    :cond_0
    return-void
.end method
