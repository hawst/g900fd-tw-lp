.class public Lcom/samsung/thumbnail/office/ooxml/diagram/XDocDiagramParser;
.super Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;
.source "XDocDiagramParser.java"


# instance fields
.field dataModel:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

.field docType:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

.field theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

.field xwpfDiagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V
    .locals 0
    .param p1, "xwpfDiagram"    # Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;
    .param p2, "dataModel"    # Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;
    .param p3, "theme"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    .param p4, "docType"    # Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;-><init>(Ljava/lang/Object;)V

    .line 24
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XDocDiagramParser;->xwpfDiagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    .line 25
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XDocDiagramParser;->dataModel:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

    .line 26
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XDocDiagramParser;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .line 27
    iput-object p4, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XDocDiagramParser;->docType:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    .line 28
    return-void
.end method


# virtual methods
.method public createRootElementHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    .locals 6

    .prologue
    .line 32
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDrawingHandler;

    const/16 v1, 0xcb

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XDocDiagramParser;->xwpfDiagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XDocDiagramParser;->dataModel:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XDocDiagramParser;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XDocDiagramParser;->docType:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-direct/range {v0 .. v5}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDrawingHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    return-object v0
.end method

.method public getMainNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .locals 1

    .prologue
    .line 38
    const/16 v0, 0xcb

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/diagram/XDocDiagramParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v0

    return-object v0
.end method
