.class public Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
.super Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;
.source "XSLFSlideMaster.java"


# instance fields
.field protected bodyStyles:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;",
            ">;"
        }
    .end annotation
.end field

.field private layouts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;",
            ">;"
        }
    .end annotation
.end field

.field protected otherStyles:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;",
            ">;"
        }
    .end annotation
.end field

.field protected titleStyles:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;-><init>()V

    .line 67
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->init()V

    .line 68
    return-void
.end method

.method protected constructor <init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V
    .locals 0
    .param p1, "part"    # Lorg/apache/poi/openxml4j/opc/PackagePart;
    .param p2, "rel"    # Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;-><init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V

    .line 73
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->init()V

    .line 74
    return-void
.end method

.method private init()V
    .locals 1

    .prologue
    .line 87
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->titleStyles:Ljava/util/Map;

    .line 88
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->bodyStyles:Ljava/util/Map;

    .line 89
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->otherStyles:Ljava/util/Map;

    .line 90
    return-void
.end method


# virtual methods
.method public addStyle(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;)V
    .locals 1
    .param p1, "styleType"    # Ljava/lang/String;
    .param p2, "level"    # Ljava/lang/String;
    .param p3, "paraProp"    # Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;

    .prologue
    .line 180
    const-string/jumbo v0, "titleStyle"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->titleStyles:Ljava/util/Map;

    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    :goto_0
    return-void

    .line 182
    :cond_0
    const-string/jumbo v0, "bodyStyle"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 183
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->bodyStyles:Ljava/util/Map;

    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 185
    :cond_1
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->otherStyles:Ljava/util/Map;

    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public getBodyStyle(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;
    .locals 1
    .param p1, "level"    # Ljava/lang/String;

    .prologue
    .line 194
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->bodyStyles:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;

    return-object v0
.end method

.method public getLayout(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;
    .locals 5
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 123
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->layouts:Ljava/util/Map;

    if-nez v3, :cond_1

    .line 124
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->layouts:Ljava/util/Map;

    .line 125
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getRelations()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/POIXMLDocumentPart;

    .line 126
    .local v2, "p":Lorg/apache/poi/POIXMLDocumentPart;
    instance-of v3, v2, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    if-eqz v3, :cond_0

    move-object v1, v2

    .line 127
    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    .line 128
    .local v1, "layout":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->layouts:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 132
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "layout":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;
    .end local v2    # "p":Lorg/apache/poi/POIXMLDocumentPart;
    :cond_1
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->layouts:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    return-object v3
.end method

.method public getOtherStyle(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;
    .locals 1
    .param p1, "level"    # Ljava/lang/String;

    .prologue
    .line 198
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->otherStyles:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;

    return-object v0
.end method

.method public getParentShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;[Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;)Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    .locals 6
    .param p1, "placeHolder"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;
    .param p2, "shapes"    # [Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;

    .prologue
    .line 159
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p2

    if-ge v1, v2, :cond_3

    .line 160
    aget-object v2, p2, v1

    instance-of v2, v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    if-eqz v2, :cond_0

    .line 161
    aget-object v0, p2, v1

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    .line 162
    .local v0, "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v2

    if-nez v2, :cond_1

    .line 159
    .end local v0    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 165
    .restart local v0    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    :cond_1
    iget-object v2, p1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;->phType:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    if-eqz v2, :cond_2

    iget-object v2, p1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;->phType:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;->phType:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    if-ne v2, v3, :cond_2

    .line 175
    .end local v0    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    :goto_1
    return-object v0

    .line 170
    .restart local v0    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    :cond_2
    iget-wide v2, p1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;->phIndex:J

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v4

    iget-wide v4, v4, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;->phIndex:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    goto :goto_1

    .line 175
    .end local v0    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected getRootElementName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    const-string/jumbo v0, "sldMaster"

    return-object v0
.end method

.method public getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    .locals 3

    .prologue
    .line 136
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    if-nez v2, :cond_1

    .line 137
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getRelations()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/POIXMLDocumentPart;

    .line 138
    .local v1, "p":Lorg/apache/poi/POIXMLDocumentPart;
    instance-of v2, v1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    if-eqz v2, :cond_0

    .line 139
    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .end local v1    # "p":Lorg/apache/poi/POIXMLDocumentPart;
    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .line 140
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->parseTheme()V

    .line 145
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    return-object v2
.end method

.method public getTitleStyle(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;
    .locals 1
    .param p1, "level"    # Ljava/lang/String;

    .prologue
    .line 190
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->titleStyles:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;

    return-object v0
.end method

.method public parseSlideMaster()V
    .locals 7

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .line 94
    move-object v2, p0

    .line 96
    .local v2, "slideMaster":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;
    const/4 v1, 0x0

    .line 101
    .local v1, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 102
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/pptx/XPPTMasterSlideParser;

    invoke-direct {v3, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XPPTMasterSlideParser;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v3, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XPPTMasterSlideParser;->parse(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    if-eqz v1, :cond_0

    .line 108
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 109
    :catch_0
    move-exception v0

    .line 110
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 103
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 104
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_2
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 106
    if-eqz v1, :cond_0

    .line 108
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 109
    :catch_2
    move-exception v0

    .line 110
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 106
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 108
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 111
    :cond_1
    :goto_1
    throw v3

    .line 109
    :catch_3
    move-exception v0

    .line 110
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v4, "DocumentService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
