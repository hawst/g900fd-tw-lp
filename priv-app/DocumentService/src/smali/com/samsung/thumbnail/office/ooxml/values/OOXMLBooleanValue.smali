.class public Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;
.super Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;
.source "OOXMLBooleanValue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue$1;
    }
.end annotation


# instance fields
.field private booleanVal:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "booleanVal"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;-><init>()V

    .line 13
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;->parseString(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;->booleanVal:Z

    .line 14
    return-void
.end method

.method private parseString(Ljava/lang/String;)Z
    .locals 5
    .param p1, "booleanVal"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 21
    if-nez p1, :cond_0

    .line 46
    :goto_0
    :pswitch_0
    return v1

    .line 24
    :cond_0
    invoke-static {p1}, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    move-result-object v0

    .line 26
    .local v0, "onOff":Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;
    if-eqz v0, :cond_1

    .line 28
    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$util$EOnOff:[I

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    move v1, v2

    .line 42
    goto :goto_0

    :pswitch_1
    move v1, v2

    .line 32
    goto :goto_0

    :pswitch_2
    move v1, v2

    .line 36
    goto :goto_0

    :pswitch_3
    move v1, v2

    .line 40
    goto :goto_0

    :cond_1
    move v1, v2

    .line 46
    goto :goto_0

    .line 28
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public getValue()Z
    .locals 1

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;->booleanVal:Z

    return v0
.end method
