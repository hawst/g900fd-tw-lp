.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderLeftHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelStyleBorderLeftHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 22
    const-string/jumbo v1, "left"

    invoke-direct {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 24
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;-><init>()V

    .line 25
    .local v0, "colorHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderLeftHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "color"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    return-void
.end method


# virtual methods
.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 56
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    const-string/jumbo v1, "borderStyleLeft"

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->setStyle(Ljava/lang/String;)V

    .line 58
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 60
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 6
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    const/4 v5, 0x0

    .line 33
    const-string/jumbo v2, "style"

    invoke-interface {p3, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "style":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    const-string/jumbo v3, "borderStyleLeft"

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->setStyle(Ljava/lang/String;)V

    .line 35
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v1

    .line 37
    .local v1, "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    if-eqz v0, :cond_0

    .line 38
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v3, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tblbordercount:I

    invoke-virtual {v2, v0, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellLeftBorderstyle(Ljava/lang/String;I)V

    .line 51
    :goto_0
    return-void

    .line 41
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    const-string/jumbo v3, "nil"

    iget v4, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tblbordercount:I

    invoke-virtual {v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellLeftBorderstyle(Ljava/lang/String;I)V

    .line 43
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v3, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tblbordercount:I

    invoke-virtual {v2, v5, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellLeftBorderRGBColor(Ljava/lang/String;I)V

    .line 45
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v3, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tblbordercount:I

    invoke-virtual {v2, v5, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellLeftBorderThemeColor(Ljava/lang/String;I)V

    goto :goto_0
.end method
