.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelCellStylesHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelCellStylesHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 18
    const-string/jumbo v1, "cellStyles"

    invoke-direct {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 20
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelCellStyleHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelCellStyleHandler;-><init>()V

    .line 21
    .local v0, "cellstyleHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelCellStyleHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelCellStylesHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "cellStyle"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 31
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 33
    return-void
.end method
