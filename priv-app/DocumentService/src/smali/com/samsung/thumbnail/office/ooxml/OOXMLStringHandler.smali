.class public Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLValueHandler;
.source "OOXMLStringHandler.java"


# instance fields
.field private valueConsumer:Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V
    .locals 0
    .param p1, "elementName"    # Ljava/lang/String;
    .param p2, "valueConsumer"    # Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLValueHandler;-><init>(Ljava/lang/String;)V

    .line 17
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;->valueConsumer:Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;

    .line 18
    return-void
.end method


# virtual methods
.method public handleValue(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 22
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    invoke-direct {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;-><init>(Ljava/lang/String;)V

    .line 23
    .local v0, "stringValue":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;->valueConsumer:Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;

    invoke-interface {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;->consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V

    .line 24
    return-void
.end method
