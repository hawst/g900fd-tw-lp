.class public abstract Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathSegmentsPointsHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "PathSegmentsPointsHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler$IPointConsumer;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V
    .locals 5
    .param p1, "elementName"    # Ljava/lang/String;
    .param p2, "pathDesc"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    .prologue
    const/16 v4, 0x1f

    .line 14
    invoke-direct {p0, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 17
    const/4 v3, 0x1

    new-array v2, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 19
    .local v2, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler;

    const-string/jumbo v3, "pt"

    invoke-direct {v0, v3, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler$IPointConsumer;)V

    .line 21
    .local v0, "ptHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    const-string/jumbo v3, "pt"

    invoke-direct {v1, v4, v3, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 23
    .local v1, "ptSeq":Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    const/4 v3, 0x0

    aput-object v1, v2, v3

    .line 25
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathSegmentsPointsHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 26
    return-void
.end method


# virtual methods
.method public abstract point([Ljava/lang/String;)V
.end method

.method public setPoint(JJ)V
    .locals 5
    .param p1, "x"    # J
    .param p3, "y"    # J

    .prologue
    .line 33
    const/4 v3, 0x2

    new-array v0, v3, [Ljava/lang/String;

    .line 34
    .local v0, "valuesArray":[Ljava/lang/String;
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 35
    .local v1, "xVal":Ljava/lang/String;
    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 37
    .local v2, "yVal":Ljava/lang/String;
    const/4 v3, 0x0

    aput-object v1, v0, v3

    .line 38
    const/4 v3, 0x1

    aput-object v2, v0, v3

    .line 40
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathSegmentsPointsHandler;->point([Ljava/lang/String;)V

    .line 42
    return-void
.end method
