.class public Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartScatterChartHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DMLChartScatterChartHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 24
    const/16 v0, 0x23

    const-string/jumbo v1, "scatterChart"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 25
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    .line 28
    const/4 v3, 0x1

    new-array v0, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 30
    .local v0, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerHandler;-><init>()V

    .line 31
    .local v1, "serHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerHandler;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v3, "ser"

    invoke-direct {v2, v3, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 33
    .local v2, "serSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/4 v3, 0x0

    aput-object v2, v0, v3

    .line 35
    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartScatterChartHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 36
    return-void
.end method


# virtual methods
.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartScatterChartHandler;->getExpectedNS(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v2

    invoke-static {p2, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartScatterChartHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)Ljava/lang/String;

    move-result-object v0

    .line 59
    .local v0, "elementName":Ljava/lang/String;
    const-string/jumbo v2, "scatterStyle"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    if-eqz v2, :cond_0

    .line 61
    const-string/jumbo v2, "val"

    invoke-virtual {p0, p3, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartScatterChartHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 62
    .local v1, "val":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    invoke-static {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->setScatterStyle(Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;)V

    .line 72
    .end local v1    # "val":Ljava/lang/String;
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    if-eqz v2, :cond_2

    .line 78
    :goto_1
    return-void

    .line 64
    :cond_0
    const-string/jumbo v2, "scatterStyle"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    if-eqz v2, :cond_1

    .line 66
    const-string/jumbo v2, "val"

    invoke-virtual {p0, p3, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartScatterChartHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 67
    .restart local v1    # "val":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-static {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->setScatterStyle(Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;)V

    goto :goto_0

    .line 70
    .end local v1    # "val":Ljava/lang/String;
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    goto :goto_0

    .line 76
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    const-string/jumbo v3, "Scatter"

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->addExChartName(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    const/4 v1, 0x6

    .line 41
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 42
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartScatterChartHandler;->init()V

    .line 43
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    if-eqz v0, :cond_0

    .line 44
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->setChartType(I)V

    .line 48
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    if-eqz v0, :cond_1

    .line 49
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->setChartType(I)V

    .line 52
    :cond_1
    return-void
.end method
