.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPContent;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XDocPContent.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/IFldSimpleObserver;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "elementName"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 19
    return-void
.end method


# virtual methods
.method protected init()V
    .locals 6

    .prologue
    .line 22
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRunHandler;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRunHandler;-><init>()V

    .line 23
    .local v2, "runHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRunHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPContent;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "r"

    invoke-virtual {v4, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocHyperlinkHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocHyperlinkHandler;-><init>()V

    .line 25
    .local v1, "hyperlinkHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocHyperlinkHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPContent;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "hyperlink"

    invoke-virtual {v4, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;

    const/16 v4, 0x65

    invoke-direct {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;-><init>(I)V

    .line 27
    .local v3, "sdtHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPContent;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "sdt"

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocFldSimpleHandler;

    invoke-direct {v0, p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocFldSimpleHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/IFldSimpleObserver;)V

    .line 29
    .local v0, "fldSample":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocFldSimpleHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPContent;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "fldSimple"

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    return-void
.end method

.method public setFldSimple(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p1, "instr"    # Ljava/lang/String;
    .param p2, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 41
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->setFldSimple(Ljava/lang/String;)V

    .line 42
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 35
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 36
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPContent;->init()V

    .line 37
    return-void
.end method
