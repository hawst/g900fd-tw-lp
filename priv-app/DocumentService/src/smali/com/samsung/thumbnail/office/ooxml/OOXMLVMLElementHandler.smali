.class public abstract Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "OOXMLVMLElementHandler.java"


# instance fields
.field protected shape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

.field protected shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "expectedNameSpaceId"    # I
    .param p2, "elementName"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "elementName"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 23
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 111
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 112
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;

    invoke-interface {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;->endShape()V

    .line 113
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 20
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 32
    invoke-super/range {p0 .. p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 33
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v8

    .line 34
    .local v8, "eleName":Ljava/lang/String;
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-static {v8}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->parseStringPrefix(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    .line 35
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-object/from16 v19, v0

    invoke-direct/range {v18 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->shape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    .line 37
    const-string/jumbo v18, "id"

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 38
    .local v16, "val":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->shape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->setId(Ljava/lang/String;)V

    .line 40
    const-string/jumbo v18, "style"

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 41
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setStyle(Ljava/lang/String;)V

    .line 44
    const-wide/16 v14, 0x0

    .line 45
    .local v14, "shapeZOrder":J
    if-eqz v16, :cond_0

    .line 46
    const-string/jumbo v18, ";"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 47
    .local v13, "styleArray":[Ljava/lang/String;
    move-object v5, v13

    .local v5, "arr$":[Ljava/lang/String;
    array-length v12, v5

    .local v12, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_0
    if-ge v10, v12, :cond_0

    aget-object v17, v5, v10

    .line 48
    .local v17, "value":Ljava/lang/String;
    if-eqz v17, :cond_4

    .line 49
    const/16 v18, 0x0

    const/16 v19, 0x3a

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v19

    invoke-virtual/range {v17 .. v19}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    .line 50
    .local v11, "key":Ljava/lang/String;
    const-string/jumbo v18, "z-index"

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 51
    const/16 v18, 0x3a

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->indexOf(I)I

    move-result v18

    add-int/lit8 v18, v18, 0x1

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    .line 52
    invoke-static/range {v16 .. v16}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    .line 59
    .end local v5    # "arr$":[Ljava/lang/String;
    .end local v10    # "i$":I
    .end local v11    # "key":Ljava/lang/String;
    .end local v12    # "len$":I
    .end local v13    # "styleArray":[Ljava/lang/String;
    .end local v17    # "value":Ljava/lang/String;
    :cond_0
    const-string/jumbo v18, "fillcolor"

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 60
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setFillColor(Ljava/lang/String;)V

    .line 62
    const-string/jumbo v18, "filled"

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 63
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setFilledStatus(Ljava/lang/String;)V

    .line 65
    const-string/jumbo v18, "stroked"

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 66
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setStrokeStatus(Ljava/lang/String;)V

    .line 68
    const-string/jumbo v18, "o:hralign"

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 69
    if-eqz v16, :cond_1

    .line 70
    const-string/jumbo v18, "center"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 71
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->CENTERED:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setHrAlign(Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;)V

    .line 78
    :cond_1
    :goto_1
    const-string/jumbo v18, "adj"

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 79
    if-eqz v16, :cond_8

    .line 80
    const/4 v6, 0x1

    .line 81
    .local v6, "count":I
    :goto_2
    const-string/jumbo v18, ","

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_7

    .line 82
    const/4 v4, 0x0

    .line 83
    .local v4, "adj":Ljava/lang/String;
    const/16 v18, 0x2c

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v18

    if-eqz v18, :cond_2

    .line 84
    const/16 v18, 0x0

    const/16 v19, 0x2c

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v19

    move-object/from16 v0, v16

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 86
    :cond_2
    const/4 v9, 0x0

    .line 87
    .local v9, "i":I
    if-eqz v4, :cond_3

    .line 89
    :try_start_0
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    .line 93
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v9, v6}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setAdj(II)V

    .line 96
    :cond_3
    const/16 v18, 0x2c

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v18

    add-int/lit8 v18, v18, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    .line 97
    add-int/lit8 v6, v6, 0x1

    .line 98
    goto :goto_2

    .line 47
    .end local v4    # "adj":Ljava/lang/String;
    .end local v6    # "count":I
    .end local v9    # "i":I
    .restart local v5    # "arr$":[Ljava/lang/String;
    .restart local v10    # "i$":I
    .restart local v12    # "len$":I
    .restart local v13    # "styleArray":[Ljava/lang/String;
    .restart local v17    # "value":Ljava/lang/String;
    :cond_4
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 72
    .end local v5    # "arr$":[Ljava/lang/String;
    .end local v10    # "i$":I
    .end local v12    # "len$":I
    .end local v13    # "styleArray":[Ljava/lang/String;
    .end local v17    # "value":Ljava/lang/String;
    :cond_5
    const-string/jumbo v18, "left"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_6

    .line 73
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->LEFT:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setHrAlign(Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;)V

    goto/16 :goto_1

    .line 74
    :cond_6
    const-string/jumbo v18, "right"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 75
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->RIGHT:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setHrAlign(Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;)V

    goto/16 :goto_1

    .line 90
    .restart local v4    # "adj":Ljava/lang/String;
    .restart local v6    # "count":I
    .restart local v9    # "i":I
    :catch_0
    move-exception v7

    .line 91
    .local v7, "e":Ljava/lang/NumberFormatException;
    const/4 v9, 0x0

    goto :goto_3

    .line 100
    .end local v4    # "adj":Ljava/lang/String;
    .end local v7    # "e":Ljava/lang/NumberFormatException;
    .end local v9    # "i":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-object/from16 v18, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v1, v6}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setAdj(II)V

    .line 103
    .end local v6    # "count":I
    :cond_8
    const-string/jumbo v18, "path"

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 104
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setPath(Ljava/lang/String;)V

    .line 106
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->shape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-interface {v0, v1, v14, v15}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;->addShape(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;J)V

    .line 107
    return-void
.end method
