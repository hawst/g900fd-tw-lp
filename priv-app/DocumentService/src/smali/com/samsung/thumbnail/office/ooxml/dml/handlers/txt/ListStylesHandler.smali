.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "ListStylesHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;


# instance fields
.field private textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "nsID"    # I
    .param p2, "ele"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;->init()V

    .line 28
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "ele"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;->init()V

    .line 33
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;)V
    .locals 1
    .param p1, "ele"    # Ljava/lang/String;
    .param p2, "textShape"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    .prologue
    .line 36
    const/16 v0, 0x1f

    invoke-direct {p0, v0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 37
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    .line 38
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;->init()V

    .line 39
    return-void
.end method

.method private init()V
    .locals 27

    .prologue
    .line 42
    const/16 v25, 0xa

    move/from16 v0, v25

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    move-object/from16 v24, v0

    .line 44
    .local v24, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    move-object/from16 v25, v0

    if-nez v25, :cond_1

    .line 45
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;

    const-string/jumbo v25, "defPPr"

    const/16 v26, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, p0

    invoke-direct {v5, v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;)V

    .line 47
    .local v5, "defPPRhandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v25, "defPPr"

    move-object/from16 v0, v25

    invoke-direct {v4, v0, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 49
    .local v4, "defPPRSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v25, 0x0

    aput-object v4, v24, v25

    .line 51
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;

    const-string/jumbo v25, "lvl1pPr"

    const/16 v26, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, p0

    invoke-direct {v7, v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;)V

    .line 53
    .local v7, "lvl1handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v25, "lvl1pPr"

    move-object/from16 v0, v25

    invoke-direct {v6, v0, v7}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 55
    .local v6, "lvl1Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v25, 0x1

    aput-object v6, v24, v25

    .line 57
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;

    const-string/jumbo v25, "lvl2pPr"

    const/16 v26, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, p0

    invoke-direct {v9, v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;)V

    .line 59
    .local v9, "lvl2handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v25, "lvl2pPr"

    move-object/from16 v0, v25

    invoke-direct {v8, v0, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 61
    .local v8, "lvl2Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v25, 0x2

    aput-object v8, v24, v25

    .line 63
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;

    const-string/jumbo v25, "lvl3pPr"

    const/16 v26, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, p0

    invoke-direct {v11, v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;)V

    .line 65
    .local v11, "lvl3handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v25, "lvl3pPr"

    move-object/from16 v0, v25

    invoke-direct {v10, v0, v11}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 67
    .local v10, "lvl3Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v25, 0x3

    aput-object v10, v24, v25

    .line 69
    new-instance v13, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;

    const-string/jumbo v25, "lvl4pPr"

    const/16 v26, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, p0

    invoke-direct {v13, v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;)V

    .line 71
    .local v13, "lvl4handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v25, "lvl4pPr"

    move-object/from16 v0, v25

    invoke-direct {v12, v0, v13}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 73
    .local v12, "lvl4Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v25, 0x4

    aput-object v12, v24, v25

    .line 75
    new-instance v15, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;

    const-string/jumbo v25, "lvl5pPr"

    const/16 v26, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, p0

    invoke-direct {v15, v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;)V

    .line 77
    .local v15, "lvl5handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    new-instance v14, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v25, "lvl5pPr"

    move-object/from16 v0, v25

    invoke-direct {v14, v0, v15}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 79
    .local v14, "lvl5Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v25, 0x5

    aput-object v14, v24, v25

    .line 81
    new-instance v17, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;

    const-string/jumbo v25, "lvl6pPr"

    const/16 v26, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    move-object/from16 v3, p0

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;)V

    .line 83
    .local v17, "lvl6handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    new-instance v16, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v25, "lvl6pPr"

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 85
    .local v16, "lvl6Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v25, 0x6

    aput-object v16, v24, v25

    .line 87
    new-instance v19, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;

    const-string/jumbo v25, "lvl7pPr"

    const/16 v26, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    move-object/from16 v3, p0

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;)V

    .line 89
    .local v19, "lvl7handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v25, "lvl7pPr"

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 91
    .local v18, "lvl7Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v25, 0x7

    aput-object v18, v24, v25

    .line 93
    new-instance v21, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;

    const-string/jumbo v25, "lvl8pPr"

    const/16 v26, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    move-object/from16 v3, p0

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;)V

    .line 95
    .local v21, "lvl8handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    new-instance v20, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v25, "lvl8pPr"

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 97
    .local v20, "lvl8Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v25, 0x8

    aput-object v20, v24, v25

    .line 99
    new-instance v23, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;

    const-string/jumbo v25, "lvl9pPr"

    const/16 v26, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    move-object/from16 v3, p0

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;)V

    .line 101
    .local v23, "lvl9handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    new-instance v22, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v25, "lvl9pPr"

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 103
    .local v22, "lvl9Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v25, 0x9

    aput-object v22, v24, v25

    .line 167
    .end local v4    # "defPPRSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    .end local v5    # "defPPRhandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    .end local v6    # "lvl1Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    .end local v7    # "lvl1handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    .end local v8    # "lvl2Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    .end local v9    # "lvl2handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    .end local v10    # "lvl3Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    .end local v11    # "lvl3handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    .end local v12    # "lvl4Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    .end local v13    # "lvl4handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    .end local v14    # "lvl5Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    .end local v15    # "lvl5handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    .end local v16    # "lvl6Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    .end local v17    # "lvl6handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    .end local v18    # "lvl7Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    .end local v19    # "lvl7handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    .end local v20    # "lvl8Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    .end local v21    # "lvl8handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    .end local v22    # "lvl9Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    .end local v23    # "lvl9handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    :cond_0
    :goto_0
    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 168
    return-void

    .line 104
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    move-object/from16 v25, v0

    if-eqz v25, :cond_0

    .line 105
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;

    const-string/jumbo v25, "defPPr"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, p0

    invoke-direct {v5, v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;)V

    .line 107
    .restart local v5    # "defPPRhandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v25, "defPPr"

    move-object/from16 v0, v25

    invoke-direct {v4, v0, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 109
    .restart local v4    # "defPPRSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v25, 0x0

    aput-object v4, v24, v25

    .line 111
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;

    const-string/jumbo v25, "lvl1pPr"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, p0

    invoke-direct {v7, v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;)V

    .line 113
    .restart local v7    # "lvl1handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v25, "lvl1pPr"

    move-object/from16 v0, v25

    invoke-direct {v6, v0, v7}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 115
    .restart local v6    # "lvl1Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v25, 0x1

    aput-object v6, v24, v25

    .line 117
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;

    const-string/jumbo v25, "lvl2pPr"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, p0

    invoke-direct {v9, v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;)V

    .line 119
    .restart local v9    # "lvl2handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v25, "lvl2pPr"

    move-object/from16 v0, v25

    invoke-direct {v8, v0, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 121
    .restart local v8    # "lvl2Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v25, 0x2

    aput-object v8, v24, v25

    .line 123
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;

    const-string/jumbo v25, "lvl3pPr"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, p0

    invoke-direct {v11, v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;)V

    .line 125
    .restart local v11    # "lvl3handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v25, "lvl3pPr"

    move-object/from16 v0, v25

    invoke-direct {v10, v0, v11}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 127
    .restart local v10    # "lvl3Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v25, 0x3

    aput-object v10, v24, v25

    .line 129
    new-instance v13, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;

    const-string/jumbo v25, "lvl4pPr"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, p0

    invoke-direct {v13, v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;)V

    .line 131
    .restart local v13    # "lvl4handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v25, "lvl4pPr"

    move-object/from16 v0, v25

    invoke-direct {v12, v0, v13}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 133
    .restart local v12    # "lvl4Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v25, 0x4

    aput-object v12, v24, v25

    .line 135
    new-instance v15, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;

    const-string/jumbo v25, "lvl5pPr"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, p0

    invoke-direct {v15, v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;)V

    .line 137
    .restart local v15    # "lvl5handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    new-instance v14, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v25, "lvl5pPr"

    move-object/from16 v0, v25

    invoke-direct {v14, v0, v15}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 139
    .restart local v14    # "lvl5Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v25, 0x5

    aput-object v14, v24, v25

    .line 141
    new-instance v17, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;

    const-string/jumbo v25, "lvl6pPr"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v26

    move-object/from16 v0, v17

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    move-object/from16 v3, p0

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;)V

    .line 143
    .restart local v17    # "lvl6handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    new-instance v16, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v25, "lvl6pPr"

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 145
    .restart local v16    # "lvl6Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v25, 0x6

    aput-object v16, v24, v25

    .line 147
    new-instance v19, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;

    const-string/jumbo v25, "lvl7pPr"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v26

    move-object/from16 v0, v19

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    move-object/from16 v3, p0

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;)V

    .line 149
    .restart local v19    # "lvl7handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v25, "lvl7pPr"

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 151
    .restart local v18    # "lvl7Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v25, 0x7

    aput-object v18, v24, v25

    .line 153
    new-instance v21, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;

    const-string/jumbo v25, "lvl8pPr"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v26

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    move-object/from16 v3, p0

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;)V

    .line 155
    .restart local v21    # "lvl8handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    new-instance v20, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v25, "lvl8pPr"

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 157
    .restart local v20    # "lvl8Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v25, 0x8

    aput-object v20, v24, v25

    .line 159
    new-instance v23, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;

    const-string/jumbo v25, "lvl9pPr"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v26

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    move-object/from16 v3, p0

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;)V

    .line 161
    .restart local v23    # "lvl9handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    new-instance v22, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v25, "lvl9pPr"

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 163
    .restart local v22    # "lvl9Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v25, 0x9

    aput-object v22, v24, v25

    goto/16 :goto_0
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 172
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 173
    return-void
.end method

.method public setPropertiesForLvl(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 3
    .param p1, "level"    # Ljava/lang/String;
    .param p2, "paraProps"    # Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;
    .param p3, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;->getElementName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "defaultTextStyle"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 179
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    invoke-virtual {v1, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->addDefTextStyle(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;)V

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    .line 185
    .local v0, "slide":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;
    instance-of v1, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    if-eqz v1, :cond_3

    .line 186
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;->getElementName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "lstStyle"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 187
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    if-eqz v1, :cond_0

    .line 188
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    invoke-virtual {v1, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;->addLstStyle(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;)V

    goto :goto_0

    .line 191
    :cond_2
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    .end local v0    # "slide":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;->getElementName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->addStyle(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;)V

    goto :goto_0

    .line 194
    .restart local v0    # "slide":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;
    :cond_3
    instance-of v1, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    if-eqz v1, :cond_0

    .line 195
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    if-eqz v1, :cond_0

    .line 196
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    invoke-virtual {v1, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;->addLstStyle(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;)V

    goto :goto_0
.end method
