.class public Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;
.super Ljava/lang/Object;
.source "EMR_PolylineTo16.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;


# instance fields
.field private Size:I

.field private Type:I

.field private aPoints:[B

.field private arrayOfPoint:[Landroid/graphics/Point;

.field private bottom:I

.field private count:I

.field private emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

.field fileContent:[B

.field private left:I

.field private localPath:Landroid/graphics/Path;

.field private offset:I

.field private right:I

.field private top:I


# direct methods
.method public constructor <init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V
    .locals 1
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I
    .param p3, "emrGlobals"    # Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .prologue
    const/4 v0, 0x0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->localPath:Landroid/graphics/Path;

    .line 61
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->arrayOfPoint:[Landroid/graphics/Point;

    .line 75
    iput-object p3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .line 76
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->fileContent:[B

    .line 77
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    .line 78
    return-void
.end method


# virtual methods
.method public getBottom()I
    .locals 1

    .prologue
    .line 200
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->bottom:I

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->count:I

    return v0
.end method

.method public getLeft()I
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->left:I

    return v0
.end method

.method public getLocalPath()Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->localPath:Landroid/graphics/Path;

    return-object v0
.end method

.method public getRecordSize()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->Size:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->Type:I

    return v0
.end method

.method public getRight()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->right:I

    return v0
.end method

.method public getTop()I
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->top:I

    return v0
.end method

.method public getaPoints()[B
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->aPoints:[B

    return-object v0
.end method

.method public prepare()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v8, 0x0

    .line 367
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->getaPoints()[B

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->setPoints([B)V

    .line 368
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->arrayOfPoint:[Landroid/graphics/Point;

    array-length v7, v7

    if-gt v7, v10, :cond_0

    .line 416
    :goto_0
    return-void

    .line 371
    :cond_0
    new-instance v7, Landroid/graphics/Path;

    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    iput-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->localPath:Landroid/graphics/Path;

    .line 373
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v7, v7, v8

    iget v7, v7, Landroid/graphics/Point;->x:I

    int-to-float v0, v7

    .line 374
    .local v0, "f1":F
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v7, v7, v8

    iget v7, v7, Landroid/graphics/Point;->y:I

    int-to-float v1, v7

    .line 375
    .local v1, "f2":F
    const/4 v2, 0x0

    .line 376
    .local v2, "f3":F
    const/4 v3, 0x0

    .line 378
    .local v3, "f4":F
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->localPath:Landroid/graphics/Path;

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->currentX:F

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v9, v9, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->currentY:F

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 379
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->localPath:Landroid/graphics/Path;

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v8, v0, v8

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v9, v9, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v9, v1, v9

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 381
    const/4 v4, 0x1

    .line 383
    .local v4, "i":I
    :goto_1
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->arrayOfPoint:[Landroid/graphics/Point;

    array-length v5, v7

    .line 384
    .local v5, "j":I
    if-lt v4, v5, :cond_3

    .line 392
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    if-eqz v7, :cond_2

    .line 393
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->pathStart:I

    if-ne v7, v10, :cond_1

    .line 394
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->currentX:F

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v9, v9, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->currentY:F

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 396
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->pathStart:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->pathStart:I

    .line 399
    :cond_1
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v8, v0, v8

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v9, v9, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v9, v1, v9

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 402
    const/4 v6, 0x1

    .line 404
    .local v6, "k":I
    :goto_2
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->arrayOfPoint:[Landroid/graphics/Point;

    array-length v5, v7

    .line 405
    if-lt v6, v5, :cond_4

    .line 415
    .end local v6    # "k":I
    :cond_2
    invoke-virtual {p0, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->setCurrentXY(FF)V

    goto :goto_0

    .line 386
    :cond_3
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v7, v7, v4

    iget v7, v7, Landroid/graphics/Point;->x:I

    int-to-float v2, v7

    .line 387
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v7, v7, v4

    iget v7, v7, Landroid/graphics/Point;->y:I

    int-to-float v3, v7

    .line 388
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->localPath:Landroid/graphics/Path;

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v8, v2, v8

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v9, v9, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v9, v3, v9

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 389
    add-int/lit8 v4, v4, 0x1

    .line 390
    goto :goto_1

    .line 407
    .restart local v6    # "k":I
    :cond_4
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v7, v7, v6

    iget v7, v7, Landroid/graphics/Point;->x:I

    int-to-float v2, v7

    .line 408
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v7, v7, v6

    iget v7, v7, Landroid/graphics/Point;->y:I

    int-to-float v3, v7

    .line 409
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v8, v2, v8

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v9, v9, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v9, v3, v9

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 411
    add-int/lit8 v6, v6, 0x1

    .line 412
    goto :goto_2
.end method

.method public readPolylineTo16()V
    .locals 14

    .prologue
    .line 236
    const/4 v5, 0x0

    .line 238
    .local v5, "i":I
    const/4 v11, 0x4

    new-array v6, v11, [B

    .line 240
    .local v6, "intConvert":[B
    const/4 v5, 0x0

    :goto_0
    const/4 v11, 0x3

    if-gt v5, v11, :cond_0

    .line 241
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 240
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 242
    :cond_0
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 243
    .local v1, "Type":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    .line 244
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->setRecordType(I)V

    .line 246
    const/4 v5, 0x0

    :goto_1
    const/4 v11, 0x3

    if-gt v5, v11, :cond_1

    .line 247
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 246
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 248
    :cond_1
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 249
    .local v0, "Size":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    .line 250
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->setRecordSize(I)V

    .line 252
    const/4 v5, 0x0

    :goto_2
    const/4 v11, 0x3

    if-gt v5, v11, :cond_2

    .line 253
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 252
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 254
    :cond_2
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v7

    .line 255
    .local v7, "lef":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    .line 256
    int-to-float v11, v7

    invoke-static {v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->setLeft(I)V

    .line 258
    const/4 v5, 0x0

    :goto_3
    const/4 v11, 0x3

    if-gt v5, v11, :cond_3

    .line 259
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 258
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 260
    :cond_3
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v10

    .line 261
    .local v10, "top":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    .line 262
    int-to-float v11, v10

    invoke-static {v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->setTop(I)V

    .line 264
    const/4 v5, 0x0

    :goto_4
    const/4 v11, 0x3

    if-gt v5, v11, :cond_4

    .line 265
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 264
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 266
    :cond_4
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v9

    .line 267
    .local v9, "right":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    .line 268
    int-to-float v11, v9

    invoke-static {v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->setRight(I)V

    .line 270
    const/4 v5, 0x0

    :goto_5
    const/4 v11, 0x3

    if-gt v5, v11, :cond_5

    .line 271
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 270
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 272
    :cond_5
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v2

    .line 273
    .local v2, "bot":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    .line 274
    int-to-float v11, v2

    invoke-static {v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->setBottom(I)V

    .line 276
    const/4 v5, 0x0

    :goto_6
    const/4 v11, 0x3

    if-gt v5, v11, :cond_6

    .line 277
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 276
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    .line 278
    :cond_6
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v4

    .line 279
    .local v4, "count":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    .line 280
    invoke-virtual {p0, v4}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->setCount(I)V

    .line 282
    add-int/lit8 v3, v0, -0x1c

    .line 283
    .local v3, "buffsize":I
    new-array v8, v3, [B

    .line 284
    .local v8, "pointsBuffer":[B
    const/4 v5, 0x0

    :goto_7
    if-ge v5, v3, :cond_7

    .line 285
    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->fileContent:[B

    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    add-int/2addr v12, v5

    aget-byte v11, v11, v12

    aput-byte v11, v8, v5

    .line 284
    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    .line 287
    :cond_7
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->offset:I

    .line 288
    invoke-virtual {p0, v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->setaPoints([B)V

    .line 289
    return-void
.end method

.method public render(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v5, 0x2

    .line 299
    const/4 v2, 0x0

    .line 300
    .local v2, "i":I
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;-><init>()V

    .line 301
    .local v0, "PaintPolyline16PropertiesObject":Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 303
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 305
    .local v1, "emrSize":I
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    if-nez v3, :cond_3

    .line 306
    if-lez v1, :cond_1

    .line 307
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 308
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandlePen:I

    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->gethandleNumber()I

    move-result v3

    if-ne v4, v3, :cond_4

    .line 313
    :cond_0
    if-ge v2, v1, :cond_1

    .line 314
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getPenStyle()I

    move-result v3

    if-ne v5, v3, :cond_5

    .line 316
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v3

    new-instance v4, Landroid/graphics/DashPathEffect;

    new-array v5, v5, [F

    fill-array-data v5, :array_0

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 326
    :goto_1
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getColor()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/thumbnail/office/emf/Util/toColorAndroid;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 330
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->getLocalPath()Landroid/graphics/Path;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 335
    :cond_1
    if-eq v2, v1, :cond_2

    if-nez v1, :cond_3

    .line 337
    :cond_2
    const/16 v3, 0x8

    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v3, v4, :cond_6

    .line 346
    :cond_3
    :goto_2
    return-void

    .line 307
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 321
    :cond_5
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getPenStyle()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/thumbnail/office/emf/Util/toStyleAndroid;->getStyle(I)Landroid/graphics/Paint$Style;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    goto :goto_1

    .line 340
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->getLocalPath()Landroid/graphics/Path;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_2

    .line 316
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x40400000    # 3.0f
    .end array-data
.end method

.method public setBottom(I)V
    .locals 0
    .param p1, "b"    # I

    .prologue
    .line 210
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->bottom:I

    .line 211
    return-void
.end method

.method public setCount(I)V
    .locals 0
    .param p1, "cnt"    # I

    .prologue
    .line 134
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->count:I

    .line 135
    return-void
.end method

.method public setCurrentXY(FF)V
    .locals 2
    .param p1, "currX"    # F
    .param p2, "currY"    # F

    .prologue
    .line 357
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v1, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v1, p1, v1

    iput v1, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->currentX:F

    .line 358
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v1, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v1, p2, v1

    iput v1, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->currentY:F

    .line 359
    return-void
.end method

.method public setLeft(I)V
    .locals 0
    .param p1, "l"    # I

    .prologue
    .line 153
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->left:I

    .line 154
    return-void
.end method

.method public setPoints([B)V
    .locals 13
    .param p1, "points"    # [B

    .prologue
    const v12, 0xffff

    const/4 v11, 0x1

    const/4 v10, 0x0

    const v9, 0x8000

    .line 434
    const/4 v2, 0x0

    .line 435
    .local v2, "i":I
    const/4 v4, 0x0

    .line 436
    .local v4, "j":I
    const/4 v5, 0x0

    .line 437
    .local v5, "k":I
    const/4 v6, 0x0

    .line 439
    .local v6, "offset":I
    array-length v7, p1

    shr-int/lit8 v7, v7, 0x2

    new-array v7, v7, [Landroid/graphics/Point;

    iput-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->arrayOfPoint:[Landroid/graphics/Point;

    .line 441
    const/4 v7, 0x2

    new-array v3, v7, [B

    .line 443
    .local v3, "intConvert":[B
    :goto_0
    array-length v7, p1

    if-ge v6, v7, :cond_4

    .line 444
    const/4 v2, 0x0

    :goto_1
    if-gt v2, v11, :cond_0

    .line 445
    rsub-int/lit8 v7, v2, 0x1

    add-int v8, v6, v2

    aget-byte v8, p1, v8

    aput-byte v8, v3, v7

    .line 444
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 446
    :cond_0
    invoke-static {v3, v10}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getSmallInt([BI)I

    move-result v0

    .line 447
    .local v0, "X":I
    add-int/2addr v6, v2

    .line 449
    and-int v7, v0, v9

    if-ne v7, v9, :cond_1

    .line 450
    xor-int/lit8 v7, v0, -0x1

    and-int/2addr v7, v12

    add-int/lit8 v7, v7, 0x1

    mul-int/lit8 v0, v7, -0x1

    .line 453
    :cond_1
    const/4 v4, 0x0

    :goto_2
    if-gt v4, v11, :cond_2

    .line 454
    rsub-int/lit8 v7, v4, 0x1

    add-int v8, v6, v4

    aget-byte v8, p1, v8

    aput-byte v8, v3, v7

    .line 453
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 455
    :cond_2
    invoke-static {v3, v10}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getSmallInt([BI)I

    move-result v1

    .line 457
    .local v1, "Y":I
    and-int v7, v1, v9

    if-ne v7, v9, :cond_3

    .line 458
    xor-int/lit8 v7, v1, -0x1

    and-int/2addr v7, v12

    add-int/lit8 v7, v7, 0x1

    mul-int/lit8 v1, v7, -0x1

    .line 461
    :cond_3
    add-int/2addr v6, v4

    .line 462
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->arrayOfPoint:[Landroid/graphics/Point;

    new-instance v8, Landroid/graphics/Point;

    invoke-direct {v8, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    aput-object v8, v7, v5

    .line 464
    add-int/lit8 v5, v5, 0x1

    .line 465
    goto :goto_0

    .line 467
    .end local v0    # "X":I
    .end local v1    # "Y":I
    :cond_4
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "RecrdSize"    # I

    .prologue
    .line 115
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->Size:I

    .line 116
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "RecrdType"    # I

    .prologue
    .line 96
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->Type:I

    .line 97
    return-void
.end method

.method public setRight(I)V
    .locals 0
    .param p1, "r"    # I

    .prologue
    .line 191
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->right:I

    .line 192
    return-void
.end method

.method public setTop(I)V
    .locals 0
    .param p1, "t"    # I

    .prologue
    .line 172
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->top:I

    .line 173
    return-void
.end method

.method public setaPoints([B)V
    .locals 0
    .param p1, "pt"    # [B

    .prologue
    .line 220
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolylineTo16;->aPoints:[B

    .line 221
    return-void
.end method
