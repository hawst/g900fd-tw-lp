.class public Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtCountHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DMLChartPtCountHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 21
    const/16 v0, 0x23

    const-string/jumbo v1, "ptCount"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 22
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 26
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 27
    const-string/jumbo v1, "val"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtCountHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 28
    .local v0, "val":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    if-eqz v1, :cond_1

    .line 37
    :cond_0
    :goto_0
    return-void

    .line 31
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getExChartVAl()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "Num"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    .line 33
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->addExChartNumval(Ljava/lang/String;)V

    goto :goto_0

    .line 34
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getExChartVAl()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "String"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 36
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->addExChartStrnum(Ljava/lang/String;)V

    goto :goto_0
.end method
