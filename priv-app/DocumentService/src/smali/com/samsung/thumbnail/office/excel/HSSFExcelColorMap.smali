.class public Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;
.super Ljava/lang/Object;
.source "HSSFExcelColorMap.java"


# static fields
.field private static final AQUA:I = 0x3

.field private static final AUTOMATIC:I = 0x40

.field private static final BLACK:I = 0x8

.field private static final BLACT_TEXT1_25PRCNT:I = 0x3f

.field private static final BLACT_TEXT1_35PRCNT:I = 0x17

.field private static final BLACT_TEXT1_50PRCNT:I = 0x1c

.field private static final BLUE:I = 0x2

.field private static final BLUE_ACCENT:I = 0xf

.field private static final BLUE_LIGHT:I = 0xa

.field private static final CADET_BLUE:I = 0x31

.field public static final CENTER_ACROSS_SELECTION_ALIGN:I = 0x6

.field public static final CENTER_ALIGN:I = 0x1

.field private static final CHOCLATE:I = 0x35

.field private static final CORN_FLOWER_BLUE:I = 0x1e

.field private static final DARK_BLUE:I = 0x12

.field private static final DARK_SLATE:I = 0x15

.field private static final DARK_TEAL:I = 0x38

.field public static final FILL_ALIGN:I = 0x4

.field private static final GREEN:I = 0xb

.field private static final GREEN_HONEYDEW:I = 0x2a

.field private static final INDIGO:I = 0x3e

.field private static IndexedColorMap:Ljava/util/Map; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final JUSTIFY_ALIGN:I = 0x3

.field public static final JUSTIFY_LEFT_ALIGN:I = 0x5

.field public static final LEFT_ALIGN:I = 0x0

.field private static final LIGHT_BLUE:I = 0x28

.field private static final LIGHT_CORNFLOWER_BLUE:I = 0x1f

.field private static final LIGHT_GRAY:I = 0x16

.field private static final MAROON_4:I = 0xc

.field private static final OLIVE_DRAB:I = 0x39

.field private static final OLIVE_GREEN:I = 0x7

.field private static final PALE_BLUE:I = 0x2c

.field private static final PURPLE:I = 0x5

.field private static final PURPLE_ACCENT_2_40PRCNT:I = 0x24

.field private static final RED_ACCENT_2:I = 0x6

.field private static final RED_ACCENT_2_30PRCNT:I = 0x2d

.field private static final RED_ACCENT_2_40PRCNT:I = 0x1d

.field public static final RIGHT_ALIGN:I = 0x2

.field private static final SADDLE_BROWN:I = 0x10

.field private static final SANDY_BROWN:I = 0x34

.field private static final SIENNA:I = 0x3c

.field private static final SKY_LIGHT_BLUE:I = 0x1b

.field private static final SNOW:I = 0x37

.field private static final SPRING_GREEN:I = 0x11

.field private static final TABLE_CELL:I = 0x33

.field private static final TABLE_HEADER:I = 0x32

.field private static final TAN:I = 0x1a

.field private static final TAN_10PRCNT:I = 0x2b

.field private static final TAN_50PRCNT:I = 0x13

.field private static final TAN_90PRCNT:I = 0x3b

.field private static final THISTLE:I = 0x2e

.field private static final VOILET_RED:I = 0xe

.field private static final WHEAT:I = 0x2f

.field private static final WHITE:I = 0x9

.field private static final YELLOW:I = 0xd

.field private static alignMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static colorMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v0, 0x0

    .line 21
    sput-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    .line 83
    sput-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    .line 84
    sput-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->alignMap:Ljava/util/Map;

    .line 87
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    .line 88
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    invoke-static {v3}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#0000FF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0xf

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#333399"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0xc

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#7D0552"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0xd

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#FFFF00"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0xe

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#d02090"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x3c

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#A0522D"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x11

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#00ff7f"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x1e

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#6495ed"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x10

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#8b4513"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x39

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#6b8e23"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x31

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#5f9ea0"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x35

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#d2691e"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x2e

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#d8bfd8"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x40

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#FFFFFF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x8

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#000000"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x1a

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#EEECE1"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x1b

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#f0ffff"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x2b

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#DDD9C3"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x13

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#948B54"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x3b

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#1D1B13"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x12

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#17375D"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/4 v1, 0x6

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#C0504D"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/4 v1, 0x7

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#9BBB59"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    invoke-static {v4}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#4BACC6"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0xb

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#00AC50"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0xa

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#0070C0"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/4 v1, 0x5

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#7030A0"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x15

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#2f4f4f"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x16

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#d3d3d3"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x37

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#bebebe"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x17

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#5A5A5A"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x1c

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#808080"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x3f

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#404040"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x1f

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#C5D9F1"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x3e

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#4B0082"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x38

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#1F497D"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x1d

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#D99793"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x24

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#B2A1C7"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x28

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#00B0F0"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x2a

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#e0eee0"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x2c

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#8DB4E3"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x2d

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#FFC7CE"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x2f

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#f5deb3"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x34

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#f4a460"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x9

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#FFFFFF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x32

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#F7BE81"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x33

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#F5F6CE"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->alignMap:Ljava/util/Map;

    .line 140
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->alignMap:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "left"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->alignMap:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "center"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->alignMap:Ljava/util/Map;

    invoke-static {v3}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "right"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->alignMap:Ljava/util/Map;

    invoke-static {v4}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "justify"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    .line 161
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "0"

    const-string/jumbo v2, "00000000"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "1"

    const-string/jumbo v2, "00FFFFFF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "2"

    const-string/jumbo v2, "00FF0000"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "3"

    const-string/jumbo v2, "0000FF00"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "4"

    const-string/jumbo v2, "000000FF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "5"

    const-string/jumbo v2, "00FFFF00"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "6"

    const-string/jumbo v2, "00FF00FF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "7"

    const-string/jumbo v2, "0000FFFF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "8"

    const-string/jumbo v2, "00000000"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "9"

    const-string/jumbo v2, "00FFFFFF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "10"

    const-string/jumbo v2, "00FF0000"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "11"

    const-string/jumbo v2, "0000FF00"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "12"

    const-string/jumbo v2, "000000FF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "13"

    const-string/jumbo v2, "00FFFF00"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "14"

    const-string/jumbo v2, "00FF00FF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "15"

    const-string/jumbo v2, "0000FFFF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "16"

    const-string/jumbo v2, "00800000"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "17"

    const-string/jumbo v2, "00008000"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "18"

    const-string/jumbo v2, "00000080"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "19"

    const-string/jumbo v2, "00808000"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "20"

    const-string/jumbo v2, "00800080"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "21"

    const-string/jumbo v2, "00008080"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "22"

    const-string/jumbo v2, "00C0C0C0"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "23"

    const-string/jumbo v2, "00808080"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "24"

    const-string/jumbo v2, "009999FF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "25"

    const-string/jumbo v2, "00993366"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "26"

    const-string/jumbo v2, "00FFFFCC"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "27"

    const-string/jumbo v2, "00CCFFFF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "28"

    const-string/jumbo v2, "00660066"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "29"

    const-string/jumbo v2, "00FF8080"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "30"

    const-string/jumbo v2, "000066CC"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "31"

    const-string/jumbo v2, "00CCCCFF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "32"

    const-string/jumbo v2, "00000080"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "33"

    const-string/jumbo v2, "00FF00FF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "34"

    const-string/jumbo v2, "00FFFF00"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "35"

    const-string/jumbo v2, "0000FFFF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "36"

    const-string/jumbo v2, "00800080"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "37"

    const-string/jumbo v2, "00800000"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "38"

    const-string/jumbo v2, "00008080"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "39"

    const-string/jumbo v2, "000000FF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "40"

    const-string/jumbo v2, "0000CCFF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "41"

    const-string/jumbo v2, "00CCFFFF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "42"

    const-string/jumbo v2, "00CCFFCC"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "43"

    const-string/jumbo v2, "00FFFF99"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "44"

    const-string/jumbo v2, "0099CCFF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "45"

    const-string/jumbo v2, "00FF99CC"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "46"

    const-string/jumbo v2, "00CC99FF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "47"

    const-string/jumbo v2, "00FFCC99"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "48"

    const-string/jumbo v2, "003366FF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "49"

    const-string/jumbo v2, "0033CCCC"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "50"

    const-string/jumbo v2, "0099CC00"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "51"

    const-string/jumbo v2, "00FFCC00"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "52"

    const-string/jumbo v2, "00FF9900"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "53"

    const-string/jumbo v2, "00FF6600"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "54"

    const-string/jumbo v2, "00666699"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "55"

    const-string/jumbo v2, "00969696"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "56"

    const-string/jumbo v2, "00003366"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "57"

    const-string/jumbo v2, "00339966"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "58"

    const-string/jumbo v2, "00003300"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "59"

    const-string/jumbo v2, "00333300"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "60"

    const-string/jumbo v2, "00993300"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "61"

    const-string/jumbo v2, "00993366"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "62"

    const-string/jumbo v2, "00333399"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    const-string/jumbo v1, "63"

    const-string/jumbo v2, "00333333"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized getAlign(I)Ljava/lang/String;
    .locals 3
    .param p0, "justification"    # I

    .prologue
    .line 156
    const-class v1, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->alignMap:Ljava/util/Map;

    invoke-static {p0}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized getHexCode(I)Ljava/lang/String;
    .locals 3
    .param p0, "index"    # I

    .prologue
    .line 151
    const-class v1, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->colorMap:Ljava/util/Map;

    invoke-static {p0}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized getIndexedColorHexCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "index"    # Ljava/lang/String;

    .prologue
    .line 228
    const-class v1, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->IndexedColorMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static intToStr(I)Ljava/lang/String;
    .locals 1
    .param p0, "val"    # I

    .prologue
    .line 147
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
