.class public Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;
.super Ljava/lang/Object;
.source "EmrRecordTypeSize.java"


# instance fields
.field private RecordSize:I

.field private RecordType:I

.field fileContent:[B

.field private offset:I


# direct methods
.method public constructor <init>([BI)V
    .locals 0
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;->fileContent:[B

    .line 39
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;->offset:I

    .line 40
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;->readEmfRecordTypeSize()V

    .line 41
    return-void
.end method


# virtual methods
.method public getRecordSize()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;->RecordSize:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;->RecordType:I

    return v0
.end method

.method public readEmfRecordTypeSize()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x0

    .line 85
    const/4 v2, 0x0

    .line 87
    .local v2, "i":I
    const/4 v4, 0x4

    new-array v3, v4, [B

    .line 89
    .local v3, "intConvert":[B
    const/4 v2, 0x0

    :goto_0
    if-gt v2, v8, :cond_0

    .line 90
    rsub-int/lit8 v4, v2, 0x3

    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;->fileContent:[B

    iget v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;->offset:I

    add-int/2addr v6, v2

    aget-byte v5, v5, v6

    aput-byte v5, v3, v4

    .line 89
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 91
    :cond_0
    invoke-static {v3, v7}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 92
    .local v1, "Type":I
    iget v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;->offset:I

    add-int/2addr v4, v2

    iput v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;->offset:I

    .line 93
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;->setRecordType(I)V

    .line 95
    const/4 v2, 0x0

    :goto_1
    if-gt v2, v8, :cond_1

    .line 96
    rsub-int/lit8 v4, v2, 0x3

    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;->fileContent:[B

    iget v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;->offset:I

    add-int/2addr v6, v2

    aget-byte v5, v5, v6

    aput-byte v5, v3, v4

    .line 95
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 97
    :cond_1
    invoke-static {v3, v7}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 98
    .local v0, "Size":I
    iget v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;->offset:I

    add-int/2addr v4, v2

    iput v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;->offset:I

    .line 99
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;->setRecordSize(I)V

    .line 100
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "RecrdSize"    # I

    .prologue
    .line 69
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;->RecordSize:I

    .line 70
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "RecrdType"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EmrRecordTypeSize;->RecordType:I

    .line 51
    return-void
.end method
