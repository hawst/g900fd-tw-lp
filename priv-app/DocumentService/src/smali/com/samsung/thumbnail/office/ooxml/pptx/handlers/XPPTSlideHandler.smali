.class public Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSlideHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XPPTSlideHandler.java"


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "ID"    # I
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 16
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSlideHandler;->init(I)V

    .line 17
    return-void
.end method

.method private init(I)V
    .locals 6
    .param p1, "ID"    # I

    .prologue
    .line 20
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSlideCSIDHandler;

    invoke-direct {v2, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSlideCSIDHandler;-><init>(I)V

    .line 21
    .local v2, "slideCsidHandler":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSlideCSIDHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSlideHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "cSld"

    invoke-virtual {v4, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorMapHandler;

    const-string/jumbo v4, "clrMap"

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorMapHandler;-><init>(Ljava/lang/String;)V

    .line 24
    .local v0, "colorMapHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorMapHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSlideHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "clrMap"

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorMapHandler;

    const-string/jumbo v4, "clrMapOvr"

    invoke-direct {v1, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorMapHandler;-><init>(Ljava/lang/String;)V

    .line 27
    .local v1, "colorMapOvrHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorMapHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSlideHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "clrMapOvr"

    invoke-virtual {v4, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTTextStylesHandler;

    invoke-direct {v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTTextStylesHandler;-><init>()V

    .line 29
    .local v3, "txtStyle":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTTextStylesHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSlideHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "txStyles"

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 35
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 37
    const-string/jumbo v3, "showMasterSp"

    invoke-virtual {p0, p3, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSlideHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 39
    .local v2, "val":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 40
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;

    invoke-direct {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;-><init>(Ljava/lang/String;)V

    .line 41
    .local v0, "boldValue":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    .line 42
    .local v1, "slide":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;->getValue()Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->setShowMasterShape(Z)V

    .line 44
    .end local v0    # "boldValue":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;
    .end local v1    # "slide":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;
    :cond_0
    return-void
.end method
