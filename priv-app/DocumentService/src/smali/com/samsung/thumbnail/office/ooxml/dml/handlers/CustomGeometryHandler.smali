.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "CustomGeometryHandler.java"


# instance fields
.field private cgHandler:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$ICustGeomHandler;

.field private pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

.field private shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;


# direct methods
.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$ICustGeomHandler;)V
    .locals 1
    .param p1, "nameSpaceId"    # I
    .param p2, "cgHandler"    # Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$ICustGeomHandler;

    .prologue
    .line 42
    const-string/jumbo v0, "custGeom"

    invoke-direct {p0, p1, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 43
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;->cgHandler:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$ICustGeomHandler;

    .line 44
    return-void
.end method

.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 6
    .param p1, "nameSpaceId"    # I
    .param p2, "shInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 24
    const-string/jumbo v4, "custGeom"

    invoke-direct {p0, p1, v4}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 25
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 27
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    invoke-direct {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;-><init>()V

    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    .line 29
    const/4 v4, 0x1

    new-array v3, v4, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 31
    .local v3, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathHandler;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V

    .line 32
    .local v0, "pathHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/diagram/OOXMLArrayHandler;

    const/16 v4, 0x1f

    const-string/jumbo v5, "pathLst"

    invoke-direct {v1, v4, v5, v0}, Lcom/samsung/thumbnail/office/ooxml/diagram/OOXMLArrayHandler;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 34
    .local v1, "pathLst":Lcom/samsung/thumbnail/office/ooxml/diagram/OOXMLArrayHandler;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v4, "pathLst"

    invoke-direct {v2, v4, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 36
    .local v2, "pathSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v4, 0x0

    aput-object v2, v3, v4

    .line 38
    iput-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 39
    return-void
.end method

.method private init()V
    .locals 6

    .prologue
    .line 47
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    invoke-direct {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;-><init>()V

    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    .line 49
    const/4 v4, 0x1

    new-array v3, v4, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 51
    .local v3, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPathHandler;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPathHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V

    .line 52
    .local v0, "pathHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPathHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/diagram/OOXMLArrayHandler;

    const/16 v4, 0x1f

    const-string/jumbo v5, "pathLst"

    invoke-direct {v1, v4, v5, v0}, Lcom/samsung/thumbnail/office/ooxml/diagram/OOXMLArrayHandler;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 54
    .local v1, "pathLst":Lcom/samsung/thumbnail/office/ooxml/diagram/OOXMLArrayHandler;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v4, "pathLst"

    invoke-direct {v2, v4, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 56
    .local v2, "pathSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v4, 0x0

    aput-object v2, v3, v4

    .line 58
    iput-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 59
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 72
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;->cgHandler:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$ICustGeomHandler;

    if-eqz v0, :cond_1

    .line 74
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;->cgHandler:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$ICustGeomHandler;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    invoke-interface {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$ICustGeomHandler;->setPathDescriptor(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setPathDescriptor(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V

    goto :goto_0
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 64
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 65
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;->cgHandler:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$ICustGeomHandler;

    if-eqz v0, :cond_0

    .line 66
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;->init()V

    .line 68
    :cond_0
    return-void
.end method
