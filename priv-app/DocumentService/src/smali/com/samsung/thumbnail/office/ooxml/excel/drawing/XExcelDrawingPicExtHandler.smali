.class public Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicExtHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelDrawingPicExtHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 20
    const/16 v0, 0xc9

    const-string/jumbo v1, "ext"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 21
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 6
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 26
    const-string/jumbo v3, "cx"

    invoke-virtual {p0, p3, v3, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicExtHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 27
    .local v1, "xVal":Ljava/lang/String;
    const-string/jumbo v3, "cy"

    invoke-virtual {p0, p3, v3, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicExtHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    .line 33
    .local v2, "yVal":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getCurrentCellAnchor()Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    move-result-object v0

    .line 34
    .local v0, "cellAnchor":Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;
    if-eqz v0, :cond_0

    instance-of v3, v0, Lcom/samsung/thumbnail/office/ooxml/excel/OneCellAnchor;

    if-eqz v3, :cond_0

    move-object v3, v0

    .line 35
    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/excel/OneCellAnchor;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/samsung/thumbnail/office/ooxml/excel/OneCellAnchor;->setCX(J)V

    .line 36
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/OneCellAnchor;

    .end local v0    # "cellAnchor":Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/samsung/thumbnail/office/ooxml/excel/OneCellAnchor;->setCY(J)V

    .line 38
    :cond_0
    return-void
.end method
