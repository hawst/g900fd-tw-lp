.class public Lcom/samsung/thumbnail/office/MimeType;
.super Ljava/lang/Object;
.source "MimeType.java"


# static fields
.field public static final MS_EXCEL_2003:Ljava/lang/String; = "application/vnd.ms-excel"

.field public static final MS_EXCEL_2007:Ljava/lang/String; = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

.field public static final MS_POINT_2003:Ljava/lang/String; = "application/vnd.ms-powerpoint"

.field public static final MS_POINT_2007:Ljava/lang/String; = "application/vnd.openxmlformats-officedocument.presentationml.presentation"

.field public static final MS_WORD_2003:Ljava/lang/String; = "application/msword"

.field public static final MS_WORD_2007:Ljava/lang/String; = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

.field public static final PDF_FORMAT:Ljava/lang/String; = "application/pdf"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAction(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 49
    const/4 v0, 0x0

    .line 50
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v1, "application/msword"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 51
    :cond_0
    const-string/jumbo v0, "com.siso.office.WORD"

    .line 59
    :cond_1
    :goto_0
    return-object v0

    .line 52
    :cond_2
    const-string/jumbo v1, "application/vnd.ms-excel"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 54
    :cond_3
    const-string/jumbo v0, "com.siso.office.EXCEL"

    goto :goto_0

    .line 55
    :cond_4
    const-string/jumbo v1, "application/vnd.ms-powerpoint"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.presentationml.presentation"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 57
    :cond_5
    const-string/jumbo v0, "com.siso.office.POINT"

    goto :goto_0
.end method

.method public static getFileExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 25
    const/4 v0, 0x0

    .line 26
    .local v0, "exten":Ljava/lang/String;
    const-string/jumbo v1, "application/msword"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 27
    const-string/jumbo v0, ".doc"

    .line 39
    :cond_0
    :goto_0
    return-object v0

    .line 28
    :cond_1
    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 29
    const-string/jumbo v0, ".docx"

    goto :goto_0

    .line 30
    :cond_2
    const-string/jumbo v1, "application/vnd.ms-excel"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 31
    const-string/jumbo v0, ".xls"

    goto :goto_0

    .line 32
    :cond_3
    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 33
    const-string/jumbo v0, ".xlsx"

    goto :goto_0

    .line 34
    :cond_4
    const-string/jumbo v1, "application/vnd.ms-powerpoint"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 35
    const-string/jumbo v0, ".ppt"

    goto :goto_0

    .line 36
    :cond_5
    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.presentationml.presentation"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 37
    const-string/jumbo v0, ".pptx"

    goto :goto_0
.end method

.method public static getType(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 63
    const/4 v0, 0x0

    .line 64
    .local v0, "type":Ljava/lang/String;
    const-string/jumbo v1, "application/msword"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 65
    const-string/jumbo v0, "doc"

    .line 77
    :cond_0
    :goto_0
    return-object v0

    .line 66
    :cond_1
    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 67
    const-string/jumbo v0, "docx"

    goto :goto_0

    .line 68
    :cond_2
    const-string/jumbo v1, "application/vnd.ms-excel"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 69
    const-string/jumbo v0, "xls"

    goto :goto_0

    .line 70
    :cond_3
    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 71
    const-string/jumbo v0, "xlsx"

    goto :goto_0

    .line 72
    :cond_4
    const-string/jumbo v1, "application/vnd.ms-powerpoint"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 73
    const-string/jumbo v0, "ppt"

    goto :goto_0

    .line 74
    :cond_5
    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.presentationml.presentation"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 75
    const-string/jumbo v0, "pptx"

    goto :goto_0
.end method
