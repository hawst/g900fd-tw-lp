.class public Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/XPresentationParser;
.super Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBaseParser;
.source "XPresentationParser.java"


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;)V
    .locals 0
    .param p1, "presentation"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBaseParser;-><init>(Ljava/lang/Object;)V

    .line 13
    return-void
.end method


# virtual methods
.method public createRootElementHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/XPptPresentationHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/XPptPresentationHandler;-><init>()V

    return-object v0
.end method

.method public getMainNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .locals 1

    .prologue
    .line 22
    const/16 v0, 0x33

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/XPresentationParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v0

    return-object v0
.end method
