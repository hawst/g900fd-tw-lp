.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeFontHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "ThemeFontHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler$ITextFontConsumer;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "elementName"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 19
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler;

    const-string/jumbo v1, "latin"

    invoke-direct {v0, p0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler$ITextFontConsumer;Ljava/lang/String;)V

    .line 20
    .local v0, "fontHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeFontHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "latin"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    return-void
.end method


# virtual methods
.method public consumeFont(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 2
    .param p1, "typeFace"    # Ljava/lang/String;
    .param p2, "fontFamily"    # Ljava/lang/String;
    .param p3, "charSetFamily"    # Ljava/lang/String;
    .param p4, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeFontHandler;->getElementName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "majorFont"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    invoke-virtual {p4}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->setMajorFont(Ljava/lang/String;)V

    .line 31
    :goto_0
    return-void

    .line 29
    :cond_0
    invoke-virtual {p4}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->setMinorFont(Ljava/lang/String;)V

    goto :goto_0
.end method
