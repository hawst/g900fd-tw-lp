.class public Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramXfrmHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DiagramXfrmHandler.java"


# instance fields
.field private xfrmDgm:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IXfrmHandlerDgm;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IXfrmHandlerDgm;)V
    .locals 2
    .param p1, "xfrmDgm"    # Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IXfrmHandlerDgm;

    .prologue
    .line 24
    const/16 v0, 0x1f

    const-string/jumbo v1, "xfrm"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 25
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramXfrmHandler;->xfrmDgm:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IXfrmHandlerDgm;

    .line 26
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramXfrmHandler;->init()V

    .line 27
    return-void
.end method

.method private init()V
    .locals 6

    .prologue
    .line 30
    const/4 v5, 0x2

    new-array v4, v5, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 32
    .local v4, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPointHandler;

    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramXfrmHandler;->xfrmDgm:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IXfrmHandlerDgm;

    invoke-direct {v2, v5}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPointHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IXfrmHandlerDgm;)V

    .line 33
    .local v2, "offHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPointHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v5, "off"

    invoke-direct {v3, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 35
    .local v3, "offSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v5, 0x0

    aput-object v3, v4, v5

    .line 37
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSizeHandler;

    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramXfrmHandler;->xfrmDgm:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IXfrmHandlerDgm;

    invoke-direct {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSizeHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IXfrmHandlerDgm;)V

    .line 38
    .local v0, "extHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSizeHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v5, "ext"

    invoke-direct {v1, v5, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 40
    .local v1, "extSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v5, 0x1

    aput-object v1, v4, v5

    .line 42
    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramXfrmHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 43
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 48
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 50
    const-string/jumbo v2, "rot"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramXfrmHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 51
    .local v0, "rot":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 52
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;

    invoke-direct {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;-><init>(Ljava/lang/String;)V

    .line 53
    .local v1, "rotation":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramXfrmHandler;->xfrmDgm:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IXfrmHandlerDgm;

    if-eqz v2, :cond_0

    .line 54
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramXfrmHandler;->xfrmDgm:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IXfrmHandlerDgm;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;->getValue()I

    move-result v3

    invoke-interface {v2, p1, v3}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IXfrmHandlerDgm;->setRotation(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;I)V

    .line 57
    .end local v1    # "rotation":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;
    :cond_0
    return-void
.end method
