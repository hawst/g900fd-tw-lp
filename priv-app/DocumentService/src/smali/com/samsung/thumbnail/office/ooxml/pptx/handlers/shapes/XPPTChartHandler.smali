.class public Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTChartHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XPPTChartHandler.java"


# instance fields
.field private graphicFrame:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;)V
    .locals 2
    .param p1, "graphicFrame"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;

    .prologue
    .line 21
    const/16 v0, 0x23

    const-string/jumbo v1, "chart"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 22
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTChartHandler;->graphicFrame:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;

    .line 23
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 28
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 30
    const/16 v2, 0xa

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->getPrefix()Ljava/lang/String;

    move-result-object v1

    .line 32
    .local v1, "prefix":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 33
    .local v0, "attrName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTChartHandler;->graphicFrame:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;->setIsChart(Z)V

    .line 34
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTChartHandler;->graphicFrame:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;

    invoke-interface {p3, v0}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;->setRelId(Ljava/lang/String;)V

    .line 36
    return-void
.end method
