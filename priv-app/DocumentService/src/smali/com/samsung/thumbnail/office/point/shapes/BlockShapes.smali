.class public Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;
.super Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;
.source "BlockShapes.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mDrawOnCanvas:Z

.field private mLeftMarg:I

.field private mTopMarg:I


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "folderPath"    # Ljava/io/File;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;-><init>()V

    .line 30
    const-string/jumbo v0, "BlockShapes"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->TAG:Ljava/lang/String;

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mDrawOnCanvas:Z

    .line 36
    iput-object p1, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->folderPath:Ljava/io/File;

    .line 37
    return-void
.end method


# virtual methods
.method protected draw(Landroid/graphics/Canvas;I)V
    .locals 42
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeCount"    # I

    .prologue
    .line 161
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    const v5, 0x46a8c000    # 21600.0f

    div-float v34, v4, v5

    .line 162
    .local v34, "mulFactor_Width":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    const v5, 0x46a8c000    # 21600.0f

    div-float v33, v4, v5

    .line 164
    .local v33, "mulFactor_Height":F
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "HomePlate"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 169
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    if-nez v4, :cond_0

    .line 170
    const/16 v4, 0x3f48

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    .line 172
    :cond_0
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float v11, v4, v5

    .line 174
    .local v11, "adj_width":F
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 175
    .local v2, "path":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v4, v11

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 176
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 178
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v4, v11

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 180
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 181
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 182
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 184
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_1

    .line 185
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 186
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_2

    .line 187
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 191
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 192
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 193
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46680800    # 14850.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 194
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    .line 754
    .end local v2    # "path":Landroid/graphics/Path;
    .end local v11    # "adj_width":F
    :cond_3
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mDrawOnCanvas:Z

    if-nez v4, :cond_4

    .line 755
    new-instance v9, Landroid/graphics/Matrix;

    invoke-direct {v9}, Landroid/graphics/Matrix;-><init>()V

    .line 758
    .local v9, "mtx":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->rotation:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    invoke-virtual {v9, v4, v5, v6}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 760
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->bitmap:Landroid/graphics/Bitmap;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->bitmapWidth:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->bitmapHight:I

    const/4 v10, 0x1

    invoke-static/range {v4 .. v10}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v39

    .line 765
    .local v39, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v37, Ljava/io/FileOutputStream;

    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->folderPath:Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "pic_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->shapeName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p2

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ".png"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v37

    invoke-direct {v0, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 769
    .local v37, "out":Ljava/io/FileOutputStream;
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    move-object/from16 v0, v39

    move-object/from16 v1, v37

    invoke-virtual {v0, v4, v5, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 770
    invoke-virtual/range {v37 .. v37}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 775
    .end local v9    # "mtx":Landroid/graphics/Matrix;
    .end local v37    # "out":Ljava/io/FileOutputStream;
    .end local v39    # "rotatedBMP":Landroid/graphics/Bitmap;
    :cond_4
    :goto_1
    return-void

    .line 197
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "Chevron"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 202
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    if-nez v4, :cond_6

    .line 203
    const/16 v4, 0x3f48

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    .line 205
    :cond_6
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float v11, v4, v5

    .line 206
    .restart local v11    # "adj_width":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    sub-float v36, v4, v11

    .line 207
    .local v36, "notch_width":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float v35, v4, v5

    .line 209
    .local v35, "notch_height":F
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 210
    .restart local v2    # "path":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v4, v11

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 211
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 213
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v4, v11

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 215
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 216
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v4, v4

    add-float v4, v4, v36

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float v5, v5, v35

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 218
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 219
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 221
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_7

    .line 222
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 223
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_8

    .line 224
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 228
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x45fd2000    # 8100.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 229
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 230
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x463f6800    # 12250.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 231
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 234
    .end local v2    # "path":Landroid/graphics/Path;
    .end local v11    # "adj_width":F
    .end local v35    # "notch_height":F
    .end local v36    # "notch_width":F
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "Plaque"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 239
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    if-nez v4, :cond_a

    .line 240
    const/16 v4, 0xe10

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    .line 242
    :cond_a
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 243
    .restart local v2    # "path":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v4, v4

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 245
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v4, v4

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v5, v5

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v7, v0

    add-float/2addr v6, v7

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v7, v7

    mul-float v7, v7, v33

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v8, v0

    add-float/2addr v7, v8

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 249
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    rsub-int v5, v5, 0x5460

    int-to-float v5, v5

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 251
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v4, v4

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    rsub-int v5, v5, 0x5460

    int-to-float v5, v5

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v6, v6

    mul-float v6, v6, v34

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v7, v0

    add-float/2addr v6, v7

    const v7, 0x46a8c000    # 21600.0f

    mul-float v7, v7, v33

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v8, v0

    add-float/2addr v7, v8

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 255
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    rsub-int v4, v4, 0x5460

    int-to-float v4, v4

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 258
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    rsub-int v4, v4, 0x5460

    int-to-float v4, v4

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    rsub-int v5, v5, 0x5460

    int-to-float v5, v5

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    mul-float v6, v6, v34

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v7, v0

    add-float/2addr v6, v7

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    rsub-int v7, v7, 0x5460

    int-to-float v7, v7

    mul-float v7, v7, v33

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v8, v0

    add-float/2addr v7, v8

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 263
    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v5, v5

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 265
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    rsub-int v4, v4, 0x5460

    int-to-float v4, v4

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v5, v5

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    rsub-int v6, v6, 0x5460

    int-to-float v6, v6

    mul-float v6, v6, v34

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v7, v0

    add-float/2addr v6, v7

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v8, v0

    add-float/2addr v7, v8

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 270
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 272
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_b

    .line 273
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 274
    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_c

    .line 275
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 279
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x457d2000    # 4050.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 280
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x457d2000    # 4050.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 281
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46891c00    # 17550.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 282
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46891c00    # 17550.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 285
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "LightningBolt"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 287
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 288
    .restart local v2    # "path":Landroid/graphics/Path;
    const v4, 0x46046000    # 8472.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 290
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x45732000    # 3890.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 292
    const v4, 0x45ed9000    # 7602.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x4602f800    # 8382.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 294
    const v4, 0x459cf000    # 5022.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x4617a400    # 9705.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 296
    const v4, 0x463ef800    # 12222.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x46592400    # 13897.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 298
    const v4, 0x461c7000    # 10012.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x46690c00    # 14915.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 300
    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 302
    const v4, 0x4666bc00    # 14767.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x46493400    # 12877.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 304
    const v4, 0x46818200    # 16577.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x463b9c00    # 12007.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 306
    const v4, 0x462ca800    # 11050.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x45d46800    # 6797.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 308
    const v4, 0x4648f000    # 12860.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/high16 v5, 0x45be0000    # 6080.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 310
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 312
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_e

    .line 313
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 314
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_f

    .line 315
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 319
    :cond_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x4622b400    # 10413.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 320
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46334c00    # 11475.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 321
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46091c00    # 8775.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 322
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x462ecc00    # 11187.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 325
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "Moon"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 332
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    if-nez v4, :cond_11

    .line 333
    const/16 v4, 0x2a30

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    .line 335
    :cond_11
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 338
    .restart local v2    # "path":Landroid/graphics/Path;
    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 340
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v7, v0

    add-float/2addr v6, v7

    const v7, 0x4628c000    # 10800.0f

    mul-float v7, v7, v33

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v8, v0

    add-float/2addr v7, v8

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 344
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x4628c000    # 10800.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 347
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    mul-float v6, v6, v34

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v7, v0

    add-float/2addr v6, v7

    const v7, 0x46a8c000    # 21600.0f

    mul-float v7, v7, v33

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v8, v0

    add-float/2addr v7, v8

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 352
    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 355
    const v4, 0x4628c000    # 10800.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x4628c000    # 10800.0f

    mul-float v6, v6, v34

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v7, v0

    add-float/2addr v6, v7

    const v7, 0x4628c000    # 10800.0f

    mul-float v7, v7, v33

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v8, v0

    add-float/2addr v7, v8

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 360
    const v4, 0x4628c000    # 10800.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x4628c000    # 10800.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 363
    const v4, 0x4628c000    # 10800.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    mul-float v6, v6, v34

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v7, v0

    add-float/2addr v6, v7

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v8, v0

    add-float/2addr v7, v8

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 367
    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 370
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 373
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_12

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_12

    .line 374
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 375
    :cond_12
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_13

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_13

    .line 376
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 380
    :cond_13
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x457d2000    # 4050.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 381
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x45bdd800    # 6075.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 382
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x4613a800    # 9450.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 383
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46729400    # 15525.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 386
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "Cube"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 391
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    if-nez v4, :cond_15

    .line 392
    const/16 v4, 0x1518

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    .line 394
    :cond_15
    new-instance v38, Landroid/graphics/Paint;

    invoke-direct/range {v38 .. v38}, Landroid/graphics/Paint;-><init>()V

    .line 395
    .local v38, "paint2":Landroid/graphics/Paint;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_16

    .line 396
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->set(Landroid/graphics/Paint;)V

    .line 397
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getAlpha()I

    move-result v4

    add-int/lit8 v4, v4, -0x14

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 399
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 400
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 403
    :cond_16
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 404
    .restart local v2    # "path":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v4, v4

    mul-float v4, v4, v34

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 405
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v5, v5

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 406
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    rsub-int v4, v4, 0x5460

    int-to-float v4, v4

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v5, v5

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 408
    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v34

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 409
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 410
    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 411
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_17

    .line 412
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 414
    :cond_17
    new-instance v2, Landroid/graphics/Path;

    .end local v2    # "path":Landroid/graphics/Path;
    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 415
    .restart local v2    # "path":Landroid/graphics/Path;
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v5, v5

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 416
    const/4 v4, 0x0

    const v5, 0x46a8c000    # 21600.0f

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 417
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    rsub-int v4, v4, 0x5460

    int-to-float v4, v4

    mul-float v4, v4, v34

    const v5, 0x46a8c000    # 21600.0f

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 419
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    rsub-int v4, v4, 0x5460

    int-to-float v4, v4

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v5, v5

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 421
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 422
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_18

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_18

    .line 423
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 424
    :cond_18
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_19

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_19

    .line 425
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 427
    :cond_19
    new-instance v2, Landroid/graphics/Path;

    .end local v2    # "path":Landroid/graphics/Path;
    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 428
    .restart local v2    # "path":Landroid/graphics/Path;
    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v34

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 429
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    rsub-int v4, v4, 0x5460

    int-to-float v4, v4

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v5, v5

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 431
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    rsub-int v4, v4, 0x5460

    int-to-float v4, v4

    mul-float v4, v4, v34

    const v5, 0x46a8c000    # 21600.0f

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 433
    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    rsub-int v5, v5, 0x5460

    int-to-float v5, v5

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 435
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 436
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_1a

    .line 437
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 438
    :cond_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_1b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_1b

    .line 439
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 443
    :cond_1b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x441c4000    # 625.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 444
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x45bdd800    # 6075.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 445
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46729400    # 15525.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 446
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 449
    .end local v2    # "path":Landroid/graphics/Path;
    .end local v38    # "paint2":Landroid/graphics/Paint;
    :cond_1c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "FoldedCorner"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_21

    .line 454
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    if-nez v4, :cond_1d

    .line 455
    const/16 v4, 0x49d4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    .line 457
    :cond_1d
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v12, v4

    .line 458
    .local v12, "at0":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    rsub-int v4, v4, 0x5460

    int-to-float v13, v4

    .line 459
    .local v13, "at1":F
    const v4, 0x46048400    # 8481.0f

    mul-float/2addr v4, v13

    const/high16 v5, 0x47000000    # 32768.0f

    div-float v23, v4, v5

    .line 460
    .local v23, "at2":F
    add-float v25, v23, v12

    .line 461
    .local v25, "at3":F
    const v4, 0x448ba000    # 1117.0f

    mul-float/2addr v4, v13

    const/high16 v5, 0x47000000    # 32768.0f

    div-float v26, v4, v5

    .line 462
    .local v26, "at4":F
    add-float v27, v26, v12

    .line 463
    .local v27, "at5":F
    const v4, 0x4637d000    # 11764.0f

    mul-float/2addr v4, v13

    const/high16 v5, 0x47000000    # 32768.0f

    div-float v28, v4, v5

    .line 464
    .local v28, "at6":F
    add-float v29, v28, v12

    .line 465
    .local v29, "at7":F
    const/high16 v4, 0x45c00000    # 6144.0f

    mul-float/2addr v4, v13

    const/high16 v5, 0x47000000    # 32768.0f

    div-float v30, v4, v5

    .line 466
    .local v30, "at8":F
    add-float v31, v30, v12

    .line 467
    .local v31, "at9":F
    const/high16 v4, 0x46a00000    # 20480.0f

    mul-float/2addr v4, v13

    const/high16 v5, 0x47000000    # 32768.0f

    div-float v14, v4, v5

    .line 468
    .local v14, "at10":F
    add-float v15, v14, v12

    .line 469
    .local v15, "at11":F
    const/high16 v4, 0x45c00000    # 6144.0f

    mul-float/2addr v4, v13

    const/high16 v5, 0x47000000    # 32768.0f

    div-float v16, v4, v5

    .line 470
    .local v16, "at12":F
    add-float v17, v16, v12

    .line 472
    .local v17, "at13":F
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 473
    .restart local v2    # "path":Landroid/graphics/Path;
    const/4 v4, 0x0

    mul-float v4, v4, v34

    const/4 v5, 0x0

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 474
    const/4 v4, 0x0

    mul-float v4, v4, v34

    const v5, 0x46a8c000    # 21600.0f

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 475
    mul-float v4, v12, v34

    const v5, 0x46a8c000    # 21600.0f

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 476
    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v34

    mul-float v5, v12, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 477
    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v34

    const/4 v5, 0x0

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 478
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 480
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_1e

    .line 481
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 483
    :cond_1e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_1f

    .line 484
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 486
    :cond_1f
    new-instance v2, Landroid/graphics/Path;

    .end local v2    # "path":Landroid/graphics/Path;
    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 487
    .restart local v2    # "path":Landroid/graphics/Path;
    mul-float v4, v12, v34

    const v5, 0x46a8c000    # 21600.0f

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 488
    mul-float v4, v25, v34

    mul-float v5, v27, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 489
    mul-float v3, v29, v34

    mul-float v4, v31, v33

    mul-float v5, v15, v34

    mul-float v6, v17, v33

    const v7, 0x46a8c000    # 21600.0f

    mul-float v7, v7, v34

    mul-float v8, v12, v33

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 493
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_20

    .line 494
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 495
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 500
    :cond_20
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 501
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x45bdd800    # 6075.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 502
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46729400    # 15525.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 503
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 506
    .end local v2    # "path":Landroid/graphics/Path;
    .end local v12    # "at0":F
    .end local v13    # "at1":F
    .end local v14    # "at10":F
    .end local v15    # "at11":F
    .end local v16    # "at12":F
    .end local v17    # "at13":F
    .end local v23    # "at2":F
    .end local v25    # "at3":F
    .end local v26    # "at4":F
    .end local v27    # "at5":F
    .end local v28    # "at6":F
    .end local v29    # "at7":F
    .end local v30    # "at8":F
    .end local v31    # "at9":F
    :cond_21
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "Bevel"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_29

    .line 511
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    if-nez v4, :cond_22

    .line 512
    const/16 v4, 0xa8c

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    .line 514
    :cond_22
    new-instance v38, Landroid/graphics/Paint;

    invoke-direct/range {v38 .. v38}, Landroid/graphics/Paint;-><init>()V

    .line 516
    .restart local v38    # "paint2":Landroid/graphics/Paint;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_23

    .line 517
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->set(Landroid/graphics/Paint;)V

    .line 518
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getAlpha()I

    move-result v4

    add-int/lit8 v4, v4, -0x14

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 521
    :cond_23
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 523
    .restart local v2    # "path":Landroid/graphics/Path;
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 524
    const/4 v4, 0x0

    const v5, 0x46a8c000    # 21600.0f

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 525
    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v34

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 526
    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v34

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 527
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 528
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_24

    .line 529
    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 530
    :cond_24
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_25

    .line 531
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 534
    :cond_25
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 535
    .local v3, "path2":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v4, v4

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v5, v5

    mul-float v5, v5, v33

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 536
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v4, v4

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v6, v6

    mul-float v6, v6, v33

    sub-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 538
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v5, v5

    mul-float v5, v5, v34

    sub-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v6, v6

    mul-float v6, v6, v33

    sub-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 540
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v5, v5

    mul-float v5, v5, v34

    sub-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v5, v5

    mul-float v5, v5, v33

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 542
    invoke-virtual {v3}, Landroid/graphics/Path;->close()V

    .line 543
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_26

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_26

    .line 544
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 545
    :cond_26
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_27

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_27

    .line 546
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 550
    :cond_27
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 551
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v4, v4

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v5, v5

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 552
    const/4 v4, 0x0

    const v5, 0x46a8c000    # 21600.0f

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 553
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v4, v4

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v6, v6

    mul-float v6, v6, v33

    sub-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 555
    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v34

    const v5, 0x46a8c000    # 21600.0f

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 556
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v5, v5

    mul-float v5, v5, v34

    sub-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v6, v6

    mul-float v6, v6, v33

    sub-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 558
    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v34

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 559
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v5, v5

    mul-float v5, v5, v34

    sub-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v5, v5

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 561
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_28

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_28

    .line 562
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 566
    :cond_28
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x457d2000    # 4050.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 567
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x457d2000    # 4050.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 568
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46891c00    # 17550.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 569
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46891c00    # 17550.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 572
    .end local v2    # "path":Landroid/graphics/Path;
    .end local v3    # "path2":Landroid/graphics/Path;
    .end local v38    # "paint2":Landroid/graphics/Paint;
    :cond_29
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "Sun"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2d

    .line 577
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    if-nez v4, :cond_2a

    .line 578
    const/16 v4, 0x1518

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    .line 580
    :cond_2a
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    rsub-int v4, v4, 0x2a30

    int-to-float v12, v4

    .line 581
    .restart local v12    # "at0":F
    const v4, 0x46ec8400    # 30274.0f

    mul-float/2addr v4, v12

    const/high16 v5, 0x47000000    # 32768.0f

    div-float v13, v4, v5

    .line 582
    .restart local v13    # "at1":F
    const v4, 0x4643f000    # 12540.0f

    mul-float/2addr v4, v12

    const/high16 v5, 0x47000000    # 32768.0f

    div-float v23, v4, v5

    .line 583
    .restart local v23    # "at2":F
    const v4, 0x4628c000    # 10800.0f

    sub-float v27, v4, v13

    .line 584
    .restart local v27    # "at5":F
    const v4, 0x4628c000    # 10800.0f

    sub-float v28, v4, v23

    .line 585
    .restart local v28    # "at6":F
    const/high16 v4, 0x40400000    # 3.0f

    mul-float v4, v4, v27

    const/high16 v5, 0x40800000    # 4.0f

    div-float v14, v4, v5

    .line 586
    .restart local v14    # "at10":F
    const/high16 v4, 0x40400000    # 3.0f

    mul-float v4, v4, v28

    const/high16 v5, 0x40800000    # 4.0f

    div-float v15, v4, v5

    .line 587
    .restart local v15    # "at11":F
    const v4, 0x4445c000    # 791.0f

    add-float v16, v14, v4

    .line 588
    .restart local v16    # "at12":F
    const v4, 0x4445c000    # 791.0f

    add-float v17, v15, v4

    .line 589
    .restart local v17    # "at13":F
    const v4, 0x4528c000    # 2700.0f

    add-float v18, v15, v4

    .line 590
    .local v18, "at14":F
    const v4, 0x46a8c000    # 21600.0f

    sub-float v19, v4, v14

    .line 591
    .local v19, "at15":F
    const v4, 0x46a8c000    # 21600.0f

    sub-float v20, v4, v16

    .line 592
    .local v20, "at16":F
    const v4, 0x46a8c000    # 21600.0f

    sub-float v21, v4, v17

    .line 593
    .local v21, "at17":F
    const v4, 0x46a8c000    # 21600.0f

    sub-float v22, v4, v18

    .line 594
    .local v22, "at18":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    rsub-int v4, v4, 0x5460

    int-to-float v0, v4

    move/from16 v24, v0

    .line 596
    .local v24, "at20":F
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 597
    .restart local v2    # "path":Landroid/graphics/Path;
    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v34

    const v5, 0x4628c000    # 10800.0f

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 598
    mul-float v4, v19, v34

    mul-float v5, v18, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 599
    mul-float v4, v19, v34

    mul-float v5, v22, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 600
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 601
    const v4, 0x46900800    # 18436.0f

    mul-float v4, v4, v34

    const v5, 0x4545b000    # 3163.0f

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 602
    mul-float v4, v21, v34

    mul-float v5, v16, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 603
    mul-float v4, v20, v34

    mul-float v5, v17, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 604
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 605
    const v4, 0x4628c000    # 10800.0f

    mul-float v4, v4, v34

    const/4 v5, 0x0

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 606
    mul-float v4, v18, v34

    mul-float v5, v14, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 607
    mul-float v4, v22, v34

    mul-float v5, v14, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 608
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 609
    const v4, 0x4545b000    # 3163.0f

    mul-float v4, v4, v34

    const v5, 0x4545b000    # 3163.0f

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 610
    mul-float v4, v16, v34

    mul-float v5, v17, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 611
    mul-float v4, v17, v34

    mul-float v5, v16, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 612
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 613
    const/4 v4, 0x0

    mul-float v4, v4, v34

    const v5, 0x4628c000    # 10800.0f

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 614
    mul-float v4, v14, v34

    mul-float v5, v22, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 615
    mul-float v4, v14, v34

    mul-float v5, v18, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 616
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 617
    const v4, 0x4545b000    # 3163.0f

    mul-float v4, v4, v34

    const v5, 0x46900800    # 18436.0f

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 618
    mul-float v4, v17, v34

    mul-float v5, v20, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 619
    mul-float v4, v16, v34

    mul-float v5, v21, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 620
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 621
    const v4, 0x4628c000    # 10800.0f

    mul-float v4, v4, v34

    const v5, 0x46a8c000    # 21600.0f

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 622
    mul-float v4, v22, v34

    mul-float v5, v19, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 623
    mul-float v4, v18, v34

    mul-float v5, v19, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 624
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 625
    const v4, 0x46900800    # 18436.0f

    mul-float v4, v4, v34

    const v5, 0x46900800    # 18436.0f

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 626
    mul-float v4, v20, v34

    mul-float v5, v21, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 627
    mul-float v4, v21, v34

    mul-float v5, v20, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 628
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 630
    const v4, 0x4628c000    # 10800.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v5, v5

    mul-float v5, v5, v33

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 631
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v4, v4

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v5, v5

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v6, v6

    mul-float v6, v6, v34

    const v7, 0x4628c000    # 10800.0f

    mul-float v7, v7, v33

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 633
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v4, v4

    mul-float v4, v4, v34

    mul-float v5, v24, v33

    const v6, 0x4628c000    # 10800.0f

    mul-float v6, v6, v34

    mul-float v7, v24, v33

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 635
    mul-float v4, v24, v34

    mul-float v5, v24, v33

    mul-float v6, v24, v34

    const v7, 0x4628c000    # 10800.0f

    mul-float v7, v7, v33

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 637
    mul-float v4, v24, v34

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v5, v5

    mul-float v5, v5, v33

    const v6, 0x4628c000    # 10800.0f

    mul-float v6, v6, v34

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    int-to-float v7, v7

    mul-float v7, v7, v33

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 639
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 641
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_2b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_2b

    .line 642
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 643
    :cond_2b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_2c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_2c

    .line 644
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 648
    :cond_2c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x45e99800    # 7475.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 649
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x45e99800    # 7475.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 650
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x465cb400    # 14125.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 651
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x465cb400    # 14125.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 654
    .end local v2    # "path":Landroid/graphics/Path;
    .end local v12    # "at0":F
    .end local v13    # "at1":F
    .end local v14    # "at10":F
    .end local v15    # "at11":F
    .end local v16    # "at12":F
    .end local v17    # "at13":F
    .end local v18    # "at14":F
    .end local v19    # "at15":F
    .end local v20    # "at16":F
    .end local v21    # "at17":F
    .end local v22    # "at18":F
    .end local v23    # "at2":F
    .end local v24    # "at20":F
    .end local v27    # "at5":F
    .end local v28    # "at6":F
    :cond_2d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "SmileyFace"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 659
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    if-nez v4, :cond_2e

    .line 660
    const/16 v4, 0x4470

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    .line 662
    :cond_2e
    const v4, 0x8106

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    sub-int/2addr v4, v5

    int-to-float v12, v4

    .line 663
    .restart local v12    # "at0":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->adjval0:I

    mul-int/lit8 v4, v4, 0x4

    int-to-float v4, v4

    const/high16 v5, 0x40400000    # 3.0f

    div-float v13, v4, v5

    .line 664
    .restart local v13    # "at1":F
    const/high16 v4, 0x3f800000    # 1.0f

    mul-float/2addr v4, v12

    const/high16 v5, 0x40400000    # 3.0f

    div-float v23, v4, v5

    .line 665
    .restart local v23    # "at2":F
    sub-float v25, v13, v23

    .line 667
    .restart local v25    # "at3":F
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 668
    .restart local v2    # "path":Landroid/graphics/Path;
    const v4, 0x4628c000    # 10800.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 670
    const/4 v4, 0x0

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const/4 v6, 0x0

    mul-float v6, v6, v34

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v7, v0

    add-float/2addr v6, v7

    const v7, 0x4628c000    # 10800.0f

    mul-float v7, v7, v33

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v8, v0

    add-float/2addr v7, v8

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 674
    const/4 v4, 0x0

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x4628c000    # 10800.0f

    mul-float v6, v6, v34

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v7, v0

    add-float/2addr v6, v7

    const v7, 0x46a8c000    # 21600.0f

    mul-float v7, v7, v33

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v8, v0

    add-float/2addr v7, v8

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 678
    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    mul-float v6, v6, v34

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v7, v0

    add-float/2addr v6, v7

    const v7, 0x4628c000    # 10800.0f

    mul-float v7, v7, v33

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v8, v0

    add-float/2addr v7, v8

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 682
    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x4628c000    # 10800.0f

    mul-float v6, v6, v34

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v7, v0

    add-float/2addr v6, v7

    const/4 v7, 0x0

    mul-float v7, v7, v33

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v8, v0

    add-float/2addr v7, v8

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 686
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 687
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_2f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_2f

    .line 688
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 689
    :cond_2f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_30

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_30

    .line 690
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 692
    :cond_30
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_31

    .line 693
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 694
    :cond_31
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 695
    .restart local v3    # "path2":Landroid/graphics/Path;
    const v4, 0x45e56000    # 7340.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x45c96800    # 6445.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 697
    const v4, 0x45c23800    # 6215.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x45c96800    # 6445.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x45c23800    # 6215.0f

    mul-float v6, v6, v34

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v7, v0

    add-float/2addr v6, v7

    const v7, 0x45ec9000    # 7570.0f

    mul-float v7, v7, v33

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v8, v0

    add-float/2addr v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 701
    const v4, 0x45c23800    # 6215.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x4607dc00    # 8695.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x45e56000    # 7340.0f

    mul-float v6, v6, v34

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v7, v0

    add-float/2addr v6, v7

    const v7, 0x4607dc00    # 8695.0f

    mul-float v7, v7, v33

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v8, v0

    add-float/2addr v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 705
    const v4, 0x46044400    # 8465.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x4607dc00    # 8695.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x46044400    # 8465.0f

    mul-float v6, v6, v34

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v7, v0

    add-float/2addr v6, v7

    const v7, 0x45ec9000    # 7570.0f

    mul-float v7, v7, v33

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v8, v0

    add-float/2addr v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 709
    const v4, 0x46044400    # 8465.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x45c96800    # 6445.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x45e56000    # 7340.0f

    mul-float v6, v6, v34

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v7, v0

    add-float/2addr v6, v7

    const v7, 0x45c96800    # 6445.0f

    mul-float v7, v7, v33

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v8, v0

    add-float/2addr v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 714
    const v4, 0x465ed000    # 14260.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x45c96800    # 6445.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 716
    const v4, 0x464d3c00    # 13135.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x45c96800    # 6445.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x464d3c00    # 13135.0f

    mul-float v6, v6, v34

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v7, v0

    add-float/2addr v6, v7

    const v7, 0x45ec9000    # 7570.0f

    mul-float v7, v7, v33

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v8, v0

    add-float/2addr v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 720
    const v4, 0x464d3c00    # 13135.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x4607dc00    # 8695.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x465ed000    # 14260.0f

    mul-float v6, v6, v34

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v7, v0

    add-float/2addr v6, v7

    const v7, 0x4607dc00    # 8695.0f

    mul-float v7, v7, v33

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v8, v0

    add-float/2addr v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 724
    const v4, 0x46706400    # 15385.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x4607dc00    # 8695.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x46706400    # 15385.0f

    mul-float v6, v6, v34

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v7, v0

    add-float/2addr v6, v7

    const v7, 0x45ec9000    # 7570.0f

    mul-float v7, v7, v33

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v8, v0

    add-float/2addr v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 728
    const v4, 0x46706400    # 15385.0f

    mul-float v4, v4, v34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x45c96800    # 6445.0f

    mul-float v5, v5, v33

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x465ed000    # 14260.0f

    mul-float v6, v6, v34

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v7, v0

    add-float/2addr v6, v7

    const v7, 0x45c96800    # 6445.0f

    mul-float v7, v7, v33

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineWidth:D

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    double-to-float v8, v0

    add-float/2addr v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 732
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_32

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_32

    .line 733
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 735
    :cond_32
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_33

    .line 736
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 737
    :cond_33
    const/high16 v4, 0x459b0000    # 4960.0f

    mul-float v4, v4, v34

    mul-float v5, v12, v33

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 738
    const v4, 0x460a5400    # 8853.0f

    mul-float v4, v4, v34

    mul-float v5, v25, v33

    const v6, 0x46472c00    # 12747.0f

    mul-float v6, v6, v34

    mul-float v7, v25, v33

    const/high16 v8, 0x46820000    # 16640.0f

    mul-float v8, v8, v34

    mul-float v9, v12, v33

    invoke-virtual/range {v3 .. v9}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 741
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_34

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_34

    .line 742
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 746
    :cond_34
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x458d0800    # 4513.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 747
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x458d0800    # 4513.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 748
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46857e00    # 17087.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 749
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46857e00    # 17087.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 771
    .end local v2    # "path":Landroid/graphics/Path;
    .end local v3    # "path2":Landroid/graphics/Path;
    .end local v12    # "at0":F
    .end local v13    # "at1":F
    .end local v23    # "at2":F
    .end local v25    # "at3":F
    .restart local v9    # "mtx":Landroid/graphics/Matrix;
    .restart local v39    # "rotatedBMP":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v32

    .line 772
    .local v32, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception while writing pictures to out file"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v32

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F

    .prologue
    .line 68
    invoke-super {p0, p3, p4, p7, p8}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IFF)V

    .line 69
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mDrawOnCanvas:Z

    .line 70
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mLeftMarg:I

    .line 71
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mTopMarg:I

    .line 72
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 73
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 74
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mDrawOnCanvas:Z

    .line 76
    return-void
.end method

.method public drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p2, "shapeCount"    # I

    .prologue
    .line 59
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V

    .line 61
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->draw(I)V

    .line 62
    return-void
.end method

.method public drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 146
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;I)V

    .line 147
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mDrawOnCanvas:Z

    .line 148
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mLeftMarg:I

    .line 149
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mTopMarg:I

    .line 150
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    .line 151
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    .line 152
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 153
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 154
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 155
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mDrawOnCanvas:Z

    .line 156
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V
    .locals 0
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 41
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 42
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->draw(I)V

    .line 43
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V
    .locals 2
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I

    .prologue
    .line 47
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mDrawOnCanvas:Z

    .line 49
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mLeftMarg:I

    .line 50
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mTopMarg:I

    .line 51
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 52
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 53
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mDrawOnCanvas:Z

    .line 55
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 102
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mDrawOnCanvas:Z

    .line 104
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mLeftMarg:I

    .line 105
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mTopMarg:I

    .line 106
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 107
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 108
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 109
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mDrawOnCanvas:Z

    .line 110
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 114
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V

    .line 115
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mDrawOnCanvas:Z

    .line 116
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mLeftMarg:I

    .line 117
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mTopMarg:I

    .line 118
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 119
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 120
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 121
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mDrawOnCanvas:Z

    .line 122
    return-void
.end method

.method public drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .prologue
    .line 96
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 97
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->draw(I)V

    .line 98
    return-void
.end method

.method public drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 82
    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p7

    move v4, p8

    move-object v5, p9

    invoke-super/range {v0 .. v5}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSSExcelShape(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mDrawOnCanvas:Z

    .line 85
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mLeftMarg:I

    .line 86
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mTopMarg:I

    .line 87
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 88
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 89
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mDrawOnCanvas:Z

    .line 91
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;I)V
    .locals 1
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 125
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 126
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->draw(I)V

    .line 127
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 131
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 132
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mDrawOnCanvas:Z

    .line 133
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mLeftMarg:I

    .line 134
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mTopMarg:I

    .line 135
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->height:F

    .line 136
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->width:F

    .line 137
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 138
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 139
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 140
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->mDrawOnCanvas:Z

    .line 141
    return-void
.end method
