.class public Lcom/samsung/thumbnail/office/ooxml/OOXMLTextOnlyInterfaceHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLTextOnlyHandler;
.source "OOXMLTextOnlyInterfaceHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/OOXMLTextOnlyInterfaceHandler$ITextOnlyConsumer;
    }
.end annotation


# instance fields
.field private consumer:Lcom/samsung/thumbnail/office/ooxml/OOXMLTextOnlyInterfaceHandler$ITextOnlyConsumer;


# direct methods
.method public constructor <init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLTextOnlyInterfaceHandler$ITextOnlyConsumer;)V
    .locals 0
    .param p1, "nsId"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "txtConsumer"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLTextOnlyInterfaceHandler$ITextOnlyConsumer;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLTextOnlyHandler;-><init>(ILjava/lang/String;)V

    .line 20
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLTextOnlyInterfaceHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/OOXMLTextOnlyInterfaceHandler$ITextOnlyConsumer;

    .line 21
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLTextOnlyInterfaceHandler$ITextOnlyConsumer;)V
    .locals 0
    .param p1, "elementName"    # Ljava/lang/String;
    .param p2, "txtConsumer"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLTextOnlyInterfaceHandler$ITextOnlyConsumer;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLTextOnlyHandler;-><init>(Ljava/lang/String;)V

    .line 14
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLTextOnlyInterfaceHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/OOXMLTextOnlyInterfaceHandler$ITextOnlyConsumer;

    .line 15
    return-void
.end method


# virtual methods
.method protected onText(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p1, "txt"    # Ljava/lang/String;
    .param p2, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLTextOnlyInterfaceHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/OOXMLTextOnlyInterfaceHandler$ITextOnlyConsumer;

    invoke-interface {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLTextOnlyInterfaceHandler$ITextOnlyConsumer;->consumeText(Ljava/lang/String;)V

    .line 25
    return-void
.end method
