.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/FillHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "FillHandler.java"


# instance fields
.field shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;


# direct methods
.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V
    .locals 5
    .param p1, "id"    # I
    .param p2, "consumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;
    .param p3, "shapeProp"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    .prologue
    const/4 v4, 0x0

    .line 16
    const-string/jumbo v2, "fill"

    invoke-direct {p0, p1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 18
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/FillHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    .line 19
    if-eqz p2, :cond_0

    .line 20
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 21
    .local v0, "noFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/FillHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "noFill"

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;

    invoke-direct {v1, p2, v4, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 25
    .local v1, "solidFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/FillHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "solidFill"

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    .end local v0    # "noFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;
    .end local v1    # "solidFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;
    :cond_0
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 32
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 34
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/FillHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    if-eqz v1, :cond_0

    .line 35
    const-string/jumbo v1, "r:id"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/FillHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 36
    .local v0, "idAttr":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 37
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/FillHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setRefId(Ljava/lang/String;)V

    .line 40
    .end local v0    # "idAttr":Ljava/lang/String;
    :cond_0
    return-void
.end method
