.class public Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
.super Ljava/lang/Object;
.source "OOXMLSequenceDescriptor.java"


# instance fields
.field public elementName:Ljava/lang/String;

.field public handler:Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

.field public nameSpaceId:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V
    .locals 0
    .param p1, "nameSpaceId"    # I
    .param p2, "elementName"    # Ljava/lang/String;
    .param p3, "handler"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;->nameSpaceId:I

    .line 16
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;->elementName:Ljava/lang/String;

    .line 17
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;->handler:Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    .line 18
    return-void
.end method


# virtual methods
.method public checkElement(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;[Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)Z
    .locals 3
    .param p1, "eleName"    # Ljava/lang/String;
    .param p2, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p3, "eleHandler"    # [Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    .prologue
    .line 22
    const/4 v0, 0x0

    .line 27
    .local v0, "val":Z
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;->elementName:Ljava/lang/String;

    iget v2, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;->nameSpaceId:I

    invoke-virtual {p2, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v2

    invoke-static {p1, v1, p2, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->checkElement(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)Z

    move-result v0

    .line 29
    if-eqz v0, :cond_0

    .line 30
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;->handler:Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    aput-object v2, p3, v1

    .line 33
    :cond_0
    return v0
.end method
