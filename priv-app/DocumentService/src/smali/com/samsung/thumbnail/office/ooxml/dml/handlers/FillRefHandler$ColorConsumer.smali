.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler$ColorConsumer;
.super Ljava/lang/Object;
.source "FillRefHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "ColorConsumer"
.end annotation


# instance fields
.field fillChConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;

.field fillProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;)V
    .locals 1
    .param p1, "fillChConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler$ColorConsumer;->fillProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .line 63
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler$ColorConsumer;->fillChConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;

    .line 64
    return-void
.end method


# virtual methods
.method public consumeColor(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 0
    .param p1, "clrScheme"    # Ljava/lang/String;
    .param p2, "val"    # Ljava/lang/String;
    .param p3, "fillProp"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    .param p4, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 80
    invoke-virtual {p3, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setFillRefColor(Ljava/lang/String;)V

    .line 82
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler$ColorConsumer;->fillProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .line 89
    return-void
.end method
