.class public abstract Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFSimpleShape;
.super Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;
.source "XSLFSimpleShape.java"


# instance fields
.field private shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

.field private final sheet:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;


# direct methods
.method protected constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;)V
    .locals 0
    .param p1, "shapeProperties"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "sheet"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFSimpleShape;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 20
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFSimpleShape;->sheet:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    .line 21
    return-void
.end method


# virtual methods
.method public getAnchor()Lorg/apache/poi/java/awt/geom/Rectangle2D;
    .locals 10

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFSimpleShape;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isTransform()Z

    move-result v0

    if-nez v0, :cond_0

    .line 38
    const/4 v1, 0x0

    .line 40
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;

    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFSimpleShape;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->shX:J

    long-to-double v2, v2

    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFSimpleShape;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->shY:J

    long-to-double v4, v4

    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFSimpleShape;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->shW:J

    long-to-double v6, v6

    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFSimpleShape;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    iget-wide v8, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->shH:J

    long-to-double v8, v8

    invoke-direct/range {v1 .. v9}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;-><init>(DDDD)V

    goto :goto_0
.end method

.method public getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFSimpleShape;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v0

    return-object v0
.end method

.method public getPrstShapeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFSimpleShape;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getPrstShapeName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShapeId()I
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFSimpleShape;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeId()I

    move-result v0

    return v0
.end method

.method public getShapeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFSimpleShape;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFSimpleShape;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    return-object v0
.end method

.method public getSheet()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFSimpleShape;->sheet:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    return-object v0
.end method

.method public isTextBox()Z
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFSimpleShape;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isTextBox()Z

    move-result v0

    return v0
.end method

.method public setAnchor(Lorg/apache/poi/java/awt/geom/Rectangle2D;)V
    .locals 0
    .param p1, "anchor"    # Lorg/apache/poi/java/awt/geom/Rectangle2D;

    .prologue
    .line 47
    return-void
.end method
