.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderExHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XDocBorderExHandler.java"


# instance fields
.field private borderObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;)V
    .locals 0
    .param p1, "eleName"    # Ljava/lang/String;
    .param p2, "borderObserver"    # Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 20
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderExHandler;->borderObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;

    .line 21
    return-void
.end method


# virtual methods
.method protected init()V
    .locals 11

    .prologue
    .line 24
    const/4 v9, 0x4

    new-array v6, v9, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 26
    .local v6, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;

    const-string/jumbo v9, "left"

    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderExHandler;->borderObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;

    invoke-direct {v2, v9, v10}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;)V

    .line 28
    .local v2, "leftBorder":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v9, "left"

    invoke-direct {v3, v9, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 30
    .local v3, "leftSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v9, 0x0

    aput-object v3, v6, v9

    .line 32
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;

    const-string/jumbo v9, "right"

    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderExHandler;->borderObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;

    invoke-direct {v4, v9, v10}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;)V

    .line 34
    .local v4, "rightBorder":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v9, "right"

    invoke-direct {v5, v9, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 36
    .local v5, "rightSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v9, 0x1

    aput-object v5, v6, v9

    .line 38
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;

    const-string/jumbo v9, "top"

    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderExHandler;->borderObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;

    invoke-direct {v7, v9, v10}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;)V

    .line 40
    .local v7, "topBorder":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v9, "top"

    invoke-direct {v8, v9, v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 42
    .local v8, "topSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v9, 0x2

    aput-object v8, v6, v9

    .line 44
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;

    const-string/jumbo v9, "bottom"

    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderExHandler;->borderObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;

    invoke-direct {v0, v9, v10}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;)V

    .line 46
    .local v0, "bottomBorder":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v9, "bottom"

    invoke-direct {v1, v9, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 48
    .local v1, "bottomSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v9, 0x3

    aput-object v1, v6, v9

    .line 50
    iput-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderExHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 51
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 56
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 57
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderExHandler;->init()V

    .line 58
    return-void
.end method
