.class public Lcom/samsung/thumbnail/office/ooxml/word/drawing/XDocDrawingHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XDocDrawingHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    const-string/jumbo v0, "drawing"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    .line 23
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/wp/InlineDrawingHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/InlineDrawingHandler;-><init>()V

    .line 24
    .local v1, "inlineHandler":Lcom/samsung/thumbnail/office/ooxml/dml/wp/InlineDrawingHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/drawing/XDocDrawingHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "inline"

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/AnchorDrawingHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/AnchorDrawingHandler;-><init>()V

    .line 27
    .local v0, "anchorHanlder":Lcom/samsung/thumbnail/office/ooxml/dml/wp/AnchorDrawingHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/drawing/XDocDrawingHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "anchor"

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    return-void
.end method


# virtual methods
.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 40
    const/16 v2, 0x1e

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v2

    invoke-static {p2, v2}, Lcom/samsung/thumbnail/office/ooxml/word/drawing/XDocDrawingHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)Ljava/lang/String;

    move-result-object v0

    .line 43
    .local v0, "ele":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/drawing/XDocDrawingHandler;->handlerMap:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    .line 45
    .local v1, "eleHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    if-eqz v1, :cond_0

    .line 46
    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->pushHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 47
    invoke-virtual {v1, p1, v0, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 49
    :cond_0
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 33
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 34
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/drawing/XDocDrawingHandler;->init()V

    .line 35
    return-void
.end method
