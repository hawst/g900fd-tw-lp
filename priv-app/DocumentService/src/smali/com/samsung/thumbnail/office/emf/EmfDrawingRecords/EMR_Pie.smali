.class public Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;
.super Ljava/lang/Object;
.source "EMR_Pie.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;


# instance fields
.field private RecordSize:I

.field private RecordType:I

.field protected bounds:Landroid/graphics/RectF;

.field private emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

.field fileContent:[B

.field private leftTopX:I

.field private leftTopY:I

.field private localPath:Landroid/graphics/Path;

.field private offset:I

.field private pointLFirstRadialX:F

.field private pointLFirstRadialY:F

.field private pointLSecondRadialX:F

.field private pointLSecondRadialY:F

.field private rightBottomX:I

.field private rightBottomY:I


# direct methods
.method public constructor <init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V
    .locals 1
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I
    .param p3, "emrGlobals"    # Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .prologue
    const/4 v0, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->bounds:Landroid/graphics/RectF;

    .line 65
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->localPath:Landroid/graphics/Path;

    .line 78
    iput-object p3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .line 79
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->fileContent:[B

    .line 80
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    .line 81
    return-void
.end method


# virtual methods
.method protected getAngles()[F
    .locals 14

    .prologue
    .line 426
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->pointLFirstRadialY:F

    float-to-double v2, v12

    .line 427
    .local v2, "d1":D
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->pointLFirstRadialX:F

    float-to-double v4, v12

    .line 428
    .local v4, "d2":D
    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v12

    double-to-float v1, v12

    .line 429
    .local v1, "f1":F
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->pointLSecondRadialY:F

    float-to-double v6, v12

    .line 430
    .local v6, "d3":D
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->pointLSecondRadialX:F

    float-to-double v8, v12

    .line 431
    .local v8, "d4":D
    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v12

    double-to-float v10, v12

    .line 434
    .local v10, "f2":F
    const/4 v12, 0x2

    new-array v0, v12, [F

    .line 437
    .local v0, "arrayOfFloat":[F
    const/4 v12, 0x0

    aput v1, v0, v12

    .line 438
    sub-float v11, v10, v1

    .line 439
    .local v11, "f4":F
    const/4 v12, 0x1

    aput v11, v0, v12

    .line 447
    return-object v0
.end method

.method public getPointLFirstRadialX()F
    .locals 1

    .prologue
    .line 203
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->pointLFirstRadialX:F

    return v0
.end method

.method public getPointLFirstRadialY()F
    .locals 1

    .prologue
    .line 222
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->pointLFirstRadialY:F

    return v0
.end method

.method public getPointLSecondRadialX()F
    .locals 1

    .prologue
    .line 241
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->pointLSecondRadialX:F

    return v0
.end method

.method public getPointLSecondRadialY()F
    .locals 1

    .prologue
    .line 260
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->pointLSecondRadialY:F

    return v0
.end method

.method public getRecordSize()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->RecordSize:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->RecordType:I

    return v0
.end method

.method public getleftTopX()I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->leftTopX:I

    return v0
.end method

.method public getleftTopY()I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->leftTopY:I

    return v0
.end method

.method public getrightBottomX()I
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->rightBottomX:I

    return v0
.end method

.method public getrightBottomY()I
    .locals 1

    .prologue
    .line 184
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->rightBottomY:I

    return v0
.end method

.method public prepare()V
    .locals 14

    .prologue
    .line 349
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->getleftTopX()I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->getleftTopY()I

    move-result v11

    int-to-float v11, v11

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->getrightBottomX()I

    move-result v12

    int-to-float v12, v12

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->getrightBottomY()I

    move-result v13

    int-to-float v13, v13

    invoke-virtual {p0, v10, v11, v12, v13}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->setBounds(FFFF)V

    .line 351
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->getPointLFirstRadialX()F

    move-result v10

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->getPointLFirstRadialY()F

    move-result v11

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->getPointLSecondRadialX()F

    move-result v12

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->getPointLSecondRadialY()F

    move-result v13

    invoke-virtual {p0, v10, v11, v12, v13}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->setStartEnd(FFFF)V

    .line 354
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->getAngles()[F

    move-result-object v0

    .line 355
    .local v0, "arrayOfFloat":[F
    new-instance v10, Landroid/graphics/Path;

    invoke-direct {v10}, Landroid/graphics/Path;-><init>()V

    iput-object v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->localPath:Landroid/graphics/Path;

    .line 356
    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->bounds:Landroid/graphics/RectF;

    .line 357
    .local v9, "localRectF":Landroid/graphics/RectF;
    const/4 v10, 0x0

    aget v10, v0, v10

    const/high16 v11, 0x43340000    # 180.0f

    mul-float/2addr v10, v11

    float-to-double v10, v10

    const-wide v12, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v10, v12

    double-to-float v7, v10

    .line 358
    .local v7, "i":F
    const/4 v10, 0x1

    aget v10, v0, v10

    const/high16 v11, 0x43340000    # 180.0f

    mul-float/2addr v10, v11

    float-to-double v10, v10

    const-wide v12, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v10, v12

    double-to-float v8, v10

    .line 360
    .local v8, "j":F
    const/4 v10, 0x0

    cmpg-float v10, v8, v10

    if-gez v10, :cond_0

    .line 361
    const/high16 v10, 0x43b40000    # 360.0f

    sub-float v8, v10, v8

    .line 363
    :cond_0
    iget-object v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->localPath:Landroid/graphics/Path;

    invoke-virtual {v10, v9, v7, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 364
    iget-object v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->bounds:Landroid/graphics/RectF;

    iget v1, v10, Landroid/graphics/RectF;->right:F

    .line 365
    .local v1, "f1":F
    iget-object v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->bounds:Landroid/graphics/RectF;

    iget v2, v10, Landroid/graphics/RectF;->left:F

    .line 366
    .local v2, "f2":F
    sub-float v10, v1, v2

    const/high16 v11, 0x40000000    # 2.0f

    div-float v3, v10, v11

    .line 367
    .local v3, "f3":F
    iget-object v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->bounds:Landroid/graphics/RectF;

    iget v4, v10, Landroid/graphics/RectF;->top:F

    .line 368
    .local v4, "f4":F
    iget-object v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->bounds:Landroid/graphics/RectF;

    iget v5, v10, Landroid/graphics/RectF;->bottom:F

    .line 369
    .local v5, "f5":F
    sub-float v10, v4, v5

    const/high16 v11, 0x40000000    # 2.0f

    div-float v6, v10, v11

    .line 370
    .local v6, "f6":F
    iget-object v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->localPath:Landroid/graphics/Path;

    invoke-virtual {v10, v3, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 371
    iget-object v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->localPath:Landroid/graphics/Path;

    invoke-virtual {v10}, Landroid/graphics/Path;->close()V

    .line 372
    return-void
.end method

.method public readEmfPie()V
    .locals 15

    .prologue
    .line 277
    const/4 v2, 0x0

    .line 279
    .local v2, "i":I
    const/4 v12, 0x4

    new-array v3, v12, [B

    .line 281
    .local v3, "intConvert":[B
    const/4 v2, 0x0

    :goto_0
    const/4 v12, 0x3

    if-gt v2, v12, :cond_0

    .line 282
    rsub-int/lit8 v12, v2, 0x3

    iget-object v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->fileContent:[B

    iget v14, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    add-int/2addr v14, v2

    aget-byte v13, v13, v14

    aput-byte v13, v3, v12

    .line 281
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 283
    :cond_0
    const/4 v12, 0x0

    invoke-static {v3, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 284
    .local v1, "Type":I
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    add-int/2addr v12, v2

    iput v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    .line 285
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->setRecordType(I)V

    .line 287
    const/4 v2, 0x0

    :goto_1
    const/4 v12, 0x3

    if-gt v2, v12, :cond_1

    .line 288
    rsub-int/lit8 v12, v2, 0x3

    iget-object v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->fileContent:[B

    iget v14, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    add-int/2addr v14, v2

    aget-byte v13, v13, v14

    aput-byte v13, v3, v12

    .line 287
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 289
    :cond_1
    const/4 v12, 0x0

    invoke-static {v3, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 290
    .local v0, "Size":I
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    add-int/2addr v12, v2

    iput v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    .line 291
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->setRecordSize(I)V

    .line 293
    const/4 v2, 0x0

    :goto_2
    const/4 v12, 0x3

    if-gt v2, v12, :cond_2

    .line 294
    rsub-int/lit8 v12, v2, 0x3

    iget-object v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->fileContent:[B

    iget v14, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    add-int/2addr v14, v2

    aget-byte v13, v13, v14

    aput-byte v13, v3, v12

    .line 293
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 295
    :cond_2
    const/4 v12, 0x0

    invoke-static {v3, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v4

    .line 296
    .local v4, "leftTopX":I
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    add-int/2addr v12, v2

    iput v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    .line 297
    invoke-virtual {p0, v4}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->setleftTopX(I)V

    .line 299
    const/4 v2, 0x0

    :goto_3
    const/4 v12, 0x3

    if-gt v2, v12, :cond_3

    .line 300
    rsub-int/lit8 v12, v2, 0x3

    iget-object v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->fileContent:[B

    iget v14, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    add-int/2addr v14, v2

    aget-byte v13, v13, v14

    aput-byte v13, v3, v12

    .line 299
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 301
    :cond_3
    const/4 v12, 0x0

    invoke-static {v3, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v5

    .line 302
    .local v5, "leftTopY":I
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    add-int/2addr v12, v2

    iput v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    .line 303
    invoke-virtual {p0, v5}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->setleftTopY(I)V

    .line 305
    const/4 v2, 0x0

    :goto_4
    const/4 v12, 0x3

    if-gt v2, v12, :cond_4

    .line 306
    rsub-int/lit8 v12, v2, 0x3

    iget-object v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->fileContent:[B

    iget v14, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    add-int/2addr v14, v2

    aget-byte v13, v13, v14

    aput-byte v13, v3, v12

    .line 305
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 307
    :cond_4
    const/4 v12, 0x0

    invoke-static {v3, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v10

    .line 308
    .local v10, "rightBottomX":I
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    add-int/2addr v12, v2

    iput v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    .line 309
    invoke-virtual {p0, v10}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->setrightBottomX(I)V

    .line 311
    const/4 v2, 0x0

    :goto_5
    const/4 v12, 0x3

    if-gt v2, v12, :cond_5

    .line 312
    rsub-int/lit8 v12, v2, 0x3

    iget-object v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->fileContent:[B

    iget v14, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    add-int/2addr v14, v2

    aget-byte v13, v13, v14

    aput-byte v13, v3, v12

    .line 311
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 313
    :cond_5
    const/4 v12, 0x0

    invoke-static {v3, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v11

    .line 314
    .local v11, "rightBottomY":I
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    add-int/2addr v12, v2

    iput v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    .line 315
    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->setrightBottomY(I)V

    .line 317
    const/4 v2, 0x0

    :goto_6
    const/4 v12, 0x3

    if-gt v2, v12, :cond_6

    .line 318
    rsub-int/lit8 v12, v2, 0x3

    iget-object v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->fileContent:[B

    iget v14, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    add-int/2addr v14, v2

    aget-byte v13, v13, v14

    aput-byte v13, v3, v12

    .line 317
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 319
    :cond_6
    const/4 v12, 0x0

    invoke-static {v3, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v6

    .line 320
    .local v6, "pointLFistRadialX":I
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    add-int/2addr v12, v2

    iput v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    .line 321
    invoke-virtual {p0, v6}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->setPointLFirstRadialX(I)V

    .line 323
    const/4 v2, 0x0

    :goto_7
    const/4 v12, 0x3

    if-gt v2, v12, :cond_7

    .line 324
    rsub-int/lit8 v12, v2, 0x3

    iget-object v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->fileContent:[B

    iget v14, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    add-int/2addr v14, v2

    aget-byte v13, v13, v14

    aput-byte v13, v3, v12

    .line 323
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 325
    :cond_7
    const/4 v12, 0x0

    invoke-static {v3, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v7

    .line 326
    .local v7, "pointLFistRadialY":I
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    add-int/2addr v12, v2

    iput v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    .line 327
    invoke-virtual {p0, v7}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->setpointLFirstRadialY(I)V

    .line 329
    const/4 v2, 0x0

    :goto_8
    const/4 v12, 0x3

    if-gt v2, v12, :cond_8

    .line 330
    rsub-int/lit8 v12, v2, 0x3

    iget-object v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->fileContent:[B

    iget v14, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    add-int/2addr v14, v2

    aget-byte v13, v13, v14

    aput-byte v13, v3, v12

    .line 329
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 331
    :cond_8
    const/4 v12, 0x0

    invoke-static {v3, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v8

    .line 332
    .local v8, "pointLSecndRadialX":I
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    add-int/2addr v12, v2

    iput v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    .line 333
    invoke-virtual {p0, v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->setPointLSecondRadialX(I)V

    .line 335
    const/4 v2, 0x0

    :goto_9
    const/4 v12, 0x3

    if-gt v2, v12, :cond_9

    .line 336
    rsub-int/lit8 v12, v2, 0x3

    iget-object v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->fileContent:[B

    iget v14, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    add-int/2addr v14, v2

    aget-byte v13, v13, v14

    aput-byte v13, v3, v12

    .line 335
    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    .line 337
    :cond_9
    const/4 v12, 0x0

    invoke-static {v3, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v9

    .line 338
    .local v9, "pointLSecndRadialY":I
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    add-int/2addr v12, v2

    iput v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->offset:I

    .line 339
    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->setPointLSecondRadialY(I)V

    .line 341
    return-void
.end method

.method public render(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 383
    const/4 v2, 0x0

    .line 384
    .local v2, "k":I
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;-><init>()V

    .line 385
    .local v0, "PaintPiePropertiesObject":Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 387
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 388
    .local v1, "emrSize":I
    if-lez v1, :cond_1

    .line 389
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 390
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandlePen:I

    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->gethandleNumber()I

    move-result v3

    if-ne v4, v3, :cond_4

    .line 395
    :cond_0
    if-ge v2, v1, :cond_1

    .line 396
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getColor()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/thumbnail/office/emf/Util/toColorAndroid;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 399
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getPenStyle()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/thumbnail/office/emf/Util/toStyleAndroid;->getStyle(I)Landroid/graphics/Paint$Style;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 402
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->localPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 407
    :cond_1
    if-eq v2, v1, :cond_2

    if-nez v1, :cond_3

    .line 409
    :cond_2
    const/16 v3, 0x8

    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v3, v4, :cond_5

    .line 416
    :cond_3
    :goto_1
    return-void

    .line 389
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 412
    :cond_5
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->localPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_1
.end method

.method public setBounds(FFFF)V
    .locals 5
    .param p1, "lx"    # F
    .param p2, "ly"    # F
    .param p3, "rx"    # F
    .param p4, "ry"    # F

    .prologue
    .line 464
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->bounds:Landroid/graphics/RectF;

    .line 465
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->bounds:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v1, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v1, p1, v1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v2, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v2, p2, v2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v3, p3, v3

    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v4, p4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 467
    return-void
.end method

.method public setPointLFirstRadialX(I)V
    .locals 1
    .param p1, "pointLFistRadialX"    # I

    .prologue
    .line 213
    int-to-float v0, p1

    iput v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->pointLFirstRadialX:F

    .line 214
    return-void
.end method

.method public setPointLSecondRadialX(I)V
    .locals 1
    .param p1, "pointLSecndRadialX"    # I

    .prologue
    .line 251
    int-to-float v0, p1

    iput v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->pointLSecondRadialX:F

    .line 252
    return-void
.end method

.method public setPointLSecondRadialY(I)V
    .locals 1
    .param p1, "pointLSecndRadialY"    # I

    .prologue
    .line 270
    int-to-float v0, p1

    iput v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->pointLSecondRadialY:F

    .line 271
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "RecrdSize"    # I

    .prologue
    .line 118
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->RecordSize:I

    .line 119
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "RecrdType"    # I

    .prologue
    .line 99
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->RecordType:I

    .line 100
    return-void
.end method

.method public setStartEnd(FFFF)V
    .locals 1
    .param p1, "lx"    # F
    .param p2, "ly"    # F
    .param p3, "rx"    # F
    .param p4, "ry"    # F

    .prologue
    .line 482
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v0, p1, v0

    iput v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->pointLFirstRadialX:F

    .line 483
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v0, p2, v0

    iput v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->pointLFirstRadialY:F

    .line 484
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v0, p3, v0

    iput v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->pointLSecondRadialX:F

    .line 485
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v0, p4, v0

    iput v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->pointLSecondRadialY:F

    .line 486
    return-void
.end method

.method public setleftTopX(I)V
    .locals 0
    .param p1, "lftTopX"    # I

    .prologue
    .line 137
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->leftTopX:I

    .line 138
    return-void
.end method

.method public setleftTopY(I)V
    .locals 0
    .param p1, "lftTopY"    # I

    .prologue
    .line 156
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->leftTopY:I

    .line 157
    return-void
.end method

.method public setpointLFirstRadialY(I)V
    .locals 1
    .param p1, "pointLFistRadialY"    # I

    .prologue
    .line 232
    int-to-float v0, p1

    iput v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->pointLFirstRadialY:F

    .line 233
    return-void
.end method

.method public setrightBottomX(I)V
    .locals 0
    .param p1, "rghtBottomX"    # I

    .prologue
    .line 175
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->rightBottomX:I

    .line 176
    return-void
.end method

.method public setrightBottomY(I)V
    .locals 0
    .param p1, "rghtBottomY"    # I

    .prologue
    .line 194
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Pie;->rightBottomY:I

    .line 195
    return-void
.end method
