.class public Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XPPTShapeHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler$IXfrmTxBodyHandler;


# instance fields
.field private autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

.field id:I

.field idMain:I

.field private shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

.field private slide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

.field private xwpfDiagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;


# direct methods
.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;)V
    .locals 1
    .param p1, "ID"    # I
    .param p2, "xwpfDiagram"    # Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    .prologue
    .line 44
    const-string/jumbo v0, "sp"

    invoke-direct {p0, p1, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 45
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->id:I

    .line 46
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->idMain:I

    .line 48
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->xwpfDiagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    .line 49
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1, "ID"    # I
    .param p2, "element"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 39
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->id:I

    .line 40
    const/16 v0, 0x33

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->idMain:I

    .line 41
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->xwpfDiagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->xwpfDiagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->addDiagramShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;)V

    .line 125
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 126
    return-void
.end method

.method protected init(I)V
    .locals 17
    .param p1, "ID"    # I

    .prologue
    .line 52
    const/4 v15, 0x6

    new-array v6, v15, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 54
    .local v6, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v13, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTTextBodyHandler;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move/from16 v0, p1

    invoke-direct {v13, v0, v15}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTTextBodyHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;)V

    .line 56
    .local v13, "txtBodyHandler":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTTextBodyHandler;
    new-instance v14, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->idMain:I

    const-string/jumbo v16, "txBody"

    move-object/from16 v0, v16

    invoke-direct {v14, v15, v0, v13}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 58
    .local v14, "txtSeq":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;
    const/4 v15, 0x0

    aput-object v14, v6, v15

    .line 60
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualShapePropHandler;

    const-string/jumbo v15, "nvSpPr"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v16, v0

    move/from16 v0, p1

    move-object/from16 v1, v16

    invoke-direct {v4, v0, v15, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualShapePropHandler;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 62
    .local v4, "nvShapeProp":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualShapePropHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->idMain:I

    const-string/jumbo v16, "nvSpPr"

    move-object/from16 v0, v16

    invoke-direct {v5, v15, v0, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 64
    .local v5, "nvShapeSeq":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;
    const/4 v15, 0x1

    aput-object v5, v6, v15

    .line 66
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move/from16 v0, p1

    invoke-direct {v7, v0, v15}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 68
    .local v7, "shapePropHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->idMain:I

    const-string/jumbo v16, "spPr"

    move-object/from16 v0, v16

    invoke-direct {v8, v15, v0, v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 70
    .local v8, "shapePropSeq":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;
    const/4 v15, 0x2

    aput-object v8, v6, v15

    .line 72
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapeStyleHandler;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move/from16 v0, p1

    invoke-direct {v9, v0, v15}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapeStyleHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 74
    .local v9, "shapeStyleHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapeStyleHandler;
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->idMain:I

    const-string/jumbo v16, "style"

    move-object/from16 v0, v16

    invoke-direct {v10, v15, v0, v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 76
    .local v10, "shapeStyleSeq":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;
    const/4 v15, 0x3

    aput-object v10, v6, v15

    .line 78
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualShapePropHandler;

    const-string/jumbo v15, "nvCxnSpPr"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v16, v0

    move/from16 v0, p1

    move-object/from16 v1, v16

    invoke-direct {v2, v0, v15, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualShapePropHandler;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 80
    .local v2, "cxnvShapeProp":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualShapePropHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->idMain:I

    const-string/jumbo v16, "nvCxnSpPr"

    move-object/from16 v0, v16

    invoke-direct {v3, v15, v0, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 82
    .local v3, "cxnvShapeSeq":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;
    const/4 v15, 0x4

    aput-object v3, v6, v15

    .line 84
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler;

    move/from16 v0, p1

    move-object/from16 v1, p0

    invoke-direct {v11, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler$IXfrmTxBodyHandler;)V

    .line 86
    .local v11, "txXfrmBodyHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler;
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;

    const-string/jumbo v15, "txXfrm"

    invoke-direct {v12, v15, v11}, Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 88
    .local v12, "txXfrmSeq":Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;
    const/4 v15, 0x5

    aput-object v12, v6, v15

    .line 90
    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 99
    return-void
.end method

.method public setTxXfrmPoints(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;JJ)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "x"    # J
    .param p4, "y"    # J

    .prologue
    .line 137
    return-void
.end method

.method public setTxXfrmRotation(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;I)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "rotation"    # I

    .prologue
    .line 130
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-static {p2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->rotationAngelToDegrees16_16(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setTextRotation(I)V

    .line 132
    return-void
.end method

.method public setTxXfrmSize(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;JJ)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "w"    # J
    .param p4, "h"    # J

    .prologue
    .line 142
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 104
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 105
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 107
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->xwpfDiagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->xwpfDiagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->getSheet()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->slide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    .line 109
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->slide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;)V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    .line 110
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setDiagramShape(Z)V

    .line 117
    :goto_0
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->id:I

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->init(I)V

    .line 118
    return-void

    .line 112
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->slide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    .line 113
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->slide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;)V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    .line 114
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->slide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->addShapes(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;)V

    goto :goto_0
.end method
