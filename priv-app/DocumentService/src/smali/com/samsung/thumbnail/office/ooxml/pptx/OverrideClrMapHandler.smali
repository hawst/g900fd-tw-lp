.class public Lcom/samsung/thumbnail/office/ooxml/pptx/OverrideClrMapHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "OverrideClrMapHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/pptx/OverrideClrMapHandler$IClrMapObserver;
    }
.end annotation


# instance fields
.field mClrMapObserver:Lcom/samsung/thumbnail/office/ooxml/pptx/OverrideClrMapHandler$IClrMapObserver;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/OverrideClrMapHandler$IClrMapObserver;)V
    .locals 2
    .param p1, "clrMapObserver"    # Lcom/samsung/thumbnail/office/ooxml/pptx/OverrideClrMapHandler$IClrMapObserver;

    .prologue
    .line 17
    const/16 v0, 0x1f

    const-string/jumbo v1, "overrideClrMapping"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 18
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/OverrideClrMapHandler;->mClrMapObserver:Lcom/samsung/thumbnail/office/ooxml/pptx/OverrideClrMapHandler$IClrMapObserver;

    .line 19
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 24
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 27
    .local v0, "colorMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p3}, Lorg/xml/sax/Attributes;->getLength()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 28
    invoke-interface {p3, v1}, Lorg/xml/sax/Attributes;->getQName(I)Ljava/lang/String;

    move-result-object v2

    .line 29
    .local v2, "name":Ljava/lang/String;
    invoke-interface {p3, v1}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object v3

    .line 30
    .local v3, "value":Ljava/lang/String;
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 31
    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 35
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "value":Ljava/lang/String;
    :cond_1
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/OverrideClrMapHandler;->mClrMapObserver:Lcom/samsung/thumbnail/office/ooxml/pptx/OverrideClrMapHandler$IClrMapObserver;

    invoke-interface {v4, p1, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/OverrideClrMapHandler$IClrMapObserver;->setClrMap(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/util/HashMap;)V

    .line 36
    return-void
.end method
