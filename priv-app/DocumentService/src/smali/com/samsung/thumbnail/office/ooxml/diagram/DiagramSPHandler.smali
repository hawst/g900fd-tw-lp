.class public Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DiagramSPHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$ICustGeomHandler;
.implements Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IGradFillHandler;
.implements Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IPstgeomHandler;
.implements Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$ISolidFillHandler;
.implements Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IXfrmHandlerDgm;
.implements Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprLineHandler$ILineFillHandler;
.implements Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler$IBodyPrHandler;
.implements Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler$IParagraphHandler;
.implements Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler$IXfrmTxBodyHandler;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;


# static fields
.field public static final TAG:Ljava/lang/String; = "DiagramSPHandler"


# instance fields
.field private shapeCount:I

.field private shapeId:Ljava/lang/String;

.field private theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

.field private xwpfDiagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

.field private xwpfShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

.field private xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;


# direct methods
.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V
    .locals 11
    .param p1, "ID"    # I
    .param p2, "xwpfDiagram"    # Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;
    .param p3, "theme"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .prologue
    const/4 v10, 0x0

    .line 69
    const-string/jumbo v9, "sp"

    invoke-direct {p0, p1, v9}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 63
    iput v10, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->shapeCount:I

    .line 71
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfDiagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    .line 72
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .line 74
    const/4 v9, 0x4

    new-array v3, v9, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 76
    .local v3, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramNvSpPrHandler;

    invoke-direct {v2, p1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramNvSpPrHandler;-><init>(I)V

    .line 77
    .local v2, "nvspprhandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramNvSpPrHandler;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;

    const-string/jumbo v9, "nvSpPr"

    invoke-direct {v0, v9, v2}, Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 79
    .local v0, "dsp1Seq":Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;
    aput-object v0, v3, v10

    .line 81
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler;

    invoke-direct {v4, p1, p0, p3}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 82
    .local v4, "spPrhandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;

    const-string/jumbo v9, "spPr"

    invoke-direct {v1, v9, v4}, Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 84
    .local v1, "dsp2Seq":Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;
    const/4 v9, 0x1

    aput-object v1, v3, v9

    .line 93
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler;

    invoke-direct {v5, p1, p0}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler$IXfrmTxBodyHandler;)V

    .line 95
    .local v5, "txXfrmBodyHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;

    const-string/jumbo v9, "txXfrm"

    invoke-direct {v6, v9, v5}, Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 97
    .local v6, "txXfrmSeq":Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;
    const/4 v9, 0x2

    aput-object v6, v3, v9

    .line 99
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler;

    invoke-direct {v7, p1, p0}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;)V

    .line 100
    .local v7, "txtBodyHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;

    const-string/jumbo v9, "txBody"

    invoke-direct {v8, v9, v7}, Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 102
    .local v8, "txtSeq":Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;
    const/4 v9, 0x3

    aput-object v8, v3, v9

    .line 111
    iput-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 112
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 132
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->shapeId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->setId(Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfDiagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->addDiagramShape(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V

    .line 136
    return-void
.end method

.method public setAdjVal(Ljava/lang/String;)V
    .locals 4
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 166
    const/4 v1, 0x0

    .line 167
    .local v1, "value":I
    if-eqz p1, :cond_0

    .line 168
    const-string/jumbo v2, "val"

    const-string/jumbo v3, ""

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 169
    .local v0, "newVal":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/thumbnail/util/Utils;->isValidInteger(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 170
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 175
    .end local v0    # "newVal":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {v2, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setAdjVal(I)V

    .line 176
    return-void
.end method

.method public setBlipEmbed(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "blipEmbed"    # Ljava/lang/String;

    .prologue
    .line 439
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setRefId(Ljava/lang/String;)V

    .line 440
    return-void
.end method

.method public setBodyProperty(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "bodyProp"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setXWPFBodyProp(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;)V

    .line 181
    return-void
.end method

.method public setGradFillProperty(Ljava/lang/String;)V
    .locals 4
    .param p1, "gradFillColor"    # Ljava/lang/String;

    .prologue
    .line 576
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setGradFillColor(Ljava/lang/String;)V

    .line 578
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    const-wide/32 v2, 0xc350

    invoke-virtual {v0, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setFillAlpha(J)V

    .line 579
    return-void
.end method

.method public setLineFillProperty(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 7
    .param p1, "fillProps"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    const/4 v6, 0x0

    .line 523
    if-eqz p1, :cond_6

    .line 525
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/thumbnail/util/Utils;->replaceThemeColorKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 529
    .local v1, "fillColor":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x23

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 533
    :goto_0
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {v2, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setStrokeColor(Ljava/lang/String;)V

    .line 535
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 536
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setLineColorLumMod(J)V

    .line 539
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumOff()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 540
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumOff()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setLineColorLumOff(J)V

    .line 543
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getTint()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 544
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getTint()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setLineColorTint(J)V

    .line 547
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getShade()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 548
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getShade()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setLineColorShade(J)V

    .line 551
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getSatMod()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 552
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getSatMod()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setLineColorSatMod(J)V

    .line 555
    :cond_4
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getSatOff()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 556
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getSatOff()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setLineColorSatOff(J)V

    .line 560
    :cond_5
    invoke-virtual {p1, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setForeColor(Ljava/lang/String;)V

    .line 561
    invoke-virtual {p1, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setLumMod(Ljava/lang/String;)V

    .line 562
    invoke-virtual {p1, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setLumOff(Ljava/lang/String;)V

    .line 563
    invoke-virtual {p1, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setShade(Ljava/lang/String;)V

    .line 564
    invoke-virtual {p1, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setTint(Ljava/lang/String;)V

    .line 565
    invoke-virtual {p1, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setSatMod(Ljava/lang/String;)V

    .line 566
    invoke-virtual {p1, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setSatOff(Ljava/lang/String;)V

    .line 567
    invoke-virtual {p1, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setAlpha(Ljava/lang/String;)V

    .line 568
    invoke-virtual {p1, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setAlphaOff(Ljava/lang/String;)V

    .line 570
    .end local v1    # "fillColor":Ljava/lang/String;
    :cond_6
    return-void

    .line 530
    .restart local v1    # "fillColor":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 531
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-virtual {v2, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method

.method public setParagraphList(Ljava/util/ArrayList;)V
    .locals 36
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 185
    .local p1, "paraList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;>;"
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 186
    .local v5, "buMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    const/4 v3, -0x1

    .line 187
    .local v3, "buCount":I
    const/16 v25, -0x1

    .local v25, "preLevel":I
    const/4 v4, -0x1

    .line 188
    .local v4, "buLevel":I
    move-object/from16 v23, p1

    .line 191
    .local v23, "paraLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;>;"
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_0
    invoke-interface/range {v23 .. v23}, Ljava/util/List;->size()I

    move-result v31

    move/from16 v0, v17

    move/from16 v1, v31

    if-ge v0, v1, :cond_7

    .line 192
    new-instance v9, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-direct {v9}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    .line 194
    .local v9, "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    move-object/from16 v0, v23

    move/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    .line 195
    .local v22, "para":Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;
    const/16 v20, 0x0

    .line 196
    .local v20, "level":I
    const/4 v2, 0x0

    .line 197
    .local v2, "buChar":Ljava/lang/String;
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v24

    .line 199
    .local v24, "paraProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    if-eqz v24, :cond_0

    .line 200
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getLevel()I

    move-result v20

    .line 202
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getMarginLeft()I

    move-result v21

    .line 203
    .local v21, "marginSpace":I
    mul-int/lit8 v31, v20, 0x32

    add-int v21, v21, v31

    .line 204
    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v31, v0

    move/from16 v0, v31

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    .line 207
    .end local v21    # "marginSpace":I
    :cond_0
    move/from16 v0, v25

    move/from16 v1, v20

    if-eq v0, v1, :cond_2

    .line 208
    move/from16 v0, v25

    move/from16 v1, v20

    if-ge v0, v1, :cond_1

    .line 209
    const/4 v3, -0x1

    .line 211
    :cond_1
    move/from16 v4, v25

    .line 214
    :cond_2
    move/from16 v25, v20

    .line 216
    const-string/jumbo v6, ""

    .line 218
    .local v6, "buText":Ljava/lang/String;
    if-eqz v24, :cond_6

    .line 220
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getJustify()Ljava/lang/String;

    move-result-object v19

    .line 222
    .local v19, "justification":Ljava/lang/String;
    const-string/jumbo v31, "right"

    move-object/from16 v0, v31

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_8

    .line 223
    sget-object v31, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->RIGHT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v31

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    .line 230
    :goto_1
    sget-object v31, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;->AUTO:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;

    move-object/from16 v0, v31

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineRule(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;)V

    .line 232
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getLineSpacing()F

    move-result v31

    const/high16 v32, 0x43700000    # 240.0f

    div-float v31, v31, v32

    move/from16 v0, v31

    float-to-int v0, v0

    move/from16 v31, v0

    invoke-static/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v31

    move/from16 v0, v31

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineSpacing(F)V

    .line 235
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getLineSpacing()F

    move-result v31

    const/16 v32, 0x0

    cmpl-float v31, v31, v32

    if-eqz v31, :cond_3

    .line 236
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getLineSpacing()F

    move-result v31

    move/from16 v0, v31

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineSpacing(F)V

    .line 240
    :cond_3
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getBulletChar()Ljava/lang/String;

    move-result-object v31

    if-eqz v31, :cond_6

    .line 241
    move/from16 v0, v20

    if-eq v4, v0, :cond_4

    .line 242
    move/from16 v0, v20

    if-le v4, v0, :cond_a

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v31

    if-eqz v31, :cond_a

    .line 243
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Ljava/lang/Integer;

    invoke-virtual/range {v31 .. v31}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 248
    :cond_4
    :goto_2
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getRunList()Ljava/util/ArrayList;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->size()I

    move-result v31

    if-eqz v31, :cond_5

    .line 249
    add-int/lit8 v3, v3, 0x1

    .line 251
    :cond_5
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getBulletChar()Ljava/lang/String;

    move-result-object v2

    .line 253
    invoke-static {v2}, Lcom/samsung/thumbnail/util/Utils;->isValidInteger(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_b

    .line 254
    move-object v6, v2

    .line 266
    :goto_3
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v5, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    .end local v19    # "justification":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getXWPFBodyProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;->getvAlign()Ljava/lang/String;

    move-result-object v30

    .line 271
    .local v30, "vAlign":Ljava/lang/String;
    const-string/jumbo v31, "ctr"

    move-object/from16 v0, v31

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_e

    .line 272
    sget-object v31, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->CENTERED:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-object/from16 v0, v31

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    .line 279
    :goto_4
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getRunList()Ljava/util/ArrayList;

    move-result-object v27

    .line 281
    .local v27, "runList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;>;"
    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v31

    if-nez v31, :cond_11

    .line 282
    new-instance v11, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-direct {v11}, Lcom/samsung/thumbnail/customview/word/Run;-><init>()V

    .line 283
    .local v11, "drawRun":Lcom/samsung/thumbnail/customview/word/Run;
    const-string/jumbo v31, ""

    move-object/from16 v0, v31

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    .line 284
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v8

    .line 285
    .local v8, "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    if-eqz v8, :cond_10

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v31

    const/16 v32, -0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-eq v0, v1, :cond_10

    .line 287
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v31

    div-int/lit8 v16, v31, 0x2

    .line 288
    .local v16, "fontSize":I
    invoke-static/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v31

    const/high16 v32, 0x40000000    # 2.0f

    div-float v31, v31, v32

    move/from16 v0, v31

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 293
    .end local v16    # "fontSize":I
    :goto_5
    invoke-virtual {v9, v11}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 417
    .end local v2    # "buChar":Ljava/lang/String;
    .end local v6    # "buText":Ljava/lang/String;
    .end local v8    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .end local v9    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v11    # "drawRun":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v20    # "level":I
    .end local v22    # "para":Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;
    .end local v24    # "paraProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    .end local v27    # "runList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;>;"
    .end local v30    # "vAlign":Ljava/lang/String;
    :cond_7
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->clear()V

    .line 418
    return-void

    .line 224
    .restart local v2    # "buChar":Ljava/lang/String;
    .restart local v6    # "buText":Ljava/lang/String;
    .restart local v9    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .restart local v19    # "justification":Ljava/lang/String;
    .restart local v20    # "level":I
    .restart local v22    # "para":Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;
    .restart local v24    # "paraProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    :cond_8
    const-string/jumbo v31, "center"

    move-object/from16 v0, v31

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_9

    .line 225
    sget-object v31, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->CENTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v31

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_1

    .line 227
    :cond_9
    sget-object v31, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->LEFT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v31

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_1

    .line 244
    :cond_a
    move/from16 v0, v20

    if-ne v4, v0, :cond_4

    .line 245
    const/4 v3, -0x1

    goto/16 :goto_2

    .line 255
    :cond_b
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v31

    const/16 v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-le v0, v1, :cond_d

    .line 256
    const/16 v31, 0x0

    move/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v31

    const/16 v32, 0x7f

    move/from16 v0, v31

    move/from16 v1, v32

    if-gt v0, v1, :cond_c

    const/16 v31, 0x0

    move/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v31

    const/16 v32, 0x6f

    move/from16 v0, v31

    move/from16 v1, v32

    if-eq v0, v1, :cond_c

    move-object v6, v2

    :goto_6
    goto/16 :goto_3

    :cond_c
    const/16 v31, 0x0

    move/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v31

    const/16 v32, 0x0

    invoke-static/range {v31 .. v32}, Lcom/samsung/thumbnail/util/Utils;->getUnicodeForBullet(IZ)Ljava/lang/String;

    move-result-object v6

    goto :goto_6

    .line 263
    :cond_d
    const-string/jumbo v6, "\u25cf"

    goto/16 :goto_3

    .line 273
    .end local v19    # "justification":Ljava/lang/String;
    .restart local v30    # "vAlign":Ljava/lang/String;
    :cond_e
    const-string/jumbo v31, "b"

    move-object/from16 v0, v31

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_f

    .line 274
    sget-object v31, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->BOTTOM:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-object/from16 v0, v31

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    goto/16 :goto_4

    .line 276
    :cond_f
    sget-object v31, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->TOP:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-object/from16 v0, v31

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    goto/16 :goto_4

    .line 291
    .restart local v8    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .restart local v11    # "drawRun":Lcom/samsung/thumbnail/customview/word/Run;
    .restart local v27    # "runList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;>;"
    :cond_10
    const/high16 v31, 0x41400000    # 12.0f

    move/from16 v0, v31

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    goto/16 :goto_5

    .line 297
    .end local v8    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .end local v11    # "drawRun":Lcom/samsung/thumbnail/customview/word/Run;
    :cond_11
    const/16 v18, 0x0

    .local v18, "j":I
    :goto_7
    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v31

    move/from16 v0, v18

    move/from16 v1, v31

    if-ge v0, v1, :cond_28

    .line 298
    move-object/from16 v0, v27

    move/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    .line 299
    .local v26, "run":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v8

    .line 300
    .restart local v8    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    new-instance v29, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-direct/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/word/Run;-><init>()V

    .line 302
    .local v29, "textRun":Lcom/samsung/thumbnail/customview/word/Run;
    const-string/jumbo v7, ""

    .line 304
    .local v7, "bullet":Ljava/lang/String;
    invoke-virtual {v9}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getAlignment()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-result-object v31

    if-eqz v31, :cond_14

    invoke-virtual {v9}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getAlignment()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-result-object v31

    sget-object v32, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->LEFT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    if-ne v0, v1, :cond_14

    .line 306
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getRotation()I

    move-result v31

    if-nez v31, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getTextRotation()I

    move-result v31

    if-eqz v31, :cond_13

    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getRotation()I

    move-result v31

    if-eqz v31, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getTextRotation()I

    move-result v31

    if-eqz v31, :cond_14

    .line 310
    :cond_13
    if-eqz v24, :cond_14

    .line 311
    const/16 v31, 0x1

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setTabStatus(Z)V

    .line 312
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getDefTabSize()I

    move-result v31

    div-int/lit8 v31, v31, 0x2

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpaceAfter()F

    move-result v32

    move/from16 v0, v32

    float-to-int v0, v0

    move/from16 v32, v0

    div-int/lit8 v32, v32, 0x3

    add-int v31, v31, v32

    move/from16 v0, v31

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setDefTabSpacing(I)V

    .line 318
    :cond_14
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getText()Ljava/lang/String;

    move-result-object v31

    const-string/jumbo v32, "<br>"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v31

    if-eqz v31, :cond_16

    .line 319
    move-object v10, v9

    .line 320
    .local v10, "docParaTemp":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    new-instance v9, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .end local v9    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-direct {v9}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    .line 321
    .restart local v9    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v31

    move/from16 v0, v31

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    .line 323
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLineSpacing()F

    move-result v31

    const/16 v32, 0x0

    cmpl-float v31, v31, v32

    if-eqz v31, :cond_15

    .line 324
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLineSpacing()F

    move-result v31

    move/from16 v0, v31

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineSpacing(F)V

    .line 297
    .end local v10    # "docParaTemp":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    :cond_15
    :goto_8
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_7

    .line 329
    :cond_16
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v15

    .line 330
    .local v15, "fontColor":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    if-nez v15, :cond_17

    .line 331
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v15

    .line 334
    :cond_17
    if-eqz v15, :cond_25

    .line 335
    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lcom/samsung/thumbnail/util/Utils;->replaceThemeColorKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 339
    .local v14, "fontClr":Ljava/lang/String;
    :try_start_0
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v32, 0x23

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 344
    :goto_9
    :try_start_1
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v32, 0x23

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v31

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 369
    .end local v14    # "fontClr":Ljava/lang/String;
    :cond_18
    :goto_a
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v31

    if-lez v31, :cond_19

    .line 371
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v31

    mul-int/lit8 v31, v31, 0x64

    div-int/lit8 v16, v31, 0x2

    .line 372
    .restart local v16    # "fontSize":I
    invoke-static/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v31

    const/high16 v32, 0x40000000    # 2.0f

    div-float v31, v31, v32

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 375
    .end local v16    # "fontSize":I
    :cond_19
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getBold()Z

    move-result v31

    if-eqz v31, :cond_1a

    .line 376
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/word/Run;->setTextBold()V

    .line 377
    :cond_1a
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getItalic()Z

    move-result v31

    if-eqz v31, :cond_1b

    .line 378
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/word/Run;->setTextItalics()V

    .line 379
    :cond_1b
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getUnderline()Z

    move-result v31

    if-eqz v31, :cond_1c

    .line 380
    const/16 v31, 0x1

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setUnderlineText(Z)V

    .line 381
    :cond_1c
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getStrike()Z

    move-result v31

    if-nez v31, :cond_1d

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getDStrike()Z

    move-result v31

    if-eqz v31, :cond_1e

    .line 382
    :cond_1d
    const/16 v31, 0x1

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setStrikeThruText(Z)V

    .line 383
    :cond_1e
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getSmallCaps()Z

    move-result v31

    if-eqz v31, :cond_1f

    .line 384
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/word/Run;->setSMALLLetterON()V

    .line 385
    :cond_1f
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getCaps()Z

    move-result v31

    if-eqz v31, :cond_20

    .line 386
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/word/Run;->setCAPLetterON()V

    .line 388
    :cond_20
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getStyleId()Ljava/lang/String;

    move-result-object v31

    if-eqz v31, :cond_21

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getStyleId()Ljava/lang/String;

    move-result-object v31

    const-string/jumbo v32, "Hyperlink"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-nez v31, :cond_22

    :cond_21
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getHLink()Ljava/lang/String;

    move-result-object v31

    if-eqz v31, :cond_23

    .line 390
    :cond_22
    const/16 v31, 0x1

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setUnderlineText(Z)V

    .line 391
    const v31, -0xffff01

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 394
    :cond_23
    if-nez v18, :cond_24

    .line 395
    const-string/jumbo v31, ""

    move-object/from16 v0, v31

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-nez v31, :cond_24

    .line 396
    const/16 v31, 0x0

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setUnderlineText(Z)V

    .line 397
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v31

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const/16 v32, 0x20

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 401
    :cond_24
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v32, " "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getText()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    .line 404
    .local v28, "textInRun":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v31

    if-eqz v31, :cond_27

    .line 405
    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    .line 410
    :goto_b
    move-object/from16 v0, v29

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_8

    .line 340
    .end local v28    # "textInRun":Ljava/lang/String;
    .restart local v14    # "fontClr":Ljava/lang/String;
    :catch_0
    move-exception v12

    .line 341
    .local v12, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v14}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    goto/16 :goto_9

    .line 345
    .end local v12    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v12

    .line 346
    .restart local v12    # "e":Ljava/lang/Exception;
    const/high16 v31, -0x1000000

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    goto/16 :goto_a

    .line 352
    .end local v12    # "e":Ljava/lang/Exception;
    .end local v14    # "fontClr":Ljava/lang/String;
    :cond_25
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getFillColor()Ljava/lang/String;

    move-result-object v13

    .line 354
    .local v13, "fillColor":Ljava/lang/String;
    if-nez v13, :cond_26

    .line 355
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getGradFillColor()Ljava/lang/String;

    move-result-object v13

    .line 359
    :cond_26
    if-eqz v13, :cond_18

    :try_start_2
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v32, 0x23

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v31

    const/16 v32, -0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-eq v0, v1, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getFillAlpha()J

    move-result-wide v32

    const-wide/16 v34, 0x0

    cmp-long v31, v32, v34

    if-eqz v31, :cond_18

    .line 362
    const/16 v31, -0x1

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_a

    .line 364
    :catch_2
    move-exception v12

    .line 365
    .restart local v12    # "e":Ljava/lang/Exception;
    const/16 v31, -0x1

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    goto/16 :goto_a

    .line 407
    .end local v12    # "e":Ljava/lang/Exception;
    .end local v13    # "fillColor":Ljava/lang/String;
    .restart local v28    # "textInRun":Ljava/lang/String;
    :cond_27
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v31

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    goto/16 :goto_b

    .line 413
    .end local v7    # "bullet":Ljava/lang/String;
    .end local v8    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .end local v15    # "fontColor":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .end local v26    # "run":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    .end local v28    # "textInRun":Ljava/lang/String;
    .end local v29    # "textRun":Lcom/samsung/thumbnail/customview/word/Run;
    :cond_28
    sget-object v31, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-object/from16 v0, v31

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 414
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v9}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 191
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_0
.end method

.method public setPathDescriptor(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V
    .locals 5
    .param p1, "pathDesc"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    .prologue
    .line 449
    if-eqz p1, :cond_1

    .line 450
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {v2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setPathDescriptor(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V

    .line 451
    const-string/jumbo v0, "lineConnector"

    .line 452
    .local v0, "shapeName":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->hasArcTo()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 453
    const-string/jumbo v0, "arcConnector"

    .line 455
    :cond_0
    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->parseStringPrefix(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-result-object v1

    .line 456
    .local v1, "shapeType":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {v2, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setPrstName(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    .line 457
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->getType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->shapeCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->shapeId:Ljava/lang/String;

    .line 460
    .end local v0    # "shapeName":Ljava/lang/String;
    .end local v1    # "shapeType":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;
    :cond_1
    return-void
.end method

.method public setPoints(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;JJ)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "x"    # J
    .param p4, "y"    # J

    .prologue
    .line 146
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setPoints(JJ)V

    .line 147
    return-void
.end method

.method public setPrst(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 4
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "prst"    # Ljava/lang/String;

    .prologue
    .line 156
    if-eqz p2, :cond_0

    .line 157
    invoke-static {p2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->parseStringPrefix(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-result-object v0

    .line 158
    .local v0, "shapeType":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setPrstName(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    .line 159
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->getType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->shapeCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->shapeId:Ljava/lang/String;

    .line 162
    .end local v0    # "shapeType":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;
    :cond_0
    return-void
.end method

.method public setRotation(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;I)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "rotation"    # I

    .prologue
    .line 140
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-static {p2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->rotationAngelToDegrees16_16(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setRotation(I)V

    .line 142
    return-void
.end method

.method public setShapeFillProperty(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 7
    .param p1, "fillProps"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    const/4 v6, 0x0

    .line 464
    if-eqz p1, :cond_8

    .line 466
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/thumbnail/util/Utils;->replaceThemeColorKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 470
    .local v1, "fillColor":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x23

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 474
    :goto_0
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {v2, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setFillColor(Ljava/lang/String;)V

    .line 476
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 477
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setFillColorLumMod(J)V

    .line 480
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumOff()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 481
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumOff()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setFillColorLumOff(J)V

    .line 484
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getTint()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 485
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getTint()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setFillColorTint(J)V

    .line 488
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getShade()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 489
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getShade()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setFillColorShade(J)V

    .line 492
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getSatMod()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 493
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getSatMod()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setFillColorSatMod(J)V

    .line 496
    :cond_4
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getSatOff()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 497
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getSatOff()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setFillColorSatOff(J)V

    .line 500
    :cond_5
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getAlpha()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 501
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getAlpha()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setFillAlpha(J)V

    .line 504
    :cond_6
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getAlphaOff()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 505
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getAlphaOff()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setFillAlphaOff(J)V

    .line 509
    :cond_7
    invoke-virtual {p1, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setForeColor(Ljava/lang/String;)V

    .line 510
    invoke-virtual {p1, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setLumMod(Ljava/lang/String;)V

    .line 511
    invoke-virtual {p1, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setLumOff(Ljava/lang/String;)V

    .line 512
    invoke-virtual {p1, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setShade(Ljava/lang/String;)V

    .line 513
    invoke-virtual {p1, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setTint(Ljava/lang/String;)V

    .line 514
    invoke-virtual {p1, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setSatMod(Ljava/lang/String;)V

    .line 515
    invoke-virtual {p1, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setSatOff(Ljava/lang/String;)V

    .line 516
    invoke-virtual {p1, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setAlpha(Ljava/lang/String;)V

    .line 517
    invoke-virtual {p1, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setAlphaOff(Ljava/lang/String;)V

    .line 519
    .end local v1    # "fillColor":Ljava/lang/String;
    :cond_8
    return-void

    .line 471
    .restart local v1    # "fillColor":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 472
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-virtual {v2, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method

.method public setSize(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;JJ)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "w"    # J
    .param p4, "h"    # J

    .prologue
    .line 151
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setSize(JJ)V

    .line 152
    return-void
.end method

.method public setTile(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "flip"    # Ljava/lang/String;

    .prologue
    .line 445
    return-void
.end method

.method public setTxXfrmPoints(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;JJ)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "x"    # J
    .param p4, "y"    # J

    .prologue
    .line 430
    return-void
.end method

.method public setTxXfrmRotation(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;I)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "rotation"    # I

    .prologue
    .line 423
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-static {p2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->rotationAngelToDegrees16_16(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setTextRotation(I)V

    .line 425
    return-void
.end method

.method public setTxXfrmSize(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;JJ)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "w"    # J
    .param p4, "h"    # J

    .prologue
    .line 435
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 117
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 119
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    .line 120
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setDiagramShape(Z)V

    .line 122
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfDiagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->getShapeCount()I

    move-result v0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->shapeCount:I

    .line 123
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->shapeCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->shapeCount:I

    .line 124
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfDiagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    iget v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->shapeCount:I

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->setShapeCount(I)V

    .line 126
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;->xwpfShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    .line 127
    return-void
.end method
