.class public interface abstract Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFConstants;
.super Ljava/lang/Object;
.source "WMFConstants.java"


# static fields
.field public static final ALTERNATE:I = 0x1

.field public static final ANSI_CHARSET:B = 0x0t

.field public static final BLACKNESS:I = 0x42

.field public static final BS_DIBPATTERN:I = 0x5

.field public static final BS_HATCHED:I = 0x2

.field public static final BS_HOLLOW:I = 0x1

.field public static final BS_NULL:I = 0x1

.field public static final BS_PATTERN:I = 0x3

.field public static final BS_SOLID:I = 0x0

.field public static final CHARSET_ANSI:Ljava/lang/String; = "ISO-8859-1"

.field public static final CHARSET_ARABIC:Ljava/lang/String; = "windows-1256"

.field public static final CHARSET_CHINESEBIG5:Ljava/lang/String; = "Big5"

.field public static final CHARSET_CYRILLIC:Ljava/lang/String; = "windows-1251"

.field public static final CHARSET_DEFAULT:Ljava/lang/String; = "US-ASCII"

.field public static final CHARSET_EASTEUROPE:Ljava/lang/String; = "cp1250"

.field public static final CHARSET_GB2312:Ljava/lang/String; = "GB2312"

.field public static final CHARSET_GREEK:Ljava/lang/String; = "windows-1253"

.field public static final CHARSET_HANGUL:Ljava/lang/String; = "cp949"

.field public static final CHARSET_HEBREW:Ljava/lang/String; = "windows-1255"

.field public static final CHARSET_JOHAB:Ljava/lang/String; = "x-Johab"

.field public static final CHARSET_OEM:Ljava/lang/String; = "cp437"

.field public static final CHARSET_SHIFTJIS:Ljava/lang/String; = "Shift_JIS"

.field public static final CHARSET_THAI:Ljava/lang/String; = "cp874"

.field public static final CHARSET_TURKISH:Ljava/lang/String; = "cp1254"

.field public static final CHARSET_VIETNAMESE:Ljava/lang/String; = "cp1258"

.field public static final CLIP_CHARACTER_PRECIS:B = 0x1t

.field public static final CLIP_DEFAULT_PRECIS:B = 0x0t

.field public static final CLIP_LH_ANGLES:B = 0x10t

.field public static final CLIP_MASK:B = 0xft

.field public static final CLIP_STROKE_PRECIS:B = 0x2t

.field public static final CLIP_TT_ALWAYS:B = 0x20t

.field public static final DEFAULT_CHARSET:B = 0x1t

.field public static final DEFAULT_INCH_VALUE:I = 0x240

.field public static final DEFAULT_PITCH:B = 0x0t

.field public static final DEFAULT_QUALITY:B = 0x0t

.field public static final DIB_PAL_COLORS:I = 0x1

.field public static final DIB_RGB_COLORS:I = 0x0

.field public static final DRAFT_QUALITY:B = 0x1t

.field public static final DSTINVERT:I = 0x550009

.field public static final ETO_CLIPPED:I = 0x4

.field public static final ETO_OPAQUE:I = 0x2

.field public static final FF_DECORATIVE:B = 0x50t

.field public static final FF_DONTCARE:B = 0x0t

.field public static final FF_MODERN:B = 0x30t

.field public static final FF_ROMAN:B = 0x10t

.field public static final FF_SCRIPT:B = 0x40t

.field public static final FF_SWISS:B = 0x20t

.field public static final FIXED_PITCH:B = 0x1t

.field public static final FW_BLACK:I = 0x384

.field public static final FW_BOLD:I = 0x2bc

.field public static final FW_DONTCARE:I = 0x64

.field public static final FW_NORMAL:I = 0x190

.field public static final FW_THIN:I = 0x64

.field public static final HS_BDIAGONAL:I = 0x3

.field public static final HS_CROSS:I = 0x4

.field public static final HS_DIAGCROSS:I = 0x5

.field public static final HS_FDIAGONAL:I = 0x2

.field public static final HS_HORIZONTAL:I = 0x0

.field public static final HS_VERTICAL:I = 0x1

.field public static final INCH_TO_MM:F = 25.4f

.field public static final MERGEPAINT:I = 0xbb0226

.field public static final META_ALDUS_APM:I = -0x65393229

.field public static final META_ANIMATEPALETTE:I = 0x436

.field public static final META_ARC:I = 0x817

.field public static final META_BITBLT:I = 0x922

.field public static final META_BLACKNESS:I = 0x42

.field public static final META_CHARSET_ANSI:I = 0x0

.field public static final META_CHARSET_ARABIC:I = 0xb2

.field public static final META_CHARSET_CHINESEBIG5:I = 0x88

.field public static final META_CHARSET_DEFAULT:I = 0x1

.field public static final META_CHARSET_EASTEUROPE:I = 0xee

.field public static final META_CHARSET_GB2312:I = 0x86

.field public static final META_CHARSET_GREEK:I = 0xa1

.field public static final META_CHARSET_HANGUL:I = 0x81

.field public static final META_CHARSET_HEBREW:I = 0xb1

.field public static final META_CHARSET_JOHAB:I = 0x82

.field public static final META_CHARSET_OEM:I = 0xff

.field public static final META_CHARSET_RUSSIAN:I = 0xcc

.field public static final META_CHARSET_SHIFTJIS:I = 0x80

.field public static final META_CHARSET_SYMBOL:I = 0x2

.field public static final META_CHARSET_THAI:I = 0xde

.field public static final META_CHARSET_TURKISH:I = 0xa2

.field public static final META_CHARSET_VIETNAMESE:I = 0xa3

.field public static final META_CHORD:I = 0x830

.field public static final META_CREATEBITMAP:I = 0x6fe

.field public static final META_CREATEBITMAPINDIRECT:I = 0x2fd

.field public static final META_CREATEBRUSH:I = 0xf8

.field public static final META_CREATEBRUSHINDIRECT:I = 0x2fc

.field public static final META_CREATEFONTINDIRECT:I = 0x2fb

.field public static final META_CREATEPALETTE:I = 0xf7

.field public static final META_CREATEPATTERNBRUSH:I = 0x1f9

.field public static final META_CREATEPENINDIRECT:I = 0x2fa

.field public static final META_CREATEREGION:I = 0x6ff

.field public static final META_DELETEOBJECT:I = 0x1f0

.field public static final META_DIBBITBLT:I = 0x940

.field public static final META_DIBCREATEPATTERNBRUSH:I = 0x142

.field public static final META_DIBSTRETCHBLT:I = 0xb41

.field public static final META_DRAWTEXT:I = 0x62f

.field public static final META_DSTINVERT:I = 0x550009

.field public static final META_ELLIPSE:I = 0x418

.field public static final META_ESCAPE:I = 0x626

.field public static final META_EXCLUDECLIPRECT:I = 0x415

.field public static final META_EXTFLOODFILL:I = 0x548

.field public static final META_EXTTEXTOUT:I = 0xa32

.field public static final META_FILLREGION:I = 0x228

.field public static final META_FLOODFILL:I = 0x419

.field public static final META_FRAMEREGION:I = 0x429

.field public static final META_INTERSECTCLIPRECT:I = 0x416

.field public static final META_INVERTREGION:I = 0x12a

.field public static final META_LINETO:I = 0x213

.field public static final META_MOVETO:I = 0x214

.field public static final META_OBJ_ANSI_FIXED_FONT:I = 0xb

.field public static final META_OBJ_ANSI_VAR_FONT:I = 0xc

.field public static final META_OBJ_BLACK_BRUSH:I = 0x4

.field public static final META_OBJ_BLACK_PEN:I = 0x7

.field public static final META_OBJ_DEFAULT_PALETTE:I = 0xf

.field public static final META_OBJ_DEVICE_DEFAULT_FONT:I = 0xe

.field public static final META_OBJ_DKGRAY_BRUSH:I = 0x3

.field public static final META_OBJ_GRAY_BRUSH:I = 0x2

.field public static final META_OBJ_HOLLOW_BRUSH:I = 0x5

.field public static final META_OBJ_LTGRAY_BRUSH:I = 0x1

.field public static final META_OBJ_NULL_BRUSH:I = 0x5

.field public static final META_OBJ_NULL_PEN:I = 0x8

.field public static final META_OBJ_OEM_FIXED_FONT:I = 0xa

.field public static final META_OBJ_SYSTEM_FIXED_FONT:I = 0x10

.field public static final META_OBJ_SYSTEM_FONT:I = 0xd

.field public static final META_OBJ_WHITE_BRUSH:I = 0x0

.field public static final META_OBJ_WHITE_PEN:I = 0x6

.field public static final META_OFFSETCLIPRGN:I = 0x220

.field public static final META_OFFSETVIEWPORTORG:I = 0x211

.field public static final META_OFFSETWINDOWORG:I = 0x20f

.field public static final META_PAINTREGION:I = 0x12b

.field public static final META_PATBLT:I = 0x61d

.field public static final META_PATCOPY:I = 0xf00021

.field public static final META_PATINVERT:I = 0x5a0049

.field public static final META_PIE:I = 0x81a

.field public static final META_POLYBEZIER16:I = 0x1000

.field public static final META_POLYGON:I = 0x324

.field public static final META_POLYLINE:I = 0x325

.field public static final META_POLYPOLYGON:I = 0x538

.field public static final META_PS_DASH:I = 0x1

.field public static final META_PS_DASHDOT:I = 0x3

.field public static final META_PS_DASHDOTDOT:I = 0x4

.field public static final META_PS_DOT:I = 0x2

.field public static final META_PS_INSIDEFRAME:I = 0x6

.field public static final META_PS_NULL:I = 0x5

.field public static final META_PS_SOLID:I = 0x0

.field public static final META_REALIZEPALETTE:I = 0x35

.field public static final META_RECTANGLE:I = 0x41b

.field public static final META_RESIZEPALETTE:I = 0x139

.field public static final META_RESTOREDC:I = 0x127

.field public static final META_ROUNDRECT:I = 0x61c

.field public static final META_SAVEDC:I = 0x1e

.field public static final META_SCALEVIEWPORTEXT:I = 0x412

.field public static final META_SCALEWINDOWEXT:I = 0x410

.field public static final META_SELECTCLIPREGION:I = 0x12c

.field public static final META_SELECTOBJECT:I = 0x12d

.field public static final META_SELECTPALETTE:I = 0x234

.field public static final META_SETBKCOLOR:I = 0x201

.field public static final META_SETBKMODE:I = 0x102

.field public static final META_SETDIBTODEV:I = 0xd33

.field public static final META_SETLAYOUT:I = 0x149

.field public static final META_SETMAPMODE:I = 0x103

.field public static final META_SETMAPPERFLAGS:I = 0x231

.field public static final META_SETPALENTRIES:I = 0x37

.field public static final META_SETPIXEL:I = 0x41f

.field public static final META_SETPOLYFILLMODE:I = 0x106

.field public static final META_SETRELABS:I = 0x105

.field public static final META_SETROP2:I = 0x104

.field public static final META_SETSTRETCHBLTMODE:I = 0x107

.field public static final META_SETTEXTALIGN:I = 0x12e

.field public static final META_SETTEXTCHAREXTRA:I = 0x108

.field public static final META_SETTEXTCOLOR:I = 0x209

.field public static final META_SETTEXTJUSTIFICATION:I = 0x20a

.field public static final META_SETVIEWPORTEXT:I = 0x20e

.field public static final META_SETVIEWPORTORG:I = 0x20d

.field public static final META_SETWINDOWEXT:I = 0x20c

.field public static final META_SETWINDOWORG:I = 0x20b

.field public static final META_SETWINDOWORG_EX:I = 0x0

.field public static final META_STRETCHBLT:I = 0xb23

.field public static final META_STRETCHDIB:I = 0xf43

.field public static final META_TEXTOUT:I = 0x521

.field public static final META_WHITENESS:I = 0xff0062

.field public static final MM_ANISOTROPIC:I = 0x8

.field public static final MM_HIENGLISH:I = 0x5

.field public static final MM_HIMETRIC:I = 0x3

.field public static final MM_HITWIPS:I = 0x6

.field public static final MM_ISOTROPIC:I = 0x7

.field public static final MM_LOENGLISH:I = 0x4

.field public static final MM_LOMETRIC:I = 0x2

.field public static final MM_TEXT:I = 0x1

.field public static final NOTSRCCOPY:I = 0x330008

.field public static final NOTSRCERASE:I = 0x1100a6

.field public static final OEM_CHARSET:B = -0x1t

.field public static final OPAQUE:I = 0x2

.field public static final OUT_CHARACTER_PRECIS:B = 0x2t

.field public static final OUT_DEFAULT_PRECIS:B = 0x0t

.field public static final OUT_DEVICE_PRECIS:B = 0x5t

.field public static final OUT_RASTER_PRECIS:B = 0x6t

.field public static final OUT_STRING_PRECIS:B = 0x1t

.field public static final OUT_STROKE_PRECIS:B = 0x3t

.field public static final OUT_TT_PRECIS:B = 0x4t

.field public static final PATCOPY:I = 0xf00021

.field public static final PATINVERT:I = 0x5a0049

.field public static final PATPAINT:I = 0xfb0a09

.field public static final PROOF_QUALITY:B = 0x2t

.field public static final R2_BLACK:I = 0x1

.field public static final R2_COPYPEN:I = 0xd

.field public static final R2_MASKNOTPENNOT:I = 0x3

.field public static final R2_MASKPEN:I = 0x9

.field public static final R2_MASKPENNOT:I = 0x5

.field public static final R2_MERGENOTPEN:I = 0xc

.field public static final R2_MERGEPEN:I = 0xf

.field public static final R2_MERGEPENNOT:I = 0xe

.field public static final R2_NOP:I = 0xb

.field public static final R2_NOT:I = 0x6

.field public static final R2_NOTCOPYPEN:I = 0x4

.field public static final R2_NOTMASKPEN:I = 0x8

.field public static final R2_NOTMERGEPEN:I = 0x2

.field public static final R2_NOTXORPEN:I = 0xa

.field public static final R2_WHITE:I = 0x10

.field public static final R2_XORPEN:I = 0x7

.field public static final SHIFTJIS_CHARSET:B = -0x80t

.field public static final SRCAND:I = 0x8800c6

.field public static final SRCCOPY:I = 0xcc0020

.field public static final SRCERASE:I = 0x440328

.field public static final SRCINVERT:I = 0x660046

.field public static final SRCPAINT:I = 0xee0086

.field public static final STRETCH_ANDSCANS:I = 0x1

.field public static final STRETCH_BLACKONWHITE:I = 0x1

.field public static final STRETCH_COLORONCOLOR:I = 0x3

.field public static final STRETCH_DELETESCANS:I = 0x3

.field public static final STRETCH_HALFTONE:I = 0x4

.field public static final STRETCH_ORSCANS:I = 0x2

.field public static final STRETCH_WHITEONBLACK:I = 0x2

.field public static final SYMBOL_CHARSET:B = 0x2t

.field public static final TA_BASELINE:I = 0x18

.field public static final TA_BOTTOM:I = 0x8

.field public static final TA_CENTER:I = 0x6

.field public static final TA_LEFT:I = 0x0

.field public static final TA_NOUPDATECP:I = 0x0

.field public static final TA_RIGHT:I = 0x2

.field public static final TA_TOP:I = 0x0

.field public static final TA_UPDATECP:I = 0x1

.field public static final TRANSPARENT:I = 0x1

.field public static final VARIABLE_PITCH:B = 0x2t

.field public static final WHITENESS:I = 0xff0062

.field public static final WINDING:I = 0x2
