.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelStyleFontHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 18
    const-string/jumbo v10, "font"

    invoke-direct {p0, v10}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 20
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontBoldHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontBoldHandler;-><init>()V

    .line 21
    .local v0, "boldHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontBoldHandler;
    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v11, "b"

    invoke-virtual {v10, v11, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontStrikeHandler;

    invoke-direct {v6}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontStrikeHandler;-><init>()V

    .line 24
    .local v6, "strikeHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontStrikeHandler;
    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v11, "strike"

    invoke-virtual {v10, v11, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontVertAlignHandler;

    invoke-direct {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontVertAlignHandler;-><init>()V

    .line 27
    .local v9, "vertAlignHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontVertAlignHandler;
    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v11, "vertAlign"

    invoke-virtual {v10, v11, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontUnderlineHandler;

    invoke-direct {v8}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontUnderlineHandler;-><init>()V

    .line 30
    .local v8, "uHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontUnderlineHandler;
    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v11, "u"

    invoke-virtual {v10, v11, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontItalicHandler;

    invoke-direct {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontItalicHandler;-><init>()V

    .line 32
    .local v3, "italicHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontItalicHandler;
    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v11, "i"

    invoke-virtual {v10, v11, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontSZHandler;

    invoke-direct {v7}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontSZHandler;-><init>()V

    .line 34
    .local v7, "szHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontSZHandler;
    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v11, "sz"

    invoke-virtual {v10, v11, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;-><init>()V

    .line 36
    .local v1, "colorHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;
    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v11, "color"

    invoke-virtual {v10, v11, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontNameHandler;

    invoke-direct {v4}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontNameHandler;-><init>()V

    .line 38
    .local v4, "nameHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontNameHandler;
    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v11, "name"

    invoke-virtual {v10, v11, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontFamilyHandler;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontFamilyHandler;-><init>()V

    .line 40
    .local v2, "familyHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontFamilyHandler;
    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v11, "family"

    invoke-virtual {v10, v11, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontSchemeHandler;

    invoke-direct {v5}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontSchemeHandler;-><init>()V

    .line 42
    .local v5, "schemeHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontSchemeHandler;
    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v11, "scheme"

    invoke-virtual {v10, v11, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 4
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 56
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v0

    .line 59
    .local v0, "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextSize()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_0

    .line 61
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    invoke-virtual {v1, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleTextSize(Ljava/lang/String;I)V

    .line 64
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExSharedStringTextBOLD()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_1

    .line 67
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    invoke-virtual {v1, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleFontBOLD(Ljava/lang/String;I)V

    .line 70
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextColorRGB()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_2

    .line 73
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    invoke-virtual {v1, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleTextColorRGB(Ljava/lang/String;I)V

    .line 76
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextColorTheme()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_3

    .line 79
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    invoke-virtual {v1, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleTextColorTheme(Ljava/lang/String;I)V

    .line 82
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextColorTint()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_4

    .line 85
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    invoke-virtual {v1, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleTextColorTint(Ljava/lang/String;I)V

    .line 88
    :cond_4
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextUnderline()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_5

    .line 91
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    invoke-virtual {v1, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleTextUnderline(Ljava/lang/String;I)V

    .line 94
    :cond_5
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextFontname()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_6

    .line 97
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    invoke-virtual {v1, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleTextFontname(Ljava/lang/String;I)V

    .line 100
    :cond_6
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextItalic()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_7

    .line 103
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    invoke-virtual {v1, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleTextItalic(Ljava/lang/String;I)V

    .line 106
    :cond_7
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextStrikethrough()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_8

    .line 109
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    invoke-virtual {v1, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleTextStrikethrough(Ljava/lang/String;I)V

    .line 112
    :cond_8
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getStyleTextVerticalAlign()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_9

    .line 115
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    invoke-virtual {v1, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addStyleTextVerticalAlign(Ljava/lang/String;I)V

    .line 119
    :cond_9
    iget v1, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    .line 120
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 121
    return-void
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 49
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    const-string/jumbo v1, "fontStylecolor"

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->setStyle(Ljava/lang/String;)V

    .line 50
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 52
    return-void
.end method
