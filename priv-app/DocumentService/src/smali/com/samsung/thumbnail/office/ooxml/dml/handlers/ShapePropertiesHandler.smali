.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "ShapePropertiesHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$ICustGeomHandler;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;


# instance fields
.field private shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;


# direct methods
.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 1
    .param p1, "ID"    # I
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 37
    const-string/jumbo v0, "spPr"

    invoke-direct {p0, p1, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 38
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 39
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;->init()V

    .line 40
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 0
    .param p1, "ID"    # I
    .param p2, "elementName"    # Ljava/lang/String;
    .param p3, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 31
    invoke-direct {p0, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 32
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 33
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;->init()V

    .line 34
    return-void
.end method

.method private init()V
    .locals 24

    .prologue
    .line 43
    const/16 v21, 0x8

    move/from16 v0, v21

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    move-object/from16 v16, v0

    .line 45
    .local v16, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v19, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler;

    const/16 v21, 0x1f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 47
    .local v19, "xfrmHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler;
    new-instance v20, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v21, "xfrm"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 49
    .local v20, "xfrmSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v21, 0x0

    aput-object v20, v16, v21

    .line 51
    new-instance v14, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;

    const/16 v21, 0x1f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v22, v0

    move/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v14, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 53
    .local v14, "prstHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;
    new-instance v15, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v21, "prstGeom"

    move-object/from16 v0, v21

    invoke-direct {v15, v0, v14}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 55
    .local v15, "prstSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v21, 0x1

    aput-object v15, v16, v21

    .line 58
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isDiagramShape()Z

    move-result v21

    if-eqz v21, :cond_0

    .line 59
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;

    const/16 v21, 0x1f

    move/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v7, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$ICustGeomHandler;)V

    .line 65
    .local v7, "custgeoHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;
    :goto_0
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v21, "custGeom"

    move-object/from16 v0, v21

    invoke-direct {v6, v0, v7}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 67
    .local v6, "custSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v21, 0x2

    aput-object v6, v16, v21

    .line 69
    new-instance v17, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    move-object/from16 v3, v23

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 71
    .local v17, "solidFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v21, "solidFill"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 73
    .local v18, "solidFillSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v21, 0x3

    aput-object v18, v16, v21

    .line 75
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v21, v0

    const-string/jumbo v22, "ln"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v10, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Ljava/lang/String;)V

    .line 77
    .local v10, "lnHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v21, "ln"

    move-object/from16 v0, v21

    invoke-direct {v11, v0, v10}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 79
    .local v11, "lnSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v21, 0x4

    aput-object v11, v16, v21

    .line 81
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v8, v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 83
    .local v8, "gradFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v21, "gradFill"

    move-object/from16 v0, v21

    invoke-direct {v9, v0, v8}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 85
    .local v9, "gradFillSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v21, 0x5

    aput-object v9, v16, v21

    .line 87
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-direct {v12, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 88
    .local v12, "noFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;
    new-instance v13, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v21, "noFill"

    move-object/from16 v0, v21

    invoke-direct {v13, v0, v12}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 90
    .local v13, "noFillSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v21, 0x6

    aput-object v13, v16, v21

    .line 92
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler;

    const/16 v21, 0x1f

    move/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v4, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;)V

    .line 94
    .local v4, "blipFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v21, "blipFill"

    move-object/from16 v0, v21

    invoke-direct {v5, v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 96
    .local v5, "blipSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v21, 0x7

    aput-object v5, v16, v21

    .line 98
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 99
    return-void

    .line 62
    .end local v4    # "blipFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler;
    .end local v5    # "blipSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    .end local v6    # "custSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    .end local v7    # "custgeoHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;
    .end local v8    # "gradFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;
    .end local v9    # "gradFillSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    .end local v10    # "lnHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;
    .end local v11    # "lnSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    .end local v12    # "noFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;
    .end local v13    # "noFillSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    .end local v17    # "solidFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;
    .end local v18    # "solidFillSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    :cond_0
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;

    const/16 v21, 0x1f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v22, v0

    move/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v7, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .restart local v7    # "custgeoHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/CustomGeometryHandler;
    goto/16 :goto_0
.end method


# virtual methods
.method public consumeGS(Ljava/lang/String;FLjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 12
    .param p1, "element"    # Ljava/lang/String;
    .param p2, "pos"    # F
    .param p3, "val"    # Ljava/lang/String;
    .param p4, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 104
    iget-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v1

    .line 106
    .local v1, "fillProperty":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 108
    .local v0, "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    if-nez v1, :cond_0

    .line 109
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .end local v1    # "fillProperty":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;-><init>()V

    .line 110
    .restart local v1    # "fillProperty":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    iget-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v8, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->consumeFill(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V

    .line 113
    :cond_0
    if-eqz p3, :cond_1

    .line 114
    invoke-virtual {v0, p3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 117
    iget-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v8, p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setGradFillColor(Ljava/lang/String;)V

    .line 119
    iget-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    const-wide/32 v10, 0xc350

    invoke-virtual {v8, v10, v11}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setFillAlpha(J)V

    .line 122
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 123
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v2

    .line 124
    .local v2, "lumMod":Ljava/lang/String;
    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setLumMod(Ljava/lang/String;)V

    .line 125
    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setLumMod(Ljava/lang/String;)V

    .line 127
    .end local v2    # "lumMod":Ljava/lang/String;
    :cond_2
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumOff()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 128
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumOff()Ljava/lang/String;

    move-result-object v3

    .line 129
    .local v3, "lumOff":Ljava/lang/String;
    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setLumOff(Ljava/lang/String;)V

    .line 130
    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setLumOff(Ljava/lang/String;)V

    .line 132
    .end local v3    # "lumOff":Ljava/lang/String;
    :cond_3
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getSatMod()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_4

    .line 133
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getSatMod()Ljava/lang/String;

    move-result-object v4

    .line 134
    .local v4, "satMod":Ljava/lang/String;
    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setSatMod(Ljava/lang/String;)V

    .line 135
    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setSatMod(Ljava/lang/String;)V

    .line 137
    .end local v4    # "satMod":Ljava/lang/String;
    :cond_4
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getSatOff()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 138
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getSatOff()Ljava/lang/String;

    move-result-object v5

    .line 139
    .local v5, "satOff":Ljava/lang/String;
    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setSatOff(Ljava/lang/String;)V

    .line 140
    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setSatOff(Ljava/lang/String;)V

    .line 142
    .end local v5    # "satOff":Ljava/lang/String;
    :cond_5
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getShade()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_6

    .line 143
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getShade()Ljava/lang/String;

    move-result-object v6

    .line 144
    .local v6, "shd":Ljava/lang/String;
    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setShade(Ljava/lang/String;)V

    .line 145
    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setShade(Ljava/lang/String;)V

    .line 147
    .end local v6    # "shd":Ljava/lang/String;
    :cond_6
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getTint()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_7

    .line 148
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getTint()Ljava/lang/String;

    move-result-object v7

    .line 149
    .local v7, "tnt":Ljava/lang/String;
    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setTint(Ljava/lang/String;)V

    .line 150
    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setTint(Ljava/lang/String;)V

    .line 153
    .end local v7    # "tnt":Ljava/lang/String;
    :cond_7
    invoke-virtual {v1, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->addPositionVal(F)V

    .line 154
    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->addXDocColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    .line 155
    return-void
.end method

.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 159
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 160
    return-void
.end method

.method public setBlipEmbed(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "blipEmbed"    # Ljava/lang/String;

    .prologue
    .line 177
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setRefId(Ljava/lang/String;)V

    .line 178
    return-void
.end method

.method public setPathDescriptor(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V
    .locals 3
    .param p1, "pathDesc"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    .prologue
    .line 164
    if-eqz p1, :cond_1

    .line 165
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v1, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setPathDescriptor(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V

    .line 166
    const-string/jumbo v0, "lineConnector"

    .line 167
    .local v0, "shapeName":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->hasArcTo()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 168
    const-string/jumbo v0, "arcConnector"

    .line 170
    :cond_0
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setPrstShapeName(Ljava/lang/String;)V

    .line 171
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setPrstName(Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;)V

    .line 173
    .end local v0    # "shapeName":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public setTile(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "flip"    # Ljava/lang/String;

    .prologue
    .line 183
    return-void
.end method
