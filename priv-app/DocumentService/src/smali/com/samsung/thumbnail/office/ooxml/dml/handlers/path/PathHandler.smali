.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLChoiceHandler;
.source "PathHandler.java"


# instance fields
.field private pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V
    .locals 6
    .param p1, "pathDesc"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    .prologue
    const/16 v5, 0x1f

    .line 15
    const-string/jumbo v3, "path"

    invoke-direct {p0, v5, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLChoiceHandler;-><init>(ILjava/lang/String;)V

    .line 16
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathHandler;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    .line 21
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathLineToHandler;

    invoke-direct {v1, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathLineToHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V

    .line 22
    .local v1, "lineToHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathLineToHandler;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v4, "lnTo"

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathMoveToHandler;

    invoke-direct {v2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathMoveToHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V

    .line 25
    .local v2, "moveToHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathMoveToHandler;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v4, "moveTo"

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathCubicBezTo;

    invoke-direct {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathCubicBezTo;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V

    .line 28
    .local v0, "cubicBezTohandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathCubicBezTo;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v4, "cubicBezTo"

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathHandler;->choiceHandlerMap:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathHandler;->handlerMap:Ljava/util/HashMap;

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 7
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 37
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLChoiceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 39
    const/4 v0, 0x0

    .line 40
    .local v0, "fill":Z
    const/4 v3, 0x0

    .line 41
    .local v3, "stroke":Z
    const/16 v4, 0x5460

    .line 42
    .local v4, "w":I
    const/16 v1, 0x5460

    .line 43
    .local v1, "h":I
    const-string/jumbo v6, "w"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v5

    .line 44
    .local v5, "width":Ljava/lang/String;
    const-string/jumbo v6, "h"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    .line 45
    .local v2, "height":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 46
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 47
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathHandler;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    invoke-virtual {v6, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->setWidth(I)V

    .line 49
    :cond_0
    if-eqz v2, :cond_1

    .line 50
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 51
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathHandler;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    invoke-virtual {v6, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->setHeight(I)V

    .line 53
    :cond_1
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathHandler;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    invoke-virtual {v6, v4, v1, v0, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->createNewPath(IIZZ)V

    .line 55
    return-void
.end method
