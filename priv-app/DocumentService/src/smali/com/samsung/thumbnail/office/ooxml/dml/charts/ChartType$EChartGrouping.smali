.class public final enum Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;
.super Ljava/lang/Enum;
.source "ChartType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EChartGrouping"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

.field public static final enum PERCENT_STACKED:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

.field public static final enum STACKED:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

.field public static final enum STANDARD:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 48
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

    const-string/jumbo v1, "PERCENT_STACKED"

    const-string/jumbo v2, "percentStacked"

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;->PERCENT_STACKED:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

    .line 51
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

    const-string/jumbo v1, "STANDARD"

    const-string/jumbo v2, "standard"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;->STANDARD:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

    .line 54
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

    const-string/jumbo v1, "STACKED"

    const-string/jumbo v2, "stacked"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;->STACKED:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

    .line 44
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;->PERCENT_STACKED:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;->STANDARD:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;->STACKED:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 58
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;->value:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public static convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 66
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;->values()[Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 67
    .local v2, "inst":Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 71
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;
    :goto_1
    return-object v2

    .line 66
    .restart local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 71
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 44
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

    return-object v0
.end method


# virtual methods
.method public xmlValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;->value:Ljava/lang/String;

    return-object v0
.end method
