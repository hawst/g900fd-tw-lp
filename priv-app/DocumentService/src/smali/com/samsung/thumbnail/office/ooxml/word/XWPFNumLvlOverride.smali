.class public Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;
.super Ljava/lang/Object;
.source "XWPFNumLvlOverride.java"


# instance fields
.field private ilvl:I

.field private isStartOverride:Z

.field private lvl:Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;

.field private startOverride:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getIlvl()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;->ilvl:I

    return v0
.end method

.method public getLvl()Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;->lvl:Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;

    return-object v0
.end method

.method public getStartOverride()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;->startOverride:I

    return v0
.end method

.method public isStartOverride()Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;->isStartOverride:Z

    return v0
.end method

.method public setIlvl(I)V
    .locals 0
    .param p1, "ilvl"    # I

    .prologue
    .line 17
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;->ilvl:I

    .line 18
    return-void
.end method

.method public setLvl(Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;)V
    .locals 0
    .param p1, "lvl"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;->lvl:Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;

    .line 27
    return-void
.end method

.method public setStartOverride(I)V
    .locals 1
    .param p1, "startOverride"    # I

    .prologue
    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;->isStartOverride:Z

    .line 22
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;->startOverride:I

    .line 23
    return-void
.end method
