.class public Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;
.super Ljava/lang/Object;
.source "EMF_LogFont.java"


# instance fields
.field private CharSet:B

.field private ClipPrecision:B

.field private Escapement:I

.field private Facename:[B

.field private Height:I

.field private Italic:B

.field private Orientation:I

.field private OutPrecision:B

.field private PitchAndFamily:B

.field private Quality:B

.field private StrikeOut:B

.field private Underline:B

.field private Weight:I

.field private Width:I

.field private fileContent:[B

.field private offset:I


# direct methods
.method public constructor <init>([BI)V
    .locals 0
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->fileContent:[B

    .line 72
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    .line 73
    return-void
.end method


# virtual methods
.method public getCharSet()B
    .locals 1

    .prologue
    .line 233
    iget-byte v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->CharSet:B

    return v0
.end method

.method public getClipPrecision()B
    .locals 1

    .prologue
    .line 271
    iget-byte v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->ClipPrecision:B

    return v0
.end method

.method public getEscapement()I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->Escapement:I

    return v0
.end method

.method public getFacename()[B
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->Facename:[B

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->Height:I

    return v0
.end method

.method public getItalic()B
    .locals 1

    .prologue
    .line 176
    iget-byte v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->Italic:B

    return v0
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->Orientation:I

    return v0
.end method

.method public getOutPrecision()B
    .locals 1

    .prologue
    .line 252
    iget-byte v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->OutPrecision:B

    return v0
.end method

.method public getPitchAndFamily()B
    .locals 1

    .prologue
    .line 309
    iget-byte v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->PitchAndFamily:B

    return v0
.end method

.method public getQuality()B
    .locals 1

    .prologue
    .line 290
    iget-byte v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->Quality:B

    return v0
.end method

.method public getStrikeOut()B
    .locals 1

    .prologue
    .line 214
    iget-byte v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->StrikeOut:B

    return v0
.end method

.method public getUnderline()B
    .locals 1

    .prologue
    .line 195
    iget-byte v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->Underline:B

    return v0
.end method

.method public getWeight()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->Weight:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->Width:I

    return v0
.end method

.method public readEmrLogFont()V
    .locals 14

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x0

    .line 345
    const/4 v7, 0x0

    .line 346
    .local v7, "i":I
    const/4 v9, 0x4

    new-array v8, v9, [B

    .line 348
    .local v8, "intConvert":[B
    const/4 v7, 0x0

    :goto_0
    if-gt v7, v13, :cond_0

    .line 349
    rsub-int/lit8 v9, v7, 0x3

    iget-object v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->fileContent:[B

    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    add-int/2addr v11, v7

    aget-byte v10, v10, v11

    aput-byte v10, v8, v9

    .line 348
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 350
    :cond_0
    invoke-static {v8, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 351
    .local v1, "Height":I
    iget v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    add-int/2addr v9, v7

    iput v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    .line 352
    int-to-float v9, v1

    invoke-static {v9}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v9

    float-to-int v9, v9

    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->setHeight(I)V

    .line 354
    const/4 v7, 0x0

    :goto_1
    if-gt v7, v13, :cond_1

    .line 355
    rsub-int/lit8 v9, v7, 0x3

    iget-object v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->fileContent:[B

    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    add-int/2addr v11, v7

    aget-byte v10, v10, v11

    aput-byte v10, v8, v9

    .line 354
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 356
    :cond_1
    invoke-static {v8, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v4

    .line 357
    .local v4, "Width":I
    iget v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    add-int/2addr v9, v7

    iput v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    .line 358
    int-to-float v9, v4

    invoke-static {v9}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v9

    float-to-int v9, v9

    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->setWidth(I)V

    .line 360
    const/4 v7, 0x0

    :goto_2
    if-gt v7, v13, :cond_2

    .line 361
    rsub-int/lit8 v9, v7, 0x3

    iget-object v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->fileContent:[B

    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    add-int/2addr v11, v7

    aget-byte v10, v10, v11

    aput-byte v10, v8, v9

    .line 360
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 362
    :cond_2
    invoke-static {v8, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 363
    .local v0, "Escapement":I
    iget v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    add-int/2addr v9, v7

    iput v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    .line 364
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->setEscapement(I)V

    .line 366
    const/4 v7, 0x0

    :goto_3
    if-gt v7, v13, :cond_3

    .line 367
    rsub-int/lit8 v9, v7, 0x3

    iget-object v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->fileContent:[B

    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    add-int/2addr v11, v7

    aget-byte v10, v10, v11

    aput-byte v10, v8, v9

    .line 366
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 368
    :cond_3
    invoke-static {v8, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v2

    .line 369
    .local v2, "Orientation":I
    iget v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    add-int/2addr v9, v7

    iput v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    .line 370
    invoke-virtual {p0, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->setOrientation(I)V

    .line 372
    const/4 v7, 0x0

    :goto_4
    if-gt v7, v13, :cond_4

    .line 373
    rsub-int/lit8 v9, v7, 0x3

    iget-object v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->fileContent:[B

    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    add-int/2addr v11, v7

    aget-byte v10, v10, v11

    aput-byte v10, v8, v9

    .line 372
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 374
    :cond_4
    invoke-static {v8, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v3

    .line 375
    .local v3, "Weight":I
    iget v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    add-int/2addr v9, v7

    iput v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    .line 376
    invoke-virtual {p0, v3}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->setWeight(I)V

    .line 378
    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    add-int/lit8 v11, v10, 0x1

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    aget-byte v9, v9, v10

    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->setItalic(B)V

    .line 380
    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    add-int/lit8 v11, v10, 0x1

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    aget-byte v9, v9, v10

    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->setUnderline(B)V

    .line 382
    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    add-int/lit8 v11, v10, 0x1

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    aget-byte v9, v9, v10

    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->setStrikeOut(B)V

    .line 384
    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    add-int/lit8 v11, v10, 0x1

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    aget-byte v9, v9, v10

    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->setCharSet(B)V

    .line 386
    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    add-int/lit8 v11, v10, 0x1

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    aget-byte v9, v9, v10

    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->setOutPrecision(B)V

    .line 388
    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    add-int/lit8 v11, v10, 0x1

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    aget-byte v9, v9, v10

    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->setClipPrecision(B)V

    .line 390
    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    add-int/lit8 v11, v10, 0x1

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    aget-byte v9, v9, v10

    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->setQuality(B)V

    .line 392
    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    add-int/lit8 v11, v10, 0x1

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    aget-byte v9, v9, v10

    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->setPitchAndFamily(B)V

    .line 394
    const/16 v5, 0x40

    .line 395
    .local v5, "buffsize":I
    new-array v6, v5, [B

    .line 396
    .local v6, "charBuffer":[B
    const/4 v7, 0x0

    :goto_5
    if-ge v7, v5, :cond_5

    .line 397
    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    add-int/2addr v10, v7

    aget-byte v9, v9, v10

    aput-byte v9, v6, v7

    .line 396
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 399
    :cond_5
    iget v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    add-int/2addr v9, v7

    iput v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->offset:I

    .line 400
    invoke-virtual {p0, v6}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->setFacename([B)V

    .line 401
    return-void
.end method

.method public setCharSet(B)V
    .locals 0
    .param p1, "charSet"    # B

    .prologue
    .line 243
    iput-byte p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->CharSet:B

    .line 244
    return-void
.end method

.method public setClipPrecision(B)V
    .locals 0
    .param p1, "clipPrecision"    # B

    .prologue
    .line 281
    iput-byte p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->ClipPrecision:B

    .line 282
    return-void
.end method

.method public setEscapement(I)V
    .locals 0
    .param p1, "escapement"    # I

    .prologue
    .line 129
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->Escapement:I

    .line 130
    return-void
.end method

.method public setFacename([B)V
    .locals 0
    .param p1, "facename"    # [B

    .prologue
    .line 338
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->Facename:[B

    .line 339
    return-void
.end method

.method public setHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 91
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->Height:I

    .line 92
    return-void
.end method

.method public setItalic(B)V
    .locals 0
    .param p1, "italic"    # B

    .prologue
    .line 186
    iput-byte p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->Italic:B

    .line 187
    return-void
.end method

.method public setOrientation(I)V
    .locals 0
    .param p1, "orientation"    # I

    .prologue
    .line 148
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->Orientation:I

    .line 149
    return-void
.end method

.method public setOutPrecision(B)V
    .locals 0
    .param p1, "outPrecision"    # B

    .prologue
    .line 262
    iput-byte p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->OutPrecision:B

    .line 263
    return-void
.end method

.method public setPitchAndFamily(B)V
    .locals 0
    .param p1, "pitchAndFamily"    # B

    .prologue
    .line 319
    iput-byte p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->PitchAndFamily:B

    .line 320
    return-void
.end method

.method public setQuality(B)V
    .locals 0
    .param p1, "quality"    # B

    .prologue
    .line 300
    iput-byte p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->Quality:B

    .line 301
    return-void
.end method

.method public setStrikeOut(B)V
    .locals 0
    .param p1, "strikeOut"    # B

    .prologue
    .line 224
    iput-byte p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->StrikeOut:B

    .line 225
    return-void
.end method

.method public setUnderline(B)V
    .locals 0
    .param p1, "underline"    # B

    .prologue
    .line 205
    iput-byte p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->Underline:B

    .line 206
    return-void
.end method

.method public setWeight(I)V
    .locals 0
    .param p1, "weight"    # I

    .prologue
    .line 167
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->Weight:I

    .line 168
    return-void
.end method

.method public setWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 110
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;->Width:I

    .line 111
    return-void
.end method
