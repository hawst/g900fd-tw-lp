.class public Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;
.super Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;
.source "XSLFTable.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;",
        "Ljava/lang/Iterable",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;",
        ">;"
    }
.end annotation


# static fields
.field public static final DEFAULT_ROW_HEIGHT:F = 330.0f

.field private static final TAG:Ljava/lang/String; = "XSLFTable"


# instance fields
.field private bandCol:I

.field private bandRow:I

.field private cellWidth:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private firstCol:I

.field private firstRow:I

.field private lastCol:I

.field private lastRow:I

.field private mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

.field private mGridColWidthArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private noOfCols:I

.field private noOfRows:I

.field private rows:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;",
            ">;"
        }
    .end annotation
.end field

.field private shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

.field private styleId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;)V
    .locals 2
    .param p1, "sheet"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;)V

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->cellWidth:Ljava/util/ArrayList;

    .line 57
    iput v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->firstRow:I

    iput v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->lastRow:I

    iput v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->firstCol:I

    iput v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->lastCol:I

    .line 58
    iput v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->bandRow:I

    iput v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->bandCol:I

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->rows:Ljava/util/ArrayList;

    .line 84
    return-void
.end method

.method private applyCellColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .locals 8
    .param p1, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p2, "cell"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;
    .param p3, "newCanvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .param p4, "cellClr"    # Ljava/lang/String;
    .param p5, "alpha"    # Ljava/lang/String;

    .prologue
    .line 842
    const/4 v3, 0x0

    .line 844
    .local v3, "fillColor":Lorg/apache/poi/java/awt/Color;
    const/16 v5, 0x10

    :try_start_0
    invoke-static {p4, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    .line 845
    .local v0, "colorvalue":I
    new-instance v4, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v4, v0}, Lorg/apache/poi/java/awt/Color;-><init>(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 846
    .end local v3    # "fillColor":Lorg/apache/poi/java/awt/Color;
    .local v4, "fillColor":Lorg/apache/poi/java/awt/Color;
    if-eqz p5, :cond_2

    .line 847
    :try_start_1
    invoke-static {p5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    const/high16 v6, 0x43800000    # 256.0f

    mul-float/2addr v5, v6

    const v6, 0x47c35000    # 100000.0f

    div-float/2addr v5, v6

    float-to-int v2, v5

    .line 848
    .local v2, "fillAlpha":I
    if-ltz v2, :cond_0

    const/16 v5, 0xff

    if-le v2, v5, :cond_1

    .line 849
    :cond_0
    const/16 v2, 0x80

    .line 851
    :cond_1
    new-instance v3, Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v4}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v5

    invoke-virtual {v4}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v6

    invoke-virtual {v4}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v7

    invoke-direct {v3, v5, v6, v7, v2}, Lorg/apache/poi/java/awt/Color;-><init>(IIII)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 858
    .end local v0    # "colorvalue":I
    .end local v2    # "fillAlpha":I
    .end local v4    # "fillColor":Lorg/apache/poi/java/awt/Color;
    .restart local v3    # "fillColor":Lorg/apache/poi/java/awt/Color;
    :goto_0
    const/4 v5, 0x0

    invoke-virtual {p3, v3, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellFillStyle(Lorg/apache/poi/java/awt/Color;Z)V

    .line 859
    return-object p3

    .line 854
    :catch_0
    move-exception v1

    .line 855
    .local v1, "e":Ljava/lang/Exception;
    :goto_1
    const-string/jumbo v5, "DocumentService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 854
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v3    # "fillColor":Lorg/apache/poi/java/awt/Color;
    .restart local v0    # "colorvalue":I
    .restart local v4    # "fillColor":Lorg/apache/poi/java/awt/Color;
    :catch_1
    move-exception v1

    move-object v3, v4

    .end local v4    # "fillColor":Lorg/apache/poi/java/awt/Color;
    .restart local v3    # "fillColor":Lorg/apache/poi/java/awt/Color;
    goto :goto_1

    .end local v3    # "fillColor":Lorg/apache/poi/java/awt/Color;
    .restart local v4    # "fillColor":Lorg/apache/poi/java/awt/Color;
    :cond_2
    move-object v3, v4

    .end local v4    # "fillColor":Lorg/apache/poi/java/awt/Color;
    .restart local v3    # "fillColor":Lorg/apache/poi/java/awt/Color;
    goto :goto_0
.end method

.method private getAlpha(Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p1, "tableStyles"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "tblFormatIdx"    # I

    .prologue
    .line 1866
    const/4 v0, 0x0

    .line 1868
    .local v0, "alpha":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillPropsLst()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getAlpha()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1870
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillPropsLst()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getAlpha()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1876
    :cond_0
    :goto_0
    return-object v0

    .line 1873
    :catch_0
    move-exception v1

    .line 1874
    .local v1, "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getTableDefColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .param p1, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p2, "tableStyles"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;
    .param p3, "element"    # Ljava/lang/String;
    .param p4, "tblFormatIdx"    # I

    .prologue
    .line 1882
    const/4 v0, 0x0

    .line 1886
    .local v0, "clr":Ljava/lang/String;
    :try_start_0
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    invoke-virtual {p2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillPropsLst()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 1888
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    invoke-virtual {p2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillPropsLst()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v0

    .line 1890
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1892
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1905
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    invoke-virtual {p2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillPropsLst()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getTint()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1907
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    invoke-virtual {p2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillPropsLst()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getTint()Ljava/lang/String;

    move-result-object v2

    .line 1910
    .local v2, "tint":Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcColorTint(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1916
    .end local v2    # "tint":Ljava/lang/String;
    :cond_1
    :goto_1
    return-object v0

    .line 1895
    :cond_2
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    invoke-virtual {p2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillPropsLst()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getFillRefColor()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1897
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    invoke-virtual {p2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillPropsLst()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getFillRefColor()Ljava/lang/String;

    move-result-object v0

    .line 1899
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1901
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto/16 :goto_0

    .line 1912
    :catch_0
    move-exception v1

    .line 1913
    .local v1, "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private setTableLineProperties(Ljava/util/HashMap;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .locals 12
    .param p2, "newCanvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .param p3, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;",
            ">;",
            "Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;",
            ")",
            "Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;"
        }
    .end annotation

    .prologue
    .local p1, "cellBorders":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;>;"
    const/16 v11, 0x10

    const/high16 v10, -0x1000000

    .line 1698
    const/4 v5, 0x0

    .line 1699
    .local v5, "lineWidth":F
    const-string/jumbo v6, "000000"

    .line 1700
    .local v6, "lnClr":Ljava/lang/String;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    invoke-direct {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;-><init>()V

    .line 1702
    .local v4, "lineProps":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    const-string/jumbo v7, "left"

    invoke-virtual {p1, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "lineProps":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .line 1703
    .restart local v4    # "lineProps":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    if-eqz v4, :cond_2

    .line 1704
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getLineWidth()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_c

    .line 1705
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getLineWidth()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    invoke-static {v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints(F)F

    move-result v5

    .line 1711
    :goto_0
    iget-object v7, v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->color:Ljava/lang/String;

    if-eqz v7, :cond_d

    .line 1712
    iget-object v6, v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->color:Ljava/lang/String;

    .line 1713
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 1715
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1718
    :cond_0
    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    new-instance v8, Lorg/apache/poi/java/awt/Color;

    invoke-static {v6, v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v9

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    invoke-virtual {p2, v7, v8, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 1731
    :cond_1
    :goto_1
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getPrstDash()Ljava/lang/String;

    move-result-object v1

    .line 1732
    .local v1, "borderLeft":Ljava/lang/String;
    if-nez v1, :cond_2

    .line 1733
    const-string/jumbo v7, "solid"

    .line 1737
    .end local v1    # "borderLeft":Ljava/lang/String;
    :cond_2
    const-string/jumbo v7, "right"

    invoke-virtual {p1, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "lineProps":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .line 1738
    .restart local v4    # "lineProps":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    if-eqz v4, :cond_5

    .line 1739
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getLineWidth()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_f

    .line 1740
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getLineWidth()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    invoke-static {v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints(F)F

    move-result v5

    .line 1746
    :goto_2
    iget-object v7, v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->color:Ljava/lang/String;

    if-eqz v7, :cond_10

    .line 1747
    iget-object v6, v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->color:Ljava/lang/String;

    .line 1748
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 1750
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1753
    :cond_3
    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    new-instance v8, Lorg/apache/poi/java/awt/Color;

    invoke-static {v6, v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v9

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    invoke-virtual {p2, v7, v8, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 1766
    :cond_4
    :goto_3
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getPrstDash()Ljava/lang/String;

    move-result-object v2

    .line 1767
    .local v2, "borderRight":Ljava/lang/String;
    if-nez v2, :cond_5

    .line 1768
    const-string/jumbo v7, "solid"

    .line 1772
    .end local v2    # "borderRight":Ljava/lang/String;
    :cond_5
    const-string/jumbo v7, "top"

    invoke-virtual {p1, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "lineProps":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .line 1773
    .restart local v4    # "lineProps":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    if-eqz v4, :cond_8

    .line 1774
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getLineWidth()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_12

    .line 1775
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getLineWidth()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    invoke-static {v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints(F)F

    move-result v5

    .line 1781
    :goto_4
    iget-object v7, v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->color:Ljava/lang/String;

    if-eqz v7, :cond_13

    .line 1782
    iget-object v6, v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->color:Ljava/lang/String;

    .line 1783
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_6

    .line 1785
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1788
    :cond_6
    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    new-instance v8, Lorg/apache/poi/java/awt/Color;

    invoke-static {v6, v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v9

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    invoke-virtual {p2, v7, v8, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 1801
    :cond_7
    :goto_5
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getPrstDash()Ljava/lang/String;

    move-result-object v3

    .line 1802
    .local v3, "borderTop":Ljava/lang/String;
    if-nez v3, :cond_8

    .line 1803
    const-string/jumbo v7, "solid"

    .line 1807
    .end local v3    # "borderTop":Ljava/lang/String;
    :cond_8
    const-string/jumbo v7, "bottom"

    invoke-virtual {p1, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "lineProps":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .line 1808
    .restart local v4    # "lineProps":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    if-eqz v4, :cond_b

    .line 1809
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getLineWidth()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_15

    .line 1810
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getLineWidth()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    invoke-static {v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints(F)F

    move-result v5

    .line 1816
    :goto_6
    iget-object v7, v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->color:Ljava/lang/String;

    if-eqz v7, :cond_16

    .line 1817
    iget-object v6, v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->color:Ljava/lang/String;

    .line 1818
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_9

    .line 1820
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1823
    :cond_9
    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    new-instance v8, Lorg/apache/poi/java/awt/Color;

    invoke-static {v6, v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v9

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    invoke-virtual {p2, v7, v8, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 1836
    :cond_a
    :goto_7
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getPrstDash()Ljava/lang/String;

    move-result-object v0

    .line 1837
    .local v0, "borderBottom":Ljava/lang/String;
    if-nez v0, :cond_b

    .line 1838
    const-string/jumbo v7, "solid"

    .line 1842
    .end local v0    # "borderBottom":Ljava/lang/String;
    :cond_b
    return-object p2

    .line 1708
    :cond_c
    const-string/jumbo v7, "12700"

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    invoke-static {v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints(F)F

    move-result v5

    goto/16 :goto_0

    .line 1720
    :cond_d
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 1722
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v6

    .line 1723
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_e

    .line 1725
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1728
    :cond_e
    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    new-instance v8, Lorg/apache/poi/java/awt/Color;

    invoke-static {v6, v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v9

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    invoke-virtual {p2, v7, v8, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    goto/16 :goto_1

    .line 1743
    :cond_f
    const-string/jumbo v7, "12700"

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    invoke-static {v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints(F)F

    move-result v5

    goto/16 :goto_2

    .line 1755
    :cond_10
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v7

    if-eqz v7, :cond_4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_4

    .line 1757
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v6

    .line 1758
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_11

    .line 1760
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1763
    :cond_11
    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    new-instance v8, Lorg/apache/poi/java/awt/Color;

    invoke-static {v6, v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v9

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    invoke-virtual {p2, v7, v8, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    goto/16 :goto_3

    .line 1778
    :cond_12
    const-string/jumbo v7, "12700"

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    invoke-static {v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints(F)F

    move-result v5

    goto/16 :goto_4

    .line 1790
    :cond_13
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v7

    if-eqz v7, :cond_7

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_7

    .line 1792
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v6

    .line 1793
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_14

    .line 1795
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1798
    :cond_14
    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    new-instance v8, Lorg/apache/poi/java/awt/Color;

    invoke-static {v6, v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v9

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    invoke-virtual {p2, v7, v8, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    goto/16 :goto_5

    .line 1813
    :cond_15
    const-string/jumbo v7, "12700"

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    invoke-static {v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints(F)F

    move-result v5

    goto/16 :goto_6

    .line 1825
    :cond_16
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v7

    if-eqz v7, :cond_a

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_a

    .line 1827
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v6

    .line 1828
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_17

    .line 1830
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1833
    :cond_17
    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    new-instance v8, Lorg/apache/poi/java/awt/Color;

    invoke-static {v6, v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v9

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    invoke-virtual {p2, v7, v8, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    goto/16 :goto_7
.end method

.method private writeTableCell(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;IILjava/util/List;Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;II)V
    .locals 70
    .param p1, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p2, "row"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;
    .param p3, "cellPos"    # I
    .param p4, "indexnumber"    # I
    .param p6, "tableStyles"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;
    .param p7, "rowNo"    # I
    .param p8, "cellWidth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;",
            "II",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;",
            ">;",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 868
    .local p5, "rowStylesList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;>;"
    new-instance v7, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-direct {v7}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;-><init>()V

    .line 870
    .local v7, "newCanvasCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;->getCells()Ljava/util/List;

    move-result-object v4

    move/from16 v0, p3

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;

    .line 873
    .local v6, "cell":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 874
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getGridSpan()I

    move-result v4

    invoke-virtual {v7, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellGradSpan(I)V

    .line 876
    const-string/jumbo v4, "normal"

    invoke-virtual {v7, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTextDirection(Ljava/lang/String;)V

    .line 879
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setMergedCellKey(Ljava/lang/String;)V

    .line 881
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v32

    .line 883
    .local v32, "cellProp":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    const/16 v68, -0x1

    .line 884
    .local v68, "tblFormatSz":I
    const/4 v8, 0x0

    .line 885
    .local v8, "cellClr":Ljava/lang/String;
    const/16 v22, 0x0

    .line 886
    .local v22, "alpha":Ljava/lang/String;
    new-instance v34, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-direct/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;-><init>()V

    .line 888
    .local v34, "charProps":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getCellColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 889
    new-instance v36, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct/range {v36 .. v36}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 890
    .local v36, "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getCellColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v36

    .line 891
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v4

    invoke-virtual/range {v36 .. v36}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3a

    .line 893
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v4

    invoke-virtual/range {v36 .. v36}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 895
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLumMod()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_39

    .line 896
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLumMod()Ljava/lang/String;

    move-result-object v55

    .line 897
    .local v55, "lumMod":Ljava/lang/String;
    invoke-static/range {v55 .. v55}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v50

    .line 898
    .local v50, "lMod":D
    invoke-virtual/range {v36 .. v36}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v4

    invoke-static/range {v50 .. v51}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcLuminousColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v8

    .line 906
    .end local v50    # "lMod":D
    .end local v55    # "lumMod":Ljava/lang/String;
    :goto_0
    const/4 v9, 0x0

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    invoke-direct/range {v4 .. v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->applyCellColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 910
    .end local v36    # "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    :cond_1
    const/16 v31, 0x0

    .line 911
    .local v31, "cellBorders":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;>;"
    const/16 v37, 0x0

    .line 912
    .local v37, "colorStatus":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblBgColor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 914
    .local v13, "tblBgColor":Ljava/lang/String;
    if-eqz v13, :cond_2

    .line 915
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v4

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 917
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v4

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 919
    const/4 v14, 0x0

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object v11, v6

    move-object v12, v7

    invoke-direct/range {v9 .. v14}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->applyCellColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 924
    :cond_2
    if-eqz p6, :cond_79

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ltz v4, :cond_79

    .line 926
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v68

    .line 927
    const/16 v49, 0x0

    .local v49, "k":I
    :goto_1
    move/from16 v0, v49

    move/from16 v1, v68

    if-ge v0, v1, :cond_78

    .line 928
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillPropsLst()Ljava/util/HashMap;

    move-result-object v4

    const-string/jumbo v5, "wholeTbl"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3b

    .line 931
    const-string/jumbo v4, "wholeTbl"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    move/from16 v3, v49

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getTableDefColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v8

    .line 933
    const-string/jumbo v4, "wholeTbl"

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move/from16 v2, v49

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getAlpha(Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    .line 935
    .end local v22    # "alpha":Ljava/lang/String;
    .local v9, "alpha":Ljava/lang/String;
    if-eqz v8, :cond_3

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    .line 936
    invoke-direct/range {v4 .. v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->applyCellColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 938
    const/16 v37, 0x1

    .line 940
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getTcBrdr()Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;->getBrdrLineProps()Ljava/util/HashMap;

    move-result-object v31

    .line 942
    if-eqz v31, :cond_4

    .line 943
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->setTableLineProperties(Ljava/util/HashMap;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 946
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getTxStyFntClr()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 948
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getTxStyFntClr()Ljava/lang/String;

    move-result-object v44

    .line 950
    .local v44, "fntClr":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v4

    move-object/from16 v0, v44

    invoke-virtual {v4, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 952
    new-instance v35, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct/range {v35 .. v35}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 953
    .local v35, "clr":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v4

    move-object/from16 v0, v44

    invoke-virtual {v4, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v44

    .line 955
    move-object/from16 v0, v35

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 956
    invoke-virtual/range {v34 .. v35}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    .line 963
    .end local v35    # "clr":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .end local v44    # "fntClr":Ljava/lang/String;
    :cond_5
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getBandCol()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_7

    .line 964
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getFirstCol()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_41

    .line 965
    rem-int/lit8 v4, p3, 0x2

    if-nez v4, :cond_3e

    .line 966
    const/16 v49, 0x0

    :goto_3
    move/from16 v0, v49

    move/from16 v1, v68

    if-ge v0, v1, :cond_7

    .line 967
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillPropsLst()Ljava/util/HashMap;

    move-result-object v4

    const-string/jumbo v5, "band2V"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3d

    .line 970
    const-string/jumbo v4, "band2V"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    move/from16 v3, v49

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getTableDefColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v8

    .line 972
    const-string/jumbo v4, "band2V"

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move/from16 v2, v49

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getAlpha(Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    .line 974
    if-eqz v8, :cond_3c

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    .line 975
    invoke-direct/range {v4 .. v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->applyCellColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 977
    const/16 v37, 0x1

    .line 987
    :cond_6
    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getTcBrdr()Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;->getBrdrLineProps()Ljava/util/HashMap;

    move-result-object v31

    .line 990
    if-eqz v31, :cond_7

    .line 991
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->setTableLineProperties(Ljava/util/HashMap;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 1085
    :cond_7
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getBandRow()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_9

    .line 1086
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getFirstRow()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_4d

    .line 1087
    rem-int/lit8 v4, p7, 0x2

    if-nez v4, :cond_4a

    .line 1088
    const/16 v49, 0x0

    :goto_6
    move/from16 v0, v49

    move/from16 v1, v68

    if-ge v0, v1, :cond_9

    .line 1089
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillPropsLst()Ljava/util/HashMap;

    move-result-object v4

    const-string/jumbo v5, "band2H"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_49

    .line 1092
    const-string/jumbo v4, "band2H"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    move/from16 v3, v49

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getTableDefColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v8

    .line 1094
    const-string/jumbo v4, "band2H"

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move/from16 v2, v49

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getAlpha(Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    .line 1096
    if-eqz v8, :cond_48

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    .line 1097
    invoke-direct/range {v4 .. v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->applyCellColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 1099
    const/16 v37, 0x1

    .line 1109
    :cond_8
    :goto_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getTcBrdr()Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;->getBrdrLineProps()Ljava/util/HashMap;

    move-result-object v31

    .line 1112
    if-eqz v31, :cond_9

    .line 1113
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->setTableLineProperties(Ljava/util/HashMap;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 1206
    :cond_9
    :goto_8
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getLastCol()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_e

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->noOfCols:I

    add-int/lit8 v4, v4, -0x1

    move/from16 v0, p3

    if-ne v0, v4, :cond_e

    .line 1207
    const/16 v49, 0x0

    :goto_9
    move/from16 v0, v49

    move/from16 v1, v68

    if-ge v0, v1, :cond_e

    .line 1208
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillPropsLst()Ljava/util/HashMap;

    move-result-object v4

    const-string/jumbo v5, "lastCol"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_54

    .line 1211
    const-string/jumbo v4, "lastCol"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    move/from16 v3, v49

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getTableDefColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v8

    .line 1213
    const-string/jumbo v4, "lastCol"

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move/from16 v2, v49

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getAlpha(Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    .line 1215
    if-eqz v8, :cond_a

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    .line 1216
    invoke-direct/range {v4 .. v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->applyCellColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 1218
    const/16 v37, 0x1

    .line 1220
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getTcBrdr()Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;->getBrdrLineProps()Ljava/util/HashMap;

    move-result-object v31

    .line 1222
    if-eqz v31, :cond_b

    .line 1223
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->setTableLineProperties(Ljava/util/HashMap;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 1226
    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getTxStyFntClr()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_c

    .line 1228
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getTxStyFntClr()Ljava/lang/String;

    move-result-object v44

    .line 1231
    .restart local v44    # "fntClr":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v4

    move-object/from16 v0, v44

    invoke-virtual {v4, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_c

    .line 1233
    new-instance v35, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct/range {v35 .. v35}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 1234
    .restart local v35    # "clr":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v4

    move-object/from16 v0, v44

    invoke-virtual {v4, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v44

    .line 1237
    move-object/from16 v0, v35

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1238
    invoke-virtual/range {v34 .. v35}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    .line 1241
    .end local v35    # "clr":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .end local v44    # "fntClr":Ljava/lang/String;
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFontBold()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 1243
    const/4 v4, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setBold(Z)V

    .line 1245
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFontItalic()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 1247
    const/4 v4, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setItalic(Z)V

    .line 1254
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getLastRow()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_13

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->noOfRows:I

    add-int/lit8 v4, v4, -0x1

    move/from16 v0, p7

    if-ne v0, v4, :cond_13

    .line 1255
    const/16 v49, 0x0

    :goto_a
    move/from16 v0, v49

    move/from16 v1, v68

    if-ge v0, v1, :cond_13

    .line 1256
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillPropsLst()Ljava/util/HashMap;

    move-result-object v4

    const-string/jumbo v5, "lastRow"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_55

    .line 1259
    const-string/jumbo v4, "lastRow"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    move/from16 v3, v49

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getTableDefColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v8

    .line 1261
    const-string/jumbo v4, "lastRow"

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move/from16 v2, v49

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getAlpha(Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    .line 1263
    if-eqz v8, :cond_f

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    .line 1264
    invoke-direct/range {v4 .. v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->applyCellColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 1266
    const/16 v37, 0x1

    .line 1268
    :cond_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getTcBrdr()Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;->getBrdrLineProps()Ljava/util/HashMap;

    move-result-object v31

    .line 1270
    if-eqz v31, :cond_10

    .line 1271
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->setTableLineProperties(Ljava/util/HashMap;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 1274
    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getTxStyFntClr()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_11

    .line 1276
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getTxStyFntClr()Ljava/lang/String;

    move-result-object v44

    .line 1279
    .restart local v44    # "fntClr":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v4

    move-object/from16 v0, v44

    invoke-virtual {v4, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_11

    .line 1281
    new-instance v35, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct/range {v35 .. v35}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 1282
    .restart local v35    # "clr":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v4

    move-object/from16 v0, v44

    invoke-virtual {v4, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v44

    .line 1285
    move-object/from16 v0, v35

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1286
    invoke-virtual/range {v34 .. v35}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    .line 1289
    .end local v35    # "clr":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .end local v44    # "fntClr":Ljava/lang/String;
    :cond_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFontBold()Z

    move-result v4

    if-eqz v4, :cond_12

    .line 1291
    const/4 v4, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setBold(Z)V

    .line 1293
    :cond_12
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFontItalic()Z

    move-result v4

    if-eqz v4, :cond_13

    .line 1295
    const/4 v4, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setItalic(Z)V

    .line 1301
    :cond_13
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getFirstCol()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_18

    if-nez p3, :cond_18

    .line 1302
    const/16 v49, 0x0

    :goto_b
    move/from16 v0, v49

    move/from16 v1, v68

    if-ge v0, v1, :cond_18

    .line 1303
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillPropsLst()Ljava/util/HashMap;

    move-result-object v4

    const-string/jumbo v5, "firstCol"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_56

    .line 1306
    const-string/jumbo v4, "firstCol"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    move/from16 v3, v49

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getTableDefColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v8

    .line 1308
    const-string/jumbo v4, "firstCol"

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move/from16 v2, v49

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getAlpha(Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    .line 1310
    if-eqz v8, :cond_14

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    .line 1311
    invoke-direct/range {v4 .. v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->applyCellColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 1313
    const/16 v37, 0x1

    .line 1315
    :cond_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getTcBrdr()Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;->getBrdrLineProps()Ljava/util/HashMap;

    move-result-object v31

    .line 1317
    if-eqz v31, :cond_15

    .line 1318
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->setTableLineProperties(Ljava/util/HashMap;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 1321
    :cond_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getTxStyFntClr()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_16

    .line 1323
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getTxStyFntClr()Ljava/lang/String;

    move-result-object v44

    .line 1326
    .restart local v44    # "fntClr":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v4

    move-object/from16 v0, v44

    invoke-virtual {v4, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_16

    .line 1328
    new-instance v35, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct/range {v35 .. v35}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 1329
    .restart local v35    # "clr":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v4

    move-object/from16 v0, v44

    invoke-virtual {v4, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v44

    .line 1332
    move-object/from16 v0, v35

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1333
    invoke-virtual/range {v34 .. v35}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    .line 1336
    .end local v35    # "clr":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .end local v44    # "fntClr":Ljava/lang/String;
    :cond_16
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFontBold()Z

    move-result v4

    if-eqz v4, :cond_17

    .line 1338
    const/4 v4, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setBold(Z)V

    .line 1340
    :cond_17
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFontItalic()Z

    move-result v4

    if-eqz v4, :cond_18

    .line 1342
    const/4 v4, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setItalic(Z)V

    .line 1348
    :cond_18
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getFirstRow()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1d

    if-nez p7, :cond_1d

    .line 1349
    const/16 v49, 0x0

    :goto_c
    move/from16 v0, v49

    move/from16 v1, v68

    if-ge v0, v1, :cond_1d

    .line 1350
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillPropsLst()Ljava/util/HashMap;

    move-result-object v4

    const-string/jumbo v5, "firstRow"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_57

    .line 1353
    const-string/jumbo v4, "firstRow"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    move/from16 v3, v49

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getTableDefColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v8

    .line 1355
    const-string/jumbo v4, "firstRow"

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move/from16 v2, v49

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getAlpha(Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    .line 1357
    if-eqz v8, :cond_19

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    .line 1358
    invoke-direct/range {v4 .. v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->applyCellColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 1360
    const/16 v37, 0x1

    .line 1362
    :cond_19
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getTcBrdr()Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;->getBrdrLineProps()Ljava/util/HashMap;

    move-result-object v31

    .line 1364
    if-eqz v31, :cond_1a

    .line 1365
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->setTableLineProperties(Ljava/util/HashMap;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 1368
    :cond_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getTxStyFntClr()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1b

    .line 1370
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getTxStyFntClr()Ljava/lang/String;

    move-result-object v44

    .line 1373
    .restart local v44    # "fntClr":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v4

    move-object/from16 v0, v44

    invoke-virtual {v4, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1b

    .line 1375
    new-instance v35, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct/range {v35 .. v35}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 1376
    .restart local v35    # "clr":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v4

    move-object/from16 v0, v44

    invoke-virtual {v4, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v44

    .line 1379
    move-object/from16 v0, v35

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1380
    invoke-virtual/range {v34 .. v35}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    .line 1383
    .end local v35    # "clr":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .end local v44    # "fntClr":Ljava/lang/String;
    :cond_1b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFontBold()Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 1385
    const/4 v4, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setBold(Z)V

    .line 1387
    :cond_1c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFontItalic()Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 1389
    const/4 v4, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setItalic(Z)V

    .line 1397
    .end local v49    # "k":I
    :cond_1d
    :goto_d
    if-nez v37, :cond_1e

    if-eqz v13, :cond_1e

    .line 1398
    const-string/jumbo v9, "20000"

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v16, v6

    move-object/from16 v17, v7

    move-object/from16 v18, v13

    move-object/from16 v19, v9

    .line 1399
    invoke-direct/range {v14 .. v19}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->applyCellColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 1404
    :cond_1e
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getVMerge()Z

    move-result v4

    if-eqz v4, :cond_58

    .line 1405
    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;->CONTINUE:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellMergeValue(Ljava/lang/String;)V

    .line 1410
    :cond_1f
    :goto_e
    move/from16 v0, p8

    int-to-float v4, v0

    invoke-virtual {v7, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellWidth(F)V

    .line 1418
    new-instance v58, Ljava/lang/StringBuilder;

    const/16 v4, 0x100

    move-object/from16 v0, v58

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1424
    .local v58, "mstrBuilder":Ljava/lang/StringBuilder;
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;->getParagraphLst()Ljava/util/List;

    move-result-object v61

    .line 1426
    .local v61, "paraList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;>;"
    const/16 v47, 0x0

    .line 1427
    .local v47, "isHlink":Z
    const/16 v63, -0x1

    .local v63, "preLevel":I
    const/16 v26, -0x1

    .line 1428
    .local v26, "buLevel":I
    const/16 v64, 0x0

    .line 1429
    .local v64, "prevBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    new-instance v27, Ljava/util/HashMap;

    invoke-direct/range {v27 .. v27}, Ljava/util/HashMap;-><init>()V

    .line 1430
    .local v27, "buMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    const/16 v25, -0x1

    .line 1431
    .local v25, "buCount":I
    new-instance v54, Ljava/util/ArrayList;

    invoke-direct/range {v54 .. v54}, Ljava/util/ArrayList;-><init>()V

    .line 1433
    .local v54, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    const/16 v46, 0x0

    .local v46, "i":I
    :goto_f
    invoke-interface/range {v61 .. v61}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v46

    if-ge v0, v4, :cond_77

    .line 1434
    move-object/from16 v0, v61

    move/from16 v1, v46

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v60

    check-cast v60, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    .line 1435
    .local v60, "para":Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;
    new-instance v40, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-direct/range {v40 .. v40}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    .line 1437
    .local v40, "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-virtual/range {v60 .. v60}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getRunList()Ljava/util/ArrayList;

    move-result-object v66

    .line 1438
    .local v66, "runList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;>;"
    invoke-virtual/range {v60 .. v60}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v62

    .line 1440
    .local v62, "paraProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    const/16 v53, 0x0

    .line 1441
    .local v53, "level":I
    const/16 v21, 0x0

    .line 1442
    .local v21, "align":Ljava/lang/String;
    const/16 v56, 0x0

    .line 1443
    .local v56, "marginLeft":Ljava/lang/String;
    const/16 v43, 0x0

    .line 1444
    .local v43, "eBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    const/16 v39, 0x0

    .line 1445
    .local v39, "curBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    const/16 v28, 0x0

    .line 1446
    .local v28, "buNum":Ljava/lang/String;
    const/16 v24, 0x0

    .line 1447
    .local v24, "buChar":Ljava/lang/String;
    invoke-virtual/range {v60 .. v60}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v62

    .line 1449
    if-eqz v62, :cond_20

    .line 1450
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getLevel()I

    move-result v53

    .line 1451
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getJustify()Ljava/lang/String;

    move-result-object v21

    .line 1452
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getBuAutoNum()Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    move-result-object v43

    .line 1453
    move-object/from16 v39, v43

    .line 1454
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getMarginLeft()I

    move-result v4

    if-eqz v4, :cond_20

    .line 1455
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getMarginLeft()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v56

    .line 1458
    :cond_20
    move/from16 v0, v63

    move/from16 v1, v53

    if-eq v0, v1, :cond_22

    .line 1459
    move/from16 v0, v63

    move/from16 v1, v53

    if-ge v0, v1, :cond_21

    .line 1460
    const/16 v25, -0x1

    .line 1462
    :cond_21
    move/from16 v26, v63

    .line 1466
    :cond_22
    if-eqz v56, :cond_23

    .line 1467
    :try_start_0
    invoke-static/range {v56 .. v56}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v57

    .line 1468
    .local v57, "marginSpace":I
    mul-int/lit8 v4, v53, 0x32

    add-int v57, v57, v4

    .line 1469
    invoke-static/range {v57 .. v57}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v56

    .line 1475
    .end local v57    # "marginSpace":I
    :cond_23
    :goto_10
    move/from16 v63, v53

    .line 1477
    if-eqz v56, :cond_24

    .line 1478
    invoke-static/range {v56 .. v56}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    move-object/from16 v0, v40

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    .line 1479
    :cond_24
    if-eqz v21, :cond_25

    .line 1480
    const-string/jumbo v4, "center"

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_59

    .line 1481
    sget-object v4, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->CENTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v40

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    .line 1489
    :cond_25
    :goto_11
    const-string/jumbo v4, "ctr"

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;->getBodyProp()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;->getvAlign()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5c

    .line 1490
    sget-object v4, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->CENTERED:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-object/from16 v0, v40

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    .line 1497
    :goto_12
    if-eqz v62, :cond_26

    .line 1498
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getLineSpacing()F

    move-result v4

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_26

    .line 1499
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getLineSpacing()F

    move-result v4

    move-object/from16 v0, v40

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineSpacing(F)V

    .line 1504
    :cond_26
    if-eqz v62, :cond_2c

    .line 1505
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBuNone()Z

    move-result v4

    if-nez v4, :cond_2c

    .line 1506
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBullet()Z

    move-result v4

    if-eqz v4, :cond_2c

    .line 1507
    if-eqz v64, :cond_27

    move-object/from16 v0, v64

    move-object/from16 v1, v39

    if-eq v0, v1, :cond_28

    .line 1509
    :cond_27
    const/16 v25, -0x1

    .line 1510
    move-object/from16 v64, v39

    .line 1512
    :cond_28
    move/from16 v0, v26

    move/from16 v1, v53

    if-eq v0, v1, :cond_29

    .line 1513
    move/from16 v0, v26

    move/from16 v1, v53

    if-le v0, v1, :cond_5e

    invoke-static/range {v53 .. v53}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_5e

    .line 1514
    invoke-static/range {v53 .. v53}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v25

    .line 1519
    :cond_29
    :goto_13
    invoke-virtual/range {v60 .. v60}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getRunList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eqz v4, :cond_2a

    .line 1520
    add-int/lit8 v25, v25, 0x1

    .line 1522
    :cond_2a
    if-eqz v43, :cond_67

    .line 1523
    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$dml$EBulletAutoNumType:[I

    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1569
    :cond_2b
    :goto_14
    invoke-static/range {v53 .. v53}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v27

    invoke-virtual {v0, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1574
    :cond_2c
    const/16 v48, 0x0

    .local v48, "j":I
    :goto_15
    invoke-interface/range {v66 .. v66}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v48

    if-ge v0, v4, :cond_76

    .line 1575
    move-object/from16 v0, v66

    move/from16 v1, v48

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v67

    check-cast v67, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    .line 1576
    .local v67, "run_text":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    invoke-virtual/range {v67 .. v67}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getText()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2d

    .line 1577
    invoke-virtual/range {v67 .. v67}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getText()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1580
    :cond_2d
    move-object/from16 v0, v66

    move/from16 v1, v48

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v65

    check-cast v65, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    .line 1581
    .local v65, "run":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    invoke-virtual/range {v65 .. v65}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v33

    .line 1582
    .local v33, "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    new-instance v69, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-direct/range {v69 .. v69}, Lcom/samsung/thumbnail/customview/word/Run;-><init>()V

    .line 1584
    .local v69, "textRun":Lcom/samsung/thumbnail/customview/word/Run;
    const-string/jumbo v29, ""

    .line 1586
    .local v29, "bullet":Ljava/lang/String;
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;->getBodyProp()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    move-result-object v4

    if-eqz v4, :cond_2e

    .line 1587
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;->getBodyProp()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    move-result-object v4

    iget v4, v4, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;->fontScale:I

    if-lez v4, :cond_2e

    .line 1588
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;->getBodyProp()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    move-result-object v5

    iget v5, v5, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;->fontScale:I

    int-to-float v5, v5

    invoke-static {v4, v5}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->getFitFontScale(FF)I

    move-result v4

    move-object/from16 v0, v33

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setFontSize(I)V

    .line 1594
    :cond_2e
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v4

    if-lez v4, :cond_68

    .line 1595
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v4

    mul-int/lit8 v4, v4, 0x55

    int-to-float v4, v4

    const/high16 v5, 0x42c80000    # 100.0f

    div-float/2addr v4, v5

    move-object/from16 v0, v69

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 1598
    :goto_16
    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getBold()Z

    move-result v4

    if-eqz v4, :cond_2f

    .line 1599
    invoke-virtual/range {v69 .. v69}, Lcom/samsung/thumbnail/customview/word/Run;->setTextBold()V

    .line 1600
    :cond_2f
    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getItalic()Z

    move-result v4

    if-eqz v4, :cond_30

    .line 1601
    invoke-virtual/range {v69 .. v69}, Lcom/samsung/thumbnail/customview/word/Run;->setTextItalics()V

    .line 1602
    :cond_30
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getUnderline()Z

    move-result v4

    if-eqz v4, :cond_31

    .line 1603
    const/4 v4, 0x1

    move-object/from16 v0, v69

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/word/Run;->setUnderlineText(Z)V

    .line 1604
    :cond_31
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getStrike()Z

    move-result v4

    if-nez v4, :cond_32

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getDStrike()Z

    move-result v4

    if-eqz v4, :cond_33

    .line 1605
    :cond_32
    const/4 v4, 0x1

    move-object/from16 v0, v69

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/word/Run;->setStrikeThruText(Z)V

    .line 1606
    :cond_33
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getSmallCaps()Z

    move-result v4

    if-eqz v4, :cond_34

    .line 1607
    invoke-virtual/range {v69 .. v69}, Lcom/samsung/thumbnail/customview/word/Run;->setSMALLLetterON()V

    .line 1608
    :cond_34
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getCaps()Z

    move-result v4

    if-eqz v4, :cond_35

    .line 1609
    invoke-virtual/range {v69 .. v69}, Lcom/samsung/thumbnail/customview/word/Run;->setCAPLetterON()V

    .line 1610
    :cond_35
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getHLink()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_36

    .line 1611
    const/16 v47, 0x1

    .line 1612
    :cond_36
    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v38

    .line 1613
    .local v38, "color_temp":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    if-nez v38, :cond_37

    .line 1614
    new-instance v38, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .end local v38    # "color_temp":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    invoke-direct/range {v38 .. v38}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 1616
    .restart local v38    # "color_temp":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    :cond_37
    if-eqz v38, :cond_6d

    .line 1617
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v4

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_38

    .line 1619
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v4

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1623
    :cond_38
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getLumMod()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_72

    .line 1624
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getLumMod()Ljava/lang/String;

    move-result-object v55

    .line 1625
    .restart local v55    # "lumMod":Ljava/lang/String;
    invoke-static/range {v55 .. v55}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v50

    .line 1626
    .restart local v50    # "lMod":D
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v4

    invoke-static/range {v50 .. v51}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcLuminousColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v45

    .line 1632
    .local v45, "fontColor":Ljava/lang/String;
    invoke-virtual/range {v45 .. v45}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x6

    if-ge v4, v5, :cond_6a

    .line 1633
    invoke-virtual/range {v45 .. v45}, Ljava/lang/String;->length()I

    move-result v4

    rsub-int/lit8 v52, v4, 0x6

    .line 1634
    .local v52, "length":I
    new-instance v59, Ljava/lang/StringBuffer;

    const-string/jumbo v4, ""

    move-object/from16 v0, v59

    invoke-direct {v0, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1635
    .local v59, "padding":Ljava/lang/StringBuffer;
    const/16 v49, 0x0

    .restart local v49    # "k":I
    :goto_17
    move/from16 v0, v49

    move/from16 v1, v52

    if-ge v0, v1, :cond_69

    .line 1636
    const-string/jumbo v4, "0"

    move-object/from16 v0, v59

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1635
    add-int/lit8 v49, v49, 0x1

    goto :goto_17

    .line 901
    .end local v9    # "alpha":Ljava/lang/String;
    .end local v13    # "tblBgColor":Ljava/lang/String;
    .end local v21    # "align":Ljava/lang/String;
    .end local v24    # "buChar":Ljava/lang/String;
    .end local v25    # "buCount":I
    .end local v26    # "buLevel":I
    .end local v27    # "buMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v28    # "buNum":Ljava/lang/String;
    .end local v29    # "bullet":Ljava/lang/String;
    .end local v31    # "cellBorders":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;>;"
    .end local v33    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .end local v37    # "colorStatus":Z
    .end local v38    # "color_temp":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .end local v39    # "curBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .end local v40    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v43    # "eBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .end local v45    # "fontColor":Ljava/lang/String;
    .end local v46    # "i":I
    .end local v47    # "isHlink":Z
    .end local v48    # "j":I
    .end local v49    # "k":I
    .end local v50    # "lMod":D
    .end local v52    # "length":I
    .end local v53    # "level":I
    .end local v54    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    .end local v55    # "lumMod":Ljava/lang/String;
    .end local v56    # "marginLeft":Ljava/lang/String;
    .end local v58    # "mstrBuilder":Ljava/lang/StringBuilder;
    .end local v59    # "padding":Ljava/lang/StringBuffer;
    .end local v60    # "para":Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;
    .end local v61    # "paraList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;>;"
    .end local v62    # "paraProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    .end local v63    # "preLevel":I
    .end local v64    # "prevBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .end local v65    # "run":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    .end local v66    # "runList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;>;"
    .end local v67    # "run_text":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    .end local v69    # "textRun":Lcom/samsung/thumbnail/customview/word/Run;
    .restart local v22    # "alpha":Ljava/lang/String;
    .restart local v36    # "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    :cond_39
    invoke-virtual/range {v36 .. v36}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    .line 904
    :cond_3a
    invoke-virtual/range {v36 .. v36}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    .line 927
    .end local v36    # "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .restart local v13    # "tblBgColor":Ljava/lang/String;
    .restart local v31    # "cellBorders":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;>;"
    .restart local v37    # "colorStatus":Z
    .restart local v49    # "k":I
    :cond_3b
    add-int/lit8 v49, v49, 0x1

    goto/16 :goto_1

    .line 979
    .end local v22    # "alpha":Ljava/lang/String;
    .restart local v9    # "alpha":Ljava/lang/String;
    :cond_3c
    if-eqz v13, :cond_6

    .line 980
    const-string/jumbo v9, "20000"

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v16, v6

    move-object/from16 v17, v7

    move-object/from16 v18, v13

    move-object/from16 v19, v9

    .line 981
    invoke-direct/range {v14 .. v19}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->applyCellColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 984
    const/16 v37, 0x1

    goto/16 :goto_4

    .line 966
    :cond_3d
    add-int/lit8 v49, v49, 0x1

    goto/16 :goto_3

    .line 998
    :cond_3e
    const/16 v49, 0x0

    :goto_18
    move/from16 v0, v49

    move/from16 v1, v68

    if-ge v0, v1, :cond_7

    .line 999
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillPropsLst()Ljava/util/HashMap;

    move-result-object v4

    const-string/jumbo v5, "band1V"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_40

    .line 1002
    const-string/jumbo v4, "band1V"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    move/from16 v3, v49

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getTableDefColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v8

    .line 1004
    const-string/jumbo v4, "band1V"

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move/from16 v2, v49

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getAlpha(Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    .line 1006
    if-eqz v8, :cond_3f

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    .line 1007
    invoke-direct/range {v4 .. v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->applyCellColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 1009
    const/16 v37, 0x1

    .line 1011
    :cond_3f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getTcBrdr()Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;->getBrdrLineProps()Ljava/util/HashMap;

    move-result-object v31

    .line 1014
    if-eqz v31, :cond_7

    .line 1015
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->setTableLineProperties(Ljava/util/HashMap;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    goto/16 :goto_5

    .line 998
    :cond_40
    add-int/lit8 v49, v49, 0x1

    goto :goto_18

    .line 1023
    :cond_41
    rem-int/lit8 v4, p3, 0x2

    if-eqz v4, :cond_45

    .line 1024
    const/16 v49, 0x0

    :goto_19
    move/from16 v0, v49

    move/from16 v1, v68

    if-ge v0, v1, :cond_7

    .line 1025
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillPropsLst()Ljava/util/HashMap;

    move-result-object v4

    const-string/jumbo v5, "band2V"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_44

    .line 1028
    const-string/jumbo v4, "band2V"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    move/from16 v3, v49

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getTableDefColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v8

    .line 1030
    const-string/jumbo v4, "band2V"

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move/from16 v2, v49

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getAlpha(Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    .line 1032
    if-eqz v8, :cond_43

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    .line 1033
    invoke-direct/range {v4 .. v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->applyCellColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 1035
    const/16 v37, 0x1

    .line 1045
    :cond_42
    :goto_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getTcBrdr()Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;->getBrdrLineProps()Ljava/util/HashMap;

    move-result-object v31

    .line 1048
    if-eqz v31, :cond_7

    .line 1049
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->setTableLineProperties(Ljava/util/HashMap;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    goto/16 :goto_5

    .line 1037
    :cond_43
    if-eqz v13, :cond_42

    .line 1038
    const-string/jumbo v9, "20000"

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v16, v6

    move-object/from16 v17, v7

    move-object/from16 v18, v13

    move-object/from16 v19, v9

    .line 1039
    invoke-direct/range {v14 .. v19}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->applyCellColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 1042
    const/16 v37, 0x1

    goto :goto_1a

    .line 1024
    :cond_44
    add-int/lit8 v49, v49, 0x1

    goto/16 :goto_19

    .line 1056
    :cond_45
    const/16 v49, 0x0

    :goto_1b
    move/from16 v0, v49

    move/from16 v1, v68

    if-ge v0, v1, :cond_7

    .line 1057
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillPropsLst()Ljava/util/HashMap;

    move-result-object v4

    const-string/jumbo v5, "band1V"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_47

    .line 1060
    const-string/jumbo v4, "band1V"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    move/from16 v3, v49

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getTableDefColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v8

    .line 1062
    const-string/jumbo v4, "band1V"

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move/from16 v2, v49

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getAlpha(Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    .line 1064
    if-eqz v8, :cond_46

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    .line 1065
    invoke-direct/range {v4 .. v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->applyCellColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 1067
    const/16 v37, 0x1

    .line 1069
    :cond_46
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getTcBrdr()Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;->getBrdrLineProps()Ljava/util/HashMap;

    move-result-object v31

    .line 1072
    if-eqz v31, :cond_7

    .line 1073
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->setTableLineProperties(Ljava/util/HashMap;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    goto/16 :goto_5

    .line 1056
    :cond_47
    add-int/lit8 v49, v49, 0x1

    goto :goto_1b

    .line 1101
    :cond_48
    if-eqz v13, :cond_8

    .line 1102
    const-string/jumbo v9, "20000"

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v16, v6

    move-object/from16 v17, v7

    move-object/from16 v18, v13

    move-object/from16 v19, v9

    .line 1103
    invoke-direct/range {v14 .. v19}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->applyCellColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 1106
    const/16 v37, 0x1

    goto/16 :goto_7

    .line 1088
    :cond_49
    add-int/lit8 v49, v49, 0x1

    goto/16 :goto_6

    .line 1120
    :cond_4a
    const/16 v49, 0x0

    :goto_1c
    move/from16 v0, v49

    move/from16 v1, v68

    if-ge v0, v1, :cond_9

    .line 1121
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillPropsLst()Ljava/util/HashMap;

    move-result-object v4

    const-string/jumbo v5, "band1H"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4c

    .line 1124
    const-string/jumbo v4, "band1H"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    move/from16 v3, v49

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getTableDefColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v8

    .line 1126
    const-string/jumbo v4, "band1H"

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move/from16 v2, v49

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getAlpha(Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    .line 1128
    if-eqz v8, :cond_4b

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    .line 1129
    invoke-direct/range {v4 .. v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->applyCellColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 1131
    const/16 v37, 0x1

    .line 1133
    :cond_4b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getTcBrdr()Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;->getBrdrLineProps()Ljava/util/HashMap;

    move-result-object v31

    .line 1136
    if-eqz v31, :cond_9

    .line 1137
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->setTableLineProperties(Ljava/util/HashMap;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    goto/16 :goto_8

    .line 1120
    :cond_4c
    add-int/lit8 v49, v49, 0x1

    goto :goto_1c

    .line 1145
    :cond_4d
    rem-int/lit8 v4, p7, 0x2

    if-eqz v4, :cond_51

    .line 1146
    const/16 v49, 0x0

    :goto_1d
    move/from16 v0, v49

    move/from16 v1, v68

    if-ge v0, v1, :cond_9

    .line 1147
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillPropsLst()Ljava/util/HashMap;

    move-result-object v4

    const-string/jumbo v5, "band2H"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_50

    .line 1150
    const-string/jumbo v4, "band2H"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    move/from16 v3, v49

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getTableDefColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v8

    .line 1152
    const-string/jumbo v4, "band2H"

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move/from16 v2, v49

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getAlpha(Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    .line 1154
    if-eqz v8, :cond_4f

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    .line 1155
    invoke-direct/range {v4 .. v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->applyCellColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 1157
    const/16 v37, 0x1

    .line 1167
    :cond_4e
    :goto_1e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getTcBrdr()Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;->getBrdrLineProps()Ljava/util/HashMap;

    move-result-object v31

    .line 1170
    if-eqz v31, :cond_9

    .line 1171
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->setTableLineProperties(Ljava/util/HashMap;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    goto/16 :goto_8

    .line 1159
    :cond_4f
    if-eqz v13, :cond_4e

    .line 1160
    const-string/jumbo v9, "20000"

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v16, v6

    move-object/from16 v17, v7

    move-object/from16 v18, v13

    move-object/from16 v19, v9

    .line 1161
    invoke-direct/range {v14 .. v19}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->applyCellColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 1164
    const/16 v37, 0x1

    goto :goto_1e

    .line 1146
    :cond_50
    add-int/lit8 v49, v49, 0x1

    goto/16 :goto_1d

    .line 1178
    :cond_51
    const/16 v49, 0x0

    :goto_1f
    move/from16 v0, v49

    move/from16 v1, v68

    if-ge v0, v1, :cond_9

    .line 1179
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillPropsLst()Ljava/util/HashMap;

    move-result-object v4

    const-string/jumbo v5, "band1H"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_53

    .line 1182
    const-string/jumbo v4, "band1H"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    move/from16 v3, v49

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getTableDefColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v8

    .line 1184
    const-string/jumbo v4, "band1H"

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move/from16 v2, v49

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getAlpha(Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    .line 1186
    if-eqz v8, :cond_52

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    .line 1187
    invoke-direct/range {v4 .. v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->applyCellColor(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    .line 1189
    const/16 v37, 0x1

    .line 1191
    :cond_52
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->getTblFormatList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move/from16 v0, v49

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getTcBrdr()Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;->getBrdrLineProps()Ljava/util/HashMap;

    move-result-object v31

    .line 1194
    if-eqz v31, :cond_9

    .line 1195
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->setTableLineProperties(Ljava/util/HashMap;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v7

    goto/16 :goto_8

    .line 1178
    :cond_53
    add-int/lit8 v49, v49, 0x1

    goto :goto_1f

    .line 1207
    :cond_54
    add-int/lit8 v49, v49, 0x1

    goto/16 :goto_9

    .line 1255
    :cond_55
    add-int/lit8 v49, v49, 0x1

    goto/16 :goto_a

    .line 1302
    :cond_56
    add-int/lit8 v49, v49, 0x1

    goto/16 :goto_b

    .line 1349
    :cond_57
    add-int/lit8 v49, v49, 0x1

    goto/16 :goto_c

    .line 1406
    .end local v49    # "k":I
    :cond_58
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getRowSpan()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_1f

    .line 1407
    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;->RESTART:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellMergeValue(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 1471
    .restart local v21    # "align":Ljava/lang/String;
    .restart local v24    # "buChar":Ljava/lang/String;
    .restart local v25    # "buCount":I
    .restart local v26    # "buLevel":I
    .restart local v27    # "buMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .restart local v28    # "buNum":Ljava/lang/String;
    .restart local v39    # "curBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .restart local v40    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .restart local v43    # "eBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .restart local v46    # "i":I
    .restart local v47    # "isHlink":Z
    .restart local v53    # "level":I
    .restart local v54    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    .restart local v56    # "marginLeft":Ljava/lang/String;
    .restart local v58    # "mstrBuilder":Ljava/lang/StringBuilder;
    .restart local v60    # "para":Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;
    .restart local v61    # "paraList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;>;"
    .restart local v62    # "paraProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    .restart local v63    # "preLevel":I
    .restart local v64    # "prevBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .restart local v66    # "runList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;>;"
    :catch_0
    move-exception v42

    .line 1472
    .local v42, "e":Ljava/lang/Exception;
    const-string/jumbo v4, "XSLFTable"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Exception : "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v42 .. v42}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_10

    .line 1482
    .end local v42    # "e":Ljava/lang/Exception;
    :cond_59
    const-string/jumbo v4, "right"

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5a

    .line 1483
    sget-object v4, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->RIGHT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v40

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_11

    .line 1484
    :cond_5a
    const-string/jumbo v4, "left"

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5b

    .line 1485
    sget-object v4, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->LEFT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v40

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_11

    .line 1486
    :cond_5b
    const-string/jumbo v4, "justify"

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_25

    .line 1487
    sget-object v4, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->JUSTIFY:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v40

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_11

    .line 1491
    :cond_5c
    const-string/jumbo v4, "b"

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;->getBodyProp()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;->getvAlign()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5d

    .line 1492
    sget-object v4, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->BOTTOM:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-object/from16 v0, v40

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    goto/16 :goto_12

    .line 1494
    :cond_5d
    sget-object v4, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->TOP:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-object/from16 v0, v40

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    goto/16 :goto_12

    .line 1515
    :cond_5e
    move/from16 v0, v26

    move/from16 v1, v53

    if-ne v0, v1, :cond_29

    .line 1516
    const/16 v25, -0x1

    goto/16 :goto_13

    .line 1525
    :pswitch_0
    rem-int/lit8 v4, v25, 0x1a

    add-int/lit8 v4, v4, 0x61

    int-to-char v0, v4

    move/from16 v20, v0

    .line 1526
    .local v20, "a":C
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v20 .. v20}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    .line 1527
    goto/16 :goto_14

    .line 1529
    .end local v20    # "a":C
    :pswitch_1
    rem-int/lit8 v4, v25, 0x1a

    add-int/lit8 v4, v4, 0x41

    int-to-char v0, v4

    move/from16 v23, v0

    .line 1530
    .local v23, "b":C
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v23 .. v23}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    .line 1531
    goto/16 :goto_14

    .line 1533
    .end local v23    # "b":C
    :pswitch_2
    rem-int/lit8 v4, v25, 0x1a

    add-int/lit8 v4, v4, 0x61

    int-to-char v0, v4

    move/from16 v30, v0

    .line 1534
    .local v30, "c":C
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v30 .. v30}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    .line 1536
    goto/16 :goto_14

    .line 1538
    .end local v30    # "c":C
    :pswitch_3
    const/4 v4, -0x1

    move/from16 v0, v25

    if-eq v0, v4, :cond_5f

    if-nez v25, :cond_60

    .line 1539
    :cond_5f
    const/16 v25, 0x1

    .line 1541
    :cond_60
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v25 .. v25}, Lcom/samsung/thumbnail/util/Utils;->binaryToRomanUC(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    .line 1543
    goto/16 :goto_14

    .line 1545
    :pswitch_4
    const/4 v4, -0x1

    move/from16 v0, v25

    if-eq v0, v4, :cond_61

    if-nez v25, :cond_62

    .line 1546
    :cond_61
    const/16 v25, 0x1

    .line 1548
    :cond_62
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v25 .. v25}, Lcom/samsung/thumbnail/util/Utils;->binaryToRomanLC(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    .line 1550
    goto/16 :goto_14

    .line 1552
    :pswitch_5
    const/4 v4, -0x1

    move/from16 v0, v25

    if-eq v0, v4, :cond_63

    if-nez v25, :cond_64

    .line 1553
    :cond_63
    const/16 v25, 0x1

    .line 1555
    :cond_64
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    .line 1556
    goto/16 :goto_14

    .line 1558
    :pswitch_6
    const/4 v4, -0x1

    move/from16 v0, v25

    if-eq v0, v4, :cond_65

    if-nez v25, :cond_66

    .line 1559
    :cond_65
    const/16 v25, 0x1

    .line 1561
    :cond_66
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    .line 1562
    goto/16 :goto_14

    .line 1566
    :cond_67
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getBulletChar()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2b

    .line 1567
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getBulletChar()Ljava/lang/String;

    move-result-object v24

    goto/16 :goto_14

    .line 1597
    .restart local v29    # "bullet":Ljava/lang/String;
    .restart local v33    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .restart local v48    # "j":I
    .restart local v65    # "run":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    .restart local v67    # "run_text":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    .restart local v69    # "textRun":Lcom/samsung/thumbnail/customview/word/Run;
    :cond_68
    const/high16 v4, 0x41700000    # 15.0f

    move-object/from16 v0, v69

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    goto/16 :goto_16

    .line 1638
    .restart local v38    # "color_temp":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .restart local v45    # "fontColor":Ljava/lang/String;
    .restart local v49    # "k":I
    .restart local v50    # "lMod":D
    .restart local v52    # "length":I
    .restart local v55    # "lumMod":Ljava/lang/String;
    .restart local v59    # "padding":Ljava/lang/StringBuffer;
    :cond_69
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v59 .. v59}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v45

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    .line 1640
    .end local v49    # "k":I
    .end local v52    # "length":I
    .end local v59    # "padding":Ljava/lang/StringBuffer;
    :cond_6a
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "#"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v45

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v69

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 1647
    .end local v45    # "fontColor":Ljava/lang/String;
    .end local v50    # "lMod":D
    .end local v55    # "lumMod":Ljava/lang/String;
    :cond_6b
    :goto_20
    if-eqz v47, :cond_6c

    .line 1648
    const-string/jumbo v4, "#0000FF"

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v69

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 1651
    :cond_6c
    const/4 v4, 0x0

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setSchemeClr(Z)V

    .line 1653
    :cond_6d
    if-nez v48, :cond_6e

    .line 1654
    if-eqz v28, :cond_73

    .line 1655
    const/4 v4, 0x0

    move-object/from16 v0, v33

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setUndreline(Z)V

    .line 1656
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    .line 1663
    :cond_6e
    :goto_21
    invoke-virtual/range {v65 .. v65}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getText()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "<br>"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_74

    .line 1664
    move-object/from16 v0, v54

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1665
    move-object/from16 v41, v40

    .line 1666
    .local v41, "docPara_temp":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    new-instance v40, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .end local v40    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-direct/range {v40 .. v40}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    .line 1667
    .restart local v40    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-virtual/range {v41 .. v41}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v4

    move-object/from16 v0, v40

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    .line 1668
    if-eqz v56, :cond_6f

    .line 1669
    invoke-virtual/range {v41 .. v41}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v4

    move-object/from16 v0, v40

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    .line 1670
    :cond_6f
    if-eqz v21, :cond_70

    .line 1671
    invoke-virtual/range {v41 .. v41}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getAlignment()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-result-object v4

    move-object/from16 v0, v40

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    .line 1672
    :cond_70
    invoke-virtual/range {v41 .. v41}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLineSpacing()F

    move-result v4

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_71

    .line 1673
    invoke-virtual/range {v41 .. v41}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLineSpacing()F

    move-result v4

    move-object/from16 v0, v40

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineSpacing(F)V

    .line 1574
    .end local v41    # "docPara_temp":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    :cond_71
    :goto_22
    add-int/lit8 v48, v48, 0x1

    goto/16 :goto_15

    .line 1643
    :cond_72
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_6b

    .line 1644
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "#"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v69

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    goto/16 :goto_20

    .line 1657
    :cond_73
    if-eqz v24, :cond_6e

    .line 1658
    const-string/jumbo v4, "Wingdings"

    move-object/from16 v0, v33

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setFontName(Ljava/lang/String;)V

    .line 1659
    const/4 v4, 0x0

    move-object/from16 v0, v33

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setUndreline(Z)V

    .line 1660
    const-string/jumbo v29, "\u2022 "

    goto/16 :goto_21

    .line 1677
    :cond_74
    invoke-virtual/range {v29 .. v29}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_75

    .line 1678
    invoke-virtual/range {v65 .. v65}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getText()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v69

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    .line 1681
    :goto_23
    move-object/from16 v0, v40

    move-object/from16 v1, v69

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto :goto_22

    .line 1680
    :cond_75
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v65 .. v65}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v69

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    goto :goto_23

    .line 1684
    .end local v29    # "bullet":Ljava/lang/String;
    .end local v33    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .end local v38    # "color_temp":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .end local v65    # "run":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    .end local v67    # "run_text":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    .end local v69    # "textRun":Lcom/samsung/thumbnail/customview/word/Run;
    :cond_76
    move-object/from16 v0, v54

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1433
    add-int/lit8 v46, v46, 0x1

    goto/16 :goto_f

    .line 1687
    .end local v21    # "align":Ljava/lang/String;
    .end local v24    # "buChar":Ljava/lang/String;
    .end local v28    # "buNum":Ljava/lang/String;
    .end local v39    # "curBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .end local v40    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v43    # "eBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .end local v48    # "j":I
    .end local v53    # "level":I
    .end local v56    # "marginLeft":Ljava/lang/String;
    .end local v60    # "para":Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;
    .end local v62    # "paraProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    .end local v66    # "runList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;>;"
    :cond_77
    move-object/from16 v0, v54

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->addDocParaList(Ljava/util/ArrayList;)V

    .line 1688
    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellText(Ljava/lang/String;)V

    .line 1690
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getCurrentRow()Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->addCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 1692
    return-void

    .end local v9    # "alpha":Ljava/lang/String;
    .end local v25    # "buCount":I
    .end local v26    # "buLevel":I
    .end local v27    # "buMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v46    # "i":I
    .end local v47    # "isHlink":Z
    .end local v54    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    .end local v58    # "mstrBuilder":Ljava/lang/StringBuilder;
    .end local v61    # "paraList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;>;"
    .end local v63    # "preLevel":I
    .end local v64    # "prevBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .restart local v22    # "alpha":Ljava/lang/String;
    .restart local v49    # "k":I
    :cond_78
    move-object/from16 v9, v22

    .end local v22    # "alpha":Ljava/lang/String;
    .restart local v9    # "alpha":Ljava/lang/String;
    goto/16 :goto_2

    .end local v9    # "alpha":Ljava/lang/String;
    .end local v49    # "k":I
    .restart local v22    # "alpha":Ljava/lang/String;
    :cond_79
    move-object/from16 v9, v22

    .end local v22    # "alpha":Ljava/lang/String;
    .restart local v9    # "alpha":Ljava/lang/String;
    goto/16 :goto_d

    .line 1523
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public addCellWidth(J)V
    .locals 3
    .param p1, "width"    # J

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->cellWidth:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    return-void
.end method

.method public addRow(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;)V
    .locals 1
    .param p1, "row"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->rows:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    return-void
.end method

.method public drawTable(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)Lcom/samsung/thumbnail/customview/tables/CanvasTable;
    .locals 28
    .param p1, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p2, "xmlSlideShow"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;
    .param p3, "tableStyles"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;
    .param p4, "mCanvasElementsCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    .line 730
    new-instance v2, Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    .line 731
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->NORMAL:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->setTableType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)V

    .line 733
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getAnchor()Lorg/apache/poi/java/awt/geom/Rectangle2D;

    move-result-object v11

    .line 734
    .local v11, "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    invoke-virtual {v11}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v20

    .line 735
    .local v20, "xPos":D
    invoke-virtual {v11}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v22

    .line 736
    .local v22, "yPos":D
    invoke-virtual {v11}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v18

    .line 738
    .local v18, "width":D
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    move-wide/from16 v0, v20

    double-to-float v3, v0

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->setTableX(F)V

    .line 739
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    move-wide/from16 v0, v22

    double-to-float v3, v0

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->setTableY(F)V

    .line 741
    move-wide/from16 v0, v18

    double-to-float v2, v0

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twentiethsToPoints(F)F

    move-result v2

    float-to-int v2, v2

    int-to-double v2, v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v24

    cmpg-double v2, v2, v24

    if-gtz v2, :cond_0

    .line 745
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    move-wide/from16 v0, v18

    double-to-int v3, v0

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->setTableWidth(I)V

    .line 748
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getRows()Ljava/util/List;

    move-result-object v16

    .line 749
    .local v16, "tableRows":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->noOfRows:I

    .line 751
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->noOfRows:I

    if-ge v9, v2, :cond_9

    .line 754
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->createNewRow()V

    .line 756
    move-object/from16 v0, v16

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;

    .line 759
    .local v4, "tblRow":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 770
    .local v7, "overrideTypes":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;>;"
    if-eqz v4, :cond_3

    .line 771
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;->getHeight()D

    move-result-wide v2

    const-wide/16 v24, 0x0

    cmpl-double v2, v2, v24

    if-lez v2, :cond_2

    .line 773
    const/high16 v15, 0x41a00000    # 20.0f

    .line 776
    .local v15, "height":F
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;->getHeight()D

    move-result-wide v2

    const-wide/high16 v24, 0x4034000000000000L    # 20.0

    cmpl-double v2, v2, v24

    if-lez v2, :cond_1

    .line 777
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;->getHeight()D

    move-result-wide v2

    double-to-float v15, v2

    .line 779
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getCurrentRow()Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-result-object v2

    invoke-virtual {v2, v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->setRowHeight(F)V

    .line 794
    .end local v15    # "height":F
    :goto_1
    if-eqz v4, :cond_8

    .line 796
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;->getCells()Ljava/util/List;

    move-result-object v13

    .line 798
    .local v13, "cellLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;>;"
    const/4 v6, 0x0

    .line 799
    .local v6, "totalIndexNumber":I
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->noOfCols:I

    .line 801
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->noOfCols:I

    if-ge v5, v2, :cond_8

    .line 802
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;->getCells()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;

    .line 804
    .local v12, "cell":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v14

    .line 806
    .local v14, "cellProp":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    if-eqz v14, :cond_4

    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getHMerge()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 801
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 784
    .end local v5    # "j":I
    .end local v6    # "totalIndexNumber":I
    .end local v12    # "cell":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;
    .end local v13    # "cellLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;>;"
    .end local v14    # "cellProp":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getCurrentRow()Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-result-object v2

    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->setRowHeight(F)V

    goto :goto_1

    .line 790
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getCurrentRow()Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-result-object v2

    const/high16 v3, 0x43a50000    # 330.0f

    invoke-static {v3}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twentiethsToPoints(F)F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->setRowHeight(F)V

    goto :goto_1

    .line 809
    .restart local v5    # "j":I
    .restart local v6    # "totalIndexNumber":I
    .restart local v12    # "cell":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;
    .restart local v13    # "cellLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;>;"
    .restart local v14    # "cellProp":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    :cond_4
    if-eqz v14, :cond_7

    .line 810
    const/4 v10, 0x0

    .line 812
    .local v10, "cellWidth":I
    const/16 v17, 0x0

    .local v17, "x":I
    :goto_4
    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getGridSpan()I

    move-result v2

    move/from16 v0, v17

    if-ge v0, v2, :cond_6

    .line 813
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getCellWidth()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getCellWidth()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int v3, v6, v17

    if-lt v2, v3, :cond_5

    .line 815
    int-to-long v0, v10

    move-wide/from16 v24, v0

    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeWidth()J

    move-result-wide v26

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->getCellWidth()Ljava/util/ArrayList;

    move-result-object v2

    add-int v3, v6, v17

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    add-long v2, v2, v26

    add-long v2, v2, v24

    long-to-int v10, v2

    .line 812
    :cond_5
    add-int/lit8 v17, v17, 0x1

    goto :goto_4

    :cond_6
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v8, p3

    .line 819
    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->writeTableCell(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;IILjava/util/List;Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;II)V

    .line 821
    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getGridSpan()I

    move-result v2

    add-int/2addr v6, v2

    .line 823
    goto :goto_3

    .line 824
    .end local v10    # "cellWidth":I
    .end local v17    # "x":I
    :cond_7
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 831
    .end local v5    # "j":I
    .end local v6    # "totalIndexNumber":I
    .end local v12    # "cell":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;
    .end local v13    # "cellLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;>;"
    .end local v14    # "cellProp":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getCurrentRow()Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getCurrentRow()Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getRowHeight()F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->updateRowHeight(F)V

    .line 751
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 836
    .end local v4    # "tblRow":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;
    .end local v7    # "overrideTypes":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;>;"
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    return-object v2
.end method

.method public getAnchor()Lorg/apache/poi/java/awt/geom/Rectangle2D;
    .locals 10

    .prologue
    .line 134
    new-instance v1, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;

    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->shX:J

    long-to-double v2, v2

    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->shY:J

    long-to-double v4, v4

    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->shW:J

    long-to-double v6, v6

    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    iget-wide v8, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->shH:J

    long-to-double v8, v8

    invoke-direct/range {v1 .. v9}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;-><init>(DDDD)V

    return-object v1
.end method

.method public getBandCol()I
    .locals 1

    .prologue
    .line 179
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->bandCol:I

    return v0
.end method

.method public getBandRow()I
    .locals 1

    .prologue
    .line 171
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->bandRow:I

    return v0
.end method

.method public getCellWidth()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->cellWidth:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getFirstCol()I
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->firstCol:I

    return v0
.end method

.method public getFirstRow()I
    .locals 1

    .prologue
    .line 139
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->firstRow:I

    return v0
.end method

.method public getGridColWidthArray()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 191
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->mGridColWidthArray:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getLastCol()I
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->lastCol:I

    return v0
.end method

.method public getLastRow()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->lastRow:I

    return v0
.end method

.method public getRows()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->rows:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getStyleId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->rows:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public setAnchor(Lorg/apache/poi/java/awt/geom/Rectangle2D;)V
    .locals 0
    .param p1, "anchor"    # Lorg/apache/poi/java/awt/geom/Rectangle2D;

    .prologue
    .line 205
    return-void
.end method

.method public setBandCol(I)V
    .locals 0
    .param p1, "bandCol"    # I

    .prologue
    .line 183
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->bandCol:I

    .line 184
    return-void
.end method

.method public setBandRow(I)V
    .locals 0
    .param p1, "bandRow"    # I

    .prologue
    .line 175
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->bandRow:I

    .line 176
    return-void
.end method

.method public setFirstCol(I)V
    .locals 0
    .param p1, "firstCol"    # I

    .prologue
    .line 159
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->firstCol:I

    .line 160
    return-void
.end method

.method public setFirstRow(I)V
    .locals 0
    .param p1, "firstRow"    # I

    .prologue
    .line 143
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->firstRow:I

    .line 144
    return-void
.end method

.method public setGridColWidthArray(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 187
    .local p1, "gridColArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->mGridColWidthArray:Ljava/util/ArrayList;

    .line 188
    return-void
.end method

.method public setLastCol(I)V
    .locals 0
    .param p1, "lastCol"    # I

    .prologue
    .line 167
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->lastCol:I

    .line 168
    return-void
.end method

.method public setLastRow(I)V
    .locals 0
    .param p1, "lastRow"    # I

    .prologue
    .line 151
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->lastRow:I

    .line 152
    return-void
.end method

.method public setShapeProperties(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 0
    .param p1, "shapeProp"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 88
    return-void
.end method

.method public setStyleId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->styleId:Ljava/lang/String;

    .line 106
    return-void
.end method
