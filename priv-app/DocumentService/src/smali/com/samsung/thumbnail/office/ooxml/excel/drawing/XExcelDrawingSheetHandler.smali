.class public Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingSheetHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelDrawingSheetHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 13
    const/16 v2, 0xc9

    const-string/jumbo v3, "wsDr"

    invoke-direct {p0, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 17
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingTwoCellAnchorHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingTwoCellAnchorHandler;-><init>()V

    .line 18
    .local v1, "twoachorHandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingTwoCellAnchorHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingSheetHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "twoCellAnchor"

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingOneCellAnchorHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingOneCellAnchorHandler;-><init>()V

    .line 22
    .local v0, "oneachorHandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingOneCellAnchorHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingSheetHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "oneCellAnchor"

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    return-void
.end method
