.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRunHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XDocRunHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPTabHandler$IPTabContObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$IXRPrObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabHandler$ITabContObserver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    const-string/jumbo v0, "r"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 22
    return-void
.end method

.method private init()V
    .locals 23

    .prologue
    .line 26
    const/16 v22, 0x9

    move/from16 v0, v22

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    move-object/from16 v17, v0

    .line 28
    .local v17, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v20, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTextHandler;

    invoke-direct/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTextHandler;-><init>()V

    .line 29
    .local v20, "textHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTextHandler;
    new-instance v21, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v22, "t"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 31
    .local v21, "textSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v22, 0x0

    aput-object v21, v17, v22

    .line 33
    new-instance v15, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$IXRPrObserver;)V

    .line 34
    .local v15, "rPrHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;
    new-instance v16, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v22, "rPr"

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v15}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 36
    .local v16, "rprSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v22, 0x1

    aput-object v16, v17, v22

    .line 38
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabHandler;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabHandler$ITabContObserver;)V

    .line 39
    .local v18, "tabHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabHandler;
    new-instance v19, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v22, "tab"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 41
    .local v19, "tabSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v22, 0x2

    aput-object v19, v17, v22

    .line 43
    new-instance v13, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPictHandler;

    invoke-direct {v13}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPictHandler;-><init>()V

    .line 44
    .local v13, "pictHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPictHandler;
    new-instance v14, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v22, "pict"

    move-object/from16 v0, v22

    invoke-direct {v14, v0, v13}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 46
    .local v14, "pictSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v22, 0x3

    aput-object v14, v17, v22

    .line 48
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/word/drawing/XDocDrawingHandler;

    invoke-direct {v7}, Lcom/samsung/thumbnail/office/ooxml/word/drawing/XDocDrawingHandler;-><init>()V

    .line 49
    .local v7, "drawingHanlder":Lcom/samsung/thumbnail/office/ooxml/word/drawing/XDocDrawingHandler;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v22, "drawing"

    move-object/from16 v0, v22

    invoke-direct {v8, v0, v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 51
    .local v8, "drawingSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v22, 0x4

    aput-object v8, v17, v22

    .line 53
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBreakHandler;

    invoke-direct {v5}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBreakHandler;-><init>()V

    .line 54
    .local v5, "breakHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBreakHandler;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v22, "br"

    move-object/from16 v0, v22

    invoke-direct {v6, v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 56
    .local v6, "breakSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v22, 0x5

    aput-object v6, v17, v22

    .line 58
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocObjectHandler;

    invoke-direct {v9}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocObjectHandler;-><init>()V

    .line 59
    .local v9, "objHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocObjectHandler;
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v22, "object"

    move-object/from16 v0, v22

    invoke-direct {v10, v0, v9}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 61
    .local v10, "objSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v22, 0x6

    aput-object v10, v17, v22

    .line 63
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocAlternateContentHandler;

    invoke-direct {v3}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocAlternateContentHandler;-><init>()V

    .line 64
    .local v3, "alternateContentHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocAlternateContentHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/word/document/MCMLSequenceDescriptor;

    const-string/jumbo v22, "AlternateContent"

    move-object/from16 v0, v22

    invoke-direct {v4, v0, v3}, Lcom/samsung/thumbnail/office/ooxml/word/document/MCMLSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 66
    .local v4, "alternateSeq":Lcom/samsung/thumbnail/office/ooxml/word/document/MCMLSequenceDescriptor;
    const/16 v22, 0x7

    aput-object v4, v17, v22

    .line 68
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPTabHandler;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPTabHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPTabHandler$IPTabContObserver;)V

    .line 69
    .local v11, "pTabHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPTabHandler;
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v22, "ptab"

    move-object/from16 v0, v22

    invoke-direct {v12, v0, v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 71
    .local v12, "pTabSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v22, 0x8

    aput-object v12, v17, v22

    .line 73
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRunHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 75
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 98
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 99
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->endText()V

    .line 100
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->endTextRun()V

    .line 101
    return-void
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 88
    invoke-virtual {p0, p2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRunHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 89
    .local v0, "elementName":Ljava/lang/String;
    const-string/jumbo v1, "lastRenderedPageBreak"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 90
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v1}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->lastRenderedPageBreak()V

    .line 94
    :goto_0
    return-void

    .line 92
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    goto :goto_0
.end method

.method public onRPrFinsihed(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p1, "charProperties"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .param p2, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 106
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->setCharacterProperty(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V

    .line 108
    return-void
.end method

.method public setPTabContent(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "alignment"    # Ljava/lang/String;

    .prologue
    .line 120
    if-eqz p1, :cond_0

    .line 121
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->addPTabCharacter(Ljava/lang/String;)V

    .line 124
    :cond_0
    return-void
.end method

.method public setTabContent(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;I)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "position"    # I

    .prologue
    .line 112
    if-eqz p1, :cond_0

    .line 113
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->addTabCharacter(Ljava/lang/String;I)V

    .line 116
    :cond_0
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 80
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 81
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRunHandler;->init()V

    .line 82
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->startTextRun()V

    .line 83
    return-void
.end method
