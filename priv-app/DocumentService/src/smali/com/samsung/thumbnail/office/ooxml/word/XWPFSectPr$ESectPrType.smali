.class public final enum Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;
.super Ljava/lang/Enum;
.source "XWPFSectPr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ESectPrType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

.field public static final enum CONTINUOUS:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

.field public static final enum EVEN_PAGE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

.field public static final enum NEXT_COLUMN:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

.field public static final enum NEXT_PAGE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

.field public static final enum ODD_PAGE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 277
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    const-string/jumbo v1, "NEXT_PAGE"

    const-string/jumbo v2, "nextPage"

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;->NEXT_PAGE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    .line 280
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    const-string/jumbo v1, "NEXT_COLUMN"

    const-string/jumbo v2, "nextColumn"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;->NEXT_COLUMN:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    .line 283
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    const-string/jumbo v1, "CONTINUOUS"

    const-string/jumbo v2, "continuous"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;->CONTINUOUS:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    .line 286
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    const-string/jumbo v1, "EVEN_PAGE"

    const-string/jumbo v2, "evenPage"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;->EVEN_PAGE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    .line 289
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    const-string/jumbo v1, "ODD_PAGE"

    const-string/jumbo v2, "oddPage"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;->ODD_PAGE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    .line 273
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;->NEXT_PAGE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;->NEXT_COLUMN:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;->CONTINUOUS:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;->EVEN_PAGE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;->ODD_PAGE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 292
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 293
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;->value:Ljava/lang/String;

    .line 294
    return-void
.end method

.method public static convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 301
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;->values()[Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 302
    .local v2, "inst":Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 306
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;
    :goto_1
    return-object v2

    .line 301
    .restart local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 306
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 273
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;
    .locals 1

    .prologue
    .line 273
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    return-object v0
.end method


# virtual methods
.method public xmlValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;->value:Ljava/lang/String;

    return-object v0
.end method
