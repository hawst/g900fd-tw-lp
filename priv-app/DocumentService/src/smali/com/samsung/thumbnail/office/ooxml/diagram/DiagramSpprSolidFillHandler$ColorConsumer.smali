.class public Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprSolidFillHandler$ColorConsumer;
.super Ljava/lang/Object;
.source "DiagramSpprSolidFillHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprSolidFillHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "ColorConsumer"
.end annotation


# instance fields
.field fillConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;

.field fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;)V
    .locals 0
    .param p1, "fillConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprSolidFillHandler$ColorConsumer;->fillConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;

    .line 35
    return-void
.end method


# virtual methods
.method public consumeColor(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 0
    .param p1, "clrScheme"    # Ljava/lang/String;
    .param p2, "val"    # Ljava/lang/String;
    .param p3, "fillProp"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    .param p4, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 41
    invoke-virtual {p3, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setForeColor(Ljava/lang/String;)V

    .line 42
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprSolidFillHandler$ColorConsumer;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .line 43
    return-void
.end method
