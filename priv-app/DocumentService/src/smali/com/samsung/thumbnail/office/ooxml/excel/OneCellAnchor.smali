.class public Lcom/samsung/thumbnail/office/ooxml/excel/OneCellAnchor;
.super Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;
.source "OneCellAnchor.java"


# instance fields
.field private cx:J

.field private cy:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;-><init>()V

    return-void
.end method


# virtual methods
.method public getCX()J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/OneCellAnchor;->cx:J

    return-wide v0
.end method

.method public getCY()J
    .locals 2

    .prologue
    .line 29
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/OneCellAnchor;->cy:J

    return-wide v0
.end method

.method public setCX(J)V
    .locals 1
    .param p1, "cx"    # J

    .prologue
    .line 17
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/OneCellAnchor;->cx:J

    .line 18
    return-void
.end method

.method public setCY(J)V
    .locals 1
    .param p1, "cy"    # J

    .prologue
    .line 25
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/OneCellAnchor;->cy:J

    .line 26
    return-void
.end method
