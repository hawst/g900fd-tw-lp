.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/LineSpacingHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "LineSpacingHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/LineSpacingHandler$ILineSpacingConsumer;
    }
.end annotation


# instance fields
.field private spacingConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/LineSpacingHandler$ILineSpacingConsumer;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/LineSpacingHandler$ILineSpacingConsumer;)V
    .locals 2
    .param p1, "spacingConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/LineSpacingHandler$ILineSpacingConsumer;

    .prologue
    .line 20
    const/16 v0, 0x1f

    const-string/jumbo v1, "lnSpc"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 21
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/LineSpacingHandler;->spacingConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/LineSpacingHandler$ILineSpacingConsumer;

    .line 22
    return-void
.end method


# virtual methods
.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 27
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 29
    invoke-virtual {p0, p2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/LineSpacingHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "spcPct"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 31
    const-string/jumbo v2, "val"

    invoke-virtual {p0, p3, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/LineSpacingHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 32
    .local v1, "val":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 33
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    .line 34
    .local v0, "spacing":F
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/LineSpacingHandler;->spacingConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/LineSpacingHandler$ILineSpacingConsumer;

    const v3, 0x47c35000    # 100000.0f

    div-float v3, v0, v3

    invoke-interface {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/LineSpacingHandler$ILineSpacingConsumer;->consumeLineSpacing(F)V

    .line 37
    .end local v0    # "spacing":F
    .end local v1    # "val":Ljava/lang/String;
    :cond_0
    return-void
.end method
