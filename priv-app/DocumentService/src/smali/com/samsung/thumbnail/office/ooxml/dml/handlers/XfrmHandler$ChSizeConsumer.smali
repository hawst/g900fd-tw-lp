.class Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$ChSizeConsumer;
.super Ljava/lang/Object;
.source "XfrmHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Size2DHandler$ISizeConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ChSizeConsumer"
.end annotation


# instance fields
.field shape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 0
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$ChSizeConsumer;->shape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 137
    return-void
.end method


# virtual methods
.method public setSize(JJ)V
    .locals 1
    .param p1, "w"    # J
    .param p3, "h"    # J

    .prologue
    .line 141
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$ChSizeConsumer;->shape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    if-nez v0, :cond_0

    .line 145
    :goto_0
    return-void

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$ChSizeConsumer;->shape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setChildSize(JJ)V

    goto :goto_0
.end method
