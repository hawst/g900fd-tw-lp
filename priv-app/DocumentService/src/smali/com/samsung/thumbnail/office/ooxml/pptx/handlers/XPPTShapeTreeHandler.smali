.class public Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTShapeTreeHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XPPTShapeTreeHandler.java"


# instance fields
.field ID:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "ID"    # I

    .prologue
    .line 18
    const-string/jumbo v0, "spTree"

    invoke-direct {p0, p1, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 19
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTShapeTreeHandler;->ID:I

    .line 20
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 57
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method protected init(I)V
    .locals 7
    .param p1, "ID"    # I

    .prologue
    .line 23
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;

    const-string/jumbo v5, "sp"

    invoke-direct {v4, p1, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;-><init>(ILjava/lang/String;)V

    .line 25
    .local v4, "shapeHandler":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTShapeTreeHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v6, "sp"

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTPictureHandler;

    invoke-direct {v3, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTPictureHandler;-><init>(I)V

    .line 28
    .local v3, "picHandler":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTPictureHandler;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTShapeTreeHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v6, "pic"

    invoke-virtual {v5, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicFrameHandler;

    invoke-direct {v1, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicFrameHandler;-><init>(I)V

    .line 31
    .local v1, "graphicFrame":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicFrameHandler;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTShapeTreeHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v6, "graphicFrame"

    invoke-virtual {v5, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;

    const-string/jumbo v5, "cxnSp"

    invoke-direct {v0, p1, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;-><init>(ILjava/lang/String;)V

    .line 35
    .local v0, "cxnShapeHandler":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTShapeTreeHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v6, "cxnSp"

    invoke-virtual {v5, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/GroupShapeHandler;

    invoke-direct {v2, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/GroupShapeHandler;-><init>(I)V

    .line 38
    .local v2, "groupShapeHandler":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/GroupShapeHandler;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTShapeTreeHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v6, "grpSp"

    invoke-virtual {v5, v6, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 51
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 52
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTShapeTreeHandler;->ID:I

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTShapeTreeHandler;->init(I)V

    .line 53
    return-void
.end method
