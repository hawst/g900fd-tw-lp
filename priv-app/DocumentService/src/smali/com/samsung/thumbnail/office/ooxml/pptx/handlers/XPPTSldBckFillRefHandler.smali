.class public Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillRefHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XPPTSldBckFillRefHandler.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "XPPTSldBckFillRefHandler"


# instance fields
.field background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;)V
    .locals 1
    .param p1, "background"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    .prologue
    .line 18
    const-string/jumbo v0, "bgRef"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 19
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillRefHandler;->background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    .line 20
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillRefHandler;->init()V

    .line 21
    return-void
.end method

.method private init()V
    .locals 3

    .prologue
    .line 24
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/SchemeClrHandler;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillRefHandler;->background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/SchemeClrHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;)V

    .line 26
    .local v0, "schemeClrHandler":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/SchemeClrHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillRefHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "a:schemeClr"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 32
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 33
    const-string/jumbo v2, "idx"

    invoke-virtual {p0, p3, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillRefHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 35
    .local v0, "bgFillRefNum":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 36
    :try_start_0
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillRefHandler;->background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->setBgFillRefNumber(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    :cond_0
    :goto_0
    return-void

    .line 39
    :catch_0
    move-exception v1

    .line 40
    .local v1, "e":Ljava/lang/NumberFormatException;
    const-string/jumbo v2, "XPPTSldBckFillRefHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
