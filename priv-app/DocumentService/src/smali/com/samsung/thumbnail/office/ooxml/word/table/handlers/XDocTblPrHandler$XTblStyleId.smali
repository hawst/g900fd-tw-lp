.class public Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler$XTblStyleId;
.super Ljava/lang/Object;
.source "XDocTblPrHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "XTblStyleId"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler$XTblStyleId;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V
    .locals 3
    .param p1, "value"    # Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;

    .prologue
    .line 63
    instance-of v1, p1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 64
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    .line 65
    .local v0, "styleId":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler$XTblStyleId;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;

    # getter for: Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;->tableProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;
    invoke-static {v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;->access$000(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;)Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->setStyle(Ljava/lang/String;)V

    .line 67
    .end local v0    # "styleId":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    :cond_0
    return-void
.end method
