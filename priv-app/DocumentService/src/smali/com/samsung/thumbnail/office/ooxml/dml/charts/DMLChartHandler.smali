.class public Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DMLChartHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    .line 17
    const/16 v3, 0x23

    const-string/jumbo v4, "chartSpace"

    invoke-direct {p0, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 19
    const/4 v3, 0x1

    new-array v2, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 21
    .local v2, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartElementsHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartElementsHandler;-><init>()V

    .line 22
    .local v0, "chartElementsHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartElementsHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v3, "chart"

    invoke-direct {v1, v3, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 24
    .local v1, "chartElementsSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/4 v3, 0x0

    aput-object v1, v2, v3

    .line 26
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 27
    return-void
.end method
