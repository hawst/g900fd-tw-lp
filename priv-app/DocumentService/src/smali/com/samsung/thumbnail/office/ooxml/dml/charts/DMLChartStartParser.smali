.class public Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStartParser;
.super Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;
.source "DMLChartStartParser.java"


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)V
    .locals 0
    .param p1, "chartobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)V

    .line 22
    return-void
.end method


# virtual methods
.method public createRootElementHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartHandler;-><init>()V

    return-object v0
.end method

.method public getMainNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .locals 1

    .prologue
    .line 31
    const/16 v0, 0x23

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStartParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v0

    return-object v0
.end method
