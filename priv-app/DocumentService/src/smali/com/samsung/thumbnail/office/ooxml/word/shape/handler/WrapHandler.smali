.class public Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/WrapHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "WrapHandler.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "elementName"    # Ljava/lang/String;

    .prologue
    .line 14
    const/16 v0, 0x29

    invoke-direct {p0, v0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 15
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 20
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 22
    const-string/jumbo v2, "w10:wrap"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 23
    const-string/jumbo v2, "anchorx"

    invoke-virtual {p0, p3, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/WrapHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 24
    .local v0, "xAnchor":Ljava/lang/String;
    const-string/jumbo v2, "anchory"

    invoke-virtual {p0, p3, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/WrapHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 25
    .local v1, "yAnchor":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v2, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->addGroupShapeAnchor(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .end local v0    # "xAnchor":Ljava/lang/String;
    .end local v1    # "yAnchor":Ljava/lang/String;
    :cond_0
    return-void
.end method
