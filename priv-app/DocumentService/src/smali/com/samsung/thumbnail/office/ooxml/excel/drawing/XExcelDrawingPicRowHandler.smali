.class public Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedTagNoChildrenHandler;
.source "XExcelDrawingPicRowHandler.java"


# instance fields
.field private cellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

.field private fromTo:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;)V
    .locals 1
    .param p1, "fromTo"    # Ljava/lang/String;
    .param p2, "cellAnchor"    # Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    .prologue
    .line 21
    const-string/jumbo v0, "row"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedTagNoChildrenHandler;-><init>(Ljava/lang/String;)V

    .line 22
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowHandler;->fromTo:Ljava/lang/String;

    .line 23
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowHandler;->cellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    .line 24
    return-void
.end method


# virtual methods
.method public charactersHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;[CII)V
    .locals 8
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "content"    # [C
    .param p3, "start"    # I
    .param p4, "end"    # I

    .prologue
    .line 29
    invoke-super {p0, p1, p2, p3, p4}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedTagNoChildrenHandler;->charactersHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;[CII)V

    .line 30
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, p2, p3, p4}, Ljava/lang/String;-><init>([CII)V

    .line 32
    .local v3, "row":Ljava/lang/String;
    const/4 v4, 0x0

    .line 34
    .local v4, "startNum":I
    :try_start_0
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 40
    :goto_0
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowHandler;->fromTo:Ljava/lang/String;

    const-string/jumbo v6, "from"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 42
    const/16 v5, 0x14

    if-gt v4, v5, :cond_1

    .line 43
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowHandler;->cellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    invoke-virtual {v5, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->setFromRow(I)V

    .line 58
    :cond_0
    :goto_1
    return-void

    .line 35
    :catch_0
    move-exception v0

    .line 36
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string/jumbo v5, "XExcelDrawingPicRowHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "NumberFormatException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 45
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :cond_1
    const/4 v1, 0x0

    .line 46
    .local v1, "found":Z
    :cond_2
    :goto_2
    if-nez v1, :cond_0

    .line 47
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->popHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    move-result-object v2

    .line 48
    .local v2, "handler":Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    if-eqz v2, :cond_3

    instance-of v5, v2, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingTwoCellAnchorHandler;

    if-nez v5, :cond_3

    instance-of v5, v2, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingOneCellAnchorHandler;

    if-eqz v5, :cond_2

    .line 51
    :cond_3
    const/4 v1, 0x1

    goto :goto_2

    .line 55
    .end local v1    # "found":Z
    .end local v2    # "handler":Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    :cond_4
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowHandler;->cellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    instance-of v5, v5, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;

    if-eqz v5, :cond_0

    .line 56
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowHandler;->cellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    check-cast v5, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;

    invoke-virtual {v5, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;->setToRow(I)V

    goto :goto_1
.end method
