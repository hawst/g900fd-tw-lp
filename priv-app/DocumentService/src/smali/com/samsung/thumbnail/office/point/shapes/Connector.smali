.class public Lcom/samsung/thumbnail/office/point/shapes/Connector;
.super Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;
.source "Connector.java"


# instance fields
.field private TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "folderPath"    # Ljava/io/File;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;-><init>()V

    .line 22
    const-string/jumbo v0, "BentConnector"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->TAG:Ljava/lang/String;

    .line 25
    iput-object p1, p0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->folderPath:Ljava/io/File;

    .line 26
    return-void
.end method


# virtual methods
.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V
    .locals 22
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 30
    invoke-super/range {p0 .. p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 32
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hslf/model/AutoShape;->getAdjustmentValue(I)I

    move-result v16

    .line 34
    .local v16, "adjustValue":I
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 35
    .local v7, "paint":Landroid/graphics/Paint;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v4}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v6

    invoke-virtual {v7, v3, v4, v5, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 39
    new-instance v2, Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmap:Landroid/graphics/Bitmap;

    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 41
    .local v2, "canvas":Landroid/graphics/Canvas;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getShapeName()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "BentConnector2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 42
    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    add-int/lit8 v5, v5, -0x1

    int-to-float v5, v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 43
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    add-int/lit8 v5, v5, -0x1

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapHight:I

    add-int/lit8 v6, v6, -0x1

    int-to-float v6, v6

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 72
    :cond_0
    :goto_0
    new-instance v14, Landroid/graphics/Matrix;

    invoke-direct {v14}, Landroid/graphics/Matrix;-><init>()V

    .line 75
    .local v14, "mtx":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->rotation:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->width:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->height:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    invoke-virtual {v14, v3, v4, v5}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 77
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmap:Landroid/graphics/Bitmap;

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapHight:I

    const/4 v15, 0x1

    invoke-static/range {v9 .. v15}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 82
    .local v21, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v20, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->folderPath:Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "pic_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->shapeName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ".png"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 86
    .local v20, "out":Ljava/io/FileOutputStream;
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 87
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    .end local v20    # "out":Ljava/io/FileOutputStream;
    :goto_1
    return-void

    .line 46
    .end local v14    # "mtx":Landroid/graphics/Matrix;
    .end local v21    # "rotatedBMP":Landroid/graphics/Bitmap;
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getShapeName()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "BentConnector3"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 47
    if-nez v16, :cond_2

    .line 48
    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 50
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapHight:I

    add-int/lit8 v6, v6, -0x1

    int-to-float v6, v6

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 52
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapHight:I

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapHight:I

    add-int/lit8 v6, v6, -0x1

    int-to-float v6, v6

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 55
    :cond_2
    const v3, 0x46a8c000    # 21600.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    int-to-float v4, v4

    div-float v17, v3, v4

    .line 56
    .local v17, "dividevalue":F
    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    move/from16 v0, v16

    int-to-float v5, v0

    div-float v5, v5, v17

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 57
    move/from16 v0, v16

    int-to-float v3, v0

    div-float v3, v3, v17

    const/high16 v4, 0x3f800000    # 1.0f

    move/from16 v0, v16

    int-to-float v5, v0

    div-float v5, v5, v17

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapHight:I

    add-int/lit8 v6, v6, -0x1

    int-to-float v6, v6

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 59
    move/from16 v0, v16

    int-to-float v3, v0

    div-float v3, v3, v17

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapHight:I

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapHight:I

    add-int/lit8 v6, v6, -0x1

    int-to-float v6, v6

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 62
    .end local v17    # "dividevalue":F
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getShapeName()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "CurvedConnector3"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 64
    const v3, 0x46a8c000    # 21600.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    int-to-float v4, v4

    div-float v18, v3, v4

    .line 65
    .local v18, "dividevalue_width":F
    new-instance v8, Landroid/graphics/Path;

    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    .line 66
    .local v8, "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v8, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 67
    move/from16 v0, v16

    int-to-float v3, v0

    div-float v9, v3, v18

    const/4 v10, 0x0

    move/from16 v0, v16

    int-to-float v3, v0

    div-float v11, v3, v18

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapHight:I

    int-to-float v12, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    int-to-float v13, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapHight:I

    int-to-float v14, v3

    invoke-virtual/range {v8 .. v14}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 69
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->paintLine:Landroid/graphics/Paint;

    invoke-virtual {v2, v8, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 88
    .end local v8    # "path":Landroid/graphics/Path;
    .end local v18    # "dividevalue_width":F
    .restart local v14    # "mtx":Landroid/graphics/Matrix;
    .restart local v21    # "rotatedBMP":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v19

    .line 89
    .local v19, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception while writing pictures to out file"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V
    .locals 17
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "marginleft"    # I
    .param p5, "marginRight"    # I

    .prologue
    .line 95
    invoke-super/range {p0 .. p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 96
    move/from16 v0, p4

    int-to-float v1, v0

    move/from16 v0, p5

    int-to-float v2, v0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 97
    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/model/AutoShape;->getAdjustmentValue(I)I

    move-result v14

    .line 99
    .local v14, "adjustValue":I
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 100
    .local v6, "paint":Landroid/graphics/Paint;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v1}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v2}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v4}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v4

    invoke-virtual {v6, v1, v2, v3, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 104
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getShapeName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "BentConnector2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 105
    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v4, v1

    const/high16 v5, 0x3f800000    # 1.0f

    move-object/from16 v1, p3

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 106
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v2, v1

    const/high16 v3, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v4, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapHight:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v5, v1

    move-object/from16 v1, p3

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 134
    :cond_0
    :goto_0
    move/from16 v0, p4

    neg-int v1, v0

    int-to-float v1, v1

    move/from16 v0, p5

    neg-int v2, v0

    int-to-float v2, v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 135
    return-void

    .line 109
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getShapeName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "BentConnector3"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 110
    if-nez v14, :cond_2

    .line 111
    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    int-to-float v1, v1

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v1, v4

    const/high16 v5, 0x3f800000    # 1.0f

    move-object/from16 v1, p3

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 113
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v1, v2

    const/high16 v3, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    int-to-float v1, v1

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v1, v4

    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapHight:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v5, v1

    move-object/from16 v1, p3

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 115
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v1, v2

    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapHight:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v3, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    int-to-float v4, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapHight:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v5, v1

    move-object/from16 v1, p3

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 118
    :cond_2
    const v1, 0x46a8c000    # 21600.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    int-to-float v2, v2

    div-float v15, v1, v2

    .line 119
    .local v15, "dividevalue":F
    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    int-to-float v1, v14

    div-float v4, v1, v15

    const/high16 v5, 0x3f800000    # 1.0f

    move-object/from16 v1, p3

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 120
    int-to-float v1, v14

    div-float v2, v1, v15

    const/high16 v3, 0x3f800000    # 1.0f

    int-to-float v1, v14

    div-float v4, v1, v15

    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapHight:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v5, v1

    move-object/from16 v1, p3

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 122
    int-to-float v1, v14

    div-float v2, v1, v15

    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapHight:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v3, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    int-to-float v4, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapHight:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v5, v1

    move-object/from16 v1, p3

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 125
    .end local v15    # "dividevalue":F
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getShapeName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "CurvedConnector3"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 127
    const v1, 0x46a8c000    # 21600.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    int-to-float v2, v2

    div-float v16, v1, v2

    .line 128
    .local v16, "dividevalue_width":F
    new-instance v7, Landroid/graphics/Path;

    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    .line 129
    .local v7, "path":Landroid/graphics/Path;
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v7, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 130
    int-to-float v1, v14

    div-float v8, v1, v16

    const/4 v9, 0x0

    int-to-float v1, v14

    div-float v10, v1, v16

    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapHight:I

    int-to-float v11, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapWidth:I

    int-to-float v12, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->bitmapHight:I

    int-to-float v13, v1

    invoke-virtual/range {v7 .. v13}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 132
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/Connector;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p3

    invoke-virtual {v0, v7, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_0
.end method
