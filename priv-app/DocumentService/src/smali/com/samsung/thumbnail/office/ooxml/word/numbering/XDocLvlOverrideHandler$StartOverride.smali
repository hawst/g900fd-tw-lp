.class Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler$StartOverride;
.super Ljava/lang/Object;
.source "XDocLvlOverrideHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StartOverride"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;


# direct methods
.method private constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler$StartOverride;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;
    .param p2, "x1"    # Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler$1;

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler$StartOverride;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;)V

    return-void
.end method


# virtual methods
.method public consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V
    .locals 3
    .param p1, "value"    # Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;

    .prologue
    .line 72
    instance-of v1, p1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 73
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;

    .line 74
    .local v0, "startOverride":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler$StartOverride;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;

    # getter for: Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;->lvlOverride:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;
    invoke-static {v1}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;->access$100(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;->getValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;->setStartOverride(I)V

    .line 76
    .end local v0    # "startOverride":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;
    :cond_0
    return-void
.end method
