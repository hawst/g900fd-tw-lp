.class public Lcom/samsung/thumbnail/office/point/shapes/Callout1;
.super Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;
.source "Callout1.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mDrawOnCanvas:Z

.field private mLeftMarg:I

.field private mTopMarg:I

.field public newBitmapHeight:I

.field public newBitmapWidth:I

.field public subFactor_Height:I

.field public subFactor_Width:I


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 2
    .param p1, "folderPath"    # Ljava/io/File;

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;-><init>()V

    .line 29
    const-string/jumbo v0, "Callout1"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->TAG:Ljava/lang/String;

    .line 30
    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    .line 31
    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    .line 32
    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->newBitmapHeight:I

    .line 33
    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->newBitmapWidth:I

    .line 34
    iput-boolean v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mDrawOnCanvas:Z

    .line 39
    iput-object p1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->folderPath:Ljava/io/File;

    .line 40
    return-void
.end method


# virtual methods
.method protected draw(I)V
    .locals 24
    .param p1, "shapeCount"    # I

    .prologue
    .line 161
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->shapeName:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 391
    :cond_0
    :goto_0
    return-void

    .line 164
    :cond_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->width:F

    const v4, 0x46a8c000    # 21600.0f

    div-float v20, v3, v4

    .line 165
    .local v20, "mulFactor_Width":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->height:F

    const v4, 0x46a8c000    # 21600.0f

    div-float v19, v3, v4

    .line 180
    .local v19, "mulFactor_Height":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->adjval0:I

    if-nez v3, :cond_2

    .line 181
    const/16 v3, -0x2058

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->adjval0:I

    .line 182
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->adjval1:I

    if-nez v3, :cond_3

    .line 183
    const/16 v3, 0x5eec

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->adjval1:I

    .line 184
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->adjval2:I

    if-nez v3, :cond_4

    .line 185
    const/16 v3, -0x708

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->adjval2:I

    .line 186
    :cond_4
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->adjval3:I

    if-nez v3, :cond_5

    .line 187
    const/16 v3, 0xfd2

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->adjval3:I

    .line 189
    :cond_5
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->adjval0:I

    .line 190
    .local v9, "calloutadjval0":I
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->adjval1:I

    .line 191
    .local v10, "calloutadjval1":I
    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->adjval2:I

    .line 192
    .local v11, "calloutadjval2":I
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->adjval3:I

    .line 196
    .local v12, "calloutadjval3":I
    invoke-static {v9, v11}, Ljava/lang/Math;->min(II)I

    move-result v17

    .line 197
    .local v17, "minX":I
    const/4 v3, 0x0

    move/from16 v0, v17

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v17

    .line 198
    invoke-static {v9, v11}, Ljava/lang/Math;->max(II)I

    move-result v15

    .line 199
    .local v15, "maxX":I
    const/16 v3, 0x5460

    invoke-static {v15, v3}, Ljava/lang/Math;->max(II)I

    move-result v15

    .line 200
    move/from16 v0, v17

    int-to-float v3, v0

    mul-float v3, v3, v20

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    .line 201
    int-to-float v3, v15

    mul-float v3, v3, v20

    move/from16 v0, v17

    int-to-float v4, v0

    mul-float v4, v4, v20

    sub-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->newBitmapWidth:I

    .line 205
    invoke-static {v10, v12}, Ljava/lang/Math;->min(II)I

    move-result v18

    .line 206
    .local v18, "minY":I
    const/4 v3, 0x0

    move/from16 v0, v18

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v18

    .line 207
    invoke-static {v10, v12}, Ljava/lang/Math;->max(II)I

    move-result v16

    .line 208
    .local v16, "maxY":I
    const/16 v3, 0x5460

    move/from16 v0, v16

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v16

    .line 209
    move/from16 v0, v18

    int-to-float v3, v0

    mul-float v3, v3, v19

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    .line 210
    move/from16 v0, v16

    int-to-float v3, v0

    mul-float v3, v3, v19

    move/from16 v0, v18

    int-to-float v4, v0

    mul-float v4, v4, v19

    sub-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->newBitmapHeight:I

    .line 212
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->newBitmapWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->newBitmapHeight:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 217
    .local v2, "_bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "Callout1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 218
    new-instance v13, Landroid/graphics/Canvas;

    invoke-direct {v13, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 220
    .local v13, "canvas":Landroid/graphics/Canvas;
    new-instance v22, Landroid/graphics/Path;

    invoke-direct/range {v22 .. v22}, Landroid/graphics/Path;-><init>()V

    .line 222
    .local v22, "path":Landroid/graphics/Path;
    int-to-float v3, v9

    mul-float v3, v3, v20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v10

    mul-float v4, v4, v19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 224
    int-to-float v3, v11

    mul-float v3, v3, v20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v12

    mul-float v4, v4, v19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 226
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_6

    .line 227
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v22

    invoke-virtual {v13, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 229
    :cond_6
    const/4 v3, 0x0

    mul-float v3, v3, v20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 231
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 233
    const/4 v3, 0x0

    mul-float v3, v3, v20

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v19

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 234
    const/4 v3, 0x0

    mul-float v3, v3, v20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 236
    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Path;->close()V

    .line 238
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_7

    .line 239
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, v22

    invoke-virtual {v13, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 243
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x461e3400    # 10125.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 244
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 245
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x46d2f000    # 27000.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 246
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    .line 358
    .end local v13    # "canvas":Landroid/graphics/Canvas;
    .end local v22    # "path":Landroid/graphics/Path;
    :cond_8
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mDrawOnCanvas:Z

    if-nez v3, :cond_0

    .line 359
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 361
    .local v7, "mtx":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->flipHorizontal:Z

    if-eqz v3, :cond_15

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->flipVertical:Z

    if-nez v3, :cond_15

    .line 362
    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 371
    :cond_9
    :goto_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->rotation:I

    int-to-float v3, v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    invoke-virtual {v7, v3, v4, v5}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 374
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v23

    .line 378
    .local v23, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v21, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->folderPath:Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "pic_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->shapeName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ".png"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 384
    .local v21, "out":Ljava/io/FileOutputStream;
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 385
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 386
    .end local v21    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v14

    .line 387
    .local v14, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception while writing pictures to out file"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 249
    .end local v7    # "mtx":Landroid/graphics/Matrix;
    .end local v14    # "e":Ljava/lang/Exception;
    .end local v23    # "rotatedBMP":Landroid/graphics/Bitmap;
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "BorderCallout1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 250
    new-instance v13, Landroid/graphics/Canvas;

    invoke-direct {v13, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 252
    .restart local v13    # "canvas":Landroid/graphics/Canvas;
    new-instance v22, Landroid/graphics/Path;

    invoke-direct/range {v22 .. v22}, Landroid/graphics/Path;-><init>()V

    .line 254
    .restart local v22    # "path":Landroid/graphics/Path;
    int-to-float v3, v9

    mul-float v3, v3, v20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v10

    mul-float v4, v4, v19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 256
    int-to-float v3, v11

    mul-float v3, v3, v20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v12

    mul-float v4, v4, v19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 258
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_b

    .line 259
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v22

    invoke-virtual {v13, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 261
    :cond_b
    const/4 v3, 0x0

    mul-float v3, v3, v20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 263
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 265
    const/4 v3, 0x0

    mul-float v3, v3, v20

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v19

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 266
    const/4 v3, 0x0

    mul-float v3, v3, v20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 268
    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Path;->close()V

    .line 270
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_c

    .line 271
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, v22

    invoke-virtual {v13, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 272
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_d

    .line 273
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v22

    invoke-virtual {v13, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 277
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x461e3400    # 10125.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 278
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 279
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x46d2f000    # 27000.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 280
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 283
    .end local v13    # "canvas":Landroid/graphics/Canvas;
    .end local v22    # "path":Landroid/graphics/Path;
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "AccentCallout1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 284
    new-instance v13, Landroid/graphics/Canvas;

    invoke-direct {v13, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 286
    .restart local v13    # "canvas":Landroid/graphics/Canvas;
    new-instance v22, Landroid/graphics/Path;

    invoke-direct/range {v22 .. v22}, Landroid/graphics/Path;-><init>()V

    .line 288
    .restart local v22    # "path":Landroid/graphics/Path;
    int-to-float v3, v9

    mul-float v3, v3, v20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v10

    mul-float v4, v4, v19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 290
    int-to-float v3, v11

    mul-float v3, v3, v20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v12

    mul-float v4, v4, v19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 292
    int-to-float v3, v11

    mul-float v3, v3, v20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 294
    int-to-float v3, v11

    mul-float v3, v3, v20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 296
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_f

    .line 297
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v22

    invoke-virtual {v13, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 299
    :cond_f
    const/4 v3, 0x0

    mul-float v3, v3, v20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 301
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 303
    const/4 v3, 0x0

    mul-float v3, v3, v20

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v19

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 304
    const/4 v3, 0x0

    mul-float v3, v3, v20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 306
    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Path;->close()V

    .line 308
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_10

    .line 309
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, v22

    invoke-virtual {v13, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 313
    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x461e3400    # 10125.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 314
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 315
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x46d2f000    # 27000.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 316
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 319
    .end local v13    # "canvas":Landroid/graphics/Canvas;
    .end local v22    # "path":Landroid/graphics/Path;
    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "AccentBorderCallout1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 320
    new-instance v13, Landroid/graphics/Canvas;

    invoke-direct {v13, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 322
    .restart local v13    # "canvas":Landroid/graphics/Canvas;
    new-instance v22, Landroid/graphics/Path;

    invoke-direct/range {v22 .. v22}, Landroid/graphics/Path;-><init>()V

    .line 324
    .restart local v22    # "path":Landroid/graphics/Path;
    int-to-float v3, v9

    mul-float v3, v3, v20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v10

    mul-float v4, v4, v19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 326
    int-to-float v3, v11

    mul-float v3, v3, v20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v12

    mul-float v4, v4, v19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 328
    int-to-float v3, v11

    mul-float v3, v3, v20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 330
    int-to-float v3, v11

    mul-float v3, v3, v20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 332
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_12

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_12

    .line 333
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v22

    invoke-virtual {v13, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 335
    :cond_12
    const/4 v3, 0x0

    mul-float v3, v3, v20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 337
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 339
    const/4 v3, 0x0

    mul-float v3, v3, v20

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v19

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 340
    const/4 v3, 0x0

    mul-float v3, v3, v20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 342
    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Path;->close()V

    .line 344
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_13

    .line 345
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, v22

    invoke-virtual {v13, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 346
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_14

    .line 347
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v22

    invoke-virtual {v13, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 351
    :cond_14
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x461e3400    # 10125.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 352
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 353
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x46d2f000    # 27000.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 354
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 364
    .end local v13    # "canvas":Landroid/graphics/Canvas;
    .end local v22    # "path":Landroid/graphics/Path;
    .restart local v7    # "mtx":Landroid/graphics/Matrix;
    :cond_15
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->flipVertical:Z

    if-eqz v3, :cond_16

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->flipHorizontal:Z

    if-nez v3, :cond_16

    .line 365
    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, -0x40800000    # -1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    goto/16 :goto_2

    .line 367
    :cond_16
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->flipHorizontal:Z

    if-eqz v3, :cond_9

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->flipVertical:Z

    if-eqz v3, :cond_9

    .line 368
    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v4, -0x40800000    # -1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    goto/16 :goto_2
.end method

.method protected draw(Landroid/graphics/Canvas;I)V
    .locals 23
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeCount"    # I

    .prologue
    .line 395
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->shapeName:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 625
    :cond_0
    :goto_0
    return-void

    .line 398
    :cond_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->width:F

    const v4, 0x46a8c000    # 21600.0f

    div-float v19, v3, v4

    .line 399
    .local v19, "mulFactor_Width":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->height:F

    const v4, 0x46a8c000    # 21600.0f

    div-float v18, v3, v4

    .line 414
    .local v18, "mulFactor_Height":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->adjval0:I

    if-nez v3, :cond_2

    .line 415
    const/16 v3, -0x2058

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->adjval0:I

    .line 416
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->adjval1:I

    if-nez v3, :cond_3

    .line 417
    const/16 v3, 0x5eec

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->adjval1:I

    .line 418
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->adjval2:I

    if-nez v3, :cond_4

    .line 419
    const/16 v3, -0x708

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->adjval2:I

    .line 420
    :cond_4
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->adjval3:I

    if-nez v3, :cond_5

    .line 421
    const/16 v3, 0xfd2

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->adjval3:I

    .line 423
    :cond_5
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->adjval0:I

    .line 424
    .local v9, "calloutadjval0":I
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->adjval1:I

    .line 425
    .local v10, "calloutadjval1":I
    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->adjval2:I

    .line 426
    .local v11, "calloutadjval2":I
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->adjval3:I

    .line 430
    .local v12, "calloutadjval3":I
    invoke-static {v9, v11}, Ljava/lang/Math;->min(II)I

    move-result v16

    .line 431
    .local v16, "minX":I
    const/4 v3, 0x0

    move/from16 v0, v16

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v16

    .line 432
    invoke-static {v9, v11}, Ljava/lang/Math;->max(II)I

    move-result v14

    .line 433
    .local v14, "maxX":I
    const/16 v3, 0x5460

    invoke-static {v14, v3}, Ljava/lang/Math;->max(II)I

    move-result v14

    .line 434
    move/from16 v0, v16

    int-to-float v3, v0

    mul-float v3, v3, v19

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    .line 435
    int-to-float v3, v14

    mul-float v3, v3, v19

    move/from16 v0, v16

    int-to-float v4, v0

    mul-float v4, v4, v19

    sub-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->newBitmapWidth:I

    .line 439
    invoke-static {v10, v12}, Ljava/lang/Math;->min(II)I

    move-result v17

    .line 440
    .local v17, "minY":I
    const/4 v3, 0x0

    move/from16 v0, v17

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v17

    .line 441
    invoke-static {v10, v12}, Ljava/lang/Math;->max(II)I

    move-result v15

    .line 442
    .local v15, "maxY":I
    const/16 v3, 0x5460

    invoke-static {v15, v3}, Ljava/lang/Math;->max(II)I

    move-result v15

    .line 443
    move/from16 v0, v17

    int-to-float v3, v0

    mul-float v3, v3, v18

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    .line 444
    int-to-float v3, v15

    mul-float v3, v3, v18

    move/from16 v0, v17

    int-to-float v4, v0

    mul-float v4, v4, v18

    sub-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->newBitmapHeight:I

    .line 446
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->newBitmapWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->newBitmapHeight:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 451
    .local v2, "_bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "Callout1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 454
    new-instance v21, Landroid/graphics/Path;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/Path;-><init>()V

    .line 456
    .local v21, "path":Landroid/graphics/Path;
    int-to-float v3, v9

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v10

    mul-float v4, v4, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 458
    int-to-float v3, v11

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v12

    mul-float v4, v4, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 460
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_6

    .line 461
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 463
    :cond_6
    const/4 v3, 0x0

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 465
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 467
    const/4 v3, 0x0

    mul-float v3, v3, v19

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v18

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 468
    const/4 v3, 0x0

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 470
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Path;->close()V

    .line 472
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_7

    .line 473
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 477
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x461e3400    # 10125.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 478
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 479
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x46d2f000    # 27000.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 480
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    .line 592
    .end local v21    # "path":Landroid/graphics/Path;
    :cond_8
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mDrawOnCanvas:Z

    if-nez v3, :cond_0

    .line 593
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 595
    .local v7, "mtx":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->flipHorizontal:Z

    if-eqz v3, :cond_15

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->flipVertical:Z

    if-nez v3, :cond_15

    .line 596
    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 605
    :cond_9
    :goto_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->rotation:I

    int-to-float v3, v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    invoke-virtual {v7, v3, v4, v5}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 608
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v22

    .line 612
    .local v22, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v20, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->folderPath:Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "pic_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->shapeName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ".png"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 618
    .local v20, "out":Ljava/io/FileOutputStream;
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 619
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 620
    .end local v20    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v13

    .line 621
    .local v13, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception while writing pictures to out file"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 483
    .end local v7    # "mtx":Landroid/graphics/Matrix;
    .end local v13    # "e":Ljava/lang/Exception;
    .end local v22    # "rotatedBMP":Landroid/graphics/Bitmap;
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "BorderCallout1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 486
    new-instance v21, Landroid/graphics/Path;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/Path;-><init>()V

    .line 488
    .restart local v21    # "path":Landroid/graphics/Path;
    int-to-float v3, v9

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v10

    mul-float v4, v4, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 490
    int-to-float v3, v11

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v12

    mul-float v4, v4, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 492
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_b

    .line 493
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 495
    :cond_b
    const/4 v3, 0x0

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 497
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 499
    const/4 v3, 0x0

    mul-float v3, v3, v19

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v18

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 500
    const/4 v3, 0x0

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 502
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Path;->close()V

    .line 504
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_c

    .line 505
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 506
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_d

    .line 507
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 511
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x461e3400    # 10125.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 512
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 513
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x46d2f000    # 27000.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 514
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 517
    .end local v21    # "path":Landroid/graphics/Path;
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "AccentCallout1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 520
    new-instance v21, Landroid/graphics/Path;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/Path;-><init>()V

    .line 522
    .restart local v21    # "path":Landroid/graphics/Path;
    int-to-float v3, v9

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v10

    mul-float v4, v4, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 524
    int-to-float v3, v11

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v12

    mul-float v4, v4, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 526
    int-to-float v3, v11

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 528
    int-to-float v3, v11

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 530
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_f

    .line 531
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 533
    :cond_f
    const/4 v3, 0x0

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 535
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 537
    const/4 v3, 0x0

    mul-float v3, v3, v19

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v18

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 538
    const/4 v3, 0x0

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 540
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Path;->close()V

    .line 542
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_10

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_10

    .line 543
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 547
    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x461e3400    # 10125.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 548
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 549
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x46d2f000    # 27000.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 550
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 553
    .end local v21    # "path":Landroid/graphics/Path;
    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "AccentBorderCallout1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 556
    new-instance v21, Landroid/graphics/Path;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/Path;-><init>()V

    .line 558
    .restart local v21    # "path":Landroid/graphics/Path;
    int-to-float v3, v9

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v10

    mul-float v4, v4, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 560
    int-to-float v3, v11

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v12

    mul-float v4, v4, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 562
    int-to-float v3, v11

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 564
    int-to-float v3, v11

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 566
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_12

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_12

    .line 567
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 569
    :cond_12
    const/4 v3, 0x0

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 571
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 573
    const/4 v3, 0x0

    mul-float v3, v3, v19

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v18

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 574
    const/4 v3, 0x0

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 576
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Path;->close()V

    .line 578
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_13

    .line 579
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 580
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_14

    .line 581
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 585
    :cond_14
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x461e3400    # 10125.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 586
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 587
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x46d2f000    # 27000.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 588
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 598
    .end local v21    # "path":Landroid/graphics/Path;
    .restart local v7    # "mtx":Landroid/graphics/Matrix;
    :cond_15
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->flipVertical:Z

    if-eqz v3, :cond_16

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->flipHorizontal:Z

    if-nez v3, :cond_16

    .line 599
    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, -0x40800000    # -1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    goto/16 :goto_2

    .line 601
    :cond_16
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->flipHorizontal:Z

    if-eqz v3, :cond_9

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->flipVertical:Z

    if-eqz v3, :cond_9

    .line 602
    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v4, -0x40800000    # -1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    goto/16 :goto_2
.end method

.method public drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F

    .prologue
    .line 115
    invoke-super {p0, p3, p4, p7, p8}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IFF)V

    .line 116
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mDrawOnCanvas:Z

    .line 117
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mLeftMarg:I

    .line 118
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mTopMarg:I

    .line 119
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 120
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->draw(Landroid/graphics/Canvas;I)V

    .line 121
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 122
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mDrawOnCanvas:Z

    .line 123
    return-void
.end method

.method public drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p2, "shapeCount"    # I

    .prologue
    .line 106
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V

    .line 108
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->draw(I)V

    .line 109
    return-void
.end method

.method public drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 92
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;I)V

    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mDrawOnCanvas:Z

    .line 94
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mLeftMarg:I

    .line 95
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mTopMarg:I

    .line 96
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->width:F

    .line 97
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->height:F

    .line 98
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 99
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->draw(Landroid/graphics/Canvas;I)V

    .line 100
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mDrawOnCanvas:Z

    .line 102
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V
    .locals 0
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 44
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 45
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->draw(I)V

    .line 46
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V
    .locals 2
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I

    .prologue
    .line 50
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mDrawOnCanvas:Z

    .line 52
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mLeftMarg:I

    .line 53
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mTopMarg:I

    .line 54
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 55
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->draw(Landroid/graphics/Canvas;I)V

    .line 56
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mDrawOnCanvas:Z

    .line 58
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 137
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 138
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mDrawOnCanvas:Z

    .line 139
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mLeftMarg:I

    .line 140
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mTopMarg:I

    .line 141
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 142
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->draw(Landroid/graphics/Canvas;I)V

    .line 143
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 144
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mDrawOnCanvas:Z

    .line 145
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 149
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V

    .line 150
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mDrawOnCanvas:Z

    .line 151
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mLeftMarg:I

    .line 152
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mTopMarg:I

    .line 153
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 154
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->draw(Landroid/graphics/Canvas;I)V

    .line 155
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 156
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mDrawOnCanvas:Z

    .line 157
    return-void
.end method

.method public drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .prologue
    .line 130
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 132
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->draw(I)V

    .line 133
    return-void
.end method

.method public drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 63
    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p7

    move v4, p8

    move-object v5, p9

    invoke-super/range {v0 .. v5}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSSExcelShape(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mDrawOnCanvas:Z

    .line 66
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mLeftMarg:I

    .line 67
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mTopMarg:I

    .line 68
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 69
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->draw(Landroid/graphics/Canvas;I)V

    .line 70
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mDrawOnCanvas:Z

    .line 72
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 76
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mDrawOnCanvas:Z

    .line 78
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mLeftMarg:I

    .line 79
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mTopMarg:I

    .line 80
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->width:F

    .line 81
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->height:F

    .line 82
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 83
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->draw(Landroid/graphics/Canvas;I)V

    .line 84
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 85
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->mDrawOnCanvas:Z

    .line 86
    return-void
.end method
