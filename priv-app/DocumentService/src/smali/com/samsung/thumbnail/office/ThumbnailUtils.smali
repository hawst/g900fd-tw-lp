.class public Lcom/samsung/thumbnail/office/ThumbnailUtils;
.super Ljava/lang/Object;
.source "ThumbnailUtils.java"


# static fields
.field private static final EXTENSION:Ljava/lang/String; = ".jpg"

.field private static final TAG:Ljava/lang/String; = "ThumbnailUtils"

.field public static THUMBNAIL_FOLDER:Ljava/lang/String;

.field public static highLightedColor:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static thumbnailFolder:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-string/jumbo v0, ".thumbnail"

    sput-object v0, Lcom/samsung/thumbnail/office/ThumbnailUtils;->THUMBNAIL_FOLDER:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getHighLightColor(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 94
    if-nez p0, :cond_0

    .line 95
    const/4 v0, 0x0

    .line 98
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/samsung/thumbnail/office/ThumbnailUtils;->highLightedColor:Ljava/util/HashMap;

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public static getPath(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 65
    .local v0, "path":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/samsung/thumbnail/office/ThumbnailUtils;->getThumbnailPath(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 67
    return-object v0
.end method

.method public static declared-synchronized getThumbnailPath(Landroid/content/Context;)Ljava/io/File;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    const-class v3, Lcom/samsung/thumbnail/office/ThumbnailUtils;

    monitor-enter v3

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 20
    .local v0, "externalDir":Ljava/lang/String;
    sget-object v2, Lcom/samsung/thumbnail/office/ThumbnailUtils;->thumbnailFolder:Ljava/io/File;

    if-nez v2, :cond_0

    .line 21
    new-instance v2, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/thumbnail/office/ThumbnailUtils;->THUMBNAIL_FOLDER:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v2, Lcom/samsung/thumbnail/office/ThumbnailUtils;->thumbnailFolder:Ljava/io/File;

    .line 23
    :cond_0
    sget-object v2, Lcom/samsung/thumbnail/office/ThumbnailUtils;->thumbnailFolder:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 24
    sget-object v2, Lcom/samsung/thumbnail/office/ThumbnailUtils;->thumbnailFolder:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    :goto_0
    monitor-exit v3

    return-object v2

    .line 27
    :cond_1
    :try_start_1
    sget-object v2, Lcom/samsung/thumbnail/office/ThumbnailUtils;->thumbnailFolder:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    move-result v1

    .line 28
    .local v1, "success":Z
    if-nez v1, :cond_2

    .line 29
    const-string/jumbo v2, "ThumbnailUtils"

    const-string/jumbo v4, "Thumbnail Folder creation failed / if the directory already existed."

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    :cond_2
    sget-object v2, Lcom/samsung/thumbnail/office/ThumbnailUtils;->thumbnailFolder:Ljava/io/File;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 18
    .end local v0    # "externalDir":Ljava/lang/String;
    .end local v1    # "success":Z
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static initHighLightColorMap()V
    .locals 3

    .prologue
    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/thumbnail/office/ThumbnailUtils;->highLightedColor:Ljava/util/HashMap;

    .line 75
    sget-object v0, Lcom/samsung/thumbnail/office/ThumbnailUtils;->highLightedColor:Ljava/util/HashMap;

    const-string/jumbo v1, "yellow"

    const-string/jumbo v2, "#ffd700"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lcom/samsung/thumbnail/office/ThumbnailUtils;->highLightedColor:Ljava/util/HashMap;

    const-string/jumbo v1, "green"

    const-string/jumbo v2, "#7cfc00"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lcom/samsung/thumbnail/office/ThumbnailUtils;->highLightedColor:Ljava/util/HashMap;

    const-string/jumbo v1, "cyan"

    const-string/jumbo v2, "#00ffff"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/samsung/thumbnail/office/ThumbnailUtils;->highLightedColor:Ljava/util/HashMap;

    const-string/jumbo v1, "magenta"

    const-string/jumbo v2, "#ff00ff"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/samsung/thumbnail/office/ThumbnailUtils;->highLightedColor:Ljava/util/HashMap;

    const-string/jumbo v1, "blue"

    const-string/jumbo v2, "#0000ff"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/samsung/thumbnail/office/ThumbnailUtils;->highLightedColor:Ljava/util/HashMap;

    const-string/jumbo v1, "red"

    const-string/jumbo v2, "#ff0000"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/samsung/thumbnail/office/ThumbnailUtils;->highLightedColor:Ljava/util/HashMap;

    const-string/jumbo v1, "darkblue"

    const-string/jumbo v2, "#000080"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/samsung/thumbnail/office/ThumbnailUtils;->highLightedColor:Ljava/util/HashMap;

    const-string/jumbo v1, "darkcyan"

    const-string/jumbo v2, "#008b8b"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/samsung/thumbnail/office/ThumbnailUtils;->highLightedColor:Ljava/util/HashMap;

    const-string/jumbo v1, "darkgreen"

    const-string/jumbo v2, "#006400"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/samsung/thumbnail/office/ThumbnailUtils;->highLightedColor:Ljava/util/HashMap;

    const-string/jumbo v1, "darkmagenta"

    const-string/jumbo v2, "#8b008b"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/samsung/thumbnail/office/ThumbnailUtils;->highLightedColor:Ljava/util/HashMap;

    const-string/jumbo v1, "darkred"

    const-string/jumbo v2, "#8b0000"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/samsung/thumbnail/office/ThumbnailUtils;->highLightedColor:Ljava/util/HashMap;

    const-string/jumbo v1, "darkyellow"

    const-string/jumbo v2, "#808000"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/samsung/thumbnail/office/ThumbnailUtils;->highLightedColor:Ljava/util/HashMap;

    const-string/jumbo v1, "darkgray"

    const-string/jumbo v2, "#a9a9a9"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/samsung/thumbnail/office/ThumbnailUtils;->highLightedColor:Ljava/util/HashMap;

    const-string/jumbo v1, "lightgray"

    const-string/jumbo v2, "#778899"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/samsung/thumbnail/office/ThumbnailUtils;->highLightedColor:Ljava/util/HashMap;

    const-string/jumbo v1, "black"

    const-string/jumbo v2, "#000000"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    return-void
.end method

.method public static isThumbnailFolderExists(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 37
    sget-object v5, Lcom/samsung/thumbnail/office/ThumbnailUtils;->thumbnailFolder:Ljava/io/File;

    if-eqz v5, :cond_1

    sget-object v5, Lcom/samsung/thumbnail/office/ThumbnailUtils;->thumbnailFolder:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 51
    :cond_0
    :goto_0
    return v3

    .line 42
    :cond_1
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {p0, v5}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v1

    .line 43
    .local v1, "externalDir":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/samsung/thumbnail/office/ThumbnailUtils;->THUMBNAIL_FOLDER:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 44
    .local v2, "thumbnailFolder":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-nez v5, :cond_0

    move v3, v4

    .line 47
    goto :goto_0

    .line 49
    .end local v1    # "externalDir":Ljava/lang/String;
    .end local v2    # "thumbnailFolder":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 50
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v3, "ThumbnailUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IOException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 51
    goto :goto_0
.end method
