.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "SpprLineHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/ILineEndsConsumer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler$ISpprLineConsumer;
    }
.end annotation


# instance fields
.field private lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

.field private shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

.field private styLineConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler$ISpprLineConsumer;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Ljava/lang/String;)V
    .locals 12
    .param p1, "shInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "elementName"    # Ljava/lang/String;

    .prologue
    .line 36
    const/16 v11, 0x1f

    invoke-direct {p0, v11, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 37
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 39
    const/4 v11, 0x5

    new-array v6, v11, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 41
    .local v6, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;

    const/4 v11, 0x1

    invoke-direct {v9, p1, v11}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Z)V

    .line 42
    .local v9, "spprNoFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v11, "noFill"

    invoke-direct {v10, v11, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 44
    .local v10, "spprNoFillSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v11, 0x0

    aput-object v10, v6, v11

    .line 46
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;

    const/4 v11, 0x0

    invoke-direct {v7, p0, p1, v11}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 48
    .local v7, "solidFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v11, "solidFill"

    invoke-direct {v8, v11, v7}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 50
    .local v8, "solidFillSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v11, 0x1

    aput-object v8, v6, v11

    .line 54
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/LineHeadTailHandler;

    const-string/jumbo v11, "headEnd"

    invoke-direct {v2, p0, v11}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/LineHeadTailHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/ILineEndsConsumer;Ljava/lang/String;)V

    .line 56
    .local v2, "lineHeadHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/LineHeadTailHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v11, "headEnd"

    invoke-direct {v3, v11, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 58
    .local v3, "lineHeadSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v11, 0x2

    aput-object v3, v6, v11

    .line 60
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/LineHeadTailHandler;

    const-string/jumbo v11, "tailEnd"

    invoke-direct {v4, p0, v11}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/LineHeadTailHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/ILineEndsConsumer;Ljava/lang/String;)V

    .line 62
    .local v4, "lineTailHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/LineHeadTailHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v11, "tailEnd"

    invoke-direct {v5, v11, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 64
    .local v5, "lineTailSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v11, 0x3

    aput-object v5, v6, v11

    .line 66
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;

    const/4 v11, 0x0

    invoke-direct {v0, p0, p1, v11}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 68
    .local v0, "gradFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v11, "gradFill"

    invoke-direct {v1, v11, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 70
    .local v1, "gradFillSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v11, 0x4

    aput-object v1, v6, v11

    .line 86
    iput-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 87
    return-void
.end method

.method private getPrsrDashValue(Lorg/xml/sax/Attributes;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;
    .locals 2
    .param p1, "attrs"    # Lorg/xml/sax/Attributes;
    .param p2, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 193
    const-string/jumbo v1, "val"

    invoke-virtual {p0, p1, v1, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 195
    .local v0, "prstDashVal":Ljava/lang/String;
    return-object v0
.end method


# virtual methods
.method public consumeFill(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 1
    .param p1, "fillProperties"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 200
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->setFillProp(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V

    .line 201
    return-void
.end method

.method public consumeGS(Ljava/lang/String;FLjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p1, "element"    # Ljava/lang/String;
    .param p2, "pos"    # F
    .param p3, "val"    # Ljava/lang/String;
    .param p4, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 236
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v0, p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setLineRefColor(Ljava/lang/String;)V

    .line 237
    return-void
.end method

.method public consumeLineHeadEnd(III)V
    .locals 0
    .param p1, "type"    # I
    .param p2, "len"    # I
    .param p3, "width"    # I

    .prologue
    .line 213
    return-void
.end method

.method public consumeLineTailEnd(III)V
    .locals 0
    .param p1, "type"    # I
    .param p2, "len"    # I
    .param p3, "width"    # I

    .prologue
    .line 223
    return-void
.end method

.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 5
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 157
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 158
    const-string/jumbo v3, "lnL"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 159
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->addLinePropsList(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    const-string/jumbo v3, "lnT"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 162
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->addLinePropsList(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V

    goto :goto_0

    .line 164
    :cond_2
    const-string/jumbo v3, "lnR"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 165
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->addLinePropsList(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V

    goto :goto_0

    .line 167
    :cond_3
    const-string/jumbo v3, "lnB"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 168
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->addLinePropsList(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V

    goto :goto_0

    .line 170
    :cond_4
    const-string/jumbo v3, "lnBlToTr"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 171
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;-><init>()V

    .line 172
    .local v0, "lineDiagBlToTr":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .line 173
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setDiagonalBorder(Z)V

    .line 174
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v3, p2, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->addDiagonalBrdr(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V

    goto :goto_0

    .line 176
    .end local v0    # "lineDiagBlToTr":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    :cond_5
    const-string/jumbo v3, "lnTlToBr"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 177
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;-><init>()V

    .line 178
    .local v1, "lineDiagTlToBr":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .line 179
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setDiagonalBorder(Z)V

    .line 180
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v3, p2, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->addDiagonalBrdr(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V

    goto :goto_0

    .line 182
    .end local v1    # "lineDiagTlToBr":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    :cond_6
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    if-eqz v3, :cond_7

    .line 183
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->consumeLine(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V

    goto :goto_0

    .line 184
    :cond_7
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;

    if-eqz v3, :cond_0

    .line 185
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;-><init>()V

    .line 186
    .local v2, "linePropsTblStyles":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .line 187
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->styLineConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler$ISpprLineConsumer;

    invoke-interface {v3, p1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler$ISpprLineConsumer;->tblStylesLineConsumer(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V

    goto/16 :goto_0
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 122
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 123
    invoke-virtual {p0, p2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 124
    .local v1, "elementName":Ljava/lang/String;
    const-string/jumbo v2, "prstDash"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 125
    invoke-direct {p0, p3, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->getPrsrDashValue(Lorg/xml/sax/Attributes;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 126
    .local v0, "dashValue":Ljava/lang/String;
    if-eqz v0, :cond_7

    .line 127
    const-string/jumbo v2, "dash"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 128
    const-string/jumbo v0, "dashed"

    .line 130
    :cond_0
    const-string/jumbo v2, "sysDashDot"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 131
    const-string/jumbo v0, "dashed"

    .line 133
    :cond_1
    const-string/jumbo v2, "sysDot"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 134
    const-string/jumbo v0, "dotted"

    .line 136
    :cond_2
    const-string/jumbo v2, "lgDash"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 137
    const-string/jumbo v0, "dashed"

    .line 139
    :cond_3
    const-string/jumbo v2, "dashDot"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 140
    const-string/jumbo v0, "dashed"

    .line 142
    :cond_4
    const-string/jumbo v2, "sysDash"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 143
    const-string/jumbo v0, "dashed"

    .line 145
    :cond_5
    const-string/jumbo v2, "lgDashDot"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 146
    const-string/jumbo v0, "dashed"

    .line 148
    :cond_6
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->setPrstDash(Ljava/lang/String;)V

    .line 153
    .end local v0    # "dashValue":Ljava/lang/String;
    :cond_7
    return-void
.end method

.method public setConsumer(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler$ISpprLineConsumer;)V
    .locals 0
    .param p1, "styLineConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler$ISpprLineConsumer;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->styLineConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler$ISpprLineConsumer;

    .line 91
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 96
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 97
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .line 107
    const-string/jumbo v1, "w"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 108
    .local v0, "width":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 109
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->setLineWidth(Ljava/lang/String;)V

    .line 117
    :cond_0
    return-void
.end method
