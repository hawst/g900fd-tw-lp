.class public Lcom/samsung/thumbnail/office/point/shapes/Callout2;
.super Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;
.source "Callout2.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mDrawOnCanvas:Z

.field private mLeftMarg:I

.field private mTopMarg:I

.field public newBitmapHeight:I

.field public newBitmapWidth:I

.field public subFactor_Height:I

.field public subFactor_Width:I


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 2
    .param p1, "folderPath"    # Ljava/io/File;

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;-><init>()V

    .line 24
    const-string/jumbo v0, "Callout2"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->TAG:Ljava/lang/String;

    .line 25
    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    .line 26
    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    .line 27
    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->newBitmapHeight:I

    .line 28
    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->newBitmapWidth:I

    .line 29
    iput-boolean v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mDrawOnCanvas:Z

    .line 34
    iput-object p1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->folderPath:Ljava/io/File;

    .line 35
    return-void
.end method


# virtual methods
.method protected draw(I)V
    .locals 26
    .param p1, "shapeCount"    # I

    .prologue
    .line 157
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->shapeName:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 413
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->width:F

    const v4, 0x46a8c000    # 21600.0f

    div-float v22, v3, v4

    .line 161
    .local v22, "mulFactor_Width":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->height:F

    const v4, 0x46a8c000    # 21600.0f

    div-float v21, v3, v4

    .line 180
    .local v21, "mulFactor_Height":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval0:I

    if-nez v3, :cond_2

    .line 181
    const/16 v3, -0x2760

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval0:I

    .line 182
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval1:I

    if-nez v3, :cond_3

    .line 183
    const/16 v3, 0x5eec

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval1:I

    .line 184
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval2:I

    if-nez v3, :cond_4

    .line 185
    const/16 v3, -0xe10

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval2:I

    .line 186
    :cond_4
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval3:I

    if-nez v3, :cond_5

    .line 187
    const/16 v3, 0xfd2

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval3:I

    .line 188
    :cond_5
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval4:I

    if-nez v3, :cond_6

    .line 189
    const/16 v3, -0x708

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval4:I

    .line 190
    :cond_6
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval5:I

    if-nez v3, :cond_7

    .line 191
    const/16 v3, 0xfd2

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval5:I

    .line 193
    :cond_7
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval0:I

    .line 194
    .local v9, "calloutadjval0":I
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval1:I

    .line 195
    .local v10, "calloutadjval1":I
    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval2:I

    .line 196
    .local v11, "calloutadjval2":I
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval3:I

    .line 197
    .local v12, "calloutadjval3":I
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval4:I

    .line 198
    .local v13, "calloutadjval4":I
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval5:I

    .line 201
    .local v14, "calloutadjval5":I
    invoke-static {v9, v11}, Ljava/lang/Math;->min(II)I

    move-result v19

    .line 202
    .local v19, "minX":I
    move/from16 v0, v19

    invoke-static {v0, v13}, Ljava/lang/Math;->min(II)I

    move-result v19

    .line 203
    const/4 v3, 0x0

    move/from16 v0, v19

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v19

    .line 204
    invoke-static {v9, v11}, Ljava/lang/Math;->max(II)I

    move-result v17

    .line 205
    .local v17, "maxX":I
    move/from16 v0, v17

    invoke-static {v0, v13}, Ljava/lang/Math;->max(II)I

    move-result v17

    .line 206
    const/16 v3, 0x5460

    move/from16 v0, v17

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v17

    .line 207
    move/from16 v0, v19

    int-to-float v3, v0

    mul-float v3, v3, v22

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    .line 208
    move/from16 v0, v17

    int-to-float v3, v0

    mul-float v3, v3, v22

    move/from16 v0, v19

    int-to-float v4, v0

    mul-float v4, v4, v22

    sub-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->newBitmapWidth:I

    .line 211
    invoke-static {v10, v12}, Ljava/lang/Math;->min(II)I

    move-result v20

    .line 212
    .local v20, "minY":I
    move/from16 v0, v20

    invoke-static {v0, v14}, Ljava/lang/Math;->min(II)I

    move-result v20

    .line 213
    const/4 v3, 0x0

    move/from16 v0, v20

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v20

    .line 214
    invoke-static {v10, v12}, Ljava/lang/Math;->max(II)I

    move-result v18

    .line 215
    .local v18, "maxY":I
    move/from16 v0, v18

    invoke-static {v0, v14}, Ljava/lang/Math;->max(II)I

    move-result v18

    .line 216
    const/16 v3, 0x5460

    move/from16 v0, v18

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v18

    .line 217
    move/from16 v0, v20

    int-to-float v3, v0

    mul-float v3, v3, v21

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    .line 218
    move/from16 v0, v18

    int-to-float v3, v0

    mul-float v3, v3, v21

    move/from16 v0, v20

    int-to-float v4, v0

    mul-float v4, v4, v21

    sub-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->newBitmapHeight:I

    .line 220
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->newBitmapWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->newBitmapHeight:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 225
    .local v2, "_bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "Callout2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 226
    new-instance v15, Landroid/graphics/Canvas;

    invoke-direct {v15, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 228
    .local v15, "canvas":Landroid/graphics/Canvas;
    new-instance v24, Landroid/graphics/Path;

    invoke-direct/range {v24 .. v24}, Landroid/graphics/Path;-><init>()V

    .line 230
    .local v24, "path":Landroid/graphics/Path;
    int-to-float v3, v9

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v10

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 232
    int-to-float v3, v11

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v12

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 234
    int-to-float v3, v13

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v14

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 236
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_8

    .line 237
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v24

    invoke-virtual {v15, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 239
    :cond_8
    new-instance v24, Landroid/graphics/Path;

    .end local v24    # "path":Landroid/graphics/Path;
    invoke-direct/range {v24 .. v24}, Landroid/graphics/Path;-><init>()V

    .line 240
    .restart local v24    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 242
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 244
    const/4 v3, 0x0

    mul-float v3, v3, v22

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v21

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 245
    const/4 v3, 0x0

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 247
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Path;->close()V

    .line 249
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_9

    .line 250
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, v24

    invoke-virtual {v15, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 254
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x462dfc00    # 11135.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 255
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 256
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x46e80800    # 29700.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 257
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    .line 380
    .end local v15    # "canvas":Landroid/graphics/Canvas;
    .end local v24    # "path":Landroid/graphics/Path;
    :cond_a
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mDrawOnCanvas:Z

    if-nez v3, :cond_0

    .line 381
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 383
    .local v7, "mtx":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->flipHorizontal:Z

    if-eqz v3, :cond_17

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->flipVertical:Z

    if-nez v3, :cond_17

    .line 384
    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 393
    :cond_b
    :goto_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->rotation:I

    int-to-float v3, v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    invoke-virtual {v7, v3, v4, v5}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 396
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v25

    .line 400
    .local v25, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v23, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->folderPath:Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "pic_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->shapeName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ".png"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v23

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 406
    .local v23, "out":Ljava/io/FileOutputStream;
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-virtual {v0, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 407
    invoke-virtual/range {v23 .. v23}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 408
    .end local v23    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v16

    .line 409
    .local v16, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception while writing pictures to out file"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 260
    .end local v7    # "mtx":Landroid/graphics/Matrix;
    .end local v16    # "e":Ljava/lang/Exception;
    .end local v25    # "rotatedBMP":Landroid/graphics/Bitmap;
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "BorderCallout2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 261
    new-instance v15, Landroid/graphics/Canvas;

    invoke-direct {v15, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 263
    .restart local v15    # "canvas":Landroid/graphics/Canvas;
    new-instance v24, Landroid/graphics/Path;

    invoke-direct/range {v24 .. v24}, Landroid/graphics/Path;-><init>()V

    .line 265
    .restart local v24    # "path":Landroid/graphics/Path;
    int-to-float v3, v9

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v10

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 267
    int-to-float v3, v11

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v12

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 269
    int-to-float v3, v13

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v14

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 271
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_d

    .line 272
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v24

    invoke-virtual {v15, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 274
    :cond_d
    new-instance v24, Landroid/graphics/Path;

    .end local v24    # "path":Landroid/graphics/Path;
    invoke-direct/range {v24 .. v24}, Landroid/graphics/Path;-><init>()V

    .line 275
    .restart local v24    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 277
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 279
    const/4 v3, 0x0

    mul-float v3, v3, v22

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v21

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 280
    const/4 v3, 0x0

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 282
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Path;->close()V

    .line 284
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_e

    .line 285
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, v24

    invoke-virtual {v15, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 286
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_f

    .line 287
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v24

    invoke-virtual {v15, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 291
    :cond_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x462dfc00    # 11135.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 292
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 293
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x46e80800    # 29700.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 294
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 297
    .end local v15    # "canvas":Landroid/graphics/Canvas;
    .end local v24    # "path":Landroid/graphics/Path;
    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "AccentCallout2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 298
    new-instance v15, Landroid/graphics/Canvas;

    invoke-direct {v15, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 300
    .restart local v15    # "canvas":Landroid/graphics/Canvas;
    new-instance v24, Landroid/graphics/Path;

    invoke-direct/range {v24 .. v24}, Landroid/graphics/Path;-><init>()V

    .line 303
    .restart local v24    # "path":Landroid/graphics/Path;
    int-to-float v3, v9

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v10

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 305
    int-to-float v3, v11

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v12

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 307
    int-to-float v3, v13

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v14

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 309
    int-to-float v3, v13

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 311
    int-to-float v3, v13

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 313
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_11

    .line 314
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v24

    invoke-virtual {v15, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 316
    :cond_11
    new-instance v24, Landroid/graphics/Path;

    .end local v24    # "path":Landroid/graphics/Path;
    invoke-direct/range {v24 .. v24}, Landroid/graphics/Path;-><init>()V

    .line 317
    .restart local v24    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 319
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 321
    const/4 v3, 0x0

    mul-float v3, v3, v22

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v21

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 322
    const/4 v3, 0x0

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 324
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Path;->close()V

    .line 326
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_12

    .line 327
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, v24

    invoke-virtual {v15, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 331
    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x462dfc00    # 11135.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 332
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 333
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x46e80800    # 29700.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 334
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 337
    .end local v15    # "canvas":Landroid/graphics/Canvas;
    .end local v24    # "path":Landroid/graphics/Path;
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "AccentBorderCallout2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 338
    new-instance v15, Landroid/graphics/Canvas;

    invoke-direct {v15, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 340
    .restart local v15    # "canvas":Landroid/graphics/Canvas;
    new-instance v24, Landroid/graphics/Path;

    invoke-direct/range {v24 .. v24}, Landroid/graphics/Path;-><init>()V

    .line 343
    .restart local v24    # "path":Landroid/graphics/Path;
    int-to-float v3, v9

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v10

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 345
    int-to-float v3, v11

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v12

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 347
    int-to-float v3, v13

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v14

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 349
    int-to-float v3, v13

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 351
    int-to-float v3, v13

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 353
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_14

    .line 354
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v24

    invoke-virtual {v15, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 356
    :cond_14
    new-instance v24, Landroid/graphics/Path;

    .end local v24    # "path":Landroid/graphics/Path;
    invoke-direct/range {v24 .. v24}, Landroid/graphics/Path;-><init>()V

    .line 357
    .restart local v24    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 359
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 361
    const/4 v3, 0x0

    mul-float v3, v3, v22

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v21

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 362
    const/4 v3, 0x0

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 364
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Path;->close()V

    .line 366
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_15

    .line 367
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, v24

    invoke-virtual {v15, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 368
    :cond_15
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_16

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_16

    .line 369
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v24

    invoke-virtual {v15, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 373
    :cond_16
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x462dfc00    # 11135.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 374
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 375
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x46e80800    # 29700.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 376
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 386
    .end local v15    # "canvas":Landroid/graphics/Canvas;
    .end local v24    # "path":Landroid/graphics/Path;
    .restart local v7    # "mtx":Landroid/graphics/Matrix;
    :cond_17
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->flipVertical:Z

    if-eqz v3, :cond_18

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->flipHorizontal:Z

    if-nez v3, :cond_18

    .line 387
    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, -0x40800000    # -1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    goto/16 :goto_2

    .line 389
    :cond_18
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->flipHorizontal:Z

    if-eqz v3, :cond_b

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->flipVertical:Z

    if-eqz v3, :cond_b

    .line 390
    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v4, -0x40800000    # -1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    goto/16 :goto_2
.end method

.method protected draw(Landroid/graphics/Canvas;I)V
    .locals 25
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeCount"    # I

    .prologue
    .line 417
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->shapeName:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 673
    :cond_0
    :goto_0
    return-void

    .line 420
    :cond_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->width:F

    const v4, 0x46a8c000    # 21600.0f

    div-float v21, v3, v4

    .line 421
    .local v21, "mulFactor_Width":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->height:F

    const v4, 0x46a8c000    # 21600.0f

    div-float v20, v3, v4

    .line 440
    .local v20, "mulFactor_Height":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval0:I

    if-nez v3, :cond_2

    .line 441
    const/16 v3, -0x2760

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval0:I

    .line 442
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval1:I

    if-nez v3, :cond_3

    .line 443
    const/16 v3, 0x5eec

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval1:I

    .line 444
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval2:I

    if-nez v3, :cond_4

    .line 445
    const/16 v3, -0xe10

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval2:I

    .line 446
    :cond_4
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval3:I

    if-nez v3, :cond_5

    .line 447
    const/16 v3, 0xfd2

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval3:I

    .line 448
    :cond_5
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval4:I

    if-nez v3, :cond_6

    .line 449
    const/16 v3, -0x708

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval4:I

    .line 450
    :cond_6
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval5:I

    if-nez v3, :cond_7

    .line 451
    const/16 v3, 0xfd2

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval5:I

    .line 453
    :cond_7
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval0:I

    .line 454
    .local v9, "calloutadjval0":I
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval1:I

    .line 455
    .local v10, "calloutadjval1":I
    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval2:I

    .line 456
    .local v11, "calloutadjval2":I
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval3:I

    .line 457
    .local v12, "calloutadjval3":I
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval4:I

    .line 458
    .local v13, "calloutadjval4":I
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->adjval5:I

    .line 461
    .local v14, "calloutadjval5":I
    invoke-static {v9, v11}, Ljava/lang/Math;->min(II)I

    move-result v18

    .line 462
    .local v18, "minX":I
    move/from16 v0, v18

    invoke-static {v0, v13}, Ljava/lang/Math;->min(II)I

    move-result v18

    .line 463
    const/4 v3, 0x0

    move/from16 v0, v18

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v18

    .line 464
    invoke-static {v9, v11}, Ljava/lang/Math;->max(II)I

    move-result v16

    .line 465
    .local v16, "maxX":I
    move/from16 v0, v16

    invoke-static {v0, v13}, Ljava/lang/Math;->max(II)I

    move-result v16

    .line 466
    const/16 v3, 0x5460

    move/from16 v0, v16

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v16

    .line 467
    move/from16 v0, v18

    int-to-float v3, v0

    mul-float v3, v3, v21

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    .line 468
    move/from16 v0, v16

    int-to-float v3, v0

    mul-float v3, v3, v21

    move/from16 v0, v18

    int-to-float v4, v0

    mul-float v4, v4, v21

    sub-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->newBitmapWidth:I

    .line 471
    invoke-static {v10, v12}, Ljava/lang/Math;->min(II)I

    move-result v19

    .line 472
    .local v19, "minY":I
    move/from16 v0, v19

    invoke-static {v0, v14}, Ljava/lang/Math;->min(II)I

    move-result v19

    .line 473
    const/4 v3, 0x0

    move/from16 v0, v19

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v19

    .line 474
    invoke-static {v10, v12}, Ljava/lang/Math;->max(II)I

    move-result v17

    .line 475
    .local v17, "maxY":I
    move/from16 v0, v17

    invoke-static {v0, v14}, Ljava/lang/Math;->max(II)I

    move-result v17

    .line 476
    const/16 v3, 0x5460

    move/from16 v0, v17

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v17

    .line 477
    move/from16 v0, v19

    int-to-float v3, v0

    mul-float v3, v3, v20

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    .line 478
    move/from16 v0, v17

    int-to-float v3, v0

    mul-float v3, v3, v20

    move/from16 v0, v19

    int-to-float v4, v0

    mul-float v4, v4, v20

    sub-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->newBitmapHeight:I

    .line 480
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->newBitmapWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->newBitmapHeight:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 485
    .local v2, "_bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "Callout2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 488
    new-instance v23, Landroid/graphics/Path;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/Path;-><init>()V

    .line 490
    .local v23, "path":Landroid/graphics/Path;
    int-to-float v3, v9

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v10

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 492
    int-to-float v3, v11

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v12

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 494
    int-to-float v3, v13

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v14

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 496
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_8

    .line 497
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 499
    :cond_8
    new-instance v23, Landroid/graphics/Path;

    .end local v23    # "path":Landroid/graphics/Path;
    invoke-direct/range {v23 .. v23}, Landroid/graphics/Path;-><init>()V

    .line 500
    .restart local v23    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 502
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 504
    const/4 v3, 0x0

    mul-float v3, v3, v21

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v20

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 505
    const/4 v3, 0x0

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 507
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Path;->close()V

    .line 509
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_9

    .line 510
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 514
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x462dfc00    # 11135.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 515
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 516
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x46e80800    # 29700.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 517
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    .line 640
    .end local v23    # "path":Landroid/graphics/Path;
    :cond_a
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mDrawOnCanvas:Z

    if-nez v3, :cond_0

    .line 641
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 643
    .local v7, "mtx":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->flipHorizontal:Z

    if-eqz v3, :cond_17

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->flipVertical:Z

    if-nez v3, :cond_17

    .line 644
    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 653
    :cond_b
    :goto_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->rotation:I

    int-to-float v3, v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    invoke-virtual {v7, v3, v4, v5}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 656
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v24

    .line 660
    .local v24, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v22, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->folderPath:Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "pic_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->shapeName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ".png"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 666
    .local v22, "out":Ljava/io/FileOutputStream;
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v0, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 667
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 668
    .end local v22    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v15

    .line 669
    .local v15, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception while writing pictures to out file"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 520
    .end local v7    # "mtx":Landroid/graphics/Matrix;
    .end local v15    # "e":Ljava/lang/Exception;
    .end local v24    # "rotatedBMP":Landroid/graphics/Bitmap;
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "BorderCallout2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 523
    new-instance v23, Landroid/graphics/Path;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/Path;-><init>()V

    .line 525
    .restart local v23    # "path":Landroid/graphics/Path;
    int-to-float v3, v9

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v10

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 527
    int-to-float v3, v11

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v12

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 529
    int-to-float v3, v13

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v14

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 531
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_d

    .line 532
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 534
    :cond_d
    new-instance v23, Landroid/graphics/Path;

    .end local v23    # "path":Landroid/graphics/Path;
    invoke-direct/range {v23 .. v23}, Landroid/graphics/Path;-><init>()V

    .line 535
    .restart local v23    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 537
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 539
    const/4 v3, 0x0

    mul-float v3, v3, v21

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v20

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 540
    const/4 v3, 0x0

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 542
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Path;->close()V

    .line 544
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_e

    .line 545
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 546
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_f

    .line 547
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 551
    :cond_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x462dfc00    # 11135.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 552
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 553
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x46e80800    # 29700.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 554
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 557
    .end local v23    # "path":Landroid/graphics/Path;
    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "AccentCallout2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 560
    new-instance v23, Landroid/graphics/Path;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/Path;-><init>()V

    .line 563
    .restart local v23    # "path":Landroid/graphics/Path;
    int-to-float v3, v9

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v10

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 565
    int-to-float v3, v11

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v12

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 567
    int-to-float v3, v13

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v14

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 569
    int-to-float v3, v13

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 571
    int-to-float v3, v13

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 573
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_11

    .line 574
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 576
    :cond_11
    new-instance v23, Landroid/graphics/Path;

    .end local v23    # "path":Landroid/graphics/Path;
    invoke-direct/range {v23 .. v23}, Landroid/graphics/Path;-><init>()V

    .line 577
    .restart local v23    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 579
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 581
    const/4 v3, 0x0

    mul-float v3, v3, v21

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v20

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 582
    const/4 v3, 0x0

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 584
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Path;->close()V

    .line 586
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_12

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_12

    .line 587
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 591
    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x462dfc00    # 11135.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 592
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 593
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x46e80800    # 29700.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 594
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 597
    .end local v23    # "path":Landroid/graphics/Path;
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "AccentBorderCallout2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 600
    new-instance v23, Landroid/graphics/Path;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/Path;-><init>()V

    .line 603
    .restart local v23    # "path":Landroid/graphics/Path;
    int-to-float v3, v9

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v10

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 605
    int-to-float v3, v11

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v12

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 607
    int-to-float v3, v13

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v14

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 609
    int-to-float v3, v13

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 611
    int-to-float v3, v13

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 613
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_14

    .line 614
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 616
    :cond_14
    new-instance v23, Landroid/graphics/Path;

    .end local v23    # "path":Landroid/graphics/Path;
    invoke-direct/range {v23 .. v23}, Landroid/graphics/Path;-><init>()V

    .line 617
    .restart local v23    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 619
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 621
    const/4 v3, 0x0

    mul-float v3, v3, v21

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v20

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 622
    const/4 v3, 0x0

    mul-float v3, v3, v21

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 624
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Path;->close()V

    .line 626
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_15

    .line 627
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 628
    :cond_15
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_16

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_16

    .line 629
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 633
    :cond_16
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x462dfc00    # 11135.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 634
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 635
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x46e80800    # 29700.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 636
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 646
    .end local v23    # "path":Landroid/graphics/Path;
    .restart local v7    # "mtx":Landroid/graphics/Matrix;
    :cond_17
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->flipVertical:Z

    if-eqz v3, :cond_18

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->flipHorizontal:Z

    if-nez v3, :cond_18

    .line 647
    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, -0x40800000    # -1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    goto/16 :goto_2

    .line 649
    :cond_18
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->flipHorizontal:Z

    if-eqz v3, :cond_b

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->flipVertical:Z

    if-eqz v3, :cond_b

    .line 650
    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v4, -0x40800000    # -1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    goto/16 :goto_2
.end method

.method public drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F

    .prologue
    .line 111
    invoke-super {p0, p3, p4, p7, p8}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IFF)V

    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mDrawOnCanvas:Z

    .line 113
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mLeftMarg:I

    .line 114
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mTopMarg:I

    .line 115
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 116
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->draw(Landroid/graphics/Canvas;I)V

    .line 117
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 118
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mDrawOnCanvas:Z

    .line 119
    return-void
.end method

.method public drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p2, "shapeCount"    # I

    .prologue
    .line 102
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V

    .line 104
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->draw(I)V

    .line 105
    return-void
.end method

.method public drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 88
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;I)V

    .line 89
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mDrawOnCanvas:Z

    .line 90
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mLeftMarg:I

    .line 91
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mTopMarg:I

    .line 92
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->width:F

    .line 93
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->height:F

    .line 94
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 95
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->draw(Landroid/graphics/Canvas;I)V

    .line 96
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 97
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mDrawOnCanvas:Z

    .line 98
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V
    .locals 0
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 39
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 41
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->draw(I)V

    .line 42
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V
    .locals 2
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I

    .prologue
    .line 46
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mDrawOnCanvas:Z

    .line 48
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mLeftMarg:I

    .line 49
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mTopMarg:I

    .line 50
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 51
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->draw(Landroid/graphics/Canvas;I)V

    .line 52
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mDrawOnCanvas:Z

    .line 54
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 133
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 134
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mDrawOnCanvas:Z

    .line 135
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mLeftMarg:I

    .line 136
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mTopMarg:I

    .line 137
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 138
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->draw(Landroid/graphics/Canvas;I)V

    .line 139
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 140
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mDrawOnCanvas:Z

    .line 141
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 145
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V

    .line 146
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mDrawOnCanvas:Z

    .line 147
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mLeftMarg:I

    .line 148
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mTopMarg:I

    .line 149
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 150
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->draw(Landroid/graphics/Canvas;I)V

    .line 151
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 152
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mDrawOnCanvas:Z

    .line 153
    return-void
.end method

.method public drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .prologue
    .line 126
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 128
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->draw(I)V

    .line 129
    return-void
.end method

.method public drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 59
    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p7

    move v4, p8

    move-object v5, p9

    invoke-super/range {v0 .. v5}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSSExcelShape(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mDrawOnCanvas:Z

    .line 62
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mLeftMarg:I

    .line 63
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mTopMarg:I

    .line 64
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 65
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->draw(Landroid/graphics/Canvas;I)V

    .line 66
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mDrawOnCanvas:Z

    .line 68
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 72
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mDrawOnCanvas:Z

    .line 74
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mLeftMarg:I

    .line 75
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mTopMarg:I

    .line 76
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->width:F

    .line 77
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->height:F

    .line 78
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 79
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->draw(Landroid/graphics/Canvas;I)V

    .line 80
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 81
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->mDrawOnCanvas:Z

    .line 82
    return-void
.end method
