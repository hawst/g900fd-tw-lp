.class public final enum Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;
.super Ljava/lang/Enum;
.source "XWPFShapeStyle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EFillType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

.field public static final enum FRAME:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

.field public static final enum GRADIENT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

.field public static final enum GRADIENT_RADIAL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

.field public static final enum PATTERN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

.field public static final enum SOLID:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

.field public static final enum TILE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 238
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    const-string/jumbo v1, "SOLID"

    const-string/jumbo v2, "solid"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;->SOLID:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    .line 241
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    const-string/jumbo v1, "GRADIENT"

    const-string/jumbo v2, "gradient"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;->GRADIENT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    .line 244
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    const-string/jumbo v1, "GRADIENT_RADIAL"

    const-string/jumbo v2, "gradientRadial"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;->GRADIENT_RADIAL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    .line 247
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    const-string/jumbo v1, "TILE"

    const-string/jumbo v2, "tile"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;->TILE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    .line 250
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    const-string/jumbo v1, "PATTERN"

    const-string/jumbo v2, "pattern"

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;->PATTERN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    .line 253
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    const-string/jumbo v1, "FRAME"

    const/4 v2, 0x5

    const-string/jumbo v3, "frame"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;->FRAME:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    .line 234
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;->SOLID:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;->GRADIENT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;->GRADIENT_RADIAL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;->TILE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;->PATTERN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;->FRAME:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 259
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 260
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;->value:Ljava/lang/String;

    .line 261
    return-void
.end method

.method public static convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 268
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;->values()[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 269
    .local v2, "inst":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 273
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;
    :goto_1
    return-object v2

    .line 268
    .restart local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 273
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 234
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;
    .locals 1

    .prologue
    .line 234
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    return-object v0
.end method


# virtual methods
.method public xmlValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;->value:Ljava/lang/String;

    return-object v0
.end method
