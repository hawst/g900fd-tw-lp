.class public Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;
.source "ShapeElementsHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler$IStrokeConsumer;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "nIDm"    # I
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;-><init>(ILjava/lang/String;)V

    .line 25
    return-void
.end method

.method private parsePathAttrs(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 102
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getPathProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;

    move-result-object v0

    .line 103
    .local v0, "pathProp":Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;
    if-nez v0, :cond_0

    .line 104
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;

    .end local v0    # "pathProp":Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;
    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;-><init>()V

    .line 105
    .restart local v0    # "pathProp":Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setPathProp(Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;)V

    .line 107
    :cond_0
    const-string/jumbo v2, "connecttype"

    const/16 v3, 0x32

    invoke-virtual {p0, p2, v2, v3, p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->getAttrForNS(Lorg/xml/sax/Attributes;Ljava/lang/String;ILcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 109
    .local v1, "val":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 110
    invoke-static {v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectType;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectType;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;->setConnectType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectType;)V

    .line 112
    :cond_1
    return-void
.end method


# virtual methods
.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 14
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 60
    const-string/jumbo v12, "style"

    move-object/from16 v0, p3

    invoke-virtual {p0, v0, v12}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 61
    .local v7, "style":Ljava/lang/String;
    if-eqz v7, :cond_1

    .line 62
    const-string/jumbo v12, ";"

    invoke-virtual {v7, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 63
    .local v2, "attrArray":[Ljava/lang/String;
    move-object v1, v2

    .local v1, "arr$":[Ljava/lang/String;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v6, :cond_1

    aget-object v8, v1, v4

    .line 64
    .local v8, "value":Ljava/lang/String;
    if-eqz v8, :cond_0

    .line 65
    const/4 v12, 0x0

    const/16 v13, 0x3a

    invoke-virtual {v8, v13}, Ljava/lang/String;->indexOf(I)I

    move-result v13

    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 66
    .local v5, "key":Ljava/lang/String;
    const-string/jumbo v12, "mso-layout-flow-alt"

    invoke-virtual {v12, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 67
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    const/16 v13, 0x3a

    invoke-virtual {v8, v13}, Ljava/lang/String;->indexOf(I)I

    move-result v13

    add-int/lit8 v13, v13, 0x1

    invoke-virtual {v8, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->setTxtBxRotParam(Ljava/lang/String;)V

    .line 63
    .end local v5    # "key":Ljava/lang/String;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 74
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v2    # "attrArray":[Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v6    # "len$":I
    .end local v8    # "value":Ljava/lang/String;
    :cond_1
    const-string/jumbo v12, "w10:wrap"

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 75
    const-string/jumbo v12, "anchorx"

    move-object/from16 v0, p3

    invoke-virtual {p0, v0, v12}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 76
    .local v10, "xVal":Ljava/lang/String;
    if-eqz v10, :cond_2

    .line 77
    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {v12, v10}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setxAnchorRef(Ljava/lang/String;)V

    .line 79
    :cond_2
    const-string/jumbo v12, "anchory"

    move-object/from16 v0, p3

    invoke-virtual {p0, v0, v12}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 80
    .local v11, "yVal":Ljava/lang/String;
    if-eqz v11, :cond_3

    .line 81
    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {v12, v11}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setyAnchorRef(Ljava/lang/String;)V

    .line 85
    .end local v10    # "xVal":Ljava/lang/String;
    .end local v11    # "yVal":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p2

    invoke-virtual {p0, v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v3

    .line 86
    .local v3, "ele":Ljava/lang/String;
    const-string/jumbo v12, "path"

    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 88
    move-object/from16 v0, p3

    invoke-direct {p0, p1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->parsePathAttrs(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lorg/xml/sax/Attributes;)V

    .line 99
    :cond_4
    :goto_1
    return-void

    .line 90
    :cond_5
    const-string/jumbo v12, "textpath"

    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 91
    const-string/jumbo v12, "string"

    move-object/from16 v0, p3

    invoke-virtual {p0, v0, v12}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 93
    .local v9, "waterMarkValue":Ljava/lang/String;
    if-eqz v9, :cond_4

    .line 94
    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {v12, v9}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setStringText(Ljava/lang/String;)V

    goto :goto_1

    .line 98
    .end local v9    # "waterMarkValue":Ljava/lang/String;
    :cond_6
    invoke-super/range {p0 .. p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    goto :goto_1
.end method

.method protected init()V
    .locals 6

    .prologue
    .line 28
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler;

    invoke-direct {v2, p0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler$IStrokeConsumer;)V

    .line 29
    .local v2, "strokeHandler":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "stroke"

    invoke-virtual {v4, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/TextBoxHandler;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->shape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    invoke-direct {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/TextBoxHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V

    .line 32
    .local v3, "txtBxHandler":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/TextBoxHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "textbox"

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/FormulaHandler;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-direct {v1, v4}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/FormulaHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 35
    .local v1, "formulaHandler":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/FormulaHandler;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLArrayHandler;

    const/16 v4, 0x28

    const-string/jumbo v5, "formulas"

    invoke-direct {v0, v4, v5, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLArrayHandler;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 37
    .local v0, "fArrayHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLArrayHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "formulas"

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    return-void
.end method

.method public setStroke(Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;)V
    .locals 1
    .param p1, "strokeProp"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setStrokeProp(Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;)V

    .line 117
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 43
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLVMLElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 44
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->init()V

    .line 46
    const-string/jumbo v1, "fillcolor"

    invoke-virtual {p0, p3, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, "style":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getColor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setFillColor(Ljava/lang/String;)V

    .line 49
    const-string/jumbo v1, "strokecolor"

    invoke-virtual {p0, p3, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 50
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getColor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setStrokeColor(Ljava/lang/String;)V

    .line 52
    const-string/jumbo v1, "strokeweight"

    invoke-virtual {p0, p3, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 53
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setStrokeWeight(Ljava/lang/String;)V

    .line 54
    return-void
.end method
