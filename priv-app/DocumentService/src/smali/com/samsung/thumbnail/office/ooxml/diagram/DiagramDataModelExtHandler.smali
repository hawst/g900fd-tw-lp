.class public Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelExtHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DiagramDataModelExtHandler.java"


# direct methods
.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;)V
    .locals 4
    .param p1, "nsID"    # I
    .param p2, "dataModel"    # Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

    .prologue
    .line 19
    const-string/jumbo v3, "ext"

    invoke-direct {p0, p1, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 21
    const/4 v3, 0x1

    new-array v2, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 23
    .local v2, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelDMExtHandler;

    const/16 v3, 0xcb

    invoke-direct {v0, v3, p2}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelDMExtHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;)V

    .line 25
    .local v0, "dataModelExtHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelDMExtHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;

    const-string/jumbo v3, "dataModelExt"

    invoke-direct {v1, v3, v0}, Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 27
    .local v1, "dspSeq":Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;
    const/4 v3, 0x0

    aput-object v1, v2, v3

    .line 29
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelExtHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 30
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 35
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 36
    return-void
.end method
