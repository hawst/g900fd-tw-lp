.class public Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;
.super Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;
.source "XSLFSlideLayout.java"


# instance fields
.field protected colorMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private master:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

.field private parsed:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->parsed:Z

    .line 46
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V
    .locals 1
    .param p1, "part"    # Lorg/apache/poi/openxml4j/opc/PackagePart;
    .param p2, "rel"    # Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;-><init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->parsed:Z

    .line 51
    return-void
.end method

.method private getShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;[Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;)Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    .locals 6
    .param p1, "placeHolder"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;
    .param p2, "shapes"    # [Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;

    .prologue
    .line 154
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p2

    if-ge v1, v2, :cond_2

    .line 155
    aget-object v2, p2, v1

    instance-of v2, v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    if-eqz v2, :cond_0

    .line 156
    aget-object v0, p2, v1

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    .line 157
    .local v0, "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v2

    if-nez v2, :cond_1

    .line 154
    .end local v0    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 161
    .restart local v0    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    :cond_1
    iget-object v2, p1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;->phType:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;->phType:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    if-ne v2, v3, :cond_0

    iget-wide v2, p1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;->phIndex:J

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v4

    iget-wide v4, v4, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;->phIndex:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-object v2, p1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;->size:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder$Size;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;->size:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder$Size;

    if-ne v2, v3, :cond_0

    .line 168
    .end local v0    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private getShapeFromMasterSlide(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;[Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;)Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    .locals 6
    .param p1, "placeHolder"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;
    .param p2, "shapes"    # [Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;

    .prologue
    .line 173
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p2

    if-ge v1, v2, :cond_3

    .line 174
    aget-object v2, p2, v1

    instance-of v2, v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    if-eqz v2, :cond_0

    .line 175
    aget-object v0, p2, v1

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    .line 176
    .local v0, "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v2

    if-nez v2, :cond_1

    .line 173
    .end local v0    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 179
    .restart local v0    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    :cond_1
    iget-object v2, p1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;->phType:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    if-eqz v2, :cond_2

    iget-object v2, p1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;->phType:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;->phType:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    if-ne v2, v3, :cond_2

    .line 189
    .end local v0    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    :goto_1
    return-object v0

    .line 184
    .restart local v0    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    :cond_2
    iget-wide v2, p1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;->phIndex:J

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v4

    iget-wide v4, v4, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;->phIndex:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    goto :goto_1

    .line 189
    .end local v0    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "color"    # Ljava/lang/String;

    .prologue
    .line 105
    move-object v0, p1

    .line 106
    .local v0, "str":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->colorMap:Ljava/util/Map;

    if-eqz v1, :cond_0

    .line 107
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->colorMap:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "str":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 108
    .restart local v0    # "str":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 109
    move-object v0, p1

    .line 113
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 114
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 116
    :cond_1
    return-object v0
.end method

.method public getColorMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->colorMap:Ljava/util/Map;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    const-string/jumbo v0, ""

    return-object v0
.end method

.method protected getRootElementName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    const-string/jumbo v0, "sldLayout"

    return-object v0
.end method

.method public getShapeForAnchor(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;)Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    .locals 3
    .param p1, "placeHolder"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    .prologue
    const/4 v1, 0x0

    .line 120
    if-nez p1, :cond_1

    move-object v0, v1

    .line 132
    :cond_0
    :goto_0
    return-object v0

    .line 123
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getShapes()[Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;[Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;)Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-result-object v0

    .line 125
    .local v0, "shape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getAnchor()Lorg/apache/poi/java/awt/geom/Rectangle2D;

    move-result-object v2

    if-nez v2, :cond_3

    .line 126
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getShapes()[Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getShapeFromMasterSlide(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;[Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;)Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-result-object v0

    .line 129
    :cond_3
    if-nez v0, :cond_0

    move-object v0, v1

    .line 132
    goto :goto_0
.end method

.method public getShapeForStyle(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;)Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    .locals 3
    .param p1, "placeHolder"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    .prologue
    const/4 v1, 0x0

    .line 137
    if-nez p1, :cond_1

    move-object v0, v1

    .line 149
    :cond_0
    :goto_0
    return-object v0

    .line 140
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getShapes()[Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;[Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;)Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-result-object v0

    .line 142
    .local v0, "shape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getLstStyle()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    if-nez v2, :cond_3

    .line 143
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getShapes()[Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getShapeFromMasterSlide(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;[Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;)Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-result-object v0

    .line 146
    :cond_3
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getLstStyle()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 147
    goto :goto_0
.end method

.method public getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .locals 5

    .prologue
    .line 214
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->master:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    if-nez v2, :cond_1

    .line 215
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getRelations()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/POIXMLDocumentPart;

    .line 217
    .local v1, "p":Lorg/apache/poi/POIXMLDocumentPart;
    instance-of v2, v1, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    if-eqz v2, :cond_0

    .line 218
    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    .end local v1    # "p":Lorg/apache/poi/POIXMLDocumentPart;
    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->master:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    goto :goto_0

    .line 222
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->master:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    if-nez v2, :cond_2

    .line 223
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "SlideMaster was not found for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 226
    :cond_2
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->master:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    return-object v2
.end method

.method public parseSlideLayout()V
    .locals 7

    .prologue
    .line 61
    iget-boolean v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->parsed:Z

    if-eqz v3, :cond_1

    .line 85
    :cond_0
    :goto_0
    return-void

    .line 64
    :cond_1
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/pptx/XPPTSlideLayoutParser;

    invoke-direct {v2, p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XPPTSlideLayoutParser;-><init>(Ljava/lang/Object;)V

    .line 66
    .local v2, "layoutParser":Lcom/samsung/thumbnail/office/ooxml/pptx/XPPTSlideLayoutParser;
    const/4 v1, 0x0

    .line 70
    .local v1, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 71
    invoke-virtual {v2, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XPPTSlideLayoutParser;->parse(Ljava/io/InputStream;)V

    .line 72
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->parsed:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    if-eqz v1, :cond_0

    .line 79
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 80
    :catch_0
    move-exception v0

    .line 81
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 74
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 75
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_2
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 77
    if-eqz v1, :cond_0

    .line 79
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 80
    :catch_2
    move-exception v0

    .line 81
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 77
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_2

    .line 79
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 82
    :cond_2
    :goto_1
    throw v3

    .line 80
    :catch_3
    move-exception v0

    .line 81
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v4, "DocumentService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setColorMap(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 97
    .local p1, "colorMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->colorMap:Ljava/util/Map;

    .line 98
    return-void
.end method
