.class public Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrRefHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DMLChartStrRefHandler.java"


# instance fields
.field private ptConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;)V
    .locals 2
    .param p1, "ptConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;

    .prologue
    .line 26
    const/16 v0, 0x23

    const-string/jumbo v1, "strRef"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 27
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrRefHandler;->ptConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;

    .line 28
    return-void
.end method

.method private init()V
    .locals 6

    .prologue
    .line 31
    const/4 v5, 0x2

    new-array v2, v5, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 33
    .local v2, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrFHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrFHandler;-><init>()V

    .line 34
    .local v0, "fHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrFHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v5, "f"

    invoke-direct {v1, v5, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 36
    .local v1, "fSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/4 v5, 0x0

    aput-object v1, v2, v5

    .line 38
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrCacheHandler;

    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrRefHandler;->ptConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;

    invoke-direct {v3, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrCacheHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;)V

    .line 40
    .local v3, "strcacheHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrCacheHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v5, "strCache"

    invoke-direct {v4, v5, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 42
    .local v4, "strcacheSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/4 v5, 0x1

    aput-object v4, v2, v5

    .line 44
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrRefHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 45
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    const/16 v1, 0xa

    .line 50
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 51
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrRefHandler;->init()V

    .line 53
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrRefHandler;->ptConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;

    invoke-interface {v0, p1, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;->setRefType(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;I)V

    .line 57
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    if-eqz v0, :cond_1

    .line 58
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrRefHandler;->ptConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;

    invoke-interface {v0, p1, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;->setRefType(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;I)V

    .line 60
    :cond_1
    return-void
.end method
