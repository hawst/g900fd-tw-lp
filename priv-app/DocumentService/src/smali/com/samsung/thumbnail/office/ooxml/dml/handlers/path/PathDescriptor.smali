.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;
.super Ljava/lang/Object;
.source "PathDescriptor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;,
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$Path;
    }
.end annotation


# static fields
.field public static final SEG_ARC_TO:I = 0x5

.field public static final SEG_CLOSE:I = 0x1

.field public static final SEG_CUBIC_BEZ_TO:I = 0x4

.field public static final SEG_LINE_TO:I = 0x2

.field public static final SEG_MOVE_TO:I = 0x0

.field public static final SEG_NOFILL:I = 0x6

.field public static final SEG_NOSTROKE:I = 0x7

.field public static final SEG_QUAD_BEZ_TO:I = 0x3


# instance fields
.field private hasArcTo:Z

.field private height:I

.field private paths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$Path;",
            ">;"
        }
    .end annotation
.end field

.field private width:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->hasArcTo:Z

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->paths:Ljava/util/ArrayList;

    .line 24
    return-void
.end method


# virtual methods
.method public addSegmentstoPath(I[Ljava/lang/String;)V
    .locals 3
    .param p1, "operation"    # I
    .param p2, "arguments"    # [Ljava/lang/String;

    .prologue
    .line 27
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;-><init>()V

    .line 28
    .local v1, "segInfo":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$Path;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$Path;-><init>()V

    .line 29
    .local v0, "curPath":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$Path;
    iput p1, v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;->operation:I

    .line 30
    iput-object p2, v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;->arguments:[Ljava/lang/String;

    .line 31
    iget-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$Path;->segments:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 32
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->paths:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 33
    return-void
.end method

.method public createNewPath(IIZZ)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "fill"    # Z
    .param p4, "stroke"    # Z

    .prologue
    .line 38
    return-void
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->height:I

    return v0
.end method

.method public getPaths()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->paths:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->width:I

    return v0
.end method

.method public hasArcTo()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->hasArcTo:Z

    return v0
.end method

.method public setArcTo(Z)V
    .locals 0
    .param p1, "hasArcTo"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->hasArcTo:Z

    .line 42
    return-void
.end method

.method public setHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->height:I

    .line 50
    return-void
.end method

.method public setWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->width:I

    .line 54
    return-void
.end method
