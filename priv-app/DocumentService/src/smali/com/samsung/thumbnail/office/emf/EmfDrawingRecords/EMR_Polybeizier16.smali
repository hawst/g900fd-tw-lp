.class public Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;
.super Ljava/lang/Object;
.source "EMR_Polybeizier16.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;


# instance fields
.field private Count:I

.field private RecordSize:I

.field private RecordType:I

.field private aPoints:[B

.field arrayOfPoint:[Landroid/graphics/Point;

.field private emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

.field fileContent:[B

.field private leftTopX:I

.field private leftTopY:I

.field private localPath:Landroid/graphics/Path;

.field private offset:I

.field private pointsCount:I

.field private rightBottomX:I

.field private rightBottomY:I


# direct methods
.method public constructor <init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V
    .locals 1
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I
    .param p3, "emrGlobals"    # Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .prologue
    const/4 v0, 0x0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->localPath:Landroid/graphics/Path;

    .line 62
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->arrayOfPoint:[Landroid/graphics/Point;

    .line 79
    iput-object p3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .line 80
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->fileContent:[B

    .line 81
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    .line 82
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 204
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->Count:I

    return v0
.end method

.method public getLeftTopX()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->leftTopX:I

    return v0
.end method

.method public getLeftTopY()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->leftTopY:I

    return v0
.end method

.method public getPath()Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->localPath:Landroid/graphics/Path;

    return-object v0
.end method

.method public getRightBottomX()I
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->rightBottomX:I

    return v0
.end method

.method public getRightBottomY()I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->rightBottomY:I

    return v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->RecordSize:I

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->RecordType:I

    return v0
.end method

.method public getaPoints()[B
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->aPoints:[B

    return-object v0
.end method

.method public prepare()V
    .locals 25

    .prologue
    .line 376
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->getCount()I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->setNumberOfPoints(I)V

    .line 377
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->getaPoints()[B

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->setPoints([B)V

    .line 378
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->pointsCount:I

    move/from16 v17, v0

    .line 379
    .local v17, "i":I
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->localPath:Landroid/graphics/Path;

    .line 380
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->arrayOfPoint:[Landroid/graphics/Point;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->arrayOfPoint:[Landroid/graphics/Point;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 381
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->arrayOfPoint:[Landroid/graphics/Point;

    const/4 v3, 0x0

    aget-object v21, v2, v3

    .line 382
    .local v21, "localPoint1":Landroid/graphics/Point;
    move-object/from16 v0, v21

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v9, v2

    .line 383
    .local v9, "f1":F
    move-object/from16 v0, v21

    iget v2, v0, Landroid/graphics/Point;->y:I

    int-to-float v10, v2

    .line 385
    .local v10, "f2":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->localPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v3, v9, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v4, v10, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 387
    const/16 v18, 0x1

    .line 388
    .local v18, "j":I
    :goto_0
    move/from16 v0, v18

    move/from16 v1, v17

    if-ge v0, v1, :cond_0

    .line 389
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v22, v2, v18

    .line 390
    .local v22, "localPoint2":Landroid/graphics/Point;
    add-int/lit8 v19, v18, 0x1

    .line 391
    .local v19, "k":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v23, v2, v19

    .line 392
    .local v23, "localPoint3":Landroid/graphics/Point;
    add-int/lit8 v20, v18, 0x2

    .line 393
    .local v20, "l":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v24, v2, v20

    .line 394
    .local v24, "localPoint4":Landroid/graphics/Point;
    move-object/from16 v0, v22

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v11, v2

    .line 395
    .local v11, "f3":F
    move-object/from16 v0, v22

    iget v2, v0, Landroid/graphics/Point;->y:I

    int-to-float v12, v2

    .line 396
    .local v12, "f4":F
    move-object/from16 v0, v23

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v13, v2

    .line 397
    .local v13, "f5":F
    move-object/from16 v0, v23

    iget v2, v0, Landroid/graphics/Point;->y:I

    int-to-float v14, v2

    .line 398
    .local v14, "f6":F
    move-object/from16 v0, v24

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v15, v2

    .line 399
    .local v15, "f7":F
    move-object/from16 v0, v24

    iget v2, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v2

    move/from16 v16, v0

    .line 401
    .local v16, "f8":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->localPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v3, v11, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v4, v12, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v5, v13, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v6, v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v6, v14, v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v7, v15, v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v8, v16, v8

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 406
    add-int/lit8 v18, v18, 0x3

    .line 407
    goto :goto_0

    .line 409
    .end local v11    # "f3":F
    .end local v12    # "f4":F
    .end local v13    # "f5":F
    .end local v14    # "f6":F
    .end local v15    # "f7":F
    .end local v16    # "f8":F
    .end local v19    # "k":I
    .end local v20    # "l":I
    .end local v22    # "localPoint2":Landroid/graphics/Point;
    .end local v23    # "localPoint3":Landroid/graphics/Point;
    .end local v24    # "localPoint4":Landroid/graphics/Point;
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->arrayOfPoint:[Landroid/graphics/Point;

    add-int/lit8 v3, v17, -0x1

    aget-object v2, v2, v3

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->arrayOfPoint:[Landroid/graphics/Point;

    add-int/lit8 v4, v17, -0x1

    aget-object v3, v3, v4

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->setCurrentXY(FF)V

    .line 412
    .end local v9    # "f1":F
    .end local v10    # "f2":F
    .end local v18    # "j":I
    .end local v21    # "localPoint1":Landroid/graphics/Point;
    :cond_1
    return-void
.end method

.method public readEmfPolybeizier16()V
    .locals 14

    .prologue
    .line 240
    const/4 v4, 0x0

    .line 242
    .local v4, "i":I
    const/4 v11, 0x4

    new-array v5, v11, [B

    .line 244
    .local v5, "intConvert":[B
    const/4 v4, 0x0

    :goto_0
    const/4 v11, 0x3

    if-gt v4, v11, :cond_0

    .line 245
    rsub-int/lit8 v11, v4, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    add-int/2addr v13, v4

    aget-byte v12, v12, v13

    aput-byte v12, v5, v11

    .line 244
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 246
    :cond_0
    const/4 v11, 0x0

    invoke-static {v5, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v2

    .line 247
    .local v2, "Type":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    add-int/2addr v11, v4

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    .line 248
    invoke-virtual {p0, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->setType(I)V

    .line 250
    const/4 v4, 0x0

    :goto_1
    const/4 v11, 0x3

    if-gt v4, v11, :cond_1

    .line 251
    rsub-int/lit8 v11, v4, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    add-int/2addr v13, v4

    aget-byte v12, v12, v13

    aput-byte v12, v5, v11

    .line 250
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 252
    :cond_1
    const/4 v11, 0x0

    invoke-static {v5, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 253
    .local v1, "Size":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    add-int/2addr v11, v4

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    .line 254
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->setSize(I)V

    .line 256
    const/4 v4, 0x0

    :goto_2
    const/4 v11, 0x3

    if-gt v4, v11, :cond_2

    .line 257
    rsub-int/lit8 v11, v4, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    add-int/2addr v13, v4

    aget-byte v12, v12, v13

    aput-byte v12, v5, v11

    .line 256
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 258
    :cond_2
    const/4 v11, 0x0

    invoke-static {v5, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v6

    .line 259
    .local v6, "leftTopX":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    add-int/2addr v11, v4

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    .line 260
    int-to-float v11, v6

    invoke-static {v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->setLeftTopX(I)V

    .line 262
    const/4 v4, 0x0

    :goto_3
    const/4 v11, 0x3

    if-gt v4, v11, :cond_3

    .line 263
    rsub-int/lit8 v11, v4, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    add-int/2addr v13, v4

    aget-byte v12, v12, v13

    aput-byte v12, v5, v11

    .line 262
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 264
    :cond_3
    const/4 v11, 0x0

    invoke-static {v5, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v7

    .line 265
    .local v7, "leftTopY":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    add-int/2addr v11, v4

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    .line 266
    int-to-float v11, v7

    invoke-static {v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->setLeftTopY(I)V

    .line 268
    const/4 v4, 0x0

    :goto_4
    const/4 v11, 0x3

    if-gt v4, v11, :cond_4

    .line 269
    rsub-int/lit8 v11, v4, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    add-int/2addr v13, v4

    aget-byte v12, v12, v13

    aput-byte v12, v5, v11

    .line 268
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 270
    :cond_4
    const/4 v11, 0x0

    invoke-static {v5, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v9

    .line 271
    .local v9, "rightBottomX":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    add-int/2addr v11, v4

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    .line 272
    int-to-float v11, v9

    invoke-static {v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->setRightBottomX(I)V

    .line 274
    const/4 v4, 0x0

    :goto_5
    const/4 v11, 0x3

    if-gt v4, v11, :cond_5

    .line 275
    rsub-int/lit8 v11, v4, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    add-int/2addr v13, v4

    aget-byte v12, v12, v13

    aput-byte v12, v5, v11

    .line 274
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 276
    :cond_5
    const/4 v11, 0x0

    invoke-static {v5, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v10

    .line 277
    .local v10, "rightBottomY":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    add-int/2addr v11, v4

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    .line 278
    int-to-float v11, v10

    invoke-static {v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->setRightBottomY(I)V

    .line 280
    const/4 v4, 0x0

    :goto_6
    const/4 v11, 0x3

    if-gt v4, v11, :cond_6

    .line 281
    rsub-int/lit8 v11, v4, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    add-int/2addr v13, v4

    aget-byte v12, v12, v13

    aput-byte v12, v5, v11

    .line 280
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 282
    :cond_6
    const/4 v11, 0x0

    invoke-static {v5, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 283
    .local v0, "Count":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    add-int/2addr v11, v4

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    .line 284
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->setCount(I)V

    .line 286
    add-int/lit8 v3, v1, -0x1c

    .line 287
    .local v3, "buffsize":I
    new-array v8, v3, [B

    .line 288
    .local v8, "pointsBuffer":[B
    const/4 v4, 0x0

    :goto_7
    if-ge v4, v3, :cond_7

    .line 289
    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->fileContent:[B

    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    add-int/2addr v12, v4

    aget-byte v11, v11, v12

    aput-byte v11, v8, v4

    .line 288
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 291
    :cond_7
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    add-int/2addr v11, v4

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->offset:I

    .line 292
    invoke-virtual {p0, v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->setaPoints([B)V

    .line 293
    return-void
.end method

.method public render(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 422
    const/4 v2, 0x0

    .line 423
    .local v2, "i":I
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;-><init>()V

    .line 424
    .local v0, "PaintPolyBezier16PropertiesObject":Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 426
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 427
    .local v1, "emrSize":I
    if-lez v1, :cond_1

    .line 428
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 429
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandlePen:I

    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->gethandleNumber()I

    move-result v3

    if-ne v4, v3, :cond_4

    .line 433
    :cond_0
    if-ge v2, v1, :cond_1

    .line 434
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getPenStyle()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/thumbnail/office/emf/Util/toStyleAndroid;->getStyle(I)Landroid/graphics/Paint$Style;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 437
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getColor()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/thumbnail/office/emf/Util/toColorAndroid;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 440
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->getPath()Landroid/graphics/Path;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 445
    :cond_1
    if-eq v2, v1, :cond_2

    if-nez v1, :cond_3

    .line 447
    :cond_2
    const/16 v3, 0x8

    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v3, v4, :cond_5

    .line 455
    :cond_3
    :goto_1
    return-void

    .line 428
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 450
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->getPath()Landroid/graphics/Path;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_1
.end method

.method public setCount(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 214
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->Count:I

    .line 215
    return-void
.end method

.method public setCurrentXY(FF)V
    .locals 2
    .param p1, "currX"    # F
    .param p2, "currY"    # F

    .prologue
    .line 304
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v1, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v1, p1, v1

    iput v1, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->currentX:F

    .line 305
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v1, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v1, p2, v1

    iput v1, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->currentY:F

    .line 306
    return-void
.end method

.method public setLeftTopX(I)V
    .locals 0
    .param p1, "leftTopX"    # I

    .prologue
    .line 138
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->leftTopX:I

    .line 139
    return-void
.end method

.method public setLeftTopY(I)V
    .locals 0
    .param p1, "leftTopY"    # I

    .prologue
    .line 157
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->leftTopY:I

    .line 158
    return-void
.end method

.method public setNumberOfPoints(I)V
    .locals 0
    .param p1, "pointCount"    # I

    .prologue
    .line 324
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->pointsCount:I

    .line 325
    return-void
.end method

.method public setPoints([B)V
    .locals 13
    .param p1, "points"    # [B

    .prologue
    const v12, 0xffff

    const/4 v11, 0x1

    const/4 v10, 0x0

    const v9, 0x8000

    .line 334
    const/4 v2, 0x0

    .line 335
    .local v2, "i":I
    const/4 v4, 0x0

    .line 336
    .local v4, "j":I
    const/4 v5, 0x0

    .line 337
    .local v5, "k":I
    const/4 v6, 0x0

    .line 339
    .local v6, "offset":I
    array-length v7, p1

    shr-int/lit8 v7, v7, 0x2

    new-array v7, v7, [Landroid/graphics/Point;

    iput-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->arrayOfPoint:[Landroid/graphics/Point;

    .line 341
    const/4 v7, 0x2

    new-array v3, v7, [B

    .line 343
    .local v3, "intConvert":[B
    :goto_0
    array-length v7, p1

    if-ge v6, v7, :cond_4

    .line 344
    const/4 v2, 0x0

    :goto_1
    if-gt v2, v11, :cond_0

    .line 345
    rsub-int/lit8 v7, v2, 0x1

    add-int v8, v6, v2

    aget-byte v8, p1, v8

    aput-byte v8, v3, v7

    .line 344
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 346
    :cond_0
    invoke-static {v3, v10}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getSmallInt([BI)I

    move-result v0

    .line 347
    .local v0, "X":I
    add-int/2addr v6, v2

    .line 349
    and-int v7, v0, v9

    if-ne v7, v9, :cond_1

    .line 350
    xor-int/lit8 v7, v0, -0x1

    and-int/2addr v7, v12

    add-int/lit8 v7, v7, 0x1

    mul-int/lit8 v0, v7, -0x1

    .line 353
    :cond_1
    const/4 v4, 0x0

    :goto_2
    if-gt v4, v11, :cond_2

    .line 354
    rsub-int/lit8 v7, v4, 0x1

    add-int v8, v6, v4

    aget-byte v8, p1, v8

    aput-byte v8, v3, v7

    .line 353
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 355
    :cond_2
    invoke-static {v3, v10}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getSmallInt([BI)I

    move-result v1

    .line 357
    .local v1, "Y":I
    and-int v7, v1, v9

    if-ne v7, v9, :cond_3

    .line 358
    xor-int/lit8 v7, v1, -0x1

    and-int/2addr v7, v12

    add-int/lit8 v7, v7, 0x1

    mul-int/lit8 v1, v7, -0x1

    .line 361
    :cond_3
    add-int/2addr v6, v4

    .line 362
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->arrayOfPoint:[Landroid/graphics/Point;

    new-instance v8, Landroid/graphics/Point;

    invoke-direct {v8, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    aput-object v8, v7, v5

    .line 364
    add-int/lit8 v5, v5, 0x1

    .line 365
    goto :goto_0

    .line 367
    .end local v0    # "X":I
    .end local v1    # "Y":I
    :cond_4
    return-void
.end method

.method public setRightBottomX(I)V
    .locals 0
    .param p1, "rightBottomX"    # I

    .prologue
    .line 176
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->rightBottomX:I

    .line 177
    return-void
.end method

.method public setRightBottomY(I)V
    .locals 0
    .param p1, "rightBottomY"    # I

    .prologue
    .line 195
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->rightBottomY:I

    .line 196
    return-void
.end method

.method public setSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 119
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->RecordSize:I

    .line 120
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 100
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->RecordType:I

    .line 101
    return-void
.end method

.method public setaPoints([B)V
    .locals 0
    .param p1, "aPoints"    # [B

    .prologue
    .line 233
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polybeizier16;->aPoints:[B

    .line 234
    return-void
.end method
