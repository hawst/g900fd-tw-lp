.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XDocRPRHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRFontsHandler$IXDocRFontsObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocUnderlineHandler$IUnderlineObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$IXRPrObserver;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrColor;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrHighlightColor;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrSpacing;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrVanish;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrImprint;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrEmoss;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrShadow;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrOutline;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrDStrike;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrStrike;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrCaps;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrSmallCaps;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrShade;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrSize;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrBold;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrVertAlign;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrItalic;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrStyleId;
    }
.end annotation


# instance fields
.field protected charProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

.field private rPrOberver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$IXRPrObserver;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$IXRPrObserver;)V
    .locals 65
    .param p1, "rPrOberver"    # Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$IXRPrObserver;

    .prologue
    .line 38
    const-string/jumbo v64, "rPr"

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 40
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;->rPrOberver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$IXRPrObserver;

    .line 42
    const/16 v64, 0x15

    move/from16 v0, v64

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    move-object/from16 v34, v0

    .line 44
    .local v34, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v54, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrStyleId;

    move-object/from16 v0, v54

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrStyleId;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;)V

    .line 45
    .local v54, "styleId":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrStyleId;
    new-instance v53, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;

    const-string/jumbo v64, "rStyle"

    move-object/from16 v0, v53

    move-object/from16 v1, v64

    move-object/from16 v2, v54

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 47
    .local v53, "styleIDHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;
    new-instance v55, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v64, "rStyle"

    move-object/from16 v0, v55

    move-object/from16 v1, v64

    move-object/from16 v2, v53

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 49
    .local v55, "styleSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v64, 0x0

    aput-object v55, v34, v64

    .line 51
    new-instance v33, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRFontsHandler;

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRFontsHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRFontsHandler$IXDocRFontsObserver;)V

    .line 52
    .local v33, "rFontsHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRFontsHandler;
    new-instance v32, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v64, "rFonts"

    move-object/from16 v0, v32

    move-object/from16 v1, v64

    move-object/from16 v2, v33

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 54
    .local v32, "rFontSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v64, 0x1

    aput-object v32, v34, v64

    .line 56
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrBold;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrBold;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;)V

    .line 57
    .local v3, "bold":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrBold;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;

    const-string/jumbo v64, "b"

    move-object/from16 v0, v64

    invoke-direct {v4, v0, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 59
    .local v4, "boldHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v64, "b"

    move-object/from16 v0, v64

    invoke-direct {v5, v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 61
    .local v5, "boldSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v64, 0x2

    aput-object v5, v34, v64

    .line 63
    new-instance v26, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrItalic;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrItalic;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;)V

    .line 64
    .local v26, "italic":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrItalic;
    new-instance v27, Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;

    const-string/jumbo v64, "i"

    move-object/from16 v0, v27

    move-object/from16 v1, v64

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 66
    .local v27, "italicHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;
    new-instance v28, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v64, "i"

    move-object/from16 v0, v28

    move-object/from16 v1, v64

    move-object/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 68
    .local v28, "italicSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v64, 0x3

    aput-object v28, v34, v64

    .line 73
    new-instance v56, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocUnderlineHandler;

    move-object/from16 v0, v56

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocUnderlineHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocUnderlineHandler$IUnderlineObserver;)V

    .line 74
    .local v56, "underlineHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocUnderlineHandler;
    new-instance v57, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v64, "u"

    move-object/from16 v0, v57

    move-object/from16 v1, v64

    move-object/from16 v2, v56

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 76
    .local v57, "underlineSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v64, 0x4

    aput-object v57, v34, v64

    .line 78
    new-instance v41, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrSize;

    move-object/from16 v0, v41

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrSize;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;)V

    .line 79
    .local v41, "size":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrSize;
    new-instance v42, Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;

    const-string/jumbo v64, "sz"

    move-object/from16 v0, v42

    move-object/from16 v1, v64

    move-object/from16 v2, v41

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 81
    .local v42, "sizeHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;
    new-instance v43, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v64, "sz"

    move-object/from16 v0, v43

    move-object/from16 v1, v64

    move-object/from16 v2, v42

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 83
    .local v43, "sizeSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v64, 0x5

    aput-object v43, v34, v64

    .line 85
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrCaps;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrCaps;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;)V

    .line 86
    .local v8, "caps":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrCaps;
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;

    const-string/jumbo v64, "caps"

    move-object/from16 v0, v64

    invoke-direct {v9, v0, v8}, Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 88
    .local v9, "capsHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v64, "caps"

    move-object/from16 v0, v64

    invoke-direct {v10, v0, v9}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 90
    .local v10, "capsSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v64, 0x6

    aput-object v10, v34, v64

    .line 92
    new-instance v44, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrSmallCaps;

    move-object/from16 v0, v44

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrSmallCaps;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;)V

    .line 93
    .local v44, "smallCaps":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrSmallCaps;
    new-instance v45, Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;

    const-string/jumbo v64, "smallCaps"

    move-object/from16 v0, v45

    move-object/from16 v1, v64

    move-object/from16 v2, v44

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 95
    .local v45, "smallCapsHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;
    new-instance v46, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v64, "smallCaps"

    move-object/from16 v0, v46

    move-object/from16 v1, v64

    move-object/from16 v2, v45

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 97
    .local v46, "smallCapsSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v64, 0x7

    aput-object v46, v34, v64

    .line 99
    new-instance v50, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrStrike;

    move-object/from16 v0, v50

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrStrike;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;)V

    .line 100
    .local v50, "strike":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrStrike;
    new-instance v51, Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;

    const-string/jumbo v64, "strike"

    move-object/from16 v0, v51

    move-object/from16 v1, v64

    move-object/from16 v2, v50

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 102
    .local v51, "strikeHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;
    new-instance v52, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v64, "strike"

    move-object/from16 v0, v52

    move-object/from16 v1, v64

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 104
    .local v52, "strikeSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v64, 0x8

    aput-object v52, v34, v64

    .line 106
    new-instance v14, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrDStrike;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrDStrike;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;)V

    .line 107
    .local v14, "dStrike":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrDStrike;
    new-instance v15, Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;

    const-string/jumbo v64, "dstrike"

    move-object/from16 v0, v64

    invoke-direct {v15, v0, v14}, Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 109
    .local v15, "dStrikeHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;
    new-instance v16, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v64, "dstrike"

    move-object/from16 v0, v16

    move-object/from16 v1, v64

    invoke-direct {v0, v1, v15}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 111
    .local v16, "dStrikeSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v64, 0x9

    aput-object v16, v34, v64

    .line 113
    new-instance v29, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrOutline;

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrOutline;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;)V

    .line 114
    .local v29, "outline":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrOutline;
    new-instance v30, Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;

    const-string/jumbo v64, "outline"

    move-object/from16 v0, v30

    move-object/from16 v1, v64

    move-object/from16 v2, v29

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 116
    .local v30, "outlineHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;
    new-instance v31, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v64, "outline"

    move-object/from16 v0, v31

    move-object/from16 v1, v64

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 118
    .local v31, "outlineSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v64, 0xa

    aput-object v31, v34, v64

    .line 120
    new-instance v38, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrShadow;

    move-object/from16 v0, v38

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrShadow;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;)V

    .line 121
    .local v38, "shadow":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrShadow;
    new-instance v39, Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;

    const-string/jumbo v64, "shadow"

    move-object/from16 v0, v39

    move-object/from16 v1, v64

    move-object/from16 v2, v38

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 123
    .local v39, "shadowHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;
    new-instance v40, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v64, "shadow"

    move-object/from16 v0, v40

    move-object/from16 v1, v64

    move-object/from16 v2, v39

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 125
    .local v40, "shadowSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v64, 0xb

    aput-object v40, v34, v64

    .line 127
    new-instance v17, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrEmoss;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrEmoss;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;)V

    .line 128
    .local v17, "emboss":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrEmoss;
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;

    const-string/jumbo v64, "emboss"

    move-object/from16 v0, v18

    move-object/from16 v1, v64

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 130
    .local v18, "embossHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;
    new-instance v19, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v64, "emboss"

    move-object/from16 v0, v19

    move-object/from16 v1, v64

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 132
    .local v19, "embossSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v64, 0xc

    aput-object v19, v34, v64

    .line 134
    new-instance v23, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrImprint;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrImprint;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;)V

    .line 135
    .local v23, "imprint":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrImprint;
    new-instance v24, Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;

    const-string/jumbo v64, "imprint"

    move-object/from16 v0, v24

    move-object/from16 v1, v64

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 137
    .local v24, "imprintHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;
    new-instance v25, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v64, "imprint"

    move-object/from16 v0, v25

    move-object/from16 v1, v64

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 139
    .local v25, "imprintSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v64, 0xd

    aput-object v25, v34, v64

    .line 141
    new-instance v58, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrVanish;

    move-object/from16 v0, v58

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrVanish;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;)V

    .line 142
    .local v58, "vanish":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrVanish;
    new-instance v59, Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;

    const-string/jumbo v64, "vanish"

    move-object/from16 v0, v59

    move-object/from16 v1, v64

    move-object/from16 v2, v58

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 144
    .local v59, "vanishHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLBooleanHandler;
    new-instance v60, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v64, "vanish"

    move-object/from16 v0, v60

    move-object/from16 v1, v64

    move-object/from16 v2, v59

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 146
    .local v60, "vanishSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v64, 0xe

    aput-object v60, v34, v64

    .line 149
    new-instance v48, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrSpacing;

    move-object/from16 v0, v48

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrSpacing;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;)V

    .line 150
    .local v48, "spacing":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrSpacing;
    new-instance v49, Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;

    const-string/jumbo v64, "spacing"

    move-object/from16 v0, v49

    move-object/from16 v1, v64

    move-object/from16 v2, v48

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 152
    .local v49, "spacingHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;
    new-instance v47, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v64, "spacing"

    move-object/from16 v0, v47

    move-object/from16 v1, v64

    move-object/from16 v2, v49

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 154
    .local v47, "spaceSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v64, 0xf

    aput-object v47, v34, v64

    .line 156
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrColor;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrColor;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;)V

    .line 157
    .local v11, "color":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrColor;
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocColorHandler;

    invoke-direct {v12, v11}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocColorHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 158
    .local v12, "colorHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocColorHandler;
    new-instance v13, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v64, "color"

    move-object/from16 v0, v64

    invoke-direct {v13, v0, v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 160
    .local v13, "colorSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v64, 0x10

    aput-object v13, v34, v64

    .line 162
    new-instance v20, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrHighlightColor;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrHighlightColor;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;)V

    .line 163
    .local v20, "highlight":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrHighlightColor;
    new-instance v21, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocHighlightHandler;

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocHighlightHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 165
    .local v21, "highlightHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocHighlightHandler;
    new-instance v22, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v64, "highlight"

    move-object/from16 v0, v22

    move-object/from16 v1, v64

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 167
    .local v22, "highlightSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v64, 0x11

    aput-object v22, v34, v64

    .line 169
    new-instance v35, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrShade;

    move-object/from16 v0, v35

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrShade;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;)V

    .line 170
    .local v35, "shade":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrShade;
    new-instance v36, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocShadeHandler;

    move-object/from16 v0, v36

    move-object/from16 v1, v35

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocShadeHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 171
    .local v36, "shadeHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocShadeHandler;
    new-instance v37, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v64, "shd"

    move-object/from16 v0, v37

    move-object/from16 v1, v64

    move-object/from16 v2, v36

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 173
    .local v37, "shadeSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v64, 0x12

    aput-object v37, v34, v64

    .line 175
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;

    const-string/jumbo v64, "bdr"

    move-object/from16 v0, v64

    move-object/from16 v1, p0

    invoke-direct {v6, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;)V

    .line 177
    .local v6, "borderHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v64, "bdr"

    move-object/from16 v0, v64

    invoke-direct {v7, v0, v6}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 179
    .local v7, "borderSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v64, 0x13

    aput-object v7, v34, v64

    .line 181
    new-instance v61, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrVertAlign;

    move-object/from16 v0, v61

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrVertAlign;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;)V

    .line 182
    .local v61, "vertAlign":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$XRPrVertAlign;
    new-instance v62, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;

    const-string/jumbo v64, "vertAlign"

    move-object/from16 v0, v62

    move-object/from16 v1, v64

    move-object/from16 v2, v61

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 184
    .local v62, "vertAlignHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;
    new-instance v63, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v64, "vertAlign"

    move-object/from16 v0, v63

    move-object/from16 v1, v64

    move-object/from16 v2, v62

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 186
    .local v63, "vertAlignSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v64, 0x14

    aput-object v63, v34, v64

    .line 188
    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 189
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 199
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 200
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;->rPrOberver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$IXRPrObserver;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;->charProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-interface {v0, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$IXRPrObserver;->onRPrFinsihed(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V

    .line 201
    return-void
.end method

.method public setBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V
    .locals 1
    .param p1, "borderType"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    .param p2, "border"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .prologue
    .line 435
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;->charProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    .line 436
    return-void
.end method

.method public setFontName(Ljava/lang/String;)V
    .locals 1
    .param p1, "fontName"    # Ljava/lang/String;

    .prologue
    .line 430
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;->charProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setFontName(Ljava/lang/String;)V

    .line 431
    return-void
.end method

.method public setThemeFont(Ljava/lang/String;)V
    .locals 1
    .param p1, "themeFont"    # Ljava/lang/String;

    .prologue
    .line 425
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;->charProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setAsciiThemeFontName(Ljava/lang/String;)V

    .line 426
    return-void
.end method

.method public setUnderline(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;)V
    .locals 1
    .param p1, "underline"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    .prologue
    .line 227
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;->charProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setUnderlineProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;)V

    .line 228
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 194
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;->charProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .line 195
    return-void
.end method
