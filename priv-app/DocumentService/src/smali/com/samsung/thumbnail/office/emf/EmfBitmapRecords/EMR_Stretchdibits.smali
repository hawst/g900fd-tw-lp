.class public Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;
.super Ljava/lang/Object;
.source "EMR_Stretchdibits.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;


# static fields
.field private static final BitmapHeaderSize:I = 0xe


# instance fields
.field private BitBltRasterOperation:I

.field BitmapBuffer:[B

.field private RecordSize:I

.field private RecordType:I

.field private UsageSrc:I

.field bimp:Landroid/graphics/Bitmap;

.field private cbBitsSrc:I

.field private cbBmiSrc:I

.field private cxDest:I

.field private cxSrc:I

.field private cyDest:I

.field private cySrc:I

.field fileContent:[B

.field private leftTopX:I

.field private leftTopY:I

.field private offBitsSrc:I

.field private offBmiSrc:I

.field private offset:I

.field private rightBottomX:I

.field private rightBottomY:I

.field private xDest:I

.field private xSrc:I

.field private yDest:I

.field private ySrc:I


# direct methods
.method public constructor <init>([BI)V
    .locals 1
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->bimp:Landroid/graphics/Bitmap;

    .line 114
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->fileContent:[B

    .line 115
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    .line 116
    return-void
.end method


# virtual methods
.method public createBmpFromByteArray()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x4

    const/4 v9, 0x0

    .line 669
    const/4 v3, 0x0

    .line 670
    .local v3, "i":I
    const/4 v6, 0x0

    .line 673
    .local v6, "offsetBmp":I
    iget v7, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->RecordSize:I

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->getOffBmiSrc()I

    move-result v8

    sub-int/2addr v7, v8

    add-int/lit8 v7, v7, 0xe

    new-array v1, v7, [B

    .line 675
    .local v1, "bmpHeader":[B
    iget v7, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->RecordSize:I

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->getOffBmiSrc()I

    move-result v8

    sub-int/2addr v7, v8

    add-int/lit8 v2, v7, 0xe

    .line 676
    .local v2, "buffsize":I
    new-array v0, v2, [B

    .line 678
    .local v0, "bitmapBuffer":[B
    const/16 v7, 0x424d

    invoke-static {v7}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->shortToByteArray(I)[B

    move-result-object v5

    .line 683
    .local v5, "littleEndianShort":[B
    aget-byte v7, v5, v9

    aput-byte v7, v1, v9

    .line 684
    aget-byte v7, v5, v11

    aput-byte v7, v1, v11

    .line 685
    add-int/lit8 v6, v6, 0x2

    .line 687
    invoke-static {v2}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->intToByteArray(I)[B

    move-result-object v4

    .line 688
    .local v4, "littleEndianInt":[B
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v10, :cond_0

    .line 689
    add-int/lit8 v7, v3, 0x2

    rsub-int/lit8 v8, v3, 0x3

    aget-byte v8, v4, v8

    aput-byte v8, v1, v7

    .line 688
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 690
    :cond_0
    add-int/lit8 v6, v3, 0x2

    .line 692
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v10, :cond_1

    .line 693
    add-int v7, v3, v6

    aput-byte v9, v1, v7

    .line 692
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 694
    :cond_1
    add-int/2addr v6, v3

    .line 696
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->getCbBmiSrc()I

    move-result v7

    add-int/lit8 v7, v7, 0xe

    invoke-static {v7}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->intToByteArray(I)[B

    move-result-object v4

    .line 701
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v10, :cond_2

    .line 702
    add-int v7, v3, v6

    rsub-int/lit8 v8, v3, 0x3

    aget-byte v8, v4, v8

    aput-byte v8, v1, v7

    .line 701
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 703
    :cond_2
    add-int/2addr v6, v3

    .line 705
    const/4 v3, 0x0

    :goto_3
    const/16 v7, 0xe

    if-ge v3, v7, :cond_3

    .line 706
    aget-byte v7, v1, v3

    aput-byte v7, v0, v3

    .line 705
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 708
    :cond_3
    const/4 v3, 0x0

    :goto_4
    add-int/lit8 v7, v2, -0xe

    if-ge v3, v7, :cond_4

    .line 709
    add-int v7, v3, v6

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->fileContent:[B

    iget v9, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    add-int/2addr v9, v3

    aget-byte v8, v8, v9

    aput-byte v8, v0, v7

    .line 708
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 711
    :cond_4
    iget v7, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    add-int/2addr v7, v3

    iput v7, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    .line 712
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->setBitmapBuffer([B)V

    .line 713
    return-void
.end method

.method public getBitBltRasterOperation()I
    .locals 1

    .prologue
    .line 447
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->BitBltRasterOperation:I

    return v0
.end method

.method public getBitmapBuffer()[B
    .locals 1

    .prologue
    .line 504
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->BitmapBuffer:[B

    return-object v0
.end method

.method public getBitmapHeaderSize()I
    .locals 1

    .prologue
    .line 523
    const/16 v0, 0xe

    return v0
.end method

.method public getCbBitsSrc()I
    .locals 1

    .prologue
    .line 409
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->cbBitsSrc:I

    return v0
.end method

.method public getCbBmiSrc()I
    .locals 1

    .prologue
    .line 371
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->cbBmiSrc:I

    return v0
.end method

.method public getCxDest()I
    .locals 1

    .prologue
    .line 466
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->cxDest:I

    return v0
.end method

.method public getCxSrc()I
    .locals 1

    .prologue
    .line 314
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->cxSrc:I

    return v0
.end method

.method public getCyDest()I
    .locals 1

    .prologue
    .line 485
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->cyDest:I

    return v0
.end method

.method public getCySrc()I
    .locals 1

    .prologue
    .line 333
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->cySrc:I

    return v0
.end method

.method public getLeftTopX()I
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->leftTopX:I

    return v0
.end method

.method public getLeftTopY()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->leftTopY:I

    return v0
.end method

.method public getOffBitsSrc()I
    .locals 1

    .prologue
    .line 390
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offBitsSrc:I

    return v0
.end method

.method public getOffBmiSrc()I
    .locals 1

    .prologue
    .line 352
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offBmiSrc:I

    return v0
.end method

.method public getRightBottomX()I
    .locals 1

    .prologue
    .line 200
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->rightBottomX:I

    return v0
.end method

.method public getRightBottomY()I
    .locals 1

    .prologue
    .line 219
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->rightBottomY:I

    return v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->RecordSize:I

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->RecordType:I

    return v0
.end method

.method public getUsageSrc()I
    .locals 1

    .prologue
    .line 428
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->UsageSrc:I

    return v0
.end method

.method public getxDest()I
    .locals 1

    .prologue
    .line 238
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->xDest:I

    return v0
.end method

.method public getxSrc()I
    .locals 1

    .prologue
    .line 276
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->xSrc:I

    return v0
.end method

.method public getyDest()I
    .locals 1

    .prologue
    .line 257
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->yDest:I

    return v0
.end method

.method public getySrc()I
    .locals 1

    .prologue
    .line 295
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->ySrc:I

    return v0
.end method

.method public prepare()V
    .locals 4

    .prologue
    .line 721
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->getBitmapBuffer()[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->getSize()I

    move-result v2

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->getOffBmiSrc()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->getBitmapHeaderSize()I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->bimp:Landroid/graphics/Bitmap;

    .line 723
    return-void
.end method

.method public readEmfStretchdibits()V
    .locals 28

    .prologue
    .line 530
    const/4 v13, 0x0

    .line 532
    .local v13, "i":I
    const/16 v25, 0x4

    move/from16 v0, v25

    new-array v14, v0, [B

    .line 534
    .local v14, "intConvert":[B
    const/4 v13, 0x0

    :goto_0
    const/16 v25, 0x3

    move/from16 v0, v25

    if-gt v13, v0, :cond_0

    .line 535
    rsub-int/lit8 v25, v13, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->fileContent:[B

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v27, v0

    add-int v27, v27, v13

    aget-byte v26, v26, v27

    aput-byte v26, v14, v25

    .line 534
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 536
    :cond_0
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-static {v14, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v4

    .line 537
    .local v4, "Type":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v25, v0

    add-int v25, v25, v13

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    .line 538
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->setType(I)V

    .line 540
    const/4 v13, 0x0

    :goto_1
    const/16 v25, 0x3

    move/from16 v0, v25

    if-gt v13, v0, :cond_1

    .line 541
    rsub-int/lit8 v25, v13, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->fileContent:[B

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v27, v0

    add-int v27, v27, v13

    aget-byte v26, v26, v27

    aput-byte v26, v14, v25

    .line 540
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 542
    :cond_1
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-static {v14, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v3

    .line 543
    .local v3, "Size":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v25, v0

    add-int v25, v25, v13

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    .line 544
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->setSize(I)V

    .line 546
    const/4 v13, 0x0

    :goto_2
    const/16 v25, 0x3

    move/from16 v0, v25

    if-gt v13, v0, :cond_2

    .line 547
    rsub-int/lit8 v25, v13, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->fileContent:[B

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v27, v0

    add-int v27, v27, v13

    aget-byte v26, v26, v27

    aput-byte v26, v14, v25

    .line 546
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 548
    :cond_2
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-static {v14, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v15

    .line 549
    .local v15, "leftTopX":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v25, v0

    add-int v25, v25, v13

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    .line 550
    int-to-float v0, v15

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->setLeftTopX(I)V

    .line 552
    const/4 v13, 0x0

    :goto_3
    const/16 v25, 0x3

    move/from16 v0, v25

    if-gt v13, v0, :cond_3

    .line 553
    rsub-int/lit8 v25, v13, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->fileContent:[B

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v27, v0

    add-int v27, v27, v13

    aget-byte v26, v26, v27

    aput-byte v26, v14, v25

    .line 552
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    .line 554
    :cond_3
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-static {v14, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v16

    .line 555
    .local v16, "leftTopY":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v25, v0

    add-int v25, v25, v13

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    .line 556
    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->setLeftTopY(I)V

    .line 558
    const/4 v13, 0x0

    :goto_4
    const/16 v25, 0x3

    move/from16 v0, v25

    if-gt v13, v0, :cond_4

    .line 559
    rsub-int/lit8 v25, v13, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->fileContent:[B

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v27, v0

    add-int v27, v27, v13

    aget-byte v26, v26, v27

    aput-byte v26, v14, v25

    .line 558
    add-int/lit8 v13, v13, 0x1

    goto :goto_4

    .line 560
    :cond_4
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-static {v14, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v19

    .line 561
    .local v19, "rightBottomX":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v25, v0

    add-int v25, v25, v13

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    .line 562
    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->setRightBottomX(I)V

    .line 564
    const/4 v13, 0x0

    :goto_5
    const/16 v25, 0x3

    move/from16 v0, v25

    if-gt v13, v0, :cond_5

    .line 565
    rsub-int/lit8 v25, v13, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->fileContent:[B

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v27, v0

    add-int v27, v27, v13

    aget-byte v26, v26, v27

    aput-byte v26, v14, v25

    .line 564
    add-int/lit8 v13, v13, 0x1

    goto :goto_5

    .line 566
    :cond_5
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-static {v14, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v20

    .line 567
    .local v20, "rightBottomY":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v25, v0

    add-int v25, v25, v13

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    .line 568
    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->setRightBottomY(I)V

    .line 570
    const/4 v13, 0x0

    :goto_6
    const/16 v25, 0x3

    move/from16 v0, v25

    if-gt v13, v0, :cond_6

    .line 571
    rsub-int/lit8 v25, v13, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->fileContent:[B

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v27, v0

    add-int v27, v27, v13

    aget-byte v26, v26, v27

    aput-byte v26, v14, v25

    .line 570
    add-int/lit8 v13, v13, 0x1

    goto :goto_6

    .line 572
    :cond_6
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-static {v14, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v21

    .line 573
    .local v21, "xDest":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v25, v0

    add-int v25, v25, v13

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    .line 574
    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->setxDest(I)V

    .line 576
    const/4 v13, 0x0

    :goto_7
    const/16 v25, 0x3

    move/from16 v0, v25

    if-gt v13, v0, :cond_7

    .line 577
    rsub-int/lit8 v25, v13, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->fileContent:[B

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v27, v0

    add-int v27, v27, v13

    aget-byte v26, v26, v27

    aput-byte v26, v14, v25

    .line 576
    add-int/lit8 v13, v13, 0x1

    goto :goto_7

    .line 578
    :cond_7
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-static {v14, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v23

    .line 579
    .local v23, "yDest":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v25, v0

    add-int v25, v25, v13

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    .line 580
    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->setyDest(I)V

    .line 582
    const/4 v13, 0x0

    :goto_8
    const/16 v25, 0x3

    move/from16 v0, v25

    if-gt v13, v0, :cond_8

    .line 583
    rsub-int/lit8 v25, v13, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->fileContent:[B

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v27, v0

    add-int v27, v27, v13

    aget-byte v26, v26, v27

    aput-byte v26, v14, v25

    .line 582
    add-int/lit8 v13, v13, 0x1

    goto :goto_8

    .line 584
    :cond_8
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-static {v14, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v22

    .line 585
    .local v22, "xSrc":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v25, v0

    add-int v25, v25, v13

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    .line 586
    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->setxSrc(I)V

    .line 588
    const/4 v13, 0x0

    :goto_9
    const/16 v25, 0x3

    move/from16 v0, v25

    if-gt v13, v0, :cond_9

    .line 589
    rsub-int/lit8 v25, v13, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->fileContent:[B

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v27, v0

    add-int v27, v27, v13

    aget-byte v26, v26, v27

    aput-byte v26, v14, v25

    .line 588
    add-int/lit8 v13, v13, 0x1

    goto :goto_9

    .line 590
    :cond_9
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-static {v14, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v24

    .line 591
    .local v24, "ySrc":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v25, v0

    add-int v25, v25, v13

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    .line 592
    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->setySrc(I)V

    .line 594
    const/4 v13, 0x0

    :goto_a
    const/16 v25, 0x3

    move/from16 v0, v25

    if-gt v13, v0, :cond_a

    .line 595
    rsub-int/lit8 v25, v13, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->fileContent:[B

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v27, v0

    add-int v27, v27, v13

    aget-byte v26, v26, v27

    aput-byte v26, v14, v25

    .line 594
    add-int/lit8 v13, v13, 0x1

    goto :goto_a

    .line 596
    :cond_a
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-static {v14, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v9

    .line 597
    .local v9, "cxSrc":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v25, v0

    add-int v25, v25, v13

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    .line 598
    int-to-float v0, v9

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->setCxSrc(I)V

    .line 600
    const/4 v13, 0x0

    :goto_b
    const/16 v25, 0x3

    move/from16 v0, v25

    if-gt v13, v0, :cond_b

    .line 601
    rsub-int/lit8 v25, v13, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->fileContent:[B

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v27, v0

    add-int v27, v27, v13

    aget-byte v26, v26, v27

    aput-byte v26, v14, v25

    .line 600
    add-int/lit8 v13, v13, 0x1

    goto :goto_b

    .line 602
    :cond_b
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-static {v14, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v11

    .line 603
    .local v11, "cySrc":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v25, v0

    add-int v25, v25, v13

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    .line 604
    int-to-float v0, v11

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->setCySrc(I)V

    .line 606
    const/4 v13, 0x0

    :goto_c
    const/16 v25, 0x3

    move/from16 v0, v25

    if-gt v13, v0, :cond_c

    .line 607
    rsub-int/lit8 v25, v13, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->fileContent:[B

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v27, v0

    add-int v27, v27, v13

    aget-byte v26, v26, v27

    aput-byte v26, v14, v25

    .line 606
    add-int/lit8 v13, v13, 0x1

    goto :goto_c

    .line 608
    :cond_c
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-static {v14, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v18

    .line 609
    .local v18, "offBmiSrc":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v25, v0

    add-int v25, v25, v13

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    .line 610
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->setOffBmiSrc(I)V

    .line 612
    const/4 v13, 0x0

    :goto_d
    const/16 v25, 0x3

    move/from16 v0, v25

    if-gt v13, v0, :cond_d

    .line 613
    rsub-int/lit8 v25, v13, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->fileContent:[B

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v27, v0

    add-int v27, v27, v13

    aget-byte v26, v26, v27

    aput-byte v26, v14, v25

    .line 612
    add-int/lit8 v13, v13, 0x1

    goto :goto_d

    .line 614
    :cond_d
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-static {v14, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v7

    .line 615
    .local v7, "cbBmiSrc":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v25, v0

    add-int v25, v25, v13

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    .line 616
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->setCbBmiSrc(I)V

    .line 618
    const/4 v13, 0x0

    :goto_e
    const/16 v25, 0x3

    move/from16 v0, v25

    if-gt v13, v0, :cond_e

    .line 619
    rsub-int/lit8 v25, v13, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->fileContent:[B

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v27, v0

    add-int v27, v27, v13

    aget-byte v26, v26, v27

    aput-byte v26, v14, v25

    .line 618
    add-int/lit8 v13, v13, 0x1

    goto :goto_e

    .line 620
    :cond_e
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-static {v14, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v17

    .line 621
    .local v17, "offBitsSrc":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v25, v0

    add-int v25, v25, v13

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    .line 622
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->setOffBitsSrc(I)V

    .line 624
    const/4 v13, 0x0

    :goto_f
    const/16 v25, 0x3

    move/from16 v0, v25

    if-gt v13, v0, :cond_f

    .line 625
    rsub-int/lit8 v25, v13, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->fileContent:[B

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v27, v0

    add-int v27, v27, v13

    aget-byte v26, v26, v27

    aput-byte v26, v14, v25

    .line 624
    add-int/lit8 v13, v13, 0x1

    goto :goto_f

    .line 626
    :cond_f
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-static {v14, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v6

    .line 627
    .local v6, "cbBitsSrc":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v25, v0

    add-int v25, v25, v13

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    .line 628
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->setCbBitsSrc(I)V

    .line 630
    const/4 v13, 0x0

    :goto_10
    const/16 v25, 0x3

    move/from16 v0, v25

    if-gt v13, v0, :cond_10

    .line 631
    rsub-int/lit8 v25, v13, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->fileContent:[B

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v27, v0

    add-int v27, v27, v13

    aget-byte v26, v26, v27

    aput-byte v26, v14, v25

    .line 630
    add-int/lit8 v13, v13, 0x1

    goto :goto_10

    .line 632
    :cond_10
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-static {v14, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v5

    .line 633
    .local v5, "UsageSrc":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v25, v0

    add-int v25, v25, v13

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    .line 634
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->setUsageSrc(I)V

    .line 636
    const/4 v13, 0x0

    :goto_11
    const/16 v25, 0x3

    move/from16 v0, v25

    if-gt v13, v0, :cond_11

    .line 637
    rsub-int/lit8 v25, v13, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->fileContent:[B

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v27, v0

    add-int v27, v27, v13

    aget-byte v26, v26, v27

    aput-byte v26, v14, v25

    .line 636
    add-int/lit8 v13, v13, 0x1

    goto :goto_11

    .line 638
    :cond_11
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-static {v14, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v2

    .line 639
    .local v2, "BitBltRasterOperation":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v25, v0

    add-int v25, v25, v13

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    .line 640
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->setBitBltRasterOperation(I)V

    .line 642
    const/4 v13, 0x0

    :goto_12
    const/16 v25, 0x3

    move/from16 v0, v25

    if-gt v13, v0, :cond_12

    .line 643
    rsub-int/lit8 v25, v13, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->fileContent:[B

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v27, v0

    add-int v27, v27, v13

    aget-byte v26, v26, v27

    aput-byte v26, v14, v25

    .line 642
    add-int/lit8 v13, v13, 0x1

    goto :goto_12

    .line 644
    :cond_12
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-static {v14, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v8

    .line 645
    .local v8, "cxDest":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v25, v0

    add-int v25, v25, v13

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    .line 646
    int-to-float v0, v8

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->setCxDest(I)V

    .line 648
    const/4 v13, 0x0

    :goto_13
    const/16 v25, 0x3

    move/from16 v0, v25

    if-gt v13, v0, :cond_13

    .line 649
    rsub-int/lit8 v25, v13, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->fileContent:[B

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v27, v0

    add-int v27, v27, v13

    aget-byte v26, v26, v27

    aput-byte v26, v14, v25

    .line 648
    add-int/lit8 v13, v13, 0x1

    goto :goto_13

    .line 650
    :cond_13
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-static {v14, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v10

    .line 651
    .local v10, "cyDest":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    move/from16 v25, v0

    add-int v25, v25, v13

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offset:I

    .line 652
    int-to-float v0, v10

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->setCyDest(I)V

    .line 655
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->createBmpFromByteArray()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 659
    :goto_14
    return-void

    .line 656
    :catch_0
    move-exception v12

    .line 657
    .local v12, "e":Ljava/io/IOException;
    const-string/jumbo v25, ""

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v27, "Exception: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual {v12}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_14
.end method

.method public render(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    .line 733
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->bimp:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 734
    return-void
.end method

.method public setBitBltRasterOperation(I)V
    .locals 0
    .param p1, "bitBltRasterOperation"    # I

    .prologue
    .line 457
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->BitBltRasterOperation:I

    .line 458
    return-void
.end method

.method public setBitmapBuffer([B)V
    .locals 0
    .param p1, "bitmapBuffer"    # [B

    .prologue
    .line 514
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->BitmapBuffer:[B

    .line 515
    return-void
.end method

.method public setCbBitsSrc(I)V
    .locals 0
    .param p1, "cbBitsSrc"    # I

    .prologue
    .line 419
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->cbBitsSrc:I

    .line 420
    return-void
.end method

.method public setCbBmiSrc(I)V
    .locals 0
    .param p1, "cbBmiSrc"    # I

    .prologue
    .line 381
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->cbBmiSrc:I

    .line 382
    return-void
.end method

.method public setCxDest(I)V
    .locals 0
    .param p1, "cxDest"    # I

    .prologue
    .line 476
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->cxDest:I

    .line 477
    return-void
.end method

.method public setCxSrc(I)V
    .locals 0
    .param p1, "cxSrc"    # I

    .prologue
    .line 324
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->cxSrc:I

    .line 325
    return-void
.end method

.method public setCyDest(I)V
    .locals 0
    .param p1, "cyDest"    # I

    .prologue
    .line 495
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->cyDest:I

    .line 496
    return-void
.end method

.method public setCySrc(I)V
    .locals 0
    .param p1, "cySrc"    # I

    .prologue
    .line 343
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->cySrc:I

    .line 344
    return-void
.end method

.method public setLeftTopX(I)V
    .locals 0
    .param p1, "leftTopX"    # I

    .prologue
    .line 172
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->leftTopX:I

    .line 173
    return-void
.end method

.method public setLeftTopY(I)V
    .locals 0
    .param p1, "leftTopY"    # I

    .prologue
    .line 191
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->leftTopY:I

    .line 192
    return-void
.end method

.method public setOffBitsSrc(I)V
    .locals 0
    .param p1, "offBitsSrc"    # I

    .prologue
    .line 400
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offBitsSrc:I

    .line 401
    return-void
.end method

.method public setOffBmiSrc(I)V
    .locals 0
    .param p1, "offBmiSrc"    # I

    .prologue
    .line 362
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->offBmiSrc:I

    .line 363
    return-void
.end method

.method public setRightBottomX(I)V
    .locals 0
    .param p1, "rightBottomX"    # I

    .prologue
    .line 210
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->rightBottomX:I

    .line 211
    return-void
.end method

.method public setRightBottomY(I)V
    .locals 0
    .param p1, "rightBottomY"    # I

    .prologue
    .line 229
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->rightBottomY:I

    .line 230
    return-void
.end method

.method public setSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 153
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->RecordSize:I

    .line 154
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 134
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->RecordType:I

    .line 135
    return-void
.end method

.method public setUsageSrc(I)V
    .locals 0
    .param p1, "usageSrc"    # I

    .prologue
    .line 438
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->UsageSrc:I

    .line 439
    return-void
.end method

.method public setxDest(I)V
    .locals 0
    .param p1, "xDest"    # I

    .prologue
    .line 248
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->xDest:I

    .line 249
    return-void
.end method

.method public setxSrc(I)V
    .locals 0
    .param p1, "xSrc"    # I

    .prologue
    .line 286
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->xSrc:I

    .line 287
    return-void
.end method

.method public setyDest(I)V
    .locals 0
    .param p1, "yDest"    # I

    .prologue
    .line 267
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->yDest:I

    .line 268
    return-void
.end method

.method public setySrc(I)V
    .locals 0
    .param p1, "ySrc"    # I

    .prologue
    .line 305
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfBitmapRecords/EMR_Stretchdibits;->ySrc:I

    .line 306
    return-void
.end method
