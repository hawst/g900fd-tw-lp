.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/FillFGColorHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "FillFGColorHandler.java"


# instance fields
.field private patternFill:Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;

.field private stop:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;)V
    .locals 1
    .param p1, "patternFill"    # Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;

    .prologue
    .line 18
    const-string/jumbo v0, "fgColor"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 19
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/FillFGColorHandler;->patternFill:Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;

    .line 20
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;)V
    .locals 0
    .param p1, "elementName"    # Ljava/lang/String;
    .param p2, "stop"    # Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 30
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/FillFGColorHandler;->stop:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;

    .line 31
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 6
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 36
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 37
    .local v0, "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 39
    const-string/jumbo v5, "theme"

    invoke-interface {p3, v5}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 40
    .local v3, "theme":Ljava/lang/String;
    const-string/jumbo v5, "rgb"

    invoke-interface {p3, v5}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 41
    .local v2, "rgb":Ljava/lang/String;
    const-string/jumbo v5, "tint"

    invoke-interface {p3, v5}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 46
    .local v4, "tint":Ljava/lang/String;
    const-string/jumbo v5, "indexed"

    invoke-interface {p3, v5}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 49
    .local v1, "indexedColor":Ljava/lang/String;
    if-nez v2, :cond_0

    if-nez v3, :cond_0

    .line 50
    if-eqz v1, :cond_0

    .line 51
    invoke-static {v1}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->getIndexedColorHexCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 55
    :cond_0
    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setThemeColor(Ljava/lang/String;)V

    .line 56
    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 57
    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setTint(Ljava/lang/String;)V

    .line 58
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/FillFGColorHandler;->stop:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;

    if-eqz v5, :cond_1

    .line 59
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/FillFGColorHandler;->stop:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;

    invoke-virtual {v5, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;->setColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    .line 62
    :cond_1
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/FillFGColorHandler;->patternFill:Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;

    if-eqz v5, :cond_2

    .line 63
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/FillFGColorHandler;->patternFill:Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;

    invoke-virtual {v5, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;->setFGColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    .line 65
    :cond_2
    return-void
.end method
