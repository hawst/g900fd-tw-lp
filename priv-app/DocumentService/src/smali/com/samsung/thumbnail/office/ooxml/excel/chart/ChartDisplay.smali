.class public Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;
.super Ljava/lang/Object;
.source "ChartDisplay.java"


# instance fields
.field private chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

.field private chartclr:[Ljava/lang/String;

.field private elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

.field private mMaxDataXValue:D

.field private mMaxDataYValue:D

.field private mMinDataXValue:D

.field private mMinDataYValue:D

.field private matchseries:Z

.field private seriesoneclrchange:I

.field private seriesstrcount:I


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 4
    .param p1, "elementCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    const/4 v3, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-boolean v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->matchseries:Z

    .line 46
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "#4169e1"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string/jumbo v2, "#a52a2a"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "#6b8e23"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "#8a2be2"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "#00bfff"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "#cd853f"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "#87cefa"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "#b8860b"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "#bdb76b"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "#9370db"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartclr:[Ljava/lang/String;

    .line 49
    iput v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    .line 50
    iput v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesoneclrchange:I

    .line 60
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .line 61
    return-void
.end method

.method private getAreaChartRenderer(IIILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 8
    .param p1, "i"    # I
    .param p2, "j"    # I
    .param p3, "numcnt"    # I
    .param p4, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    .prologue
    .line 629
    invoke-virtual {p4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getOrderVal()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 630
    .local v2, "seriescount":I
    new-instance v1, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v1}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 631
    .local v1, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v4, 0x41700000    # 15.0f

    invoke-virtual {v1, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 632
    const/high16 v4, 0x41a00000    # 20.0f

    invoke-virtual {v1, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 633
    const/high16 v4, 0x41200000    # 10.0f

    invoke-virtual {v1, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 634
    const/high16 v4, 0x41200000    # 10.0f

    invoke-virtual {v1, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 635
    const/high16 v4, 0x40a00000    # 5.0f

    invoke-virtual {v1, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setPointSize(F)V

    .line 636
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    const-wide/16 v6, 0x0

    cmpg-double v4, v4, v6

    if-gez v4, :cond_0

    .line 637
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 638
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 639
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 641
    :cond_0
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 643
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    const-wide/16 v6, 0x0

    cmpg-double v4, v4, v6

    if-gez v4, :cond_1

    .line 644
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 645
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 646
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 648
    :cond_1
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMax(D)V

    .line 650
    const/4 v4, 0x4

    new-array v4, v4, [I

    fill-array-data v4, :array_0

    invoke-virtual {v1, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 652
    const/4 v3, 0x0

    .local v3, "x":I
    :goto_0
    if-ge v3, v2, :cond_2

    .line 653
    new-instance v0, Lorg/achartengine/renderer/XYSeriesRenderer;

    invoke-direct {v0}, Lorg/achartengine/renderer/XYSeriesRenderer;-><init>()V

    .line 654
    .local v0, "r":Lorg/achartengine/renderer/XYSeriesRenderer;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartclr:[Ljava/lang/String;

    sub-int v5, v2, v3

    add-int/lit8 v5, v5, -0x1

    rem-int/lit8 v5, v5, 0xa

    aget-object v4, v4, v5

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v4}, Lorg/achartengine/renderer/XYSeriesRenderer;->setColor(I)V

    .line 655
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lorg/achartengine/renderer/XYSeriesRenderer;->setFillBelowLine(Z)V

    .line 656
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartclr:[Ljava/lang/String;

    sub-int v5, v2, v3

    add-int/lit8 v5, v5, -0x1

    aget-object v4, v4, v5

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v4}, Lorg/achartengine/renderer/XYSeriesRenderer;->setFillBelowLineColor(I)V

    .line 658
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lorg/achartengine/renderer/XYSeriesRenderer;->setFillPoints(Z)V

    .line 659
    invoke-virtual {v1, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 652
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 667
    .end local v0    # "r":Lorg/achartengine/renderer/XYSeriesRenderer;
    :cond_2
    const/4 v3, 0x0

    :goto_1
    if-ge v3, p3, :cond_4

    .line 669
    invoke-virtual {p4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatstr()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-le v4, p2, :cond_3

    .line 670
    add-int/lit8 v4, v3, 0x1

    int-to-double v6, v4

    invoke-virtual {p4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatstr()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v1, v6, v7, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    .line 667
    :goto_2
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    .line 673
    :cond_3
    add-int/lit8 v4, v3, 0x1

    int-to-double v4, v4

    add-int/lit8 v6, p2, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v4, v5, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    goto :goto_2

    .line 677
    :cond_4
    const v4, -0xbbbbbc

    invoke-virtual {v1, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxesColor(I)V

    .line 678
    const v4, -0x333334

    invoke-virtual {v1, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsColor(I)V

    .line 679
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setShowGrid(Z)V

    .line 681
    const/4 v4, -0x1

    invoke-virtual {v1, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setBackgroundColor(I)V

    .line 682
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setApplyBackgroundColor(Z)V

    .line 683
    return-object v1

    .line 650
    nop

    :array_0
    .array-data 4
        0x14
        0x1e
        0xf
        0x1e
    .end array-data
.end method

.method private getBarChartRenderer(ILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 11
    .param p1, "numCount"    # I
    .param p2, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    .prologue
    const/high16 v5, 0x41200000    # 10.0f

    const/4 v10, 0x1

    const-wide/16 v8, 0x0

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    .line 980
    new-instance v2, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 981
    .local v2, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v4, 0x41700000    # 15.0f

    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 982
    const/high16 v4, 0x41a00000    # 20.0f

    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 983
    invoke-virtual {v2, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 984
    invoke-virtual {v2, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 986
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_0

    .line 987
    new-instance v1, Lorg/achartengine/renderer/SimpleSeriesRenderer;

    invoke-direct {v1}, Lorg/achartengine/renderer/SimpleSeriesRenderer;-><init>()V

    .line 988
    .local v1, "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartclr:[Ljava/lang/String;

    rem-int/lit8 v5, v0, 0xa

    aget-object v4, v4, v5

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v1, v4}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setColor(I)V

    .line 989
    invoke-virtual {v2, v1}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 986
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 992
    .end local v1    # "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    :cond_0
    sget-object v4, Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;->VERTICAL:Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;

    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setOrientation(Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;)V

    .line 994
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    cmpg-double v4, v4, v8

    if-gez v4, :cond_1

    .line 995
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 996
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 997
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 999
    :cond_1
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 1001
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    cmpg-double v4, v4, v8

    if-gez v4, :cond_2

    .line 1002
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 1003
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 1004
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 1007
    :cond_2
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 1009
    invoke-virtual {v2, v8, v9}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 1010
    add-int/lit8 v4, p1, 0x1

    int-to-double v4, v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMax(D)V

    .line 1016
    const v4, -0xbbbbbc

    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxesColor(I)V

    .line 1017
    const v4, -0x333334

    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsColor(I)V

    .line 1023
    const/4 v0, 0x0

    :goto_1
    if-ge v0, p1, :cond_4

    .line 1024
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatstr()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-le v4, v0, :cond_3

    .line 1025
    add-int/lit8 v4, v0, 0x1

    int-to-double v6, v4

    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatstr()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v2, v6, v7, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    .line 1023
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1028
    :cond_3
    add-int/lit8 v4, v0, 0x1

    int-to-double v4, v4

    add-int/lit8 v6, v0, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v4, v5, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    goto :goto_2

    .line 1032
    :cond_4
    const v4, -0x777778

    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setGridColor(I)V

    .line 1033
    invoke-virtual {v2, v10}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setShowGridX(Z)V

    .line 1034
    const/4 v4, 0x4

    new-array v4, v4, [I

    fill-array-data v4, :array_0

    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 1036
    const-wide v4, 0x3fd3333333333333L    # 0.3

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setBarSpacing(D)V

    .line 1037
    const/4 v0, 0x0

    :goto_3
    if-ge v0, p1, :cond_5

    .line 1038
    invoke-virtual {v2, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getSeriesRendererAt(I)Lorg/achartengine/renderer/SimpleSeriesRenderer;

    move-result-object v3

    .line 1040
    .local v3, "seriesRenderer":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    invoke-virtual {v3, v10}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setDisplayChartValues(Z)V

    .line 1037
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1043
    .end local v3    # "seriesRenderer":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    :cond_5
    const/4 v4, -0x1

    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setBackgroundColor(I)V

    .line 1044
    invoke-virtual {v2, v10}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setApplyBackgroundColor(Z)V

    .line 1045
    return-object v2

    .line 1034
    nop

    :array_0
    .array-data 4
        0x14
        0x28
        0x32
        0xa
    .end array-data
.end method

.method private getBubbleChartDataSetForCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Lorg/achartengine/model/XYMultipleSeriesDataset;
    .locals 23
    .param p1, "i"    # I
    .param p2, "j"    # I
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    .prologue
    .line 373
    new-instance v20, Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-direct/range {v20 .. v20}, Lorg/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    .line 374
    .local v20, "series":Lorg/achartengine/model/XYMultipleSeriesDataset;
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getOrderVal()Ljava/util/ArrayList;

    move-result-object v8

    move/from16 v0, p1

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v21

    .line 375
    .local v21, "seriescount":I
    const/4 v2, 0x0

    .line 383
    .local v2, "bubsize":I
    const/16 v19, 0x0

    .local v19, "order":I
    :goto_0
    move/from16 v0, v19

    move/from16 v1, v21

    if-ge v0, v1, :cond_7

    .line 384
    new-instance v3, Lorg/achartengine/model/XYValueSeries;

    const-string/jumbo v8, ""

    invoke-direct {v3, v8}, Lorg/achartengine/model/XYValueSeries;-><init>(Ljava/lang/String;)V

    .line 385
    .local v3, "xyValueSeries":Lorg/achartengine/model/XYValueSeries;
    const/4 v8, 0x1

    move/from16 v0, v21

    if-le v0, v8, :cond_5

    .line 388
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartTxstr()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    if-le v8, v9, :cond_0

    .line 389
    new-instance v3, Lorg/achartengine/model/XYValueSeries;

    .end local v3    # "xyValueSeries":Lorg/achartengine/model/XYValueSeries;
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartTxstr()Ljava/util/ArrayList;

    move-result-object v8

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {v3, v8}, Lorg/achartengine/model/XYValueSeries;-><init>(Ljava/lang/String;)V

    .line 392
    .restart local v3    # "xyValueSeries":Lorg/achartengine/model/XYValueSeries;
    :cond_0
    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    add-int/lit8 v8, v8, 0x1

    move-object/from16 v0, p0

    iput v8, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    .line 396
    :goto_1
    const/16 v17, 0x1

    .line 397
    .local v17, "k":I
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartNumCount()Ljava/util/ArrayList;

    move-result-object v8

    move/from16 v0, p1

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    .line 398
    .local v22, "total":I
    const/16 v18, 0x0

    .line 399
    .local v18, "num":I
    :goto_2
    move/from16 v0, v18

    move/from16 v1, v22

    if-ge v0, v1, :cond_6

    .line 401
    :try_start_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 402
    .local v4, "dXVal":D
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataXValue:D

    cmpl-double v8, v4, v8

    if-lez v8, :cond_1

    .line 403
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataXValue:D

    .line 404
    :cond_1
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataXValue:D

    cmpg-double v8, v4, v8

    if-gez v8, :cond_2

    const-wide/16 v8, 0x0

    cmpg-double v8, v4, v8

    if-gez v8, :cond_2

    .line 405
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataXValue:D

    .line 407
    :cond_2
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartYVal()Ljava/util/ArrayList;

    move-result-object v8

    move/from16 v0, p2

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    .line 409
    .local v6, "dYVal":D
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    cmpl-double v8, v6, v8

    if-lez v8, :cond_3

    .line 410
    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 411
    :cond_3
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    cmpg-double v8, v6, v8

    if-gez v8, :cond_4

    const-wide/16 v8, 0x0

    cmpg-double v8, v6, v8

    if-gez v8, :cond_4

    .line 412
    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 413
    :cond_4
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getBubbleSz()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-virtual/range {v3 .. v9}, Lorg/achartengine/model/XYValueSeries;->add(DDD)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 422
    .end local v4    # "dXVal":D
    .end local v6    # "dYVal":D
    :goto_3
    add-int/lit8 p2, p2, 0x1

    .line 423
    add-int/lit8 v18, v18, 0x1

    .line 424
    add-int/lit8 v17, v17, 0x1

    .line 425
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    .line 394
    .end local v17    # "k":I
    .end local v18    # "num":I
    .end local v22    # "total":I
    :cond_5
    new-instance v3, Lorg/achartengine/model/XYValueSeries;

    .end local v3    # "xyValueSeries":Lorg/achartengine/model/XYValueSeries;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Series"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    add-int/lit8 v9, v19, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Lorg/achartengine/model/XYValueSeries;-><init>(Ljava/lang/String;)V

    .restart local v3    # "xyValueSeries":Lorg/achartengine/model/XYValueSeries;
    goto/16 :goto_1

    .line 418
    .restart local v17    # "k":I
    .restart local v18    # "num":I
    .restart local v22    # "total":I
    :catch_0
    move-exception v16

    .line 419
    .local v16, "e":Ljava/lang/Exception;
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    move-object v9, v3

    invoke-virtual/range {v9 .. v15}, Lorg/achartengine/model/XYValueSeries;->add(DDD)V

    goto :goto_3

    .line 427
    .end local v16    # "e":Ljava/lang/Exception;
    :cond_6
    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lorg/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lorg/achartengine/model/XYSeries;)V

    .line 383
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_0

    .line 429
    .end local v3    # "xyValueSeries":Lorg/achartengine/model/XYValueSeries;
    .end local v17    # "k":I
    .end local v18    # "num":I
    .end local v22    # "total":I
    :cond_7
    return-object v20
.end method

.method private getBubbleChartRendrer(I)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 11
    .param p1, "numcnt"    # I

    .prologue
    const/4 v10, 0x1

    const/high16 v4, 0x41200000    # 10.0f

    const-wide/16 v8, 0x0

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    .line 322
    new-instance v2, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 323
    .local v2, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 324
    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 325
    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 326
    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 327
    const/4 v3, 0x4

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 328
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_0

    .line 329
    new-instance v1, Lorg/achartengine/renderer/XYSeriesRenderer;

    invoke-direct {v1}, Lorg/achartengine/renderer/XYSeriesRenderer;-><init>()V

    .line 330
    .local v1, "newTicketRenderer":Lorg/achartengine/renderer/XYSeriesRenderer;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartclr:[Ljava/lang/String;

    rem-int/lit8 v4, v0, 0xa

    aget-object v3, v3, v4

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v3}, Lorg/achartengine/renderer/XYSeriesRenderer;->setColor(I)V

    .line 331
    invoke-virtual {v2, v1}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 328
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 333
    .end local v1    # "newTicketRenderer":Lorg/achartengine/renderer/XYSeriesRenderer;
    :cond_0
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataXValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_1

    .line 334
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataXValue:D

    .line 335
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataXValue:D

    .line 336
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataXValue:D

    .line 338
    :cond_1
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 340
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataXValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_2

    .line 341
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataXValue:D

    .line 342
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataXValue:D

    .line 343
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataXValue:D

    .line 345
    :cond_2
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 347
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_3

    .line 348
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 349
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 350
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 352
    :cond_3
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 354
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_4

    .line 355
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 356
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 357
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 359
    :cond_4
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMax(D)V

    .line 361
    const v3, -0x777778

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxesColor(I)V

    .line 362
    const v3, -0x333334

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsColor(I)V

    .line 363
    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXLabels(I)V

    .line 364
    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYLabels(I)V

    .line 365
    invoke-virtual {v2, v10}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setShowGrid(Z)V

    .line 366
    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setBackgroundColor(I)V

    .line 367
    invoke-virtual {v2, v10}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setApplyBackgroundColor(Z)V

    .line 368
    return-object v2

    .line 327
    nop

    :array_0
    .array-data 4
        0x14
        0x1e
        0xf
        0x14
    .end array-data
.end method

.method private getDoughnutChartRendrer()Lorg/achartengine/renderer/DefaultRenderer;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/high16 v6, 0x41700000    # 15.0f

    .line 1134
    new-instance v5, Lorg/achartengine/renderer/DefaultRenderer;

    invoke-direct {v5}, Lorg/achartengine/renderer/DefaultRenderer;-><init>()V

    .line 1135
    .local v5, "renderer":Lorg/achartengine/renderer/DefaultRenderer;
    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setLabelsTextSize(F)V

    .line 1136
    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setLegendTextSize(F)V

    .line 1137
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_0

    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setMargins([I)V

    .line 1138
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartclr:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 1139
    .local v1, "color":Ljava/lang/String;
    new-instance v4, Lorg/achartengine/renderer/SimpleSeriesRenderer;

    invoke-direct {v4}, Lorg/achartengine/renderer/SimpleSeriesRenderer;-><init>()V

    .line 1140
    .local v4, "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v6}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setColor(I)V

    .line 1141
    invoke-virtual {v5, v4}, Lorg/achartengine/renderer/DefaultRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 1138
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1143
    .end local v1    # "color":Ljava/lang/String;
    .end local v4    # "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    :cond_0
    const/high16 v6, 0x41a00000    # 20.0f

    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setChartTitleTextSize(F)V

    .line 1144
    invoke-virtual {v5, v7}, Lorg/achartengine/renderer/DefaultRenderer;->setDisplayValues(Z)V

    .line 1145
    invoke-virtual {v5, v7}, Lorg/achartengine/renderer/DefaultRenderer;->setShowLabels(Z)V

    .line 1146
    const/4 v6, -0x1

    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setBackgroundColor(I)V

    .line 1147
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setApplyBackgroundColor(Z)V

    .line 1149
    return-object v5

    .line 1137
    nop

    :array_0
    .array-data 4
        0x14
        0x1e
        0xf
        0x3c
    .end array-data
.end method

.method private getLineChartRenderer(ILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 12
    .param p1, "numcnt"    # I
    .param p2, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    .prologue
    const v11, -0x333334

    const/high16 v4, 0x41200000    # 10.0f

    const/4 v10, 0x1

    const-wide/16 v8, 0x0

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    .line 689
    new-instance v2, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 690
    .local v2, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 691
    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 692
    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 693
    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 694
    const/high16 v3, 0x40a00000    # 5.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setPointSize(F)V

    .line 695
    invoke-virtual {v2, v8, v9}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 696
    int-to-double v4, p1

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 702
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_0

    .line 703
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 704
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 705
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 707
    :cond_0
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 709
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_1

    .line 710
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 711
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 712
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 715
    :cond_1
    invoke-virtual {v2, v11}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setGridColor(I)V

    .line 716
    invoke-virtual {v2, v10}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setShowGridX(Z)V

    .line 717
    const/4 v3, 0x4

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 718
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_2

    .line 719
    new-instance v1, Lorg/achartengine/renderer/XYSeriesRenderer;

    invoke-direct {v1}, Lorg/achartengine/renderer/XYSeriesRenderer;-><init>()V

    .line 720
    .local v1, "r":Lorg/achartengine/renderer/XYSeriesRenderer;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartclr:[Ljava/lang/String;

    rem-int/lit8 v4, v0, 0xa

    aget-object v3, v3, v4

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v3}, Lorg/achartengine/renderer/XYSeriesRenderer;->setColor(I)V

    .line 721
    sget-object v3, Lorg/achartengine/chart/PointStyle;->SQUARE:Lorg/achartengine/chart/PointStyle;

    invoke-virtual {v1, v3}, Lorg/achartengine/renderer/XYSeriesRenderer;->setPointStyle(Lorg/achartengine/chart/PointStyle;)V

    .line 722
    invoke-virtual {v1, v10}, Lorg/achartengine/renderer/XYSeriesRenderer;->setFillPoints(Z)V

    .line 723
    invoke-virtual {v2, v1}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 718
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 726
    .end local v1    # "r":Lorg/achartengine/renderer/XYSeriesRenderer;
    :cond_2
    const/4 v0, 0x0

    :goto_1
    if-ge v0, p1, :cond_4

    .line 735
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatstr()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v3, v0, :cond_3

    .line 736
    add-int/lit8 v3, v0, 0x1

    int-to-double v4, v3

    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatstr()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v4, v5, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    .line 726
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 739
    :cond_3
    add-int/lit8 v3, v0, 0x1

    int-to-double v4, v3

    add-int/lit8 v3, v0, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v5, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    goto :goto_2

    .line 743
    :cond_4
    const v3, -0xbbbbbc

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxesColor(I)V

    .line 744
    invoke-virtual {v2, v11}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsColor(I)V

    .line 745
    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setBackgroundColor(I)V

    .line 746
    invoke-virtual {v2, v10}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setApplyBackgroundColor(Z)V

    .line 747
    return-object v2

    .line 717
    :array_0
    .array-data 4
        0x14
        0x1e
        0xf
        0x1e
    .end array-data
.end method

.method private getPieChartRenderer()Lorg/achartengine/renderer/DefaultRenderer;
    .locals 12

    .prologue
    const/high16 v6, 0x41700000    # 15.0f

    const-wide/16 v10, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1050
    new-instance v5, Lorg/achartengine/renderer/DefaultRenderer;

    invoke-direct {v5}, Lorg/achartengine/renderer/DefaultRenderer;-><init>()V

    .line 1051
    .local v5, "renderer":Lorg/achartengine/renderer/DefaultRenderer;
    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setLabelsTextSize(F)V

    .line 1052
    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setLegendTextSize(F)V

    .line 1053
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_0

    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setMargins([I)V

    .line 1055
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartclr:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 1056
    .local v1, "color":Ljava/lang/String;
    new-instance v4, Lorg/achartengine/renderer/SimpleSeriesRenderer;

    invoke-direct {v4}, Lorg/achartengine/renderer/SimpleSeriesRenderer;-><init>()V

    .line 1057
    .local v4, "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v6}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setColor(I)V

    .line 1058
    invoke-virtual {v5, v4}, Lorg/achartengine/renderer/DefaultRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 1055
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1060
    .end local v1    # "color":Ljava/lang/String;
    .end local v4    # "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    :cond_0
    invoke-virtual {v5, v7}, Lorg/achartengine/renderer/DefaultRenderer;->setZoomButtonsVisible(Z)V

    .line 1061
    invoke-virtual {v5, v7}, Lorg/achartengine/renderer/DefaultRenderer;->setZoomEnabled(Z)V

    .line 1062
    const/high16 v6, 0x41a00000    # 20.0f

    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setChartTitleTextSize(F)V

    .line 1063
    invoke-virtual {v5, v8}, Lorg/achartengine/renderer/DefaultRenderer;->setDisplayValues(Z)V

    .line 1064
    invoke-virtual {v5, v8}, Lorg/achartengine/renderer/DefaultRenderer;->setShowLabels(Z)V

    .line 1065
    invoke-virtual {v5, v8}, Lorg/achartengine/renderer/DefaultRenderer;->getSeriesRendererAt(I)Lorg/achartengine/renderer/SimpleSeriesRenderer;

    move-result-object v4

    .line 1066
    .restart local v4    # "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    invoke-virtual {v4, v7}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setGradientEnabled(Z)V

    .line 1067
    const v6, -0xffff01

    invoke-virtual {v4, v10, v11, v6}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setGradientStart(DI)V

    .line 1068
    const v6, -0xff0100

    invoke-virtual {v4, v10, v11, v6}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setGradientStop(DI)V

    .line 1069
    invoke-virtual {v4, v7}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setHighlighted(Z)V

    .line 1070
    const/4 v6, -0x1

    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setBackgroundColor(I)V

    .line 1071
    invoke-virtual {v5, v7}, Lorg/achartengine/renderer/DefaultRenderer;->setApplyBackgroundColor(Z)V

    .line 1073
    return-object v5

    .line 1053
    nop

    :array_0
    .array-data 4
        0x14
        0x1e
        0xf
        0x3c
    .end array-data
.end method

.method private getScatterChartDatasetForCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Lorg/achartengine/model/XYMultipleSeriesDataset;
    .locals 16
    .param p1, "i"    # I
    .param p2, "j"    # I
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    .prologue
    .line 752
    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataXValue:D

    .line 753
    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataXValue:D

    .line 754
    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 755
    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 757
    new-instance v6, Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-direct {v6}, Lorg/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    .line 758
    .local v6, "dataset":Lorg/achartengine/model/XYMultipleSeriesDataset;
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getOrderVal()Ljava/util/ArrayList;

    move-result-object v13

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    .line 765
    .local v11, "seriescount":I
    const/4 v9, 0x0

    .local v9, "order":I
    :goto_0
    if-ge v9, v11, :cond_6

    .line 767
    const/4 v13, 0x1

    if-le v11, v13, :cond_4

    .line 768
    new-instance v10, Lorg/achartengine/model/XYSeries;

    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartTxstr()Ljava/util/ArrayList;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-direct {v10, v13}, Lorg/achartengine/model/XYSeries;-><init>(Ljava/lang/String;)V

    .line 771
    .local v10, "series":Lorg/achartengine/model/XYSeries;
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    .line 775
    :goto_1
    const/4 v7, 0x1

    .line 776
    .local v7, "k":I
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartNumCount()Ljava/util/ArrayList;

    move-result-object v13

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 777
    .local v12, "total":I
    const/4 v8, 0x0

    .line 778
    .local v8, "num":I
    :goto_2
    if-ge v8, v12, :cond_5

    .line 779
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, ""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 780
    .local v2, "dXVal":D
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataXValue:D

    cmpl-double v13, v2, v14

    if-lez v13, :cond_0

    .line 781
    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataXValue:D

    .line 782
    :cond_0
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataXValue:D

    cmpg-double v13, v2, v14

    if-gez v13, :cond_1

    const-wide/16 v14, 0x0

    cmpg-double v13, v2, v14

    if-gez v13, :cond_1

    .line 783
    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataXValue:D

    .line 785
    :cond_1
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartYVal()Ljava/util/ArrayList;

    move-result-object v13

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-static {v13}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 787
    .local v4, "dYVal":D
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    cmpl-double v13, v4, v14

    if-lez v13, :cond_2

    .line 788
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 789
    :cond_2
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    cmpg-double v13, v4, v14

    if-gez v13, :cond_3

    const-wide/16 v14, 0x0

    cmpg-double v13, v4, v14

    if-gez v13, :cond_3

    .line 790
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 792
    :cond_3
    invoke-virtual {v10, v2, v3, v4, v5}, Lorg/achartengine/model/XYSeries;->add(DD)V

    .line 793
    add-int/lit8 p2, p2, 0x1

    .line 794
    add-int/lit8 v8, v8, 0x1

    .line 795
    add-int/lit8 v7, v7, 0x1

    .line 796
    goto :goto_2

    .line 773
    .end local v2    # "dXVal":D
    .end local v4    # "dYVal":D
    .end local v7    # "k":I
    .end local v8    # "num":I
    .end local v10    # "series":Lorg/achartengine/model/XYSeries;
    .end local v12    # "total":I
    :cond_4
    new-instance v10, Lorg/achartengine/model/XYSeries;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Series"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    add-int/lit8 v14, v9, 0x1

    invoke-static {v14}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v10, v13}, Lorg/achartengine/model/XYSeries;-><init>(Ljava/lang/String;)V

    .restart local v10    # "series":Lorg/achartengine/model/XYSeries;
    goto/16 :goto_1

    .line 797
    .restart local v7    # "k":I
    .restart local v8    # "num":I
    .restart local v12    # "total":I
    :cond_5
    invoke-virtual {v6, v10}, Lorg/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lorg/achartengine/model/XYSeries;)V

    .line 765
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 800
    .end local v7    # "k":I
    .end local v8    # "num":I
    .end local v10    # "series":Lorg/achartengine/model/XYSeries;
    .end local v12    # "total":I
    :cond_6
    return-object v6
.end method

.method private getScatterChartRenderer(I)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 11
    .param p1, "numCount"    # I

    .prologue
    const/high16 v4, 0x41200000    # 10.0f

    const/4 v10, 0x1

    const-wide/16 v8, 0x0

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    .line 804
    new-instance v2, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 805
    .local v2, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 806
    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 807
    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 808
    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 809
    const/high16 v3, 0x40a00000    # 5.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setPointSize(F)V

    .line 811
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataXValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_0

    .line 812
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataXValue:D

    .line 813
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataXValue:D

    .line 814
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataXValue:D

    .line 816
    :cond_0
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 818
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataXValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_1

    .line 819
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataXValue:D

    .line 820
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataXValue:D

    .line 821
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataXValue:D

    .line 823
    :cond_1
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 825
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_2

    .line 826
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 827
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 828
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 830
    :cond_2
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 832
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_3

    .line 833
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 834
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 835
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 838
    :cond_3
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMax(D)V

    .line 839
    const/4 v3, 0x4

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 841
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_4

    .line 842
    new-instance v1, Lorg/achartengine/renderer/XYSeriesRenderer;

    invoke-direct {v1}, Lorg/achartengine/renderer/XYSeriesRenderer;-><init>()V

    .line 843
    .local v1, "r":Lorg/achartengine/renderer/XYSeriesRenderer;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartclr:[Ljava/lang/String;

    rem-int/lit8 v4, v0, 0xa

    aget-object v3, v3, v4

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v3}, Lorg/achartengine/renderer/XYSeriesRenderer;->setColor(I)V

    .line 844
    invoke-virtual {v1, v10}, Lorg/achartengine/renderer/XYSeriesRenderer;->setFillPoints(Z)V

    .line 845
    invoke-virtual {v2, v1}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 841
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 848
    .end local v1    # "r":Lorg/achartengine/renderer/XYSeriesRenderer;
    :cond_4
    invoke-virtual {v2, v10}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setShowGrid(Z)V

    .line 849
    const v3, -0xbbbbbc

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxesColor(I)V

    .line 850
    const v3, -0x333334

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsColor(I)V

    .line 851
    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setBackgroundColor(I)V

    .line 852
    invoke-virtual {v2, v10}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setApplyBackgroundColor(Z)V

    .line 853
    return-object v2

    .line 839
    nop

    :array_0
    .array-data 4
        0x14
        0x1e
        0xf
        0x1e
    .end array-data
.end method


# virtual methods
.method public areaChartDisplayForCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)V
    .locals 4
    .param p1, "i"    # I
    .param p2, "j"    # I
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    .prologue
    .line 262
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartNumCount()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 263
    .local v1, "numcnt":I
    new-instance v0, Lorg/achartengine/chart/TimeChart;

    invoke-virtual {p0, p1, p2, v1, p3}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->getAreaChartDatasetForcanvas(IIILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v2

    invoke-direct {p0, p1, p2, v1, p3}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->getAreaChartRenderer(IIILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/achartengine/chart/TimeChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;)V

    .line 266
    .local v0, "areaChart":Lorg/achartengine/chart/TimeChart;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 267
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setAreaChart(Lorg/achartengine/chart/TimeChart;)V

    .line 268
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 269
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 271
    return-void
.end method

.method public barchartdisplayForCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)V
    .locals 5
    .param p1, "i"    # I
    .param p2, "j"    # I
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    .prologue
    .line 308
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartNumCount()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 309
    .local v2, "numcnt":I
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->getColChartDatasetForcanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v1

    .line 311
    .local v1, "dataSet":Lorg/achartengine/model/XYMultipleSeriesDataset;
    new-instance v0, Lorg/achartengine/chart/ColumnChart;

    invoke-direct {p0, v2, p3}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->getBarChartRenderer(ILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lorg/achartengine/chart/ColumnChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;)V

    .line 313
    .local v0, "colChart":Lorg/achartengine/chart/ColumnChart;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    const/16 v4, 0x10

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 314
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v3, v0}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setHorizontalBarChart(Lorg/achartengine/chart/ColumnChart;)V

    .line 315
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    sget-object v4, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 316
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 319
    return-void
.end method

.method public bubbleChartDisplayForCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)V
    .locals 4
    .param p1, "i"    # I
    .param p2, "j"    # I
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    .prologue
    .line 274
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartNumCount()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 275
    .local v1, "numcnt":I
    new-instance v0, Lorg/achartengine/chart/BubbleChart;

    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->getBubbleChartDataSetForCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v2

    invoke-direct {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->getBubbleChartRendrer(I)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/achartengine/chart/BubbleChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;)V

    .line 278
    .local v0, "bubbleChart":Lorg/achartengine/chart/BubbleChart;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    const/16 v3, 0xf

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 279
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setBubbleChart(Lorg/achartengine/chart/XYChart;)V

    .line 280
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 281
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 282
    return-void
.end method

.method public colchartdisplayForCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)V
    .locals 5
    .param p1, "i"    # I
    .param p2, "j"    # I
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    .prologue
    .line 294
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartNumCount()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 295
    .local v2, "numcnt":I
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->getColChartDatasetForcanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v1

    .line 297
    .local v1, "dataSet":Lorg/achartengine/model/XYMultipleSeriesDataset;
    new-instance v0, Lorg/achartengine/chart/BarChart;

    invoke-virtual {p0, v2, p3}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->getColChartRenderer(ILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v3

    sget-object v4, Lorg/achartengine/chart/BarChart$Type;->DEFAULT:Lorg/achartengine/chart/BarChart$Type;

    invoke-direct {v0, v1, v3, v4}, Lorg/achartengine/chart/BarChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;Lorg/achartengine/chart/BarChart$Type;)V

    .line 299
    .local v0, "barChart":Lorg/achartengine/chart/BarChart;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 300
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v3, v0}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setColumnBarChart(Lorg/achartengine/chart/AbstractChart;)V

    .line 301
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    sget-object v4, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 302
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 305
    return-void
.end method

.method public doughnutChartDisplayForCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)V
    .locals 3
    .param p1, "i"    # I
    .param p2, "j"    # I
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    .prologue
    .line 240
    new-instance v0, Lorg/achartengine/chart/DoughnutChart;

    invoke-virtual {p0, p3, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->getDoughnutChartDatasetForCanvas(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;II)Lorg/achartengine/model/CategorySeries;

    move-result-object v1

    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->getDoughnutChartRendrer()Lorg/achartengine/renderer/DefaultRenderer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/achartengine/chart/DoughnutChart;-><init>(Lorg/achartengine/model/CategorySeries;Lorg/achartengine/renderer/DefaultRenderer;)V

    .line 243
    .local v0, "doughnutChart":Lorg/achartengine/chart/DoughnutChart;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 244
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setDoughnutChart(Lorg/achartengine/chart/DoughnutChart;)V

    .line 245
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    sget-object v2, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 246
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 248
    return-void
.end method

.method public drawChartForCanvas(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 25
    .param p1, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p2, "themeobj"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 69
    .local p3, "topCoordinates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .local p4, "bottomCoordinates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .local p5, "rightCoordinates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .local p6, "leftCoordinates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    .line 71
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartData()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    move-result-object v23

    if-eqz v23, :cond_3

    .line 72
    const/4 v11, 0x0

    .local v11, "j":I
    const/4 v12, 0x0

    .line 73
    .local v12, "k":I
    move-object/from16 v0, p1

    iget v0, v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartcnt:I

    move/from16 v21, v0

    .line 74
    .local v21, "totalchart":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    move/from16 v0, v21

    if-ge v10, v0, :cond_3

    .line 75
    const-string/jumbo v14, "false"

    .local v14, "lines":Ljava/lang/String;
    const-string/jumbo v3, "false"

    .local v3, "bar":Ljava/lang/String;
    const-string/jumbo v7, "false"

    .local v7, "column":Ljava/lang/String;
    const-string/jumbo v16, "false"

    .local v16, "pie":Ljava/lang/String;
    const-string/jumbo v18, "false"

    .local v18, "scatter":Ljava/lang/String;
    const-string/jumbo v4, "false"

    .local v4, "bubble":Ljava/lang/String;
    const-string/jumbo v2, "false"

    .local v2, "area":Ljava/lang/String;
    const-string/jumbo v8, "false"

    .local v8, "doughnut":Ljava/lang/String;
    const-string/jumbo v17, "false"

    .line 76
    .local v17, "radar":Ljava/lang/String;
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->matchseries:Z

    .line 78
    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Integer;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v20

    .line 79
    .local v20, "top":I
    move-object/from16 v0, p6

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Integer;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v13

    .line 80
    .local v13, "left":I
    move-object/from16 v0, p5

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Integer;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v24

    move-object/from16 v0, p6

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Integer;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v23

    sub-int v22, v24, v23

    .line 81
    .local v22, "width":I
    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Integer;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v24

    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Integer;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v23

    sub-int v9, v24, v23

    .line 84
    .local v9, "height":I
    if-nez v20, :cond_1

    if-nez v13, :cond_1

    if-nez v22, :cond_1

    if-nez v9, :cond_1

    .line 74
    :cond_0
    :goto_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 88
    :cond_1
    if-eqz v22, :cond_0

    if-eqz v9, :cond_0

    .line 92
    new-instance v23, Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-direct/range {v23 .. v23}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    .line 93
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartNumCount()Ljava/util/ArrayList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 94
    .local v5, "chartNumCount":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setX(I)V

    .line 95
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setY(I)V

    .line 96
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    move-object/from16 v23, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartWidth(F)V

    .line 97
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    move-object/from16 v23, v0

    int-to-float v0, v9

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartHeight(F)V

    .line 99
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v23

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-lt v0, v1, :cond_0

    .line 101
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartNumCount()Ljava/util/ArrayList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    .line 103
    .local v15, "numcnt":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartName()Ljava/util/ArrayList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 104
    .local v6, "chartname":Ljava/lang/String;
    const-string/jumbo v23, "Line"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v23

    if-nez v23, :cond_4

    .line 105
    const-string/jumbo v14, "true"

    .line 125
    :cond_2
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getOrderVal()Ljava/util/ArrayList;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v23

    move/from16 v0, v23

    if-lt v10, v0, :cond_c

    .line 205
    .end local v2    # "area":Ljava/lang/String;
    .end local v3    # "bar":Ljava/lang/String;
    .end local v4    # "bubble":Ljava/lang/String;
    .end local v5    # "chartNumCount":Ljava/lang/String;
    .end local v6    # "chartname":Ljava/lang/String;
    .end local v7    # "column":Ljava/lang/String;
    .end local v8    # "doughnut":Ljava/lang/String;
    .end local v9    # "height":I
    .end local v10    # "i":I
    .end local v11    # "j":I
    .end local v12    # "k":I
    .end local v13    # "left":I
    .end local v14    # "lines":Ljava/lang/String;
    .end local v15    # "numcnt":I
    .end local v16    # "pie":Ljava/lang/String;
    .end local v17    # "radar":Ljava/lang/String;
    .end local v18    # "scatter":Ljava/lang/String;
    .end local v20    # "top":I
    .end local v21    # "totalchart":I
    .end local v22    # "width":I
    :cond_3
    return-void

    .line 106
    .restart local v2    # "area":Ljava/lang/String;
    .restart local v3    # "bar":Ljava/lang/String;
    .restart local v4    # "bubble":Ljava/lang/String;
    .restart local v5    # "chartNumCount":Ljava/lang/String;
    .restart local v6    # "chartname":Ljava/lang/String;
    .restart local v7    # "column":Ljava/lang/String;
    .restart local v8    # "doughnut":Ljava/lang/String;
    .restart local v9    # "height":I
    .restart local v10    # "i":I
    .restart local v11    # "j":I
    .restart local v12    # "k":I
    .restart local v13    # "left":I
    .restart local v14    # "lines":Ljava/lang/String;
    .restart local v15    # "numcnt":I
    .restart local v16    # "pie":Ljava/lang/String;
    .restart local v17    # "radar":Ljava/lang/String;
    .restart local v18    # "scatter":Ljava/lang/String;
    .restart local v20    # "top":I
    .restart local v21    # "totalchart":I
    .restart local v22    # "width":I
    :cond_4
    const-string/jumbo v23, "Bar"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v23

    if-nez v23, :cond_5

    .line 107
    const-string/jumbo v3, "true"

    goto :goto_2

    .line 108
    :cond_5
    const-string/jumbo v23, "Pie"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v23

    if-nez v23, :cond_6

    .line 109
    const-string/jumbo v16, "true"

    goto :goto_2

    .line 110
    :cond_6
    const-string/jumbo v23, "Scatter"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v23

    if-nez v23, :cond_7

    .line 111
    const-string/jumbo v18, "true"

    goto :goto_2

    .line 112
    :cond_7
    const-string/jumbo v23, "Bubble"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v23

    if-nez v23, :cond_8

    .line 113
    const-string/jumbo v4, "true"

    goto :goto_2

    .line 114
    :cond_8
    const-string/jumbo v23, "Area"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v23

    if-nez v23, :cond_9

    .line 115
    const-string/jumbo v2, "true"

    goto :goto_2

    .line 116
    :cond_9
    const-string/jumbo v23, "DoughnutChart"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v23

    if-nez v23, :cond_a

    .line 117
    const-string/jumbo v8, "true"

    goto :goto_2

    .line 118
    :cond_a
    const-string/jumbo v23, "Column"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v23

    if-nez v23, :cond_b

    .line 119
    const-string/jumbo v7, "true"

    goto :goto_2

    .line 120
    :cond_b
    const-string/jumbo v23, "Radar"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v23

    if-nez v23, :cond_2

    .line 121
    const-string/jumbo v17, "true"

    goto/16 :goto_2

    .line 128
    :cond_c
    const-string/jumbo v23, "true"

    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v23

    if-nez v23, :cond_e

    .line 129
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getOrderVal()Ljava/util/ArrayList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    .line 131
    .local v19, "seriescount":I
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v10, v11, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->pieChartDisplayForCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)V

    .line 132
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->matchseries:Z

    move/from16 v23, v0

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_d

    .line 133
    add-int/2addr v11, v15

    .line 135
    :cond_d
    mul-int v23, v15, v19

    add-int v11, v11, v23

    .line 136
    goto/16 :goto_1

    .end local v19    # "seriescount":I
    :cond_e
    const-string/jumbo v23, "true"

    move-object/from16 v0, v18

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v23

    if-nez v23, :cond_10

    .line 138
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getOrderVal()Ljava/util/ArrayList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    .line 141
    .restart local v19    # "seriescount":I
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v10, v12, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->scatterChartDisplayForCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)V

    .line 142
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->matchseries:Z

    move/from16 v23, v0

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_f

    .line 143
    add-int/2addr v11, v15

    .line 145
    :cond_f
    mul-int v23, v15, v19

    add-int v12, v12, v23

    .line 147
    goto/16 :goto_1

    .end local v19    # "seriescount":I
    :cond_10
    const-string/jumbo v23, "true"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v23

    if-nez v23, :cond_12

    .line 148
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getOrderVal()Ljava/util/ArrayList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    .line 151
    .restart local v19    # "seriescount":I
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v10, v12, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->bubbleChartDisplayForCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)V

    .line 152
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->matchseries:Z

    move/from16 v23, v0

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_11

    .line 153
    add-int/2addr v11, v15

    .line 155
    :cond_11
    mul-int v23, v15, v19

    add-int v12, v12, v23

    .line 156
    goto/16 :goto_1

    .end local v19    # "seriescount":I
    :cond_12
    const-string/jumbo v23, "true"

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v23

    if-nez v23, :cond_14

    .line 157
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getOrderVal()Ljava/util/ArrayList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    .line 160
    .restart local v19    # "seriescount":I
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v10, v11, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->areaChartDisplayForCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)V

    .line 161
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->matchseries:Z

    move/from16 v23, v0

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_13

    .line 162
    add-int/2addr v11, v15

    .line 164
    :cond_13
    mul-int v23, v15, v19

    add-int v11, v11, v23

    .line 165
    goto/16 :goto_1

    .end local v19    # "seriescount":I
    :cond_14
    const-string/jumbo v23, "true"

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v23

    if-nez v23, :cond_15

    .line 166
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getOrderVal()Ljava/util/ArrayList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    .line 168
    .restart local v19    # "seriescount":I
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v10, v11, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->barchartdisplayForCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)V

    .line 169
    mul-int v23, v15, v19

    add-int v11, v11, v23

    .line 173
    goto/16 :goto_1

    .end local v19    # "seriescount":I
    :cond_15
    const-string/jumbo v23, "true"

    move-object/from16 v0, v23

    invoke-virtual {v7, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v23

    if-nez v23, :cond_16

    .line 174
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getOrderVal()Ljava/util/ArrayList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    .line 176
    .restart local v19    # "seriescount":I
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v10, v11, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->colchartdisplayForCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)V

    .line 177
    mul-int v23, v15, v19

    add-int v11, v11, v23

    .line 181
    goto/16 :goto_1

    .end local v19    # "seriescount":I
    :cond_16
    const-string/jumbo v23, "true"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v23

    if-nez v23, :cond_17

    .line 182
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getOrderVal()Ljava/util/ArrayList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    .line 185
    .restart local v19    # "seriescount":I
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v10, v11, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->lineChartDisplayForCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)V

    .line 186
    mul-int v23, v15, v19

    add-int v11, v11, v23

    .line 188
    goto/16 :goto_1

    .end local v19    # "seriescount":I
    :cond_17
    const-string/jumbo v23, "true"

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v23

    if-nez v23, :cond_18

    .line 189
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getOrderVal()Ljava/util/ArrayList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    .line 192
    .restart local v19    # "seriescount":I
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v10, v11, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->doughnutChartDisplayForCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)V

    .line 193
    mul-int v23, v15, v19

    add-int v11, v11, v23

    .line 195
    goto/16 :goto_1

    .end local v19    # "seriescount":I
    :cond_18
    const-string/jumbo v23, "true"

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v23

    if-nez v23, :cond_0

    .line 196
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getOrderVal()Ljava/util/ArrayList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    .line 199
    .restart local v19    # "seriescount":I
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v10, v11, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->radarChartDisplayForCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)V

    .line 200
    mul-int v23, v15, v19

    add-int v11, v11, v23

    goto/16 :goto_1
.end method

.method public getAreaChartDatasetForcanvas(IIILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Lorg/achartengine/model/XYMultipleSeriesDataset;
    .locals 18
    .param p1, "i"    # I
    .param p2, "j"    # I
    .param p3, "numcnt"    # I
    .param p4, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    .prologue
    .line 552
    new-instance v4, Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-direct {v4}, Lorg/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    .line 553
    .local v4, "dataset":Lorg/achartengine/model/XYMultipleSeriesDataset;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 555
    .local v11, "valueList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Double;>;"
    invoke-virtual/range {p4 .. p4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getOrderVal()Ljava/util/ArrayList;

    move-result-object v13

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 556
    .local v9, "seriescount":I
    new-array v8, v9, [Lorg/achartengine/model/CategorySeries;

    .line 565
    .local v8, "seriesArray":[Lorg/achartengine/model/CategorySeries;
    const/4 v6, 0x0

    .local v6, "order":I
    :goto_0
    if-ge v6, v9, :cond_9

    .line 568
    const/4 v13, 0x1

    if-le v9, v13, :cond_2

    .line 569
    new-instance v7, Lorg/achartengine/model/CategorySeries;

    invoke-virtual/range {p4 .. p4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartTxstr()Ljava/util/ArrayList;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-le v13, v6, :cond_1

    invoke-virtual/range {p4 .. p4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartTxstr()Ljava/util/ArrayList;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    :goto_1
    invoke-direct {v7, v13}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 574
    .local v7, "series":Lorg/achartengine/model/CategorySeries;
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesoneclrchange:I

    add-int/2addr v13, v14

    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesoneclrchange:I

    .line 576
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    .line 590
    :goto_2
    invoke-virtual/range {p4 .. p4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartNumCount()Ljava/util/ArrayList;

    move-result-object v13

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 591
    .local v10, "total":I
    const/4 v5, 0x0

    .line 592
    .local v5, "num":I
    :goto_3
    if-ge v5, v10, :cond_0

    .line 593
    const-wide/16 v2, 0x0

    .line 594
    .local v2, "addedValue":D
    invoke-virtual/range {p4 .. p4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatnum()Ljava/util/ArrayList;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    move/from16 v0, p2

    if-gt v13, v0, :cond_4

    .line 618
    .end local v2    # "addedValue":D
    :cond_0
    aput-object v7, v8, v6

    .line 565
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 569
    .end local v5    # "num":I
    .end local v7    # "series":Lorg/achartengine/model/CategorySeries;
    .end local v10    # "total":I
    :cond_1
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Series"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    add-int/lit8 v14, v6, 0x1

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto :goto_1

    .line 580
    :cond_2
    invoke-virtual/range {p4 .. p4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartTxstr()Ljava/util/ArrayList;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    if-le v13, v14, :cond_3

    .line 581
    new-instance v7, Lorg/achartengine/model/CategorySeries;

    invoke-virtual/range {p4 .. p4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartTxstr()Ljava/util/ArrayList;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-direct {v7, v13}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 584
    .restart local v7    # "series":Lorg/achartengine/model/CategorySeries;
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    goto :goto_2

    .line 586
    .end local v7    # "series":Lorg/achartengine/model/CategorySeries;
    :cond_3
    new-instance v7, Lorg/achartengine/model/CategorySeries;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Series"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    add-int/lit8 v14, v6, 0x1

    invoke-static {v14}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v7, v13}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .restart local v7    # "series":Lorg/achartengine/model/CategorySeries;
    goto/16 :goto_2

    .line 597
    .restart local v2    # "addedValue":D
    .restart local v5    # "num":I
    .restart local v10    # "total":I
    :cond_4
    if-nez v6, :cond_8

    .line 598
    invoke-virtual/range {p4 .. p4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatnum()Ljava/util/ArrayList;

    move-result-object v13

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-static {v13}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 604
    :goto_4
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-le v13, v5, :cond_5

    .line 605
    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 606
    :cond_5
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v13

    invoke-virtual {v11, v5, v13}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 608
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    cmpl-double v13, v2, v14

    if-lez v13, :cond_6

    .line 609
    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 610
    :cond_6
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    cmpg-double v13, v2, v14

    if-gez v13, :cond_7

    const-wide/16 v14, 0x0

    cmpg-double v13, v2, v14

    if-gez v13, :cond_7

    .line 611
    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 612
    :cond_7
    invoke-virtual {v7, v2, v3}, Lorg/achartengine/model/CategorySeries;->add(D)V

    .line 614
    add-int/lit8 p2, p2, 0x1

    .line 615
    add-int/lit8 v5, v5, 0x1

    .line 617
    goto/16 :goto_3

    .line 601
    :cond_8
    invoke-virtual/range {p4 .. p4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatnum()Ljava/util/ArrayList;

    move-result-object v13

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-static {v13}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v14

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Double;

    invoke-virtual {v13}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v16

    add-double v2, v14, v16

    goto :goto_4

    .line 622
    .end local v2    # "addedValue":D
    .end local v5    # "num":I
    .end local v7    # "series":Lorg/achartengine/model/CategorySeries;
    .end local v10    # "total":I
    :cond_9
    array-length v13, v8

    add-int/lit8 v12, v13, -0x1

    .local v12, "x":I
    :goto_5
    if-ltz v12, :cond_a

    .line 623
    aget-object v13, v8, v12

    invoke-virtual {v13}, Lorg/achartengine/model/CategorySeries;->toXYSeries()Lorg/achartengine/model/XYSeries;

    move-result-object v13

    invoke-virtual {v4, v13}, Lorg/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lorg/achartengine/model/XYSeries;)V

    .line 622
    add-int/lit8 v12, v12, -0x1

    goto :goto_5

    .line 624
    :cond_a
    return-object v4
.end method

.method public getColChartDatasetForcanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Lorg/achartengine/model/XYMultipleSeriesDataset;
    .locals 12
    .param p1, "i"    # I
    .param p2, "j"    # I
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    .prologue
    .line 434
    new-instance v2, Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-direct {v2}, Lorg/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    .line 435
    .local v2, "dataset":Lorg/achartengine/model/XYMultipleSeriesDataset;
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getOrderVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 443
    .local v7, "seriescount":I
    const/4 v5, 0x0

    .local v5, "order":I
    :goto_0
    if-ge v5, v7, :cond_7

    .line 444
    const-wide/16 v10, 0x0

    iput-wide v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 445
    const-wide/16 v10, 0x0

    iput-wide v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 447
    const/4 v9, 0x1

    if-le v7, v9, :cond_2

    .line 448
    new-instance v6, Lorg/achartengine/model/CategorySeries;

    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartTxstr()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-le v9, v5, :cond_1

    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartTxstr()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    :goto_1
    invoke-direct {v6, v9}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 453
    .local v6, "series":Lorg/achartengine/model/CategorySeries;
    iget v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    .line 454
    const/4 v9, 0x0

    iput v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesoneclrchange:I

    .line 466
    :goto_2
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartNumCount()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 467
    .local v8, "total":I
    const/4 v4, 0x0

    .line 468
    .local v4, "num":I
    :goto_3
    if-ge v4, v8, :cond_0

    .line 470
    :try_start_0
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatnum()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    if-gt v9, p2, :cond_4

    .line 489
    :cond_0
    invoke-virtual {v6}, Lorg/achartengine/model/CategorySeries;->toXYSeries()Lorg/achartengine/model/XYSeries;

    move-result-object v9

    invoke-virtual {v2, v9}, Lorg/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lorg/achartengine/model/XYSeries;)V

    .line 443
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 448
    .end local v4    # "num":I
    .end local v6    # "series":Lorg/achartengine/model/CategorySeries;
    .end local v8    # "total":I
    :cond_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Series"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    add-int/lit8 v10, v5, 0x1

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto :goto_1

    .line 457
    :cond_2
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartTxstr()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    iget v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    if-le v9, v10, :cond_3

    .line 458
    new-instance v6, Lorg/achartengine/model/CategorySeries;

    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartTxstr()Ljava/util/ArrayList;

    move-result-object v9

    iget v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-direct {v6, v9}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 460
    .restart local v6    # "series":Lorg/achartengine/model/CategorySeries;
    iget v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    goto :goto_2

    .line 462
    .end local v6    # "series":Lorg/achartengine/model/CategorySeries;
    :cond_3
    new-instance v6, Lorg/achartengine/model/CategorySeries;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Series"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    add-int/lit8 v10, v5, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v9}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .restart local v6    # "series":Lorg/achartengine/model/CategorySeries;
    goto :goto_2

    .line 473
    .restart local v4    # "num":I
    .restart local v8    # "total":I
    :cond_4
    :try_start_1
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatnum()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 475
    .local v0, "dVal":D
    iget-wide v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    cmpl-double v9, v0, v10

    if-lez v9, :cond_5

    .line 476
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 477
    :cond_5
    iget-wide v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    cmpg-double v9, v0, v10

    if-gez v9, :cond_6

    const-wide/16 v10, 0x0

    cmpg-double v9, v0, v10

    if-gez v9, :cond_6

    .line 478
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 479
    :cond_6
    invoke-virtual {v6, v0, v1}, Lorg/achartengine/model/CategorySeries;->add(D)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    .line 484
    .end local v0    # "dVal":D
    :goto_4
    add-int/lit8 p2, p2, 0x1

    .line 485
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_3

    .line 480
    :catch_0
    move-exception v3

    .line 481
    .local v3, "ex":Ljava/lang/NumberFormatException;
    const-string/jumbo v9, "DocumentService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Exception: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    const-wide/16 v10, 0x0

    invoke-virtual {v6, v10, v11}, Lorg/achartengine/model/CategorySeries;->add(D)V

    goto :goto_4

    .line 492
    .end local v3    # "ex":Ljava/lang/NumberFormatException;
    .end local v4    # "num":I
    .end local v6    # "series":Lorg/achartengine/model/CategorySeries;
    .end local v8    # "total":I
    :cond_7
    return-object v2
.end method

.method public getColChartRenderer(ILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 12
    .param p1, "numCount"    # I
    .param p2, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    .prologue
    const/4 v11, 0x1

    const v10, -0x333334

    const/high16 v4, 0x41200000    # 10.0f

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    const-wide/16 v6, 0x0

    .line 918
    new-instance v2, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 919
    .local v2, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 920
    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 921
    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 922
    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 923
    const/4 v3, 0x4

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 925
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_0

    .line 926
    new-instance v1, Lorg/achartengine/renderer/SimpleSeriesRenderer;

    invoke-direct {v1}, Lorg/achartengine/renderer/SimpleSeriesRenderer;-><init>()V

    .line 927
    .local v1, "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartclr:[Ljava/lang/String;

    rem-int/lit8 v4, v0, 0xa

    aget-object v3, v3, v4

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v3}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setColor(I)V

    .line 928
    invoke-virtual {v2, v1}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 925
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 931
    .end local v1    # "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    :cond_0
    invoke-virtual {v2, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 932
    add-int/lit8 v3, p1, 0x1

    int-to-double v4, v3

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 938
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    cmpg-double v3, v4, v6

    if-gez v3, :cond_1

    .line 939
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    mul-double/2addr v4, v8

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 940
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 941
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    mul-double/2addr v4, v8

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 943
    :cond_1
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 945
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    cmpg-double v3, v4, v6

    if-gez v3, :cond_2

    .line 946
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    mul-double/2addr v4, v8

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 947
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 948
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    mul-double/2addr v4, v8

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 951
    :cond_2
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMax(D)V

    .line 952
    invoke-virtual {v2, v10}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setGridColor(I)V

    .line 953
    invoke-virtual {v2, v11}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setShowGridX(Z)V

    .line 954
    const-wide v4, 0x3fd3333333333333L    # 0.3

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setBarSpacing(D)V

    .line 959
    const v3, -0xbbbbbc

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxesColor(I)V

    .line 960
    invoke-virtual {v2, v10}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsColor(I)V

    .line 962
    const/4 v0, 0x0

    :goto_1
    if-ge v0, p1, :cond_4

    .line 963
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatstr()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v3, v0, :cond_3

    .line 964
    add-int/lit8 v3, v0, 0x1

    int-to-double v4, v3

    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatstr()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v4, v5, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    .line 962
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 967
    :cond_3
    add-int/lit8 v3, v0, 0x1

    int-to-double v4, v3

    add-int/lit8 v3, v0, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v5, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    goto :goto_2

    .line 971
    :cond_4
    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 972
    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 973
    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setBackgroundColor(I)V

    .line 974
    invoke-virtual {v2, v11}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setApplyBackgroundColor(Z)V

    .line 975
    return-object v2

    .line 923
    :array_0
    .array-data 4
        0x1e
        0x1e
        0xf
        0x1e
    .end array-data
.end method

.method public getDoughnutChartDatasetForCanvas(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;II)Lorg/achartengine/model/CategorySeries;
    .locals 6
    .param p1, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p2, "i"    # I
    .param p3, "j"    # I

    .prologue
    .line 1112
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartNumCount()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 1113
    .local v2, "totalcnt":I
    add-int/2addr v2, p3

    .line 1114
    new-instance v1, Lorg/achartengine/model/CategorySeries;

    const-string/jumbo v3, ""

    invoke-direct {v1, v3}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 1116
    .local v1, "series":Lorg/achartengine/model/CategorySeries;
    :goto_0
    if-ge p3, v2, :cond_0

    .line 1118
    :try_start_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatnum()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gt v3, p3, :cond_1

    .line 1129
    :cond_0
    return-object v1

    .line 1121
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatstr()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatnum()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-virtual {v1, v3, v4, v5}, Lorg/achartengine/model/CategorySeries;->add(Ljava/lang/String;D)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1127
    :goto_1
    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    .line 1123
    :catch_0
    move-exception v0

    .line 1124
    .local v0, "ex":Ljava/lang/NumberFormatException;
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1125
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatstr()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v3, v4, v5}, Lorg/achartengine/model/CategorySeries;->add(Ljava/lang/String;D)V

    goto :goto_1
.end method

.method public getLableForRadar(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Ljava/util/ArrayList;
    .locals 6
    .param p1, "i"    # I
    .param p2, "j"    # I
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 901
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 902
    .local v2, "serVal":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 903
    .local v1, "chartCategoriesStrSize":I
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatstr()Ljava/util/ArrayList;

    move-result-object v0

    .line 905
    .local v0, "chartCategories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    .line 906
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 908
    :cond_0
    const/4 v3, 0x0

    .local v3, "x":I
    :goto_0
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartNumCount()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 910
    if-eqz v0, :cond_1

    if-le v1, p2, :cond_1

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    :goto_1
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 909
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 910
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Category "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v3, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 913
    :cond_2
    return-object v2
.end method

.method public getLineChartDatasetForcanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Lorg/achartengine/model/XYMultipleSeriesDataset;
    .locals 12
    .param p1, "i"    # I
    .param p2, "j"    # I
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    .prologue
    .line 497
    new-instance v2, Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-direct {v2}, Lorg/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    .line 498
    .local v2, "dataset":Lorg/achartengine/model/XYMultipleSeriesDataset;
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getOrderVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 505
    .local v7, "seriescount":I
    const/4 v5, 0x0

    .local v5, "order":I
    :goto_0
    if-ge v5, v7, :cond_7

    .line 507
    const/4 v9, 0x1

    if-le v7, v9, :cond_2

    .line 508
    new-instance v6, Lorg/achartengine/model/CategorySeries;

    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartTxstr()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-le v9, v5, :cond_1

    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartTxstr()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    :goto_1
    invoke-direct {v6, v9}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 513
    .local v6, "series":Lorg/achartengine/model/CategorySeries;
    iget v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    .line 523
    :goto_2
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartNumCount()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 524
    .local v8, "total":I
    const/4 v4, 0x0

    .line 525
    .local v4, "num":I
    :goto_3
    if-ge v4, v8, :cond_0

    .line 527
    :try_start_0
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatnum()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    if-gt v9, p2, :cond_4

    .line 544
    :cond_0
    invoke-virtual {v6}, Lorg/achartengine/model/CategorySeries;->toXYSeries()Lorg/achartengine/model/XYSeries;

    move-result-object v9

    invoke-virtual {v2, v9}, Lorg/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lorg/achartengine/model/XYSeries;)V

    .line 505
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 508
    .end local v4    # "num":I
    .end local v6    # "series":Lorg/achartengine/model/CategorySeries;
    .end local v8    # "total":I
    :cond_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Series"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    add-int/lit8 v10, v5, 0x1

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto :goto_1

    .line 515
    :cond_2
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartTxstr()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    iget v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    if-le v9, v10, :cond_3

    .line 516
    new-instance v6, Lorg/achartengine/model/CategorySeries;

    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartTxstr()Ljava/util/ArrayList;

    move-result-object v9

    iget v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-direct {v6, v9}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 518
    .restart local v6    # "series":Lorg/achartengine/model/CategorySeries;
    iget v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->seriesstrcount:I

    goto :goto_2

    .line 520
    .end local v6    # "series":Lorg/achartengine/model/CategorySeries;
    :cond_3
    new-instance v6, Lorg/achartengine/model/CategorySeries;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Series"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    add-int/lit8 v10, v5, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v9}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .restart local v6    # "series":Lorg/achartengine/model/CategorySeries;
    goto :goto_2

    .line 530
    .restart local v4    # "num":I
    .restart local v8    # "total":I
    :cond_4
    :try_start_1
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatnum()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 532
    .local v0, "dVal":D
    iget-wide v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    cmpl-double v9, v0, v10

    if-lez v9, :cond_5

    .line 533
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMaxDataYValue:D

    .line 534
    :cond_5
    iget-wide v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    cmpg-double v9, v0, v10

    if-gez v9, :cond_6

    const-wide/16 v10, 0x0

    cmpg-double v9, v0, v10

    if-gez v9, :cond_6

    .line 535
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->mMinDataYValue:D

    .line 536
    :cond_6
    invoke-virtual {v6, v0, v1}, Lorg/achartengine/model/CategorySeries;->add(D)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    .line 541
    .end local v0    # "dVal":D
    :goto_4
    add-int/lit8 p2, p2, 0x1

    .line 542
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_3

    .line 537
    :catch_0
    move-exception v3

    .line 538
    .local v3, "ex":Ljava/lang/NumberFormatException;
    const-string/jumbo v9, "DocumentService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Exception: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 539
    const-wide/16 v10, 0x0

    invoke-virtual {v6, v10, v11}, Lorg/achartengine/model/CategorySeries;->add(D)V

    goto :goto_4

    .line 546
    .end local v3    # "ex":Ljava/lang/NumberFormatException;
    .end local v4    # "num":I
    .end local v6    # "series":Lorg/achartengine/model/CategorySeries;
    .end local v8    # "total":I
    :cond_7
    return-object v2
.end method

.method public getPieChartdatasetForCanvas(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;II)Lorg/achartengine/model/CategorySeries;
    .locals 8
    .param p1, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p2, "i"    # I
    .param p3, "j"    # I

    .prologue
    .line 1079
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartNumCount()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 1080
    .local v2, "totalcnt":I
    add-int/2addr v2, p3

    .line 1081
    new-instance v1, Lorg/achartengine/model/CategorySeries;

    const-string/jumbo v3, ""

    invoke-direct {v1, v3}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 1083
    .local v1, "series":Lorg/achartengine/model/CategorySeries;
    :goto_0
    if-ge p3, v2, :cond_0

    .line 1085
    :try_start_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatnum()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gt v3, p3, :cond_1

    .line 1105
    :cond_0
    return-object v1

    .line 1088
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatstr()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_2

    .line 1089
    add-int/lit8 v3, p3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatnum()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-virtual {v1, v4, v6, v7}, Lorg/achartengine/model/CategorySeries;->add(Ljava/lang/String;D)V

    .line 1103
    :goto_1
    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    .line 1094
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatstr()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatnum()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-virtual {v1, v3, v4, v5}, Lorg/achartengine/model/CategorySeries;->add(Ljava/lang/String;D)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1099
    :catch_0
    move-exception v0

    .line 1100
    .local v0, "ex":Ljava/lang/NumberFormatException;
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1101
    const-wide/16 v4, 0x0

    invoke-virtual {v1, v4, v5}, Lorg/achartengine/model/CategorySeries;->add(D)V

    goto :goto_1
.end method

.method public getRadarSeriesForCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Ljava/util/ArrayList;
    .locals 5
    .param p1, "i"    # I
    .param p2, "j"    # I
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 887
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 888
    .local v1, "serVal":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getOrderVal()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 889
    .local v2, "seriescount":I
    const/4 v0, 0x0

    .local v0, "order":I
    :goto_0
    if-ge v0, v2, :cond_2

    .line 891
    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    .line 892
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartTxstr()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v3, v0, :cond_1

    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartTxstr()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    :goto_1
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 889
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 892
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Series"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 896
    :cond_2
    return-object v1
.end method

.method public getSeriesDataForRadarCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Ljava/util/ArrayList;
    .locals 10
    .param p1, "i"    # I
    .param p2, "j"    # I
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;",
            ")",
            "Ljava/util/ArrayList",
            "<[D>;"
        }
    .end annotation

    .prologue
    .line 859
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 860
    .local v0, "dataSet":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[D>;"
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getOrderVal()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 861
    .local v5, "seriescount":I
    const/4 v4, 0x0

    .local v4, "order":I
    :goto_0
    if-ge v4, v5, :cond_2

    .line 862
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartNumCount()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 863
    .local v6, "total":I
    const/4 v3, 0x0

    .line 864
    .local v3, "num":I
    new-array v1, v6, [D

    .line 865
    .local v1, "doubleArray":[D
    :goto_1
    if-ge v3, v6, :cond_0

    .line 867
    :try_start_0
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatnum()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    if-gt v7, p2, :cond_1

    .line 880
    :cond_0
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 861
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 870
    :cond_1
    :try_start_1
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartCatnum()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    aput-wide v8, v1, v3
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    .line 877
    :goto_2
    add-int/lit8 p2, p2, 0x1

    .line 878
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 873
    :catch_0
    move-exception v2

    .line 874
    .local v2, "ex":Ljava/lang/NumberFormatException;
    const-string/jumbo v7, "DocumentService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 882
    .end local v1    # "doubleArray":[D
    .end local v2    # "ex":Ljava/lang/NumberFormatException;
    .end local v3    # "num":I
    .end local v6    # "total":I
    :cond_2
    return-object v0
.end method

.method public lineChartDisplayForCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)V
    .locals 4
    .param p1, "i"    # I
    .param p2, "j"    # I
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    .prologue
    .line 221
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartNumCount()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 224
    .local v1, "numcnt":I
    new-instance v0, Lorg/achartengine/chart/LineChart;

    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->getLineChartDatasetForcanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v2

    invoke-direct {p0, v1, p3}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->getLineChartRenderer(ILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/achartengine/chart/LineChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;)V

    .line 226
    .local v0, "lineChart":Lorg/achartengine/chart/XYChart;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 227
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setLineChart(Lorg/achartengine/chart/XYChart;)V

    .line 228
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 229
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 231
    return-void
.end method

.method public pieChartDisplayForCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)V
    .locals 3
    .param p1, "i"    # I
    .param p2, "j"    # I
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    .prologue
    .line 285
    new-instance v0, Lorg/achartengine/chart/PieChart;

    invoke-virtual {p0, p3, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->getPieChartdatasetForCanvas(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;II)Lorg/achartengine/model/CategorySeries;

    move-result-object v1

    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->getPieChartRenderer()Lorg/achartengine/renderer/DefaultRenderer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/achartengine/chart/PieChart;-><init>(Lorg/achartengine/model/CategorySeries;Lorg/achartengine/renderer/DefaultRenderer;)V

    .line 287
    .local v0, "pieChart":Lorg/achartengine/chart/PieChart;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 288
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setPieChart(Lorg/achartengine/chart/PieChart;)V

    .line 289
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    sget-object v2, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 290
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 291
    return-void
.end method

.method public radarChartDisplayForCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)V
    .locals 4
    .param p1, "i"    # I
    .param p2, "j"    # I
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    .prologue
    .line 251
    new-instance v0, Lorg/achartengine/chart/RadarChart;

    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->getSeriesDataForRadarCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->getLableForRadar(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->getRadarSeriesForCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lorg/achartengine/chart/RadarChart;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 255
    .local v0, "radarChartView":Lorg/achartengine/chart/RadarChart;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 256
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setRadarChart(Lorg/achartengine/chart/RadarChart;)V

    .line 257
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    sget-object v2, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 258
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 259
    return-void
.end method

.method public scatterChartDisplayForCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)V
    .locals 4
    .param p1, "i"    # I
    .param p2, "j"    # I
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    .prologue
    .line 209
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartNumCount()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 210
    .local v0, "numcnt":I
    new-instance v1, Lorg/achartengine/chart/ScatterChart;

    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->getScatterChartDatasetForCanvas(IILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v2

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->getScatterChartRenderer(I)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/achartengine/chart/ScatterChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;)V

    .line 214
    .local v1, "scatterChart":Lorg/achartengine/chart/XYChart;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 215
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v2, v1}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setScatterChart(Lorg/achartengine/chart/XYChart;)V

    .line 216
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 217
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 218
    return-void
.end method
