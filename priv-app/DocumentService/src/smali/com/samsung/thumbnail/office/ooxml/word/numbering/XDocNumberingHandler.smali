.class public Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumberingHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLChoiceHandler;
.source "XDocNumberingHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    .line 17
    const-string/jumbo v2, "numbering"

    invoke-direct {p0, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLChoiceHandler;-><init>(Ljava/lang/String;)V

    .line 19
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;-><init>()V

    .line 20
    .local v0, "abstractNumHandler":Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumberingHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "abstractNum"

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumHandler;-><init>()V

    .line 22
    .local v1, "numHandler":Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumberingHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "num"

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumberingHandler;->choiceHandlerMap:Ljava/util/HashMap;

    const/16 v3, 0x14

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumberingHandler;->handlerMap:Ljava/util/HashMap;

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    return-void
.end method
