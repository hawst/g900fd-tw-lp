.class Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler$MultiLvlType;
.super Ljava/lang/Object;
.source "XDocAbstractNumHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MultiLvlType"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;


# direct methods
.method constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler$MultiLvlType;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V
    .locals 3
    .param p1, "value"    # Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;

    .prologue
    .line 84
    instance-of v1, p1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 85
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    .line 86
    .local v0, "multiLvlType":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler$MultiLvlType;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;

    # getter for: Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;->abstractNum:Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;
    invoke-static {v1}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;->access$000(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum$EMultiLevelType;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum$EMultiLevelType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;->setMultiLvlType(Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum$EMultiLevelType;)V

    .line 89
    .end local v0    # "multiLvlType":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    :cond_0
    return-void
.end method
