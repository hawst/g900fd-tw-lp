.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/FormatSchemeHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "FormatSchemeHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/theme/FormatSchemeHandler$FillPropConsumer;
    }
.end annotation


# instance fields
.field private theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V
    .locals 1
    .param p1, "theme"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .prologue
    .line 23
    const-string/jumbo v0, "fmtScheme"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 24
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FormatSchemeHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .line 26
    return-void
.end method


# virtual methods
.method protected init(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 8
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 30
    const/4 v6, 0x2

    new-array v5, v6, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 31
    .local v5, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FormatSchemeHandler$FillPropConsumer;

    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FormatSchemeHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-direct {v3, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FormatSchemeHandler$FillPropConsumer;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 33
    .local v3, "fillPropConsumer":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FormatSchemeHandler$FillPropConsumer;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;

    const-string/jumbo v6, "fillStyleLst"

    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FormatSchemeHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-direct {v2, v3, v6, v7}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 35
    .local v2, "fillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v6, "fillStyleLst"

    invoke-direct {v4, v6, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 37
    .local v4, "fillSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v6, 0x0

    aput-object v4, v5, v6

    .line 39
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;

    const-string/jumbo v6, "bgFillStyleLst"

    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FormatSchemeHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-direct {v0, v3, v6, v7}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 41
    .local v0, "bgFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v6, "bgFillStyleLst"

    invoke-direct {v1, v6, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 43
    .local v1, "bgFillSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v6, 0x1

    aput-object v1, v5, v6

    .line 45
    iput-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FormatSchemeHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 46
    return-void
.end method

.method public setBlipEmbed(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "blipEmbed"    # Ljava/lang/String;

    .prologue
    .line 76
    return-void
.end method

.method public setTile(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "flip"    # Ljava/lang/String;

    .prologue
    .line 81
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 51
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 53
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FormatSchemeHandler;->init(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V

    .line 54
    return-void
.end method
