.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableBackgroundHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "TableBackgroundHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableBackgroundHandler$ITableBackgroundConsumer;
    }
.end annotation


# instance fields
.field fillProperty:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

.field private tableBackgroundConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableBackgroundHandler$ITableBackgroundConsumer;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableBackgroundHandler$ITableBackgroundConsumer;)V
    .locals 1
    .param p1, "tableBackgroundConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableBackgroundHandler$ITableBackgroundConsumer;

    .prologue
    .line 27
    const-string/jumbo v0, "tblBg"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 28
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableBackgroundHandler;->tableBackgroundConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableBackgroundHandler$ITableBackgroundConsumer;

    .line 29
    return-void
.end method


# virtual methods
.method public consumeFill(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 0
    .param p1, "fillProperties"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableBackgroundHandler;->fillProperty:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .line 60
    return-void
.end method

.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableBackgroundHandler;->tableBackgroundConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableBackgroundHandler$ITableBackgroundConsumer;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableBackgroundHandler;->fillProperty:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getFillRefColor()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableBackgroundHandler$ITableBackgroundConsumer;->handleTableBackground(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 53
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 54
    return-void
.end method

.method protected init()V
    .locals 5

    .prologue
    .line 32
    const/4 v3, 0x0

    .line 33
    .local v3, "shapeInfo":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    const/4 v4, 0x1

    new-array v2, v4, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 34
    .local v2, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler;

    invoke-direct {v0, p0, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 35
    .local v0, "fillRefHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v4, "fillRef"

    invoke-direct {v1, v4, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 37
    .local v1, "fillRefSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v4, 0x0

    aput-object v1, v2, v4

    .line 38
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableBackgroundHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 39
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 44
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 46
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableBackgroundHandler;->init()V

    .line 48
    return-void
.end method
