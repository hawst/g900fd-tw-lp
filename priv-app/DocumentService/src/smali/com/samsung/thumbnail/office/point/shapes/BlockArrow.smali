.class public Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;
.super Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;
.source "BlockArrow.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mDrawOnCanvas:Z

.field private mLeftMarg:I

.field private mTopMarg:I


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "folderPath"    # Ljava/io/File;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;-><init>()V

    .line 28
    const-string/jumbo v0, "BlockArrow"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->TAG:Ljava/lang/String;

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mDrawOnCanvas:Z

    .line 34
    iput-object p1, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->folderPath:Ljava/io/File;

    .line 35
    return-void
.end method


# virtual methods
.method protected draw(Landroid/graphics/Canvas;I)V
    .locals 19
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shape_Count"    # I

    .prologue
    .line 160
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "DownArrow"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 169
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval0:I

    if-nez v2, :cond_0

    .line 170
    const/16 v2, 0x3f48

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval0:I

    .line 171
    :cond_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval1:I

    if-nez v2, :cond_1

    .line 172
    const/16 v2, 0x1518

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval1:I

    .line 174
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval0:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v9, v2, v3

    .line 175
    .local v9, "adj_height":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval1:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v12, v2, v3

    .line 177
    .local v12, "adj_width2":F
    new-instance v17, Landroid/graphics/Path;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Path;-><init>()V

    .line 178
    .local v17, "path":Landroid/graphics/Path;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v9

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 179
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v9

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 181
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v12

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 182
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    sub-float/2addr v2, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 184
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    sub-float/2addr v2, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v9

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 186
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v9

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 188
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 190
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Path;->close()V

    .line 191
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintFill:Landroid/graphics/Paint;

    if-eqz v2, :cond_2

    .line 192
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 193
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_3

    .line 194
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 198
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x45d2f000    # 6750.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 199
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 200
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x46680800    # 14850.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 201
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x46680800    # 14850.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    .line 490
    .end local v9    # "adj_height":F
    .end local v12    # "adj_width2":F
    .end local v17    # "path":Landroid/graphics/Path;
    :cond_4
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mDrawOnCanvas:Z

    if-nez v2, :cond_5

    .line 491
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 494
    .local v7, "mtx":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->rotation:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-virtual {v7, v2, v3, v4}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 496
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->bitmap:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->bitmapWidth:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->bitmapHight:I

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 501
    .local v18, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v16, Ljava/io/FileOutputStream;

    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->folderPath:Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "pic_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->shapeName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ".png"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 507
    .local v16, "out":Ljava/io/FileOutputStream;
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 508
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 513
    .end local v7    # "mtx":Landroid/graphics/Matrix;
    .end local v16    # "out":Ljava/io/FileOutputStream;
    .end local v18    # "rotatedBMP":Landroid/graphics/Bitmap;
    :cond_5
    :goto_1
    return-void

    .line 204
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "UpArrow"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 213
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval0:I

    if-nez v2, :cond_7

    .line 214
    const/16 v2, 0x1518

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval0:I

    .line 215
    :cond_7
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval1:I

    if-nez v2, :cond_8

    .line 216
    const/16 v2, 0x1518

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval1:I

    .line 218
    :cond_8
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval0:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v9, v2, v3

    .line 219
    .restart local v9    # "adj_height":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval1:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v12, v2, v3

    .line 221
    .restart local v12    # "adj_width2":F
    new-instance v17, Landroid/graphics/Path;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Path;-><init>()V

    .line 222
    .restart local v17    # "path":Landroid/graphics/Path;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v9

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 223
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v9

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 225
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v12

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 227
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    sub-float/2addr v2, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 229
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    sub-float/2addr v2, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v9

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 231
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v9

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 233
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 234
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Path;->close()V

    .line 235
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintFill:Landroid/graphics/Paint;

    if-eqz v2, :cond_9

    .line 236
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 237
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_a

    .line 238
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 242
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x45d2f000    # 6750.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 243
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x45d2f000    # 6750.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 244
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x46680800    # 14850.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 245
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 248
    .end local v9    # "adj_height":F
    .end local v12    # "adj_width2":F
    .end local v17    # "path":Landroid/graphics/Path;
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "Arrow"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "RightArrow"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 258
    :cond_c
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval0:I

    if-nez v2, :cond_d

    .line 259
    const/16 v2, 0x3f48

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval0:I

    .line 260
    :cond_d
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval1:I

    if-nez v2, :cond_e

    .line 261
    const/16 v2, 0x1518

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval1:I

    .line 263
    :cond_e
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval0:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v11, v2, v3

    .line 264
    .local v11, "adj_width":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval1:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v10, v2, v3

    .line 266
    .local v10, "adj_height2":F
    new-instance v17, Landroid/graphics/Path;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Path;-><init>()V

    .line 267
    .restart local v17    # "path":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v11

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 268
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v11

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v10

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 270
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v10

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 271
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    sub-float/2addr v3, v10

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 273
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v11

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    sub-float/2addr v3, v10

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 275
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v11

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 277
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 279
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Path;->close()V

    .line 280
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintFill:Landroid/graphics/Paint;

    if-eqz v2, :cond_f

    .line 281
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 282
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_10

    .line 283
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 287
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 288
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x45d2f000    # 6750.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 289
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x46680800    # 14850.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 290
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x46680800    # 14850.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 293
    .end local v10    # "adj_height2":F
    .end local v11    # "adj_width":F
    .end local v17    # "path":Landroid/graphics/Path;
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "LeftArrow"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 302
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval0:I

    if-nez v2, :cond_12

    .line 303
    const/16 v2, 0x1518

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval0:I

    .line 304
    :cond_12
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval1:I

    if-nez v2, :cond_13

    .line 305
    const/16 v2, 0x1518

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval1:I

    .line 307
    :cond_13
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval0:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v11, v2, v3

    .line 308
    .restart local v11    # "adj_width":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval1:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v10, v2, v3

    .line 310
    .restart local v10    # "adj_height2":F
    new-instance v17, Landroid/graphics/Path;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Path;-><init>()V

    .line 311
    .restart local v17    # "path":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v11

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 312
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v11

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v10

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 314
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v10

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 316
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    sub-float/2addr v3, v10

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 318
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v11

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    sub-float/2addr v3, v10

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 320
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v11

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 322
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 323
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Path;->close()V

    .line 324
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintFill:Landroid/graphics/Paint;

    if-eqz v2, :cond_14

    .line 325
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 326
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_15

    .line 327
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 331
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x45d2f000    # 6750.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 332
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x45d2f000    # 6750.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 333
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 334
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x46680800    # 14850.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 337
    .end local v10    # "adj_height2":F
    .end local v11    # "adj_width":F
    .end local v17    # "path":Landroid/graphics/Path;
    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "LeftRightArrow"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 346
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval0:I

    if-nez v2, :cond_17

    .line 347
    const/16 v2, 0x10e0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval0:I

    .line 348
    :cond_17
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval1:I

    if-nez v2, :cond_18

    .line 349
    const/16 v2, 0x1518

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval1:I

    .line 351
    :cond_18
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval0:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v11, v2, v3

    .line 352
    .restart local v11    # "adj_width":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval1:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v10, v2, v3

    .line 354
    .restart local v10    # "adj_height2":F
    new-instance v17, Landroid/graphics/Path;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Path;-><init>()V

    .line 355
    .restart local v17    # "path":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v11

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 356
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v11

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v10

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 358
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    sub-float/2addr v2, v11

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v10

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 360
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    sub-float/2addr v2, v11

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 362
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 364
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    sub-float/2addr v2, v11

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 366
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    sub-float/2addr v2, v11

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    sub-float/2addr v3, v10

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 368
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v11

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    sub-float/2addr v3, v10

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 370
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v11

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 372
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 373
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Path;->close()V

    .line 375
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintFill:Landroid/graphics/Paint;

    if-eqz v2, :cond_19

    .line 376
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 377
    :cond_19
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_1a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_1a

    .line 378
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 382
    :cond_1a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x45d18800    # 6705.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 383
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x45d2f000    # 6750.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 384
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x46680800    # 14850.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 385
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x46680800    # 14850.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 388
    .end local v10    # "adj_height2":F
    .end local v11    # "adj_width":F
    .end local v17    # "path":Landroid/graphics/Path;
    :cond_1b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "UpDownArrow"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_20

    .line 397
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval0:I

    if-nez v2, :cond_1c

    .line 398
    const/16 v2, 0x10e0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval0:I

    .line 399
    :cond_1c
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval1:I

    if-nez v2, :cond_1d

    .line 400
    const/16 v2, 0x1518

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval1:I

    .line 402
    :cond_1d
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval1:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v9, v2, v3

    .line 403
    .restart local v9    # "adj_height":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval0:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v12, v2, v3

    .line 405
    .restart local v12    # "adj_width2":F
    new-instance v17, Landroid/graphics/Path;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Path;-><init>()V

    .line 406
    .restart local v17    # "path":Landroid/graphics/Path;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v9

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 407
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v9

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 409
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v12

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    sub-float/2addr v3, v9

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 411
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    sub-float/2addr v3, v9

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 413
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 415
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    sub-float/2addr v3, v9

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 417
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    sub-float/2addr v2, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    sub-float/2addr v3, v9

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 419
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    sub-float/2addr v2, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v9

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 421
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v9

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 423
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 424
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Path;->close()V

    .line 426
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_1e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintFill:Landroid/graphics/Paint;

    if-eqz v2, :cond_1e

    .line 427
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 428
    :cond_1e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_1f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_1f

    .line 429
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 433
    :cond_1f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x45d2f000    # 6750.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 434
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x45d2f000    # 6750.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 435
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x46680800    # 14850.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 436
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x46680800    # 14850.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 439
    .end local v9    # "adj_height":F
    .end local v12    # "adj_width2":F
    .end local v17    # "path":Landroid/graphics/Path;
    :cond_20
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "NotchedRightArrow"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 448
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval0:I

    if-nez v2, :cond_21

    .line 449
    const/16 v2, 0x3f48

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval0:I

    .line 450
    :cond_21
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval1:I

    if-nez v2, :cond_22

    .line 451
    const/16 v2, 0x1518

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval1:I

    .line 453
    :cond_22
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval0:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v11, v2, v3

    .line 454
    .restart local v11    # "adj_width":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->adjval1:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v9, v2, v3

    .line 455
    .restart local v9    # "adj_height":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    sub-float/2addr v2, v11

    const/high16 v3, 0x40000000    # 2.0f

    div-float v15, v2, v3

    .line 456
    .local v15, "notch_width":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float v14, v2, v3

    .line 458
    .local v14, "notch_height":F
    new-instance v17, Landroid/graphics/Path;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Path;-><init>()V

    .line 459
    .restart local v17    # "path":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v11

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 460
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 462
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v11

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 464
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v11

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    sub-float/2addr v3, v9

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 466
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    sub-float/2addr v3, v9

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 468
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v15

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v14

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 470
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v9

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 471
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v11

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v9

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 473
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Path;->close()V

    .line 475
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_23

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintFill:Landroid/graphics/Paint;

    if-eqz v2, :cond_23

    .line 476
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 477
    :cond_23
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_24

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_24

    .line 478
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 482
    :cond_24
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x45d2f000    # 6750.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 483
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x45d2f000    # 6750.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 484
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x46680800    # 14850.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 485
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->textArea:Landroid/graphics/RectF;

    const v3, 0x46680800    # 14850.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 509
    .end local v9    # "adj_height":F
    .end local v11    # "adj_width":F
    .end local v14    # "notch_height":F
    .end local v15    # "notch_width":F
    .end local v17    # "path":Landroid/graphics/Path;
    .restart local v7    # "mtx":Landroid/graphics/Matrix;
    .restart local v18    # "rotatedBMP":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v13

    .line 510
    .local v13, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception while writing pictures to out file"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F

    .prologue
    .line 111
    invoke-super {p0, p3, p4, p7, p8}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IFF)V

    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mDrawOnCanvas:Z

    .line 113
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mLeftMarg:I

    .line 114
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mTopMarg:I

    .line 115
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 116
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->draw(Landroid/graphics/Canvas;I)V

    .line 117
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 118
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mDrawOnCanvas:Z

    .line 119
    return-void
.end method

.method public drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p2, "shapeCount"    # I

    .prologue
    .line 102
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V

    .line 104
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->draw(I)V

    .line 105
    return-void
.end method

.method public drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 145
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;I)V

    .line 146
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mDrawOnCanvas:Z

    .line 147
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mLeftMarg:I

    .line 148
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mTopMarg:I

    .line 149
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    .line 150
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    .line 151
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 152
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->draw(Landroid/graphics/Canvas;I)V

    .line 153
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 154
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mDrawOnCanvas:Z

    .line 155
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V
    .locals 0
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 39
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 40
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->draw(I)V

    .line 41
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V
    .locals 2
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I

    .prologue
    .line 45
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mDrawOnCanvas:Z

    .line 47
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mLeftMarg:I

    .line 48
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mTopMarg:I

    .line 49
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 50
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->draw(Landroid/graphics/Canvas;I)V

    .line 51
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mDrawOnCanvas:Z

    .line 53
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 78
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mDrawOnCanvas:Z

    .line 80
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mLeftMarg:I

    .line 81
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mTopMarg:I

    .line 82
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 83
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->draw(Landroid/graphics/Canvas;I)V

    .line 84
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 85
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mDrawOnCanvas:Z

    .line 86
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 90
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V

    .line 91
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mDrawOnCanvas:Z

    .line 92
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mLeftMarg:I

    .line 93
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mTopMarg:I

    .line 94
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 95
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->draw(Landroid/graphics/Canvas;I)V

    .line 96
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 97
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mDrawOnCanvas:Z

    .line 98
    return-void
.end method

.method public drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .prologue
    .line 72
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 73
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->draw(I)V

    .line 74
    return-void
.end method

.method public drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 58
    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p7

    move v4, p8

    move-object v5, p9

    invoke-super/range {v0 .. v5}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSSExcelShape(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mDrawOnCanvas:Z

    .line 61
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mLeftMarg:I

    .line 62
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mTopMarg:I

    .line 63
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 64
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->draw(Landroid/graphics/Canvas;I)V

    .line 65
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mDrawOnCanvas:Z

    .line 67
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;I)V
    .locals 1
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 124
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 125
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->draw(I)V

    .line 126
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 130
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 131
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mDrawOnCanvas:Z

    .line 132
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mLeftMarg:I

    .line 133
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mTopMarg:I

    .line 134
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->width:F

    .line 135
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->height:F

    .line 136
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 137
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->draw(Landroid/graphics/Canvas;I)V

    .line 138
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 139
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->mDrawOnCanvas:Z

    .line 140
    return-void
.end method
