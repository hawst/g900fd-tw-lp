.class public Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;
.super Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;
.source "XWPFHeader.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/IBodyElement;


# instance fields
.field private _headerName:Ljava/lang/String;

.field private relArray:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;-><init>()V

    .line 48
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;)V
    .locals 0
    .param p1, "doc"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;)V

    .line 71
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/POIXMLDocumentPart;Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V
    .locals 1
    .param p1, "parent"    # Lorg/apache/poi/POIXMLDocumentPart;
    .param p2, "part"    # Lorg/apache/poi/openxml4j/opc/PackagePart;
    .param p3, "rel"    # Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;-><init>(Lorg/apache/poi/POIXMLDocumentPart;Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V

    .line 53
    invoke-virtual {p2}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getPartName()Lorg/apache/poi/openxml4j/opc/PackagePartName;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;->_headerName:Ljava/lang/String;

    .line 54
    invoke-virtual {p2}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getRelArray()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;->relArray:Ljava/util/List;

    .line 55
    return-void
.end method


# virtual methods
.method public getElementType()Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->HEADER:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    return-object v0
.end method

.method public getHeaderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;->_headerName:Ljava/lang/String;

    return-object v0
.end method

.method public getRelList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;->relArray:Ljava/util/List;

    return-object v0
.end method

.method protected onDocumentRead(Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;)V
    .locals 2
    .param p1, "consumer"    # Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    invoke-super {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;->onDocumentRead()V

    .line 82
    const/4 v0, 0x0

    .line 84
    .local v0, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 85
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XDocHeaderParser;

    invoke-direct {v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XDocHeaderParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;)V

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XDocHeaderParser;->parse(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 105
    :cond_0
    return-void

    .line 87
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    .line 88
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    :cond_1
    throw v1
.end method

.method public setHeaderName(Ljava/lang/String;)V
    .locals 0
    .param p1, "_headerName"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;->_headerName:Ljava/lang/String;

    .line 44
    return-void
.end method
