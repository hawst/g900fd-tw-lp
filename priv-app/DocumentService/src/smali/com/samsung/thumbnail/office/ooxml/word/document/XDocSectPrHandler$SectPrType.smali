.class Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler$SectPrType;
.super Ljava/lang/Object;
.source "XDocSectPrHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SectPrType"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;


# direct methods
.method constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler$SectPrType;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V
    .locals 3
    .param p1, "value"    # Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;

    .prologue
    .line 117
    instance-of v1, p1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 118
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    .line 119
    .local v0, "type":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler$SectPrType;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;

    # getter for: Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;
    invoke-static {v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;->access$000(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->setType(Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;)V

    .line 121
    .end local v0    # "type":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    :cond_0
    return-void
.end method
