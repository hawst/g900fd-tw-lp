.class public Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/SlideMasterIdHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "SlideMasterIdHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    const-string/jumbo v0, "sldMasterId"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 14
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 19
    const-string/jumbo v1, "id"

    const/16 v2, 0xa

    invoke-virtual {p0, p3, v1, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/SlideMasterIdHandler;->getAttrForNS(Lorg/xml/sax/Attributes;Ljava/lang/String;ILcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 21
    .local v0, "slideId":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 25
    :goto_0
    return-void

    .line 24
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->addMasterSlideID(Ljava/lang/String;)V

    goto :goto_0
.end method
