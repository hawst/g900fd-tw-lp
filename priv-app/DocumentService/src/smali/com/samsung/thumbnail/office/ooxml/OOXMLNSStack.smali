.class public Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;
.super Ljava/lang/Object;
.source "OOXMLNSStack.java"


# instance fields
.field protected mDefNameSpace:Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

.field protected mLevel:I

.field protected mNameSpace:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->mNameSpace:Ljava/util/Vector;

    .line 18
    return-void
.end method


# virtual methods
.method public addNameSpace(Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V
    .locals 1
    .param p1, "nameSpace"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    .prologue
    .line 21
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->getPrefix()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 22
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->mDefNameSpace:Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    .line 24
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->mNameSpace:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 25
    return-void
.end method

.method public decrementLevel()V
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->mLevel:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->mLevel:I

    .line 60
    return-void
.end method

.method public getDefaultNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->mDefNameSpace:Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    return-object v0
.end method

.method public getNameSpaceByID(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 32
    const/4 v0, 0x0

    .line 33
    .local v0, "nameSpace":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->mNameSpace:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v2

    .line 35
    .local v2, "nsEnum":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 36
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    .line 39
    .local v1, "ns":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->getID()I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 40
    move-object v0, v1

    .line 51
    .end local v1    # "ns":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    :cond_1
    return-object v0
.end method

.method public incrementLevel()V
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->mLevel:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->mLevel:I

    .line 56
    return-void
.end method
