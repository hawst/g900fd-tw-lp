.class public Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DiagramTxBodyHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler$IParagraphHandler;,
        Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler$IBodyPrHandler;
    }
.end annotation


# instance fields
.field private iPara:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler$IParagraphHandler;

.field private paraList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;)V
    .locals 1
    .param p1, "ID"    # I
    .param p2, "dsp"    # Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;

    .prologue
    .line 25
    const-string/jumbo v0, "txBody"

    invoke-direct {p0, p1, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler;->paraList:Ljava/util/ArrayList;

    .line 27
    invoke-virtual {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler;->init(ILcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;)V

    .line 28
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler;->iPara:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler$IParagraphHandler;

    .line 29
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 65
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler;->iPara:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler$IParagraphHandler;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler;->paraList:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler$IParagraphHandler;->setParagraphList(Ljava/util/ArrayList;)V

    .line 67
    return-void
.end method

.method protected init(ILcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;)V
    .locals 6
    .param p1, "ID"    # I
    .param p2, "dsp"    # Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSPHandler;

    .prologue
    .line 32
    const/4 v5, 0x2

    new-array v4, v5, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 34
    .local v4, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyPrHandler;

    invoke-direct {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyPrHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler$IBodyPrHandler;)V

    .line 36
    .local v0, "bodyPrHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyPrHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v5, "bodyPr"

    invoke-direct {v1, v5, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 39
    .local v1, "bodyPrSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v5, 0x0

    aput-object v1, v4, v5

    .line 41
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyParagraphHandler;

    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler;->paraList:Ljava/util/ArrayList;

    invoke-direct {v2, v5}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyParagraphHandler;-><init>(Ljava/util/ArrayList;)V

    .line 43
    .local v2, "paraHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyParagraphHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v5, "p"

    invoke-direct {v3, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 45
    .local v3, "paraSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v5, 0x1

    aput-object v3, v4, v5

    .line 54
    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 55
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 60
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 61
    return-void
.end method
