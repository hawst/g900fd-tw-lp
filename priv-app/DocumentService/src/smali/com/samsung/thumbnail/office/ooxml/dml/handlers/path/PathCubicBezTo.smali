.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathCubicBezTo;
.super Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathSegmentsPointsHandler;
.source "PathCubicBezTo.java"


# instance fields
.field private coordinates:[Ljava/lang/String;

.field private count:I

.field private pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V
    .locals 1
    .param p1, "pathDesc"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    .prologue
    .line 15
    const-string/jumbo v0, "cubicBezTo"

    invoke-direct {p0, v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathSegmentsPointsHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V

    .line 16
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathCubicBezTo;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    .line 17
    return-void
.end method


# virtual methods
.method public point([Ljava/lang/String;)V
    .locals 6
    .param p1, "valuesArray"    # [Ljava/lang/String;

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 31
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathCubicBezTo;->count:I

    if-nez v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathCubicBezTo;->coordinates:[Ljava/lang/String;

    aget-object v1, p1, v3

    aput-object v1, v0, v3

    .line 33
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathCubicBezTo;->coordinates:[Ljava/lang/String;

    aget-object v1, p1, v2

    aput-object v1, v0, v2

    .line 34
    iput v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathCubicBezTo;->count:I

    .line 47
    :goto_0
    return-void

    .line 37
    :cond_0
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathCubicBezTo;->count:I

    if-ne v0, v2, :cond_1

    .line 38
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathCubicBezTo;->coordinates:[Ljava/lang/String;

    aget-object v1, p1, v3

    aput-object v1, v0, v4

    .line 39
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathCubicBezTo;->coordinates:[Ljava/lang/String;

    const/4 v1, 0x3

    aget-object v2, p1, v2

    aput-object v2, v0, v1

    .line 40
    iput v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathCubicBezTo;->count:I

    goto :goto_0

    .line 43
    :cond_1
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathCubicBezTo;->coordinates:[Ljava/lang/String;

    aget-object v1, p1, v3

    aput-object v1, v0, v5

    .line 44
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathCubicBezTo;->coordinates:[Ljava/lang/String;

    const/4 v1, 0x5

    aget-object v2, p1, v2

    aput-object v2, v0, v1

    .line 46
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathCubicBezTo;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathCubicBezTo;->coordinates:[Ljava/lang/String;

    invoke-virtual {v0, v5, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->addSegmentstoPath(I[Ljava/lang/String;)V

    goto :goto_0
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 22
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathSegmentsPointsHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 24
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathCubicBezTo;->coordinates:[Ljava/lang/String;

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathCubicBezTo;->count:I

    .line 26
    return-void
.end method
