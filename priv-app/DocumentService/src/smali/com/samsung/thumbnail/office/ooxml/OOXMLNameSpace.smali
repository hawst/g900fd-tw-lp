.class public Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
.super Ljava/lang/Object;
.source "OOXMLNameSpace.java"


# instance fields
.field private id:I

.field private nameSpaceUri:Ljava/lang/String;

.field private prefix:Ljava/lang/String;

.field private xmlCatagory:Ljava/lang/String;

.field private xmlNS:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1, "nameSpaceUri"    # Ljava/lang/String;
    .param p2, "id"    # I

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->nameSpaceUri:Ljava/lang/String;

    .line 19
    iput p2, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->id:I

    .line 20
    return-void
.end method


# virtual methods
.method public getID()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->id:I

    return v0
.end method

.method public getNameSpaceUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->nameSpaceUri:Ljava/lang/String;

    return-object v0
.end method

.method public getPrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->prefix:Ljava/lang/String;

    return-object v0
.end method

.method public getXMLCatagory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->xmlCatagory:Ljava/lang/String;

    return-object v0
.end method

.method public getXMLNS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->xmlNS:Ljava/lang/String;

    return-object v0
.end method

.method public setID(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->id:I

    .line 32
    return-void
.end method

.method public setNameSpaceUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "nameSpaceUri"    # Ljava/lang/String;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->nameSpaceUri:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public setPrefix(Ljava/lang/String;)V
    .locals 0
    .param p1, "prefix"    # Ljava/lang/String;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->prefix:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public setXMLCatagory(Ljava/lang/String;)V
    .locals 0
    .param p1, "xmlCatagory"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->xmlCatagory:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public setXMLNS(Ljava/lang/String;)V
    .locals 0
    .param p1, "xmlNS"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->xmlNS:Ljava/lang/String;

    .line 48
    return-void
.end method
