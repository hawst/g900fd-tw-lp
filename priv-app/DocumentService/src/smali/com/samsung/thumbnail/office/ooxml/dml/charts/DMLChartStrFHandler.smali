.class public Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrFHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DMLChartStrFHandler.java"


# instance fields
.field val:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 20
    const/16 v0, 0x23

    const-string/jumbo v1, "f"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 17
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrFHandler;->val:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public charactersHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;[CII)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "content"    # [C
    .param p3, "start"    # I
    .param p4, "end"    # I

    .prologue
    .line 26
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2, p3, p4}, Ljava/lang/String;-><init>([CII)V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrFHandler;->val:Ljava/lang/String;

    .line 28
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    if-eqz v0, :cond_0

    .line 34
    :goto_0
    return-void

    .line 32
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrFHandler;->val:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->addExChartFVal(Ljava/lang/String;)V

    goto :goto_0
.end method
