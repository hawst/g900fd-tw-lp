.class public Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;
.super Ljava/lang/Object;
.source "EMR_Arc.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;


# instance fields
.field private RecordSize:I

.field private RecordType:I

.field protected bounds:Landroid/graphics/RectF;

.field private emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

.field fileContent:[B

.field private leftTopX:I

.field private leftTopY:I

.field private localPath:Landroid/graphics/Path;

.field private offset:I

.field private pointLEndX:F

.field private pointLEndY:F

.field private pointLStartX:F

.field private pointLStartY:F

.field private rightBottomX:I

.field private rightBottomY:I


# direct methods
.method public constructor <init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V
    .locals 1
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I
    .param p3, "emrGlobals"    # Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .prologue
    const/4 v0, 0x0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->bounds:Landroid/graphics/RectF;

    .line 65
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->localPath:Landroid/graphics/Path;

    .line 79
    iput-object p3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .line 80
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->fileContent:[B

    .line 81
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    .line 82
    return-void
.end method


# virtual methods
.method protected getAngles()[F
    .locals 14

    .prologue
    .line 436
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->pointLStartY:F

    float-to-double v2, v12

    .line 437
    .local v2, "d1":D
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->pointLStartX:F

    float-to-double v4, v12

    .line 438
    .local v4, "d2":D
    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v12

    double-to-float v1, v12

    .line 439
    .local v1, "f1":F
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->pointLEndY:F

    float-to-double v6, v12

    .line 440
    .local v6, "d3":D
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->pointLEndX:F

    float-to-double v8, v12

    .line 441
    .local v8, "d4":D
    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v12

    double-to-float v10, v12

    .line 444
    .local v10, "f2":F
    const/4 v12, 0x2

    new-array v0, v12, [F

    .line 447
    .local v0, "arrayOfFloat":[F
    const/4 v12, 0x0

    aput v1, v0, v12

    .line 448
    sub-float v11, v10, v1

    .line 449
    .local v11, "f4":F
    const/4 v12, 0x1

    aput v11, v0, v12

    .line 456
    return-object v0
.end method

.method public getRecordSize()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->RecordSize:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->RecordType:I

    return v0
.end method

.method public getleftTopX()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->leftTopX:I

    return v0
.end method

.method public getleftTopY()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->leftTopY:I

    return v0
.end method

.method public getpointLEndX()F
    .locals 1

    .prologue
    .line 242
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->pointLEndX:F

    return v0
.end method

.method public getpointLEndY()F
    .locals 1

    .prologue
    .line 261
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->pointLEndY:F

    return v0
.end method

.method public getpointLStartX()F
    .locals 1

    .prologue
    .line 204
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->pointLStartX:F

    return v0
.end method

.method public getpointLStartY()F
    .locals 1

    .prologue
    .line 223
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->pointLStartY:F

    return v0
.end method

.method public getrightBottomX()I
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->rightBottomX:I

    return v0
.end method

.method public getrightBottomY()I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->rightBottomY:I

    return v0
.end method

.method public prepare()V
    .locals 11

    .prologue
    const/high16 v10, 0x43340000    # 180.0f

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    .line 350
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->getleftTopX()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->getleftTopY()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->getrightBottomX()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->getrightBottomY()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->setBounds(FFFF)V

    .line 352
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->getpointLStartX()F

    move-result v4

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->getpointLStartY()F

    move-result v5

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->getpointLEndX()F

    move-result v6

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->getpointLEndY()F

    move-result v7

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->setStartEnd(FFFF)V

    .line 355
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->getAngles()[F

    move-result-object v0

    .line 356
    .local v0, "arrayOfFloat":[F
    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    iput-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->localPath:Landroid/graphics/Path;

    .line 357
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->bounds:Landroid/graphics/RectF;

    .line 358
    .local v3, "localRectF":Landroid/graphics/RectF;
    const/4 v4, 0x0

    aget v4, v0, v4

    mul-float/2addr v4, v10

    float-to-double v4, v4

    div-double/2addr v4, v8

    double-to-float v1, v4

    .line 359
    .local v1, "i":F
    const/4 v4, 0x1

    aget v4, v0, v4

    mul-float/2addr v4, v10

    float-to-double v4, v4

    div-double/2addr v4, v8

    double-to-float v2, v4

    .line 361
    .local v2, "j":F
    const/4 v4, 0x0

    cmpg-float v4, v2, v4

    if-gez v4, :cond_0

    .line 362
    const/high16 v4, 0x43b40000    # 360.0f

    sub-float v2, v4, v2

    .line 364
    :cond_0
    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->localPath:Landroid/graphics/Path;

    invoke-virtual {v4, v3, v1, v2}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 365
    return-void
.end method

.method public readEmfArc()V
    .locals 15

    .prologue
    .line 278
    const/4 v2, 0x0

    .line 280
    .local v2, "i":I
    const/4 v12, 0x4

    new-array v3, v12, [B

    .line 282
    .local v3, "intConvert":[B
    const/4 v2, 0x0

    :goto_0
    const/4 v12, 0x3

    if-gt v2, v12, :cond_0

    .line 283
    rsub-int/lit8 v12, v2, 0x3

    iget-object v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->fileContent:[B

    iget v14, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    add-int/2addr v14, v2

    aget-byte v13, v13, v14

    aput-byte v13, v3, v12

    .line 282
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 284
    :cond_0
    const/4 v12, 0x0

    invoke-static {v3, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 285
    .local v1, "Type":I
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    add-int/2addr v12, v2

    iput v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    .line 286
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->setRecordType(I)V

    .line 288
    const/4 v2, 0x0

    :goto_1
    const/4 v12, 0x3

    if-gt v2, v12, :cond_1

    .line 289
    rsub-int/lit8 v12, v2, 0x3

    iget-object v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->fileContent:[B

    iget v14, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    add-int/2addr v14, v2

    aget-byte v13, v13, v14

    aput-byte v13, v3, v12

    .line 288
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 290
    :cond_1
    const/4 v12, 0x0

    invoke-static {v3, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 291
    .local v0, "Size":I
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    add-int/2addr v12, v2

    iput v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    .line 292
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->setRecordSize(I)V

    .line 294
    const/4 v2, 0x0

    :goto_2
    const/4 v12, 0x3

    if-gt v2, v12, :cond_2

    .line 295
    rsub-int/lit8 v12, v2, 0x3

    iget-object v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->fileContent:[B

    iget v14, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    add-int/2addr v14, v2

    aget-byte v13, v13, v14

    aput-byte v13, v3, v12

    .line 294
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 296
    :cond_2
    const/4 v12, 0x0

    invoke-static {v3, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v4

    .line 297
    .local v4, "leftTopX":I
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    add-int/2addr v12, v2

    iput v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    .line 298
    invoke-virtual {p0, v4}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->setleftTopX(I)V

    .line 300
    const/4 v2, 0x0

    :goto_3
    const/4 v12, 0x3

    if-gt v2, v12, :cond_3

    .line 301
    rsub-int/lit8 v12, v2, 0x3

    iget-object v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->fileContent:[B

    iget v14, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    add-int/2addr v14, v2

    aget-byte v13, v13, v14

    aput-byte v13, v3, v12

    .line 300
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 302
    :cond_3
    const/4 v12, 0x0

    invoke-static {v3, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v5

    .line 303
    .local v5, "leftTopY":I
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    add-int/2addr v12, v2

    iput v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    .line 304
    invoke-virtual {p0, v5}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->setleftTopY(I)V

    .line 306
    const/4 v2, 0x0

    :goto_4
    const/4 v12, 0x3

    if-gt v2, v12, :cond_4

    .line 307
    rsub-int/lit8 v12, v2, 0x3

    iget-object v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->fileContent:[B

    iget v14, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    add-int/2addr v14, v2

    aget-byte v13, v13, v14

    aput-byte v13, v3, v12

    .line 306
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 308
    :cond_4
    const/4 v12, 0x0

    invoke-static {v3, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v10

    .line 309
    .local v10, "rightBottomX":I
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    add-int/2addr v12, v2

    iput v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    .line 310
    invoke-virtual {p0, v10}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->setrightBottomX(I)V

    .line 312
    const/4 v2, 0x0

    :goto_5
    const/4 v12, 0x3

    if-gt v2, v12, :cond_5

    .line 313
    rsub-int/lit8 v12, v2, 0x3

    iget-object v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->fileContent:[B

    iget v14, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    add-int/2addr v14, v2

    aget-byte v13, v13, v14

    aput-byte v13, v3, v12

    .line 312
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 314
    :cond_5
    const/4 v12, 0x0

    invoke-static {v3, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v11

    .line 315
    .local v11, "rightBottomY":I
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    add-int/2addr v12, v2

    iput v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    .line 316
    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->setrightBottomY(I)V

    .line 318
    const/4 v2, 0x0

    :goto_6
    const/4 v12, 0x3

    if-gt v2, v12, :cond_6

    .line 319
    rsub-int/lit8 v12, v2, 0x3

    iget-object v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->fileContent:[B

    iget v14, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    add-int/2addr v14, v2

    aget-byte v13, v13, v14

    aput-byte v13, v3, v12

    .line 318
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 320
    :cond_6
    const/4 v12, 0x0

    invoke-static {v3, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v8

    .line 321
    .local v8, "pontLStartX":I
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    add-int/2addr v12, v2

    iput v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    .line 322
    invoke-virtual {p0, v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->setpointLStartX(I)V

    .line 324
    const/4 v2, 0x0

    :goto_7
    const/4 v12, 0x3

    if-gt v2, v12, :cond_7

    .line 325
    rsub-int/lit8 v12, v2, 0x3

    iget-object v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->fileContent:[B

    iget v14, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    add-int/2addr v14, v2

    aget-byte v13, v13, v14

    aput-byte v13, v3, v12

    .line 324
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 326
    :cond_7
    const/4 v12, 0x0

    invoke-static {v3, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v9

    .line 327
    .local v9, "pontLStartY":I
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    add-int/2addr v12, v2

    iput v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    .line 328
    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->setpointLStartY(I)V

    .line 330
    const/4 v2, 0x0

    :goto_8
    const/4 v12, 0x3

    if-gt v2, v12, :cond_8

    .line 331
    rsub-int/lit8 v12, v2, 0x3

    iget-object v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->fileContent:[B

    iget v14, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    add-int/2addr v14, v2

    aget-byte v13, v13, v14

    aput-byte v13, v3, v12

    .line 330
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 332
    :cond_8
    const/4 v12, 0x0

    invoke-static {v3, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v6

    .line 333
    .local v6, "pontLEndX":I
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    add-int/2addr v12, v2

    iput v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    .line 334
    invoke-virtual {p0, v6}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->setpointLEndX(I)V

    .line 336
    const/4 v2, 0x0

    :goto_9
    const/4 v12, 0x3

    if-gt v2, v12, :cond_9

    .line 337
    rsub-int/lit8 v12, v2, 0x3

    iget-object v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->fileContent:[B

    iget v14, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    add-int/2addr v14, v2

    aget-byte v13, v13, v14

    aput-byte v13, v3, v12

    .line 336
    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    .line 338
    :cond_9
    const/4 v12, 0x0

    invoke-static {v3, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v7

    .line 339
    .local v7, "pontLEndY":I
    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    add-int/2addr v12, v2

    iput v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->offset:I

    .line 340
    invoke-virtual {p0, v7}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->setpointLEndY(I)V

    .line 342
    return-void
.end method

.method public render(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 395
    const/4 v2, 0x0

    .line 396
    .local v2, "k":I
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;-><init>()V

    .line 397
    .local v0, "PaintArcPropertiesObject":Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 399
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 400
    .local v1, "emrSize":I
    if-lez v1, :cond_1

    .line 401
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 402
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandlePen:I

    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->gethandleNumber()I

    move-result v3

    if-ne v4, v3, :cond_4

    .line 407
    :cond_0
    if-ge v2, v1, :cond_1

    .line 408
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getColor()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/thumbnail/office/emf/Util/toColorAndroid;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 411
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getPenStyle()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/thumbnail/office/emf/Util/toStyleAndroid;->getStyle(I)Landroid/graphics/Paint$Style;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 414
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->localPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 419
    :cond_1
    if-eq v2, v1, :cond_2

    if-nez v1, :cond_3

    .line 420
    :cond_2
    const/16 v3, 0x8

    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v3, v4, :cond_5

    .line 426
    :cond_3
    :goto_1
    return-void

    .line 401
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 423
    :cond_5
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->localPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_1
.end method

.method public setBounds(FFFF)V
    .locals 5
    .param p1, "lx"    # F
    .param p2, "ly"    # F
    .param p3, "rx"    # F
    .param p4, "ry"    # F

    .prologue
    .line 473
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->bounds:Landroid/graphics/RectF;

    .line 474
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->bounds:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v1, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v1, p1, v1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v2, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v2, p2, v2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v3, p3, v3

    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v4, p4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 476
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "RecrdSize"    # I

    .prologue
    .line 119
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->RecordSize:I

    .line 120
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "RecrdType"    # I

    .prologue
    .line 100
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->RecordType:I

    .line 101
    return-void
.end method

.method public setStartEnd(FFFF)V
    .locals 1
    .param p1, "lx"    # F
    .param p2, "ly"    # F
    .param p3, "rx"    # F
    .param p4, "ry"    # F

    .prologue
    .line 380
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v0, p1, v0

    iput v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->pointLStartX:F

    .line 381
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v0, p2, v0

    iput v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->pointLStartY:F

    .line 382
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v0, p3, v0

    iput v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->pointLEndX:F

    .line 383
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v0, p4, v0

    iput v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->pointLEndY:F

    .line 384
    return-void
.end method

.method public setleftTopX(I)V
    .locals 0
    .param p1, "lftTopX"    # I

    .prologue
    .line 138
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->leftTopX:I

    .line 139
    return-void
.end method

.method public setleftTopY(I)V
    .locals 0
    .param p1, "lftTopY"    # I

    .prologue
    .line 157
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->leftTopY:I

    .line 158
    return-void
.end method

.method public setpointLEndX(I)V
    .locals 1
    .param p1, "pontLEndX"    # I

    .prologue
    .line 252
    int-to-float v0, p1

    iput v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->pointLEndX:F

    .line 253
    return-void
.end method

.method public setpointLEndY(I)V
    .locals 1
    .param p1, "pontLEndY"    # I

    .prologue
    .line 271
    int-to-float v0, p1

    iput v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->pointLEndY:F

    .line 272
    return-void
.end method

.method public setpointLStartX(I)V
    .locals 1
    .param p1, "pontLStartX"    # I

    .prologue
    .line 214
    int-to-float v0, p1

    iput v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->pointLStartX:F

    .line 215
    return-void
.end method

.method public setpointLStartY(I)V
    .locals 1
    .param p1, "pontLStartY"    # I

    .prologue
    .line 233
    int-to-float v0, p1

    iput v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->pointLStartY:F

    .line 234
    return-void
.end method

.method public setrightBottomX(I)V
    .locals 0
    .param p1, "rghtBottomX"    # I

    .prologue
    .line 176
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->rightBottomX:I

    .line 177
    return-void
.end method

.method public setrightBottomY(I)V
    .locals 0
    .param p1, "rghtBottomY"    # I

    .prologue
    .line 195
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Arc;->rightBottomY:I

    .line 196
    return-void
.end method
