.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler;
.super Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;
.source "LineRefHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ILineChoiceConsumer;,
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ColorConsumer;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ILineChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 6
    .param p1, "consumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ILineChoiceConsumer;
    .param p2, "shInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 17
    const/16 v1, 0x1f

    const-string/jumbo v2, "lnRef"

    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ColorConsumer;

    invoke-direct {v3, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ColorConsumer;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ILineChoiceConsumer;)V

    const/4 v5, 0x0

    move-object v0, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 19
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 24
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler;->colorConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ColorConsumer;

    .line 25
    .local v0, "clrConsumer":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ColorConsumer;
    iget-object v1, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ColorConsumer;->lineConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ILineChoiceConsumer;

    iget-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ColorConsumer;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    invoke-interface {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ILineChoiceConsumer;->consumeLine(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V

    .line 26
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 31
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 40
    return-void
.end method
