.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FillOverLayHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "FillOverLayHandler.java"


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;)V
    .locals 4
    .param p1, "gsConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;

    .prologue
    const/4 v3, 0x0

    .line 12
    const/16 v1, 0x1f

    const-string/jumbo v2, "fillOverlay"

    invoke-direct {p0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 14
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;

    invoke-direct {v0, p1, v3, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 15
    .local v0, "gradFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FillOverLayHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "gradFill"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    return-void
.end method
