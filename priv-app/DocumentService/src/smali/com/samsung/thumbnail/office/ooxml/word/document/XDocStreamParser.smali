.class public abstract Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
.source "XDocStreamParser.java"


# static fields
.field protected static docNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .prologue
    const/16 v15, 0xa

    const/4 v14, 0x3

    .line 16
    const/16 v12, 0xc

    new-array v12, v12, [Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    sput-object v12, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;->docNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    .line 17
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v12, "http://schemas.openxmlformats.org/wordprocessingml/2006/main"

    const/16 v13, 0x14

    invoke-direct {v10, v12, v13}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 19
    .local v10, "wordMLMain":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const-string/jumbo v12, "wordprocessingml"

    invoke-virtual {v10, v12}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setXMLNS(Ljava/lang/String;)V

    .line 20
    const-string/jumbo v12, "main"

    invoke-virtual {v10, v12}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setXMLCatagory(Ljava/lang/String;)V

    .line 21
    sget-object v12, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;->docNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const/4 v13, 0x0

    aput-object v10, v12, v13

    .line 23
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v12, "http://schemas.openxmlformats.org/officeDocument/2006/relationships"

    invoke-direct {v7, v12, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 25
    .local v7, "relationship":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const-string/jumbo v12, "officeDocument"

    invoke-virtual {v7, v12}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setXMLNS(Ljava/lang/String;)V

    .line 26
    const-string/jumbo v12, "relationships"

    invoke-virtual {v7, v12}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setXMLCatagory(Ljava/lang/String;)V

    .line 27
    sget-object v12, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;->docNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const/4 v13, 0x1

    aput-object v7, v12, v13

    .line 29
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v12, "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"

    const/16 v13, 0x1e

    invoke-direct {v9, v12, v13}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 31
    .local v9, "wordDML":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const-string/jumbo v12, "drawingml"

    invoke-virtual {v9, v12}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setXMLNS(Ljava/lang/String;)V

    .line 32
    const-string/jumbo v12, "wordprocessingDrawing"

    invoke-virtual {v9, v12}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setXMLCatagory(Ljava/lang/String;)V

    .line 33
    sget-object v12, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;->docNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const/4 v13, 0x2

    aput-object v9, v12, v13

    .line 35
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v12, "http://schemas.openxmlformats.org/drawingml/2006/main"

    const/16 v13, 0x1f

    invoke-direct {v1, v12, v13}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 37
    .local v1, "dMLMain":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const-string/jumbo v12, "drawingml"

    invoke-virtual {v1, v12}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setXMLNS(Ljava/lang/String;)V

    .line 38
    const-string/jumbo v12, "main"

    invoke-virtual {v1, v12}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setXMLCatagory(Ljava/lang/String;)V

    .line 39
    sget-object v12, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;->docNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    aput-object v1, v12, v14

    .line 41
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v12, "http://schemas.openxmlformats.org/drawingml/2006/picture"

    const/16 v13, 0x20

    invoke-direct {v2, v12, v13}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 43
    .local v2, "dMLPict":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const-string/jumbo v12, "drawingml"

    invoke-virtual {v2, v12}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setXMLNS(Ljava/lang/String;)V

    .line 44
    const-string/jumbo v12, "picture"

    invoke-virtual {v2, v12}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setXMLCatagory(Ljava/lang/String;)V

    .line 45
    sget-object v12, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;->docNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const/4 v13, 0x4

    aput-object v2, v12, v13

    .line 47
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v12, "urn:schemas-microsoft-com:vml"

    const/16 v13, 0x28

    invoke-direct {v8, v12, v13}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 49
    .local v8, "vML":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    sget-object v12, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;->docNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const/4 v13, 0x5

    aput-object v8, v12, v13

    .line 51
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v12, "urn:schemas-microsoft-com:office:office"

    const/16 v13, 0x32

    invoke-direct {v6, v12, v13}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 53
    .local v6, "office":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    sget-object v12, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;->docNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const/4 v13, 0x6

    aput-object v6, v12, v13

    .line 55
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v12, "http://schemas.openxmlformats.org/drawingml/2006/chart"

    const/16 v13, 0x23

    invoke-direct {v0, v12, v13}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 57
    .local v0, "chart":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const-string/jumbo v12, "drawingml"

    invoke-virtual {v0, v12}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setXMLNS(Ljava/lang/String;)V

    .line 58
    const-string/jumbo v12, "chart"

    invoke-virtual {v0, v12}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setXMLCatagory(Ljava/lang/String;)V

    .line 59
    sget-object v12, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;->docNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const/4 v13, 0x7

    aput-object v0, v12, v13

    .line 61
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v12, "http://schemas.openxmlformats.org/drawingml/2006/diagram"

    const/16 v13, 0xca

    invoke-direct {v3, v12, v13}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 63
    .local v3, "diagram":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const-string/jumbo v12, "drawingml"

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setXMLNS(Ljava/lang/String;)V

    .line 64
    const-string/jumbo v12, "diagram"

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setXMLCatagory(Ljava/lang/String;)V

    .line 65
    sget-object v12, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;->docNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const/16 v13, 0x8

    aput-object v3, v12, v13

    .line 67
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v12, "http://schemas.microsoft.com/office/drawing/2008/diagram"

    const/16 v13, 0xcb

    invoke-direct {v4, v12, v13}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 70
    .local v4, "diagramShape":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const-string/jumbo v12, "drawingml"

    invoke-virtual {v4, v12}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setXMLNS(Ljava/lang/String;)V

    .line 71
    const-string/jumbo v12, "diagram"

    invoke-virtual {v4, v12}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setXMLCatagory(Ljava/lang/String;)V

    .line 72
    sget-object v12, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;->docNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const/16 v13, 0x9

    aput-object v4, v12, v13

    .line 74
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v12, "http://schemas.openxmlformats.org/markup-compatibility/2006"

    invoke-direct {v5, v12, v14}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 76
    .local v5, "mCMl":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    sget-object v12, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;->docNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    aput-object v5, v12, v15

    .line 78
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v12, "urn:schemas-microsoft-com:office:word"

    const/16 v13, 0x29

    invoke-direct {v11, v12, v13}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 81
    .local v11, "wordURN":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    sget-object v12, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;->docNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const/16 v13, 0xb

    aput-object v11, v12, v13

    .line 82
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;)V
    .locals 1
    .param p1, "consumer"    # Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;-><init>(Ljava/lang/Object;)V

    .line 86
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;->docNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;->setPredefinedNameSpaces([Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 87
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;-><init>(Ljava/lang/Object;)V

    .line 91
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;->docNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;->setPredefinedNameSpaces([Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 92
    return-void
.end method
