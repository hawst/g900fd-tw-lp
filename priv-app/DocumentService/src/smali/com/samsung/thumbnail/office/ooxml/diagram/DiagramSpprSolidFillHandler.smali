.class public Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprSolidFillHandler;
.super Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;
.source "DiagramSpprSolidFillHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprSolidFillHandler$ColorConsumer;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;)V
    .locals 6
    .param p1, "fillConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;

    .prologue
    const/4 v4, 0x0

    .line 17
    const/16 v1, 0x1f

    const-string/jumbo v2, "solidFill"

    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprSolidFillHandler$ColorConsumer;

    invoke-direct {v3, p1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprSolidFillHandler$ColorConsumer;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;)V

    move-object v0, p0

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 19
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 24
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprSolidFillHandler;->colorConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprSolidFillHandler$ColorConsumer;

    .line 25
    .local v0, "clrConsumer":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprSolidFillHandler$ColorConsumer;
    iget-object v1, v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprSolidFillHandler$ColorConsumer;->fillConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;

    iget-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprSolidFillHandler$ColorConsumer;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-interface {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;->consumeFill(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V

    .line 26
    return-void
.end method
