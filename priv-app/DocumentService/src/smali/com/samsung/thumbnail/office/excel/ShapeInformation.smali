.class public Lcom/samsung/thumbnail/office/excel/ShapeInformation;
.super Ljava/lang/Object;
.source "ShapeInformation.java"


# instance fields
.field private anchrRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

.field private optRecord:Lorg/apache/poi/ddf/EscherOptRecord;

.field private pictureFilename:Ljava/lang/String;

.field private spRecord:Lorg/apache/poi/ddf/EscherSpRecord;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "level"    # I

    .prologue
    .line 35
    const-string/jumbo v0, "UTF-16LE"

    invoke-direct {p0, p1, v0}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;-><init>(ILjava/lang/String;)V

    .line 36
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1, "level"    # I
    .param p2, "encoding"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object v0, p0, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->optRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    .line 23
    iput-object v0, p0, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->anchrRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    .line 25
    iput-object v0, p0, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->spRecord:Lorg/apache/poi/ddf/EscherSpRecord;

    .line 30
    iput-object v0, p0, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->pictureFilename:Ljava/lang/String;

    .line 42
    return-void
.end method


# virtual methods
.method public getCol1()S
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->anchrRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getCol1()S

    move-result v0

    return v0
.end method

.method public getCol2()S
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->anchrRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getCol2()S

    move-result v0

    return v0
.end method

.method public getOffsetCol1()S
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->anchrRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getDx1()S

    move-result v0

    return v0
.end method

.method public getOffsetCol2()S
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->anchrRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getDx2()S

    move-result v0

    return v0
.end method

.method public getOffsetRow1()S
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->anchrRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getDy1()S

    move-result v0

    return v0
.end method

.method public getOffsetRow2()S
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->anchrRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getDy2()S

    move-result v0

    return v0
.end method

.method public getPictureFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->pictureFilename:Ljava/lang/String;

    return-object v0
.end method

.method public getRotationAngle()I
    .locals 3

    .prologue
    .line 170
    iget-object v1, p0, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->optRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/samsung/thumbnail/util/Utils;->getIntEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;S)I

    move-result v0

    .line 174
    .local v0, "rawRotation":I
    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->rotationAngleToDegree_97Format(I)I

    move-result v1

    return v1
.end method

.method public getRow1()S
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->anchrRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getRow1()S

    move-result v0

    return v0
.end method

.method public getRow2()S
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->anchrRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getRow2()S

    move-result v0

    return v0
.end method

.method public getShapeId()I
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->spRecord:Lorg/apache/poi/ddf/EscherSpRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSpRecord;->getShapeId()I

    move-result v0

    return v0
.end method

.method public setAnchorRecord(Lorg/apache/poi/ddf/EscherClientAnchorRecord;)V
    .locals 0
    .param p1, "anchrRecord"    # Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->anchrRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    .line 67
    return-void
.end method

.method public setOptRecord(Lorg/apache/poi/ddf/EscherOptRecord;)V
    .locals 8
    .param p1, "optRecord"    # Lorg/apache/poi/ddf/EscherOptRecord;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->optRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    .line 46
    iget-object v5, p0, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->optRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    invoke-virtual {v5}, Lorg/apache/poi/ddf/EscherOptRecord;->getEscherProperties()Ljava/util/List;

    move-result-object v2

    .line 47
    .local v2, "propList":Ljava/util/List;, "Ljava/util/List<*>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 48
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 49
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherProperty;

    .line 50
    .local v3, "property":Lorg/apache/poi/ddf/EscherProperty;
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherProperty;->getPropertyNumber()S

    move-result v5

    const/16 v6, 0x105

    if-ne v5, v6, :cond_0

    .line 51
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherProperty;->isComplex()Z

    move-result v5

    if-eqz v5, :cond_0

    move-object v0, v3

    .line 52
    check-cast v0, Lorg/apache/poi/ddf/EscherComplexProperty;

    .line 54
    .local v0, "complexProp":Lorg/apache/poi/ddf/EscherComplexProperty;
    :try_start_0
    new-instance v5, Ljava/lang/String;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherComplexProperty;->getComplexData()[B

    move-result-object v6

    const-string/jumbo v7, "UTF-16LE"

    invoke-direct {v5, v6, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->pictureFilename:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 57
    :catch_0
    move-exception v4

    .line 58
    .local v4, "ueEx":Ljava/io/UnsupportedEncodingException;
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->pictureFilename:Ljava/lang/String;

    goto :goto_0

    .line 63
    .end local v0    # "complexProp":Lorg/apache/poi/ddf/EscherComplexProperty;
    .end local v3    # "property":Lorg/apache/poi/ddf/EscherProperty;
    .end local v4    # "ueEx":Ljava/io/UnsupportedEncodingException;
    :cond_1
    return-void
.end method

.method public setSpRecord(Lorg/apache/poi/ddf/EscherSpRecord;)V
    .locals 0
    .param p1, "spRecord"    # Lorg/apache/poi/ddf/EscherSpRecord;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->spRecord:Lorg/apache/poi/ddf/EscherSpRecord;

    .line 72
    return-void
.end method
