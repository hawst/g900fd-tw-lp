.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
.super Ljava/lang/Object;
.source "LineProperties.java"


# static fields
.field public static final HAS_LINE:I = 0x1

.field public static final LINE_NOT_SET:I = 0xff


# instance fields
.field public color:Ljava/lang/String;

.field public fillProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

.field public hasLine:I

.field public isSchemeClr:Z

.field public lineColor:Ljava/lang/String;

.field public lineDash:I

.field public lineWidth:Ljava/lang/String;

.field public prstDash:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V
    .locals 0
    .param p1, "lineProps"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    if-nez p1, :cond_0

    .line 25
    :goto_0
    return-void

    .line 24
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->assign(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V

    goto :goto_0
.end method


# virtual methods
.method public assign(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V
    .locals 1
    .param p1, "lineProp"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .prologue
    .line 92
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->hasLine:I

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->hasLine:I

    .line 93
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->lineDash:I

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->lineDash:I

    .line 94
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->lineWidth:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->lineWidth:Ljava/lang/String;

    .line 95
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->color:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->color:Ljava/lang/String;

    .line 96
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->isSchemeClr:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->isSchemeClr:Z

    .line 97
    return-void
.end method

.method public getColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->color:Ljava/lang/String;

    return-object v0
.end method

.method public getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->fillProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    return-object v0
.end method

.method public getHasLine()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->hasLine:I

    return v0
.end method

.method public getLineColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->lineColor:Ljava/lang/String;

    return-object v0
.end method

.method public getLineDash()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->lineDash:I

    return v0
.end method

.method public getLineWidth()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->lineWidth:Ljava/lang/String;

    return-object v0
.end method

.method public getPrstDash()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->prstDash:Ljava/lang/String;

    return-object v0
.end method

.method public isSchemeClr()Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->isSchemeClr:Z

    return v0
.end method

.method public setColor(Ljava/lang/String;)V
    .locals 0
    .param p1, "color"    # Ljava/lang/String;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->color:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public setFillProp(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 0
    .param p1, "fillProps"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->fillProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .line 41
    return-void
.end method

.method public setHasLine(I)V
    .locals 0
    .param p1, "hasLine"    # I

    .prologue
    .line 56
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->hasLine:I

    .line 57
    return-void
.end method

.method public setLineColor(Ljava/lang/String;)V
    .locals 0
    .param p1, "lineColor"    # Ljava/lang/String;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->lineColor:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public setLineDash(I)V
    .locals 0
    .param p1, "lineDash"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->lineDash:I

    .line 65
    return-void
.end method

.method public setLineWidth(Ljava/lang/String;)V
    .locals 0
    .param p1, "lineWidth"    # Ljava/lang/String;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->lineWidth:Ljava/lang/String;

    .line 81
    return-void
.end method

.method public setPrstDash(Ljava/lang/String;)V
    .locals 0
    .param p1, "prstDash"    # Ljava/lang/String;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->prstDash:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public setSchemeClr(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->isSchemeClr:Z

    .line 85
    return-void
.end method
