.class public Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;
.super Ljava/lang/Object;
.source "EMR_Polyline16.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;


# instance fields
.field private Size:I

.field private Type:I

.field private aPoints:[B

.field arrayOfPoint:[Landroid/graphics/Point;

.field private bottom:I

.field private count:I

.field private emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

.field fileContent:[B

.field private left:I

.field localPath:Landroid/graphics/Path;

.field private offset:I

.field private right:I

.field private top:I


# direct methods
.method public constructor <init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V
    .locals 1
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I
    .param p3, "emrGlobals"    # Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .prologue
    const/4 v0, 0x0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 339
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->localPath:Landroid/graphics/Path;

    .line 342
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->arrayOfPoint:[Landroid/graphics/Point;

    .line 72
    iput-object p3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .line 73
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->fileContent:[B

    .line 74
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    .line 75
    return-void
.end method


# virtual methods
.method public getBottom()I
    .locals 1

    .prologue
    .line 197
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->bottom:I

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->count:I

    return v0
.end method

.method public getLeft()I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->left:I

    return v0
.end method

.method public getLocalPath()Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->localPath:Landroid/graphics/Path;

    return-object v0
.end method

.method public getRecordSize()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->Size:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->Type:I

    return v0
.end method

.method public getRight()I
    .locals 1

    .prologue
    .line 178
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->right:I

    return v0
.end method

.method public getTop()I
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->top:I

    return v0
.end method

.method public getaPoints()[B
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->aPoints:[B

    return-object v0
.end method

.method public prepare()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 363
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->getaPoints()[B

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->setPoints([B)V

    .line 364
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->arrayOfPoint:[Landroid/graphics/Point;

    array-length v6, v6

    const/4 v7, 0x1

    if-gt v6, v7, :cond_0

    .line 386
    :goto_0
    return-void

    .line 367
    :cond_0
    new-instance v6, Landroid/graphics/Path;

    invoke-direct {v6}, Landroid/graphics/Path;-><init>()V

    iput-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->localPath:Landroid/graphics/Path;

    .line 368
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v6, v6, v8

    iget v6, v6, Landroid/graphics/Point;->x:I

    int-to-float v0, v6

    .line 369
    .local v0, "f1":F
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v6, v6, v8

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v1, v6

    .line 370
    .local v1, "f2":F
    const/4 v2, 0x0

    .line 371
    .local v2, "f3":F
    const/4 v3, 0x0

    .line 373
    .local v3, "f4":F
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->localPath:Landroid/graphics/Path;

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v7, v0, v7

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v8, v1, v8

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 375
    const/4 v4, 0x1

    .line 377
    .local v4, "i":I
    :goto_1
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->arrayOfPoint:[Landroid/graphics/Point;

    array-length v5, v6

    .line 378
    .local v5, "j":I
    if-lt v4, v5, :cond_1

    .line 385
    invoke-virtual {p0, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->setCurrentXY(FF)V

    goto :goto_0

    .line 380
    :cond_1
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v6, v6, v4

    iget v6, v6, Landroid/graphics/Point;->x:I

    int-to-float v2, v6

    .line 381
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v6, v6, v4

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v3, v6

    .line 382
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->localPath:Landroid/graphics/Path;

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v7, v2, v7

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v8, v3, v8

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 383
    add-int/lit8 v4, v4, 0x1

    .line 384
    goto :goto_1
.end method

.method public readpolyline16()V
    .locals 14

    .prologue
    .line 233
    const/4 v5, 0x0

    .line 235
    .local v5, "i":I
    const/4 v11, 0x4

    new-array v6, v11, [B

    .line 237
    .local v6, "intConvert":[B
    const/4 v5, 0x0

    :goto_0
    const/4 v11, 0x3

    if-gt v5, v11, :cond_0

    .line 238
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 237
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 239
    :cond_0
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 240
    .local v1, "Type":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    .line 241
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->setRecordType(I)V

    .line 243
    const/4 v5, 0x0

    :goto_1
    const/4 v11, 0x3

    if-gt v5, v11, :cond_1

    .line 244
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 243
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 245
    :cond_1
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 246
    .local v0, "Size":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    .line 247
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->setRecordSize(I)V

    .line 249
    const/4 v5, 0x0

    :goto_2
    const/4 v11, 0x3

    if-gt v5, v11, :cond_2

    .line 250
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 249
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 251
    :cond_2
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v7

    .line 252
    .local v7, "lef":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    .line 253
    int-to-float v11, v7

    invoke-static {v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->setLeft(I)V

    .line 255
    const/4 v5, 0x0

    :goto_3
    const/4 v11, 0x3

    if-gt v5, v11, :cond_3

    .line 256
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 255
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 257
    :cond_3
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v10

    .line 258
    .local v10, "top":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    .line 259
    int-to-float v11, v10

    invoke-static {v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->setTop(I)V

    .line 261
    const/4 v5, 0x0

    :goto_4
    const/4 v11, 0x3

    if-gt v5, v11, :cond_4

    .line 262
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 261
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 263
    :cond_4
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v9

    .line 264
    .local v9, "right":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    .line 265
    int-to-float v11, v9

    invoke-static {v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->setRight(I)V

    .line 267
    const/4 v5, 0x0

    :goto_5
    const/4 v11, 0x3

    if-gt v5, v11, :cond_5

    .line 268
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 267
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 269
    :cond_5
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v2

    .line 270
    .local v2, "bot":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    .line 271
    int-to-float v11, v2

    invoke-static {v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->setBottom(I)V

    .line 274
    const/4 v5, 0x0

    :goto_6
    const/4 v11, 0x3

    if-gt v5, v11, :cond_6

    .line 275
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 274
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    .line 276
    :cond_6
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v4

    .line 277
    .local v4, "count":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    .line 278
    invoke-virtual {p0, v4}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->setCount(I)V

    .line 280
    add-int/lit8 v3, v0, -0x1c

    .line 281
    .local v3, "buffsize":I
    new-array v8, v3, [B

    .line 282
    .local v8, "pointsBuffer":[B
    const/4 v5, 0x0

    :goto_7
    if-ge v5, v3, :cond_7

    .line 283
    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->fileContent:[B

    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    add-int/2addr v12, v5

    aget-byte v11, v11, v12

    aput-byte v11, v8, v5

    .line 282
    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    .line 285
    :cond_7
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->offset:I

    .line 286
    invoke-virtual {p0, v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->setaPoints([B)V

    .line 287
    return-void
.end method

.method public render(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v5, 0x2

    .line 297
    const/4 v2, 0x0

    .line 298
    .local v2, "i":I
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;-><init>()V

    .line 299
    .local v0, "PaintPolyline16PropertiesObject":Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 301
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 303
    .local v1, "emrSize":I
    if-lez v1, :cond_2

    .line 304
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 305
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandlePen:I

    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->gethandleNumber()I

    move-result v3

    if-ne v4, v3, :cond_5

    .line 310
    :cond_0
    if-ge v2, v1, :cond_2

    .line 311
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getPenStyle()I

    move-result v3

    if-ne v5, v3, :cond_1

    .line 313
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v3

    new-instance v4, Landroid/graphics/DashPathEffect;

    new-array v5, v5, [F

    fill-array-data v5, :array_0

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 318
    :cond_1
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getColor()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/thumbnail/office/emf/Util/toColorAndroid;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 321
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->getLocalPath()Landroid/graphics/Path;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 326
    :cond_2
    if-eq v2, v1, :cond_3

    if-nez v1, :cond_4

    .line 328
    :cond_3
    const/16 v3, 0x8

    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v3, v4, :cond_6

    .line 336
    :cond_4
    :goto_1
    return-void

    .line 304
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 331
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->getLocalPath()Landroid/graphics/Path;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 313
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x40400000    # 3.0f
    .end array-data
.end method

.method public setBottom(I)V
    .locals 0
    .param p1, "b"    # I

    .prologue
    .line 207
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->bottom:I

    .line 208
    return-void
.end method

.method public setCount(I)V
    .locals 0
    .param p1, "cnt"    # I

    .prologue
    .line 131
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->count:I

    .line 132
    return-void
.end method

.method public setCurrentXY(FF)V
    .locals 2
    .param p1, "currX"    # F
    .param p2, "currY"    # F

    .prologue
    .line 353
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v1, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v1, p1, v1

    iput v1, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->currentX:F

    .line 354
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v1, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v1, p2, v1

    iput v1, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->currentY:F

    .line 355
    return-void
.end method

.method public setLeft(I)V
    .locals 0
    .param p1, "l"    # I

    .prologue
    .line 150
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->left:I

    .line 151
    return-void
.end method

.method public setPoints([B)V
    .locals 13
    .param p1, "points"    # [B

    .prologue
    const v12, 0xffff

    const/4 v11, 0x1

    const/4 v10, 0x0

    const v9, 0x8000

    .line 404
    const/4 v2, 0x0

    .line 405
    .local v2, "i":I
    const/4 v4, 0x0

    .line 406
    .local v4, "j":I
    const/4 v5, 0x0

    .line 407
    .local v5, "k":I
    const/4 v6, 0x0

    .line 409
    .local v6, "offset":I
    array-length v7, p1

    shr-int/lit8 v7, v7, 0x2

    new-array v7, v7, [Landroid/graphics/Point;

    iput-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->arrayOfPoint:[Landroid/graphics/Point;

    .line 411
    const/4 v7, 0x2

    new-array v3, v7, [B

    .line 413
    .local v3, "intConvert":[B
    :goto_0
    array-length v7, p1

    if-ge v6, v7, :cond_4

    .line 414
    const/4 v2, 0x0

    :goto_1
    if-gt v2, v11, :cond_0

    .line 415
    rsub-int/lit8 v7, v2, 0x1

    add-int v8, v6, v2

    aget-byte v8, p1, v8

    aput-byte v8, v3, v7

    .line 414
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 416
    :cond_0
    invoke-static {v3, v10}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getSmallInt([BI)I

    move-result v0

    .line 417
    .local v0, "X":I
    add-int/2addr v6, v2

    .line 419
    and-int v7, v0, v9

    if-ne v7, v9, :cond_1

    .line 420
    xor-int/lit8 v7, v0, -0x1

    and-int/2addr v7, v12

    add-int/lit8 v7, v7, 0x1

    mul-int/lit8 v0, v7, -0x1

    .line 423
    :cond_1
    const/4 v4, 0x0

    :goto_2
    if-gt v4, v11, :cond_2

    .line 424
    rsub-int/lit8 v7, v4, 0x1

    add-int v8, v6, v4

    aget-byte v8, p1, v8

    aput-byte v8, v3, v7

    .line 423
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 425
    :cond_2
    invoke-static {v3, v10}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getSmallInt([BI)I

    move-result v1

    .line 427
    .local v1, "Y":I
    and-int v7, v1, v9

    if-ne v7, v9, :cond_3

    .line 428
    xor-int/lit8 v7, v1, -0x1

    and-int/2addr v7, v12

    add-int/lit8 v7, v7, 0x1

    mul-int/lit8 v1, v7, -0x1

    .line 431
    :cond_3
    add-int/2addr v6, v4

    .line 432
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->arrayOfPoint:[Landroid/graphics/Point;

    new-instance v8, Landroid/graphics/Point;

    invoke-direct {v8, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    aput-object v8, v7, v5

    .line 434
    add-int/lit8 v5, v5, 0x1

    .line 435
    goto :goto_0

    .line 437
    .end local v0    # "X":I
    .end local v1    # "Y":I
    :cond_4
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "RecrdSize"    # I

    .prologue
    .line 112
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->Size:I

    .line 113
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "RecrdType"    # I

    .prologue
    .line 93
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->Type:I

    .line 94
    return-void
.end method

.method public setRight(I)V
    .locals 0
    .param p1, "r"    # I

    .prologue
    .line 188
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->right:I

    .line 189
    return-void
.end method

.method public setTop(I)V
    .locals 0
    .param p1, "t"    # I

    .prologue
    .line 169
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->top:I

    .line 170
    return-void
.end method

.method public setaPoints([B)V
    .locals 0
    .param p1, "pt"    # [B

    .prologue
    .line 217
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline16;->aPoints:[B

    .line 218
    return-void
.end method
