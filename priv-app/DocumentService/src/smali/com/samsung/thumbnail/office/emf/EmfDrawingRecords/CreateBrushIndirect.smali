.class public Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/CreateBrushIndirect;
.super Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;
.source "CreateBrushIndirect.java"


# instance fields
.field myCreateBrushIndirectObject:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/CreateBrushIndirect;->myCreateBrushIndirectObject:Landroid/graphics/Paint;

    return-void
.end method


# virtual methods
.method public getMyCreateBrushIndirect()Landroid/graphics/Paint;
    .locals 2

    .prologue
    .line 25
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/CreateBrushIndirect;->myCreateBrushIndirectObject:Landroid/graphics/Paint;

    .line 27
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/CreateBrushIndirect;->myStyle:Landroid/graphics/Paint$Style;

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/CreateBrushIndirect;->myCreateBrushIndirectObject:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/CreateBrushIndirect;->myStyle:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 30
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/CreateBrushIndirect;->myCreateBrushIndirectObject:Landroid/graphics/Paint;

    iget v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/CreateBrushIndirect;->myColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 31
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/CreateBrushIndirect;->myCreateBrushIndirectObject:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 32
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/CreateBrushIndirect;->myCreateBrushIndirectObject:Landroid/graphics/Paint;

    iget v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/CreateBrushIndirect;->myStrokeWidth:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 34
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/CreateBrushIndirect;->myCreateBrushIndirectObject:Landroid/graphics/Paint;

    return-object v0
.end method
