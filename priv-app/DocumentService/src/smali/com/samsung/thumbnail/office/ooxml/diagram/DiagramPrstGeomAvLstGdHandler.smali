.class public Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPrstGeomAvLstGdHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "DiagramPrstGeomAvLstGdHandler.java"


# instance fields
.field pstGeom:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IPstgeomHandler;


# direct methods
.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IPstgeomHandler;)V
    .locals 1
    .param p1, "nsID"    # I
    .param p2, "pstGeom"    # Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IPstgeomHandler;

    .prologue
    .line 19
    const-string/jumbo v0, "gd"

    invoke-direct {p0, p1, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 20
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPrstGeomAvLstGdHandler;->pstGeom:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IPstgeomHandler;

    .line 21
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 26
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 29
    const-string/jumbo v1, "fmla"

    invoke-interface {p3, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 31
    .local v0, "adjvalue":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPrstGeomAvLstGdHandler;->pstGeom:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IPstgeomHandler;

    invoke-interface {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IPstgeomHandler;->setAdjVal(Ljava/lang/String;)V

    .line 32
    return-void
.end method
