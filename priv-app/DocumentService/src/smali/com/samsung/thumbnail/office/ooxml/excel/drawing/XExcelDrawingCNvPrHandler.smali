.class public Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingCNvPrHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelDrawingCNvPrHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 18
    const/16 v0, 0xc9

    const-string/jumbo v1, "cNvPr"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 20
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 25
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 26
    const-string/jumbo v3, "id"

    invoke-interface {p3, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 27
    .local v1, "id":Ljava/lang/String;
    const-string/jumbo v3, "name"

    invoke-interface {p3, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 32
    .local v2, "name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getCurrentCellAnchor()Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    move-result-object v0

    .line 35
    .local v0, "cellAnchor":Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;
    if-eqz v0, :cond_0

    .line 36
    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->setId(Ljava/lang/String;)V

    .line 37
    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->setName(Ljava/lang/String;)V

    .line 39
    :cond_0
    return-void
.end method
