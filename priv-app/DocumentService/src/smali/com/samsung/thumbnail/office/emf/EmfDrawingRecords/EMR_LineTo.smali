.class public Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;
.super Ljava/lang/Object;
.source "EMR_LineTo.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;


# instance fields
.field private Size:I

.field private Type:I

.field private emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

.field fileContent:[B

.field private localPath:Landroid/graphics/Path;

.field private moveToEx_ID:I

.field private offset:I

.field private posX:I

.field private posY:I


# direct methods
.method public constructor <init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V
    .locals 1
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I
    .param p3, "emrGlobals"    # Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->localPath:Landroid/graphics/Path;

    .line 62
    iput-object p3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .line 63
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->fileContent:[B

    .line 64
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->offset:I

    .line 65
    return-void
.end method


# virtual methods
.method public getMoveToEx_ID()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->moveToEx_ID:I

    return v0
.end method

.method public getRecordSize()I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->Size:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->Type:I

    return v0
.end method

.method public getX()I
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->posX:I

    return v0
.end method

.method public getY()I
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->posY:I

    return v0
.end method

.method public prepare()V
    .locals 4

    .prologue
    .line 265
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->localPath:Landroid/graphics/Path;

    .line 266
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->localPath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v1, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->MoveToExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->getX()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v2, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v2, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->MoveToExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->getY()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 268
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->localPath:Landroid/graphics/Path;

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->getX()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v2, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->getY()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 271
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    if-eqz v0, :cond_1

    .line 272
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->pathStart:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 273
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v1, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->MoveToExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->getX()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v2, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v2, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->MoveToExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->getY()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 276
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v1, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->pathStart:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->pathStart:I

    .line 278
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->getX()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v2, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->getY()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 282
    :cond_1
    return-void
.end method

.method public readLineTo()V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x0

    .line 179
    const/4 v4, 0x0

    .line 181
    .local v4, "i":I
    const/4 v6, 0x4

    new-array v5, v6, [B

    .line 183
    .local v5, "intConvert":[B
    const/4 v4, 0x0

    :goto_0
    if-gt v4, v10, :cond_0

    .line 184
    rsub-int/lit8 v6, v4, 0x3

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->fileContent:[B

    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->offset:I

    add-int/2addr v8, v4

    aget-byte v7, v7, v8

    aput-byte v7, v5, v6

    .line 183
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 185
    :cond_0
    invoke-static {v5, v9}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 186
    .local v1, "Type":I
    iget v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->offset:I

    add-int/2addr v6, v4

    iput v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->offset:I

    .line 187
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->setRecordType(I)V

    .line 189
    const/4 v4, 0x0

    :goto_1
    if-gt v4, v10, :cond_1

    .line 190
    rsub-int/lit8 v6, v4, 0x3

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->fileContent:[B

    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->offset:I

    add-int/2addr v8, v4

    aget-byte v7, v7, v8

    aput-byte v7, v5, v6

    .line 189
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 191
    :cond_1
    invoke-static {v5, v9}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 192
    .local v0, "Size":I
    iget v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->offset:I

    add-int/2addr v6, v4

    iput v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->offset:I

    .line 193
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->setRecordSize(I)V

    .line 195
    const/4 v4, 0x0

    :goto_2
    if-gt v4, v10, :cond_2

    .line 196
    rsub-int/lit8 v6, v4, 0x3

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->fileContent:[B

    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->offset:I

    add-int/2addr v8, v4

    aget-byte v7, v7, v8

    aput-byte v7, v5, v6

    .line 195
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 197
    :cond_2
    invoke-static {v5, v9}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v2

    .line 198
    .local v2, "X":I
    iget v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->offset:I

    add-int/2addr v6, v4

    iput v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->offset:I

    .line 199
    invoke-virtual {p0, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->setX(I)V

    .line 201
    const/4 v4, 0x0

    :goto_3
    if-gt v4, v10, :cond_3

    .line 202
    rsub-int/lit8 v6, v4, 0x3

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->fileContent:[B

    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->offset:I

    add-int/2addr v8, v4

    aget-byte v7, v7, v8

    aput-byte v7, v5, v6

    .line 201
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 203
    :cond_3
    invoke-static {v5, v9}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v3

    .line 204
    .local v3, "Y":I
    iget v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->offset:I

    add-int/2addr v6, v4

    iput v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->offset:I

    .line 205
    invoke-virtual {p0, v3}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->setY(I)V

    .line 207
    int-to-float v6, v2

    int-to-float v7, v3

    invoke-virtual {p0, v6, v7}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->setCurrentXY(FF)V

    .line 209
    return-void
.end method

.method public render(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 219
    const/4 v2, 0x0

    .line 220
    .local v2, "i":I
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;-><init>()V

    .line 221
    .local v0, "PaintLineToPropertiesObject":Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 223
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 224
    .local v1, "emrSize":I
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    if-nez v3, :cond_5

    .line 225
    if-lez v1, :cond_1

    .line 226
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 227
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandlePen:I

    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->gethandleNumber()I

    move-result v3

    if-ne v4, v3, :cond_6

    .line 231
    :cond_0
    if-ge v2, v1, :cond_1

    .line 232
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getColor()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/thumbnail/office/emf/Util/toColorAndroid;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 236
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getPenStyle()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/thumbnail/office/emf/Util/toStyleAndroid;->getStyle(I)Landroid/graphics/Paint$Style;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 240
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->localPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 245
    :cond_1
    if-eq v2, v1, :cond_2

    if-nez v1, :cond_4

    .line 247
    :cond_2
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v3, v4, :cond_3

    .line 248
    const/4 v3, -0x1

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 249
    :cond_3
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->localPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 253
    :cond_4
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->MoveToExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->getX()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->setX(I)V

    .line 254
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->MoveToExObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->getY()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->setY(I)V

    .line 256
    :cond_5
    return-void

    .line 226
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public setCurrentXY(FF)V
    .locals 2
    .param p1, "currX"    # F
    .param p2, "currY"    # F

    .prologue
    .line 171
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v1, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v1, p1, v1

    iput v1, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->currentX:F

    .line 172
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v1, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v1, p2, v1

    iput v1, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->currentY:F

    .line 173
    return-void
.end method

.method public setMoveToEx_ID(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 159
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->moveToEx_ID:I

    .line 160
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "RecrdSize"    # I

    .prologue
    .line 102
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->Size:I

    .line 103
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "RecrdType"    # I

    .prologue
    .line 83
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->Type:I

    .line 84
    return-void
.end method

.method public setX(I)V
    .locals 0
    .param p1, "X"    # I

    .prologue
    .line 121
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->posX:I

    .line 122
    return-void
.end method

.method public setY(I)V
    .locals 0
    .param p1, "Y"    # I

    .prologue
    .line 140
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_LineTo;->posY:I

    .line 141
    return-void
.end method
