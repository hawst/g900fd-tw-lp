.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapeBlipFillHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "ShapeBlipFillHandler.java"


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;)V
    .locals 4
    .param p1, "blip"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;

    .prologue
    .line 16
    const-string/jumbo v3, "blipFill"

    invoke-direct {p0, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 18
    const/4 v3, 0x1

    new-array v2, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 19
    .local v2, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipHandler;

    invoke-direct {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;)V

    .line 20
    .local v0, "blipHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v3, "blip"

    invoke-direct {v1, v3, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 22
    .local v1, "blipSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v3, 0x0

    aput-object v1, v2, v3

    .line 24
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapeBlipFillHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 25
    return-void
.end method
