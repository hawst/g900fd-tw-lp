.class Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrSpacing;
.super Ljava/lang/Object;
.source "XDocPPrHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/properties/IXDocPropery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "XPPrSpacing"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;


# direct methods
.method constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;)V
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrSpacing;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consumePropery(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty;)V
    .locals 2
    .param p1, "propery"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty;

    .prologue
    .line 200
    instance-of v1, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 201
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    .line 202
    .local v0, "spacing":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrSpacing;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;

    iget-object v1, v1, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;->paraProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->setSpacing(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;)V

    .line 204
    .end local v0    # "spacing":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;
    :cond_0
    return-void
.end method
