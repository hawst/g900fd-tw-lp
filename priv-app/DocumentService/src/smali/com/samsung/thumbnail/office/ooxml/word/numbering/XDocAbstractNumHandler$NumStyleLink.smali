.class Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler$NumStyleLink;
.super Ljava/lang/Object;
.source "XDocAbstractNumHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "NumStyleLink"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;


# direct methods
.method constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler$NumStyleLink;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V
    .locals 3
    .param p1, "value"    # Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;

    .prologue
    .line 96
    instance-of v1, p1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 97
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    .line 98
    .local v0, "numStyleLink":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler$NumStyleLink;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;

    # getter for: Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;->abstractNum:Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;
    invoke-static {v1}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;->access$000(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;->setNumStyleLink(Ljava/lang/String;)V

    .line 100
    .end local v0    # "numStyleLink":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    :cond_0
    return-void
.end method
