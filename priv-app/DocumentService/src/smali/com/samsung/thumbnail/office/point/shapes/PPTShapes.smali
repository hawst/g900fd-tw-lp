.class public Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;
.super Ljava/lang/Object;
.source "PPTShapes.java"


# static fields
.field public static final ALPHA_VALUE_WATERMARKS:I = 0x4d

.field private static final MAX_ALPHA_VAL:I = 0xff

.field private static final TAG:Ljava/lang/String; = "PPTShapes"

.field private static gradtstartcount:I


# instance fields
.field protected adjval0:I

.field protected adjval1:I

.field protected adjval2:I

.field protected adjval3:I

.field protected adjval4:I

.field protected adjval5:I

.field protected adjval6:I

.field protected adjval7:I

.field protected bitmap:Landroid/graphics/Bitmap;

.field protected bitmapHight:I

.field protected bitmapWidth:I

.field protected flipHorizontal:Z

.field protected flipVertical:Z

.field protected folderPath:Ljava/io/File;

.field protected height:F

.field protected lineColor:Lorg/apache/poi/java/awt/Color;

.field protected lineWidth:D

.field protected paintFill:Landroid/graphics/Paint;

.field protected paintLine:Landroid/graphics/Paint;

.field protected rotation:I

.field protected shapeColor:Lorg/apache/poi/java/awt/Color;

.field protected shapeName:Ljava/lang/String;

.field protected textArea:Landroid/graphics/RectF;

.field protected width:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    sput v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->gradtstartcount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    .line 54
    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    .line 55
    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    .line 56
    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    .line 57
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineWidth:D

    .line 63
    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval0:I

    .line 64
    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval1:I

    .line 65
    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval2:I

    .line 66
    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval3:I

    .line 67
    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval4:I

    .line 68
    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval5:I

    .line 69
    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval6:I

    .line 70
    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval7:I

    .line 74
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v3, v3, v3, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->textArea:Landroid/graphics/RectF;

    .line 79
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    .line 80
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 81
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 82
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    .line 83
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 84
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 85
    return-void
.end method

.method private getAlphaVal(Ljava/lang/String;)I
    .locals 5
    .param p1, "alphaVal"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0xff

    .line 2167
    if-nez p1, :cond_0

    move v0, v2

    .line 2201
    :goto_0
    return v0

    .line 2170
    :cond_0
    const/4 v0, 0x0

    .line 2173
    .local v0, "alphaValue":I
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2179
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 2197
    const/16 v0, 0xff

    goto :goto_0

    .line 2174
    :catch_0
    move-exception v1

    .line 2175
    .local v1, "e":Ljava/lang/NumberFormatException;
    const-string/jumbo v3, "PPTShapes"

    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 2176
    goto :goto_0

    .line 2181
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    :pswitch_0
    mul-int/lit16 v2, v0, 0xff

    div-int/lit8 v0, v2, 0xa

    .line 2182
    goto :goto_0

    .line 2184
    :pswitch_1
    mul-int/lit16 v2, v0, 0xff

    div-int/lit8 v0, v2, 0x64

    .line 2185
    goto :goto_0

    .line 2187
    :pswitch_2
    mul-int/lit16 v2, v0, 0xff

    div-int/lit16 v0, v2, 0x3e8

    .line 2188
    goto :goto_0

    .line 2190
    :pswitch_3
    mul-int/lit16 v2, v0, 0xff

    div-int/lit16 v0, v2, 0x2710

    .line 2191
    goto :goto_0

    .line 2193
    :pswitch_4
    mul-int/lit16 v2, v0, 0xff

    const v3, 0x186a0

    div-int v0, v2, v3

    .line 2194
    goto :goto_0

    .line 2179
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getShapeColor(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)Lorg/apache/poi/java/awt/Color;
    .locals 11
    .param p0, "fillProp"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    .param p1, "theme"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .prologue
    const/16 v10, 0x23

    .line 1631
    const/4 v8, 0x0

    .line 1632
    .local v8, "shapeColor":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 1633
    const-string/jumbo v0, ""

    .line 1634
    .local v0, "clr":Ljava/lang/String;
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 1636
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1640
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_3

    .line 1641
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v4

    .line 1642
    .local v4, "lumMod":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumOff()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 1643
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 1644
    .local v5, "mod":I
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumOff()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 1645
    .local v7, "off":I
    add-int v6, v5, v7

    .line 1646
    .local v6, "modOff":I
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 1648
    .end local v5    # "mod":I
    .end local v6    # "modOff":I
    .end local v7    # "off":I
    :cond_0
    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 1649
    .local v2, "lMod":D
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    invoke-static {v0, v9}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcLuminousColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v1

    .line 1650
    .local v1, "colorWithLum":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v8

    .line 1655
    .end local v0    # "clr":Ljava/lang/String;
    .end local v1    # "colorWithLum":Ljava/lang/String;
    .end local v2    # "lMod":D
    .end local v4    # "lumMod":Ljava/lang/String;
    :cond_1
    :goto_1
    return-object v8

    .line 1638
    .restart local v0    # "clr":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1652
    :cond_3
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v8

    goto :goto_1
.end method

.method private initTextBox()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2136
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->textArea:Landroid/graphics/RectF;

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 2137
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->textArea:Landroid/graphics/RectF;

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 2138
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->textArea:Landroid/graphics/RectF;

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 2139
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->textArea:Landroid/graphics/RectF;

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 2141
    return-void
.end method


# virtual methods
.method protected draw(I)V
    .locals 2
    .param p1, "shapeCount"    # I

    .prologue
    .line 2145
    iget-object v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 2146
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2147
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2148
    invoke-virtual {p0, v0, p1}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 2151
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    :cond_0
    return-void
.end method

.method protected draw(Landroid/graphics/Canvas;I)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeCount"    # I

    .prologue
    .line 2156
    return-void
.end method

.method public drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V
    .locals 9
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p2, "shape_Count"    # I

    .prologue
    const/4 v3, -0x1

    const/4 v8, 0x1

    const/high16 v7, 0x42d80000    # 108.0f

    .line 202
    new-instance v2, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v2, v3}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    iput-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    .line 203
    new-instance v2, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v2, v3}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    iput-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    .line 205
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 207
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v0

    .line 208
    .local v0, "lineClr":Lorg/apache/poi/java/awt/Color;
    if-eqz v0, :cond_0

    .line 209
    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    .line 210
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v4}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v5

    iget-object v6, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 215
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getShapeColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v1

    .line 216
    .local v1, "shapeClr":Lorg/apache/poi/java/awt/Color;
    if-eqz v1, :cond_1

    .line 217
    iput-object v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    .line 218
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v4}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v5

    iget-object v6, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 222
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getShapeName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    .line 223
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 278
    :goto_0
    return-void

    .line 225
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getWidth()F

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 226
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getHeight()F

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 229
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "EllipseRibbon2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "EllipseRibbon"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 231
    :cond_3
    iput v7, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 232
    iput v7, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 237
    :cond_4
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "Moon"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 239
    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    cmpg-float v2, v2, v7

    if-ltz v2, :cond_5

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    cmpg-float v2, v2, v7

    if-gez v2, :cond_6

    .line 240
    :cond_5
    iput v7, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 241
    iput v7, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 251
    :cond_6
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "StraightConnector1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "Line"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 253
    :cond_7
    const/high16 v2, 0x41600000    # 14.0f

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 258
    :cond_8
    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    float-to-int v2, v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    .line 259
    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    float-to-int v2, v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    .line 261
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getAdjustmentValue(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval0:I

    .line 262
    invoke-virtual {p1, v8}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getAdjustmentValue(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval1:I

    .line 263
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getAdjustmentValue(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval2:I

    .line 264
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getAdjustmentValue(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval3:I

    .line 265
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getAdjustmentValue(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval4:I

    .line 266
    const/4 v2, 0x5

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getAdjustmentValue(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval5:I

    .line 267
    const/4 v2, 0x6

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getAdjustmentValue(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval6:I

    .line 268
    const/4 v2, 0x7

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getAdjustmentValue(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval7:I

    .line 270
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->initTextBox()V

    .line 271
    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    if-gtz v2, :cond_9

    .line 272
    iput v8, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    .line 273
    :cond_9
    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    if-gtz v2, :cond_a

    .line 274
    iput v8, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    .line 275
    :cond_a
    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmap:Landroid/graphics/Bitmap;

    goto/16 :goto_0
.end method

.method public drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IFF)V
    .locals 10
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p2, "shape_Count"    # I
    .param p3, "width"    # F
    .param p4, "height"    # F

    .prologue
    const/4 v3, -0x1

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/high16 v7, 0x42d80000    # 108.0f

    .line 295
    new-instance v2, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v2, v3}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    iput-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    .line 296
    new-instance v2, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v2, v3}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    iput-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    .line 297
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 298
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 300
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 302
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v0

    .line 303
    .local v0, "lineClr":Lorg/apache/poi/java/awt/Color;
    if-eqz v0, :cond_0

    .line 304
    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    .line 305
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v4}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v5

    iget-object v6, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 310
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getShapeColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v1

    .line 311
    .local v1, "shapeClr":Lorg/apache/poi/java/awt/Color;
    if-eqz v1, :cond_1

    .line 312
    iput-object v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    .line 313
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v4}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v5

    iget-object v6, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 317
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getShapeName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    .line 318
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 381
    :goto_0
    return-void

    .line 320
    :cond_2
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "TextBox"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 321
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getShapeColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-nez v2, :cond_3

    .line 322
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v2, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 323
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-nez v2, :cond_4

    .line 324
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    invoke-virtual {v2, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 326
    :cond_4
    iput p3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 327
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 330
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "EllipseRibbon2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "EllipseRibbon"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 332
    :cond_5
    iput v7, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 333
    iput v7, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 338
    :cond_6
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "Moon"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 340
    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    cmpg-float v2, v2, v7

    if-ltz v2, :cond_7

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    cmpg-float v2, v2, v7

    if-gez v2, :cond_8

    .line 341
    :cond_7
    iput v7, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 342
    iput v7, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 352
    :cond_8
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "StraightConnector1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "Line"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 354
    :cond_9
    const/high16 v2, 0x41600000    # 14.0f

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 359
    :cond_a
    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    float-to-int v2, v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    .line 360
    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    float-to-int v2, v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    .line 362
    invoke-virtual {p1, v8}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getAdjustmentValue(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval0:I

    .line 363
    invoke-virtual {p1, v9}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getAdjustmentValue(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval1:I

    .line 364
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getAdjustmentValue(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval2:I

    .line 365
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getAdjustmentValue(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval3:I

    .line 366
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getAdjustmentValue(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval4:I

    .line 367
    const/4 v2, 0x5

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getAdjustmentValue(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval5:I

    .line 368
    const/4 v2, 0x6

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getAdjustmentValue(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval6:I

    .line 369
    const/4 v2, 0x7

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getAdjustmentValue(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval7:I

    .line 371
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->initTextBox()V

    .line 373
    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    if-gtz v2, :cond_b

    .line 374
    iput v9, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    .line 375
    :cond_b
    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    if-gtz v2, :cond_c

    .line 376
    iput v9, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    .line 378
    :cond_c
    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmap:Landroid/graphics/Bitmap;

    goto/16 :goto_0
.end method

.method public drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;I)V
    .locals 10
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .param p2, "shape_Count"    # I

    .prologue
    const/16 v9, 0xff

    const/16 v3, 0xf2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/high16 v6, 0x42d80000    # 108.0f

    .line 384
    new-instance v1, Lorg/apache/poi/java/awt/Color;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    .line 385
    new-instance v1, Lorg/apache/poi/java/awt/Color;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    .line 387
    iget-object v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 388
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 389
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    .line 390
    iget-object v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v2}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v4}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 399
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getShapeColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v0

    .line 400
    .local v0, "shapeClr":Lorg/apache/poi/java/awt/Color;
    if-eqz v0, :cond_1

    .line 401
    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    .line 402
    iget-object v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v2}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v4}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 409
    :goto_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getShapeName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    .line 410
    iget-object v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 456
    :goto_2
    return-void

    .line 395
    .end local v0    # "shapeClr":Lorg/apache/poi/java/awt/Color;
    :cond_0
    iget-object v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    invoke-virtual {v1, v9, v3, v3, v3}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto :goto_0

    .line 406
    .restart local v0    # "shapeClr":Lorg/apache/poi/java/awt/Color;
    :cond_1
    iget-object v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v1, v9, v7, v7, v9}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto :goto_1

    .line 412
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getWidth()F

    move-result v1

    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 413
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getHeight()F

    move-result v1

    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 416
    iget-object v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v2, "EllipseRibbon2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v2, "EllipseRibbon"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 418
    :cond_3
    iput v6, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 419
    iput v6, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 424
    :cond_4
    iget-object v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v2, "Moon"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 426
    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    cmpg-float v1, v1, v6

    if-ltz v1, :cond_5

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    cmpg-float v1, v1, v6

    if-gez v1, :cond_6

    .line 427
    :cond_5
    iput v6, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 428
    iput v6, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 434
    :cond_6
    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    .line 435
    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    .line 437
    invoke-virtual {p1, v7}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getAdjustmentValue(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval0:I

    .line 438
    invoke-virtual {p1, v8}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getAdjustmentValue(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval1:I

    .line 439
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getAdjustmentValue(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval2:I

    .line 440
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getAdjustmentValue(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval3:I

    .line 441
    const/4 v1, 0x4

    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getAdjustmentValue(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval4:I

    .line 442
    const/4 v1, 0x5

    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getAdjustmentValue(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval5:I

    .line 443
    const/4 v1, 0x6

    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getAdjustmentValue(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval6:I

    .line 444
    const/4 v1, 0x7

    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getAdjustmentValue(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval7:I

    .line 446
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->initTextBox()V

    .line 448
    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    if-gtz v1, :cond_7

    .line 449
    iput v8, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    .line 450
    :cond_7
    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    if-gtz v1, :cond_8

    .line 451
    iput v8, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    .line 453
    :cond_8
    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmap:Landroid/graphics/Bitmap;

    goto/16 :goto_2
.end method

.method public drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;IFF)V
    .locals 9
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .param p2, "shape_Count"    # I
    .param p3, "width"    # F
    .param p4, "height"    # F

    .prologue
    const/4 v3, -0x1

    const/16 v4, 0xf2

    const/16 v8, 0xff

    const/high16 v7, 0x42d80000    # 108.0f

    .line 461
    new-instance v2, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v2, v3}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    iput-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    .line 462
    new-instance v2, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v2, v3}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    iput-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    .line 464
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 467
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v0

    .line 468
    .local v0, "lineClr":Lorg/apache/poi/java/awt/Color;
    if-eqz v0, :cond_2

    .line 469
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getStrokeStatus()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 470
    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    .line 471
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v4}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v5

    iget-object v6, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 479
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getShapeColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v1

    .line 480
    .local v1, "shapeClr":Lorg/apache/poi/java/awt/Color;
    if-eqz v1, :cond_3

    .line 481
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getFilledStatus()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 482
    iput-object v1, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    .line 483
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v4}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v5

    iget-object v6, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 490
    :cond_1
    :goto_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getShapeName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    .line 491
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 526
    :goto_2
    return-void

    .line 475
    .end local v1    # "shapeClr":Lorg/apache/poi/java/awt/Color;
    :cond_2
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    invoke-virtual {v2, v8, v4, v4, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto :goto_0

    .line 487
    .restart local v1    # "shapeClr":Lorg/apache/poi/java/awt/Color;
    :cond_3
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v2, v8, v8, v8, v8}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto :goto_1

    .line 493
    :cond_4
    iput p3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 494
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 497
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "EllipseRibbon2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "EllipseRibbon"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 499
    :cond_5
    iput v7, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 500
    iput v7, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 505
    :cond_6
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "Moon"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 507
    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    cmpg-float v2, v2, v7

    if-ltz v2, :cond_7

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    cmpg-float v2, v2, v7

    if-gez v2, :cond_8

    .line 508
    :cond_7
    iput v7, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 509
    iput v7, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 515
    :cond_8
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getAdjustmentValue(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval0:I

    .line 516
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getAdjustmentValue(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval1:I

    .line 517
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getAdjustmentValue(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval2:I

    .line 518
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getAdjustmentValue(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval3:I

    .line 519
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getAdjustmentValue(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval4:I

    .line 520
    const/4 v2, 0x5

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getAdjustmentValue(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval5:I

    .line 521
    const/4 v2, 0x6

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getAdjustmentValue(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval6:I

    .line 522
    const/4 v2, 0x7

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getAdjustmentValue(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval7:I

    .line 524
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->initTextBox()V

    goto/16 :goto_2
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V
    .locals 12
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shape_Count"    # I

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    const/high16 v9, 0x42d80000    # 108.0f

    const/16 v8, 0xff

    .line 93
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/AutoShape;->getAnchor()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v0

    .line 95
    .local v0, "anchor":Lorg/apache/poi/java/awt/Rectangle;
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmpl-double v3, v4, v6

    if-eqz v3, :cond_0

    .line 96
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineWidth:D

    .line 98
    :cond_0
    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    iget-wide v4, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineWidth:D

    double-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 100
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/AutoShape;->getShapeName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    .line 102
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/AutoShape;->getRotation()I

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->rotation:I

    .line 106
    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->rotation:I

    const/16 v4, 0x2d

    if-lt v3, v4, :cond_1

    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->rotation:I

    const/16 v4, 0x87

    if-le v3, v4, :cond_2

    :cond_1
    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->rotation:I

    const/16 v4, -0x2d

    if-gt v3, v4, :cond_c

    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->rotation:I

    const/16 v4, -0x87

    if-lt v3, v4, :cond_c

    .line 108
    :cond_2
    invoke-virtual {v0}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v4

    double-to-float v3, v4

    iput v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 109
    invoke-virtual {v0}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v4

    double-to-float v3, v4

    iput v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 117
    :goto_0
    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "EllipseRibbon2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "EllipseRibbon"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 119
    :cond_3
    iput v9, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 120
    iput v9, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 125
    :cond_4
    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "Moon"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 127
    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    cmpg-float v3, v3, v9

    if-ltz v3, :cond_5

    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    cmpg-float v3, v3, v9

    if-gez v3, :cond_6

    .line 128
    :cond_5
    iput v9, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 129
    iput v9, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 136
    :cond_6
    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    float-to-int v3, v3

    iget-wide v4, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineWidth:D

    double-to-int v4, v4

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    iput v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    .line 137
    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    float-to-int v3, v3

    iget-wide v4, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineWidth:D

    double-to-int v4, v4

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    iput v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    .line 140
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 141
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    .line 142
    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v4}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v5

    iget-object v6, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v6

    iget-object v7, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 145
    :cond_7
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/AutoShape;->getFill()Lorg/apache/poi/hslf/model/Fill;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/hslf/model/Fill;->getForegroundColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 146
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/AutoShape;->getFill()Lorg/apache/poi/hslf/model/Fill;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/hslf/model/Fill;->getForegroundColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    .line 147
    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_8

    .line 148
    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v4}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v5

    iget-object v6, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v6

    iget-object v7, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 156
    :cond_8
    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-nez v3, :cond_9

    .line 157
    new-instance v1, Lorg/apache/poi/hslf/model/Fill;

    invoke-direct {v1, p1}, Lorg/apache/poi/hslf/model/Fill;-><init>(Lorg/apache/poi/hslf/model/Shape;)V

    .line 159
    .local v1, "fillvalue":Lorg/apache/poi/hslf/model/Fill;
    invoke-virtual {v1}, Lorg/apache/poi/hslf/model/Fill;->getNofillpropertyvalue()I

    move-result v2

    .line 160
    .local v2, "propertyval":I
    const/high16 v3, 0x100000

    if-ne v2, v3, :cond_d

    .line 162
    new-instance v3, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v3, v8, v8, v8, v11}, Lorg/apache/poi/java/awt/Color;-><init>(IIII)V

    iput-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    .line 163
    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_9

    .line 164
    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v4}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v5

    iget-object v6, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v6

    iget-object v7, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 180
    .end local v1    # "fillvalue":Lorg/apache/poi/hslf/model/Fill;
    .end local v2    # "propertyval":I
    :cond_9
    :goto_1
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/AutoShape;->getFlipVertical()Z

    move-result v3

    iput-boolean v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->flipVertical:Z

    .line 181
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/AutoShape;->getFlipHorizontal()Z

    move-result v3

    iput-boolean v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->flipHorizontal:Z

    .line 183
    invoke-virtual {p1, v11}, Lorg/apache/poi/hslf/model/AutoShape;->getAdjustmentValue(I)I

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval0:I

    .line 184
    invoke-virtual {p1, v10}, Lorg/apache/poi/hslf/model/AutoShape;->getAdjustmentValue(I)I

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval1:I

    .line 185
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, Lorg/apache/poi/hslf/model/AutoShape;->getAdjustmentValue(I)I

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval2:I

    .line 186
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, Lorg/apache/poi/hslf/model/AutoShape;->getAdjustmentValue(I)I

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval3:I

    .line 187
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, Lorg/apache/poi/hslf/model/AutoShape;->getAdjustmentValue(I)I

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval4:I

    .line 188
    const/4 v3, 0x5

    invoke-virtual {p1, v3}, Lorg/apache/poi/hslf/model/AutoShape;->getAdjustmentValue(I)I

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval5:I

    .line 189
    const/4 v3, 0x6

    invoke-virtual {p1, v3}, Lorg/apache/poi/hslf/model/AutoShape;->getAdjustmentValue(I)I

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval6:I

    .line 190
    const/4 v3, 0x7

    invoke-virtual {p1, v3}, Lorg/apache/poi/hslf/model/AutoShape;->getAdjustmentValue(I)I

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval7:I

    .line 192
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->initTextBox()V

    .line 193
    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    if-gtz v3, :cond_a

    .line 194
    iput v10, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    .line 195
    :cond_a
    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    if-gtz v3, :cond_b

    .line 196
    iput v10, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    .line 197
    :cond_b
    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    iget v4, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmap:Landroid/graphics/Bitmap;

    .line 199
    return-void

    .line 111
    :cond_c
    invoke-virtual {v0}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v4

    double-to-float v3, v4

    iput v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 112
    invoke-virtual {v0}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v4

    double-to-float v3, v4

    iput v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    goto/16 :goto_0

    .line 168
    .restart local v1    # "fillvalue":Lorg/apache/poi/hslf/model/Fill;
    .restart local v2    # "propertyval":I
    :cond_d
    const v3, 0x100010

    if-ne v2, v3, :cond_9

    .line 170
    new-instance v3, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v3, v8, v8, v8, v8}, Lorg/apache/poi/java/awt/Color;-><init>(IIII)V

    iput-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    .line 171
    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_9

    .line 172
    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v4}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v5

    iget-object v6, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v6

    iget-object v7, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_1
.end method

.method public drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V
    .locals 39
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "shape_Count"    # I
    .param p3, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .prologue
    .line 1394
    new-instance v15, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct {v15}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 1396
    .local v15, "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    new-instance v5, Lorg/apache/poi/java/awt/Color;

    const/4 v6, -0x1

    invoke-direct {v5, v6}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    .line 1397
    new-instance v5, Lorg/apache/poi/java/awt/Color;

    const/4 v6, -0x1

    invoke-direct {v5, v6}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    .line 1399
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    const/high16 v6, 0x40000000    # 2.0f

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1401
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v14

    .line 1402
    .local v14, "theme":Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    move-result-object v5

    if-eqz v5, :cond_10

    move-object/from16 v0, p1

    iget-boolean v5, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isLineNoFill:Z

    if-nez v5, :cond_10

    .line 1403
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v26

    .line 1404
    .local v26, "fillProperty":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    if-eqz v26, :cond_e

    .line 1405
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_a

    .line 1406
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1414
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->setSchemeClr(Z)V

    .line 1415
    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v24

    .line 1416
    .local v24, "clr":Ljava/lang/String;
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_c

    .line 1417
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v29

    .line 1418
    .local v29, "lumMod":Ljava/lang/String;
    invoke-static/range {v29 .. v29}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v30

    .line 1419
    .local v30, "lMod":D
    invoke-static/range {v30 .. v31}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-static {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcLuminousColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v25

    .line 1420
    .local v25, "colorWithLum":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v6, 0x23

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    .line 1429
    .end local v25    # "colorWithLum":Ljava/lang/String;
    .end local v29    # "lumMod":Ljava/lang/String;
    .end local v30    # "lMod":D
    :goto_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v10}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v5, v6, v7, v10, v0}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 1447
    .end local v24    # "clr":Ljava/lang/String;
    .end local v26    # "fillProperty":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    :goto_2
    move-object/from16 v0, p1

    iget-boolean v5, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isShapeNoFill:Z

    if-nez v5, :cond_19

    .line 1448
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    if-eqz v5, :cond_16

    .line 1449
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 1450
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_11

    .line 1452
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1457
    :goto_3
    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v24

    .line 1458
    .restart local v24    # "clr":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_12

    .line 1459
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v29

    .line 1460
    .restart local v29    # "lumMod":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumOff()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1461
    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v32

    .line 1462
    .local v32, "mod":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumOff()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v34

    .line 1464
    .local v34, "off":I
    add-int v33, v32, v34

    .line 1465
    .local v33, "modOff":I
    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v29

    .line 1467
    .end local v32    # "mod":I
    .end local v33    # "modOff":I
    .end local v34    # "off":I
    :cond_0
    invoke-static/range {v29 .. v29}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v30

    .line 1468
    .restart local v30    # "lMod":D
    invoke-static/range {v30 .. v31}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-static {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcLuminousColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v25

    .line 1469
    .restart local v25    # "colorWithLum":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v6, 0x23

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    .line 1475
    .end local v24    # "clr":Ljava/lang/String;
    .end local v25    # "colorWithLum":Ljava/lang/String;
    .end local v29    # "lumMod":Ljava/lang/String;
    .end local v30    # "lMod":D
    :cond_1
    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-eqz v5, :cond_2

    .line 1476
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v37

    .line 1478
    .local v37, "sizeGradColor":I
    move-object/from16 v0, p0

    move/from16 v1, v37

    move-object/from16 v2, p1

    move-object/from16 v3, p3

    invoke-virtual {v0, v1, v2, v3, v15}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->getGradientColorString(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)[I

    move-result-object v8

    .line 1480
    .local v8, "gradCol":[I
    move-object/from16 v0, p0

    move/from16 v1, v37

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->getPositionString(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)[F

    move-result-object v9

    .line 1482
    .local v9, "pos":[F
    const/4 v4, 0x0

    .line 1483
    .local v4, "mShader":Landroid/graphics/Shader;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getGradFillPathName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "circle"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_13

    .line 1485
    new-instance v4, Landroid/graphics/RadialGradient;

    .end local v4    # "mShader":Landroid/graphics/Shader;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeWidth()J

    move-result-wide v6

    const-wide/16 v16, 0x2

    div-long v6, v6, v16

    long-to-float v5, v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeHeight()J

    move-result-wide v6

    const-wide/16 v16, 0x2

    div-long v6, v6, v16

    long-to-float v6, v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeWidth()J

    move-result-wide v16

    const-wide/16 v18, 0x2

    div-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-float v7, v0

    sget-object v10, Landroid/graphics/Shader$TileMode;->MIRROR:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v4 .. v10}, Landroid/graphics/RadialGradient;-><init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V

    .line 1507
    .end local v14    # "theme":Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    .end local v15    # "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .restart local v4    # "mShader":Landroid/graphics/Shader;
    :goto_5
    if-eqz v4, :cond_2

    .line 1508
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 1546
    .end local v4    # "mShader":Landroid/graphics/Shader;
    .end local v8    # "gradCol":[I
    .end local v9    # "pos":[F
    .end local v37    # "sizeGradColor":I
    :cond_2
    :goto_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v10}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v5, v6, v7, v10, v0}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 1552
    :goto_7
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getPrstShapeName()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    .line 1554
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeHeight()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v28

    .line 1555
    .local v28, "ht":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeWidth()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v38

    .line 1556
    .local v38, "wt":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getRotation()I

    move-result v35

    .line 1558
    .local v35, "rot":I
    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    int-to-float v5, v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 1559
    invoke-static/range {v38 .. v38}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    int-to-float v5, v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 1560
    invoke-static/range {v35 .. v35}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->rotationAngelToDegrees16_16(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->rotation:I

    .line 1563
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "EllipseRibbon2"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "EllipseRibbon"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1565
    :cond_3
    const/high16 v5, 0x42d80000    # 108.0f

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 1566
    const/high16 v5, 0x42d80000    # 108.0f

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 1571
    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "Moon"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1572
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    const/high16 v6, 0x42d80000    # 108.0f

    cmpg-float v5, v5, v6

    if-ltz v5, :cond_5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    const/high16 v6, 0x42d80000    # 108.0f

    cmpg-float v5, v5, v6

    if-gez v5, :cond_6

    .line 1573
    :cond_5
    const/high16 v5, 0x42d80000    # 108.0f

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 1574
    const/high16 v5, 0x42d80000    # 108.0f

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 1580
    :cond_6
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    float-to-int v5, v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineWidth:D

    double-to-int v6, v6

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    .line 1581
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    float-to-int v5, v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineWidth:D

    double-to-int v6, v6

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    .line 1584
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "rightArrow"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "leftArrow"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "upArrow"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "downArrow"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "leftRightArrow"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "upDownArrow"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "notchedRightArrow"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "Ribbon2"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "Ribbon"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 1593
    :cond_7
    const/4 v5, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval0:I

    .line 1594
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal()I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval1:I

    .line 1595
    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval2:I

    .line 1596
    const/4 v5, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval3:I

    .line 1613
    :goto_8
    const/4 v5, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval4:I

    .line 1614
    const/4 v5, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval5:I

    .line 1615
    const/4 v5, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval6:I

    .line 1616
    const/4 v5, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval7:I

    .line 1618
    invoke-direct/range {p0 .. p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->initTextBox()V

    .line 1620
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    if-gtz v5, :cond_8

    .line 1621
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    .line 1622
    :cond_8
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    if-gtz v5, :cond_9

    .line 1623
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    .line 1625
    :cond_9
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmap:Landroid/graphics/Bitmap;

    .line 1628
    return-void

    .line 1408
    .end local v28    # "ht":Ljava/lang/String;
    .end local v35    # "rot":I
    .end local v38    # "wt":Ljava/lang/String;
    .restart local v14    # "theme":Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    .restart local v15    # "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .restart local v26    # "fillProperty":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    :cond_a
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_b

    .line 1409
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineRefColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1412
    :cond_b
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1421
    .restart local v24    # "clr":Ljava/lang/String;
    :cond_c
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getShade()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_d

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getShade()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "255"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 1423
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getShade()Ljava/lang/String;

    move-result-object v36

    .line 1424
    .local v36, "shade":Ljava/lang/String;
    move-object/from16 v0, v24

    move-object/from16 v1, v36

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calculateShadeColor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 1425
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    goto/16 :goto_1

    .line 1427
    .end local v36    # "shade":Ljava/lang/String;
    :cond_d
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v6, 0x23

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    goto/16 :goto_1

    .line 1431
    .end local v24    # "clr":Ljava/lang/String;
    :cond_e
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineRefColor()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_f

    .line 1432
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineRefColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1434
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->setSchemeClr(Z)V

    .line 1435
    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v24

    .line 1436
    .restart local v24    # "clr":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v6, 0x23

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    .line 1437
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v10}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v5, v6, v7, v10, v0}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_2

    .line 1440
    .end local v24    # "clr":Ljava/lang/String;
    :cond_f
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    const/16 v6, 0xff

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v10}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v5, v6, v7, v10, v0}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_2

    .line 1444
    .end local v26    # "fillProperty":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    :cond_10
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    goto/16 :goto_2

    .line 1455
    :cond_11
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1471
    .restart local v24    # "clr":Ljava/lang/String;
    :cond_12
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v6, 0x23

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    goto/16 :goto_4

    .line 1490
    .end local v24    # "clr":Ljava/lang/String;
    .restart local v4    # "mShader":Landroid/graphics/Shader;
    .restart local v8    # "gradCol":[I
    .restart local v9    # "pos":[F
    .restart local v37    # "sizeGradColor":I
    :cond_13
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getGradFillPathName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "rect"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getGradFillPathName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "shape"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 1494
    :cond_14
    new-instance v4, Landroid/graphics/SweepGradient;

    .end local v4    # "mShader":Landroid/graphics/Shader;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeWidth()J

    move-result-wide v6

    const-wide/16 v16, 0x2

    div-long v6, v6, v16

    long-to-float v5, v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeHeight()J

    move-result-wide v6

    const-wide/16 v16, 0x2

    div-long v6, v6, v16

    long-to-float v6, v6

    invoke-direct {v4, v5, v6, v8, v9}, Landroid/graphics/SweepGradient;-><init>(FF[I[F)V

    .restart local v4    # "mShader":Landroid/graphics/Shader;
    goto/16 :goto_5

    .line 1499
    :cond_15
    new-instance v4, Landroid/graphics/LinearGradient;

    .end local v4    # "mShader":Landroid/graphics/Shader;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeXvalue()J

    move-result-wide v6

    long-to-float v11, v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeYvalue()J

    move-result-wide v6

    long-to-float v12, v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeWidth()J

    move-result-wide v6

    long-to-float v13, v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeHeight()J

    move-result-wide v6

    long-to-float v14, v6

    sget-object v17, Landroid/graphics/Shader$TileMode;->MIRROR:Landroid/graphics/Shader$TileMode;

    .end local v14    # "theme":Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    move-object v10, v4

    move-object v15, v8

    move-object/from16 v16, v9

    invoke-direct/range {v10 .. v17}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    .end local v15    # "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .restart local v4    # "mShader":Landroid/graphics/Shader;
    goto/16 :goto_5

    .line 1510
    .end local v4    # "mShader":Landroid/graphics/Shader;
    .end local v8    # "gradCol":[I
    .end local v9    # "pos":[F
    .end local v37    # "sizeGradColor":I
    .restart local v14    # "theme":Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    .restart local v15    # "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    :cond_16
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillRefColor()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 1511
    const/4 v12, -0x1

    .line 1512
    .local v12, "colorLstSz":I
    const/4 v13, -0x1

    .line 1513
    .local v13, "id":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillRefColor()Ljava/lang/String;

    move-result-object v11

    .line 1514
    .local v11, "fillRefColor":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillRefId()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_17

    .line 1515
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillRefId()Ljava/lang/String;

    move-result-object v27

    .line 1517
    .local v27, "fillRefId":Ljava/lang/String;
    const-string/jumbo v5, "1"

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_18

    const-string/jumbo v5, "0"

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_18

    .line 1518
    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v13, v5, -0x2

    .line 1519
    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getGradColorLst()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 1522
    const/4 v5, -0x1

    if-eq v12, v5, :cond_17

    const/4 v5, -0x1

    if-eq v13, v5, :cond_17

    move-object/from16 v10, p0

    .line 1523
    invoke-virtual/range {v10 .. v15}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->getGradientColorFromTheme(Ljava/lang/String;IILcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)[I

    move-result-object v8

    .line 1525
    .restart local v8    # "gradCol":[I
    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13, v14}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->getGradientPositionFromTheme(IILcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)[F

    move-result-object v9

    .line 1527
    .restart local v9    # "pos":[F
    new-instance v4, Landroid/graphics/LinearGradient;

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/high16 v20, 0x42c80000    # 100.0f

    sget-object v23, Landroid/graphics/Shader$TileMode;->MIRROR:Landroid/graphics/Shader$TileMode;

    move-object/from16 v16, v4

    move-object/from16 v21, v8

    move-object/from16 v22, v9

    invoke-direct/range {v16 .. v23}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    .line 1529
    .restart local v4    # "mShader":Landroid/graphics/Shader;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 1538
    .end local v4    # "mShader":Landroid/graphics/Shader;
    .end local v8    # "gradCol":[I
    .end local v9    # "pos":[F
    .end local v27    # "fillRefId":Ljava/lang/String;
    :cond_17
    :goto_9
    invoke-virtual {v14, v11}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 1539
    invoke-virtual {v14, v11}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1540
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v6, 0x23

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    goto/16 :goto_6

    .line 1531
    .restart local v27    # "fillRefId":Ljava/lang/String;
    :cond_18
    invoke-virtual {v14, v11}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_17

    .line 1532
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v5

    invoke-virtual {v5, v11}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1534
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    goto :goto_9

    .line 1549
    .end local v11    # "fillRefColor":Ljava/lang/String;
    .end local v12    # "colorLstSz":I
    .end local v13    # "id":I
    .end local v27    # "fillRefId":Ljava/lang/String;
    :cond_19
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_7

    .line 1597
    .end local v14    # "theme":Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    .end local v15    # "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .restart local v28    # "ht":Ljava/lang/String;
    .restart local v35    # "rot":I
    .restart local v38    # "wt":Ljava/lang/String;
    :cond_1a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "RightArrowCallout"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1b

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "LeftArrowCallout"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1b

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "UpArrowCallout"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1b

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "DownArrowCallout"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1b

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "LeftRightArrowCallout"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1c

    .line 1602
    :cond_1b
    const/4 v5, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval0:I

    .line 1603
    const/4 v5, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval1:I

    .line 1604
    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval2:I

    .line 1605
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal()I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval3:I

    goto/16 :goto_8

    .line 1607
    :cond_1c
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal()I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval0:I

    .line 1608
    const/4 v5, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval1:I

    .line 1609
    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval2:I

    .line 1610
    const/4 v5, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval3:I

    goto/16 :goto_8
.end method

.method public drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V
    .locals 39
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "shape_Count"    # I
    .param p3, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    .prologue
    .line 1109
    new-instance v15, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct {v15}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 1111
    .local v15, "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    new-instance v5, Lorg/apache/poi/java/awt/Color;

    const/4 v6, -0x1

    invoke-direct {v5, v6}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    .line 1112
    new-instance v5, Lorg/apache/poi/java/awt/Color;

    const/4 v6, -0x1

    invoke-direct {v5, v6}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    .line 1114
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    const/high16 v6, 0x40000000    # 2.0f

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1115
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v14

    .line 1117
    .local v14, "theme":Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    move-result-object v5

    if-eqz v5, :cond_10

    move-object/from16 v0, p1

    iget-boolean v5, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isLineNoFill:Z

    if-nez v5, :cond_10

    .line 1118
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v26

    .line 1119
    .local v26, "filProp":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    if-eqz v26, :cond_e

    .line 1120
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_a

    .line 1121
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1129
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->setSchemeClr(Z)V

    .line 1130
    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v24

    .line 1131
    .local v24, "clr":Ljava/lang/String;
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_c

    .line 1132
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v29

    .line 1134
    .local v29, "lumMod":Ljava/lang/String;
    invoke-static/range {v29 .. v29}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v30

    .line 1135
    .local v30, "lMod":D
    invoke-static/range {v30 .. v31}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-static {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcLuminousColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v25

    .line 1136
    .local v25, "colorWithLum":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    .line 1145
    .end local v25    # "colorWithLum":Ljava/lang/String;
    .end local v29    # "lumMod":Ljava/lang/String;
    .end local v30    # "lMod":D
    :goto_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v10}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v5, v6, v7, v10, v0}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 1163
    .end local v24    # "clr":Ljava/lang/String;
    .end local v26    # "filProp":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    if-eqz v5, :cond_16

    move-object/from16 v0, p1

    iget-boolean v5, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isShapeNoFill:Z

    if-nez v5, :cond_16

    .line 1164
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 1165
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_11

    .line 1167
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1173
    :goto_3
    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v24

    .line 1174
    .restart local v24    # "clr":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_12

    .line 1175
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v29

    .line 1176
    .restart local v29    # "lumMod":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumOff()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1177
    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v32

    .line 1178
    .local v32, "mod":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumOff()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v34

    .line 1180
    .local v34, "off":I
    add-int v33, v32, v34

    .line 1181
    .local v33, "modOff":I
    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v29

    .line 1183
    .end local v32    # "mod":I
    .end local v33    # "modOff":I
    .end local v34    # "off":I
    :cond_0
    invoke-static/range {v29 .. v29}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v30

    .line 1184
    .restart local v30    # "lMod":D
    invoke-static/range {v30 .. v31}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-static {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcLuminousColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v25

    .line 1185
    .restart local v25    # "colorWithLum":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    .line 1190
    .end local v25    # "colorWithLum":Ljava/lang/String;
    .end local v29    # "lumMod":Ljava/lang/String;
    .end local v30    # "lMod":D
    :goto_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getAlpha()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->getAlphaVal(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v10}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v5, v6, v7, v10, v0}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 1195
    .end local v24    # "clr":Ljava/lang/String;
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-eqz v5, :cond_2

    .line 1196
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v37

    .line 1198
    .local v37, "sizeGradColor":I
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v5

    move-object/from16 v0, p0

    move/from16 v1, v37

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2, v5, v15}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->getGradientColorString(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)[I

    move-result-object v8

    .line 1200
    .local v8, "gradCol":[I
    move-object/from16 v0, p0

    move/from16 v1, v37

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->getPositionString(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)[F

    move-result-object v9

    .line 1201
    .local v9, "pos":[F
    const/4 v4, 0x0

    .line 1202
    .local v4, "mShader":Landroid/graphics/Shader;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getGradFillPathName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "circle"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_13

    .line 1203
    new-instance v4, Landroid/graphics/RadialGradient;

    .end local v4    # "mShader":Landroid/graphics/Shader;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeWidth()J

    move-result-wide v6

    const-wide/16 v16, 0x2

    div-long v6, v6, v16

    long-to-float v5, v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeHeight()J

    move-result-wide v6

    const-wide/16 v16, 0x2

    div-long v6, v6, v16

    long-to-float v6, v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeWidth()J

    move-result-wide v16

    const-wide/16 v18, 0x2

    div-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-float v7, v0

    sget-object v10, Landroid/graphics/Shader$TileMode;->MIRROR:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v4 .. v10}, Landroid/graphics/RadialGradient;-><init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V

    .line 1224
    .end local v14    # "theme":Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    .end local v15    # "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .restart local v4    # "mShader":Landroid/graphics/Shader;
    :goto_5
    if-eqz v4, :cond_2

    .line 1225
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 1310
    .end local v4    # "mShader":Landroid/graphics/Shader;
    .end local v8    # "gradCol":[I
    .end local v9    # "pos":[F
    .end local v37    # "sizeGradColor":I
    :cond_2
    :goto_6
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getPrstShapeName()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    .line 1312
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeHeight()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v28

    .line 1313
    .local v28, "ht":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeWidth()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v38

    .line 1314
    .local v38, "wt":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getRotation()I

    move-result v35

    .line 1316
    .local v35, "rot":I
    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    int-to-float v5, v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 1317
    invoke-static/range {v38 .. v38}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    int-to-float v5, v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 1318
    invoke-static/range {v35 .. v35}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->rotationAngelToDegrees16_16(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->rotation:I

    .line 1321
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "EllipseRibbon2"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "EllipseRibbon"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1323
    :cond_3
    const/high16 v5, 0x42d80000    # 108.0f

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 1324
    const/high16 v5, 0x42d80000    # 108.0f

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 1330
    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "Moon"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1332
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    const/high16 v6, 0x42d80000    # 108.0f

    cmpg-float v5, v5, v6

    if-ltz v5, :cond_5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    const/high16 v6, 0x42d80000    # 108.0f

    cmpg-float v5, v5, v6

    if-gez v5, :cond_6

    .line 1333
    :cond_5
    const/high16 v5, 0x42d80000    # 108.0f

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 1334
    const/high16 v5, 0x42d80000    # 108.0f

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 1341
    :cond_6
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    float-to-int v5, v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineWidth:D

    double-to-int v6, v6

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    .line 1342
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    float-to-int v5, v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineWidth:D

    double-to-int v6, v6

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    .line 1345
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "rightArrow"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "leftArrow"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "upArrow"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "downArrow"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "leftRightArrow"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "upDownArrow"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "notchedRightArrow"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "Ribbon2"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "Ribbon"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1d

    .line 1354
    :cond_7
    const/4 v5, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval0:I

    .line 1355
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal()I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval1:I

    .line 1356
    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval2:I

    .line 1357
    const/4 v5, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval3:I

    .line 1374
    :goto_7
    const/4 v5, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval4:I

    .line 1375
    const/4 v5, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval5:I

    .line 1376
    const/4 v5, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval6:I

    .line 1377
    const/4 v5, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval7:I

    .line 1379
    invoke-direct/range {p0 .. p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->initTextBox()V

    .line 1381
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    if-gtz v5, :cond_8

    .line 1382
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    .line 1383
    :cond_8
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    if-gtz v5, :cond_9

    .line 1384
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    .line 1386
    :cond_9
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmap:Landroid/graphics/Bitmap;

    .line 1390
    return-void

    .line 1123
    .end local v28    # "ht":Ljava/lang/String;
    .end local v35    # "rot":I
    .end local v38    # "wt":Ljava/lang/String;
    .restart local v14    # "theme":Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    .restart local v15    # "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .restart local v26    # "filProp":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    :cond_a
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_b

    .line 1124
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineRefColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1127
    :cond_b
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1137
    .restart local v24    # "clr":Ljava/lang/String;
    :cond_c
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getShade()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_d

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getShade()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "255"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 1139
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getShade()Ljava/lang/String;

    move-result-object v36

    .line 1140
    .local v36, "shade":Ljava/lang/String;
    move-object/from16 v0, v24

    move-object/from16 v1, v36

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calculateShadeColor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 1141
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    goto/16 :goto_1

    .line 1143
    .end local v36    # "shade":Ljava/lang/String;
    :cond_d
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v6, 0x23

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    goto/16 :goto_1

    .line 1147
    .end local v24    # "clr":Ljava/lang/String;
    :cond_e
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineRefColor()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_f

    .line 1148
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineRefColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1150
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->setSchemeClr(Z)V

    .line 1151
    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v24

    .line 1152
    .restart local v24    # "clr":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    .line 1153
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v10}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v5, v6, v7, v10, v0}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_2

    .line 1156
    .end local v24    # "clr":Ljava/lang/String;
    :cond_f
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    const/16 v6, 0xff

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v10}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v5, v6, v7, v10, v0}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_2

    .line 1160
    .end local v26    # "filProp":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    :cond_10
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    goto/16 :goto_2

    .line 1171
    :cond_11
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1187
    .restart local v24    # "clr":Ljava/lang/String;
    :cond_12
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    goto/16 :goto_4

    .line 1208
    .end local v24    # "clr":Ljava/lang/String;
    .restart local v4    # "mShader":Landroid/graphics/Shader;
    .restart local v8    # "gradCol":[I
    .restart local v9    # "pos":[F
    .restart local v37    # "sizeGradColor":I
    :cond_13
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getGradFillPathName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "rect"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getGradFillPathName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "shape"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 1212
    :cond_14
    new-instance v4, Landroid/graphics/SweepGradient;

    .end local v4    # "mShader":Landroid/graphics/Shader;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeWidth()J

    move-result-wide v6

    const-wide/16 v16, 0x2

    div-long v6, v6, v16

    long-to-float v5, v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeHeight()J

    move-result-wide v6

    const-wide/16 v16, 0x2

    div-long v6, v6, v16

    long-to-float v6, v6

    invoke-direct {v4, v5, v6, v8, v9}, Landroid/graphics/SweepGradient;-><init>(FF[I[F)V

    .restart local v4    # "mShader":Landroid/graphics/Shader;
    goto/16 :goto_5

    .line 1217
    :cond_15
    new-instance v4, Landroid/graphics/LinearGradient;

    .end local v4    # "mShader":Landroid/graphics/Shader;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeXvalue()J

    move-result-wide v6

    long-to-float v11, v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeYvalue()J

    move-result-wide v6

    long-to-float v12, v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeWidth()J

    move-result-wide v6

    long-to-float v13, v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeHeight()J

    move-result-wide v6

    long-to-float v14, v6

    sget-object v17, Landroid/graphics/Shader$TileMode;->MIRROR:Landroid/graphics/Shader$TileMode;

    .end local v14    # "theme":Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    move-object v10, v4

    move-object v15, v8

    move-object/from16 v16, v9

    invoke-direct/range {v10 .. v17}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    .end local v15    # "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .restart local v4    # "mShader":Landroid/graphics/Shader;
    goto/16 :goto_5

    .line 1227
    .end local v4    # "mShader":Landroid/graphics/Shader;
    .end local v8    # "gradCol":[I
    .end local v9    # "pos":[F
    .end local v37    # "sizeGradColor":I
    .restart local v14    # "theme":Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    .restart local v15    # "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    :cond_16
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillRefColor()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1c

    move-object/from16 v0, p1

    iget-boolean v5, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isShapeNoFill:Z

    if-nez v5, :cond_1c

    .line 1228
    const/4 v12, -0x1

    .line 1229
    .local v12, "colorLstSz":I
    const/4 v13, -0x1

    .line 1230
    .local v13, "id":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillRefColor()Ljava/lang/String;

    move-result-object v11

    .line 1233
    .local v11, "fillRefColor":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillRefId()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_17

    .line 1234
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillRefId()Ljava/lang/String;

    move-result-object v27

    .line 1236
    .local v27, "fillRefId":Ljava/lang/String;
    const-string/jumbo v5, "1"

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1b

    const-string/jumbo v5, "0"

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1b

    .line 1237
    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v13, v5, -0x2

    .line 1238
    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getGradColorLst()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v12

    move-object/from16 v10, p0

    .line 1241
    invoke-virtual/range {v10 .. v15}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->getGradientColorFromTheme(Ljava/lang/String;IILcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)[I

    move-result-object v8

    .line 1243
    .restart local v8    # "gradCol":[I
    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13, v14}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->getGradientPositionFromTheme(IILcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)[F

    move-result-object v9

    .line 1246
    .restart local v9    # "pos":[F
    const/4 v4, 0x0

    .line 1247
    .restart local v4    # "mShader":Landroid/graphics/Shader;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getGradFillPathName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "circle"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_18

    .line 1249
    new-instance v4, Landroid/graphics/RadialGradient;

    .end local v4    # "mShader":Landroid/graphics/Shader;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeWidth()J

    move-result-wide v6

    const-wide/16 v16, 0x2

    div-long v6, v6, v16

    long-to-float v5, v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeHeight()J

    move-result-wide v6

    const-wide/16 v16, 0x2

    div-long v6, v6, v16

    long-to-float v6, v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeWidth()J

    move-result-wide v16

    const-wide/16 v18, 0x2

    div-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-float v7, v0

    sget-object v10, Landroid/graphics/Shader$TileMode;->MIRROR:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v4 .. v10}, Landroid/graphics/RadialGradient;-><init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V

    .line 1269
    .restart local v4    # "mShader":Landroid/graphics/Shader;
    :goto_8
    if-eqz v4, :cond_17

    .line 1270
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 1281
    .end local v4    # "mShader":Landroid/graphics/Shader;
    .end local v8    # "gradCol":[I
    .end local v9    # "pos":[F
    .end local v27    # "fillRefId":Ljava/lang/String;
    :cond_17
    :goto_9
    invoke-virtual {v14, v11}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 1283
    invoke-virtual {v14, v11}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1284
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    .line 1285
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v10}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v5, v6, v7, v10, v0}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_6

    .line 1254
    .restart local v4    # "mShader":Landroid/graphics/Shader;
    .restart local v8    # "gradCol":[I
    .restart local v9    # "pos":[F
    .restart local v27    # "fillRefId":Ljava/lang/String;
    :cond_18
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getGradFillPathName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "rect"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_19

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getGradFillPathName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "shape"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 1258
    :cond_19
    new-instance v4, Landroid/graphics/SweepGradient;

    .end local v4    # "mShader":Landroid/graphics/Shader;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeWidth()J

    move-result-wide v6

    const-wide/16 v16, 0x2

    div-long v6, v6, v16

    long-to-float v5, v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeHeight()J

    move-result-wide v6

    const-wide/16 v16, 0x2

    div-long v6, v6, v16

    long-to-float v6, v6

    invoke-direct {v4, v5, v6, v8, v9}, Landroid/graphics/SweepGradient;-><init>(FF[I[F)V

    .restart local v4    # "mShader":Landroid/graphics/Shader;
    goto/16 :goto_8

    .line 1262
    :cond_1a
    new-instance v4, Landroid/graphics/LinearGradient;

    .end local v4    # "mShader":Landroid/graphics/Shader;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeXvalue()J

    move-result-wide v6

    long-to-float v0, v6

    move/from16 v17, v0

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeYvalue()J

    move-result-wide v6

    long-to-float v0, v6

    move/from16 v18, v0

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeWidth()J

    move-result-wide v6

    long-to-float v0, v6

    move/from16 v19, v0

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeHeight()J

    move-result-wide v6

    long-to-float v0, v6

    move/from16 v20, v0

    sget-object v23, Landroid/graphics/Shader$TileMode;->MIRROR:Landroid/graphics/Shader$TileMode;

    move-object/from16 v16, v4

    move-object/from16 v21, v8

    move-object/from16 v22, v9

    invoke-direct/range {v16 .. v23}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    .restart local v4    # "mShader":Landroid/graphics/Shader;
    goto/16 :goto_8

    .line 1271
    .end local v4    # "mShader":Landroid/graphics/Shader;
    .end local v8    # "gradCol":[I
    .end local v9    # "pos":[F
    :cond_1b
    invoke-virtual {v14, v11}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_17

    .line 1273
    invoke-virtual {v14, v11}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1274
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    .line 1275
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v10}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v5, v6, v7, v10, v0}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_9

    .line 1290
    .end local v11    # "fillRefColor":Ljava/lang/String;
    .end local v12    # "colorLstSz":I
    .end local v13    # "id":I
    .end local v27    # "fillRefId":Ljava/lang/String;
    :cond_1c
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    goto/16 :goto_6

    .line 1358
    .end local v14    # "theme":Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    .end local v15    # "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .restart local v28    # "ht":Ljava/lang/String;
    .restart local v35    # "rot":I
    .restart local v38    # "wt":Ljava/lang/String;
    :cond_1d
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "RightArrowCallout"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1e

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "LeftArrowCallout"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1e

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "UpArrowCallout"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1e

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "DownArrowCallout"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1e

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "LeftRightArrowCallout"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1f

    .line 1363
    :cond_1e
    const/4 v5, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval0:I

    .line 1364
    const/4 v5, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval1:I

    .line 1365
    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval2:I

    .line 1366
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal()I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval3:I

    goto/16 :goto_7

    .line 1368
    :cond_1f
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal()I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval0:I

    .line 1369
    const/4 v5, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval1:I

    .line 1370
    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval2:I

    .line 1371
    const/4 v5, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjVal(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval3:I

    goto/16 :goto_7
.end method

.method public drawXSSExcelShape(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 33
    .param p1, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p2, "shape_Count"    # I
    .param p3, "width"    # F
    .param p4, "height"    # F
    .param p5, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 766
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 767
    move/from16 v0, p4

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 768
    new-instance v5, Lorg/apache/poi/java/awt/Color;

    const/4 v6, -0x1

    invoke-direct {v5, v6}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    .line 769
    new-instance v5, Lorg/apache/poi/java/awt/Color;

    const/4 v6, -0x1

    invoke-direct {v5, v6}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    .line 771
    new-instance v16, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 775
    .local v16, "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    const/high16 v25, 0x40000000    # 2.0f

    .line 776
    .local v25, "lineWidth":F
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 778
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v15

    .line 779
    .local v15, "theme":Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    move-result-object v24

    .line 780
    .local v24, "lineProp":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    if-eqz v24, :cond_15

    move-object/from16 v0, p5

    iget-boolean v5, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isLineNoFill:Z

    if-nez v5, :cond_15

    if-eqz v15, :cond_15

    .line 781
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getLineWidth()Ljava/lang/String;

    move-result-object v26

    .line 783
    .local v26, "lnWidth":Ljava/lang/String;
    if-eqz v26, :cond_0

    .line 784
    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints(I)I

    move-result v5

    int-to-float v0, v5

    move/from16 v25, v0

    .line 785
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 788
    :cond_0
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v19

    .line 789
    .local v19, "fillProperty":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    if-eqz v19, :cond_13

    .line 790
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v21

    .line 791
    .local v21, "foreColor":Ljava/lang/String;
    if-eqz v21, :cond_e

    const-string/jumbo v5, "bg2"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string/jumbo v5, "bg"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string/jumbo v5, "tx1"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 795
    :cond_1
    const-string/jumbo v21, "dk2"

    .line 796
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setForeColor(Ljava/lang/String;)V

    .line 803
    :cond_2
    :goto_0
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_f

    .line 804
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 812
    :goto_1
    const/4 v5, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->setSchemeClr(Z)V

    .line 813
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v17

    .line 814
    .local v17, "clr":Ljava/lang/String;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_11

    .line 815
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v27

    .line 816
    .local v27, "lumMod":Ljava/lang/String;
    invoke-static/range {v27 .. v27}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v22

    .line 817
    .local v22, "lMod":D
    invoke-static/range {v22 .. v23}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-static {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcLuminousColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v18

    .line 818
    .local v18, "colorWithLum":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v6, 0x23

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    .line 828
    .end local v18    # "colorWithLum":Ljava/lang/String;
    .end local v22    # "lMod":D
    .end local v27    # "lumMod":Ljava/lang/String;
    :goto_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v8}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v11}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v11

    invoke-virtual {v5, v6, v7, v8, v11}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 846
    .end local v17    # "clr":Ljava/lang/String;
    .end local v19    # "fillProperty":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    .end local v21    # "foreColor":Ljava/lang/String;
    .end local v26    # "lnWidth":Ljava/lang/String;
    :goto_3
    move-object/from16 v0, p5

    iget-boolean v5, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isShapeNoFill:Z

    if-nez v5, :cond_1c

    if-eqz v15, :cond_1c

    .line 847
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    if-eqz v5, :cond_19

    .line 848
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v21

    .line 849
    .restart local v21    # "foreColor":Ljava/lang/String;
    if-eqz v21, :cond_16

    const-string/jumbo v5, "bg2"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string/jumbo v5, "bg"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string/jumbo v5, "tx1"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_16

    .line 853
    :cond_3
    const-string/jumbo v21, "dk2"

    .line 854
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setForeColor(Ljava/lang/String;)V

    .line 860
    :cond_4
    :goto_4
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_6

    .line 861
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_17

    .line 863
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 868
    :goto_5
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v17

    .line 869
    .restart local v17    # "clr":Ljava/lang/String;
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_18

    .line 870
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v27

    .line 871
    .restart local v27    # "lumMod":Ljava/lang/String;
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumOff()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 872
    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v28

    .line 873
    .local v28, "mod":I
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumOff()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v30

    .line 875
    .local v30, "off":I
    add-int v29, v28, v30

    .line 876
    .local v29, "modOff":I
    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v27

    .line 878
    .end local v28    # "mod":I
    .end local v29    # "modOff":I
    .end local v30    # "off":I
    :cond_5
    invoke-static/range {v27 .. v27}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v22

    .line 879
    .restart local v22    # "lMod":D
    invoke-static/range {v22 .. v23}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-static {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcLuminousColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v18

    .line 880
    .restart local v18    # "colorWithLum":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v6, 0x23

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    .line 886
    .end local v17    # "clr":Ljava/lang/String;
    .end local v18    # "colorWithLum":Ljava/lang/String;
    .end local v22    # "lMod":D
    .end local v27    # "lumMod":Ljava/lang/String;
    :cond_6
    :goto_6
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-eqz v5, :cond_7

    .line 887
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v32

    .line 889
    .local v32, "sizeGradColor":I
    move-object/from16 v0, p0

    move/from16 v1, v32

    move-object/from16 v2, p5

    move-object/from16 v3, v16

    invoke-virtual {v0, v1, v2, v15, v3}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->getGradientColorString(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)[I

    move-result-object v9

    .line 891
    .local v9, "gradCol":[I
    move-object/from16 v0, p0

    move/from16 v1, v32

    move-object/from16 v2, p5

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->getPositionString(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)[F

    move-result-object v10

    .line 892
    .local v10, "pos":[F
    new-instance v4, Landroid/graphics/LinearGradient;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/high16 v8, 0x42c80000    # 100.0f

    sget-object v11, Landroid/graphics/Shader$TileMode;->MIRROR:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v4 .. v11}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    .line 894
    .local v4, "mShader":Landroid/graphics/Shader;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 931
    .end local v4    # "mShader":Landroid/graphics/Shader;
    .end local v9    # "gradCol":[I
    .end local v10    # "pos":[F
    .end local v21    # "foreColor":Ljava/lang/String;
    .end local v32    # "sizeGradColor":I
    :cond_7
    :goto_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v8}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v11}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v11

    invoke-virtual {v5, v6, v7, v8, v11}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 936
    :goto_8
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getPrstShapeName()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    .line 942
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "StraightConnector1"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "Line"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 944
    :cond_8
    const/high16 v5, 0x41600000    # 14.0f

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 950
    :cond_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "EllipseRibbon2"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_a

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "EllipseRibbon"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 952
    :cond_a
    const/high16 p4, 0x42d80000    # 108.0f

    .line 953
    const/high16 p3, 0x42d80000    # 108.0f

    .line 958
    :cond_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v6, "Moon"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 960
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    const/high16 v6, 0x42d80000    # 108.0f

    cmpg-float v5, v5, v6

    if-ltz v5, :cond_c

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    const/high16 v6, 0x42d80000    # 108.0f

    cmpg-float v5, v5, v6

    if-gez v5, :cond_d

    .line 961
    :cond_c
    const/high16 v5, 0x42d80000    # 108.0f

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 962
    const/high16 v5, 0x42d80000    # 108.0f

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 968
    :cond_d
    move/from16 v0, p3

    float-to-int v5, v0

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    .line 969
    move/from16 v0, p4

    float-to-int v5, v0

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    .line 971
    invoke-direct/range {p0 .. p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->initTextBox()V

    .line 974
    return-void

    .line 797
    .restart local v19    # "fillProperty":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    .restart local v21    # "foreColor":Ljava/lang/String;
    .restart local v26    # "lnWidth":Ljava/lang/String;
    :cond_e
    if-eqz v21, :cond_2

    const-string/jumbo v5, "bg1"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 799
    const-string/jumbo v21, "dk1"

    .line 800
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setForeColor(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 806
    :cond_f
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_10

    .line 807
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineRefColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 810
    :cond_10
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 819
    .restart local v17    # "clr":Ljava/lang/String;
    :cond_11
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getShade()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_12

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getShade()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "255"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_12

    .line 821
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getShade()Ljava/lang/String;

    move-result-object v31

    .line 822
    .local v31, "shade":Ljava/lang/String;
    move-object/from16 v0, v17

    move-object/from16 v1, v31

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calculateShadeColor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 823
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    goto/16 :goto_2

    .line 825
    .end local v31    # "shade":Ljava/lang/String;
    :cond_12
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v6, 0x23

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    goto/16 :goto_2

    .line 830
    .end local v17    # "clr":Ljava/lang/String;
    .end local v21    # "foreColor":Ljava/lang/String;
    :cond_13
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineRefColor()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_14

    .line 831
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineRefColor()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 833
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->setSchemeClr(Z)V

    .line 834
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v17

    .line 835
    .restart local v17    # "clr":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v6, 0x23

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    .line 836
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v8}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v11}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v11

    invoke-virtual {v5, v6, v7, v8, v11}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_3

    .line 839
    .end local v17    # "clr":Ljava/lang/String;
    :cond_14
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    const/16 v6, 0xff

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v8}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v11}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v11

    invoke-virtual {v5, v6, v7, v8, v11}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_3

    .line 843
    .end local v19    # "fillProperty":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    .end local v26    # "lnWidth":Ljava/lang/String;
    :cond_15
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    goto/16 :goto_3

    .line 855
    .restart local v21    # "foreColor":Ljava/lang/String;
    :cond_16
    if-eqz v21, :cond_4

    const-string/jumbo v5, "bg1"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 857
    const-string/jumbo v21, "dk1"

    .line 858
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setForeColor(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 866
    :cond_17
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 882
    .restart local v17    # "clr":Ljava/lang/String;
    :cond_18
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v6, 0x23

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    goto/16 :goto_6

    .line 896
    .end local v17    # "clr":Ljava/lang/String;
    .end local v21    # "foreColor":Ljava/lang/String;
    :cond_19
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillRefColor()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_7

    .line 897
    const/4 v13, 0x0

    .line 898
    .local v13, "colorLstSz":I
    const/4 v14, 0x0

    .line 899
    .local v14, "id":I
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillRefColor()Ljava/lang/String;

    move-result-object v12

    .line 900
    .local v12, "fillRefColor":Ljava/lang/String;
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillRefId()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1a

    .line 901
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillRefId()Ljava/lang/String;

    move-result-object v20

    .line 903
    .local v20, "fillRefId":Ljava/lang/String;
    const-string/jumbo v5, "1"

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1b

    const-string/jumbo v5, "0"

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1b

    .line 904
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v14, v5, -0x2

    .line 905
    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getGradColorLst()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v13

    .line 908
    if-eqz v13, :cond_1a

    move-object/from16 v11, p0

    .line 909
    invoke-virtual/range {v11 .. v16}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->getGradientColorFromTheme(Ljava/lang/String;IILcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)[I

    move-result-object v9

    .line 911
    .restart local v9    # "gradCol":[I
    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14, v15}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->getGradientPositionFromTheme(IILcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)[F

    move-result-object v10

    .line 913
    .restart local v10    # "pos":[F
    new-instance v4, Landroid/graphics/LinearGradient;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/high16 v8, 0x42c80000    # 100.0f

    sget-object v11, Landroid/graphics/Shader$TileMode;->MIRROR:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v4 .. v11}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    .line 915
    .restart local v4    # "mShader":Landroid/graphics/Shader;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 923
    .end local v4    # "mShader":Landroid/graphics/Shader;
    .end local v9    # "gradCol":[I
    .end local v10    # "pos":[F
    .end local v20    # "fillRefId":Ljava/lang/String;
    :cond_1a
    :goto_9
    invoke-virtual {v15, v12}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_7

    .line 924
    invoke-virtual {v15, v12}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 925
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v6, 0x23

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    goto/16 :goto_7

    .line 917
    .restart local v20    # "fillRefId":Ljava/lang/String;
    :cond_1b
    invoke-virtual {v15, v12}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1a

    .line 918
    invoke-virtual {v15, v12}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 919
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    goto :goto_9

    .line 934
    .end local v12    # "fillRefColor":Ljava/lang/String;
    .end local v13    # "colorLstSz":I
    .end local v14    # "id":I
    .end local v20    # "fillRefId":Ljava/lang/String;
    :cond_1c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_8
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V
    .locals 18
    .param p1, "shapeProp"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    .prologue
    .line 1665
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->isDiagramShape()Z

    move-result v12

    if-eqz v12, :cond_16

    .line 1666
    new-instance v12, Lorg/apache/poi/java/awt/Color;

    const/4 v13, -0x1

    invoke-direct {v12, v13}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    .line 1671
    :goto_0
    new-instance v12, Lorg/apache/poi/java/awt/Color;

    const/4 v13, -0x1

    invoke-direct {v12, v13}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    .line 1672
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineWidth:D

    .line 1674
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getStrokeWeight()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_0

    .line 1675
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getStrokeWeight()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->parseString(Ljava/lang/String;)F

    move-result v12

    float-to-double v12, v12

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineWidth:D

    .line 1678
    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineWidth:D

    double-to-float v13, v14

    invoke-virtual {v12, v13}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1680
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getStrokeColor()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getStrokeStatus()Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1681
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getStrokeColor()Ljava/lang/String;

    move-result-object v2

    .line 1682
    .local v2, "clrStr":Ljava/lang/String;
    const-string/jumbo v12, "#"

    const-string/jumbo v13, ""

    invoke-virtual {v2, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 1684
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getLineColorLumMod()J

    move-result-wide v6

    .line 1688
    .local v6, "mod":J
    const-wide/16 v12, 0x0

    cmp-long v12, v6, v12

    if-eqz v12, :cond_1

    .line 1689
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    .line 1690
    .local v11, "value":Ljava/lang/String;
    invoke-static {v11}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 1691
    .local v4, "lMod":D
    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v12

    invoke-static {v2, v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcLuminousColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v2

    .line 1694
    .end local v4    # "lMod":D
    .end local v11    # "value":Ljava/lang/String;
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getLineColorSatMod()J

    move-result-wide v6

    .line 1697
    const-wide/16 v12, 0x0

    cmp-long v12, v6, v12

    if-eqz v12, :cond_2

    .line 1698
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    .line 1699
    .restart local v11    # "value":Ljava/lang/String;
    invoke-static {v11}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 1700
    .restart local v4    # "lMod":D
    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v12

    invoke-static {v2, v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcSaturationColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v2

    .line 1703
    .end local v4    # "lMod":D
    .end local v11    # "value":Ljava/lang/String;
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getLineColorShade()J

    move-result-wide v12

    const-wide/16 v14, 0x0

    cmp-long v12, v12, v14

    if-eqz v12, :cond_3

    .line 1704
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getFillColorShade()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    .line 1705
    .restart local v11    # "value":Ljava/lang/String;
    invoke-static {v2, v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calculateShadeColor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1708
    .end local v11    # "value":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getLineColorTint()J

    move-result-wide v12

    const-wide/16 v14, 0x0

    cmp-long v12, v12, v14

    if-eqz v12, :cond_4

    .line 1709
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getFillColorTint()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    .line 1710
    .restart local v11    # "value":Ljava/lang/String;
    invoke-static {v2, v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcColorTint(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1713
    .end local v11    # "value":Ljava/lang/String;
    :cond_4
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v13, 0x23

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->stringToColor(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    .line 1716
    .end local v2    # "clrStr":Ljava/lang/String;
    .end local v6    # "mod":J
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getStrokeColor()Ljava/lang/String;

    move-result-object v12

    if-nez v12, :cond_6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->isDiagramShape()Z

    move-result v12

    if-nez v12, :cond_17

    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getStrokeStatus()Z

    move-result v12

    if-eqz v12, :cond_17

    .line 1718
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v13}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v14}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v15}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v16

    invoke-virtual/range {v12 .. v16}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 1724
    :goto_1
    const/16 v3, 0xff

    .line 1725
    .local v3, "fillAlpha":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getFillColor()Ljava/lang/String;

    move-result-object v2

    .line 1726
    .restart local v2    # "clrStr":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->isDiagramShape()Z

    move-result v12

    if-eqz v12, :cond_7

    if-nez v2, :cond_7

    .line 1727
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getGradFillColor()Ljava/lang/String;

    move-result-object v2

    .line 1730
    :cond_7
    if-eqz v2, :cond_d

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getFilledStatus()Z

    move-result v12

    if-eqz v12, :cond_d

    .line 1731
    const-string/jumbo v12, "#"

    const-string/jumbo v13, ""

    invoke-virtual {v2, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 1733
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getFillColorLumMod()J

    move-result-wide v6

    .line 1737
    .restart local v6    # "mod":J
    const-wide/16 v12, 0x0

    cmp-long v12, v6, v12

    if-eqz v12, :cond_8

    .line 1738
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    .line 1739
    .restart local v11    # "value":Ljava/lang/String;
    invoke-static {v11}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 1740
    .restart local v4    # "lMod":D
    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v12

    invoke-static {v2, v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcLuminousColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v2

    .line 1743
    .end local v4    # "lMod":D
    .end local v11    # "value":Ljava/lang/String;
    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getFillColorSatMod()J

    move-result-wide v6

    .line 1746
    const-wide/16 v12, 0x0

    cmp-long v12, v6, v12

    if-eqz v12, :cond_9

    .line 1747
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    .line 1748
    .restart local v11    # "value":Ljava/lang/String;
    invoke-static {v11}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 1749
    .restart local v4    # "lMod":D
    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v12

    invoke-static {v2, v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcSaturationColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v2

    .line 1752
    .end local v4    # "lMod":D
    .end local v11    # "value":Ljava/lang/String;
    :cond_9
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getFillColorShade()J

    move-result-wide v12

    const-wide/16 v14, 0x0

    cmp-long v12, v12, v14

    if-eqz v12, :cond_a

    .line 1753
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getFillColorShade()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    .line 1754
    .restart local v11    # "value":Ljava/lang/String;
    invoke-static {v2, v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calculateShadeColor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1757
    .end local v11    # "value":Ljava/lang/String;
    :cond_a
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getFillColorTint()J

    move-result-wide v12

    const-wide/16 v14, 0x0

    cmp-long v12, v12, v14

    if-eqz v12, :cond_b

    .line 1758
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getFillColorTint()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    .line 1759
    .restart local v11    # "value":Ljava/lang/String;
    invoke-static {v2, v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcColorTint(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1762
    .end local v11    # "value":Ljava/lang/String;
    :cond_b
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v13, 0x23

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->stringToColor(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    .line 1766
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getFillAlpha()J

    move-result-wide v12

    const-wide/16 v14, -0x1

    cmp-long v12, v12, v14

    if-eqz v12, :cond_18

    .line 1767
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getFillAlpha()J

    move-result-wide v12

    const-wide/16 v14, 0x100

    mul-long/2addr v12, v14

    const-wide/32 v14, 0x186a0

    div-long/2addr v12, v14

    long-to-int v3, v12

    .line 1768
    if-ltz v3, :cond_c

    const/16 v12, 0xff

    if-le v3, v12, :cond_d

    .line 1769
    :cond_c
    const/16 v3, 0x80

    .line 1776
    .end local v6    # "mod":J
    :cond_d
    :goto_2
    if-eqz v2, :cond_19

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getFilledStatus()Z

    move-result v12

    if-eqz v12, :cond_19

    .line 1777
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v13}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v14}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v15}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v15

    invoke-virtual {v12, v3, v13, v14, v15}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 1785
    :goto_3
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getShapeType()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->name()Ljava/lang/String;

    move-result-object v12

    sget-object v13, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v12, v13}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    .line 1786
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getStyle()Ljava/lang/String;

    move-result-object v8

    .line 1788
    .local v8, "style":Ljava/lang/String;
    const-string/jumbo v9, ""

    .line 1789
    .local v9, "temp":Ljava/lang/String;
    if-eqz v8, :cond_1a

    .line 1790
    const-string/jumbo v12, "width:"

    invoke-virtual {v8, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_e

    .line 1791
    const-string/jumbo v12, "width:"

    invoke-virtual {v8, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v12

    invoke-virtual {v8, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    .line 1792
    const/4 v12, 0x1

    invoke-static {v9, v12}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getValuesFromStyle(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v9

    .line 1794
    invoke-static {v9}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 1797
    :cond_e
    const-string/jumbo v12, "height:"

    invoke-virtual {v8, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_f

    .line 1798
    const-string/jumbo v12, "height:"

    invoke-virtual {v8, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v12

    invoke-virtual {v8, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    .line 1799
    const/4 v12, 0x1

    invoke-static {v9, v12}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getValuesFromStyle(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v9

    .line 1801
    invoke-static {v9}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 1809
    :cond_f
    :goto_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v13, "EllipseRibbon2"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v13, "EllipseRibbon"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_11

    .line 1811
    :cond_10
    const/high16 v12, 0x42d80000    # 108.0f

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 1812
    const/high16 v12, 0x42d80000    # 108.0f

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 1817
    :cond_11
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v13, "Moon"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_13

    .line 1819
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    const/high16 v13, 0x42d80000    # 108.0f

    cmpg-float v12, v12, v13

    if-ltz v12, :cond_12

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    const/high16 v13, 0x42d80000    # 108.0f

    cmpg-float v12, v12, v13

    if-gez v12, :cond_13

    .line 1820
    :cond_12
    const/high16 v12, 0x42d80000    # 108.0f

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    .line 1821
    const/high16 v12, 0x42d80000    # 108.0f

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 1829
    :cond_13
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    float-to-double v12, v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineWidth:D

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    double-to-int v12, v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    .line 1830
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    float-to-double v12, v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineWidth:D

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    double-to-int v12, v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    .line 1832
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getAdj1()I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval0:I

    .line 1833
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getAdj2()I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval1:I

    .line 1834
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getAdj3()I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval2:I

    .line 1835
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getAdj4()I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval3:I

    .line 1836
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getAdj5()I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval4:I

    .line 1837
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getAdj6()I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval5:I

    .line 1838
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getAdj7()I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval6:I

    .line 1839
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getAdj8()I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->adjval7:I

    .line 1841
    invoke-direct/range {p0 .. p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->initTextBox()V

    .line 1843
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    if-gtz v12, :cond_14

    .line 1844
    const/4 v12, 0x1

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    .line 1845
    :cond_14
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    if-gtz v12, :cond_15

    .line 1846
    const/4 v12, 0x1

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    .line 1848
    :cond_15
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapWidth:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmapHight:I

    sget-object v14, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v12, v13, v14}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->bitmap:Landroid/graphics/Bitmap;

    .line 1850
    return-void

    .line 1668
    .end local v2    # "clrStr":Ljava/lang/String;
    .end local v3    # "fillAlpha":I
    .end local v8    # "style":Ljava/lang/String;
    .end local v9    # "temp":Ljava/lang/String;
    :cond_16
    new-instance v12, Lorg/apache/poi/java/awt/Color;

    const/high16 v13, -0x1000000

    invoke-direct {v12, v13}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    goto/16 :goto_0

    .line 1721
    :cond_17
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintLine:Landroid/graphics/Paint;

    goto/16 :goto_1

    .line 1772
    .restart local v2    # "clrStr":Ljava/lang/String;
    .restart local v3    # "fillAlpha":I
    .restart local v6    # "mod":J
    :cond_18
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v12}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v3

    goto/16 :goto_2

    .line 1780
    .end local v6    # "mod":J
    :cond_19
    new-instance v10, Lorg/apache/poi/java/awt/Color;

    const/4 v12, 0x0

    invoke-direct {v10, v12}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 1781
    .local v10, "traColor":Lorg/apache/poi/java/awt/Color;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->paintFill:Landroid/graphics/Paint;

    const/4 v13, 0x0

    invoke-virtual {v10}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v14

    invoke-virtual {v10}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v15

    invoke-virtual {v10}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v16

    invoke-virtual/range {v12 .. v16}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_3

    .line 1804
    .end local v10    # "traColor":Lorg/apache/poi/java/awt/Color;
    .restart local v8    # "style":Ljava/lang/String;
    .restart local v9    # "temp":Ljava/lang/String;
    :cond_1a
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getShapeWidth()J

    move-result-wide v12

    long-to-float v12, v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->width:F

    .line 1805
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getShapeHeight()J

    move-result-wide v12

    long-to-float v12, v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->height:F

    goto/16 :goto_4
.end method

.method public getExcelGradPositionString(ILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;I)[F
    .locals 5
    .param p1, "size"    # I
    .param p2, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p3, "shape_Count"    # I

    .prologue
    .line 986
    new-array v1, p1, [F

    .line 987
    .local v1, "fltarray":[F
    const/4 v2, 0x0

    .local v2, "i":I
    const/4 v3, 0x0

    .line 988
    .local v3, "k":I
    if-nez p3, :cond_0

    .line 989
    const/4 v2, 0x0

    .line 998
    :goto_0
    if-ge v2, p1, :cond_1

    .line 999
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getGradPosClr()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1000
    .local v0, "fltValue":F
    aput v0, v1, v3

    .line 1001
    add-int/lit8 v3, v3, 0x1

    .line 1002
    add-int/lit8 v2, v2, 0x1

    .line 1003
    goto :goto_0

    .line 993
    .end local v0    # "fltValue":F
    :cond_0
    sget v2, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->gradtstartcount:I

    .line 994
    sget v4, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->gradtstartcount:I

    add-int/2addr p1, v4

    goto :goto_0

    .line 1004
    :cond_1
    sput p1, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->gradtstartcount:I

    .line 1005
    return-object v1
.end method

.method public getExcelGradientColorString(ILcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;I)[I
    .locals 20
    .param p1, "size"    # I
    .param p2, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p3, "color"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .param p4, "shape_Count"    # I

    .prologue
    .line 1018
    move/from16 v0, p1

    new-array v5, v0, [I

    .line 1019
    .local v5, "intarray":[I
    const/4 v4, 0x0

    .line 1020
    .local v4, "i":I
    const/4 v6, 0x0

    .line 1023
    .local v6, "k":I
    if-nez p4, :cond_6

    .line 1024
    const/4 v4, 0x0

    .line 1034
    :goto_0
    move/from16 v0, p1

    if-ge v4, v0, :cond_8

    .line 1038
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getGradFillClr()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getColorSchemakeyval(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_7

    .line 1040
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getGradFillClr()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getColorSchemakeyval(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1046
    .local v2, "clr":Ljava/lang/String;
    :goto_1
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getGradFillClr()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    if-eqz v18, :cond_5

    .line 1047
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getGradFillClr()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getLumMod()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_1

    .line 1048
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getGradFillClr()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getLumMod()Ljava/lang/String;

    move-result-object v7

    .line 1051
    .local v7, "lumMod":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getGradFillClr()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getLumOff()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_0

    .line 1052
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 1053
    .local v10, "mod":I
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getGradFillClr()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getLumOff()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 1055
    .local v12, "off":I
    add-int v11, v10, v12

    .line 1056
    .local v11, "modOff":I
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    .line 1058
    .end local v10    # "mod":I
    .end local v11    # "modOff":I
    .end local v12    # "off":I
    :cond_0
    invoke-static {v7}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    .line 1059
    .local v8, "lMod":D
    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcLuminousColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v2

    .line 1062
    .end local v7    # "lumMod":Ljava/lang/String;
    .end local v8    # "lMod":D
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getGradFillClr()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getSatMod()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_3

    .line 1065
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getGradFillClr()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getSatMod()Ljava/lang/String;

    move-result-object v13

    .line 1067
    .local v13, "satMod":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getGradFillClr()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getSatOff()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_2

    .line 1068
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 1069
    .restart local v10    # "mod":I
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getGradFillClr()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getSatOff()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 1071
    .restart local v12    # "off":I
    add-int v11, v10, v12

    .line 1072
    .restart local v11    # "modOff":I
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    .line 1074
    .end local v10    # "mod":I
    .end local v11    # "modOff":I
    .end local v12    # "off":I
    :cond_2
    invoke-static {v13}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v14

    .line 1075
    .local v14, "sMod":D
    invoke-static {v14, v15}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcSaturationColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v2

    .line 1078
    .end local v13    # "satMod":Ljava/lang/String;
    .end local v14    # "sMod":D
    :cond_3
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getGradFillClr()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_4

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getGradFillClr()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v18

    const-string/jumbo v19, "0"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_4

    .line 1081
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getGradFillClr()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v17

    .line 1082
    .local v17, "tint":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-static {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcColorTint(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1085
    .end local v17    # "tint":Ljava/lang/String;
    :cond_4
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getGradFillClr()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getShade()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_5

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getGradFillClr()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getShade()Ljava/lang/String;

    move-result-object v18

    const-string/jumbo v19, "255"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_5

    .line 1088
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getGradFillClr()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getShade()Ljava/lang/String;

    move-result-object v16

    .line 1091
    .local v16, "shade":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-static {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calculateShadeColor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1095
    .end local v16    # "shade":Ljava/lang/String;
    :cond_5
    add-int/lit8 v4, v4, 0x1

    .line 1096
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "#"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v3

    .line 1098
    .local v3, "co":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v18

    aput v18, v5, v6

    .line 1099
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 1028
    .end local v2    # "clr":Ljava/lang/String;
    .end local v3    # "co":Lorg/apache/poi/java/awt/Color;
    :cond_6
    sget v4, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->gradtstartcount:I

    .line 1029
    sget v18, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->gradtstartcount:I

    add-int p1, p1, v18

    goto/16 :goto_0

    .line 1043
    :cond_7
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getGradFillClr()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "clr":Ljava/lang/String;
    goto/16 :goto_1

    .line 1103
    .end local v2    # "clr":Ljava/lang/String;
    :cond_8
    return-object v5
.end method

.method public getGradientColorFromTheme(Ljava/lang/String;IILcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)[I
    .locals 22
    .param p1, "fillRefColor"    # Ljava/lang/String;
    .param p2, "size"    # I
    .param p3, "idx"    # I
    .param p4, "theme"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    .param p5, "color"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .prologue
    .line 2036
    move/from16 v0, p2

    new-array v4, v0, [I

    .line 2045
    .local v4, "intarray":[I
    move-object/from16 v0, p4

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_7

    .line 2046
    move-object/from16 v0, p4

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2051
    .local v2, "clr":Ljava/lang/String;
    :goto_0
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_1
    move/from16 v0, p2

    if-ge v5, v0, :cond_9

    .line 2052
    invoke-virtual/range {p4 .. p4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getGradColorLst()Ljava/util/ArrayList;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/util/ArrayList;

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .line 2056
    .local v16, "thClr":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    invoke-virtual/range {p4 .. p4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getGradColorLst()Ljava/util/ArrayList;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/util/ArrayList;

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v17

    .line 2058
    .local v17, "themeClr":Ljava/lang/String;
    const-string/jumbo v19, "phClr"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 2059
    move-object/from16 v17, v2

    .line 2066
    :cond_0
    :goto_2
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getLumMod()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_2

    .line 2067
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getLumMod()Ljava/lang/String;

    move-result-object v8

    .line 2069
    .local v8, "lumMod":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getLumOff()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_1

    .line 2070
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 2071
    .local v9, "mod":I
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getLumOff()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    .line 2072
    .local v11, "off":I
    add-int v10, v9, v11

    .line 2073
    .local v10, "modOff":I
    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    .line 2075
    .end local v9    # "mod":I
    .end local v10    # "modOff":I
    .end local v11    # "off":I
    :cond_1
    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    .line 2076
    .local v6, "lMod":D
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcLuminousColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v17

    .line 2080
    .end local v6    # "lMod":D
    .end local v8    # "lumMod":Ljava/lang/String;
    :cond_2
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getSatMod()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_4

    .line 2083
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getSatMod()Ljava/lang/String;

    move-result-object v14

    .line 2084
    .local v14, "satMod":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getSatOff()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_3

    .line 2085
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 2086
    .restart local v9    # "mod":I
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getSatOff()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    .line 2087
    .restart local v11    # "off":I
    add-int v10, v9, v11

    .line 2088
    .restart local v10    # "modOff":I
    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v14

    .line 2090
    .end local v9    # "mod":I
    .end local v10    # "modOff":I
    .end local v11    # "off":I
    :cond_3
    invoke-static {v14}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v12

    .line 2091
    .local v12, "sMod":D
    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcSaturationColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v17

    .line 2093
    sget-object v19, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v21, "Theme getSatMod : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2095
    .end local v12    # "sMod":D
    .end local v14    # "satMod":Ljava/lang/String;
    :cond_4
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_5

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v19

    const-string/jumbo v20, "0"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_5

    .line 2096
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v18

    .line 2098
    .local v18, "tint":Ljava/lang/String;
    invoke-static/range {v17 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcColorTint(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 2099
    sget-object v19, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v21, "Theme tint  themeClr : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2102
    .end local v18    # "tint":Ljava/lang/String;
    :cond_5
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getShade()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_6

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getShade()Ljava/lang/String;

    move-result-object v19

    const-string/jumbo v20, "255"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_6

    .line 2103
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getShade()Ljava/lang/String;

    move-result-object v15

    .line 2105
    .local v15, "shade":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-static {v0, v15}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calculateShadeColor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 2109
    .end local v15    # "shade":Ljava/lang/String;
    :cond_6
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "#"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v3

    .line 2111
    .local v3, "co":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v19

    aput v19, v4, v5

    .line 2051
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    .line 2048
    .end local v2    # "clr":Ljava/lang/String;
    .end local v3    # "co":Lorg/apache/poi/java/awt/Color;
    .end local v5    # "j":I
    .end local v16    # "thClr":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .end local v17    # "themeClr":Ljava/lang/String;
    :cond_7
    move-object/from16 v2, p1

    .restart local v2    # "clr":Ljava/lang/String;
    goto/16 :goto_0

    .line 2061
    .restart local v5    # "j":I
    .restart local v16    # "thClr":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .restart local v17    # "themeClr":Ljava/lang/String;
    :cond_8
    move-object/from16 v0, p4

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_0

    .line 2062
    move-object/from16 v0, p4

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_2

    .line 2113
    .end local v16    # "thClr":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .end local v17    # "themeClr":Ljava/lang/String;
    :cond_9
    return-object v4
.end method

.method public getGradientColorString(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)[I
    .locals 20
    .param p1, "size"    # I
    .param p2, "shInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "theme"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    .param p4, "color"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .prologue
    .line 1854
    move/from16 v0, p1

    new-array v6, v0, [I

    .line 1857
    .local v6, "intarray":[I
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v4

    .line 1858
    .local v4, "fillProps":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    move/from16 v0, p1

    if-ge v5, v0, :cond_7

    .line 1859
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_6

    .line 1862
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1869
    .local v2, "clr":Ljava/lang/String;
    :goto_1
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    if-eqz v18, :cond_5

    .line 1870
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getLumMod()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_1

    .line 1871
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getLumMod()Ljava/lang/String;

    move-result-object v7

    .line 1873
    .local v7, "lumMod":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getLumOff()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_0

    .line 1874
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 1875
    .local v10, "mod":I
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getLumOff()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 1877
    .local v12, "off":I
    add-int v11, v10, v12

    .line 1878
    .local v11, "modOff":I
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    .line 1880
    .end local v10    # "mod":I
    .end local v11    # "modOff":I
    .end local v12    # "off":I
    :cond_0
    invoke-static {v7}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    .line 1881
    .local v8, "lMod":D
    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcLuminousColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v2

    .line 1884
    .end local v7    # "lumMod":Ljava/lang/String;
    .end local v8    # "lMod":D
    :cond_1
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getSatMod()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_3

    .line 1887
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getSatMod()Ljava/lang/String;

    move-result-object v13

    .line 1888
    .local v13, "satMod":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getSatOff()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_2

    .line 1889
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 1890
    .restart local v10    # "mod":I
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getSatOff()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 1892
    .restart local v12    # "off":I
    add-int v11, v10, v12

    .line 1893
    .restart local v11    # "modOff":I
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    .line 1895
    .end local v10    # "mod":I
    .end local v11    # "modOff":I
    .end local v12    # "off":I
    :cond_2
    invoke-static {v13}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v14

    .line 1896
    .local v14, "sMod":D
    invoke-static {v14, v15}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcSaturationColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v2

    .line 1899
    .end local v13    # "satMod":Ljava/lang/String;
    .end local v14    # "sMod":D
    :cond_3
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v18

    const-string/jumbo v19, "0"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_4

    .line 1902
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v17

    .line 1903
    .local v17, "tint":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-static {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcColorTint(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1906
    .end local v17    # "tint":Ljava/lang/String;
    :cond_4
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getShade()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_5

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getShade()Ljava/lang/String;

    move-result-object v18

    const-string/jumbo v19, "255"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_5

    .line 1909
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getShade()Ljava/lang/String;

    move-result-object v16

    .line 1910
    .local v16, "shade":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-static {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calculateShadeColor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1914
    .end local v16    # "shade":Ljava/lang/String;
    :cond_5
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v19, 0x23

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v3

    .line 1915
    .local v3, "co":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v18

    aput v18, v6, v5

    .line 1858
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 1866
    .end local v2    # "clr":Ljava/lang/String;
    .end local v3    # "co":Lorg/apache/poi/java/awt/Color;
    :cond_6
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "clr":Ljava/lang/String;
    goto/16 :goto_1

    .line 1917
    .end local v2    # "clr":Ljava/lang/String;
    :cond_7
    return-object v6
.end method

.method public getGradientColorString(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)[I
    .locals 20
    .param p1, "size"    # I
    .param p2, "shInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p4, "color"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .prologue
    .line 1929
    move/from16 v0, p1

    new-array v6, v0, [I

    .line 1934
    .local v6, "intarray":[I
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v4

    .line 1939
    .local v4, "fillProps":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    move/from16 v0, p1

    if-ge v5, v0, :cond_7

    .line 1943
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v19

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_6

    .line 1949
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v19

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1959
    .local v2, "clr":Ljava/lang/String;
    :goto_1
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    if-eqz v18, :cond_5

    .line 1960
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getLumMod()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_1

    .line 1961
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getLumMod()Ljava/lang/String;

    move-result-object v7

    .line 1963
    .local v7, "lumMod":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getLumOff()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_0

    .line 1964
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 1965
    .local v10, "mod":I
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getLumOff()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 1967
    .local v12, "off":I
    add-int v11, v10, v12

    .line 1968
    .local v11, "modOff":I
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    .line 1970
    .end local v10    # "mod":I
    .end local v11    # "modOff":I
    .end local v12    # "off":I
    :cond_0
    invoke-static {v7}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    .line 1971
    .local v8, "lMod":D
    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcLuminousColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v2

    .line 1974
    .end local v7    # "lumMod":Ljava/lang/String;
    .end local v8    # "lMod":D
    :cond_1
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getSatMod()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_3

    .line 1977
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getSatMod()Ljava/lang/String;

    move-result-object v13

    .line 1978
    .local v13, "satMod":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getSatOff()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_2

    .line 1979
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 1980
    .restart local v10    # "mod":I
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getSatOff()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 1982
    .restart local v12    # "off":I
    add-int v11, v10, v12

    .line 1983
    .restart local v11    # "modOff":I
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    .line 1985
    .end local v10    # "mod":I
    .end local v11    # "modOff":I
    .end local v12    # "off":I
    :cond_2
    invoke-static {v13}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v14

    .line 1986
    .local v14, "sMod":D
    invoke-static {v14, v15}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcSaturationColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v2

    .line 1989
    .end local v13    # "satMod":Ljava/lang/String;
    .end local v14    # "sMod":D
    :cond_3
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v18

    const-string/jumbo v19, "0"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_4

    .line 1992
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v17

    .line 1993
    .local v17, "tint":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-static {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcColorTint(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1996
    .end local v17    # "tint":Ljava/lang/String;
    :cond_4
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getShade()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_5

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getShade()Ljava/lang/String;

    move-result-object v18

    const-string/jumbo v19, "255"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_5

    .line 1999
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getShade()Ljava/lang/String;

    move-result-object v16

    .line 2001
    .local v16, "shade":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-static {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calculateShadeColor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2005
    .end local v16    # "shade":Ljava/lang/String;
    :cond_5
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "#"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v3

    .line 2007
    .local v3, "co":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v18

    aput v18, v6, v5

    .line 1939
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 1956
    .end local v2    # "clr":Ljava/lang/String;
    .end local v3    # "co":Lorg/apache/poi/java/awt/Color;
    :cond_6
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "clr":Ljava/lang/String;
    goto/16 :goto_1

    .line 2014
    .end local v2    # "clr":Ljava/lang/String;
    :cond_7
    return-object v6
.end method

.method public getGradientPositionFromTheme(IILcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)[F
    .locals 4
    .param p1, "size"    # I
    .param p2, "idx"    # I
    .param p3, "theme"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .prologue
    .line 2118
    new-array v1, p1, [F

    .line 2119
    .local v1, "fltarray":[F
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, p1, :cond_0

    .line 2120
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getGradColorLst()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getPosition()F

    move-result v0

    .line 2125
    .local v0, "fltValue":F
    aput v0, v1, v2

    .line 2119
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2127
    .end local v0    # "fltValue":F
    :cond_0
    return-object v1
.end method

.method public getPositionString(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)[F
    .locals 4
    .param p1, "size"    # I
    .param p2, "shInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 2026
    new-array v1, p1, [F

    .line 2027
    .local v1, "fltarray":[F
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, p1, :cond_0

    .line 2028
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getPosition()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 2029
    .local v0, "fltValue":F
    aput v0, v1, v2

    .line 2027
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2031
    .end local v0    # "fltValue":F
    :cond_0
    return-object v1
.end method

.method public getTextBoxArea()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 2131
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->textArea:Landroid/graphics/RectF;

    return-object v0
.end method
