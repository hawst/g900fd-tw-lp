.class public Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DiagramDataModelHandler.java"


# direct methods
.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;)V
    .locals 4
    .param p1, "nsID"    # I
    .param p2, "dataModel"    # Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

    .prologue
    .line 15
    const-string/jumbo v3, "dataModel"

    invoke-direct {p0, p1, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 17
    const/4 v3, 0x1

    new-array v2, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 19
    .local v2, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelExtLstHandler;

    invoke-direct {v0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelExtLstHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;)V

    .line 21
    .local v0, "extLstHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelExtLstHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DgmSeqDescriptor;

    const-string/jumbo v3, "extLst"

    invoke-direct {v1, p1, v3, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DgmSeqDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 23
    .local v1, "extLstSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DgmSeqDescriptor;
    const/4 v3, 0x0

    aput-object v1, v2, v3

    .line 25
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 26
    return-void
.end method
