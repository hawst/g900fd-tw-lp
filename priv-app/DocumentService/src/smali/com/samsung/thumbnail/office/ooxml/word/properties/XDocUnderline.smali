.class public Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;
.super Ljava/lang/Object;
.source "XDocUnderline.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$1;,
        Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;
    }
.end annotation


# instance fields
.field private color:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

.field private underlineStyle:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    return-void
.end method


# virtual methods
.method public createUnderlineStyle()Ljava/lang/String;
    .locals 4

    .prologue
    .line 44
    const-string/jumbo v0, ""

    .line 45
    .local v0, "attr":Ljava/lang/String;
    const-string/jumbo v1, ""

    .line 47
    .local v1, "val":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 48
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$properties$XDocUnderline$EUnderline:[I

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 74
    const-string/jumbo v0, "text-decoration:"

    .line 75
    const-string/jumbo v1, "underline"

    .line 84
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 85
    const-string/jumbo v2, "underline"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 86
    const-string/jumbo v0, "border-bottom:"

    .line 87
    const-string/jumbo v1, "1px solid"

    .line 89
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " #"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 93
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 50
    :pswitch_0
    const-string/jumbo v0, "border-bottom:"

    .line 51
    const-string/jumbo v1, "3px double"

    .line 52
    goto :goto_0

    .line 57
    :pswitch_1
    const-string/jumbo v0, "border-bottom:"

    .line 58
    const-string/jumbo v1, "1px dotted"

    .line 59
    goto :goto_0

    .line 66
    :pswitch_2
    const-string/jumbo v0, "border-bottom:"

    .line 67
    const-string/jumbo v1, "1px dashed"

    .line 68
    goto :goto_0

    .line 70
    :pswitch_3
    const-string/jumbo v0, "text-decoration:"

    .line 71
    const-string/jumbo v1, "none"

    .line 72
    goto :goto_0

    .line 80
    :cond_2
    const-string/jumbo v0, "text-decoration:"

    .line 81
    const-string/jumbo v1, "underline"

    goto :goto_0

    .line 48
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->color:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    return-object v0
.end method

.method public getStyle()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->underlineStyle:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    return-object v0
.end method

.method public merge(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;)V
    .locals 1
    .param p1, "underline"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->color:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    if-nez v0, :cond_0

    .line 36
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->color:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->color:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->underlineStyle:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    if-nez v0, :cond_1

    .line 39
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->underlineStyle:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->underlineStyle:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    .line 41
    :cond_1
    return-void
.end method

.method public setColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V
    .locals 0
    .param p1, "color"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->color:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .line 23
    return-void
.end method

.method public setStyle(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;)V
    .locals 0
    .param p1, "underlineStyle"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->underlineStyle:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    .line 19
    return-void
.end method
