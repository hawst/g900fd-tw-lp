.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringParser;
.super Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;
.source "XExcelSharedStringParser.java"


# instance fields
.field mSharedstringobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;)V
    .locals 1
    .param p1, "sharedstringobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;)V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringParser;->mSharedstringobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    .line 18
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringParser;->mSharedstringobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    .line 19
    return-void
.end method


# virtual methods
.method public createRootElementHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringHandler;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringParser;->mSharedstringobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;)V

    return-object v0
.end method

.method public getMainNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0xc8

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v0

    return-object v0
.end method
