.class public final enum Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;
.super Ljava/lang/Enum;
.source "XSLFShape.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EPlaceHolderType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

.field public static final enum BODY:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

.field public static final enum CHART:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

.field public static final enum CLIP_ART:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

.field public static final enum CTR_TITLE:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

.field public static final enum DGM:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

.field public static final enum DT:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

.field public static final enum FTR:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

.field public static final enum HDR:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

.field public static final enum MEDIA:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

.field public static final enum NONE:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

.field public static final enum OBJ:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

.field public static final enum PIC:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

.field public static final enum SLD_IMG:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

.field public static final enum SLD_NUM:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

.field public static final enum SUB_TITLE:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

.field public static final enum TBL:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

.field public static final enum TITLE:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 42
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    const-string/jumbo v1, "TITLE"

    const-string/jumbo v2, "title"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->TITLE:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    .line 44
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    const-string/jumbo v1, "BODY"

    const-string/jumbo v2, "body"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->BODY:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    .line 46
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    const-string/jumbo v1, "CTR_TITLE"

    const-string/jumbo v2, "ctrTitle"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->CTR_TITLE:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    .line 48
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    const-string/jumbo v1, "SUB_TITLE"

    const-string/jumbo v2, "subTitle"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->SUB_TITLE:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    .line 50
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    const-string/jumbo v1, "SLD_NUM"

    const-string/jumbo v2, "sldNum"

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->SLD_NUM:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    .line 52
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    const-string/jumbo v1, "FTR"

    const/4 v2, 0x5

    const-string/jumbo v3, "ftr"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->FTR:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    .line 54
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    const-string/jumbo v1, "HDR"

    const/4 v2, 0x6

    const-string/jumbo v3, "hdr"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->HDR:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    .line 56
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    const-string/jumbo v1, "OBJ"

    const/4 v2, 0x7

    const-string/jumbo v3, "obj"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->OBJ:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    .line 58
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    const-string/jumbo v1, "CHART"

    const/16 v2, 0x8

    const-string/jumbo v3, "chart"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->CHART:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    .line 60
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    const-string/jumbo v1, "TBL"

    const/16 v2, 0x9

    const-string/jumbo v3, "tbl"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->TBL:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    .line 62
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    const-string/jumbo v1, "CLIP_ART"

    const/16 v2, 0xa

    const-string/jumbo v3, "clipArt"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->CLIP_ART:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    .line 64
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    const-string/jumbo v1, "DGM"

    const/16 v2, 0xb

    const-string/jumbo v3, "dgm"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->DGM:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    .line 66
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    const-string/jumbo v1, "MEDIA"

    const/16 v2, 0xc

    const-string/jumbo v3, "media"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->MEDIA:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    .line 68
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    const-string/jumbo v1, "SLD_IMG"

    const/16 v2, 0xd

    const-string/jumbo v3, "sldImg"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->SLD_IMG:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    .line 70
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    const-string/jumbo v1, "PIC"

    const/16 v2, 0xe

    const-string/jumbo v3, "pic"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->PIC:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    .line 72
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    const-string/jumbo v1, "DT"

    const/16 v2, 0xf

    const-string/jumbo v3, "dt"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->DT:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    .line 74
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    const-string/jumbo v1, "NONE"

    const/16 v2, 0x10

    const-string/jumbo v3, "none"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->NONE:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    .line 41
    const/16 v0, 0x11

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->TITLE:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->BODY:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->CTR_TITLE:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->SUB_TITLE:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->SLD_NUM:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->FTR:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->HDR:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->OBJ:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->CHART:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->TBL:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->CLIP_ART:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->DGM:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->MEDIA:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->SLD_IMG:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->PIC:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->DT:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->NONE:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 79
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->value:Ljava/lang/String;

    .line 80
    return-void
.end method

.method public static convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 87
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->values()[Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 88
    .local v2, "inst":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 92
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;
    :goto_1
    return-object v2

    .line 87
    .restart local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 92
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 41
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    return-object v0
.end method


# virtual methods
.method public xmlValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->value:Ljava/lang/String;

    return-object v0
.end method
