.class public Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
.super Ljava/lang/Object;
.source "ExcelShapeInfo.java"


# instance fields
.field private escherClientAnchorRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

.field private escherOptRecord:Lorg/apache/poi/ddf/EscherOptRecord;

.field private escherSpRecord:Lorg/apache/poi/ddf/EscherSpRecord;

.field private shapeColor:Lorg/apache/poi/java/awt/Color;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherOptRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    .line 22
    iput-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherClientAnchorRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    .line 23
    iput-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherSpRecord:Lorg/apache/poi/ddf/EscherSpRecord;

    .line 28
    return-void
.end method


# virtual methods
.method public getAdjustmentValue(I)I
    .locals 2
    .param p1, "idx"    # I

    .prologue
    .line 139
    if-ltz p1, :cond_0

    const/16 v0, 0x9

    if-le p1, v0, :cond_1

    .line 140
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "The index of an adjustment value must be in the [0, 9] range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 144
    :cond_1
    iget-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherOptRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    add-int/lit16 v1, p1, 0x147

    int-to-short v1, v1

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/util/Utils;->getIntEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;S)I

    move-result v0

    return v0
.end method

.method public getCol1()S
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherClientAnchorRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getCol1()S

    move-result v0

    return v0
.end method

.method public getCol2()S
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherClientAnchorRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getCol2()S

    move-result v0

    return v0
.end method

.method public getEscherOptRecord()Lorg/apache/poi/ddf/EscherOptRecord;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherOptRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    return-object v0
.end method

.method public getHeight()F
    .locals 8

    .prologue
    .line 98
    iget-object v6, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherClientAnchorRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v6}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getDy1()S

    move-result v6

    mul-int/lit8 v6, v6, 0x64

    shr-int/lit8 v2, v6, 0x8

    .line 100
    .local v2, "percentageOfDy1":I
    iget-object v6, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherClientAnchorRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v6}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getDy2()S

    move-result v6

    mul-int/lit8 v6, v6, 0x64

    shr-int/lit8 v3, v6, 0x8

    .line 102
    .local v3, "percentageOfDy2":I
    mul-int/lit8 v6, v2, 0xf

    div-int/lit8 v6, v6, 0x64

    rsub-int/lit8 v4, v6, 0xf

    .line 103
    .local v4, "y1":I
    mul-int/lit8 v6, v3, 0xf

    div-int/lit8 v5, v6, 0x64

    .line 104
    .local v5, "y2":I
    iget-object v6, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherClientAnchorRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v6}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getRow2()S

    move-result v6

    iget-object v7, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherClientAnchorRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v7}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getRow1()S

    move-result v7

    sub-int/2addr v6, v7

    add-int/lit8 v0, v6, 0x1

    .line 106
    .local v0, "coldiff":I
    add-int v6, v4, v5

    add-int/lit8 v7, v0, -0x2

    mul-int/lit8 v7, v7, 0xf

    add-int v1, v6, v7

    .line 107
    .local v1, "height":I
    int-to-float v6, v1

    return v6
.end method

.method public getLeft()F
    .locals 4

    .prologue
    .line 121
    iget-object v3, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherClientAnchorRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getDx1()S

    move-result v3

    mul-int/lit8 v3, v3, 0x64

    shr-int/lit8 v1, v3, 0xa

    .line 123
    .local v1, "percentageOfDx1":I
    mul-int/lit8 v3, v1, 0x46

    div-int/lit8 v2, v3, 0x64

    .line 124
    .local v2, "x1":I
    iget-object v3, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherClientAnchorRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getRow1()S

    move-result v3

    mul-int/lit8 v3, v3, 0x46

    add-int v0, v3, v2

    .line 125
    .local v0, "left":I
    int-to-float v3, v0

    return v3
.end method

.method public getLineColor()Lorg/apache/poi/java/awt/Color;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 157
    iget-object v6, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherOptRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    const/16 v7, 0x1c0

    invoke-static {v6, v7}, Lcom/samsung/thumbnail/util/Utils;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 160
    .local v1, "p1":Lorg/apache/poi/ddf/EscherSimpleProperty;
    iget-object v6, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherOptRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    const/16 v7, 0x1ff

    invoke-static {v6, v7}, Lcom/samsung/thumbnail/util/Utils;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 163
    .local v2, "p2":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-nez v2, :cond_2

    move v3, v4

    .line 164
    .local v3, "p2val":I
    :goto_0
    const/4 v0, 0x0

    .line 165
    .local v0, "clr":Lorg/apache/poi/java/awt/Color;
    and-int/lit8 v6, v3, 0x8

    if-nez v6, :cond_0

    and-int/lit8 v6, v3, 0x10

    if-eqz v6, :cond_1

    .line 166
    :cond_0
    if-nez v1, :cond_3

    .line 167
    .local v4, "rgb":I
    :goto_1
    new-instance v5, Lorg/apache/poi/java/awt/Color;

    const/4 v6, 0x1

    invoke-direct {v5, v4, v6}, Lorg/apache/poi/java/awt/Color;-><init>(IZ)V

    .line 168
    .local v5, "tmp":Lorg/apache/poi/java/awt/Color;
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    .end local v0    # "clr":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v6

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v7

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v8

    invoke-direct {v0, v6, v7, v8}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    .line 170
    .end local v4    # "rgb":I
    .end local v5    # "tmp":Lorg/apache/poi/java/awt/Color;
    .restart local v0    # "clr":Lorg/apache/poi/java/awt/Color;
    :cond_1
    return-object v0

    .line 163
    .end local v0    # "clr":Lorg/apache/poi/java/awt/Color;
    .end local v3    # "p2val":I
    :cond_2
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v3

    goto :goto_0

    .line 166
    .restart local v0    # "clr":Lorg/apache/poi/java/awt/Color;
    .restart local v3    # "p2val":I
    :cond_3
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v4

    goto :goto_1
.end method

.method public getLineSimpleProperty(S)Lorg/apache/poi/ddf/EscherSimpleProperty;
    .locals 2
    .param p1, "inId"    # S

    .prologue
    .line 184
    iget-object v1, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherOptRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    invoke-static {v1, p1}, Lcom/samsung/thumbnail/util/Utils;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 187
    .local v0, "p1":Lorg/apache/poi/ddf/EscherSimpleProperty;
    return-object v0
.end method

.method public getOffsetCol1()S
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherClientAnchorRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getDx1()S

    move-result v0

    return v0
.end method

.method public getOffsetCol2()S
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherClientAnchorRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getDx2()S

    move-result v0

    return v0
.end method

.method public getOffsetRow1()S
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherClientAnchorRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getDy1()S

    move-result v0

    return v0
.end method

.method public getOffsetRow2()S
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherClientAnchorRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getDy2()S

    move-result v0

    return v0
.end method

.method public getRotationAngle()I
    .locals 3

    .prologue
    .line 174
    iget-object v1, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherOptRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/samsung/thumbnail/util/Utils;->getIntEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;S)I

    move-result v0

    .line 178
    .local v0, "rawRotation":I
    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->rotationAngleToDegree_97Format(I)I

    move-result v1

    return v1
.end method

.method public getRow1()S
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherClientAnchorRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getRow1()S

    move-result v0

    return v0
.end method

.method public getRow2()S
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherClientAnchorRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getRow2()S

    move-result v0

    return v0
.end method

.method public getShapeColor()Lorg/apache/poi/java/awt/Color;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->shapeColor:Lorg/apache/poi/java/awt/Color;

    return-object v0
.end method

.method public getShapeId()I
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherSpRecord:Lorg/apache/poi/ddf/EscherSpRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSpRecord;->getShapeId()I

    move-result v0

    return v0
.end method

.method public getShapeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherSpRecord:Lorg/apache/poi/ddf/EscherSpRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSpRecord;->getOptions()S

    move-result v0

    shr-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Lorg/apache/poi/hslf/model/ShapeTypes;->typeName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTop()F
    .locals 4

    .prologue
    .line 112
    iget-object v3, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherClientAnchorRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getDy1()S

    move-result v3

    mul-int/lit8 v3, v3, 0x64

    shr-int/lit8 v0, v3, 0x8

    .line 114
    .local v0, "percentageOfDy1":I
    mul-int/lit8 v3, v0, 0xf

    div-int/lit8 v2, v3, 0x64

    .line 115
    .local v2, "y1":I
    iget-object v3, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherClientAnchorRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getRow1()S

    move-result v3

    mul-int/lit8 v3, v3, 0xf

    add-int v1, v3, v2

    .line 116
    .local v1, "top":I
    int-to-float v3, v1

    return v3
.end method

.method public getWidth()F
    .locals 8

    .prologue
    .line 84
    iget-object v6, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherClientAnchorRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v6}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getDx1()S

    move-result v6

    mul-int/lit8 v6, v6, 0x64

    shr-int/lit8 v0, v6, 0xa

    .line 86
    .local v0, "percentageOfDx1":I
    iget-object v6, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherClientAnchorRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v6}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getDx2()S

    move-result v6

    mul-int/lit8 v6, v6, 0x64

    shr-int/lit8 v1, v6, 0xa

    .line 88
    .local v1, "percentageOfDx2":I
    mul-int/lit8 v6, v0, 0x46

    div-int/lit8 v6, v6, 0x64

    rsub-int/lit8 v4, v6, 0x46

    .line 89
    .local v4, "x1":I
    mul-int/lit8 v6, v1, 0x46

    div-int/lit8 v5, v6, 0x64

    .line 90
    .local v5, "x2":I
    iget-object v6, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherClientAnchorRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v6}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getCol2()S

    move-result v6

    iget-object v7, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherClientAnchorRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v7}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getCol1()S

    move-result v7

    sub-int/2addr v6, v7

    add-int/lit8 v2, v6, 0x1

    .line 92
    .local v2, "rowdiff":I
    add-int v6, v4, v5

    add-int/lit8 v7, v2, -0x2

    mul-int/lit8 v7, v7, 0x46

    add-int v3, v6, v7

    .line 93
    .local v3, "width":I
    int-to-float v6, v3

    return v6
.end method

.method public setEscherClientAnchorRecord(Lorg/apache/poi/ddf/EscherClientAnchorRecord;)V
    .locals 0
    .param p1, "escherClientAnchorRecord"    # Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherClientAnchorRecord:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    .line 37
    return-void
.end method

.method public setEscherOptRecord(Lorg/apache/poi/ddf/EscherOptRecord;)V
    .locals 0
    .param p1, "escherOptRecord"    # Lorg/apache/poi/ddf/EscherOptRecord;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherOptRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    .line 41
    return-void
.end method

.method public setShapeColor(Lorg/apache/poi/java/awt/Color;)V
    .locals 0
    .param p1, "shapeColor"    # Lorg/apache/poi/java/awt/Color;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->shapeColor:Lorg/apache/poi/java/awt/Color;

    .line 150
    return-void
.end method

.method public setSpRecord(Lorg/apache/poi/ddf/EscherSpRecord;)V
    .locals 0
    .param p1, "spRecord"    # Lorg/apache/poi/ddf/EscherSpRecord;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->escherSpRecord:Lorg/apache/poi/ddf/EscherSpRecord;

    .line 32
    return-void
.end method
