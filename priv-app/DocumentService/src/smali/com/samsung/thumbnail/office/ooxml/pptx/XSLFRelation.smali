.class public Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;
.super Lorg/apache/poi/POIXMLRelation;
.source "XSLFRelation.java"


# static fields
.field public static final CHART:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final COMMENTS:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final DATA_MODEL:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final HYPERLINK:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final IMAGES:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final IMAGE_DIB:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final IMAGE_EMF:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final IMAGE_GIF:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final IMAGE_JPEG:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final IMAGE_PICT:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final IMAGE_PNG:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final IMAGE_WMF:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final MACRO:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final MACRO_TEMPLATE:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final MAIN:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final NOTES:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final PRESENTATIONML:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final PRESENTATIONML_TEMPLATE:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final PRESENTATION_MACRO:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final SLIDE:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final SLIDE_LAYOUT:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final SLIDE_MASTER:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final SMART_ART:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final TABLESTYLES:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final THEME:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final THEME_MANAGER:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field public static final VML_DRAWING:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

.field protected static _table:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;",
            ">;"
        }
    .end annotation
.end field

.field private static log:Lorg/apache/poi/util/POILogger;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 35
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->log:Lorg/apache/poi/util/POILogger;

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->_table:Ljava/util/Map;

    .line 42
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.presentationml.presentation.main+xml"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->MAIN:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 46
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "application/vnd.ms-powerpoint.slideshow.macroEnabled.main+xml"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->MACRO:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 50
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "application/vnd.ms-powerpoint.template.macroEnabled.main+xml"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->MACRO_TEMPLATE:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 54
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.presentationml.slideshow.main+xml"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->PRESENTATIONML:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 58
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.presentationml.template.main+xml"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->PRESENTATIONML_TEMPLATE:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 62
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "application/vnd.ms-powerpoint.presentation.macroEnabled.main+xml"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->PRESENTATION_MACRO:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 66
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.themeManager+xml"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->THEME_MANAGER:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 70
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.presentationml.notesSlide+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/notesSlide"

    invoke-direct {v0, v1, v2, v5, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->NOTES:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 75
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.presentationml.slide+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slide"

    const-string/jumbo v3, "/ppt/slides/slide#.xml"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->SLIDE:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 80
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.presentationml.slideLayout+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideLayout"

    const-string/jumbo v3, "/ppt/slideLayouts/slideLayout#.xml"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->SLIDE_LAYOUT:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 85
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.presentationml.slideMaster+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideMaster"

    const-string/jumbo v3, "/ppt/slideMasters/slideMaster#.xml"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->SLIDE_MASTER:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 90
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.presentationml.comments+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/comments"

    invoke-direct {v0, v1, v2, v5, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->COMMENTS:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 95
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink"

    invoke-direct {v0, v5, v1, v5, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->HYPERLINK:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 100
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.theme+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme"

    const-string/jumbo v3, "/ppt/theme/theme#.xml"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->THEME:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 105
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.drawingml.drawing+xml"

    const-string/jumbo v2, "http://schemas.microsoft.com/office/2007/relationships/diagramDrawing"

    const-string/jumbo v3, "/ppt/diagrams/drawing#.xml"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->SMART_ART:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 110
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.drawingml.dataModel+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/diagramData"

    const-string/jumbo v3, "/ppt/diagrams/data#.xml"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->DATA_MODEL:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 115
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.drawingml.chart+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/chart"

    const-string/jumbo v3, "/ppt/charts/chart#.xml"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->CHART:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 119
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.vmlDrawing"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/vmlDrawing"

    const-string/jumbo v3, "/ppt/drawings/vmlDrawing#.vml"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->VML_DRAWING:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 124
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.drawingml.tableStyles+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/tableStyles"

    const-string/jumbo v3, "/ppt/tableStyles.xml"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->TABLESTYLES:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 129
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "image/x-emf"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    const-string/jumbo v3, "/ppt/media/image#.emf"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->IMAGE_EMF:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 133
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "image/x-wmf"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    const-string/jumbo v3, "/ppt/media/image#.wmf"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->IMAGE_WMF:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 137
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "image/pict"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    const-string/jumbo v3, "/ppt/media/image#.pict"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->IMAGE_PICT:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 141
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "image/jpeg"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    const-string/jumbo v3, "/ppt/media/image#.jpeg"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->IMAGE_JPEG:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 145
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "image/png"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    const-string/jumbo v3, "/ppt/media/image#.png"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->IMAGE_PNG:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 149
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "image/dib"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    const-string/jumbo v3, "/ppt/media/image#.dib"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->IMAGE_DIB:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 153
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "image/gif"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    const-string/jumbo v3, "/ppt/media/image#.gif"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->IMAGE_GIF:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    .line 158
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    const-string/jumbo v1, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    invoke-direct {v0, v5, v1, v5, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->IMAGES:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V
    .locals 1
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "rel"    # Ljava/lang/String;
    .param p3, "defaultName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/poi/POIXMLDocumentPart;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 165
    .local p4, "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/poi/POIXMLDocumentPart;>;"
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/poi/POIXMLRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    .line 167
    if-eqz p4, :cond_0

    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->_table:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 168
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->_table:Ljava/util/Map;

    invoke-interface {v0, p2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    :cond_0
    return-void
.end method

.method public static getInstance(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;
    .locals 1
    .param p0, "rel"    # Ljava/lang/String;

    .prologue
    .line 180
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->_table:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    return-object v0
.end method
