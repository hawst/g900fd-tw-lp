.class public Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;
.super Ljava/lang/Object;
.source "EMR_RoundRect.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;


# instance fields
.field private RecordSize:I

.field private RecordType:I

.field protected bounds:Landroid/graphics/Rect;

.field private cornerHeight:I

.field private cornerWidth:I

.field fileContent:[B

.field private leftTopX:I

.field private leftTopY:I

.field localPath:Landroid/graphics/Path;

.field private offset:I

.field private rightBottomX:I

.field private rightBottomY:I


# direct methods
.method public constructor <init>([BI)V
    .locals 1
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I

    .prologue
    const/4 v0, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->bounds:Landroid/graphics/Rect;

    .line 56
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->localPath:Landroid/graphics/Path;

    .line 67
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->fileContent:[B

    .line 68
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    .line 69
    return-void
.end method


# virtual methods
.method public getCornerHeight()I
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->cornerHeight:I

    return v0
.end method

.method public getCornerWidth()I
    .locals 1

    .prologue
    .line 191
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->cornerWidth:I

    return v0
.end method

.method public getPath()Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->localPath:Landroid/graphics/Path;

    return-object v0
.end method

.method public getRecordSize()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->RecordSize:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->RecordType:I

    return v0
.end method

.method public getleftTopX()I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->leftTopX:I

    return v0
.end method

.method public getleftTopY()I
    .locals 1

    .prologue
    .line 134
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->leftTopY:I

    return v0
.end method

.method public getrightBottomX()I
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->rightBottomX:I

    return v0
.end method

.method public getrightBottomY()I
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->rightBottomY:I

    return v0
.end method

.method public prepare()V
    .locals 9

    .prologue
    .line 313
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->getleftTopX()I

    move-result v5

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->getleftTopY()I

    move-result v6

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->getrightBottomX()I

    move-result v7

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->getrightBottomY()I

    move-result v8

    invoke-virtual {p0, v5, v6, v7, v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->setBounds(IIII)V

    .line 315
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->getCornerWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->getCornerHeight()I

    move-result v6

    invoke-virtual {p0, v5, v6}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->setCornerWidthHeight(II)V

    .line 316
    new-instance v5, Landroid/graphics/Path;

    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    iput-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->localPath:Landroid/graphics/Path;

    .line 317
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->bounds:Landroid/graphics/Rect;

    .line 318
    .local v3, "localRect":Landroid/graphics/Rect;
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 319
    .local v4, "localRectF":Landroid/graphics/RectF;
    iget v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->cornerWidth:I

    int-to-float v0, v5

    .line 320
    .local v0, "f1":F
    iget v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->cornerHeight:I

    int-to-float v1, v5

    .line 321
    .local v1, "f2":F
    sget-object v2, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 322
    .local v2, "localDirection":Landroid/graphics/Path$Direction;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->localPath:Landroid/graphics/Path;

    invoke-virtual {v5, v4, v0, v1, v2}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    .line 323
    return-void
.end method

.method public readEmfRoundRect()V
    .locals 15

    .prologue
    const/4 v14, 0x3

    const/4 v13, 0x0

    .line 227
    const/4 v4, 0x0

    .line 229
    .local v4, "i":I
    const/4 v10, 0x4

    new-array v5, v10, [B

    .line 231
    .local v5, "intConvert":[B
    const/4 v4, 0x0

    :goto_0
    if-gt v4, v14, :cond_0

    .line 232
    rsub-int/lit8 v10, v4, 0x3

    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->fileContent:[B

    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    add-int/2addr v12, v4

    aget-byte v11, v11, v12

    aput-byte v11, v5, v10

    .line 231
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 233
    :cond_0
    invoke-static {v5, v13}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 234
    .local v1, "Type":I
    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    add-int/2addr v10, v4

    iput v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    .line 235
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->setRecordType(I)V

    .line 237
    const/4 v4, 0x0

    :goto_1
    if-gt v4, v14, :cond_1

    .line 238
    rsub-int/lit8 v10, v4, 0x3

    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->fileContent:[B

    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    add-int/2addr v12, v4

    aget-byte v11, v11, v12

    aput-byte v11, v5, v10

    .line 237
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 239
    :cond_1
    invoke-static {v5, v13}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 240
    .local v0, "Size":I
    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    add-int/2addr v10, v4

    iput v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    .line 241
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->setRecordSize(I)V

    .line 243
    const/4 v4, 0x0

    :goto_2
    if-gt v4, v14, :cond_2

    .line 244
    rsub-int/lit8 v10, v4, 0x3

    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->fileContent:[B

    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    add-int/2addr v12, v4

    aget-byte v11, v11, v12

    aput-byte v11, v5, v10

    .line 243
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 245
    :cond_2
    invoke-static {v5, v13}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v6

    .line 246
    .local v6, "leftTopX":I
    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    add-int/2addr v10, v4

    iput v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    .line 247
    int-to-float v10, v6

    invoke-static {v10}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v10

    float-to-int v10, v10

    invoke-virtual {p0, v10}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->setleftTopX(I)V

    .line 249
    const/4 v4, 0x0

    :goto_3
    if-gt v4, v14, :cond_3

    .line 250
    rsub-int/lit8 v10, v4, 0x3

    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->fileContent:[B

    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    add-int/2addr v12, v4

    aget-byte v11, v11, v12

    aput-byte v11, v5, v10

    .line 249
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 251
    :cond_3
    invoke-static {v5, v13}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v7

    .line 252
    .local v7, "leftTopY":I
    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    add-int/2addr v10, v4

    iput v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    .line 253
    int-to-float v10, v7

    invoke-static {v10}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v10

    float-to-int v10, v10

    invoke-virtual {p0, v10}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->setleftTopY(I)V

    .line 255
    const/4 v4, 0x0

    :goto_4
    if-gt v4, v14, :cond_4

    .line 256
    rsub-int/lit8 v10, v4, 0x3

    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->fileContent:[B

    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    add-int/2addr v12, v4

    aget-byte v11, v11, v12

    aput-byte v11, v5, v10

    .line 255
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 257
    :cond_4
    invoke-static {v5, v13}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v8

    .line 258
    .local v8, "rightBottomX":I
    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    add-int/2addr v10, v4

    iput v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    .line 259
    int-to-float v10, v8

    invoke-static {v10}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v10

    float-to-int v10, v10

    invoke-virtual {p0, v10}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->setrightBottomX(I)V

    .line 261
    const/4 v4, 0x0

    :goto_5
    if-gt v4, v14, :cond_5

    .line 262
    rsub-int/lit8 v10, v4, 0x3

    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->fileContent:[B

    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    add-int/2addr v12, v4

    aget-byte v11, v11, v12

    aput-byte v11, v5, v10

    .line 261
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 263
    :cond_5
    invoke-static {v5, v13}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v9

    .line 264
    .local v9, "rightBottomY":I
    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    add-int/2addr v10, v4

    iput v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    .line 265
    int-to-float v10, v9

    invoke-static {v10}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v10

    float-to-int v10, v10

    invoke-virtual {p0, v10}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->setrightBottomY(I)V

    .line 267
    const/4 v4, 0x0

    :goto_6
    if-gt v4, v14, :cond_6

    .line 268
    rsub-int/lit8 v10, v4, 0x3

    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->fileContent:[B

    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    add-int/2addr v12, v4

    aget-byte v11, v11, v12

    aput-byte v11, v5, v10

    .line 267
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 269
    :cond_6
    invoke-static {v5, v13}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v3

    .line 270
    .local v3, "cornerwidth":I
    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    add-int/2addr v10, v4

    iput v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    .line 271
    int-to-float v10, v3

    invoke-static {v10}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v10

    float-to-int v10, v10

    invoke-virtual {p0, v10}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->setCornerWidth(I)V

    .line 273
    const/4 v4, 0x0

    :goto_7
    if-gt v4, v14, :cond_7

    .line 274
    rsub-int/lit8 v10, v4, 0x3

    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->fileContent:[B

    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    add-int/2addr v12, v4

    aget-byte v11, v11, v12

    aput-byte v11, v5, v10

    .line 273
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 275
    :cond_7
    invoke-static {v5, v13}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v2

    .line 276
    .local v2, "cornerheight":I
    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    add-int/2addr v10, v4

    iput v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->offset:I

    .line 277
    int-to-float v10, v2

    invoke-static {v10}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v10

    float-to-int v10, v10

    invoke-virtual {p0, v10}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->setCornerHeight(I)V

    .line 279
    return-void
.end method

.method public render(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 333
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;-><init>()V

    .line 334
    .local v0, "PaintRounRectPropertiesObject":Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 335
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->getPath()Landroid/graphics/Path;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 337
    return-void
.end method

.method public setBounds(IIII)V
    .locals 1
    .param p1, "lx"    # I
    .param p2, "ly"    # I
    .param p3, "rx"    # I
    .param p4, "ry"    # I

    .prologue
    .line 294
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->bounds:Landroid/graphics/Rect;

    .line 295
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->bounds:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 296
    return-void
.end method

.method public setCornerHeight(I)V
    .locals 0
    .param p1, "cornerHeght"    # I

    .prologue
    .line 220
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->cornerHeight:I

    .line 221
    return-void
.end method

.method public setCornerWidth(I)V
    .locals 0
    .param p1, "cornerWdth"    # I

    .prologue
    .line 201
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->cornerWidth:I

    .line 202
    return-void
.end method

.method public setCornerWidthHeight(II)V
    .locals 0
    .param p1, "wd"    # I
    .param p2, "ht"    # I

    .prologue
    .line 348
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->cornerWidth:I

    .line 349
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->cornerHeight:I

    .line 350
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "RecrdSize"    # I

    .prologue
    .line 106
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->RecordSize:I

    .line 107
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "RecrdType"    # I

    .prologue
    .line 87
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->RecordType:I

    .line 88
    return-void
.end method

.method public setleftTopX(I)V
    .locals 0
    .param p1, "lftTopX"    # I

    .prologue
    .line 125
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->leftTopX:I

    .line 126
    return-void
.end method

.method public setleftTopY(I)V
    .locals 0
    .param p1, "lftTopY"    # I

    .prologue
    .line 144
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->leftTopY:I

    .line 145
    return-void
.end method

.method public setrightBottomX(I)V
    .locals 0
    .param p1, "rghtBottomX"    # I

    .prologue
    .line 163
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->rightBottomX:I

    .line 164
    return-void
.end method

.method public setrightBottomY(I)V
    .locals 0
    .param p1, "rghtBottomY"    # I

    .prologue
    .line 182
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_RoundRect;->rightBottomY:I

    .line 183
    return-void
.end method
