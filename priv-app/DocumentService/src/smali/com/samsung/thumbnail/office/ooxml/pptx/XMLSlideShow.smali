.class public Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;
.super Lorg/apache/poi/POIXMLDocument;
.source "XMLSlideShow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "XMLSlideShow"


# instance fields
.field public URI_PREFIX:Ljava/lang/String;

.field protected buMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field chartLinkSrc:Z

.field private context:Landroid/content/Context;

.field private file:Ljava/io/File;

.field private folderName:Ljava/lang/String;

.field private folderPath:Ljava/io/File;

.field incrId:I

.field private isHlink:Z

.field private mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

.field private mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

.field private masters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;",
            ">;"
        }
    .end annotation
.end field

.field private presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

.field private shape_Count:I

.field private slide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

.field slideCnt:I

.field private slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

.field private slides:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;",
            ">;"
        }
    .end annotation
.end field

.field tableCnt:I

.field private tableStyles:Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 121
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->empty()Lorg/apache/poi/openxml4j/opc/OPCPackage;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;-><init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;)V

    .line 122
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/io/File;Landroid/content/Context;)V
    .locals 1
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "file"    # Ljava/io/File;
    .param p3, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 155
    invoke-static {p1}, Lorg/apache/poi/util/PackageHelper;->open(Ljava/io/InputStream;)Lorg/apache/poi/openxml4j/opc/OPCPackage;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;-><init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;)V

    .line 156
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->context:Landroid/content/Context;

    .line 157
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->file:Ljava/io/File;

    .line 158
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->init()V

    .line 159
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;)V
    .locals 3
    .param p1, "pkg"    # Lorg/apache/poi/openxml4j/opc/OPCPackage;

    .prologue
    const/4 v2, 0x0

    .line 133
    invoke-direct {p0, p1}, Lorg/apache/poi/POIXMLDocument;-><init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;)V

    .line 106
    iput v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->shape_Count:I

    .line 107
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->URI_PREFIX:Ljava/lang/String;

    .line 108
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->chartLinkSrc:Z

    .line 109
    iput v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->incrId:I

    .line 110
    iput v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->tableCnt:I

    .line 111
    iput v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slideCnt:I

    .line 112
    iput-boolean v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->isHlink:Z

    .line 136
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->getCorePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getContentType()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->THEME_MANAGER:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->getContentType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 138
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->getPackage()Lorg/apache/poi/openxml4j/opc/OPCPackage;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->rebase(Lorg/apache/poi/openxml4j/opc/OPCPackage;)V

    .line 140
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->getCorePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getContentType()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->CHART:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->getContentType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 142
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->getPackage()Lorg/apache/poi/openxml4j/opc/OPCPackage;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->rebase(Lorg/apache/poi/openxml4j/opc/OPCPackage;)V

    .line 147
    :cond_1
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFFactory;->getInstance()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFFactory;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->load(Lorg/apache/poi/POIXMLFactory;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    return-void

    .line 148
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Lorg/apache/poi/POIXMLException;

    invoke-direct {v1, v0}, Lorg/apache/poi/POIXMLException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private drawBackground(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V
    .locals 26
    .param p1, "masterSlide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .param p2, "slideLayout"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;
    .param p3, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .prologue
    .line 351
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getBackground()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    move-result-object v21

    if-eqz v21, :cond_5

    .line 353
    new-instance v7, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;

    invoke-direct {v7}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;-><init>()V

    .line 354
    .local v7, "bgCont":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getBackground()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    move-result-object v5

    .line 356
    .local v5, "bg":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;
    if-eqz v5, :cond_1

    .line 357
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getBgFillRefNumber()I

    move-result v21

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_3

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getBgFillRefNumber()I

    move-result v21

    const/16 v22, 0x3e8

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_3

    .line 359
    const/4 v12, 0x0

    .line 360
    .local v12, "color":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getBgRefClr()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 363
    .local v19, "strColor":Ljava/lang/String;
    if-eqz v19, :cond_21

    :try_start_0
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x6

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_21

    .line 364
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getThemeName()Ljava/lang/String;

    move-result-object v21

    if-eqz v21, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getThemeName()Ljava/lang/String;

    move-result-object v21

    const-string/jumbo v22, "Foundry"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_2

    .line 372
    new-instance v13, Lorg/apache/poi/java/awt/Color;

    const/16 v21, 0x0

    const/16 v22, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x10

    invoke-static/range {v21 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v21

    const/16 v22, 0x2

    const/16 v23, 0x4

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x10

    invoke-static/range {v22 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v22

    const/16 v23, 0x4

    const/16 v24, 0x6

    move-object/from16 v0, v19

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x10

    invoke-static/range {v23 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v23

    const/16 v24, 0xc8

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-direct {v13, v0, v1, v2, v3}, Lorg/apache/poi/java/awt/Color;-><init>(IIII)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 389
    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .local v13, "color":Lorg/apache/poi/java/awt/Color;
    :goto_0
    if-nez v13, :cond_20

    :try_start_1
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v21

    if-eqz v21, :cond_20

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x6

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_20

    .line 394
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v19

    .line 396
    new-instance v12, Lorg/apache/poi/java/awt/Color;

    const/16 v21, 0x0

    const/16 v22, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x10

    invoke-static/range {v21 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v21

    const/16 v22, 0x2

    const/16 v23, 0x4

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x10

    invoke-static/range {v22 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v22

    const/16 v23, 0x4

    const/16 v24, 0x6

    move-object/from16 v0, v19

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x10

    invoke-static/range {v23 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v23

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v12, v0, v1, v2}, Lorg/apache/poi/java/awt/Color;-><init>(III)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_c

    .line 404
    .end local v13    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    :goto_1
    if-eqz v12, :cond_0

    .line 405
    :try_start_2
    invoke-virtual {v12}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->setBGColor(I)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0

    .line 467
    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .end local v19    # "strColor":Ljava/lang/String;
    :cond_0
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 761
    .end local v5    # "bg":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;
    .end local v7    # "bgCont":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    :cond_1
    :goto_3
    return-void

    .line 380
    .restart local v5    # "bg":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;
    .restart local v7    # "bgCont":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v19    # "strColor":Ljava/lang/String;
    :cond_2
    :try_start_3
    new-instance v13, Lorg/apache/poi/java/awt/Color;

    const/16 v21, 0x0

    const/16 v22, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x10

    invoke-static/range {v21 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v21

    const/16 v22, 0x2

    const/16 v23, 0x4

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x10

    invoke-static/range {v22 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v22

    const/16 v23, 0x4

    const/16 v24, 0x6

    move-object/from16 v0, v19

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x10

    invoke-static/range {v23 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v23

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v13, v0, v1, v2}, Lorg/apache/poi/java/awt/Color;-><init>(III)V
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_0

    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v13    # "color":Lorg/apache/poi/java/awt/Color;
    goto/16 :goto_0

    .line 407
    .end local v13    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    :catch_0
    move-exception v14

    .line 408
    .local v14, "e":Ljava/lang/NumberFormatException;
    :goto_4
    const-string/jumbo v21, "XMLSlideShow"

    invoke-virtual {v14}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 410
    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .end local v14    # "e":Ljava/lang/NumberFormatException;
    .end local v19    # "strColor":Ljava/lang/String;
    :cond_3
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getPictureData()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    move-result-object v21

    if-eqz v21, :cond_4

    .line 411
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getPictureData()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->file:Ljava/io/File;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->setFile(Ljava/io/File;)V

    .line 412
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getPictureData()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->getData()[B

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getPictureData()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->getData()[B

    move-result-object v23

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v23, v0

    invoke-static/range {v21 .. v23}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 415
    .local v10, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v10, :cond_0

    .line 416
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->setIsBackgroundImage(Z)V

    .line 417
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v17

    .line 419
    .local v17, "slideDim":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v22

    move-object/from16 v0, v17

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v21, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v22

    move-object/from16 v0, v17

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    const/16 v23, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-static {v10, v0, v1, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 424
    invoke-virtual {v7, v10}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->setBGBitMap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_2

    .line 426
    .end local v10    # "bitmap":Landroid/graphics/Bitmap;
    .end local v17    # "slideDim":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    :cond_4
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v21

    if-eqz v21, :cond_0

    .line 428
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->setIsBackgroundImage(Z)V

    .line 429
    const/4 v12, 0x0

    .line 430
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v21

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 435
    .restart local v19    # "strColor":Ljava/lang/String;
    if-eqz v19, :cond_1f

    :try_start_4
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x6

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_1f

    .line 436
    new-instance v13, Lorg/apache/poi/java/awt/Color;

    const/16 v21, 0x0

    const/16 v22, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x10

    invoke-static/range {v21 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v21

    const/16 v22, 0x2

    const/16 v23, 0x4

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x10

    invoke-static/range {v22 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v22

    const/16 v23, 0x4

    const/16 v24, 0x6

    move-object/from16 v0, v19

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x10

    invoke-static/range {v23 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v23

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v13, v0, v1, v2}, Lorg/apache/poi/java/awt/Color;-><init>(III)V
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_1

    .line 445
    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v13    # "color":Lorg/apache/poi/java/awt/Color;
    :goto_5
    if-nez v13, :cond_1e

    :try_start_5
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v21

    if-eqz v21, :cond_1e

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x6

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_1e

    .line 450
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v19

    .line 452
    new-instance v12, Lorg/apache/poi/java/awt/Color;

    const/16 v21, 0x0

    const/16 v22, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x10

    invoke-static/range {v21 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v21

    const/16 v22, 0x2

    const/16 v23, 0x4

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x10

    invoke-static/range {v22 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v22

    const/16 v23, 0x4

    const/16 v24, 0x6

    move-object/from16 v0, v19

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x10

    invoke-static/range {v23 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v23

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v12, v0, v1, v2}, Lorg/apache/poi/java/awt/Color;-><init>(III)V
    :try_end_5
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_5} :catch_b

    .line 460
    .end local v13    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    :goto_6
    if-eqz v12, :cond_0

    .line 461
    :try_start_6
    invoke-virtual {v12}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->setBGColor(I)V
    :try_end_6
    .catch Ljava/lang/NumberFormatException; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_2

    .line 463
    :catch_1
    move-exception v14

    .line 464
    .restart local v14    # "e":Ljava/lang/NumberFormatException;
    :goto_7
    const-string/jumbo v21, "XMLSlideShow"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v23, "NumberFormatException :"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 470
    .end local v5    # "bg":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;
    .end local v7    # "bgCont":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .end local v14    # "e":Ljava/lang/NumberFormatException;
    .end local v19    # "strColor":Ljava/lang/String;
    :cond_5
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getBackground()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    move-result-object v21

    if-eqz v21, :cond_12

    .line 471
    new-instance v8, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;

    invoke-direct {v8}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;-><init>()V

    .line 472
    .local v8, "bgCont1":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->setIsBackgroundImage(Z)V

    .line 473
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v18

    .line 475
    .local v18, "slideDim1":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    const/16 v20, 0x0

    .line 477
    .local v20, "themeBg":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getBgBlipFillIdLst()Ljava/util/ArrayList;

    move-result-object v21

    if-eqz v21, :cond_1

    .line 478
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getBgBlipFillIdLst()Ljava/util/ArrayList;

    move-result-object v6

    .line 480
    .local v6, "bgBlipLst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .line 481
    .local v15, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_6
    :goto_8
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_d

    .line 482
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 483
    .local v16, "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->isBgBlipTile(Ljava/lang/String;)Z

    move-result v21

    if-nez v21, :cond_6

    .line 484
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->file:Ljava/io/File;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->setFile(Ljava/io/File;)V

    .line 485
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getBgPictureData(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v21

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v22

    move-object/from16 v0, v18

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v24

    move-object/from16 v0, v18

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v24

    move-wide/from16 v0, v24

    double-to-int v0, v0

    move/from16 v23, v0

    const/16 v24, 0x0

    invoke-static/range {v21 .. v24}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 491
    .local v11, "bitmap1":Landroid/graphics/Bitmap;
    invoke-virtual {v8, v11}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->setBGBitMap(Landroid/graphics/Bitmap;)V

    .line 492
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getThemeName()Ljava/lang/String;

    move-result-object v21

    const-string/jumbo v22, "Couture"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-nez v21, :cond_7

    .line 494
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 497
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getThemeName()Ljava/lang/String;

    move-result-object v21

    if-eqz v21, :cond_c

    .line 498
    new-instance v9, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;

    invoke-direct {v9}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;-><init>()V

    .line 499
    .local v9, "bgCont2":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->setIsBackgroundImage(Z)V

    .line 500
    const/4 v12, 0x0

    .line 501
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    const/16 v19, 0x0

    .line 502
    .restart local v19    # "strColor":Ljava/lang/String;
    const/4 v4, 0x0

    .line 503
    .local v4, "alphaValue":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getThemeName()Ljava/lang/String;

    move-result-object v21

    const-string/jumbo v22, "Paper"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-nez v21, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getThemeName()Ljava/lang/String;

    move-result-object v21

    const-string/jumbo v22, "Hardcover"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_9

    .line 512
    :cond_8
    const-string/jumbo v21, "dk2"

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 515
    const/16 v4, 0x96

    .line 518
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getThemeName()Ljava/lang/String;

    move-result-object v21

    const-string/jumbo v22, "Trek"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_a

    .line 520
    const-string/jumbo v21, "lt2"

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 523
    const/16 v4, 0xb4

    .line 527
    :cond_a
    if-eqz v19, :cond_b

    :try_start_7
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x6

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_b

    .line 528
    new-instance v13, Lorg/apache/poi/java/awt/Color;

    const/16 v21, 0x0

    const/16 v22, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x10

    invoke-static/range {v21 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v21

    const/16 v22, 0x2

    const/16 v23, 0x4

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x10

    invoke-static/range {v22 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v22

    const/16 v23, 0x4

    const/16 v24, 0x6

    move-object/from16 v0, v19

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x10

    invoke-static/range {v23 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v23

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v13, v0, v1, v2, v4}, Lorg/apache/poi/java/awt/Color;-><init>(IIII)V

    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v13    # "color":Lorg/apache/poi/java/awt/Color;
    move-object v12, v13

    .line 538
    .end local v13    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    :cond_b
    if-eqz v12, :cond_c

    .line 539
    invoke-virtual {v12}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->setBGColor(I)V

    .line 540
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_7 .. :try_end_7} :catch_2

    .line 547
    .end local v4    # "alphaValue":I
    .end local v9    # "bgCont2":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .end local v19    # "strColor":Ljava/lang/String;
    :cond_c
    :goto_9
    const/16 v20, 0x1

    goto/16 :goto_8

    .line 542
    .restart local v4    # "alphaValue":I
    .restart local v9    # "bgCont2":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v19    # "strColor":Ljava/lang/String;
    :catch_2
    move-exception v14

    .line 543
    .restart local v14    # "e":Ljava/lang/NumberFormatException;
    const-string/jumbo v21, "XMLSlideShow"

    invoke-virtual {v14}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 551
    .end local v4    # "alphaValue":I
    .end local v9    # "bgCont2":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    .end local v11    # "bitmap1":Landroid/graphics/Bitmap;
    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .end local v14    # "e":Ljava/lang/NumberFormatException;
    .end local v16    # "key":Ljava/lang/String;
    .end local v19    # "strColor":Ljava/lang/String;
    :cond_d
    if-nez v20, :cond_1

    .line 552
    new-instance v7, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;

    invoke-direct {v7}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;-><init>()V

    .line 554
    .restart local v7    # "bgCont":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getBackground()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    move-result-object v5

    .line 556
    .restart local v5    # "bg":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;
    if-eqz v5, :cond_1

    .line 557
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getBgFillRefNumber()I

    move-result v21

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_10

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getBgFillRefNumber()I

    move-result v21

    const/16 v22, 0x3e8

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_10

    .line 559
    const/4 v12, 0x0

    .line 560
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getBgRefClr()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 563
    .restart local v19    # "strColor":Ljava/lang/String;
    if-eqz v19, :cond_1d

    :try_start_8
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x6

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_1d

    .line 564
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getThemeName()Ljava/lang/String;

    move-result-object v21

    if-eqz v21, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getThemeName()Ljava/lang/String;

    move-result-object v21

    const-string/jumbo v22, "Foundry"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_f

    .line 572
    new-instance v13, Lorg/apache/poi/java/awt/Color;

    const/16 v21, 0x0

    const/16 v22, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x10

    invoke-static/range {v21 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v21

    const/16 v22, 0x2

    const/16 v23, 0x4

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x10

    invoke-static/range {v22 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v22

    const/16 v23, 0x4

    const/16 v24, 0x6

    move-object/from16 v0, v19

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x10

    invoke-static/range {v23 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v23

    const/16 v24, 0xc8

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-direct {v13, v0, v1, v2, v3}, Lorg/apache/poi/java/awt/Color;-><init>(IIII)V
    :try_end_8
    .catch Ljava/lang/NumberFormatException; {:try_start_8 .. :try_end_8} :catch_3

    .line 589
    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v13    # "color":Lorg/apache/poi/java/awt/Color;
    :goto_a
    if-nez v13, :cond_1c

    :try_start_9
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v21

    if-eqz v21, :cond_1c

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x6

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_1c

    .line 594
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v19

    .line 596
    new-instance v12, Lorg/apache/poi/java/awt/Color;

    const/16 v21, 0x0

    const/16 v22, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x10

    invoke-static/range {v21 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v21

    const/16 v22, 0x2

    const/16 v23, 0x4

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x10

    invoke-static/range {v22 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v22

    const/16 v23, 0x4

    const/16 v24, 0x6

    move-object/from16 v0, v19

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x10

    invoke-static/range {v23 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v23

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v12, v0, v1, v2}, Lorg/apache/poi/java/awt/Color;-><init>(III)V
    :try_end_9
    .catch Ljava/lang/NumberFormatException; {:try_start_9 .. :try_end_9} :catch_a

    .line 604
    .end local v13    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    :goto_b
    if-eqz v12, :cond_e

    .line 605
    :try_start_a
    invoke-virtual {v12}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->setBGColor(I)V
    :try_end_a
    .catch Ljava/lang/NumberFormatException; {:try_start_a .. :try_end_a} :catch_3

    .line 667
    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .end local v19    # "strColor":Ljava/lang/String;
    :cond_e
    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_3

    .line 580
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v19    # "strColor":Ljava/lang/String;
    :cond_f
    :try_start_b
    new-instance v13, Lorg/apache/poi/java/awt/Color;

    const/16 v21, 0x0

    const/16 v22, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x10

    invoke-static/range {v21 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v21

    const/16 v22, 0x2

    const/16 v23, 0x4

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x10

    invoke-static/range {v22 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v22

    const/16 v23, 0x4

    const/16 v24, 0x6

    move-object/from16 v0, v19

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x10

    invoke-static/range {v23 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v23

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v13, v0, v1, v2}, Lorg/apache/poi/java/awt/Color;-><init>(III)V
    :try_end_b
    .catch Ljava/lang/NumberFormatException; {:try_start_b .. :try_end_b} :catch_3

    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v13    # "color":Lorg/apache/poi/java/awt/Color;
    goto/16 :goto_a

    .line 607
    .end local v13    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    :catch_3
    move-exception v14

    .line 608
    .restart local v14    # "e":Ljava/lang/NumberFormatException;
    :goto_d
    const-string/jumbo v21, "XMLSlideShow"

    invoke-virtual {v14}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_c

    .line 610
    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .end local v14    # "e":Ljava/lang/NumberFormatException;
    .end local v19    # "strColor":Ljava/lang/String;
    :cond_10
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getPictureData()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    move-result-object v21

    if-eqz v21, :cond_11

    .line 611
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getPictureData()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->file:Ljava/io/File;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->setFile(Ljava/io/File;)V

    .line 612
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getPictureData()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->getData()[B

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getPictureData()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->getData()[B

    move-result-object v23

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v23, v0

    invoke-static/range {v21 .. v23}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 615
    .restart local v10    # "bitmap":Landroid/graphics/Bitmap;
    if-eqz v10, :cond_e

    .line 616
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->setIsBackgroundImage(Z)V

    .line 617
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v17

    .line 619
    .restart local v17    # "slideDim":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v22

    move-object/from16 v0, v17

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v21, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v22

    move-object/from16 v0, v17

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    const/16 v23, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-static {v10, v0, v1, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 624
    invoke-virtual {v7, v10}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->setBGBitMap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_c

    .line 626
    .end local v10    # "bitmap":Landroid/graphics/Bitmap;
    .end local v17    # "slideDim":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    :cond_11
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v21

    if-eqz v21, :cond_e

    .line 628
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->setIsBackgroundImage(Z)V

    .line 629
    const/4 v12, 0x0

    .line 630
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v21

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 635
    .restart local v19    # "strColor":Ljava/lang/String;
    if-eqz v19, :cond_1b

    :try_start_c
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x6

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_1b

    .line 636
    new-instance v13, Lorg/apache/poi/java/awt/Color;

    const/16 v21, 0x0

    const/16 v22, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x10

    invoke-static/range {v21 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v21

    const/16 v22, 0x2

    const/16 v23, 0x4

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x10

    invoke-static/range {v22 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v22

    const/16 v23, 0x4

    const/16 v24, 0x6

    move-object/from16 v0, v19

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x10

    invoke-static/range {v23 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v23

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v13, v0, v1, v2}, Lorg/apache/poi/java/awt/Color;-><init>(III)V
    :try_end_c
    .catch Ljava/lang/NumberFormatException; {:try_start_c .. :try_end_c} :catch_4

    .line 645
    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v13    # "color":Lorg/apache/poi/java/awt/Color;
    :goto_e
    if-nez v13, :cond_1a

    :try_start_d
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v21

    if-eqz v21, :cond_1a

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x6

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_1a

    .line 650
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v19

    .line 652
    new-instance v12, Lorg/apache/poi/java/awt/Color;

    const/16 v21, 0x0

    const/16 v22, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x10

    invoke-static/range {v21 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v21

    const/16 v22, 0x2

    const/16 v23, 0x4

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x10

    invoke-static/range {v22 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v22

    const/16 v23, 0x4

    const/16 v24, 0x6

    move-object/from16 v0, v19

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x10

    invoke-static/range {v23 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v23

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v12, v0, v1, v2}, Lorg/apache/poi/java/awt/Color;-><init>(III)V
    :try_end_d
    .catch Ljava/lang/NumberFormatException; {:try_start_d .. :try_end_d} :catch_9

    .line 660
    .end local v13    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    :goto_f
    if-eqz v12, :cond_e

    .line 661
    :try_start_e
    invoke-virtual {v12}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->setBGColor(I)V
    :try_end_e
    .catch Ljava/lang/NumberFormatException; {:try_start_e .. :try_end_e} :catch_4

    goto/16 :goto_c

    .line 663
    :catch_4
    move-exception v14

    .line 664
    .restart local v14    # "e":Ljava/lang/NumberFormatException;
    :goto_10
    const-string/jumbo v21, "XMLSlideShow"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v23, "NumberFormatException :"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_c

    .line 672
    .end local v5    # "bg":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;
    .end local v6    # "bgBlipLst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7    # "bgCont":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    .end local v8    # "bgCont1":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .end local v14    # "e":Ljava/lang/NumberFormatException;
    .end local v15    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v18    # "slideDim1":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    .end local v19    # "strColor":Ljava/lang/String;
    .end local v20    # "themeBg":Z
    :cond_12
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getBackground()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    move-result-object v21

    if-eqz v21, :cond_1

    .line 673
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getBackground()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    move-result-object v5

    .line 675
    .restart local v5    # "bg":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;
    if-eqz v5, :cond_1

    .line 676
    new-instance v7, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;

    invoke-direct {v7}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;-><init>()V

    .line 677
    .restart local v7    # "bgCont":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getBgFillRefNumber()I

    move-result v21

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_14

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getBgFillRefNumber()I

    move-result v21

    const/16 v22, 0x3e8

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_14

    .line 679
    const/4 v12, 0x0

    .line 680
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v21

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getBgRefClr()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 683
    .restart local v19    # "strColor":Ljava/lang/String;
    if-eqz v19, :cond_19

    :try_start_f
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x6

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_19

    .line 684
    new-instance v13, Lorg/apache/poi/java/awt/Color;

    const/16 v21, 0x0

    const/16 v22, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x10

    invoke-static/range {v21 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v21

    const/16 v22, 0x2

    const/16 v23, 0x4

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x10

    invoke-static/range {v22 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v22

    const/16 v23, 0x4

    const/16 v24, 0x6

    move-object/from16 v0, v19

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x10

    invoke-static/range {v23 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v23

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v13, v0, v1, v2}, Lorg/apache/poi/java/awt/Color;-><init>(III)V
    :try_end_f
    .catch Ljava/lang/NumberFormatException; {:try_start_f .. :try_end_f} :catch_5

    .line 691
    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v13    # "color":Lorg/apache/poi/java/awt/Color;
    :goto_11
    if-nez v13, :cond_18

    :try_start_10
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v21

    if-eqz v21, :cond_18

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x6

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_18

    .line 695
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v19

    .line 696
    new-instance v12, Lorg/apache/poi/java/awt/Color;

    const/16 v21, 0x0

    const/16 v22, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x10

    invoke-static/range {v21 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v21

    const/16 v22, 0x2

    const/16 v23, 0x4

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x10

    invoke-static/range {v22 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v22

    const/16 v23, 0x4

    const/16 v24, 0x6

    move-object/from16 v0, v19

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x10

    invoke-static/range {v23 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v23

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v12, v0, v1, v2}, Lorg/apache/poi/java/awt/Color;-><init>(III)V
    :try_end_10
    .catch Ljava/lang/NumberFormatException; {:try_start_10 .. :try_end_10} :catch_8

    .line 702
    .end local v13    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    :goto_12
    if-eqz v12, :cond_13

    .line 703
    :try_start_11
    invoke-virtual {v12}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->setBGColor(I)V
    :try_end_11
    .catch Ljava/lang/NumberFormatException; {:try_start_11 .. :try_end_11} :catch_5

    .line 758
    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .end local v19    # "strColor":Ljava/lang/String;
    :cond_13
    :goto_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_3

    .line 705
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v19    # "strColor":Ljava/lang/String;
    :catch_5
    move-exception v14

    .line 706
    .restart local v14    # "e":Ljava/lang/NumberFormatException;
    :goto_14
    const-string/jumbo v21, "XMLSlideShow"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v23, "NumberFormatException :"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_13

    .line 708
    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .end local v14    # "e":Ljava/lang/NumberFormatException;
    .end local v19    # "strColor":Ljava/lang/String;
    :cond_14
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getPictureData()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    move-result-object v21

    if-eqz v21, :cond_15

    .line 709
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getPictureData()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->file:Ljava/io/File;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->setFile(Ljava/io/File;)V

    .line 710
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getPictureData()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->getData()[B

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getPictureData()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->getData()[B

    move-result-object v23

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v23, v0

    invoke-static/range {v21 .. v23}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 713
    .restart local v10    # "bitmap":Landroid/graphics/Bitmap;
    if-eqz v10, :cond_13

    .line 714
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->setIsBackgroundImage(Z)V

    .line 715
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v17

    .line 717
    .restart local v17    # "slideDim":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v22

    move-object/from16 v0, v17

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v21, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v22

    move-object/from16 v0, v17

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    const/16 v23, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-static {v10, v0, v1, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 722
    invoke-virtual {v7, v10}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->setBGBitMap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_13

    .line 724
    .end local v10    # "bitmap":Landroid/graphics/Bitmap;
    .end local v17    # "slideDim":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    :cond_15
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v21

    if-eqz v21, :cond_13

    .line 726
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->setIsBackgroundImage(Z)V

    .line 727
    const/4 v12, 0x0

    .line 728
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v21

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 732
    .restart local v19    # "strColor":Ljava/lang/String;
    if-eqz v19, :cond_17

    :try_start_12
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x6

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_17

    .line 733
    new-instance v13, Lorg/apache/poi/java/awt/Color;

    const/16 v21, 0x0

    const/16 v22, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x10

    invoke-static/range {v21 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v21

    const/16 v22, 0x2

    const/16 v23, 0x4

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x10

    invoke-static/range {v22 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v22

    const/16 v23, 0x4

    const/16 v24, 0x6

    move-object/from16 v0, v19

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x10

    invoke-static/range {v23 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v23

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v13, v0, v1, v2}, Lorg/apache/poi/java/awt/Color;-><init>(III)V
    :try_end_12
    .catch Ljava/lang/NumberFormatException; {:try_start_12 .. :try_end_12} :catch_6

    .line 740
    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v13    # "color":Lorg/apache/poi/java/awt/Color;
    :goto_15
    if-nez v13, :cond_16

    :try_start_13
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v21

    if-eqz v21, :cond_16

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x6

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_16

    .line 744
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v19

    .line 745
    new-instance v12, Lorg/apache/poi/java/awt/Color;

    const/16 v21, 0x0

    const/16 v22, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x10

    invoke-static/range {v21 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v21

    const/16 v22, 0x2

    const/16 v23, 0x4

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x10

    invoke-static/range {v22 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v22

    const/16 v23, 0x4

    const/16 v24, 0x6

    move-object/from16 v0, v19

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x10

    invoke-static/range {v23 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v23

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v12, v0, v1, v2}, Lorg/apache/poi/java/awt/Color;-><init>(III)V
    :try_end_13
    .catch Ljava/lang/NumberFormatException; {:try_start_13 .. :try_end_13} :catch_7

    .line 751
    .end local v13    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    :goto_16
    if-eqz v12, :cond_13

    .line 752
    :try_start_14
    invoke-virtual {v12}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->setBGColor(I)V
    :try_end_14
    .catch Ljava/lang/NumberFormatException; {:try_start_14 .. :try_end_14} :catch_6

    goto/16 :goto_13

    .line 754
    :catch_6
    move-exception v14

    .line 755
    .restart local v14    # "e":Ljava/lang/NumberFormatException;
    :goto_17
    const-string/jumbo v21, "XMLSlideShow"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v23, "NumberFormatException :"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_13

    .line 754
    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .end local v14    # "e":Ljava/lang/NumberFormatException;
    .restart local v13    # "color":Lorg/apache/poi/java/awt/Color;
    :catch_7
    move-exception v14

    move-object v12, v13

    .end local v13    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    goto :goto_17

    .line 705
    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v13    # "color":Lorg/apache/poi/java/awt/Color;
    :catch_8
    move-exception v14

    move-object v12, v13

    .end local v13    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    goto/16 :goto_14

    .line 663
    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v6    # "bgBlipLst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v8    # "bgCont1":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    .restart local v13    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v15    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .restart local v18    # "slideDim1":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    .restart local v20    # "themeBg":Z
    :catch_9
    move-exception v14

    move-object v12, v13

    .end local v13    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    goto/16 :goto_10

    .line 607
    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v13    # "color":Lorg/apache/poi/java/awt/Color;
    :catch_a
    move-exception v14

    move-object v12, v13

    .end local v13    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    goto/16 :goto_d

    .line 463
    .end local v6    # "bgBlipLst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8    # "bgCont1":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .end local v15    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v18    # "slideDim1":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    .end local v20    # "themeBg":Z
    .restart local v13    # "color":Lorg/apache/poi/java/awt/Color;
    :catch_b
    move-exception v14

    move-object v12, v13

    .end local v13    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    goto/16 :goto_7

    .line 407
    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v13    # "color":Lorg/apache/poi/java/awt/Color;
    :catch_c
    move-exception v14

    move-object v12, v13

    .end local v13    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    goto/16 :goto_4

    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v13    # "color":Lorg/apache/poi/java/awt/Color;
    :cond_16
    move-object v12, v13

    .end local v13    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    goto :goto_16

    :cond_17
    move-object v13, v12

    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v13    # "color":Lorg/apache/poi/java/awt/Color;
    goto/16 :goto_15

    :cond_18
    move-object v12, v13

    .end local v13    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    goto/16 :goto_12

    :cond_19
    move-object v13, v12

    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v13    # "color":Lorg/apache/poi/java/awt/Color;
    goto/16 :goto_11

    .restart local v6    # "bgBlipLst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v8    # "bgCont1":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    .restart local v15    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .restart local v18    # "slideDim1":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    .restart local v20    # "themeBg":Z
    :cond_1a
    move-object v12, v13

    .end local v13    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    goto/16 :goto_f

    :cond_1b
    move-object v13, v12

    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v13    # "color":Lorg/apache/poi/java/awt/Color;
    goto/16 :goto_e

    :cond_1c
    move-object v12, v13

    .end local v13    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    goto/16 :goto_b

    :cond_1d
    move-object v13, v12

    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v13    # "color":Lorg/apache/poi/java/awt/Color;
    goto/16 :goto_a

    .end local v6    # "bgBlipLst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8    # "bgCont1":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    .end local v15    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v18    # "slideDim1":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    .end local v20    # "themeBg":Z
    :cond_1e
    move-object v12, v13

    .end local v13    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    goto/16 :goto_6

    :cond_1f
    move-object v13, v12

    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v13    # "color":Lorg/apache/poi/java/awt/Color;
    goto/16 :goto_5

    :cond_20
    move-object v12, v13

    .end local v13    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v12    # "color":Lorg/apache/poi/java/awt/Color;
    goto/16 :goto_1

    :cond_21
    move-object v13, v12

    .end local v12    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v13    # "color":Lorg/apache/poi/java/awt/Color;
    goto/16 :goto_0
.end method

.method static final empty()Lorg/apache/poi/openxml4j/opc/OPCPackage;
    .locals 4

    .prologue
    .line 216
    const-class v2, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;

    const-string/jumbo v3, "empty.pptx"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 217
    .local v1, "is":Ljava/io/InputStream;
    if-nez v1, :cond_0

    .line 218
    new-instance v2, Ljava/lang/RuntimeException;

    const-string/jumbo v3, "Missing resource \'empty.pptx\'"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 221
    :cond_0
    :try_start_0
    invoke-static {v1}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->open(Ljava/io/InputStream;)Lorg/apache/poi/openxml4j/opc/OPCPackage;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 222
    :catch_0
    move-exception v0

    .line 223
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Lorg/apache/poi/POIXMLException;

    invoke-direct {v2, v0}, Lorg/apache/poi/POIXMLException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method private getDefProperties(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;I)Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;
    .locals 2
    .param p1, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p2, "curShape"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    .param p3, "phShape"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    .param p4, "level"    # I

    .prologue
    .line 1927
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->isTextBox()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1928
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideShow()Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->getPresentation()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    move-result-object v0

    invoke-virtual {p0, p4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->getLvlName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getDefTextStyleProp(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;

    move-result-object v0

    .line 1935
    :goto_0
    return-object v0

    .line 1932
    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getLstStyle()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 1933
    :cond_1
    invoke-virtual {p0, p1, p2, p4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->getStyleFromMaster(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;I)Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;

    move-result-object v0

    goto :goto_0

    .line 1935
    :cond_2
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getLstStyle()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0, p4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->getLvlName(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;

    invoke-virtual {p0, p1, p2, p4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->getStyleFromMaster(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;I)Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mergeProperties(Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;)Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;

    move-result-object v0

    goto :goto_0
.end method

.method private init()V
    .locals 4

    .prologue
    .line 162
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->folderName:Ljava/lang/String;

    .line 163
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->folderName:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->folderPath:Ljava/io/File;

    .line 164
    return-void
.end method

.method private processShapes([Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V
    .locals 36
    .param p1, "shapes"    # [Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;
    .param p2, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .prologue
    .line 769
    if-nez p1, :cond_1

    .line 928
    :cond_0
    return-void

    .line 773
    :cond_1
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_0
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v31, v0

    move/from16 v0, v31

    if-ge v15, v0, :cond_0

    .line 775
    aget-object v21, p1, v15

    .line 776
    .local v21, "shape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;
    move-object/from16 v0, v21

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move/from16 v31, v0

    if-eqz v31, :cond_6

    move-object/from16 v6, v21

    .line 777
    check-cast v6, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    .line 778
    .local v6, "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->isGroup()Z

    move-result v31

    if-eqz v31, :cond_3

    .line 779
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getShapes()Ljava/util/ArrayList;

    move-result-object v22

    .line 780
    .local v22, "shapeLst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;>;"
    if-eqz v22, :cond_2

    .line 781
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v31

    move/from16 v0, v31

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;

    move-object/from16 v31, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v31

    check-cast v31, [Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;

    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->processShapes([Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 773
    .end local v6    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    .end local v22    # "shapeLst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;>;"
    :cond_2
    :goto_1
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 785
    .restart local v6    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    :cond_3
    const/16 v31, 0x0

    const/16 v32, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v31

    move-object/from16 v3, v32

    invoke-virtual {v0, v1, v6, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->drawShape(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Ljava/util/List;)V

    .line 787
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v24

    .line 788
    .local v24, "shapeProp":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getPrstShapeName()Ljava/lang/String;

    move-result-object v23

    .line 789
    .local v23, "shapeName":Ljava/lang/String;
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getRefId()Ljava/lang/String;

    move-result-object v31

    if-eqz v31, :cond_2

    .line 790
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;)V

    .line 792
    .local v18, "picShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->file:Ljava/io/File;

    move-object/from16 v31, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->setFile(Ljava/io/File;)V

    .line 793
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getRefId()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v18

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->setBlipEmbed(Ljava/lang/String;)V

    .line 795
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->folderName:Ljava/lang/String;

    move-object/from16 v31, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v31

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->drawPicture(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    move-result-object v7

    .line 796
    .local v7, "bi":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    if-eqz v7, :cond_2

    .line 797
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getRotation()I

    move-result v31

    invoke-static/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->rotationAngelToDegrees16_16(I)I

    move-result v19

    .line 800
    .local v19, "rotation":I
    move/from16 v0, v19

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setRotationAngle(I)V

    .line 801
    const-string/jumbo v31, "ellipse"

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v31

    if-nez v31, :cond_4

    const-string/jumbo v31, "oval"

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v31

    if-nez v31, :cond_4

    const-string/jumbo v31, "circle"

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_5

    .line 804
    :cond_4
    const-string/jumbo v31, "ellipse"

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setEnclosedName(Ljava/lang/String;)V

    .line 806
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 810
    .end local v6    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    .end local v7    # "bi":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .end local v18    # "picShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;
    .end local v19    # "rotation":I
    .end local v23    # "shapeName":Ljava/lang/String;
    .end local v24    # "shapeProp":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    :cond_6
    move-object/from16 v0, v21

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

    move/from16 v31, v0

    if-eqz v31, :cond_7

    move-object/from16 v18, v21

    .line 811
    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

    .line 812
    .restart local v18    # "picShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->file:Ljava/io/File;

    move-object/from16 v31, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->setFile(Ljava/io/File;)V

    .line 813
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->folderName:Ljava/lang/String;

    move-object/from16 v31, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v31

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->drawPicture(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    move-result-object v7

    .line 814
    .restart local v7    # "bi":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    if-eqz v7, :cond_2

    .line 816
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getRotation()I

    move-result v31

    invoke-static/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->rotationAngelToDegrees16_16(I)I

    move-result v19

    .line 818
    .restart local v19    # "rotation":I
    move/from16 v0, v19

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setRotationAngle(I)V

    .line 819
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 821
    .end local v7    # "bi":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .end local v18    # "picShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;
    .end local v19    # "rotation":I
    :cond_7
    move-object/from16 v0, v21

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    move/from16 v31, v0

    if-eqz v31, :cond_8

    .line 822
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->tableCnt:I

    move/from16 v31, v0

    add-int/lit8 v31, v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->tableCnt:I

    .line 823
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->tableCnt:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->setTableNumber(I)V

    move-object/from16 v25, v21

    .line 824
    check-cast v25, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    .line 826
    .local v25, "table":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->tableStyles:Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v32, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p2

    move-object/from16 v2, p0

    move-object/from16 v3, v31

    move-object/from16 v4, v32

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->drawTable(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    move-result-object v8

    .line 828
    .local v8, "canvasTable":Lcom/samsung/thumbnail/customview/tables/CanvasTable;
    sget-object v31, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPTX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-object/from16 v0, v31

    invoke-virtual {v8, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 829
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v8}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 830
    .end local v8    # "canvasTable":Lcom/samsung/thumbnail/customview/tables/CanvasTable;
    .end local v25    # "table":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;
    :cond_8
    move-object/from16 v0, v21

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;

    move/from16 v31, v0

    if-eqz v31, :cond_2

    move-object/from16 v12, v21

    .line 832
    check-cast v12, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;

    .line 833
    .local v12, "graphicFrame":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v20

    .line 838
    .local v20, "shInfo":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeHeight()J

    move-result-wide v32

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v14

    .line 839
    .local v14, "ht":Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeWidth()J

    move-result-wide v32

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v29

    .line 840
    .local v29, "wd":Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeXvalue()J

    move-result-wide v32

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v27

    .line 841
    .local v27, "valX":Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeYvalue()J

    move-result-wide v32

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v28

    .line 843
    .local v28, "valY":Ljava/lang/String;
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v31

    move/from16 v0, v31

    int-to-float v13, v0

    .line 844
    .local v13, "height":F
    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v31

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v30, v0

    .line 845
    .local v30, "width":F
    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v26

    .line 846
    .local v26, "top":I
    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    .line 848
    .local v17, "left":I
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;->getIsChart()Z

    move-result v31

    if-eqz v31, :cond_9

    .line 849
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;->getRelId()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getChartContents(Ljava/lang/String;)Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    move-result-object v9

    .line 852
    .local v9, "chart":Lcom/samsung/thumbnail/customview/hslf/ChartContents;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getIsChart()Z

    move-result v31

    const/16 v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_2

    if-eqz v9, :cond_2

    .line 853
    move/from16 v0, v17

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setX(I)V

    .line 854
    move/from16 v0, v26

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setY(I)V

    .line 856
    move/from16 v0, v30

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartWidth(F)V

    .line 857
    invoke-virtual {v9, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartHeight(F)V

    .line 858
    sget-object v31, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->CENTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v31

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    .line 860
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v9}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 862
    .end local v9    # "chart":Lcom/samsung/thumbnail/customview/hslf/ChartContents;
    :cond_9
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;->isDiagram()Z

    move-result v31

    if-eqz v31, :cond_2

    .line 864
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;->getRelId()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getDiagramDataModelId(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

    move-result-object v10

    .line 866
    .local v10, "dataModel":Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;->parseDataModel()V

    .line 868
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;->getDiagramRelId()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getDiagramRelId(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    move-result-object v11

    .line 870
    .local v11, "diagram":Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;
    move-object/from16 v0, p2

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->setSheet(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;)V

    .line 871
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v31

    sget-object v32, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPTX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v11, v10, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->parseDiagram(Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 875
    const/16 v16, 0x0

    .local v16, "j":I
    :goto_2
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->getXSLFShapeAL()Ljava/util/ArrayList;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->size()I

    move-result v31

    move/from16 v0, v16

    move/from16 v1, v31

    if-ge v0, v1, :cond_2

    .line 876
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->getXSLFShapeAL()Ljava/util/ArrayList;

    move-result-object v31

    move-object/from16 v0, v31

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    .line 878
    .restart local v6    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v24

    .line 880
    .restart local v24    # "shapeProp":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getPrstShapeName()Ljava/lang/String;

    move-result-object v23

    .line 882
    .restart local v23    # "shapeName":Ljava/lang/String;
    const-string/jumbo v31, "trapezium"

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v31

    if-nez v31, :cond_a

    const-string/jumbo v31, "trapezoid"

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v31

    if-nez v31, :cond_a

    const-string/jumbo v31, "blockArc"

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v31

    if-nez v31, :cond_a

    const-string/jumbo v31, "arc"

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_b

    .line 886
    :cond_a
    new-instance v31, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v32, "Shape is not supported"

    invoke-direct/range {v31 .. v32}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v31

    .line 890
    :cond_b
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeXvalue()J

    move-result-wide v32

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v34, v0

    add-long v32, v32, v34

    move-object/from16 v0, v24

    move-wide/from16 v1, v32

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setShapeXvalue(J)V

    .line 892
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeYvalue()J

    move-result-wide v32

    move/from16 v0, v26

    int-to-long v0, v0

    move-wide/from16 v34, v0

    add-long v32, v32, v34

    move-object/from16 v0, v24

    move-wide/from16 v1, v32

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setShapeYvalue(J)V

    .line 894
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v32

    move-wide/from16 v0, v32

    double-to-float v0, v0

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPPTHeight()J

    move-result-wide v32

    move-wide/from16 v0, v32

    long-to-float v0, v0

    move/from16 v32, v0

    div-float v31, v31, v32

    move-object/from16 v0, v24

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setSDRatio(F)V

    .line 899
    const/16 v31, 0x0

    const/16 v32, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v31

    move-object/from16 v3, v32

    invoke-virtual {v0, v1, v6, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->drawShape(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Ljava/util/List;)V

    .line 901
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getRefId()Ljava/lang/String;

    move-result-object v31

    if-eqz v31, :cond_e

    .line 902
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;)V

    .line 904
    .restart local v18    # "picShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->file:Ljava/io/File;

    move-object/from16 v31, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->setFile(Ljava/io/File;)V

    .line 905
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getRefId()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v18

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->setBlipEmbed(Ljava/lang/String;)V

    .line 906
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->setIsDiagramShape()V

    .line 907
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->getRelList()Ljava/util/List;

    move-result-object v31

    move-object/from16 v0, v18

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->setRelList(Ljava/util/List;)V

    .line 909
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->folderName:Ljava/lang/String;

    move-object/from16 v31, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v31

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->drawPicture(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    move-result-object v7

    .line 911
    .restart local v7    # "bi":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    if-eqz v7, :cond_e

    .line 912
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getRotation()I

    move-result v31

    invoke-static/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->rotationAngelToDegrees16_16(I)I

    move-result v19

    .line 915
    .restart local v19    # "rotation":I
    move/from16 v0, v19

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setRotationAngle(I)V

    .line 916
    const-string/jumbo v31, "ellipse"

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v31

    if-nez v31, :cond_c

    const-string/jumbo v31, "oval"

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v31

    if-nez v31, :cond_c

    const-string/jumbo v31, "circle"

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_d

    .line 919
    :cond_c
    const-string/jumbo v31, "ellipse"

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setEnclosedName(Ljava/lang/String;)V

    .line 921
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 875
    .end local v7    # "bi":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .end local v18    # "picShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;
    .end local v19    # "rotation":I
    :cond_e
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_2
.end method

.method private processShapes([Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;)V
    .locals 16
    .param p1, "shapes"    # [Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;
    .param p2, "slideLayout"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    .prologue
    .line 995
    if-nez p1, :cond_1

    .line 1055
    :cond_0
    return-void

    .line 998
    :cond_1
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    move-object/from16 v0, p1

    array-length v13, v0

    if-ge v4, v13, :cond_0

    .line 999
    aget-object v11, p1, v4

    .line 1000
    .local v11, "shape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;
    instance-of v13, v11, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    if-eqz v13, :cond_7

    move-object v2, v11

    .line 1001
    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    .line 1002
    .local v2, "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->isGroup()Z

    move-result v13

    if-eqz v13, :cond_3

    .line 1003
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getShapes()Ljava/util/ArrayList;

    move-result-object v12

    .line 1004
    .local v12, "shapeLst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;>;"
    if-eqz v12, :cond_2

    .line 1005
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v13

    new-array v13, v13, [Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v13, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->processShapes([Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;)V

    .line 998
    .end local v2    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    .end local v12    # "shapeLst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;>;"
    :cond_2
    :goto_1
    :pswitch_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1008
    .restart local v2    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    :cond_3
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v5

    .line 1009
    .local v5, "ph":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;
    if-eqz v5, :cond_6

    .line 1010
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v13

    iget-object v8, v13, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;->phType:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    .line 1011
    .local v8, "phType":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;
    if-nez v8, :cond_4

    .line 1013
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v13

    iget-wide v6, v13, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;->phIndex:J

    .line 1014
    .local v6, "phIndex":J
    const-wide/16 v14, 0x0

    cmp-long v13, v6, v14

    if-lez v13, :cond_5

    .line 1015
    sget-object v8, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->BODY:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    .line 1020
    .end local v6    # "phIndex":J
    :cond_4
    :goto_2
    sget-object v13, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$pptx$shapes$XSLFShape$EPlaceHolderType:[I

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->ordinal()I

    move-result v14

    aget v13, v13, v14

    packed-switch v13, :pswitch_data_0

    .line 1032
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v2, v14, v15}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->drawShape(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Ljava/util/List;)V

    goto :goto_1

    .line 1017
    .restart local v6    # "phIndex":J
    :cond_5
    sget-object v8, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->NONE:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    goto :goto_2

    .line 1036
    .end local v6    # "phIndex":J
    .end local v8    # "phType":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;
    :cond_6
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v2, v14, v15}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->drawShape(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Ljava/util/List;)V

    goto :goto_1

    .line 1039
    .end local v2    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    .end local v5    # "ph":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;
    :cond_7
    instance-of v13, v11, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

    if-eqz v13, :cond_8

    move-object v9, v11

    .line 1040
    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

    .line 1041
    .local v9, "picShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->file:Ljava/io/File;

    invoke-virtual {v9, v13}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->setFile(Ljava/io/File;)V

    .line 1042
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->folderName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    invoke-virtual {v9, v13, v14}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->drawPicture(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    move-result-object v3

    .line 1043
    .local v3, "bi":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    if-eqz v3, :cond_2

    .line 1045
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getRotation()I

    move-result v13

    invoke-static {v13}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->rotationAngelToDegrees16_16(I)I

    move-result v10

    .line 1047
    .local v10, "rotation":I
    invoke-virtual {v3, v10}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setRotationAngle(I)V

    .line 1048
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v13, v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto :goto_1

    .line 1050
    .end local v3    # "bi":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .end local v9    # "picShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;
    .end local v10    # "rotation":I
    :cond_8
    instance-of v13, v11, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    if-eqz v13, :cond_2

    .line 1051
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->tableCnt:I

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->tableCnt:I

    .line 1052
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->tableCnt:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->setTableNumber(I)V

    goto/16 :goto_1

    .line 1020
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private processShapes([Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V
    .locals 12
    .param p1, "shapes"    # [Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;
    .param p2, "masterSlide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    .prologue
    const/4 v11, 0x0

    .line 936
    if-nez p1, :cond_1

    .line 987
    :cond_0
    return-void

    .line 939
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v9, p1

    if-ge v2, v9, :cond_0

    .line 940
    aget-object v7, p1, v2

    .line 941
    .local v7, "shape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;
    instance-of v9, v7, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    if-eqz v9, :cond_6

    move-object v0, v7

    .line 942
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    .line 943
    .local v0, "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->isGroup()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 944
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getShapes()Ljava/util/ArrayList;

    move-result-object v8

    .line 945
    .local v8, "shapeLst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;>;"
    if-eqz v8, :cond_2

    .line 946
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    new-array v9, v9, [Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;

    invoke-direct {p0, v9, p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->processShapes([Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V

    .line 939
    .end local v0    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    .end local v8    # "shapeLst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;>;"
    :cond_2
    :goto_1
    :pswitch_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 951
    .restart local v0    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    :cond_3
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v3

    .line 952
    .local v3, "ph":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;
    if-eqz v3, :cond_5

    .line 953
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v9

    iget-object v4, v9, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;->phType:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    .line 954
    .local v4, "phType":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;
    if-nez v4, :cond_4

    .line 955
    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->NONE:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    .line 958
    :cond_4
    sget-object v9, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$pptx$shapes$XSLFShape$EPlaceHolderType:[I

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 968
    invoke-virtual {p0, p2, v0, v11, v11}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->drawShape(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Ljava/util/List;)V

    goto :goto_1

    .line 972
    .end local v4    # "phType":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;
    :cond_5
    invoke-virtual {p0, p2, v0, v11, v11}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->drawShape(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Ljava/util/List;)V

    goto :goto_1

    .line 974
    .end local v0    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    .end local v3    # "ph":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;
    :cond_6
    instance-of v9, v7, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

    if-eqz v9, :cond_2

    move-object v5, v7

    .line 975
    check-cast v5, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

    .line 976
    .local v5, "picShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->file:Ljava/io/File;

    invoke-virtual {v5, v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->setFile(Ljava/io/File;)V

    .line 977
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->folderName:Ljava/lang/String;

    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    invoke-virtual {v5, v9, v10}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->drawPicture(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    move-result-object v1

    .line 978
    .local v1, "bi":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    if-eqz v1, :cond_2

    .line 980
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getRotation()I

    move-result v9

    invoke-static {v9}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->rotationAngelToDegrees16_16(I)I

    move-result v6

    .line 982
    .local v6, "rotation":I
    invoke-virtual {v1, v6}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setRotationAngle(I)V

    .line 983
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v9, v1}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto :goto_1

    .line 958
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public drawCanvasSlide(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V
    .locals 1
    .param p1, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .prologue
    .line 344
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getShapes()[Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;

    move-result-object v0

    .line 345
    .local v0, "shapes":[Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;
    invoke-direct {p0, v0, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->processShapes([Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 346
    return-void
.end method

.method public drawCanvasSlide(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;Z)V
    .locals 1
    .param p1, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;
    .param p2, "showMasterShapes"    # Z

    .prologue
    .line 338
    if-eqz p2, :cond_0

    .line 339
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getShapes()[Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->processShapes([Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;)V

    .line 341
    :cond_0
    return-void
.end method

.method public drawCanvasSlide(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V
    .locals 1
    .param p1, "masterSlide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    .prologue
    .line 331
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->showMasterShape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getShapes()[Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->processShapes([Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V

    .line 334
    :cond_0
    return-void
.end method

.method public drawShape(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Ljava/util/List;)V
    .locals 66
    .param p1, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p2, "shape"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    .param p3, "shInfoProperties"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1382
    .local p4, "paragraphLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;>;"
    const/16 v31, 0x0

    .line 1383
    .local v31, "defProp":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;
    const/16 v56, 0x0

    .line 1385
    .local v56, "paraProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    const/4 v10, 0x0

    .line 1387
    .local v10, "shapeInfo":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    const/16 v58, 0x0

    .line 1388
    .local v58, "phShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    const/4 v6, 0x0

    .line 1390
    .local v6, "spCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    if-eqz p2, :cond_34

    .line 1392
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v10

    .line 1394
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getLstStyle()Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Map;->size()I

    move-result v7

    if-nez v7, :cond_0

    .line 1395
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getShapeForStyle(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;)Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-result-object v58

    .line 1399
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getAnchor()Lorg/apache/poi/java/awt/geom/Rectangle2D;

    move-result-object v17

    .line 1400
    .local v17, "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    if-nez v17, :cond_2

    .line 1401
    if-eqz v58, :cond_32

    .line 1402
    invoke-virtual/range {v58 .. v58}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getAnchor()Lorg/apache/poi/java/awt/geom/Rectangle2D;

    move-result-object v17

    .line 1403
    if-nez v17, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getShapeForAnchor(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;)Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 1405
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getShapeForAnchor(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;)Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getAnchor()Lorg/apache/poi/java/awt/geom/Rectangle2D;

    move-result-object v17

    .line 1413
    :cond_1
    :goto_0
    if-eqz v58, :cond_2

    .line 1414
    invoke-virtual/range {v58 .. v58}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getRotation()I

    move-result v7

    invoke-virtual {v10, v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setRotation(I)V

    .line 1419
    :cond_2
    if-nez v58, :cond_3

    .line 1420
    move-object/from16 v58, p2

    .line 1423
    :cond_3
    if-eqz v17, :cond_4

    .line 1424
    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->setAnchor(Lorg/apache/poi/java/awt/geom/Rectangle2D;)V

    .line 1425
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->toEMU(D)I

    move-result v7

    int-to-long v8, v7

    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v12

    invoke-static {v12, v13}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->toEMU(D)I

    move-result v7

    int-to-long v12, v7

    invoke-virtual {v10, v8, v9, v12, v13}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setSize(JJ)V

    .line 1427
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->toEMU(D)I

    move-result v7

    int-to-long v8, v7

    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v12

    invoke-static {v12, v13}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->toEMU(D)I

    move-result v7

    int-to-long v12, v7

    invoke-virtual {v10, v8, v9, v12, v13}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setPoints(JJ)V

    .line 1435
    .end local v17    # "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    :cond_4
    :goto_1
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->buMap:Ljava/util/HashMap;

    .line 1436
    const/16 v21, -0x1

    .line 1438
    .local v21, "buCount":I
    new-instance v6, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v6    # "spCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->getCanvas()Landroid/graphics/Canvas;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->context:Landroid/content/Context;

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->shape_Count:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->folderPath:Ljava/io/File;

    move-object/from16 v9, p2

    move-object/from16 v11, p1

    invoke-direct/range {v6 .. v13}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;ILjava/io/File;)V

    .line 1441
    .restart local v6    # "spCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    const/16 v59, -0x1

    .local v59, "preLevel":I
    const/16 v22, -0x1

    .line 1442
    .local v22, "buLevel":I
    const/16 v60, 0x0

    .line 1443
    .local v60, "prevBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    const/16 v55, 0x0

    .line 1444
    .local v55, "paraLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;>;"
    if-eqz p2, :cond_35

    .line 1445
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getParagraphLst()Ljava/util/List;

    move-result-object v55

    .line 1450
    :cond_5
    :goto_2
    const/16 v41, 0x0

    .local v41, "i":I
    :goto_3
    invoke-interface/range {v55 .. v55}, Ljava/util/List;->size()I

    move-result v7

    move/from16 v0, v41

    if-ge v0, v7, :cond_64

    .line 1451
    new-instance v32, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-direct/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    .line 1453
    .local v32, "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    move-object/from16 v0, v55

    move/from16 v1, v41

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v54

    check-cast v54, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    .line 1454
    .local v54, "para":Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;
    const/16 v48, 0x0

    .line 1455
    .local v48, "level":I
    const/16 v16, 0x0

    .line 1456
    .local v16, "align":Ljava/lang/String;
    const/16 v42, 0x0

    .line 1457
    .local v42, "indent":Ljava/lang/String;
    const/16 v50, 0x0

    .line 1458
    .local v50, "marginLeft":Ljava/lang/String;
    const/16 v36, 0x0

    .line 1459
    .local v36, "eBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    const/16 v30, 0x0

    .line 1460
    .local v30, "curBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    const/16 v23, 0x0

    .line 1461
    .local v23, "buNum":Ljava/lang/String;
    const/16 v20, 0x0

    .line 1462
    .local v20, "buChar":Ljava/lang/String;
    invoke-virtual/range {v54 .. v54}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v56

    .line 1464
    if-eqz v56, :cond_6

    .line 1465
    invoke-virtual/range {v56 .. v56}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getLevel()I

    move-result v48

    .line 1466
    invoke-virtual/range {v56 .. v56}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getJustify()Ljava/lang/String;

    move-result-object v16

    .line 1467
    invoke-virtual/range {v56 .. v56}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentPPT()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v42

    .line 1468
    invoke-virtual/range {v56 .. v56}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getBuAutoNum()Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    move-result-object v36

    .line 1469
    move-object/from16 v30, v36

    .line 1470
    invoke-virtual/range {v56 .. v56}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getMarginLeft()I

    move-result v7

    if-eqz v7, :cond_6

    .line 1471
    invoke-virtual/range {v56 .. v56}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getMarginLeft()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v50

    .line 1474
    :cond_6
    move/from16 v0, v59

    move/from16 v1, v48

    if-eq v0, v1, :cond_9

    .line 1475
    move/from16 v0, v59

    move/from16 v1, v48

    if-ge v0, v1, :cond_7

    .line 1476
    const/16 v21, -0x1

    .line 1478
    :cond_7
    if-eqz p2, :cond_8

    .line 1479
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v58

    move/from16 v4, v48

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->getDefProperties(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;I)Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;

    move-result-object v31

    .line 1481
    :cond_8
    move/from16 v22, v59

    .line 1485
    :cond_9
    if-eqz v50, :cond_a

    .line 1486
    :try_start_0
    invoke-static/range {v50 .. v50}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v51

    .line 1487
    .local v51, "marginSpace":I
    mul-int/lit8 v7, v48, 0x32

    add-int v51, v51, v7

    .line 1488
    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v50

    .line 1494
    .end local v51    # "marginSpace":I
    :cond_a
    :goto_4
    move/from16 v59, v48

    .line 1496
    if-eqz v31, :cond_f

    .line 1497
    if-nez v16, :cond_b

    .line 1498
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getJustify()Ljava/lang/String;

    move-result-object v16

    .line 1501
    :cond_b
    if-nez v42, :cond_c

    .line 1502
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentPPT()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v42

    .line 1506
    :cond_c
    if-nez v56, :cond_e

    .line 1507
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getMarginLeft()I

    move-result v7

    if-eqz v7, :cond_d

    .line 1508
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getMarginLeft()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v50

    .line 1511
    :cond_d
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v56

    .line 1513
    :cond_e
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    move-object/from16 v0, v56

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->mergeParaProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V

    .line 1516
    :cond_f
    if-eqz v50, :cond_10

    .line 1517
    invoke-static/range {v50 .. v50}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    .line 1520
    :cond_10
    if-eqz v16, :cond_11

    .line 1521
    const-string/jumbo v7, "center"

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_36

    .line 1522
    sget-object v7, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->CENTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    .line 1534
    :cond_11
    :goto_5
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getBodyProp()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    move-result-object v19

    .line 1538
    .local v19, "bodyProp":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;
    if-eqz v19, :cond_14

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;->getvAlign()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_14

    .line 1539
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getShapeForAnchor(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;)Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-result-object v57

    .line 1542
    .local v57, "parentShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    if-eqz v57, :cond_14

    .line 1543
    invoke-virtual/range {v57 .. v57}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getBodyProp()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    move-result-object v7

    if-eqz v7, :cond_12

    .line 1544
    invoke-virtual/range {v57 .. v57}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getBodyProp()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    move-result-object v19

    .line 1548
    :cond_12
    if-eqz v19, :cond_13

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;->getvAlign()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_14

    .line 1549
    :cond_13
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v63

    .line 1551
    .local v63, "sldMaster":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v7

    invoke-virtual/range {v63 .. v63}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getShapes()[Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;

    move-result-object v8

    move-object/from16 v0, v63

    invoke-virtual {v0, v7, v8}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getParentShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;[Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;)Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-result-object v52

    .line 1554
    .local v52, "masterParentShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    if-eqz v52, :cond_14

    invoke-virtual/range {v52 .. v52}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getBodyProp()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    move-result-object v7

    if-eqz v7, :cond_14

    .line 1556
    invoke-virtual/range {v52 .. v52}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getBodyProp()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    move-result-object v19

    .line 1563
    .end local v52    # "masterParentShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    .end local v57    # "parentShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    .end local v63    # "sldMaster":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    :cond_14
    if-eqz v19, :cond_15

    .line 1564
    const-string/jumbo v7, "ctr"

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;->getvAlign()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_39

    .line 1565
    sget-object v7, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->CENTERED:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    .line 1573
    :cond_15
    :goto_6
    if-eqz v56, :cond_16

    .line 1574
    invoke-virtual/range {v56 .. v56}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getLineSpacing()F

    move-result v7

    const/4 v8, 0x0

    cmpl-float v7, v7, v8

    if-eqz v7, :cond_16

    .line 1575
    invoke-virtual/range {v56 .. v56}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getLineSpacing()F

    move-result v7

    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineSpacing(F)V

    .line 1579
    :cond_16
    const-string/jumbo v24, ""

    .line 1582
    .local v24, "buText":Ljava/lang/String;
    if-eqz v56, :cond_19

    .line 1583
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isDiagramShape()Z

    move-result v7

    if-eqz v7, :cond_3f

    .line 1584
    invoke-virtual/range {v56 .. v56}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getBulletChar()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_19

    .line 1585
    move/from16 v0, v22

    move/from16 v1, v48

    if-eq v0, v1, :cond_17

    .line 1586
    move/from16 v0, v22

    move/from16 v1, v48

    if-le v0, v1, :cond_3b

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->buMap:Ljava/util/HashMap;

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_3b

    .line 1587
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->buMap:Ljava/util/HashMap;

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v21

    .line 1592
    :cond_17
    :goto_7
    invoke-virtual/range {v54 .. v54}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getRunList()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-eqz v7, :cond_18

    .line 1593
    add-int/lit8 v21, v21, 0x1

    .line 1595
    :cond_18
    invoke-virtual/range {v56 .. v56}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getBulletChar()Ljava/lang/String;

    move-result-object v20

    .line 1597
    invoke-static/range {v20 .. v20}, Lcom/samsung/thumbnail/util/Utils;->isValidInteger(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3c

    .line 1598
    move-object/from16 v24, v20

    .line 1610
    :goto_8
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->buMap:Ljava/util/HashMap;

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679
    :cond_19
    :goto_9
    invoke-virtual/range {v54 .. v54}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getRunList()Ljava/util/ArrayList;

    move-result-object v62

    .line 1682
    .local v62, "runList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;>;"
    invoke-interface/range {v62 .. v62}, Ljava/util/List;->size()I

    move-result v7

    if-nez v7, :cond_1b

    .line 1683
    new-instance v34, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-direct/range {v34 .. v34}, Lcom/samsung/thumbnail/customview/word/Run;-><init>()V

    .line 1684
    .local v34, "drawRun":Lcom/samsung/thumbnail/customview/word/Run;
    const-string/jumbo v7, ""

    move-object/from16 v0, v34

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    .line 1685
    invoke-virtual/range {v54 .. v54}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v27

    .line 1687
    .local v27, "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    if-eqz v31, :cond_1a

    .line 1688
    if-eqz v27, :cond_4f

    .line 1689
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getCharProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v7

    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->mergeCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V

    .line 1694
    :cond_1a
    :goto_a
    if-eqz v27, :cond_50

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_50

    .line 1695
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v7

    int-to-float v7, v7

    move-object/from16 v0, v34

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 1699
    :goto_b
    move-object/from16 v0, v32

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 1703
    .end local v27    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .end local v34    # "drawRun":Lcom/samsung/thumbnail/customview/word/Run;
    :cond_1b
    const/16 v43, 0x0

    .local v43, "j":I
    :goto_c
    invoke-interface/range {v62 .. v62}, Ljava/util/List;->size()I

    move-result v7

    move/from16 v0, v43

    if-ge v0, v7, :cond_63

    .line 1704
    move-object/from16 v0, v62

    move/from16 v1, v43

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v61

    check-cast v61, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    .line 1705
    .local v61, "run":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    invoke-virtual/range {v61 .. v61}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v27

    .line 1706
    .restart local v27    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    new-instance v64, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-direct/range {v64 .. v64}, Lcom/samsung/thumbnail/customview/word/Run;-><init>()V

    .line 1708
    .local v64, "textRun":Lcom/samsung/thumbnail/customview/word/Run;
    const-string/jumbo v25, ""

    .line 1711
    .local v25, "bullet":Ljava/lang/String;
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isDiagramShape()Z

    move-result v7

    if-eqz v7, :cond_1f

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v7

    if-nez v7, :cond_1f

    .line 1712
    const/16 v38, 0x0

    .line 1714
    .local v38, "fillColor":Ljava/lang/String;
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v7

    if-eqz v7, :cond_1c

    .line 1715
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v38

    .line 1718
    :cond_1c
    if-nez v38, :cond_1d

    .line 1719
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getGradFillColor()Ljava/lang/String;

    move-result-object v38

    .line 1722
    :cond_1d
    if-eqz v38, :cond_1e

    .line 1723
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    move-object/from16 v0, v38

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    .line 1727
    .local v37, "fillClr":Ljava/lang/String;
    :try_start_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v8, 0x23

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v37

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1732
    :goto_d
    move-object/from16 v38, v37

    .line 1739
    .end local v37    # "fillClr":Ljava/lang/String;
    :cond_1e
    new-instance v28, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct/range {v28 .. v28}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 1740
    .local v28, "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    const-string/jumbo v7, "FFFFFF"

    move-object/from16 v0, v28

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1742
    if-eqz v38, :cond_1f

    :try_start_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v8, 0x23

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v38

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_1f

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillAlpha()J

    move-result-wide v8

    const-wide/16 v12, 0x0

    cmp-long v7, v8, v12

    if-eqz v7, :cond_1f

    .line 1746
    const/4 v7, -0x1

    move-object/from16 v0, v64

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 1747
    invoke-virtual/range {v27 .. v28}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 1755
    .end local v28    # "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .end local v38    # "fillColor":Ljava/lang/String;
    :cond_1f
    :goto_e
    if-eqz v31, :cond_20

    .line 1756
    if-eqz v27, :cond_51

    .line 1757
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getCharProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v7

    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->mergeCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V

    .line 1763
    :cond_20
    :goto_f
    if-eqz p2, :cond_21

    .line 1764
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getBodyProp()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    move-result-object v7

    if-eqz v7, :cond_21

    .line 1765
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getBodyProp()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    move-result-object v7

    iget v7, v7, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;->fontScale:I

    if-lez v7, :cond_21

    .line 1766
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getBodyProp()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    move-result-object v8

    iget v8, v8, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;->fontScale:I

    int-to-float v8, v8

    invoke-static {v7, v8}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->getFitFontScale(FF)I

    move-result v7

    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setFontSize(I)V

    .line 1773
    :cond_21
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v7

    if-lez v7, :cond_22

    .line 1775
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v7

    mul-int/lit8 v7, v7, 0x55

    int-to-float v7, v7

    const/high16 v8, 0x42c80000    # 100.0f

    div-float/2addr v7, v8

    move-object/from16 v0, v64

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 1776
    :cond_22
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getBold()Z

    move-result v7

    if-eqz v7, :cond_23

    .line 1777
    invoke-virtual/range {v64 .. v64}, Lcom/samsung/thumbnail/customview/word/Run;->setTextBold()V

    .line 1778
    :cond_23
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getItalic()Z

    move-result v7

    if-eqz v7, :cond_24

    .line 1779
    invoke-virtual/range {v64 .. v64}, Lcom/samsung/thumbnail/customview/word/Run;->setTextItalics()V

    .line 1780
    :cond_24
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getUnderline()Z

    move-result v7

    if-eqz v7, :cond_25

    .line 1781
    const/4 v7, 0x1

    move-object/from16 v0, v64

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/Run;->setUnderlineText(Z)V

    .line 1782
    :cond_25
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getStrike()Z

    move-result v7

    if-nez v7, :cond_26

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getDStrike()Z

    move-result v7

    if-eqz v7, :cond_27

    .line 1783
    :cond_26
    const/4 v7, 0x1

    move-object/from16 v0, v64

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/Run;->setStrikeThruText(Z)V

    .line 1784
    :cond_27
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getSmallCaps()Z

    move-result v7

    if-eqz v7, :cond_28

    .line 1785
    invoke-virtual/range {v64 .. v64}, Lcom/samsung/thumbnail/customview/word/Run;->setSMALLLetterON()V

    .line 1786
    :cond_28
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getCaps()Z

    move-result v7

    if-eqz v7, :cond_29

    .line 1787
    invoke-virtual/range {v64 .. v64}, Lcom/samsung/thumbnail/customview/word/Run;->setCAPLetterON()V

    .line 1788
    :cond_29
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getBaseline()I

    move-result v7

    if-gez v7, :cond_2a

    .line 1789
    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUBSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-object/from16 v0, v64

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/Run;->setVertivalAlignment(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;)V

    .line 1791
    :cond_2a
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getBaseline()I

    move-result v7

    if-lez v7, :cond_2b

    .line 1792
    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUPERSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-object/from16 v0, v64

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/Run;->setVertivalAlignment(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;)V

    .line 1794
    :cond_2b
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getHLink()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2c

    .line 1795
    const/4 v7, 0x1

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->isHlink:Z

    .line 1797
    :cond_2c
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isDiagramShape()Z

    move-result v7

    if-eqz v7, :cond_2f

    .line 1798
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getAlignment()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-result-object v7

    if-eqz v7, :cond_2f

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getAlignment()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-result-object v7

    sget-object v8, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->LEFT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    if-ne v7, v8, :cond_2f

    .line 1800
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getRotation()I

    move-result v7

    if-nez v7, :cond_2d

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getTextRotation()I

    move-result v7

    if-eqz v7, :cond_2e

    :cond_2d
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getRotation()I

    move-result v7

    if-eqz v7, :cond_2f

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getTextRotation()I

    move-result v7

    if-eqz v7, :cond_2f

    .line 1804
    :cond_2e
    if-eqz v56, :cond_2f

    .line 1805
    const/4 v7, 0x1

    move-object/from16 v0, v64

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/Run;->setTabStatus(Z)V

    .line 1806
    invoke-virtual/range {v56 .. v56}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getDefTabSize()I

    move-result v7

    div-int/lit8 v7, v7, 0x4

    invoke-virtual/range {v56 .. v56}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpaceAfter()F

    move-result v8

    float-to-int v8, v8

    div-int/lit8 v8, v8, 0x4

    add-int/2addr v7, v8

    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setDefTabSpacing(I)V

    .line 1814
    :cond_2f
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v29

    .line 1816
    .local v29, "colorTemp":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    if-nez v29, :cond_30

    .line 1817
    new-instance v29, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .end local v29    # "colorTemp":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    invoke-direct/range {v29 .. v29}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 1820
    .restart local v29    # "colorTemp":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    :cond_30
    if-eqz v29, :cond_59

    .line 1821
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_52

    .line 1823
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1832
    :cond_31
    :goto_10
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getLumMod()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_55

    .line 1833
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getLumMod()Ljava/lang/String;

    move-result-object v49

    .line 1834
    .local v49, "lumMod":Ljava/lang/String;
    invoke-static/range {v49 .. v49}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v46

    .line 1835
    .local v46, "lMod":D
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v46 .. v47}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcLuminousColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v39

    .line 1841
    .local v39, "fontColor":Ljava/lang/String;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x6

    if-ge v7, v8, :cond_54

    .line 1842
    invoke-virtual/range {v39 .. v39}, Ljava/lang/String;->length()I

    move-result v7

    rsub-int/lit8 v45, v7, 0x6

    .line 1843
    .local v45, "length":I
    new-instance v53, Ljava/lang/StringBuffer;

    const-string/jumbo v7, ""

    move-object/from16 v0, v53

    invoke-direct {v0, v7}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1844
    .local v53, "padding":Ljava/lang/StringBuffer;
    const/16 v44, 0x0

    .local v44, "k":I
    :goto_11
    move/from16 v0, v44

    move/from16 v1, v45

    if-ge v0, v1, :cond_53

    .line 1845
    const-string/jumbo v7, "0"

    move-object/from16 v0, v53

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1844
    add-int/lit8 v44, v44, 0x1

    goto :goto_11

    .line 1409
    .end local v16    # "align":Ljava/lang/String;
    .end local v19    # "bodyProp":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;
    .end local v20    # "buChar":Ljava/lang/String;
    .end local v21    # "buCount":I
    .end local v22    # "buLevel":I
    .end local v23    # "buNum":Ljava/lang/String;
    .end local v24    # "buText":Ljava/lang/String;
    .end local v25    # "bullet":Ljava/lang/String;
    .end local v27    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .end local v29    # "colorTemp":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .end local v30    # "curBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .end local v32    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v36    # "eBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .end local v39    # "fontColor":Ljava/lang/String;
    .end local v41    # "i":I
    .end local v42    # "indent":Ljava/lang/String;
    .end local v43    # "j":I
    .end local v44    # "k":I
    .end local v45    # "length":I
    .end local v46    # "lMod":D
    .end local v48    # "level":I
    .end local v49    # "lumMod":Ljava/lang/String;
    .end local v50    # "marginLeft":Ljava/lang/String;
    .end local v53    # "padding":Ljava/lang/StringBuffer;
    .end local v54    # "para":Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;
    .end local v55    # "paraLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;>;"
    .end local v59    # "preLevel":I
    .end local v60    # "prevBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .end local v61    # "run":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    .end local v62    # "runList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;>;"
    .end local v64    # "textRun":Lcom/samsung/thumbnail/customview/word/Run;
    .restart local v17    # "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    :cond_32
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getShapeForAnchor(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;)Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-result-object v58

    .line 1411
    if-eqz v58, :cond_33

    invoke-virtual/range {v58 .. v58}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getAnchor()Lorg/apache/poi/java/awt/geom/Rectangle2D;

    move-result-object v17

    :goto_12
    goto/16 :goto_0

    :cond_33
    const/16 v17, 0x0

    goto :goto_12

    .line 1431
    .end local v17    # "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    :cond_34
    if-eqz p3, :cond_4

    .line 1432
    move-object/from16 v10, p3

    goto/16 :goto_1

    .line 1446
    .restart local v21    # "buCount":I
    .restart local v22    # "buLevel":I
    .restart local v55    # "paraLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;>;"
    .restart local v59    # "preLevel":I
    .restart local v60    # "prevBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    :cond_35
    if-eqz p4, :cond_5

    .line 1447
    move-object/from16 v55, p4

    goto/16 :goto_2

    .line 1490
    .restart local v16    # "align":Ljava/lang/String;
    .restart local v20    # "buChar":Ljava/lang/String;
    .restart local v23    # "buNum":Ljava/lang/String;
    .restart local v30    # "curBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .restart local v32    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .restart local v36    # "eBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .restart local v41    # "i":I
    .restart local v42    # "indent":Ljava/lang/String;
    .restart local v48    # "level":I
    .restart local v50    # "marginLeft":Ljava/lang/String;
    .restart local v54    # "para":Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;
    :catch_0
    move-exception v35

    .line 1491
    .local v35, "e":Ljava/lang/Exception;
    const-string/jumbo v7, "XMLSlideShow"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Exception : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {v35 .. v35}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 1523
    .end local v35    # "e":Ljava/lang/Exception;
    :cond_36
    const-string/jumbo v7, "right"

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_37

    .line 1524
    sget-object v7, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->RIGHT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_5

    .line 1525
    :cond_37
    const-string/jumbo v7, "justify"

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_38

    .line 1526
    sget-object v7, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->JUSTIFY:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_5

    .line 1529
    :cond_38
    sget-object v7, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->LEFT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_5

    .line 1566
    .restart local v19    # "bodyProp":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;
    :cond_39
    const-string/jumbo v7, "b"

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;->getvAlign()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3a

    .line 1567
    sget-object v7, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->BOTTOM:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    goto/16 :goto_6

    .line 1569
    :cond_3a
    sget-object v7, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->TOP:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    goto/16 :goto_6

    .line 1588
    .restart local v24    # "buText":Ljava/lang/String;
    :cond_3b
    move/from16 v0, v22

    move/from16 v1, v48

    if-ne v0, v1, :cond_17

    .line 1589
    const/16 v21, -0x1

    goto/16 :goto_7

    .line 1599
    :cond_3c
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x1

    if-le v7, v8, :cond_3e

    .line 1600
    const/4 v7, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x7f

    if-gt v7, v8, :cond_3d

    const/4 v7, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x6f

    if-eq v7, v8, :cond_3d

    move-object/from16 v24, v20

    :goto_13
    goto/16 :goto_8

    :cond_3d
    const/4 v7, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/samsung/thumbnail/util/Utils;->getUnicodeForBullet(IZ)Ljava/lang/String;

    move-result-object v24

    goto :goto_13

    .line 1608
    :cond_3e
    const-string/jumbo v24, "\u25cf"

    goto/16 :goto_8

    .line 1612
    :cond_3f
    invoke-virtual/range {v56 .. v56}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBuNone()Z

    move-result v7

    if-nez v7, :cond_19

    .line 1613
    invoke-virtual/range {v56 .. v56}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBullet()Z

    move-result v7

    if-eqz v7, :cond_19

    .line 1614
    if-eqz v60, :cond_40

    move-object/from16 v0, v60

    move-object/from16 v1, v30

    if-eq v0, v1, :cond_41

    .line 1616
    :cond_40
    const/16 v21, -0x1

    .line 1617
    move-object/from16 v60, v30

    .line 1619
    :cond_41
    move/from16 v0, v22

    move/from16 v1, v48

    if-eq v0, v1, :cond_42

    .line 1620
    move/from16 v0, v22

    move/from16 v1, v48

    if-le v0, v1, :cond_45

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->buMap:Ljava/util/HashMap;

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_45

    .line 1621
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->buMap:Ljava/util/HashMap;

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v21

    .line 1626
    :cond_42
    :goto_14
    invoke-virtual/range {v54 .. v54}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getRunList()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-eqz v7, :cond_43

    .line 1627
    add-int/lit8 v21, v21, 0x1

    .line 1629
    :cond_43
    if-eqz v36, :cond_4e

    .line 1630
    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$dml$EBulletAutoNumType:[I

    invoke-virtual/range {v36 .. v36}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 1674
    :cond_44
    :goto_15
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->buMap:Ljava/util/HashMap;

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_9

    .line 1622
    :cond_45
    move/from16 v0, v22

    move/from16 v1, v48

    if-ne v0, v1, :cond_42

    .line 1623
    const/16 v21, -0x1

    goto :goto_14

    .line 1632
    :pswitch_0
    rem-int/lit8 v7, v21, 0x1a

    add-int/lit8 v7, v7, 0x61

    int-to-char v14, v7

    .line 1633
    .local v14, "a":C
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v14}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 1634
    goto :goto_15

    .line 1636
    .end local v14    # "a":C
    :pswitch_1
    rem-int/lit8 v7, v21, 0x1a

    add-int/lit8 v7, v7, 0x41

    int-to-char v0, v7

    move/from16 v18, v0

    .line 1637
    .local v18, "b":C
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v18 .. v18}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 1638
    goto :goto_15

    .line 1640
    .end local v18    # "b":C
    :pswitch_2
    rem-int/lit8 v7, v21, 0x1a

    add-int/lit8 v7, v7, 0x61

    int-to-char v0, v7

    move/from16 v26, v0

    .line 1641
    .local v26, "c":C
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v26 .. v26}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 1643
    goto :goto_15

    .line 1645
    .end local v26    # "c":C
    :pswitch_3
    const/4 v7, -0x1

    move/from16 v0, v21

    if-eq v0, v7, :cond_46

    if-nez v21, :cond_47

    .line 1646
    :cond_46
    const/16 v21, 0x1

    .line 1648
    :cond_47
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v21 .. v21}, Lcom/samsung/thumbnail/util/Utils;->binaryToRomanUC(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 1649
    goto/16 :goto_15

    .line 1651
    :pswitch_4
    const/4 v7, -0x1

    move/from16 v0, v21

    if-eq v0, v7, :cond_48

    if-nez v21, :cond_49

    .line 1652
    :cond_48
    const/16 v21, 0x1

    .line 1654
    :cond_49
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v21 .. v21}, Lcom/samsung/thumbnail/util/Utils;->binaryToRomanLC(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 1655
    goto/16 :goto_15

    .line 1657
    :pswitch_5
    const/4 v7, -0x1

    move/from16 v0, v21

    if-eq v0, v7, :cond_4a

    if-nez v21, :cond_4b

    .line 1658
    :cond_4a
    const/16 v21, 0x1

    .line 1660
    :cond_4b
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 1661
    goto/16 :goto_15

    .line 1663
    :pswitch_6
    const/4 v7, -0x1

    move/from16 v0, v21

    if-eq v0, v7, :cond_4c

    if-nez v21, :cond_4d

    .line 1664
    :cond_4c
    const/16 v21, 0x1

    .line 1666
    :cond_4d
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 1667
    goto/16 :goto_15

    .line 1671
    :cond_4e
    invoke-virtual/range {v56 .. v56}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getBulletChar()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_44

    .line 1672
    invoke-virtual/range {v56 .. v56}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getBulletChar()Ljava/lang/String;

    move-result-object v20

    goto/16 :goto_15

    .line 1691
    .restart local v27    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .restart local v34    # "drawRun":Lcom/samsung/thumbnail/customview/word/Run;
    .restart local v62    # "runList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;>;"
    :cond_4f
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getCharProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v27

    goto/16 :goto_a

    .line 1697
    :cond_50
    const/high16 v7, 0x41400000    # 12.0f

    move-object/from16 v0, v34

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    goto/16 :goto_b

    .line 1728
    .end local v34    # "drawRun":Lcom/samsung/thumbnail/customview/word/Run;
    .restart local v25    # "bullet":Ljava/lang/String;
    .restart local v37    # "fillClr":Ljava/lang/String;
    .restart local v38    # "fillColor":Ljava/lang/String;
    .restart local v43    # "j":I
    .restart local v61    # "run":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    .restart local v64    # "textRun":Lcom/samsung/thumbnail/customview/word/Run;
    :catch_1
    move-exception v35

    .line 1729
    .restart local v35    # "e":Ljava/lang/Exception;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    move-object/from16 v0, v38

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    goto/16 :goto_d

    .line 1749
    .end local v35    # "e":Ljava/lang/Exception;
    .end local v37    # "fillClr":Ljava/lang/String;
    .restart local v28    # "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    :catch_2
    move-exception v35

    .line 1750
    .restart local v35    # "e":Ljava/lang/Exception;
    const/4 v7, -0x1

    move-object/from16 v0, v64

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 1751
    invoke-virtual/range {v27 .. v28}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    goto/16 :goto_e

    .line 1759
    .end local v28    # "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .end local v35    # "e":Ljava/lang/Exception;
    .end local v38    # "fillColor":Ljava/lang/String;
    :cond_51
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getCharProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v27

    goto/16 :goto_f

    .line 1825
    .restart local v29    # "colorTemp":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    :cond_52
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v7

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_31

    .line 1828
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v7

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    goto/16 :goto_10

    .line 1847
    .restart local v39    # "fontColor":Ljava/lang/String;
    .restart local v44    # "k":I
    .restart local v45    # "length":I
    .restart local v46    # "lMod":D
    .restart local v49    # "lumMod":Ljava/lang/String;
    .restart local v53    # "padding":Ljava/lang/StringBuffer;
    :cond_53
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v53 .. v53}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v39

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    .line 1849
    .end local v44    # "k":I
    .end local v45    # "length":I
    .end local v53    # "padding":Ljava/lang/StringBuffer;
    :cond_54
    move-object/from16 v0, v29

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1851
    .end local v39    # "fontColor":Ljava/lang/String;
    .end local v46    # "lMod":D
    .end local v49    # "lumMod":Ljava/lang/String;
    :cond_55
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v15

    .line 1852
    .local v15, "actualColor":Ljava/lang/String;
    if-eqz v15, :cond_57

    .line 1853
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v65

    .line 1854
    .local v65, "tint":Ljava/lang/String;
    if-eqz v65, :cond_56

    const-string/jumbo v7, "0"

    move-object/from16 v0, v65

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_56

    .line 1855
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v7

    invoke-static {v15, v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcColorTint(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1858
    :cond_56
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v8, 0x23

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, v64

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 1862
    .end local v65    # "tint":Ljava/lang/String;
    :cond_57
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->isHlink:Z

    if-eqz v7, :cond_58

    .line 1863
    const/4 v7, 0x1

    move-object/from16 v0, v64

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/Run;->setUnderlineText(Z)V

    .line 1864
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v7

    const-string/jumbo v8, "hlink"

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    .line 1867
    .local v40, "hLinkColor":Ljava/lang/String;
    if-eqz v40, :cond_5e

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v8, 0x23

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v40

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v7

    :goto_16
    move-object/from16 v0, v64

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 1871
    .end local v40    # "hLinkColor":Ljava/lang/String;
    :cond_58
    const/4 v7, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setSchemeClr(Z)V

    .line 1874
    .end local v15    # "actualColor":Ljava/lang/String;
    :cond_59
    if-nez v43, :cond_5a

    .line 1875
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isDiagramShape()Z

    move-result v7

    if-eqz v7, :cond_5f

    .line 1876
    const-string/jumbo v7, ""

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5a

    .line 1877
    const/4 v7, 0x0

    move-object/from16 v0, v64

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/Run;->setUnderlineText(Z)V

    .line 1878
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x20

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    .line 1892
    :cond_5a
    :goto_17
    invoke-virtual/range {v61 .. v61}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getText()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "<br>"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_61

    .line 1893
    move-object/from16 v0, v32

    invoke-virtual {v6, v0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 1895
    move-object/from16 v33, v32

    .line 1896
    .local v33, "docPara_temp":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    new-instance v32, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .end local v32    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-direct/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    .line 1897
    .restart local v32    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v7

    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    .line 1898
    if-eqz v50, :cond_5b

    .line 1899
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v7

    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    .line 1900
    :cond_5b
    if-eqz v16, :cond_5c

    .line 1901
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getAlignment()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-result-object v7

    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    .line 1902
    :cond_5c
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLineSpacing()F

    move-result v7

    const/4 v8, 0x0

    cmpl-float v7, v7, v8

    if-eqz v7, :cond_5d

    .line 1903
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLineSpacing()F

    move-result v7

    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineSpacing(F)V

    .line 1703
    .end local v33    # "docPara_temp":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    :cond_5d
    :goto_18
    add-int/lit8 v43, v43, 0x1

    goto/16 :goto_c

    .line 1867
    .restart local v15    # "actualColor":Ljava/lang/String;
    .restart local v40    # "hLinkColor":Ljava/lang/String;
    :cond_5e
    const-string/jumbo v7, "#0000FF"

    invoke-static {v7}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_16

    .line 1881
    .end local v15    # "actualColor":Ljava/lang/String;
    .end local v40    # "hLinkColor":Ljava/lang/String;
    :cond_5f
    if-eqz v23, :cond_60

    .line 1882
    const/4 v7, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setUndreline(Z)V

    .line 1883
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    goto :goto_17

    .line 1884
    :cond_60
    if-eqz v20, :cond_5a

    .line 1885
    const-string/jumbo v7, "Wingdings"

    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setFontName(Ljava/lang/String;)V

    .line 1886
    const/4 v7, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setUndreline(Z)V

    .line 1887
    const-string/jumbo v25, "\u2022 "

    goto/16 :goto_17

    .line 1907
    :cond_61
    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_62

    .line 1908
    invoke-virtual/range {v61 .. v61}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getText()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v64

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    .line 1913
    :goto_19
    move-object/from16 v0, v32

    move-object/from16 v1, v64

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 1914
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->isHlink:Z

    goto :goto_18

    .line 1910
    :cond_62
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v61 .. v61}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getText()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v64

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    goto :goto_19

    .line 1917
    .end local v25    # "bullet":Ljava/lang/String;
    .end local v27    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .end local v29    # "colorTemp":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .end local v61    # "run":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    .end local v64    # "textRun":Lcom/samsung/thumbnail/customview/word/Run;
    :cond_63
    move-object/from16 v0, v32

    invoke-virtual {v6, v0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 1450
    add-int/lit8 v41, v41, 0x1

    goto/16 :goto_3

    .line 1920
    .end local v16    # "align":Ljava/lang/String;
    .end local v19    # "bodyProp":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;
    .end local v20    # "buChar":Ljava/lang/String;
    .end local v23    # "buNum":Ljava/lang/String;
    .end local v24    # "buText":Ljava/lang/String;
    .end local v30    # "curBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .end local v32    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v36    # "eBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .end local v42    # "indent":Ljava/lang/String;
    .end local v43    # "j":I
    .end local v48    # "level":I
    .end local v50    # "marginLeft":Ljava/lang/String;
    .end local v54    # "para":Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;
    .end local v62    # "runList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;>;"
    :cond_64
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isDiagramShape()Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/customview/word/ShapesController;->setDiagramShape(Z)V

    .line 1921
    sget-object v7, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPTX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/customview/word/ShapesController;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 1922
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v7, v6}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 1923
    return-void

    .line 1630
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public drawShape(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Ljava/util/List;)V
    .locals 47
    .param p1, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .param p2, "shape"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    .param p3, "shInfoProperties"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1066
    .local p4, "paragraphLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;>;"
    const/16 v23, 0x0

    .line 1067
    .local v23, "defProp":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;
    const/16 v41, 0x0

    .line 1069
    .local v41, "paraProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    const/4 v6, 0x0

    .line 1070
    .local v6, "shapeInfo":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    const/4 v2, 0x0

    .line 1072
    .local v2, "spCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    if-eqz p2, :cond_1

    .line 1073
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getAnchor()Lorg/apache/poi/java/awt/geom/Rectangle2D;

    move-result-object v12

    .line 1074
    .local v12, "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v6

    .line 1076
    if-eqz v12, :cond_0

    .line 1077
    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->setAnchor(Lorg/apache/poi/java/awt/geom/Rectangle2D;)V

    .line 1078
    invoke-virtual {v12}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->toEMU(D)I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v12}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->toEMU(D)I

    move-result v3

    int-to-long v8, v3

    invoke-virtual {v6, v4, v5, v8, v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setSize(JJ)V

    .line 1080
    invoke-virtual {v12}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->toEMU(D)I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v12}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->toEMU(D)I

    move-result v3

    int-to-long v8, v3

    invoke-virtual {v6, v4, v5, v8, v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setPoints(JJ)V

    .line 1087
    .end local v12    # "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    :cond_0
    :goto_0
    if-nez v6, :cond_2

    .line 1371
    :goto_1
    return-void

    .line 1084
    :cond_1
    move-object/from16 v6, p3

    goto :goto_0

    .line 1091
    :cond_2
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->buMap:Ljava/util/HashMap;

    .line 1092
    const/4 v15, -0x1

    .line 1094
    .local v15, "buCount":I
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 1095
    new-instance v37, Landroid/graphics/Paint;

    invoke-direct/range {v37 .. v37}, Landroid/graphics/Paint;-><init>()V

    .line 1096
    .local v37, "paintFill":Landroid/graphics/Paint;
    const/4 v3, 0x1

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1097
    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1099
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v3, v4, v5, v7}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 1102
    .end local v37    # "paintFill":Landroid/graphics/Paint;
    :cond_3
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "spCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->getCanvas()Landroid/graphics/Canvas;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->context:Landroid/content/Context;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->shape_Count:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->folderPath:Ljava/io/File;

    move-object/from16 v5, p2

    move-object/from16 v7, p1

    invoke-direct/range {v2 .. v9}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;ILjava/io/File;)V

    .line 1105
    .restart local v2    # "spCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    const/16 v42, -0x1

    .local v42, "preLevel":I
    const/16 v16, -0x1

    .line 1106
    .local v16, "buLevel":I
    const/16 v43, 0x0

    .line 1107
    .local v43, "prevBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    const/16 v39, 0x0

    .line 1108
    .local v39, "paraLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;>;"
    if-eqz p2, :cond_2a

    .line 1109
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getParagraphLst()Ljava/util/List;

    move-result-object v39

    .line 1113
    :goto_2
    const/16 v40, 0x0

    .line 1114
    .local v40, "paraLstSize":I
    if-eqz v39, :cond_4

    .line 1115
    invoke-interface/range {v39 .. v39}, Ljava/util/List;->size()I

    move-result v40

    .line 1118
    :cond_4
    const/16 v28, 0x0

    .local v28, "i":I
    :goto_3
    move/from16 v0, v28

    move/from16 v1, v40

    if-ge v0, v1, :cond_3e

    .line 1119
    new-instance v24, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-direct/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    .line 1121
    .local v24, "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    move-object/from16 v0, v39

    move/from16 v1, v28

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v38

    check-cast v38, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    .line 1122
    .local v38, "para":Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;
    const/16 v31, 0x0

    .line 1123
    .local v31, "level":I
    const/4 v11, 0x0

    .line 1124
    .local v11, "align":Ljava/lang/String;
    const/16 v29, 0x0

    .line 1125
    .local v29, "indent":Ljava/lang/String;
    const/16 v35, 0x0

    .line 1126
    .local v35, "marginLeft":Ljava/lang/String;
    const/16 v26, 0x0

    .line 1127
    .local v26, "eBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    const/16 v22, 0x0

    .line 1128
    .local v22, "curBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    const/16 v17, 0x0

    .line 1129
    .local v17, "buNum":Ljava/lang/String;
    const/4 v14, 0x0

    .line 1130
    .local v14, "buChar":Ljava/lang/String;
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v41

    .line 1132
    if-eqz v41, :cond_5

    .line 1133
    invoke-virtual/range {v41 .. v41}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getLevel()I

    move-result v31

    .line 1134
    invoke-virtual/range {v41 .. v41}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getJustify()Ljava/lang/String;

    move-result-object v11

    .line 1135
    invoke-virtual/range {v41 .. v41}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentPPT()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v29

    .line 1136
    invoke-virtual/range {v41 .. v41}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getBuAutoNum()Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    move-result-object v26

    .line 1137
    move-object/from16 v22, v26

    .line 1138
    invoke-virtual/range {v41 .. v41}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getMarginLeft()I

    move-result v3

    if-eqz v3, :cond_5

    .line 1139
    invoke-virtual/range {v41 .. v41}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getMarginLeft()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v35

    .line 1142
    :cond_5
    move/from16 v0, v42

    move/from16 v1, v31

    if-eq v0, v1, :cond_7

    .line 1143
    move/from16 v0, v42

    move/from16 v1, v31

    if-ge v0, v1, :cond_6

    .line 1144
    const/4 v15, -0x1

    .line 1146
    :cond_6
    move/from16 v16, v42

    .line 1149
    :cond_7
    if-eqz v35, :cond_8

    invoke-static/range {v35 .. v35}, Lcom/samsung/thumbnail/util/Utils;->isValidInteger(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1150
    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v36

    .line 1151
    .local v36, "marginSpace":I
    mul-int/lit8 v3, v31, 0x32

    add-int v36, v36, v3

    .line 1152
    invoke-static/range {v36 .. v36}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v35

    .line 1153
    invoke-static/range {v35 .. v35}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    .line 1156
    .end local v36    # "marginSpace":I
    :cond_8
    move/from16 v42, v31

    .line 1158
    if-eqz v23, :cond_d

    .line 1159
    if-nez v11, :cond_9

    .line 1160
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getJustify()Ljava/lang/String;

    move-result-object v11

    .line 1163
    :cond_9
    if-nez v29, :cond_a

    .line 1164
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentPPT()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v29

    .line 1168
    :cond_a
    if-nez v41, :cond_c

    .line 1169
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getMarginLeft()I

    move-result v3

    if-eqz v3, :cond_b

    .line 1170
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getMarginLeft()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v35

    .line 1173
    :cond_b
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v41

    .line 1175
    :cond_c
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v3

    move-object/from16 v0, v41

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->mergeParaProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V

    .line 1178
    :cond_d
    if-eqz v11, :cond_e

    .line 1179
    const-string/jumbo v3, "center"

    invoke-virtual {v11, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2b

    .line 1180
    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->CENTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    .line 1189
    :cond_e
    :goto_4
    if-eqz v41, :cond_f

    .line 1190
    invoke-virtual/range {v41 .. v41}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getLineSpacing()F

    move-result v3

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_f

    .line 1191
    invoke-virtual/range {v41 .. v41}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getLineSpacing()F

    move-result v3

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineSpacing(F)V

    .line 1195
    :cond_f
    if-eqz v41, :cond_15

    .line 1196
    invoke-virtual/range {v41 .. v41}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBuNone()Z

    move-result v3

    if-nez v3, :cond_15

    .line 1197
    invoke-virtual/range {v41 .. v41}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBullet()Z

    move-result v3

    if-eqz v3, :cond_15

    .line 1198
    if-eqz v43, :cond_10

    move-object/from16 v0, v43

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_11

    .line 1200
    :cond_10
    const/4 v15, -0x1

    .line 1201
    move-object/from16 v43, v22

    .line 1203
    :cond_11
    move/from16 v0, v16

    move/from16 v1, v31

    if-eq v0, v1, :cond_12

    .line 1204
    move/from16 v0, v16

    move/from16 v1, v31

    if-le v0, v1, :cond_2e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->buMap:Ljava/util/HashMap;

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_2e

    .line 1205
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->buMap:Ljava/util/HashMap;

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v15

    .line 1210
    :cond_12
    :goto_5
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getRunList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_13

    .line 1211
    add-int/lit8 v15, v15, 0x1

    .line 1213
    :cond_13
    if-eqz v26, :cond_37

    .line 1214
    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$dml$EBulletAutoNumType:[I

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1258
    :cond_14
    :goto_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->buMap:Ljava/util/HashMap;

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1263
    :cond_15
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getRunList()Ljava/util/ArrayList;

    move-result-object v45

    .line 1264
    .local v45, "runList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;>;"
    const/16 v30, 0x0

    .local v30, "j":I
    :goto_7
    invoke-interface/range {v45 .. v45}, Ljava/util/List;->size()I

    move-result v3

    move/from16 v0, v30

    if-ge v0, v3, :cond_3d

    .line 1265
    move-object/from16 v0, v45

    move/from16 v1, v30

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v44

    check-cast v44, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    .line 1266
    .local v44, "run":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    invoke-virtual/range {v44 .. v44}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v20

    .line 1267
    .local v20, "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    new-instance v46, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-direct/range {v46 .. v46}, Lcom/samsung/thumbnail/customview/word/Run;-><init>()V

    .line 1269
    .local v46, "textRun":Lcom/samsung/thumbnail/customview/word/Run;
    const-string/jumbo v18, ""

    .line 1271
    .local v18, "bullet":Ljava/lang/String;
    if-eqz v23, :cond_16

    .line 1272
    if-eqz v20, :cond_38

    .line 1273
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getCharProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->mergeCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V

    .line 1279
    :cond_16
    :goto_8
    if-eqz p2, :cond_17

    .line 1280
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getBodyProp()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    move-result-object v3

    if-eqz v3, :cond_17

    .line 1281
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getBodyProp()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    move-result-object v3

    iget v3, v3, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;->fontScale:I

    if-lez v3, :cond_17

    .line 1282
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getBodyProp()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    move-result-object v4

    iget v4, v4, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;->fontScale:I

    int-to-float v4, v4

    invoke-static {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->getFitFontScale(FF)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setFontSize(I)V

    .line 1288
    :cond_17
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v3

    if-lez v3, :cond_18

    .line 1289
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v3

    int-to-float v3, v3

    move-object/from16 v0, v46

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 1290
    :cond_18
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getBold()Z

    move-result v3

    if-eqz v3, :cond_19

    .line 1291
    invoke-virtual/range {v46 .. v46}, Lcom/samsung/thumbnail/customview/word/Run;->setTextBold()V

    .line 1292
    :cond_19
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getItalic()Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 1293
    invoke-virtual/range {v46 .. v46}, Lcom/samsung/thumbnail/customview/word/Run;->setTextItalics()V

    .line 1294
    :cond_1a
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getUnderline()Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 1295
    const/4 v3, 0x1

    move-object/from16 v0, v46

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/word/Run;->setUnderlineText(Z)V

    .line 1296
    :cond_1b
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getStrike()Z

    move-result v3

    if-nez v3, :cond_1c

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getDStrike()Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 1297
    :cond_1c
    const/4 v3, 0x1

    move-object/from16 v0, v46

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/word/Run;->setStrikeThruText(Z)V

    .line 1298
    :cond_1d
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getSmallCaps()Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 1299
    invoke-virtual/range {v46 .. v46}, Lcom/samsung/thumbnail/customview/word/Run;->setSMALLLetterON()V

    .line 1300
    :cond_1e
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getCaps()Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 1301
    invoke-virtual/range {v46 .. v46}, Lcom/samsung/thumbnail/customview/word/Run;->setCAPLetterON()V

    .line 1302
    :cond_1f
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getHLink()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_20

    .line 1303
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->isHlink:Z

    .line 1304
    :cond_20
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v21

    .line 1305
    .local v21, "color_temp":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    if-nez v21, :cond_21

    .line 1306
    new-instance v21, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .end local v21    # "color_temp":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    invoke-direct/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 1308
    .restart local v21    # "color_temp":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    :cond_21
    if-eqz v21, :cond_25

    .line 1309
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v3

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_22

    .line 1311
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v3

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1314
    :cond_22
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getLumMod()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_39

    .line 1315
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getLumMod()Ljava/lang/String;

    move-result-object v34

    .line 1316
    .local v34, "lumMod":Ljava/lang/String;
    invoke-static/range {v34 .. v34}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v32

    .line 1317
    .local v32, "lMod":D
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v3

    invoke-static/range {v32 .. v33}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcLuminousColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v27

    .line 1319
    .local v27, "fontColor":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "#"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v46

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 1326
    .end local v27    # "fontColor":Ljava/lang/String;
    .end local v32    # "lMod":D
    .end local v34    # "lumMod":Ljava/lang/String;
    :cond_23
    :goto_9
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->isHlink:Z

    if-eqz v3, :cond_24

    .line 1327
    const-string/jumbo v3, "#0000FF"

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v46

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 1330
    :cond_24
    const/4 v3, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setSchemeClr(Z)V

    .line 1333
    :cond_25
    if-nez v30, :cond_26

    .line 1334
    if-eqz v17, :cond_3a

    .line 1335
    const/4 v3, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setUndreline(Z)V

    .line 1336
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 1344
    :cond_26
    :goto_a
    invoke-virtual/range {v44 .. v44}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getText()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "<br>"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3b

    .line 1345
    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 1346
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPTX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/word/ShapesController;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 1347
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 1349
    move-object/from16 v25, v24

    .line 1350
    .local v25, "docPara_temp":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    new-instance v24, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .end local v24    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-direct/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    .line 1351
    .restart local v24    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v3

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    .line 1352
    if-eqz v35, :cond_27

    .line 1353
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v3

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    .line 1354
    :cond_27
    if-eqz v11, :cond_28

    .line 1355
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getAlignment()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-result-object v3

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    .line 1356
    :cond_28
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLineSpacing()F

    move-result v3

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_29

    .line 1357
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLineSpacing()F

    move-result v3

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineSpacing(F)V

    .line 1264
    .end local v25    # "docPara_temp":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    :cond_29
    :goto_b
    add-int/lit8 v30, v30, 0x1

    goto/16 :goto_7

    .line 1111
    .end local v11    # "align":Ljava/lang/String;
    .end local v14    # "buChar":Ljava/lang/String;
    .end local v17    # "buNum":Ljava/lang/String;
    .end local v18    # "bullet":Ljava/lang/String;
    .end local v20    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .end local v21    # "color_temp":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .end local v22    # "curBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .end local v24    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v26    # "eBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .end local v28    # "i":I
    .end local v29    # "indent":Ljava/lang/String;
    .end local v30    # "j":I
    .end local v31    # "level":I
    .end local v35    # "marginLeft":Ljava/lang/String;
    .end local v38    # "para":Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;
    .end local v40    # "paraLstSize":I
    .end local v44    # "run":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    .end local v45    # "runList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;>;"
    .end local v46    # "textRun":Lcom/samsung/thumbnail/customview/word/Run;
    :cond_2a
    move-object/from16 v39, p4

    goto/16 :goto_2

    .line 1181
    .restart local v11    # "align":Ljava/lang/String;
    .restart local v14    # "buChar":Ljava/lang/String;
    .restart local v17    # "buNum":Ljava/lang/String;
    .restart local v22    # "curBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .restart local v24    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .restart local v26    # "eBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .restart local v28    # "i":I
    .restart local v29    # "indent":Ljava/lang/String;
    .restart local v31    # "level":I
    .restart local v35    # "marginLeft":Ljava/lang/String;
    .restart local v38    # "para":Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;
    .restart local v40    # "paraLstSize":I
    :cond_2b
    const-string/jumbo v3, "right"

    invoke-virtual {v11, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2c

    .line 1182
    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->RIGHT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_4

    .line 1183
    :cond_2c
    const-string/jumbo v3, "left"

    invoke-virtual {v11, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2d

    .line 1184
    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->LEFT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_4

    .line 1185
    :cond_2d
    const-string/jumbo v3, "justify"

    invoke-virtual {v11, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1186
    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->JUSTIFY:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_4

    .line 1206
    :cond_2e
    move/from16 v0, v16

    move/from16 v1, v31

    if-ne v0, v1, :cond_12

    .line 1207
    const/4 v15, -0x1

    goto/16 :goto_5

    .line 1216
    :pswitch_0
    rem-int/lit8 v3, v15, 0x1a

    add-int/lit8 v3, v3, 0x61

    int-to-char v10, v3

    .line 1217
    .local v10, "a":C
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v10}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 1218
    goto/16 :goto_6

    .line 1220
    .end local v10    # "a":C
    :pswitch_1
    rem-int/lit8 v3, v15, 0x1a

    add-int/lit8 v3, v3, 0x41

    int-to-char v13, v3

    .line 1221
    .local v13, "b":C
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v13}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 1222
    goto/16 :goto_6

    .line 1224
    .end local v13    # "b":C
    :pswitch_2
    rem-int/lit8 v3, v15, 0x1a

    add-int/lit8 v3, v3, 0x61

    int-to-char v0, v3

    move/from16 v19, v0

    .line 1225
    .local v19, "c":C
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v19 .. v19}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 1227
    goto/16 :goto_6

    .line 1229
    .end local v19    # "c":C
    :pswitch_3
    const/4 v3, -0x1

    if-eq v15, v3, :cond_2f

    if-nez v15, :cond_30

    .line 1230
    :cond_2f
    const/4 v15, 0x1

    .line 1232
    :cond_30
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v15}, Lcom/samsung/thumbnail/util/Utils;->binaryToRomanUC(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 1233
    goto/16 :goto_6

    .line 1235
    :pswitch_4
    const/4 v3, -0x1

    if-eq v15, v3, :cond_31

    if-nez v15, :cond_32

    .line 1236
    :cond_31
    const/4 v15, 0x1

    .line 1238
    :cond_32
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v15}, Lcom/samsung/thumbnail/util/Utils;->binaryToRomanLC(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 1239
    goto/16 :goto_6

    .line 1241
    :pswitch_5
    const/4 v3, -0x1

    if-eq v15, v3, :cond_33

    if-nez v15, :cond_34

    .line 1242
    :cond_33
    const/4 v15, 0x1

    .line 1244
    :cond_34
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v15}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 1245
    goto/16 :goto_6

    .line 1247
    :pswitch_6
    const/4 v3, -0x1

    if-eq v15, v3, :cond_35

    if-nez v15, :cond_36

    .line 1248
    :cond_35
    const/4 v15, 0x1

    .line 1250
    :cond_36
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v15}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 1251
    goto/16 :goto_6

    .line 1255
    :cond_37
    invoke-virtual/range {v41 .. v41}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getBulletChar()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_14

    .line 1256
    invoke-virtual/range {v41 .. v41}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getBulletChar()Ljava/lang/String;

    move-result-object v14

    goto/16 :goto_6

    .line 1275
    .restart local v18    # "bullet":Ljava/lang/String;
    .restart local v20    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .restart local v30    # "j":I
    .restart local v44    # "run":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    .restart local v45    # "runList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;>;"
    .restart local v46    # "textRun":Lcom/samsung/thumbnail/customview/word/Run;
    :cond_38
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getCharProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v20

    goto/16 :goto_8

    .line 1322
    .restart local v21    # "color_temp":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    :cond_39
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_23

    .line 1323
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "#"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v46

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    goto/16 :goto_9

    .line 1337
    :cond_3a
    if-eqz v14, :cond_26

    .line 1338
    const-string/jumbo v3, "Wingdings"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setFontName(Ljava/lang/String;)V

    .line 1339
    const/4 v3, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setUndreline(Z)V

    .line 1340
    const-string/jumbo v18, "\u2022 "

    goto/16 :goto_a

    .line 1360
    :cond_3b
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3c

    .line 1361
    invoke-virtual/range {v44 .. v44}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getText()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v46

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    .line 1364
    :goto_c
    move-object/from16 v0, v24

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_b

    .line 1363
    :cond_3c
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v44 .. v44}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v46

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    goto :goto_c

    .line 1367
    .end local v18    # "bullet":Ljava/lang/String;
    .end local v20    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .end local v21    # "color_temp":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .end local v44    # "run":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    .end local v46    # "textRun":Lcom/samsung/thumbnail/customview/word/Run;
    :cond_3d
    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 1118
    add-int/lit8 v28, v28, 0x1

    goto/16 :goto_3

    .line 1369
    .end local v11    # "align":Ljava/lang/String;
    .end local v14    # "buChar":Ljava/lang/String;
    .end local v17    # "buNum":Ljava/lang/String;
    .end local v22    # "curBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .end local v24    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v26    # "eBulletType":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .end local v29    # "indent":Ljava/lang/String;
    .end local v30    # "j":I
    .end local v31    # "level":I
    .end local v35    # "marginLeft":Ljava/lang/String;
    .end local v38    # "para":Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;
    .end local v45    # "runList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;>;"
    :cond_3e
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPTX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/word/ShapesController;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 1370
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 1214
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public getAllEmbedds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/openxml4j/opc/PackagePart;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/OpenXML4JException;
        }
    .end annotation

    .prologue
    .line 2011
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCanvas()Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    return-object v0
.end method

.method public getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    return-object v0
.end method

.method public getFolderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->folderName:Ljava/lang/String;

    return-object v0
.end method

.method public getFolderPath()Ljava/io/File;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->folderPath:Ljava/io/File;

    return-object v0
.end method

.method public getLvlName(I)Ljava/lang/String;
    .locals 1
    .param p1, "level"    # I

    .prologue
    .line 1981
    packed-switch p1, :pswitch_data_0

    .line 2002
    const-string/jumbo v0, "lvl1pPr"

    :goto_0
    return-object v0

    .line 1983
    :pswitch_0
    const-string/jumbo v0, "lvl1pPr"

    goto :goto_0

    .line 1985
    :pswitch_1
    const-string/jumbo v0, "lvl2pPr"

    goto :goto_0

    .line 1987
    :pswitch_2
    const-string/jumbo v0, "lvl3pPr"

    goto :goto_0

    .line 1989
    :pswitch_3
    const-string/jumbo v0, "lvl4pPr"

    goto :goto_0

    .line 1991
    :pswitch_4
    const-string/jumbo v0, "lvl5pPr"

    goto :goto_0

    .line 1993
    :pswitch_5
    const-string/jumbo v0, "lvl6pPr"

    goto :goto_0

    .line 1995
    :pswitch_6
    const-string/jumbo v0, "lvl7pPr"

    goto :goto_0

    .line 1997
    :pswitch_7
    const-string/jumbo v0, "lvl8pPr"

    goto :goto_0

    .line 1999
    :pswitch_8
    const-string/jumbo v0, "lvl9pPr"

    goto :goto_0

    .line 1981
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public getPageSize()Lorg/apache/poi/java/awt/Dimension;
    .locals 3

    .prologue
    .line 2048
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getSlideWidth()I

    move-result v0

    .line 2049
    .local v0, "cx":I
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getSlideHeight()I

    move-result v1

    .line 2050
    .local v1, "cy":I
    new-instance v2, Lorg/apache/poi/java/awt/Dimension;

    invoke-direct {v2, v0, v1}, Lorg/apache/poi/java/awt/Dimension;-><init>(II)V

    return-object v2
.end method

.method public getPresentation()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;
    .locals 1

    .prologue
    .line 2055
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    return-object v0
.end method

.method public getSlideMasters()[Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .locals 2

    .prologue
    .line 2015
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->masters:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->masters:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    new-array v1, v1, [Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    return-object v0
.end method

.method public getSlideNumber()I
    .locals 1

    .prologue
    .line 2026
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slideCnt:I

    return v0
.end method

.method public getSlides()[Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .locals 2

    .prologue
    .line 2022
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slides:Ljava/util/List;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slides:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    return-object v0
.end method

.method public getStyleFromMaster(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;I)Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;
    .locals 3
    .param p1, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p2, "curShape"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    .param p3, "level"    # I

    .prologue
    .line 1943
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v0

    .line 1944
    .local v0, "master":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getShapeName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "title"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1945
    invoke-virtual {p0, p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->getLvlName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTitleStyle(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;

    move-result-object v1

    .line 1951
    :goto_0
    return-object v1

    .line 1946
    :cond_0
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getShapeName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "content placeholder"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getShapeName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "subtitle"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1949
    :cond_1
    invoke-virtual {p0, p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->getLvlName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getBodyStyle(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;

    move-result-object v1

    goto :goto_0

    .line 1951
    :cond_2
    invoke-virtual {p0, p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->getLvlName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getOtherStyle(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;

    move-result-object v1

    goto :goto_0
.end method

.method public getTableNumber()I
    .locals 1

    .prologue
    .line 2034
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->tableCnt:I

    return v0
.end method

.method public initializeThumbnail(II)V
    .locals 10
    .param p1, "slideWidth"    # I
    .param p2, "slideHeight"    # I

    .prologue
    .line 167
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->context:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 169
    .local v3, "metrics":Landroid/util/DisplayMetrics;
    new-instance v5, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-direct {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;-><init>()V

    iput-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    .line 171
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    iget v6, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-double v6, v6

    invoke-virtual {v5, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setSlideWidth(D)V

    .line 172
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    iget v6, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v6, v6

    invoke-virtual {v5, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setSlideHeight(D)V

    .line 175
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    int-to-long v6, p2

    invoke-virtual {v5, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setPPTHeight(J)V

    .line 177
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    int-to-long v6, p1

    invoke-virtual {v5, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setPPTWidth(J)V

    .line 179
    new-instance v5, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-direct {v5, v6}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;-><init>(Lcom/samsung/thumbnail/customview/hslf/SlideDimention;)V

    iput-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .line 180
    new-instance v5, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-direct {v5, v6}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;-><init>(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    iput-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    .line 182
    const/4 v4, 0x0

    .line 183
    .local v4, "width":I
    const/4 v2, 0x0

    .line 185
    .local v2, "height":I
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getSlideWidth()D

    move-result-wide v6

    double-to-int v5, v6

    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getSlideHeight()D

    move-result-wide v6

    double-to-int v6, v6

    if-ge v5, v6, :cond_0

    .line 187
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPPTWidth()J

    move-result-wide v6

    long-to-int v4, v6

    .line 188
    int-to-long v6, v4

    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPPTHeight()J

    move-result-wide v8

    mul-long/2addr v6, v8

    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPPTWidth()J

    move-result-wide v8

    div-long/2addr v6, v8

    long-to-int v2, v6

    .line 196
    :goto_0
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    int-to-double v6, v4

    invoke-virtual {v5, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setCustomViewWidth(D)V

    .line 197
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    int-to-double v6, v2

    invoke-virtual {v5, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setCustomViewHeight(D)V

    .line 199
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    int-to-double v6, v4

    invoke-virtual {v5, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v6

    double-to-int v5, v6

    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    int-to-double v8, v2

    invoke-virtual {v6, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v6

    double-to-int v6, v6

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 204
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 205
    .local v1, "canvas":Landroid/graphics/Canvas;
    const/4 v5, -0x1

    invoke-virtual {v1, v5}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 207
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    invoke-virtual {v5, v1}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->setCanvas(Landroid/graphics/Canvas;)V

    .line 208
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    invoke-virtual {v5, v0}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->setCanvasBitmap(Landroid/graphics/Bitmap;)V

    .line 209
    return-void

    .line 191
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    :cond_0
    const-wide v6, 0x3fe999999999999aL    # 0.8

    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPPTHeight()J

    move-result-wide v8

    long-to-double v8, v8

    mul-double/2addr v6, v8

    double-to-int v2, v6

    .line 192
    int-to-long v6, v2

    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPPTWidth()J

    move-result-wide v8

    mul-long/2addr v6, v8

    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPPTHeight()J

    move-result-wide v8

    div-long/2addr v6, v8

    long-to-int v4, v6

    goto :goto_0
.end method

.method public mergeProperties(Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;)Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;
    .locals 3
    .param p1, "phProp"    # Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;
    .param p2, "defProp"    # Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;

    .prologue
    .line 1957
    if-nez p1, :cond_0

    .line 1977
    .end local p2    # "defProp":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;
    :goto_0
    return-object p2

    .line 1961
    .restart local p2    # "defProp":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v1

    .line 1962
    .local v1, "paraProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    if-nez v1, :cond_1

    .line 1963
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v1

    .line 1964
    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->setParaProperty(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V

    .line 1969
    :goto_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getCharProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v0

    .line 1970
    .local v0, "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    if-nez v0, :cond_2

    .line 1971
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getCharProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v0

    .line 1972
    invoke-virtual {p1, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->setCharProperty(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V

    :goto_2
    move-object p2, p1

    .line 1977
    goto :goto_0

    .line 1966
    .end local v0    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    :cond_1
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->mergeParaProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V

    goto :goto_1

    .line 1974
    .restart local v0    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    :cond_2
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getCharProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->mergeCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V

    goto :goto_2
.end method

.method protected onDocumentRead()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 327
    return-void
.end method

.method public readDocument(Ljava/io/File;)V
    .locals 16
    .param p1, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 231
    const/4 v4, 0x0

    .line 232
    .local v4, "inputStream":Ljava/io/InputStream;
    const/4 v10, 0x0

    .line 234
    .local v10, "tablesInputStream":Ljava/io/InputStream;
    :try_start_0
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;-><init>(Lorg/apache/poi/POIXMLDocumentPart;)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    .line 235
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->getCorePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v12

    invoke-virtual {v12}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    .line 236
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    invoke-virtual {v12, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->parsePresentation(Ljava/io/InputStream;)V

    .line 238
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getSlideWidth()I

    move-result v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getSlideHeight()I

    move-result v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->initializeThumbnail(II)V

    .line 241
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "file://"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->context:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->URI_PREFIX:Ljava/lang/String;

    .line 242
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 244
    .local v7, "shIdMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;>;"
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->masters:Ljava/util/Map;

    .line 245
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->getRelations()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/poi/POIXMLDocumentPart;

    .line 247
    .local v6, "p":Lorg/apache/poi/POIXMLDocumentPart;
    instance-of v12, v6, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    if-eqz v12, :cond_3

    .line 248
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->getRelationId(Lorg/apache/poi/POIXMLDocumentPart;)Ljava/lang/String;

    move-result-object v12

    check-cast v6, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .end local v6    # "p":Lorg/apache/poi/POIXMLDocumentPart;
    invoke-interface {v7, v12, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 305
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v7    # "shIdMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;>;"
    :catchall_0
    move-exception v12

    if-eqz v4, :cond_1

    .line 307
    :try_start_1
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    .line 312
    :cond_1
    :goto_1
    if-eqz v10, :cond_2

    .line 314
    :try_start_2
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    .line 317
    :cond_2
    :goto_2
    throw v12

    .line 249
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v6    # "p":Lorg/apache/poi/POIXMLDocumentPart;
    .restart local v7    # "shIdMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;>;"
    :cond_3
    :try_start_3
    instance-of v12, v6, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    if-eqz v12, :cond_4

    .line 250
    move-object v0, v6

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object v5, v0

    .line 252
    .local v5, "master":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->masters:Ljava/util/Map;

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->getRelationId(Lorg/apache/poi/POIXMLDocumentPart;)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 253
    .end local v5    # "master":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    :cond_4
    invoke-virtual {v6}, Lorg/apache/poi/POIXMLDocumentPart;->getPackageRelationship()Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v12

    invoke-virtual {v12}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getRelationshipType()Ljava/lang/String;

    move-result-object v12

    const-string/jumbo v13, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/tableStyles"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 257
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;

    invoke-direct {v12}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;-><init>()V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->tableStyles:Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;

    .line 258
    invoke-virtual {v6}, Lorg/apache/poi/POIXMLDocumentPart;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v12

    invoke-virtual {v12}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v10

    .line 259
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->tableStyles:Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;

    invoke-virtual {v12, v10}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->parseTableStyles(Ljava/io/InputStream;)V

    goto :goto_0

    .line 264
    .end local v6    # "p":Lorg/apache/poi/POIXMLDocumentPart;
    :cond_5
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slides:Ljava/util/List;

    .line 265
    const/4 v8, 0x0

    .line 267
    .local v8, "slideLayout":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getSlideList()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->size()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v12

    if-ge v2, v12, :cond_b

    .line 269
    if-lez v2, :cond_8

    .line 305
    if-eqz v4, :cond_6

    .line 307
    :try_start_4
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 312
    :cond_6
    :goto_4
    if-eqz v10, :cond_7

    .line 314
    :try_start_5
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 320
    :cond_7
    :goto_5
    return-void

    .line 308
    :catch_0
    move-exception v1

    .line 309
    .local v1, "e":Ljava/io/IOException;
    const-string/jumbo v12, "XMLSlideShow"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "IOException : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 315
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 316
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v12, "XMLSlideShow"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "IOException : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 272
    .end local v1    # "e":Ljava/io/IOException;
    :cond_8
    add-int/lit8 v12, v2, 0x1

    :try_start_6
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->setSlideNumber(I)V

    .line 273
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->presentation:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->getSlideList()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    invoke-interface {v7, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .line 274
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    if-eqz v12, :cond_a

    .line 276
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->parseSlide()V

    .line 278
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v8

    .line 279
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->parseSlideLayout()V

    .line 280
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->context:Landroid/content/Context;

    invoke-virtual {v12, v13}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->setContext(Landroid/content/Context;)V

    .line 281
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->context:Landroid/content/Context;

    invoke-virtual {v8, v12}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->setContext(Landroid/content/Context;)V

    .line 283
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v9

    .line 284
    .local v9, "slideMaster":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->parseSlideMaster()V

    .line 285
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->context:Landroid/content/Context;

    invoke-virtual {v9, v12}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->setContext(Landroid/content/Context;)V

    .line 286
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->showMasterShape()Z

    move-result v12

    invoke-virtual {v9, v12}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->setShowMasterShape(Z)V

    .line 289
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getColorMap()Ljava/util/Map;

    move-result-object v12

    if-eqz v12, :cond_9

    .line 290
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v11

    .line 291
    .local v11, "theme":Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getColorMap()Ljava/util/Map;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->setColorMap(Ljava/util/Map;)V

    .line 295
    .end local v11    # "theme":Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    :cond_9
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v8, v12}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->drawBackground(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 297
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->drawCanvasSlide(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V

    .line 299
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->showMasterShape()Z

    move-result v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v12}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->drawCanvasSlide(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;Z)V

    .line 301
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->drawCanvasSlide(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 267
    .end local v9    # "slideMaster":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3

    .line 305
    :cond_b
    if-eqz v4, :cond_c

    .line 307
    :try_start_7
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    .line 312
    :cond_c
    :goto_6
    if-eqz v10, :cond_7

    .line 314
    :try_start_8
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    goto/16 :goto_5

    .line 315
    :catch_2
    move-exception v1

    .line 316
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v12, "XMLSlideShow"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "IOException : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 308
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 309
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v12, "XMLSlideShow"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "IOException : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 308
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "i":I
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v7    # "shIdMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;>;"
    .end local v8    # "slideLayout":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;
    :catch_4
    move-exception v1

    .line 309
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v13, "XMLSlideShow"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "IOException : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 315
    .end local v1    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v1

    .line 316
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v13, "XMLSlideShow"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "IOException : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method public setEmailPreview(Z)V
    .locals 0
    .param p1, "isPreview"    # Z

    .prologue
    .line 228
    return-void
.end method

.method public setSlideNumber(I)V
    .locals 0
    .param p1, "slideNum"    # I

    .prologue
    .line 2030
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->slideCnt:I

    .line 2031
    return-void
.end method

.method public setTableNumber(I)V
    .locals 0
    .param p1, "tableNum"    # I

    .prologue
    .line 2038
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->tableCnt:I

    .line 2039
    return-void
.end method
