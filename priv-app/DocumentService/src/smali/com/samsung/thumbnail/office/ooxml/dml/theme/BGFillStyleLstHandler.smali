.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/BGFillStyleLstHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "BGFillStyleLstHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;


# instance fields
.field private styleName:Ljava/lang/String;

.field private theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V
    .locals 7
    .param p1, "consumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;
    .param p2, "styleName"    # Ljava/lang/String;
    .param p3, "theme"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .prologue
    const/4 v6, 0x0

    .line 33
    invoke-direct {p0, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 35
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/BGFillStyleLstHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .line 36
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/BGFillStyleLstHandler;->styleName:Ljava/lang/String;

    .line 38
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;

    invoke-direct {v2, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 39
    .local v2, "noFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/BGFillStyleLstHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "noFill"

    invoke-virtual {v4, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;

    invoke-direct {v3, p0, v6, p3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 43
    .local v3, "solidFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/BGFillStyleLstHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "solidFill"

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;

    invoke-direct {v1, p0, v6, p3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 46
    .local v1, "gradFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/BGFillStyleLstHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "gradFill"

    invoke-virtual {v4, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler;

    const/16 v4, 0x1f

    invoke-direct {v0, v4, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;)V

    .line 50
    .local v0, "blipFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/BGFillStyleLstHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "blipFill"

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    return-void
.end method


# virtual methods
.method public consumeFill(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 1
    .param p1, "fillProperties"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/BGFillStyleLstHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->setBgFillProp(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V

    .line 156
    return-void
.end method

.method public consumeGS(Ljava/lang/String;FLjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 0
    .param p1, "element"    # Ljava/lang/String;
    .param p2, "pos"    # F
    .param p3, "val"    # Ljava/lang/String;
    .param p4, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 57
    return-void
.end method

.method public setBlipEmbed(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "blipEmbed"    # Ljava/lang/String;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/BGFillStyleLstHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/BGFillStyleLstHandler;->styleName:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/BGFillStyleLstHandler;->styleName:Ljava/lang/String;

    const-string/jumbo v1, "bgFillStyleLst"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 135
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/BGFillStyleLstHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->setBgBlipFillId(Ljava/lang/String;)V

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 136
    :cond_1
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/BGFillStyleLstHandler;->styleName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/BGFillStyleLstHandler;->styleName:Ljava/lang/String;

    const-string/jumbo v1, "fillStyleLst"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/BGFillStyleLstHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->setBlipFill(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setTile(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "flip"    # Ljava/lang/String;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/BGFillStyleLstHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/BGFillStyleLstHandler;->styleName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/BGFillStyleLstHandler;->styleName:Ljava/lang/String;

    const-string/jumbo v1, "bgFillStyleLst"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/BGFillStyleLstHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->setIsBgBlipTile(Z)V

    .line 151
    :cond_0
    return-void
.end method
