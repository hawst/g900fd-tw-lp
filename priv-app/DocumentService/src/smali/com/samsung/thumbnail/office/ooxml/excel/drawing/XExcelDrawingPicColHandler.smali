.class public Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicColHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedTagNoChildrenHandler;
.source "XExcelDrawingPicColHandler.java"


# instance fields
.field private cellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

.field private fromTo:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;)V
    .locals 2
    .param p1, "fromTo"    # Ljava/lang/String;
    .param p2, "cellAnchor"    # Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    .prologue
    .line 22
    const/16 v0, 0xc9

    const-string/jumbo v1, "col"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedTagNoChildrenHandler;-><init>(ILjava/lang/String;)V

    .line 24
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicColHandler;->fromTo:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicColHandler;->cellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    .line 26
    return-void
.end method


# virtual methods
.method public charactersHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;[CII)V
    .locals 8
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "content"    # [C
    .param p3, "start"    # I
    .param p4, "end"    # I

    .prologue
    .line 31
    invoke-super {p0, p1, p2, p3, p4}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedTagNoChildrenHandler;->charactersHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;[CII)V

    .line 32
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2, p3, p4}, Ljava/lang/String;-><init>([CII)V

    .line 34
    .local v0, "col":Ljava/lang/String;
    const/4 v4, 0x0

    .line 36
    .local v4, "startNum":I
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 42
    :goto_0
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicColHandler;->fromTo:Ljava/lang/String;

    const-string/jumbo v6, "from"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 44
    const/16 v5, 0x10

    if-gt v4, v5, :cond_1

    .line 45
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicColHandler;->cellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    invoke-virtual {v5, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->setFromCol(I)V

    .line 60
    :cond_0
    :goto_1
    return-void

    .line 37
    :catch_0
    move-exception v1

    .line 38
    .local v1, "e":Ljava/lang/NumberFormatException;
    const-string/jumbo v5, "XExcelDrawingPicColHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "NumberFormatException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 47
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    :cond_1
    const/4 v2, 0x0

    .line 48
    .local v2, "found":Z
    :cond_2
    :goto_2
    if-nez v2, :cond_0

    .line 49
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->popHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    move-result-object v3

    .line 50
    .local v3, "handler":Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    if-eqz v3, :cond_3

    instance-of v5, v3, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingTwoCellAnchorHandler;

    if-nez v5, :cond_3

    instance-of v5, v3, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingOneCellAnchorHandler;

    if-eqz v5, :cond_2

    .line 53
    :cond_3
    const/4 v2, 0x1

    goto :goto_2

    .line 57
    .end local v2    # "found":Z
    .end local v3    # "handler":Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    :cond_4
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicColHandler;->cellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    instance-of v5, v5, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;

    if-eqz v5, :cond_0

    .line 58
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicColHandler;->cellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    check-cast v5, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;

    invoke-virtual {v5, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;->setToCol(I)V

    goto :goto_1
.end method
