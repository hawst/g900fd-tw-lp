.class public Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;
.super Ljava/lang/Object;
.source "EMR_PolyPolygon16.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;


# instance fields
.field private RecordSize:I

.field private RecordType:I

.field private aPoints:[B

.field arrayOfPoint:[Landroid/graphics/Point;

.field private emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

.field fileContent:[B

.field private leftTopX:I

.field private leftTopY:I

.field private localPath:Landroid/graphics/Path;

.field private maxPolygonCount:I

.field private offset:I

.field private pointsCount:I

.field private polygonCount:I

.field private polygonCountArray:[I

.field private polygonPointCount:[B

.field private rightBottomX:I

.field private rightBottomY:I

.field twoDArrayPoints:[[Landroid/graphics/Point;


# direct methods
.method public constructor <init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V
    .locals 1
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I
    .param p3, "emrGlobals"    # Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .prologue
    const/4 v0, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->localPath:Landroid/graphics/Path;

    .line 66
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->arrayOfPoint:[Landroid/graphics/Point;

    .line 69
    check-cast v0, [[Landroid/graphics/Point;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->twoDArrayPoints:[[Landroid/graphics/Point;

    .line 83
    iput-object p3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .line 84
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->fileContent:[B

    .line 85
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    .line 86
    return-void
.end method


# virtual methods
.method public getLeftTopX()I
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->leftTopX:I

    return v0
.end method

.method public getLeftTopY()I
    .locals 1

    .prologue
    .line 151
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->leftTopY:I

    return v0
.end method

.method public getPath()Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->localPath:Landroid/graphics/Path;

    return-object v0
.end method

.method public getPointsCount()I
    .locals 1

    .prologue
    .line 227
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->pointsCount:I

    return v0
.end method

.method public getPolygonCount()I
    .locals 1

    .prologue
    .line 208
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->polygonCount:I

    return v0
.end method

.method public getPolygonPointCount()[B
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->polygonPointCount:[B

    return-object v0
.end method

.method public getRecordSize()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->RecordSize:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->RecordType:I

    return v0
.end method

.method public getRightBottomX()I
    .locals 1

    .prologue
    .line 170
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->rightBottomX:I

    return v0
.end method

.method public getRightBottomY()I
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->rightBottomY:I

    return v0
.end method

.method public getaPoints()[B
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->aPoints:[B

    return-object v0
.end method

.method public prepare()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 365
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->getaPoints()[B

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->setPoints([B)V

    .line 366
    new-instance v7, Landroid/graphics/Path;

    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    .line 367
    .local v7, "localPath1":Landroid/graphics/Path;
    new-instance v8, Landroid/graphics/Path;

    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    iput-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->localPath:Landroid/graphics/Path;

    .line 369
    const/4 v6, 0x0

    .line 372
    .local v6, "l":I
    :goto_0
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->polygonCount:I

    if-lt v6, v8, :cond_0

    .line 404
    return-void

    .line 375
    :cond_0
    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->twoDArrayPoints:[[Landroid/graphics/Point;

    aget-object v8, v8, v6

    aget-object v8, v8, v10

    iget v8, v8, Landroid/graphics/Point;->x:I

    int-to-float v0, v8

    .line 376
    .local v0, "f1":F
    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->twoDArrayPoints:[[Landroid/graphics/Point;

    aget-object v8, v8, v6

    aget-object v8, v8, v10

    iget v8, v8, Landroid/graphics/Point;->y:I

    int-to-float v1, v8

    .line 377
    .local v1, "f2":F
    const/4 v2, 0x0

    .line 378
    .local v2, "f3":F
    const/4 v3, 0x0

    .line 380
    .local v3, "f4":F
    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->twoDArrayPoints:[[Landroid/graphics/Point;

    aget-object v8, v8, v6

    aget-object v8, v8, v10

    if-eqz v8, :cond_1

    .line 381
    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v8, v0, v8

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v9, v9, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v9, v1, v9

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 384
    :cond_1
    const/4 v4, 0x0

    .line 385
    .local v4, "i":I
    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->polygonCountArray:[I

    aget v5, v8, v6

    .line 388
    .local v5, "j":I
    :goto_1
    if-lt v4, v5, :cond_2

    .line 400
    add-int/lit8 v6, v6, 0x1

    .line 401
    invoke-virtual {v7}, Landroid/graphics/Path;->close()V

    .line 402
    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->localPath:Landroid/graphics/Path;

    invoke-virtual {v8, v7}, Landroid/graphics/Path;->addPath(Landroid/graphics/Path;)V

    goto :goto_0

    .line 391
    :cond_2
    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->twoDArrayPoints:[[Landroid/graphics/Point;

    aget-object v8, v8, v6

    aget-object v8, v8, v4

    if-eqz v8, :cond_3

    .line 392
    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->twoDArrayPoints:[[Landroid/graphics/Point;

    aget-object v8, v8, v6

    aget-object v8, v8, v4

    iget v8, v8, Landroid/graphics/Point;->x:I

    int-to-float v2, v8

    .line 393
    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->twoDArrayPoints:[[Landroid/graphics/Point;

    aget-object v8, v8, v6

    aget-object v8, v8, v4

    iget v8, v8, Landroid/graphics/Point;->y:I

    int-to-float v3, v8

    .line 395
    :cond_3
    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v8, v2, v8

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v9, v9, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v9, v3, v9

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 397
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method public readEmfPolyPolygon16()V
    .locals 20

    .prologue
    .line 282
    const/4 v6, 0x0

    .line 284
    .local v6, "i":I
    const/16 v17, 0x4

    move/from16 v0, v17

    new-array v7, v0, [B

    .line 286
    .local v7, "intConvert":[B
    const/4 v6, 0x0

    :goto_0
    const/16 v17, 0x3

    move/from16 v0, v17

    if-gt v6, v0, :cond_0

    .line 287
    rsub-int/lit8 v17, v6, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->fileContent:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    move/from16 v19, v0

    add-int v19, v19, v6

    aget-byte v18, v18, v19

    aput-byte v18, v7, v17

    .line 286
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 288
    :cond_0
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v7, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v4

    .line 289
    .local v4, "Type":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    move/from16 v17, v0

    add-int v17, v17, v6

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    .line 290
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->setRecordType(I)V

    .line 292
    const/4 v6, 0x0

    :goto_1
    const/16 v17, 0x3

    move/from16 v0, v17

    if-gt v6, v0, :cond_1

    .line 293
    rsub-int/lit8 v17, v6, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->fileContent:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    move/from16 v19, v0

    add-int v19, v19, v6

    aget-byte v18, v18, v19

    aput-byte v18, v7, v17

    .line 292
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 294
    :cond_1
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v7, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v3

    .line 295
    .local v3, "Size":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    move/from16 v17, v0

    add-int v17, v17, v6

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    .line 296
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->setRecordSize(I)V

    .line 298
    const/4 v6, 0x0

    :goto_2
    const/16 v17, 0x3

    move/from16 v0, v17

    if-gt v6, v0, :cond_2

    .line 299
    rsub-int/lit8 v17, v6, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->fileContent:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    move/from16 v19, v0

    add-int v19, v19, v6

    aget-byte v18, v18, v19

    aput-byte v18, v7, v17

    .line 298
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 300
    :cond_2
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v7, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v9

    .line 301
    .local v9, "leftTopX":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    move/from16 v17, v0

    add-int v17, v17, v6

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    .line 302
    int-to-float v0, v9

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->setLeftTopX(I)V

    .line 304
    const/4 v6, 0x0

    :goto_3
    const/16 v17, 0x3

    move/from16 v0, v17

    if-gt v6, v0, :cond_3

    .line 305
    rsub-int/lit8 v17, v6, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->fileContent:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    move/from16 v19, v0

    add-int v19, v19, v6

    aget-byte v18, v18, v19

    aput-byte v18, v7, v17

    .line 304
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 306
    :cond_3
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v7, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v10

    .line 307
    .local v10, "leftTopY":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    move/from16 v17, v0

    add-int v17, v17, v6

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    .line 308
    int-to-float v0, v10

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->setLeftTopY(I)V

    .line 310
    const/4 v6, 0x0

    :goto_4
    const/16 v17, 0x3

    move/from16 v0, v17

    if-gt v6, v0, :cond_4

    .line 311
    rsub-int/lit8 v17, v6, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->fileContent:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    move/from16 v19, v0

    add-int v19, v19, v6

    aget-byte v18, v18, v19

    aput-byte v18, v7, v17

    .line 310
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 312
    :cond_4
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v7, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v15

    .line 313
    .local v15, "rightBottomX":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    move/from16 v17, v0

    add-int v17, v17, v6

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    .line 314
    int-to-float v0, v15

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->setRightBottomX(I)V

    .line 316
    const/4 v6, 0x0

    :goto_5
    const/16 v17, 0x3

    move/from16 v0, v17

    if-gt v6, v0, :cond_5

    .line 317
    rsub-int/lit8 v17, v6, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->fileContent:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    move/from16 v19, v0

    add-int v19, v19, v6

    aget-byte v18, v18, v19

    aput-byte v18, v7, v17

    .line 316
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 318
    :cond_5
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v7, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v16

    .line 319
    .local v16, "rightBottomY":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    move/from16 v17, v0

    add-int v17, v17, v6

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    .line 320
    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->setRightBottomY(I)V

    .line 322
    const/4 v6, 0x0

    :goto_6
    const/16 v17, 0x3

    move/from16 v0, v17

    if-gt v6, v0, :cond_6

    .line 323
    rsub-int/lit8 v17, v6, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->fileContent:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    move/from16 v19, v0

    add-int v19, v19, v6

    aget-byte v18, v18, v19

    aput-byte v18, v7, v17

    .line 322
    add-int/lit8 v6, v6, 0x1

    goto :goto_6

    .line 324
    :cond_6
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v7, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v14

    .line 325
    .local v14, "polygonCount":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    move/from16 v17, v0

    add-int v17, v17, v6

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    .line 326
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->setPolygonCount(I)V

    .line 328
    const/4 v6, 0x0

    :goto_7
    const/16 v17, 0x3

    move/from16 v0, v17

    if-gt v6, v0, :cond_7

    .line 329
    rsub-int/lit8 v17, v6, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->fileContent:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    move/from16 v19, v0

    add-int v19, v19, v6

    aget-byte v18, v18, v19

    aput-byte v18, v7, v17

    .line 328
    add-int/lit8 v6, v6, 0x1

    goto :goto_7

    .line 330
    :cond_7
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v7, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v12

    .line 331
    .local v12, "pointCount":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    move/from16 v17, v0

    add-int v17, v17, v6

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    .line 332
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->setPointsCount(I)V

    .line 335
    shl-int/lit8 v17, v14, 0x2

    move/from16 v0, v17

    new-array v2, v0, [B

    .line 336
    .local v2, "PolygonPointCount":[B
    new-array v0, v14, [I

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->polygonCountArray:[I

    .line 337
    const/4 v6, 0x0

    :goto_8
    if-ge v6, v14, :cond_a

    .line 338
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_9
    const/16 v17, 0x3

    move/from16 v0, v17

    if-gt v8, v0, :cond_8

    .line 339
    rsub-int/lit8 v17, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->fileContent:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    move/from16 v19, v0

    add-int v19, v19, v8

    aget-byte v18, v18, v19

    aput-byte v18, v7, v17

    .line 338
    add-int/lit8 v8, v8, 0x1

    goto :goto_9

    .line 340
    :cond_8
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v7, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v11

    .line 341
    .local v11, "pcount":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    move/from16 v17, v0

    add-int v17, v17, v8

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    .line 342
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->polygonCountArray:[I

    move-object/from16 v17, v0

    aput v11, v17, v6

    .line 344
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->maxPolygonCount:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->polygonCountArray:[I

    move-object/from16 v18, v0

    aget v18, v18, v6

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_9

    .line 345
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->polygonCountArray:[I

    move-object/from16 v17, v0

    aget v17, v17, v6

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->maxPolygonCount:I

    .line 337
    :cond_9
    add-int/lit8 v6, v6, 0x1

    goto :goto_8

    .line 348
    .end local v8    # "j":I
    .end local v11    # "pcount":I
    :cond_a
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->setPolygonPointCount([B)V

    .line 350
    add-int/lit8 v17, v3, -0x20

    shl-int/lit8 v18, v14, 0x2

    sub-int v5, v17, v18

    .line 351
    .local v5, "buffsize":I
    new-array v13, v5, [B

    .line 352
    .local v13, "pointsBuffer":[B
    const/4 v6, 0x0

    :goto_a
    if-ge v6, v5, :cond_b

    .line 353
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->fileContent:[B

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    move/from16 v18, v0

    add-int v18, v18, v6

    aget-byte v17, v17, v18

    aput-byte v17, v13, v6

    .line 352
    add-int/lit8 v6, v6, 0x1

    goto :goto_a

    .line 355
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    move/from16 v17, v0

    add-int v17, v17, v6

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->offset:I

    .line 356
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->setaPoints([B)V

    .line 357
    return-void
.end method

.method public render(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 507
    return-void
.end method

.method public setLeftTopX(I)V
    .locals 0
    .param p1, "leftTopX"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->leftTopX:I

    .line 143
    return-void
.end method

.method public setLeftTopY(I)V
    .locals 0
    .param p1, "leftTopY"    # I

    .prologue
    .line 161
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->leftTopY:I

    .line 162
    return-void
.end method

.method public setPoints([B)V
    .locals 13
    .param p1, "points"    # [B

    .prologue
    .line 516
    const/4 v2, 0x0

    .line 517
    .local v2, "i":I
    const/4 v4, 0x0

    .line 518
    .local v4, "j":I
    const/4 v5, 0x0

    .line 519
    .local v5, "k":I
    const/4 v8, 0x0

    .line 521
    .local v8, "offset":I
    array-length v11, p1

    shr-int/lit8 v11, v11, 0x2

    new-array v11, v11, [Landroid/graphics/Point;

    iput-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->arrayOfPoint:[Landroid/graphics/Point;

    .line 522
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->polygonCount:I

    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->maxPolygonCount:I

    filled-new-array {v11, v12}, [I

    move-result-object v11

    const-class v12, Landroid/graphics/Point;

    invoke-static {v12, v11}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [[Landroid/graphics/Point;

    iput-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->twoDArrayPoints:[[Landroid/graphics/Point;

    .line 524
    const/4 v11, 0x2

    new-array v3, v11, [B

    .line 526
    .local v3, "intConvert":[B
    :goto_0
    array-length v11, p1

    if-ge v8, v11, :cond_4

    .line 527
    const/4 v2, 0x0

    :goto_1
    const/4 v11, 0x1

    if-gt v2, v11, :cond_0

    .line 528
    rsub-int/lit8 v11, v2, 0x1

    add-int v12, v8, v2

    aget-byte v12, p1, v12

    aput-byte v12, v3, v11

    .line 527
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 529
    :cond_0
    const/4 v11, 0x0

    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getSmallInt([BI)I

    move-result v0

    .line 530
    .local v0, "X":I
    add-int/2addr v8, v2

    .line 532
    const v11, 0x8000

    and-int/2addr v11, v0

    const v12, 0x8000

    if-ne v11, v12, :cond_1

    .line 533
    xor-int/lit8 v11, v0, -0x1

    const v12, 0xffff

    and-int/2addr v11, v12

    add-int/lit8 v11, v11, 0x1

    mul-int/lit8 v0, v11, -0x1

    .line 536
    :cond_1
    const/4 v4, 0x0

    :goto_2
    const/4 v11, 0x1

    if-gt v4, v11, :cond_2

    .line 537
    rsub-int/lit8 v11, v4, 0x1

    add-int v12, v8, v4

    aget-byte v12, p1, v12

    aput-byte v12, v3, v11

    .line 536
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 538
    :cond_2
    const/4 v11, 0x0

    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getSmallInt([BI)I

    move-result v1

    .line 540
    .local v1, "Y":I
    const v11, 0x8000

    and-int/2addr v11, v1

    const v12, 0x8000

    if-ne v11, v12, :cond_3

    .line 541
    xor-int/lit8 v11, v1, -0x1

    const v12, 0xffff

    and-int/2addr v11, v12

    add-int/lit8 v11, v11, 0x1

    mul-int/lit8 v1, v11, -0x1

    .line 544
    :cond_3
    add-int/2addr v8, v4

    .line 545
    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->arrayOfPoint:[Landroid/graphics/Point;

    new-instance v12, Landroid/graphics/Point;

    invoke-direct {v12, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    aput-object v12, v11, v5

    .line 547
    add-int/lit8 v5, v5, 0x1

    .line 548
    goto :goto_0

    .line 550
    .end local v0    # "X":I
    .end local v1    # "Y":I
    :cond_4
    const/4 v9, 0x0

    .line 551
    .local v9, "r":I
    const/4 v6, 0x0

    .local v6, "m":I
    :goto_3
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->polygonCount:I

    if-ge v6, v11, :cond_6

    .line 552
    const/4 v7, 0x0

    .local v7, "n":I
    :goto_4
    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->polygonCountArray:[I

    aget v11, v11, v6

    if-ge v7, v11, :cond_5

    .line 553
    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->twoDArrayPoints:[[Landroid/graphics/Point;

    aget-object v11, v11, v6

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->arrayOfPoint:[Landroid/graphics/Point;

    add-int/lit8 v10, v9, 0x1

    .end local v9    # "r":I
    .local v10, "r":I
    aget-object v12, v12, v9

    aput-object v12, v11, v7

    .line 552
    add-int/lit8 v7, v7, 0x1

    move v9, v10

    .end local v10    # "r":I
    .restart local v9    # "r":I
    goto :goto_4

    .line 551
    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 556
    .end local v7    # "n":I
    :cond_6
    return-void
.end method

.method public setPointsCount(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 237
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->pointsCount:I

    .line 238
    return-void
.end method

.method public setPolygonCount(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 218
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->polygonCount:I

    .line 219
    return-void
.end method

.method public setPolygonPointCount([B)V
    .locals 0
    .param p1, "polygonPointCount"    # [B

    .prologue
    .line 256
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->polygonPointCount:[B

    .line 257
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 123
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->RecordSize:I

    .line 124
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 104
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->RecordType:I

    .line 105
    return-void
.end method

.method public setRightBottomX(I)V
    .locals 0
    .param p1, "rightBottomX"    # I

    .prologue
    .line 180
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->rightBottomX:I

    .line 181
    return-void
.end method

.method public setRightBottomY(I)V
    .locals 0
    .param p1, "rightBottomY"    # I

    .prologue
    .line 199
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->rightBottomY:I

    .line 200
    return-void
.end method

.method public setaPoints([B)V
    .locals 0
    .param p1, "aPoints"    # [B

    .prologue
    .line 275
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolygon16;->aPoints:[B

    .line 276
    return-void
.end method
