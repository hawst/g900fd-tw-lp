.class public Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler$Shade;
.super Ljava/lang/Object;
.source "XDocTblPrExHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Shade"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler$Shade;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V
    .locals 3
    .param p1, "value"    # Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;

    .prologue
    .line 83
    instance-of v1, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadeValue;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 84
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadeValue;

    .line 85
    .local v0, "shade":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadeValue;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler$Shade;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;

    iget-object v1, v1, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;->tableProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadeValue;->getValue()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->setShade(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;)V

    .line 87
    .end local v0    # "shade":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadeValue;
    :cond_0
    return-void
.end method
