.class public abstract Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;
.super Ljava/lang/Object;
.source "XSLFShape.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;,
        Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    return-void
.end method


# virtual methods
.method public abstract getAnchor()Lorg/apache/poi/java/awt/geom/Rectangle2D;
.end method

.method public abstract getPrstShapeName()Ljava/lang/String;
.end method

.method public abstract getShapeId()I
.end method

.method public abstract getShapeName()Ljava/lang/String;
.end method

.method public abstract getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
.end method

.method public abstract setAnchor(Lorg/apache/poi/java/awt/geom/Rectangle2D;)V
.end method
