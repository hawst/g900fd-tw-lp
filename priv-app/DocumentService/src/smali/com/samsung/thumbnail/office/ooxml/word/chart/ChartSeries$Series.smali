.class public Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
.super Ljava/lang/Object;
.source "ChartSeries.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Series"
.end annotation


# instance fields
.field private fillColor:Ljava/lang/String;

.field private formatCode:Ljava/lang/String;

.field private ptCount:I

.field private ptVal:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private refType:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->ptVal:Ljava/util/ArrayList;

    .line 58
    return-void
.end method


# virtual methods
.method public addPtVal(Ljava/lang/String;)V
    .locals 1
    .param p1, "ptVal"    # Ljava/lang/String;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->ptVal:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 62
    return-void
.end method

.method public getFillColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->fillColor:Ljava/lang/String;

    return-object v0
.end method

.method public getFormatCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->formatCode:Ljava/lang/String;

    return-object v0
.end method

.method public getPtCount()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->ptCount:I

    return v0
.end method

.method public getPtVal()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->ptVal:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getRefType()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->refType:I

    return v0
.end method

.method public setFilColor(Ljava/lang/String;)V
    .locals 0
    .param p1, "fillColor"    # Ljava/lang/String;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->fillColor:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public setPtCount(I)V
    .locals 0
    .param p1, "ptCount"    # I

    .prologue
    .line 77
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->ptCount:I

    .line 78
    return-void
.end method

.method public setRefType(I)V
    .locals 0
    .param p1, "refType"    # I

    .prologue
    .line 69
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->refType:I

    .line 70
    return-void
.end method

.method public setformatCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "formatCode"    # Ljava/lang/String;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->formatCode:Ljava/lang/String;

    .line 94
    return-void
.end method
