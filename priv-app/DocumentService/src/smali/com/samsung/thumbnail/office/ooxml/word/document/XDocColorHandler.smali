.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocColorHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLValueHandler;
.source "XDocColorHandler.java"


# instance fields
.field private valueConsumer:Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V
    .locals 1
    .param p1, "valueConsumer"    # Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;

    .prologue
    .line 19
    const-string/jumbo v0, "color"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLValueHandler;-><init>(Ljava/lang/String;)V

    .line 20
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocColorHandler;->valueConsumer:Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;

    .line 21
    return-void
.end method

.method private getDocXColor(Lorg/xml/sax/Attributes;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Lcom/samsung/thumbnail/office/ooxml/word/XDocColorValue;
    .locals 4
    .param p1, "attrs"    # Lorg/xml/sax/Attributes;
    .param p2, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 43
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 44
    .local v0, "docColor":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocColorValue;

    invoke-direct {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColorValue;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    .line 46
    .local v1, "docColorVal":Lcom/samsung/thumbnail/office/ooxml/word/XDocColorValue;
    const-string/jumbo v3, "val"

    invoke-virtual {p0, p1, v3, p2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocColorHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    .line 47
    .local v2, "value":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 48
    const-string/jumbo v3, "auto"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 49
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setAuto(Z)V

    .line 51
    :cond_0
    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 54
    :cond_1
    const-string/jumbo v3, "themeColor"

    invoke-virtual {p0, p1, v3, p2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocColorHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    .line 55
    if-eqz v2, :cond_2

    .line 56
    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setThemeColor(Ljava/lang/String;)V

    .line 59
    :cond_2
    const-string/jumbo v3, "themeShade"

    invoke-virtual {p0, p1, v3, p2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocColorHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    .line 60
    if-eqz v2, :cond_3

    .line 61
    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setShade(Ljava/lang/String;)V

    .line 64
    :cond_3
    const-string/jumbo v3, "themeTint"

    invoke-virtual {p0, p1, v3, p2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocColorHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    .line 65
    if-eqz v2, :cond_4

    .line 66
    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setTint(Ljava/lang/String;)V

    .line 68
    :cond_4
    return-object v1
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 26
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocColorHandler;->valueConsumer:Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;

    if-nez v1, :cond_0

    .line 31
    :goto_0
    return-void

    .line 29
    :cond_0
    invoke-direct {p0, p3, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocColorHandler;->getDocXColor(Lorg/xml/sax/Attributes;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Lcom/samsung/thumbnail/office/ooxml/word/XDocColorValue;

    move-result-object v0

    .line 30
    .local v0, "docColorVal":Lcom/samsung/thumbnail/office/ooxml/word/XDocColorValue;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocColorHandler;->valueConsumer:Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;

    invoke-interface {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;->consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V

    goto :goto_0
.end method
