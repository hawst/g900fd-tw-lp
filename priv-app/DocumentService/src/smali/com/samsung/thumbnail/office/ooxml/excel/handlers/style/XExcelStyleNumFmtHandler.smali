.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleNumFmtHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelStyleNumFmtHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    const-string/jumbo v0, "numFmt"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 18
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 23
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 24
    const-string/jumbo v2, "numFmtId"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleNumFmtHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 26
    .local v1, "numFmtId":Ljava/lang/String;
    const-string/jumbo v2, "formatCode"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleNumFmtHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 28
    .local v0, "formatcode":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v2, v1, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addstylenumfmtcode(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    return-void
.end method
