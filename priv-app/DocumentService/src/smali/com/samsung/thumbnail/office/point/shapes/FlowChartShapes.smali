.class public Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;
.super Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;
.source "FlowChartShapes.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mDrawOnCanvas:Z

.field private mLeftMarg:I

.field private mTopMarg:I


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "folderPath"    # Ljava/io/File;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;-><init>()V

    .line 29
    const-string/jumbo v0, "FlowChartShapes"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->TAG:Ljava/lang/String;

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mDrawOnCanvas:Z

    .line 35
    iput-object p1, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->folderPath:Ljava/io/File;

    .line 36
    return-void
.end method


# virtual methods
.method protected draw(Landroid/graphics/Canvas;I)V
    .locals 20
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeCount"    # I

    .prologue
    .line 162
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    const v4, 0x46a8c000    # 21600.0f

    div-float v12, v3, v4

    .line 163
    .local v12, "mulFactor_Width":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    const v4, 0x46a8c000    # 21600.0f

    div-float v11, v3, v4

    .line 165
    .local v11, "mulFactor_Height":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartProcess"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 167
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 168
    .local v2, "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 169
    const/4 v3, 0x0

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 170
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 172
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 173
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 175
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_0

    .line 176
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 177
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_1

    .line 178
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 182
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 183
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 184
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 185
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    .line 1098
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_2
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mDrawOnCanvas:Z

    if-nez v3, :cond_3

    .line 1099
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 1102
    .local v8, "mtx":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->rotation:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    invoke-virtual {v8, v3, v4, v5}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 1104
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->bitmap:Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->bitmapWidth:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->bitmapHight:I

    const/4 v9, 0x1

    invoke-static/range {v3 .. v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 1109
    .local v16, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v13, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->folderPath:Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "pic_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ".png"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v13, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 1115
    .local v13, "out":Ljava/io/FileOutputStream;
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4, v13}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1116
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1121
    .end local v8    # "mtx":Landroid/graphics/Matrix;
    .end local v13    # "out":Ljava/io/FileOutputStream;
    .end local v16    # "rotatedBMP":Landroid/graphics/Bitmap;
    :cond_3
    :goto_1
    return-void

    .line 188
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartAlternateProcess"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 193
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->adjval0:I

    if-nez v3, :cond_5

    .line 194
    const/16 v3, 0xa8c

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->adjval0:I

    .line 197
    :cond_5
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 198
    .restart local v2    # "path":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->adjval0:I

    int-to-float v3, v3

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 200
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->adjval0:I

    int-to-float v6, v6

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 203
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->adjval0:I

    int-to-float v5, v5

    mul-float/2addr v5, v11

    sub-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 205
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->adjval0:I

    int-to-float v5, v5

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 208
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->adjval0:I

    int-to-float v4, v4

    mul-float/2addr v4, v12

    sub-float/2addr v3, v4

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 210
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->adjval0:I

    int-to-float v7, v7

    mul-float/2addr v7, v11

    sub-float/2addr v6, v7

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 213
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->adjval0:I

    int-to-float v4, v4

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 215
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->adjval0:I

    int-to-float v6, v6

    mul-float/2addr v6, v12

    sub-float/2addr v5, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 218
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 220
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_6

    .line 221
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 222
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_7

    .line 223
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 227
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 228
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 229
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 230
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 233
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartDecision"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 235
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 236
    .restart local v2    # "path":Landroid/graphics/Path;
    const v3, 0x4628c000    # 10800.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 237
    const/4 v3, 0x0

    const v4, 0x4628c000    # 10800.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 238
    const v3, 0x4628c000    # 10800.0f

    mul-float/2addr v3, v12

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 239
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    const v4, 0x4628c000    # 10800.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 240
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 242
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_9

    .line 243
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 244
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_a

    .line 245
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 249
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x45d2f000    # 6750.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 250
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x45d2f000    # 6750.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 251
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46680800    # 14850.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 252
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46680800    # 14850.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 255
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartInputOutput"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 257
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 258
    .restart local v2    # "path":Landroid/graphics/Path;
    const v3, 0x45870800    # 4321.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 259
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 260
    const v3, 0x46866800    # 17204.0f

    mul-float/2addr v3, v12

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 261
    const/4 v3, 0x0

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 262
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 264
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_c

    .line 265
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 266
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_d

    .line 267
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 271
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x45a8c000    # 5400.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 272
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 273
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x467d2000    # 16200.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 274
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a8c000    # 21600.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 277
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartPredefinedProcess"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 279
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 280
    .restart local v2    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 281
    const/4 v3, 0x0

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 282
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 283
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 284
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 286
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_f

    .line 287
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 288
    :cond_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_10

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_10

    .line 289
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 291
    :cond_10
    const v3, 0x45232000    # 2610.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 292
    const v3, 0x45232000    # 2610.0f

    mul-float/2addr v3, v12

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 293
    const v3, 0x46945c00    # 18990.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 294
    const v3, 0x46945c00    # 18990.0f

    mul-float/2addr v3, v12

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 296
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_11

    .line 297
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 301
    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x457d2000    # 4050.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 302
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 303
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46891c00    # 17550.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 304
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 307
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartInternalStorage"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_16

    .line 308
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 309
    .restart local v2    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 310
    const/4 v3, 0x0

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 311
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 312
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 313
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 315
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_13

    .line 316
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 317
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_14

    .line 318
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 320
    :cond_14
    const v3, 0x45846000    # 4236.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 321
    const v3, 0x45846000    # 4236.0f

    mul-float/2addr v3, v12

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 322
    const/4 v3, 0x0

    const v4, 0x45846000    # 4236.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 323
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    const v4, 0x45846000    # 4236.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 325
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_15

    .line 326
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 330
    :cond_15
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x457d2000    # 4050.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 331
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x457d2000    # 4050.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 332
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 333
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 336
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_16
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartTerminator"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_19

    .line 337
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 338
    .restart local v2    # "path":Landroid/graphics/Path;
    const v3, 0x45593000    # 3475.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 340
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x4628c000    # 10800.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 343
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x45593000    # 3475.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 347
    const v3, 0x468d9a00    # 18125.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 349
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x4628c000    # 10800.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 353
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x468d9a00    # 18125.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 356
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 358
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_17

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_17

    .line 359
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 360
    :cond_17
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_18

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_18

    .line 361
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 365
    :cond_18
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 366
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x457d2000    # 4050.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 367
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 368
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46891c00    # 17550.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 371
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_19
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartPreparation"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1c

    .line 372
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 373
    .restart local v2    # "path":Landroid/graphics/Path;
    const v3, 0x45880800    # 4353.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 374
    const v3, 0x46867c00    # 17214.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 375
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    const v4, 0x4628c000    # 10800.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 377
    const v3, 0x46867c00    # 17214.0f

    mul-float/2addr v3, v12

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 378
    const v3, 0x45880800    # 4353.0f

    mul-float/2addr v3, v12

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 380
    const/4 v3, 0x0

    const v4, 0x4628c000    # 10800.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 381
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 383
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_1a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_1a

    .line 384
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 385
    :cond_1a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_1b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_1b

    .line 386
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 390
    :cond_1b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x45a8c000    # 5400.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 391
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 392
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x467d2000    # 16200.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 393
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 396
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_1c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartManualInput"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 397
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 398
    .restart local v2    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    const v4, 0x45862000    # 4292.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 399
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 400
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 401
    const/4 v3, 0x0

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 402
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 404
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_1d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_1d

    .line 405
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 406
    :cond_1d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_1e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_1e

    .line 407
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 410
    :cond_1e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 411
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x457d2000    # 4050.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 412
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 413
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 416
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_1f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartManualOperation"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_22

    .line 417
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 418
    .restart local v2    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 419
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 420
    const v3, 0x4686b000    # 17240.0f

    mul-float/2addr v3, v12

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 421
    const v3, 0x45884000    # 4360.0f

    mul-float/2addr v3, v12

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 423
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 425
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_20

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_20

    .line 426
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 427
    :cond_20
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_21

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_21

    .line 428
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 432
    :cond_21
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x45a8c000    # 5400.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 433
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 434
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x467d2000    # 16200.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 435
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 438
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_22
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartConnector"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_25

    .line 440
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 441
    .restart local v2    # "path":Landroid/graphics/Path;
    const v3, 0x4628c000    # 10800.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 443
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x4628c000    # 10800.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 446
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x4628c000    # 10800.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 450
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x4628c000    # 10800.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 454
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x4628c000    # 10800.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 457
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 459
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_23

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_23

    .line 460
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 461
    :cond_23
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_24

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_24

    .line 462
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 466
    :cond_24
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x4545b000    # 3163.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 467
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x4545b000    # 3163.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 468
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46900a00    # 18437.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 469
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46900a00    # 18437.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 472
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_25
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartOffpageConnector"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_28

    .line 473
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 474
    .restart local v2    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 475
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 476
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    const v4, 0x4686ce00    # 17255.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 477
    const v3, 0x4628c000    # 10800.0f

    mul-float/2addr v3, v12

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 478
    const/4 v3, 0x0

    const v4, 0x4686ce00    # 17255.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 479
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 481
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_26

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_26

    .line 482
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 483
    :cond_26
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_27

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_27

    .line 484
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 488
    :cond_27
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 489
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 490
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 491
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x467d2000    # 16200.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 494
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_28
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartPunchedCard"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2b

    .line 495
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 496
    .restart local v2    # "path":Landroid/graphics/Path;
    const v3, 0x45870800    # 4321.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 497
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 498
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 499
    const/4 v3, 0x0

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 500
    const/4 v3, 0x0

    const v4, 0x45879000    # 4338.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 501
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 503
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_29

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_29

    .line 504
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 505
    :cond_29
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_2a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_2a

    .line 506
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 510
    :cond_2a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 511
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x459d8000    # 5040.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 512
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 513
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 516
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_2b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartSummingJunction"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2f

    .line 517
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 518
    .restart local v2    # "path":Landroid/graphics/Path;
    const v3, 0x4628c000    # 10800.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 520
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x4628c000    # 10800.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 523
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x4628c000    # 10800.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 527
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x4628c000    # 10800.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 531
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x4628c000    # 10800.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 534
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 535
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_2c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_2c

    .line 536
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 537
    :cond_2c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_2d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_2d

    .line 538
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 540
    :cond_2d
    const v3, 0x4545b000    # 3163.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x4545b000    # 3163.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 542
    const v3, 0x46900a00    # 18437.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x46900a00    # 18437.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 544
    const v3, 0x4545b000    # 3163.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x46900a00    # 18437.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 546
    const v3, 0x46900a00    # 18437.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x4545b000    # 3163.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 548
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_2e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_2e

    .line 549
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 553
    :cond_2e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x4545b000    # 3163.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 554
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x4545b000    # 3163.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 555
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46900a00    # 18437.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 556
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46900a00    # 18437.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 559
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_2f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartOr"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_33

    .line 560
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 561
    .restart local v2    # "path":Landroid/graphics/Path;
    const v3, 0x4628c000    # 10800.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 563
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x4628c000    # 10800.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 566
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x4628c000    # 10800.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 570
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x4628c000    # 10800.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 574
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x4628c000    # 10800.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 577
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 578
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_30

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_30

    .line 579
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 580
    :cond_30
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_31

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_31

    .line 581
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 583
    :cond_31
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x4628c000    # 10800.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 585
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x4628c000    # 10800.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 587
    const v3, 0x4628c000    # 10800.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 589
    const v3, 0x4628c000    # 10800.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 591
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_32

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_32

    .line 592
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 596
    :cond_32
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x4545b000    # 3163.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 597
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x4545b000    # 3163.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 598
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46900a00    # 18437.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 599
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46900a00    # 18437.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 602
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_33
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartCollate"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_36

    .line 603
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 604
    .restart local v2    # "path":Landroid/graphics/Path;
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 605
    const/4 v3, 0x0

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 606
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 607
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 608
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 610
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_34

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_34

    .line 611
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 612
    :cond_34
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_35

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_35

    .line 613
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 617
    :cond_35
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x45bdd800    # 6075.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 618
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x45bdd800    # 6075.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 619
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46729400    # 15525.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 620
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46729400    # 15525.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 623
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_36
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartSort"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3a

    .line 624
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 625
    .restart local v2    # "path":Landroid/graphics/Path;
    const v3, 0x4628c000    # 10800.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 626
    const/4 v3, 0x0

    const v4, 0x4628c000    # 10800.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 627
    const v3, 0x4628c000    # 10800.0f

    mul-float/2addr v3, v12

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 628
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    const v4, 0x4628c000    # 10800.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 629
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 630
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_37

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_37

    .line 631
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 632
    :cond_37
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_38

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_38

    .line 633
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 635
    :cond_38
    const/4 v3, 0x0

    const v4, 0x4628c000    # 10800.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 636
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    const v4, 0x4628c000    # 10800.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 637
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_39

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_39

    .line 638
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 642
    :cond_39
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x45bdd800    # 6075.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 643
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x45bdd800    # 6075.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 644
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46729400    # 15525.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 645
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46729400    # 15525.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 648
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_3a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartExtract"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3d

    .line 649
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 650
    .restart local v2    # "path":Landroid/graphics/Path;
    const v3, 0x4628c000    # 10800.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 651
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 652
    const/4 v3, 0x0

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 653
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 655
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_3b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_3b

    .line 656
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 657
    :cond_3b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_3c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_3c

    .line 658
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 662
    :cond_3c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x45bdd800    # 6075.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 663
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46334c00    # 11475.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 664
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46729400    # 15525.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 665
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 668
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_3d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartMerge"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_40

    .line 669
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 670
    .restart local v2    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 671
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 672
    const v3, 0x4628c000    # 10800.0f

    mul-float/2addr v3, v12

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 673
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 675
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_3e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_3e

    .line 676
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 677
    :cond_3e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_3f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_3f

    .line 678
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 682
    :cond_3f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x45bdd800    # 6075.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 683
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 684
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46729400    # 15525.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 685
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x461e3400    # 10125.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 688
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_40
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartOnlineStorage"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_43

    .line 689
    new-instance v14, Landroid/graphics/RectF;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/high16 v5, 0x45610000    # 3600.0f

    mul-float/2addr v5, v12

    const/high16 v6, 0x40000000    # 2.0f

    mul-float/2addr v5, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-direct {v14, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 693
    .local v14, "rectF":Landroid/graphics/RectF;
    new-instance v15, Landroid/graphics/RectF;

    const v3, 0x468ca000    # 18000.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x46c4e000    # 25200.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-direct {v15, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 698
    .local v15, "rectF2":Landroid/graphics/RectF;
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 699
    .restart local v2    # "path":Landroid/graphics/Path;
    const/high16 v3, 0x42b40000    # 90.0f

    const/high16 v4, 0x43340000    # 180.0f

    invoke-virtual {v2, v14, v3, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 700
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 702
    const/high16 v3, 0x43870000    # 270.0f

    const/high16 v4, -0x3ccc0000    # -180.0f

    invoke-virtual {v2, v15, v3, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 703
    const/high16 v3, 0x42c80000    # 100.0f

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 705
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 707
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_41

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_41

    .line 708
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 709
    :cond_41
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_42

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_42

    .line 710
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 714
    :cond_42
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x457d2000    # 4050.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 715
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 716
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46891c00    # 17550.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 717
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 720
    .end local v2    # "path":Landroid/graphics/Path;
    .end local v14    # "rectF":Landroid/graphics/RectF;
    .end local v15    # "rectF2":Landroid/graphics/RectF;
    :cond_43
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartDelay"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_46

    .line 721
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 722
    .restart local v2    # "path":Landroid/graphics/Path;
    const v3, 0x4628c000    # 10800.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 724
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x4628c000    # 10800.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 728
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x4628c000    # 10800.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 732
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 734
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 735
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 737
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_44

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_44

    .line 738
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 739
    :cond_44
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_45

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_45

    .line 740
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 744
    :cond_45
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 745
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x45a8c000    # 5400.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 746
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x4683d600    # 16875.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 747
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x467d2000    # 16200.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 750
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_46
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartMagneticTape"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_49

    .line 751
    new-instance v14, Landroid/graphics/RectF;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-direct {v14, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 756
    .restart local v14    # "rectF":Landroid/graphics/RectF;
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 757
    .restart local v2    # "path":Landroid/graphics/Path;
    const/high16 v3, 0x42b40000    # 90.0f

    const/high16 v4, 0x439c0000    # 312.0f

    invoke-virtual {v2, v14, v3, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 758
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x468dea00    # 18165.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 760
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 762
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 764
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_47

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_47

    .line 765
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 766
    :cond_47
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_48

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_48

    .line 767
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 771
    :cond_48
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x4545b000    # 3163.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 772
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x4545b000    # 3163.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 773
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46900a00    # 18437.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 774
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46900a00    # 18437.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 777
    .end local v2    # "path":Landroid/graphics/Path;
    .end local v14    # "rectF":Landroid/graphics/RectF;
    :cond_49
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartMagneticDisk"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4d

    .line 778
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 779
    .restart local v2    # "path":Landroid/graphics/Path;
    const v3, 0x4628c000    # 10800.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 781
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x4553f000    # 3391.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 784
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x468e4200    # 18209.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 786
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x4628c000    # 10800.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 790
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x468e4200    # 18209.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 794
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x4553f000    # 3391.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 796
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x4628c000    # 10800.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 799
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 800
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_4a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_4a

    .line 801
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 802
    :cond_4a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_4b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_4b

    .line 803
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 805
    :cond_4b
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x4553f000    # 3391.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 807
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x45d3f000    # 6782.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x4628c000    # 10800.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x45d3f000    # 6782.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 811
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x45d3f000    # 6782.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x4553f000    # 3391.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 815
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_4c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_4c

    .line 816
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 820
    :cond_4c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 821
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x4613a800    # 9450.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 822
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 823
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x467d2000    # 16200.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 826
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_4d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartMagneticDrum"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_51

    .line 827
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 828
    .restart local v2    # "path":Landroid/graphics/Path;
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x4628c000    # 10800.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 830
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x468cc600    # 18019.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 834
    const v3, 0x455fd000    # 3581.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 836
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x4628c000    # 10800.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 839
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x455fd000    # 3581.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 842
    const v3, 0x468cc600    # 18019.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 844
    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x4628c000    # 10800.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 848
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 849
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_4e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_4e

    .line 850
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 851
    :cond_4e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_4f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_4f

    .line 852
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 854
    :cond_4f
    const v3, 0x468cc600    # 18019.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 856
    const v3, 0x46619800    # 14438.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float/2addr v4, v11

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x46619800    # 14438.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x4628c000    # 10800.0f

    mul-float/2addr v6, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 860
    const v3, 0x46619800    # 14438.0f

    mul-float/2addr v3, v12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x468cc600    # 18019.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineWidth:D

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    double-to-float v7, v0

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 863
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_50

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_50

    .line 864
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 868
    :cond_50
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x4613a800    # 9450.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 869
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 870
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x467d2000    # 16200.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 871
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 874
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_51
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartDisplay"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_54

    .line 875
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 876
    .restart local v2    # "path":Landroid/graphics/Path;
    const v3, 0x468c4600    # 17955.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 877
    const v3, 0x44578000    # 862.0f

    mul-float/2addr v3, v12

    const/high16 v4, 0x438d0000    # 282.0f

    mul-float/2addr v4, v11

    const v5, 0x44eaa000    # 1877.0f

    mul-float/2addr v5, v12

    const v6, 0x44b04000    # 1410.0f

    mul-float/2addr v6, v11

    const v7, 0x451ad000    # 2477.0f

    mul-float/2addr v7, v12

    const v9, 0x453e5000    # 3045.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 880
    const v3, 0x46a45600    # 21035.0f

    mul-float/2addr v3, v12

    const v4, 0x45a76800    # 5357.0f

    mul-float/2addr v4, v11

    const v5, 0x46a6f800    # 21372.0f

    mul-float/2addr v5, v12

    const v6, 0x45f6b800    # 7895.0f

    mul-float/2addr v6, v11

    const v7, 0x46a8ba00    # 21597.0f

    mul-float/2addr v7, v12

    const v9, 0x46292c00    # 10827.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 883
    const/high16 v3, -0x3c9f0000    # -225.0f

    mul-float/2addr v3, v12

    const v4, 0x452cb000    # 2763.0f

    mul-float/2addr v4, v11

    const v5, -0x3bf38000    # -562.0f

    mul-float/2addr v5, v12

    const v6, 0x45a5a000    # 5300.0f

    mul-float/2addr v6, v11

    const v7, -0x3b6e6000    # -1165.0f

    mul-float/2addr v7, v12

    const v9, 0x45ede800    # 7613.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 886
    const v3, 0x469af000    # 19832.0f

    mul-float/2addr v3, v12

    const v4, 0x469d4800    # 20132.0f

    mul-float/2addr v4, v11

    const v5, 0x46930200    # 18817.0f

    mul-float/2addr v5, v12

    const v6, 0x46a61800    # 21260.0f

    mul-float/2addr v6, v11

    const v7, 0x468c4600    # 17955.0f

    mul-float/2addr v7, v12

    const v9, 0x46a8ba00    # 21597.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 889
    const v3, -0x399f3000    # -14388.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 890
    const/4 v3, 0x0

    const v4, 0x46292c00    # 10827.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 891
    const v3, 0x455ef000    # 3567.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 892
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 894
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_52

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_52

    .line 895
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 896
    :cond_52
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_53

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_53

    .line 897
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 901
    :cond_53
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x457d2000    # 4050.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 902
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 903
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46680800    # 14850.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 904
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 907
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_54
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartPunchedTape"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_57

    .line 908
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 909
    .restart local v2    # "path":Landroid/graphics/Path;
    const v3, 0x46a8ba00    # 21597.0f

    mul-float/2addr v3, v12

    const v4, 0x4697f400    # 19450.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 910
    const/high16 v3, -0x3c9f0000    # -225.0f

    mul-float/2addr v3, v12

    const v4, -0x3bf48000    # -558.0f

    mul-float/2addr v4, v11

    const v5, -0x3bc48000    # -750.0f

    mul-float/2addr v5, v12

    const v6, -0x3b79e000    # -1073.0f

    mul-float/2addr v6, v11

    const v7, -0x3b31c000    # -1650.0f

    mul-float/2addr v7, v12

    const v9, -0x3b3ee000    # -1545.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 913
    const v3, 0x4693a200    # 18897.0f

    mul-float/2addr v3, v12

    const v4, 0x46898a00    # 17605.0f

    mul-float/2addr v4, v11

    const v5, 0x46896200    # 17585.0f

    mul-float/2addr v5, v12

    const v6, 0x46878600    # 17347.0f

    mul-float/2addr v6, v11

    const v7, 0x467d1400    # 16197.0f

    mul-float/2addr v7, v12

    const v9, 0x4686d800    # 17260.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 916
    const v3, -0x3b448000    # -1500.0f

    mul-float/2addr v3, v12

    const/high16 v4, 0x42ae0000    # 87.0f

    mul-float/2addr v4, v11

    const v5, -0x3ad74000    # -2700.0f

    mul-float/2addr v5, v12

    const v6, 0x43ac8000    # 345.0f

    mul-float/2addr v6, v11

    const v7, -0x3a935000    # -3787.0f

    mul-float/2addr v7, v12

    const v9, 0x44214000    # 645.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 919
    const v3, 0x46334000    # 11472.0f

    mul-float/2addr v3, v12

    const v4, 0x468f9200    # 18377.0f

    mul-float/2addr v4, v11

    const v5, 0x462a7800    # 10910.0f

    mul-float/2addr v5, v12

    const v6, 0x46939800    # 18892.0f

    mul-float/2addr v6, v11

    const v7, 0x4628c000    # 10800.0f

    mul-float/2addr v7, v12

    const v9, 0x4697f400    # 19450.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 922
    const/high16 v3, -0x3cc40000    # -188.0f

    mul-float/2addr v3, v12

    const v4, 0x4400c000    # 515.0f

    mul-float/2addr v4, v11

    const v5, -0x3bc48000    # -750.0f

    mul-float/2addr v5, v12

    const v6, 0x44866000    # 1075.0f

    mul-float/2addr v6, v11

    const v7, -0x3b366000    # -1613.0f

    mul-float/2addr v7, v12

    const v9, 0x44b68000    # 1460.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 925
    const v3, 0x45fd2000    # 8100.0f

    mul-float/2addr v3, v12

    const v4, 0x46a5b400    # 21210.0f

    mul-float/2addr v4, v11

    const v5, 0x45d54800    # 6825.0f

    mul-float/2addr v5, v12

    const v6, 0x46a76200    # 21425.0f

    mul-float/2addr v6, v11

    const v7, 0x45a8c000    # 5400.0f

    mul-float/2addr v7, v12

    const v9, 0x46a8ba00    # 21597.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 928
    const v3, 0x45761000    # 3937.0f

    mul-float/2addr v3, v12

    const v4, 0x46a76200    # 21425.0f

    mul-float/2addr v4, v11

    const v5, 0x4528c000    # 2700.0f

    mul-float/2addr v5, v12

    const v6, 0x46a5b400    # 21210.0f

    mul-float/2addr v6, v11

    const v7, 0x44c98000    # 1612.0f

    mul-float/2addr v7, v12

    const v9, 0x46a35c00    # 20910.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 931
    const v3, 0x4428c000    # 675.0f

    mul-float/2addr v3, v12

    const v4, 0x46a05a00    # 20525.0f

    mul-float/2addr v4, v11

    const/high16 v5, 0x43160000    # 150.0f

    mul-float/2addr v5, v12

    const v6, 0x469bfa00    # 19965.0f

    mul-float/2addr v6, v11

    const/4 v7, 0x0

    mul-float/2addr v7, v12

    const v9, 0x4697f400    # 19450.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 934
    const/4 v3, 0x0

    const v4, 0x45063000    # 2147.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 935
    const/high16 v3, 0x43160000    # 150.0f

    mul-float/2addr v3, v12

    const v4, 0x440b8000    # 558.0f

    mul-float/2addr v4, v11

    const v5, 0x4428c000    # 675.0f

    mul-float/2addr v5, v12

    const v6, 0x44862000    # 1073.0f

    mul-float/2addr v6, v11

    const v7, 0x44c98000    # 1612.0f

    mul-float/2addr v7, v12

    const v9, 0x44b68000    # 1460.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 938
    const v3, 0x4528c000    # 2700.0f

    mul-float/2addr v3, v12

    const v4, 0x4576e000    # 3950.0f

    mul-float/2addr v4, v11

    const v5, 0x45761000    # 3937.0f

    mul-float/2addr v5, v12

    const v6, 0x45822800    # 4165.0f

    mul-float/2addr v6, v11

    const v7, 0x45a8c000    # 5400.0f

    mul-float/2addr v7, v12

    const v9, 0x45878800    # 4337.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 941
    const v3, 0x45d54800    # 6825.0f

    mul-float/2addr v3, v12

    const v4, 0x45822800    # 4165.0f

    mul-float/2addr v4, v11

    const v5, 0x45fd2000    # 8100.0f

    mul-float/2addr v5, v12

    const v6, 0x4576e000    # 3950.0f

    mul-float/2addr v6, v11

    const v7, 0x460f8c00    # 9187.0f

    mul-float/2addr v7, v12

    const v9, 0x45617000    # 3607.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 944
    const v3, 0x4457c000    # 863.0f

    mul-float/2addr v3, v12

    const v4, -0x3c3e8000    # -387.0f

    mul-float/2addr v4, v11

    const v5, 0x44b22000    # 1425.0f

    mul-float/2addr v5, v12

    const v6, -0x3b9e8000    # -902.0f

    mul-float/2addr v6, v11

    const v7, 0x44c9a000    # 1613.0f

    mul-float/2addr v7, v12

    const v9, -0x3b498000    # -1460.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 947
    const v3, 0x462a7800    # 10910.0f

    mul-float/2addr v3, v12

    const/high16 v4, 0x44cc0000    # 1632.0f

    mul-float/2addr v4, v11

    const v5, 0x46334000    # 11472.0f

    mul-float/2addr v5, v12

    const/high16 v6, 0x44860000    # 1072.0f

    mul-float/2addr v6, v11

    const v7, 0x4641e800    # 12410.0f

    mul-float/2addr v7, v12

    const/high16 v9, 0x44160000    # 600.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 950
    const v3, 0x4652e400    # 13497.0f

    mul-float/2addr v3, v12

    const/high16 v4, 0x43960000    # 300.0f

    mul-float/2addr v4, v11

    const v5, 0x4665a400    # 14697.0f

    mul-float/2addr v5, v12

    const/high16 v6, 0x42aa0000    # 85.0f

    mul-float/2addr v6, v11

    const v7, 0x467d1400    # 16197.0f

    mul-float/2addr v7, v12

    const/4 v9, 0x0

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 953
    const v3, 0x44ad8000    # 1388.0f

    mul-float/2addr v3, v12

    const/high16 v4, 0x42aa0000    # 85.0f

    mul-float/2addr v4, v11

    const v5, 0x4528c000    # 2700.0f

    mul-float/2addr v5, v12

    const/high16 v6, 0x43960000    # 300.0f

    mul-float/2addr v6, v11

    const v7, 0x456a6000    # 3750.0f

    mul-float/2addr v7, v12

    const/high16 v9, 0x44160000    # 600.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 956
    const v3, 0x46a2de00    # 20847.0f

    mul-float/2addr v3, v12

    const/high16 v4, 0x44860000    # 1072.0f

    mul-float/2addr v4, v11

    const v5, 0x46a6f800    # 21372.0f

    mul-float/2addr v5, v12

    const/high16 v6, 0x44cc0000    # 1632.0f

    mul-float/2addr v6, v11

    const v7, 0x46a8ba00    # 21597.0f

    mul-float/2addr v7, v12

    const v9, 0x45063000    # 2147.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 959
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 961
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_55

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_55

    .line 962
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 963
    :cond_55
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_56

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_56

    .line 964
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 968
    :cond_56
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 969
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x45d2f000    # 6750.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 970
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 971
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46680800    # 14850.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 974
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_57
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartDocument"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5a

    .line 975
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 976
    .restart local v2    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    mul-float/2addr v3, v12

    const v4, 0x469d9800    # 20172.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 977
    const v3, 0x446c4000    # 945.0f

    mul-float/2addr v3, v12

    const/high16 v4, 0x43c80000    # 400.0f

    mul-float/2addr v4, v11

    const v5, 0x44ebe000    # 1887.0f

    mul-float/2addr v5, v12

    const/high16 v6, 0x441d0000    # 628.0f

    mul-float/2addr v6, v11

    const v7, 0x452eb000    # 2795.0f

    mul-float/2addr v7, v12

    const v9, 0x44644000    # 913.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 980
    const v3, 0x45603000    # 3587.0f

    mul-float/2addr v3, v12

    const v4, 0x46a68000    # 21312.0f

    mul-float/2addr v4, v11

    const v5, 0x4587b000    # 4342.0f

    mul-float/2addr v5, v12

    const v6, 0x46a6f400    # 21370.0f

    mul-float/2addr v6, v11

    const v7, 0x459e2000    # 5060.0f

    mul-float/2addr v7, v12

    const v9, 0x46a8ba00    # 21597.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 983
    const v3, 0x44fea000    # 2037.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    mul-float/2addr v4, v11

    const v5, 0x45207000    # 2567.0f

    mul-float/2addr v5, v12

    const/high16 v6, -0x3c9d0000    # -227.0f

    mul-float/2addr v6, v11

    const v7, 0x45417000    # 3095.0f

    mul-float/2addr v7, v12

    const v9, -0x3c718000    # -285.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 986
    const v3, 0x46084800    # 8722.0f

    mul-float/2addr v3, v12

    const v4, 0x46a59a00    # 21197.0f

    mul-float/2addr v4, v11

    const v5, 0x4611b400    # 9325.0f

    mul-float/2addr v5, v12

    const v6, 0x46a3d400    # 20970.0f

    mul-float/2addr v6, v11

    const v7, 0x4619fc00    # 9855.0f

    mul-float/2addr v7, v12

    const v9, 0x46a28000    # 20800.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 989
    const/high16 v3, 0x43f50000    # 490.0f

    mul-float/2addr v3, v12

    const/high16 v4, -0x3c9c0000    # -228.0f

    mul-float/2addr v4, v11

    const v5, 0x446c4000    # 945.0f

    mul-float/2addr v5, v12

    const/high16 v6, -0x3c380000    # -400.0f

    mul-float/2addr v6, v11

    const/high16 v7, 0x44b80000    # 1472.0f

    mul-float/2addr v7, v12

    const/high16 v9, -0x3bc70000    # -740.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 992
    const v3, 0x4638a400    # 11817.0f

    mul-float/2addr v3, v12

    const v4, 0x469b5e00    # 19887.0f

    mul-float/2addr v4, v11

    const v5, 0x4640ec00    # 12347.0f

    mul-float/2addr v5, v12

    const v6, 0x46999800    # 19660.0f

    mul-float/2addr v6, v11

    const v7, 0x46492c00    # 12875.0f

    mul-float/2addr v7, v12

    const v9, 0x46975e00    # 19375.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 995
    const v3, 0x440dc000    # 567.0f

    mul-float/2addr v3, v12

    const/high16 v4, -0x3c9c0000    # -228.0f

    mul-float/2addr v4, v11

    const v5, 0x4488e000    # 1095.0f

    mul-float/2addr v5, v12

    const v6, -0x3bffc000    # -513.0f

    mul-float/2addr v6, v11

    const v7, 0x44d48000    # 1700.0f

    mul-float/2addr v7, v12

    const/high16 v9, -0x3bc70000    # -740.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 998
    const v3, 0x466d2400    # 15177.0f

    mul-float/2addr v3, v12

    const v4, 0x46903c00    # 18462.0f

    mul-float/2addr v4, v11

    const v5, 0x46769800    # 15782.0f

    mul-float/2addr v5, v12

    const v6, 0x468d9400    # 18122.0f

    mul-float/2addr v6, v11

    const v7, 0x46813200    # 16537.0f

    mul-float/2addr v7, v12

    const v9, 0x468c3c00    # 17950.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 1001
    const v3, 0x44338000    # 718.0f

    mul-float/2addr v3, v12

    const/high16 v4, -0x3d1e0000    # -113.0f

    mul-float/2addr v4, v11

    const v5, 0x44aec000    # 1398.0f

    mul-float/2addr v5, v12

    const/high16 v6, -0x3c390000    # -398.0f

    mul-float/2addr v6, v11

    const v7, 0x450b4000    # 2228.0f

    mul-float/2addr v7, v12

    const v9, -0x3bffc000    # -513.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 1004
    const v3, 0x46996600    # 19635.0f

    mul-float/2addr v3, v12

    const v4, 0x46883a00    # 17437.0f

    mul-float/2addr v4, v11

    const v5, 0x46a0c200    # 20577.0f

    mul-float/2addr v5, v12

    const v6, 0x46875400    # 17322.0f

    mul-float/2addr v6, v11

    const v7, 0x46a8ba00    # 21597.0f

    mul-float/2addr v7, v12

    const v9, 0x46875400    # 17322.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 1007
    const v3, 0x46a8ba00    # 21597.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1008
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1009
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 1011
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_58

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_58

    .line 1012
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1013
    :cond_58
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_59

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_59

    .line 1014
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1018
    :cond_59
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 1019
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 1020
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 1021
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46729400    # 15525.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 1024
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_5a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "FlowChartMultidocument"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1025
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 1026
    .restart local v2    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    mul-float/2addr v3, v12

    const v4, 0x469fe200    # 20465.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1027
    const v3, 0x444a8000    # 810.0f

    mul-float/2addr v3, v12

    const v4, 0x439e8000    # 317.0f

    mul-float/2addr v4, v11

    const v5, 0x44ca8000    # 1620.0f

    mul-float/2addr v5, v12

    const/high16 v6, 0x43e20000    # 452.0f

    mul-float/2addr v6, v11

    const v7, 0x4515d000    # 2397.0f

    mul-float/2addr v7, v12

    const v9, 0x44354000    # 725.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 1030
    const v3, 0x45405000    # 3077.0f

    mul-float/2addr v3, v12

    const v4, 0x46a69a00    # 21325.0f

    mul-float/2addr v4, v11

    const v5, 0x456ce000    # 3790.0f

    mul-float/2addr v5, v12

    const v6, 0x46a75200    # 21417.0f

    mul-float/2addr v6, v11

    const v7, 0x4589a800    # 4405.0f

    mul-float/2addr v7, v12

    const v9, 0x46a8ba00    # 21597.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 1033
    const v3, 0x44ca8000    # 1620.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    mul-float/2addr v4, v11

    const v5, 0x4509a000    # 2202.0f

    mul-float/2addr v5, v12

    const/high16 v6, -0x3ccc0000    # -180.0f

    mul-float/2addr v6, v11

    const v7, 0x45261000    # 2657.0f

    mul-float/2addr v7, v12

    const/high16 v9, -0x3c780000    # -272.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 1036
    const v3, 0x45ece000    # 7580.0f

    mul-float/2addr v3, v12

    const v4, 0x46a64000    # 21280.0f

    mul-float/2addr v4, v11

    const v5, 0x45fa1000    # 8002.0f

    mul-float/2addr v5, v12

    const v6, 0x46a42400    # 21010.0f

    mul-float/2addr v6, v11

    const v7, 0x46041c00    # 8455.0f

    mul-float/2addr v7, v12

    const v9, 0x46a36a00    # 20917.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 1039
    const/high16 v3, 0x43d30000    # 422.0f

    mul-float/2addr v3, v12

    const/high16 v4, -0x3cf90000    # -135.0f

    mul-float/2addr v4, v11

    const v5, 0x444a8000    # 810.0f

    mul-float/2addr v5, v12

    const v6, -0x3c358000    # -405.0f

    mul-float/2addr v6, v11

    const v7, 0x44a5e000    # 1327.0f

    mul-float/2addr v7, v12

    const v9, -0x3bf88000    # -542.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 1042
    const v3, 0x461f7400    # 10205.0f

    mul-float/2addr v3, v12

    const v4, 0x469d6c00    # 20150.0f

    mul-float/2addr v4, v11

    const v5, 0x46268400    # 10657.0f

    mul-float/2addr v5, v12

    const v6, 0x469bfe00    # 19967.0f

    mul-float/2addr v6, v11

    const v7, 0x462d2000    # 11080.0f

    mul-float/2addr v7, v12

    const v9, 0x469a3c00    # 19742.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 1045
    const v3, 0x44014000    # 517.0f

    mul-float/2addr v3, v12

    const/high16 v4, -0x3cca0000    # -182.0f

    mul-float/2addr v4, v11

    const v5, 0x44728000    # 970.0f

    mul-float/2addr v5, v12

    const v6, -0x3c348000    # -407.0f

    mul-float/2addr v6, v11

    const v7, 0x44b22000    # 1425.0f

    mul-float/2addr v7, v12

    const v9, -0x3bec8000    # -590.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 1048
    const v3, 0x464c7c00    # 13087.0f

    mul-float/2addr v3, v12

    const v4, 0x46949200    # 19017.0f

    mul-float/2addr v4, v11

    const v5, 0x46549400    # 13605.0f

    mul-float/2addr v5, v12

    const v6, 0x46927200    # 18745.0f

    mul-float/2addr v6, v11

    const v7, 0x465ebc00    # 14255.0f

    mul-float/2addr v7, v12

    const v9, 0x46916400    # 18610.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 1051
    const v3, 0x4419c000    # 615.0f

    mul-float/2addr v3, v12

    const/high16 v4, -0x3ccc0000    # -180.0f

    mul-float/2addr v4, v11

    const v5, 0x449dc000    # 1262.0f

    mul-float/2addr v5, v12

    const/high16 v6, -0x3c610000    # -318.0f

    mul-float/2addr v6, v11

    const v7, 0x44f2c000    # 1942.0f

    mul-float/2addr v7, v12

    const/high16 v9, -0x3c340000    # -408.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 1054
    const v3, 0x46849e00    # 16975.0f

    mul-float/2addr v3, v12

    const v4, 0x468e3400    # 18202.0f

    mul-float/2addr v4, v11

    const v5, 0x468af200    # 17785.0f

    mul-float/2addr v5, v12

    const v6, 0x468ccc00    # 18022.0f

    mul-float/2addr v6, v11

    const v7, 0x46914600    # 18595.0f

    mul-float/2addr v7, v12

    const v9, 0x468ccc00    # 18022.0f

    mul-float v8, v9, v11

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 1057
    const/4 v3, 0x0

    const v4, -0x3b2f4000    # -1670.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 1058
    const v3, 0x4695f000    # 19192.0f

    mul-float/2addr v3, v12

    const v4, 0x467df000    # 16252.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1059
    const/high16 v3, 0x444a0000    # 808.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 1060
    const v3, 0x469c4000    # 20000.0f

    mul-float/2addr v3, v12

    const v4, 0x46620c00    # 14467.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1061
    const v3, 0x44348000    # 722.0f

    mul-float/2addr v3, v12

    const/high16 v4, -0x3d6a0000    # -75.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 1062
    const v3, 0x46a8ba00    # 21597.0f

    mul-float/2addr v3, v12

    const v4, 0x4660e000    # 14392.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1063
    const v3, 0x46a8ba00    # 21597.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1064
    const v3, 0x4539c000    # 2972.0f

    mul-float/2addr v3, v12

    const/4 v4, 0x0

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1065
    const/4 v3, 0x0

    mul-float/2addr v3, v12

    const v4, 0x44e2e000    # 1815.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 1066
    const v3, 0x44bf8000    # 1532.0f

    mul-float/2addr v3, v12

    const v4, 0x44e2e000    # 1815.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1067
    const/4 v3, 0x0

    mul-float/2addr v3, v12

    const v4, 0x44e88000    # 1860.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 1068
    const/4 v3, 0x0

    mul-float/2addr v3, v12

    const v4, 0x4565b000    # 3675.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1069
    const/4 v3, 0x0

    mul-float/2addr v3, v12

    const v4, 0x469fe200    # 20465.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1070
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 1071
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_5b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_5b

    .line 1072
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1073
    :cond_5b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_5c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_5c

    .line 1074
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1078
    :cond_5c
    const v3, 0x44bf8000    # 1532.0f

    mul-float/2addr v3, v12

    const v4, 0x4565b000    # 3675.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1079
    const v3, 0x46914600    # 18595.0f

    mul-float/2addr v3, v12

    const v4, 0x4565b000    # 3675.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1080
    const/4 v3, 0x0

    mul-float/2addr v3, v12

    const v4, 0x46461400    # 12677.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 1081
    const v3, 0x4539c000    # 2972.0f

    mul-float/2addr v3, v12

    const v4, 0x44e2e000    # 1815.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1082
    const v3, 0x469c4000    # 20000.0f

    mul-float/2addr v3, v12

    const v4, 0x44e2e000    # 1815.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1083
    const/4 v3, 0x0

    mul-float/2addr v3, v12

    const v4, 0x4645b000    # 12652.0f

    mul-float/2addr v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 1085
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_5d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_5d

    .line 1086
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1090
    :cond_5d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 1091
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x4593a800    # 4725.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 1092
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46891c00    # 17550.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 1093
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x468e6200    # 18225.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 1117
    .end local v2    # "path":Landroid/graphics/Path;
    .restart local v8    # "mtx":Landroid/graphics/Matrix;
    .restart local v16    # "rotatedBMP":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v10

    .line 1118
    .local v10, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception while writing pictures to out file"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F

    .prologue
    .line 81
    invoke-super {p0, p3, p4, p7, p8}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IFF)V

    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mDrawOnCanvas:Z

    .line 83
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mLeftMarg:I

    .line 84
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mTopMarg:I

    .line 85
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 86
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 87
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mDrawOnCanvas:Z

    .line 89
    return-void
.end method

.method public drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p2, "shapeCount"    # I

    .prologue
    .line 72
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V

    .line 74
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->draw(I)V

    .line 75
    return-void
.end method

.method public drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 147
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;I)V

    .line 148
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mDrawOnCanvas:Z

    .line 149
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mLeftMarg:I

    .line 150
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mTopMarg:I

    .line 151
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    .line 152
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    .line 153
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 154
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 155
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 156
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mDrawOnCanvas:Z

    .line 157
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V
    .locals 0
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 40
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 41
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->draw(I)V

    .line 42
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V
    .locals 2
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I

    .prologue
    .line 46
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mDrawOnCanvas:Z

    .line 48
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mLeftMarg:I

    .line 49
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mTopMarg:I

    .line 50
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 51
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 52
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mDrawOnCanvas:Z

    .line 54
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 102
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mDrawOnCanvas:Z

    .line 104
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mLeftMarg:I

    .line 105
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mTopMarg:I

    .line 106
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 107
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 108
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 109
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mDrawOnCanvas:Z

    .line 110
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 114
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V

    .line 115
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mDrawOnCanvas:Z

    .line 116
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mLeftMarg:I

    .line 117
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mTopMarg:I

    .line 118
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 119
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 120
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 121
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mDrawOnCanvas:Z

    .line 122
    return-void
.end method

.method public drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .prologue
    .line 96
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 97
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->draw(I)V

    .line 98
    return-void
.end method

.method public drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 59
    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p7

    move v4, p8

    move-object v5, p9

    invoke-super/range {v0 .. v5}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSSExcelShape(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mDrawOnCanvas:Z

    .line 62
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mLeftMarg:I

    .line 63
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mTopMarg:I

    .line 64
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 65
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 66
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mDrawOnCanvas:Z

    .line 68
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;I)V
    .locals 1
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 125
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 126
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->draw(I)V

    .line 127
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 131
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 132
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mDrawOnCanvas:Z

    .line 133
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mLeftMarg:I

    .line 134
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mTopMarg:I

    .line 135
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->width:F

    .line 136
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->height:F

    .line 137
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 138
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 139
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 140
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->mDrawOnCanvas:Z

    .line 141
    return-void
.end method
