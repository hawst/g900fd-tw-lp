.class public Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;
.super Lorg/apache/poi/POIXMLDocumentPart;
.source "XSSFChart.java"


# instance fields
.field private bubbleSz:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private charstr:Ljava/lang/String;

.field private chartType:I

.field private colorList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private curSeries:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

.field private exFval:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private exchartname:Ljava/lang/String;

.field private exnumval:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private exstrval:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private grouping:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

.field private height:F

.field private lumModList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mcontext:Landroid/content/Context;

.field private numcnt:Ljava/lang/String;

.field private orderValList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private parsed:Z

.field private scatterStyle:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

.field private seriesLst:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;",
            ">;"
        }
    .end annotation
.end field

.field private shapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

.field private strcnt:Ljava/lang/String;

.field private styles:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;

.field private txnumval:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private txstrval:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private valX:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private valXstr:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private valY:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private width:F


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0}, Lorg/apache/poi/POIXMLDocumentPart;-><init>()V

    .line 49
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->charstr:Ljava/lang/String;

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->exstrval:Ljava/util/ArrayList;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->exnumval:Ljava/util/ArrayList;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->txstrval:Ljava/util/ArrayList;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->txnumval:Ljava/util/ArrayList;

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->valX:Ljava/util/ArrayList;

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->valY:Ljava/util/ArrayList;

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->valXstr:Ljava/util/ArrayList;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->exFval:Ljava/util/ArrayList;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->colorList:Ljava/util/ArrayList;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->lumModList:Ljava/util/ArrayList;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->bubbleSz:Ljava/util/ArrayList;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->orderValList:Ljava/util/ArrayList;

    .line 65
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->exchartname:Ljava/lang/String;

    .line 66
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->strcnt:Ljava/lang/String;

    .line 67
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->numcnt:Ljava/lang/String;

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->parsed:Z

    .line 99
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->init()V

    .line 101
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->createChart()V

    .line 102
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V
    .locals 1
    .param p1, "part"    # Lorg/apache/poi/openxml4j/opc/PackagePart;
    .param p2, "rel"    # Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 127
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/POIXMLDocumentPart;-><init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V

    .line 49
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->charstr:Ljava/lang/String;

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->exstrval:Ljava/util/ArrayList;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->exnumval:Ljava/util/ArrayList;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->txstrval:Ljava/util/ArrayList;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->txnumval:Ljava/util/ArrayList;

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->valX:Ljava/util/ArrayList;

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->valY:Ljava/util/ArrayList;

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->valXstr:Ljava/util/ArrayList;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->exFval:Ljava/util/ArrayList;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->colorList:Ljava/util/ArrayList;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->lumModList:Ljava/util/ArrayList;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->bubbleSz:Ljava/util/ArrayList;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->orderValList:Ljava/util/ArrayList;

    .line 65
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->exchartname:Ljava/lang/String;

    .line 66
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->strcnt:Ljava/lang/String;

    .line 67
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->numcnt:Ljava/lang/String;

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->parsed:Z

    .line 128
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->init()V

    .line 132
    return-void
.end method

.method private createChart()V
    .locals 0

    .prologue
    .line 402
    return-void
.end method

.method private init()V
    .locals 1

    .prologue
    .line 135
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->seriesLst:Ljava/util/ArrayList;

    .line 136
    return-void
.end method


# virtual methods
.method public addBubbleSz(Ljava/lang/String;)V
    .locals 1
    .param p1, "buSz"    # Ljava/lang/String;

    .prologue
    .line 321
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->bubbleSz:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 322
    return-void
.end method

.method public addChartCatNumVal(Ljava/lang/String;)V
    .locals 1
    .param p1, "exnumval"    # Ljava/lang/String;

    .prologue
    .line 249
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->exnumval:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 250
    return-void
.end method

.method public addChartCatStrVal(Ljava/lang/String;)V
    .locals 1
    .param p1, "exstrval"    # Ljava/lang/String;

    .prologue
    .line 233
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->exstrval:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 234
    return-void
.end method

.method public addChartTxNumVal(Ljava/lang/String;)V
    .locals 1
    .param p1, "txnumval"    # Ljava/lang/String;

    .prologue
    .line 257
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->txnumval:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 258
    return-void
.end method

.method public addChartTxStrVal(Ljava/lang/String;)V
    .locals 1
    .param p1, "txstrval"    # Ljava/lang/String;

    .prologue
    .line 241
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->txstrval:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 242
    return-void
.end method

.method public addChartXStrVal(Ljava/lang/String;)V
    .locals 1
    .param p1, "xVal"    # Ljava/lang/String;

    .prologue
    .line 313
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->valXstr:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 314
    return-void
.end method

.method public addChartXVal(Ljava/lang/String;)V
    .locals 1
    .param p1, "xVal"    # Ljava/lang/String;

    .prologue
    .line 297
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->valX:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 298
    return-void
.end method

.method public addChartYVal(Ljava/lang/String;)V
    .locals 1
    .param p1, "yVal"    # Ljava/lang/String;

    .prologue
    .line 305
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->valY:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 306
    return-void
.end method

.method public addColorVal(Ljava/lang/String;)V
    .locals 1
    .param p1, "colorVal"    # Ljava/lang/String;

    .prologue
    .line 345
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->colorList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 346
    return-void
.end method

.method public addExChartFVal(Ljava/lang/String;)V
    .locals 1
    .param p1, "exfval"    # Ljava/lang/String;

    .prologue
    .line 265
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->exFval:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 266
    return-void
.end method

.method public addExChartName(Ljava/lang/String;)V
    .locals 0
    .param p1, "exval"    # Ljava/lang/String;

    .prologue
    .line 289
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->exchartname:Ljava/lang/String;

    .line 290
    return-void
.end method

.method public addExChartNumval(Ljava/lang/String;)V
    .locals 0
    .param p1, "exfval"    # Ljava/lang/String;

    .prologue
    .line 281
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->numcnt:Ljava/lang/String;

    .line 282
    return-void
.end method

.method public addExChartStrnum(Ljava/lang/String;)V
    .locals 0
    .param p1, "exfval"    # Ljava/lang/String;

    .prologue
    .line 273
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->strcnt:Ljava/lang/String;

    .line 274
    return-void
.end method

.method public addLumMod(Ljava/lang/String;)V
    .locals 1
    .param p1, "lumModVal"    # Ljava/lang/String;

    .prologue
    .line 353
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->lumModList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 354
    return-void
.end method

.method public addOrderVal(Ljava/lang/String;)V
    .locals 1
    .param p1, "orderVal"    # Ljava/lang/String;

    .prologue
    .line 329
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->orderValList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 330
    return-void
.end method

.method public addSeries(Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;)V
    .locals 1
    .param p1, "series"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    .prologue
    .line 204
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->curSeries:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    .line 205
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->seriesLst:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 206
    return-void
.end method

.method public getBubbleSz()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 317
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->bubbleSz:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getChartCatNumVal()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 245
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->exnumval:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getChartCatStrVal()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 229
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->exstrval:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getChartTxNumVal()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 253
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->txnumval:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getChartTxStrVal()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 237
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->txstrval:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getChartType()I
    .locals 1

    .prologue
    .line 209
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->chartType:I

    return v0
.end method

.method public getColorVal()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 341
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->colorList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCurSeries()Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->curSeries:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    return-object v0
.end method

.method public getExChartFVal()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 261
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->exFval:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExChartName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->exchartname:Ljava/lang/String;

    return-object v0
.end method

.method public getExChartNumval()Ljava/lang/String;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->numcnt:Ljava/lang/String;

    return-object v0
.end method

.method public getExChartStrnum()Ljava/lang/String;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->strcnt:Ljava/lang/String;

    return-object v0
.end method

.method public getExChartVAl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->charstr:Ljava/lang/String;

    return-object v0
.end method

.method public getGrouping()Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->grouping:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

    return-object v0
.end method

.method public getHeight()F
    .locals 1

    .prologue
    .line 200
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->height:F

    return v0
.end method

.method public getLumMod()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 349
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->lumModList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getOrderVal()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->orderValList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getScatterStyle()Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->scatterStyle:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    return-object v0
.end method

.method public getSeries()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->seriesLst:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getShapeProps()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->shapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    return-object v0
.end method

.method public getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->styles:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;

    return-object v0
.end method

.method public getWidth()F
    .locals 1

    .prologue
    .line 192
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->width:F

    return v0
.end method

.method public getXStrVal()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 309
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->valXstr:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getXVal()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 293
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->valX:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getYVal()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 301
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->valY:Ljava/util/ArrayList;

    return-object v0
.end method

.method public parseChart()V
    .locals 7

    .prologue
    .line 155
    iget-boolean v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->parsed:Z

    if-eqz v3, :cond_1

    .line 177
    :cond_0
    :goto_0
    return-void

    .line 158
    :cond_1
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XPPTChartParser;

    invoke-direct {v0, p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XPPTChartParser;-><init>(Ljava/lang/Object;)V

    .line 160
    .local v0, "chartParser":Lcom/samsung/thumbnail/office/ooxml/pptx/XPPTChartParser;
    const/4 v2, 0x0

    .line 163
    .local v2, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 164
    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XPPTChartParser;->parse(Ljava/io/InputStream;)V

    .line 165
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->parsed:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    if-eqz v2, :cond_0

    .line 171
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 172
    :catch_0
    move-exception v1

    .line 173
    .local v1, "e":Ljava/io/IOException;
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 166
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 167
    .restart local v1    # "e":Ljava/io/IOException;
    :try_start_2
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 169
    if-eqz v2, :cond_0

    .line 171
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 172
    :catch_2
    move-exception v1

    .line 173
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 169
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_2

    .line 171
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 174
    :cond_2
    :goto_1
    throw v3

    .line 172
    :catch_3
    move-exception v1

    .line 173
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v4, "DocumentService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setChartType(I)V
    .locals 0
    .param p1, "chartType"    # I

    .prologue
    .line 213
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->chartType:I

    .line 214
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "mContex"    # Landroid/content/Context;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->mcontext:Landroid/content/Context;

    .line 140
    return-void
.end method

.method public setExChartVAl(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 225
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->charstr:Ljava/lang/String;

    .line 226
    return-void
.end method

.method public setGrouping(Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;)V
    .locals 0
    .param p1, "grouping"    # Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

    .prologue
    .line 180
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->grouping:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

    .line 181
    return-void
.end method

.method public setHeight(F)V
    .locals 0
    .param p1, "height"    # F

    .prologue
    .line 196
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->height:F

    .line 197
    return-void
.end method

.method public setScatterStyle(Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;)V
    .locals 0
    .param p1, "scatterStyle"    # Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    .prologue
    .line 357
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->scatterStyle:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    .line 358
    return-void
.end method

.method public setShapeProps(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 337
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->shapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 338
    return-void
.end method

.method public setWidth(F)V
    .locals 0
    .param p1, "width"    # F

    .prologue
    .line 188
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->width:F

    .line 189
    return-void
.end method
