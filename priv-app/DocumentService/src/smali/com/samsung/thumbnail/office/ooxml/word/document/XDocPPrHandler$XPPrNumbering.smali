.class Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrNumbering;
.super Ljava/lang/Object;
.source "XDocPPrHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/properties/IXDocPropery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "XPPrNumbering"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;


# direct methods
.method constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;)V
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrNumbering;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consumePropery(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty;)V
    .locals 2
    .param p1, "property"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty;

    .prologue
    .line 222
    instance-of v1, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 223
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;

    .line 224
    .local v0, "numbering":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrNumbering;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;

    iget-object v1, v1, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;->paraProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->setBullet()V

    .line 225
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrNumbering;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;

    iget-object v1, v1, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;->paraProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->setNumbering(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;)V

    .line 227
    .end local v0    # "numbering":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;
    :cond_0
    return-void
.end method
