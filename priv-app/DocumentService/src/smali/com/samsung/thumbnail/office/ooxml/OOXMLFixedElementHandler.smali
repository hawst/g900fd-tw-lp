.class public abstract Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
.source "OOXMLFixedElementHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler$IPositionOffsetObserver;
    }
.end annotation


# instance fields
.field private fixedElementName:Ljava/lang/String;

.field protected handlerMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1, "expectedNameSpaceId"    # I
    .param p2, "elementName"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;-><init>()V

    .line 25
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->fixedElementName:Ljava/lang/String;

    .line 26
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->expectedNameSpaceId:I

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handlerMap:Ljava/util/HashMap;

    .line 28
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "elementName"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->fixedElementName:Ljava/lang/String;

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->expectedNameSpaceId:I

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handlerMap:Ljava/util/HashMap;

    .line 22
    return-void
.end method


# virtual methods
.method public getElementName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->fixedElementName:Ljava/lang/String;

    return-object v0
.end method

.method public getExpectedNS(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 37
    const/4 v0, 0x0

    .line 38
    .local v0, "nameSpace":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    iget v1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->expectedNameSpaceId:I

    if-nez v1, :cond_0

    .line 39
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getMainNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v0

    .line 43
    :goto_0
    return-object v0

    .line 41
    :cond_0
    iget v1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->expectedNameSpaceId:I

    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v0

    goto :goto_0
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 52
    invoke-virtual {p0, p2, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 56
    .local v1, "elementName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handlerMap:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    .line 62
    .local v0, "eleHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {p1, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->pushHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 68
    invoke-virtual {v0, p1, v1, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 72
    :goto_0
    return-void

    .line 70
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->startUnknownElement()V

    goto :goto_0
.end method
