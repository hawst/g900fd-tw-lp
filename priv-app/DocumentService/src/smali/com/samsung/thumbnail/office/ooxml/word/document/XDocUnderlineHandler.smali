.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocUnderlineHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;
.source "XDocUnderlineHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocUnderlineHandler$IUnderlineObserver;
    }
.end annotation


# instance fields
.field private observer:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocUnderlineHandler$IUnderlineObserver;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocUnderlineHandler$IUnderlineObserver;)V
    .locals 1
    .param p1, "observer"    # Lcom/samsung/thumbnail/office/ooxml/word/document/XDocUnderlineHandler$IUnderlineObserver;

    .prologue
    .line 26
    const-string/jumbo v0, "u"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;-><init>(Ljava/lang/String;)V

    .line 27
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocUnderlineHandler;->observer:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocUnderlineHandler$IUnderlineObserver;

    .line 28
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 33
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 35
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;-><init>()V

    .line 37
    .local v1, "underline":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;
    const-string/jumbo v3, "val"

    invoke-virtual {p0, p3, v3, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocUnderlineHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    .line 39
    .local v2, "val":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 41
    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->setStyle(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;)V

    .line 46
    :cond_0
    const-string/jumbo v3, "color"

    invoke-virtual {p0, p3, v3, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocUnderlineHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    .line 47
    if-eqz v2, :cond_1

    .line 48
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 49
    .local v0, "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 50
    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->setColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    .line 52
    .end local v0    # "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    :cond_1
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocUnderlineHandler;->observer:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocUnderlineHandler$IUnderlineObserver;

    invoke-interface {v3, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocUnderlineHandler$IUnderlineObserver;->setUnderline(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;)V

    .line 53
    return-void
.end method
