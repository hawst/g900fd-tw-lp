.class public Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingTwoCellAnchorHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelDrawingTwoCellAnchorHandler.java"


# instance fields
.field private twoCellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 21
    const/16 v0, 0xc9

    const-string/jumbo v1, "twoCellAnchor"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 23
    return-void
.end method

.method private init()V
    .locals 8

    .prologue
    .line 26
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicFromRowColHandler;

    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingTwoCellAnchorHandler;->twoCellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;

    invoke-direct {v1, v6}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicFromRowColHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;)V

    .line 27
    .local v1, "fromhandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicFromRowColHandler;
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingTwoCellAnchorHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v7, "from"

    invoke-virtual {v6, v7, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicToRowColHandler;

    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingTwoCellAnchorHandler;->twoCellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;

    invoke-direct {v5, v6}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicToRowColHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;)V

    .line 29
    .local v5, "tohandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicToRowColHandler;
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingTwoCellAnchorHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v7, "to"

    invoke-virtual {v6, v7, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicHandler;

    invoke-direct {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicHandler;-><init>()V

    .line 31
    .local v3, "pichandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicHandler;
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingTwoCellAnchorHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v7, "pic"

    invoke-virtual {v6, v7, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingSPHandler;

    invoke-direct {v4}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingSPHandler;-><init>()V

    .line 33
    .local v4, "sphandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingSPHandler;
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingTwoCellAnchorHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v7, "sp"

    invoke-virtual {v6, v7, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingCxnSPHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingCxnSPHandler;-><init>()V

    .line 39
    .local v0, "cxnsphandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingCxnSPHandler;
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingTwoCellAnchorHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v7, "cxnSp"

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XSSFChartGraphicFrameHandler;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XSSFChartGraphicFrameHandler;-><init>()V

    .line 45
    .local v2, "graphicFrame":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XSSFChartGraphicFrameHandler;
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingTwoCellAnchorHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v7, "graphicFrame"

    invoke-virtual {v6, v7, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 51
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 53
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingTwoCellAnchorHandler;->twoCellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;

    .line 54
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingTwoCellAnchorHandler;->twoCellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->addCellAnchor(Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;)V

    .line 55
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingTwoCellAnchorHandler;->init()V

    .line 56
    return-void
.end method
