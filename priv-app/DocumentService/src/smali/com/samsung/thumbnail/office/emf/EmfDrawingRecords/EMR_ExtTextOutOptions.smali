.class public Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutOptions;
.super Ljava/lang/Object;
.source "EMR_ExtTextOutOptions.java"


# static fields
.field public static final ETO_CLIPPED:I = 0x4

.field public static final ETO_GLYPH_INDEX:I = 0x10

.field public static final ETO_IGNORELANGUAGE:I = 0x1000

.field public static final ETO_NO_RECT:I = 0x100

.field public static final ETO_NUMERICSLATIN:I = 0x800

.field public static final ETO_NUMERICSLOCAL:I = 0x400

.field public static final ETO_OPAQUE:I = 0x2

.field public static final ETO_PDY:I = 0x2000

.field public static final ETO_REVERSE_INDEX_MAP:I = 0x10000

.field public static final ETO_RTLREADING:I = 0x80

.field public static final ETO_SMALL_CHARS:I = 0x200


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    return-void
.end method
