.class public Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;
.super Ljava/lang/Object;
.source "EMFHeader.java"


# instance fields
.field private BoundsBottom:I

.field private BoundsLeft:I

.field private BoundsRight:I

.field private BoundsTop:I

.field private FrameBottom:I

.field private FrameLeft:I

.field private FrameRight:I

.field private FrameTop:I

.field private HeightDevMM:I

.field private HeightDevPixels:I

.field private NumOfHandles:I

.field private NumOfRecords:I

.field private NumPalEntries:I

.field private OffsOfDescrip:I

.field private RecordSize:I

.field private RecordType:I

.field private Reserved:I

.field private Signature:I

.field private Size:I

.field private SizeOfDescrip:I

.field private Version:I

.field private WidthDevMM:I

.field private WidthDevPixels:I

.field fileContent:[B

.field private offset:I


# direct methods
.method public constructor <init>([BI)V
    .locals 0
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->fileContent:[B

    .line 41
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 42
    return-void
.end method


# virtual methods
.method public getBoundsBottom()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->BoundsBottom:I

    return v0
.end method

.method public getBoundsLeft()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->BoundsLeft:I

    return v0
.end method

.method public getBoundsRight()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->BoundsRight:I

    return v0
.end method

.method public getBoundsTop()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->BoundsTop:I

    return v0
.end method

.method public getFrameBottom()I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->FrameBottom:I

    return v0
.end method

.method public getFrameLeft()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->FrameLeft:I

    return v0
.end method

.method public getFrameRight()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->FrameRight:I

    return v0
.end method

.method public getFrameTop()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->FrameTop:I

    return v0
.end method

.method public getHeightDevMM()I
    .locals 1

    .prologue
    .line 221
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->HeightDevMM:I

    return v0
.end method

.method public getHeightDevPixels()I
    .locals 1

    .prologue
    .line 205
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->HeightDevPixels:I

    return v0
.end method

.method public getNumOfHandles()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->NumOfHandles:I

    return v0
.end method

.method public getNumOfRecords()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->NumOfRecords:I

    return v0
.end method

.method public getNumPalEntries()I
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->NumPalEntries:I

    return v0
.end method

.method public getOffsOfDescrip()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->OffsOfDescrip:I

    return v0
.end method

.method public getRecordSize()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->RecordSize:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->RecordType:I

    return v0
.end method

.method public getReserved()I
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->Reserved:I

    return v0
.end method

.method public getSignature()I
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->Signature:I

    return v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 141
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->Size:I

    return v0
.end method

.method public getSizeOfDescrip()I
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->SizeOfDescrip:I

    return v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->Version:I

    return v0
.end method

.method public getWidthDevMM()I
    .locals 1

    .prologue
    .line 213
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->WidthDevMM:I

    return v0
.end method

.method public getWidthDevPixels()I
    .locals 1

    .prologue
    .line 197
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->WidthDevPixels:I

    return v0
.end method

.method public readEmfHeader()V
    .locals 31

    .prologue
    .line 229
    const/16 v25, 0x0

    .line 236
    .local v25, "i":I
    const/16 v28, 0x4

    move/from16 v0, v28

    new-array v0, v0, [B

    move-object/from16 v26, v0

    .line 237
    .local v26, "intConvert":[B
    const/16 v28, 0x2

    move/from16 v0, v28

    new-array v0, v0, [B

    move-object/from16 v27, v0

    .line 239
    .local v27, "shortConvert":[B
    const/16 v25, 0x0

    :goto_0
    const/16 v28, 0x3

    move/from16 v0, v25

    move/from16 v1, v28

    if-gt v0, v1, :cond_0

    .line 240
    rsub-int/lit8 v28, v25, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->fileContent:[B

    move-object/from16 v29, v0

    aget-byte v29, v29, v25

    aput-byte v29, v26, v28

    .line 239
    add-int/lit8 v25, v25, 0x1

    goto :goto_0

    .line 241
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v28, v0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v17

    .line 242
    .local v17, "RecordType":I
    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 243
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->setRecordType(I)V

    .line 245
    const/16 v25, 0x0

    :goto_1
    const/16 v28, 0x3

    move/from16 v0, v25

    move/from16 v1, v28

    if-gt v0, v1, :cond_1

    .line 246
    rsub-int/lit8 v28, v25, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v25

    aget-byte v29, v29, v30

    aput-byte v29, v26, v28

    .line 245
    add-int/lit8 v25, v25, 0x1

    goto :goto_1

    .line 247
    :cond_1
    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v16

    .line 248
    .local v16, "RecordSize":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v25

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 249
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->setRecordSize(I)V

    .line 251
    const/16 v25, 0x0

    :goto_2
    const/16 v28, 0x3

    move/from16 v0, v25

    move/from16 v1, v28

    if-gt v0, v1, :cond_2

    .line 252
    rsub-int/lit8 v28, v25, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v25

    aget-byte v29, v29, v30

    aput-byte v29, v26, v28

    .line 251
    add-int/lit8 v25, v25, 0x1

    goto :goto_2

    .line 253
    :cond_2
    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v5

    .line 254
    .local v5, "Boundsleft":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v25

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 255
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->setBoundsLeft(I)V

    .line 257
    const/16 v25, 0x0

    :goto_3
    const/16 v28, 0x3

    move/from16 v0, v25

    move/from16 v1, v28

    if-gt v0, v1, :cond_3

    .line 258
    rsub-int/lit8 v28, v25, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v25

    aget-byte v29, v29, v30

    aput-byte v29, v26, v28

    .line 257
    add-int/lit8 v25, v25, 0x1

    goto :goto_3

    .line 259
    :cond_3
    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v3

    .line 260
    .local v3, "BoundsRight":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v25

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 261
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->setBoundsRight(I)V

    .line 263
    const/16 v25, 0x0

    :goto_4
    const/16 v28, 0x3

    move/from16 v0, v25

    move/from16 v1, v28

    if-gt v0, v1, :cond_4

    .line 264
    rsub-int/lit8 v28, v25, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v25

    aget-byte v29, v29, v30

    aput-byte v29, v26, v28

    .line 263
    add-int/lit8 v25, v25, 0x1

    goto :goto_4

    .line 265
    :cond_4
    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v4

    .line 266
    .local v4, "BoundsTop":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v25

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 267
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->setBoundsTop(I)V

    .line 269
    const/16 v25, 0x0

    :goto_5
    const/16 v28, 0x3

    move/from16 v0, v25

    move/from16 v1, v28

    if-gt v0, v1, :cond_5

    .line 270
    rsub-int/lit8 v28, v25, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v25

    aget-byte v29, v29, v30

    aput-byte v29, v26, v28

    .line 269
    add-int/lit8 v25, v25, 0x1

    goto :goto_5

    .line 271
    :cond_5
    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v2

    .line 272
    .local v2, "BoundsBottom":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v25

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 273
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->setBoundsBottom(I)V

    .line 275
    const/16 v25, 0x0

    :goto_6
    const/16 v28, 0x3

    move/from16 v0, v25

    move/from16 v1, v28

    if-gt v0, v1, :cond_6

    .line 276
    rsub-int/lit8 v28, v25, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v25

    aget-byte v29, v29, v30

    aput-byte v29, v26, v28

    .line 275
    add-int/lit8 v25, v25, 0x1

    goto :goto_6

    .line 277
    :cond_6
    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v7

    .line 278
    .local v7, "FrameLeft":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v25

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 279
    int-to-float v0, v7

    move/from16 v28, v0

    invoke-static/range {v28 .. v28}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v28

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->setFrameLeft(I)V

    .line 281
    const/16 v25, 0x0

    :goto_7
    const/16 v28, 0x3

    move/from16 v0, v25

    move/from16 v1, v28

    if-gt v0, v1, :cond_7

    .line 282
    rsub-int/lit8 v28, v25, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v25

    aget-byte v29, v29, v30

    aput-byte v29, v26, v28

    .line 281
    add-int/lit8 v25, v25, 0x1

    goto :goto_7

    .line 283
    :cond_7
    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v8

    .line 284
    .local v8, "FrameRight":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v25

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 285
    int-to-float v0, v8

    move/from16 v28, v0

    invoke-static/range {v28 .. v28}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v28

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->setFrameRight(I)V

    .line 287
    const/16 v25, 0x0

    :goto_8
    const/16 v28, 0x3

    move/from16 v0, v25

    move/from16 v1, v28

    if-gt v0, v1, :cond_8

    .line 288
    rsub-int/lit8 v28, v25, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v25

    aget-byte v29, v29, v30

    aput-byte v29, v26, v28

    .line 287
    add-int/lit8 v25, v25, 0x1

    goto :goto_8

    .line 289
    :cond_8
    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v9

    .line 290
    .local v9, "FrameTop":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v25

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 291
    int-to-float v0, v9

    move/from16 v28, v0

    invoke-static/range {v28 .. v28}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v28

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->setFrameTop(I)V

    .line 293
    const/16 v25, 0x0

    :goto_9
    const/16 v28, 0x3

    move/from16 v0, v25

    move/from16 v1, v28

    if-gt v0, v1, :cond_9

    .line 294
    rsub-int/lit8 v28, v25, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v25

    aget-byte v29, v29, v30

    aput-byte v29, v26, v28

    .line 293
    add-int/lit8 v25, v25, 0x1

    goto :goto_9

    .line 295
    :cond_9
    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v6

    .line 296
    .local v6, "FrameBottom":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v25

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 297
    int-to-float v0, v6

    move/from16 v28, v0

    invoke-static/range {v28 .. v28}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v28

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->setFrameBottom(I)V

    .line 299
    const/16 v25, 0x0

    :goto_a
    const/16 v28, 0x3

    move/from16 v0, v25

    move/from16 v1, v28

    if-gt v0, v1, :cond_a

    .line 300
    rsub-int/lit8 v28, v25, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v25

    aget-byte v29, v29, v30

    aput-byte v29, v26, v28

    .line 299
    add-int/lit8 v25, v25, 0x1

    goto :goto_a

    .line 301
    :cond_a
    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v19

    .line 302
    .local v19, "Signature":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v25

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 303
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->setSignature(I)V

    .line 305
    const/16 v25, 0x0

    :goto_b
    const/16 v28, 0x3

    move/from16 v0, v25

    move/from16 v1, v28

    if-gt v0, v1, :cond_b

    .line 306
    rsub-int/lit8 v28, v25, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v25

    aget-byte v29, v29, v30

    aput-byte v29, v26, v28

    .line 305
    add-int/lit8 v25, v25, 0x1

    goto :goto_b

    .line 307
    :cond_b
    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v22

    .line 308
    .local v22, "Version":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v25

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 309
    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->setVersion(I)V

    .line 311
    const/16 v25, 0x0

    :goto_c
    const/16 v28, 0x3

    move/from16 v0, v25

    move/from16 v1, v28

    if-gt v0, v1, :cond_c

    .line 312
    rsub-int/lit8 v28, v25, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v25

    aget-byte v29, v29, v30

    aput-byte v29, v26, v28

    .line 311
    add-int/lit8 v25, v25, 0x1

    goto :goto_c

    .line 313
    :cond_c
    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v20

    .line 314
    .local v20, "Size":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v25

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 315
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->setSize(I)V

    .line 317
    const/16 v25, 0x0

    :goto_d
    const/16 v28, 0x3

    move/from16 v0, v25

    move/from16 v1, v28

    if-gt v0, v1, :cond_d

    .line 318
    rsub-int/lit8 v28, v25, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v25

    aget-byte v29, v29, v30

    aput-byte v29, v26, v28

    .line 317
    add-int/lit8 v25, v25, 0x1

    goto :goto_d

    .line 319
    :cond_d
    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v13

    .line 320
    .local v13, "NumOfRecords":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v25

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 321
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->setNumOfRecords(I)V

    .line 323
    const/16 v25, 0x0

    :goto_e
    const/16 v28, 0x1

    move/from16 v0, v25

    move/from16 v1, v28

    if-gt v0, v1, :cond_e

    .line 324
    rsub-int/lit8 v28, v25, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v25

    aget-byte v29, v29, v30

    aput-byte v29, v27, v28

    .line 323
    add-int/lit8 v25, v25, 0x1

    goto :goto_e

    .line 325
    :cond_e
    const/16 v28, 0x0

    invoke-static/range {v27 .. v28}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getSmallInt([BI)I

    move-result v12

    .line 326
    .local v12, "NumOfHandles":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v25

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 327
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->setNumOfHandles(I)V

    .line 329
    const/16 v25, 0x0

    :goto_f
    const/16 v28, 0x1

    move/from16 v0, v25

    move/from16 v1, v28

    if-gt v0, v1, :cond_f

    .line 330
    rsub-int/lit8 v28, v25, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v25

    aget-byte v29, v29, v30

    aput-byte v29, v27, v28

    .line 329
    add-int/lit8 v25, v25, 0x1

    goto :goto_f

    .line 331
    :cond_f
    const/16 v28, 0x0

    invoke-static/range {v27 .. v28}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getSmallInt([BI)I

    move-result v18

    .line 332
    .local v18, "Reserved":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v25

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 333
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->setReserved(I)V

    .line 335
    const/16 v25, 0x0

    :goto_10
    const/16 v28, 0x3

    move/from16 v0, v25

    move/from16 v1, v28

    if-gt v0, v1, :cond_10

    .line 336
    rsub-int/lit8 v28, v25, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v25

    aget-byte v29, v29, v30

    aput-byte v29, v26, v28

    .line 335
    add-int/lit8 v25, v25, 0x1

    goto :goto_10

    .line 337
    :cond_10
    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v21

    .line 338
    .local v21, "SizeOfDescrip":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v25

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 339
    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->setSizeOfDescrip(I)V

    .line 341
    const/16 v25, 0x0

    :goto_11
    const/16 v28, 0x3

    move/from16 v0, v25

    move/from16 v1, v28

    if-gt v0, v1, :cond_11

    .line 342
    rsub-int/lit8 v28, v25, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v25

    aget-byte v29, v29, v30

    aput-byte v29, v26, v28

    .line 341
    add-int/lit8 v25, v25, 0x1

    goto :goto_11

    .line 343
    :cond_11
    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v15

    .line 344
    .local v15, "OffsOfDescrip":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v25

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 345
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->setOffsOfDescrip(I)V

    .line 347
    const/16 v25, 0x0

    :goto_12
    const/16 v28, 0x3

    move/from16 v0, v25

    move/from16 v1, v28

    if-gt v0, v1, :cond_12

    .line 348
    rsub-int/lit8 v28, v25, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v25

    aget-byte v29, v29, v30

    aput-byte v29, v26, v28

    .line 347
    add-int/lit8 v25, v25, 0x1

    goto :goto_12

    .line 349
    :cond_12
    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v14

    .line 350
    .local v14, "NumPalEntries":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v25

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 351
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->setNumPalEntries(I)V

    .line 353
    const/16 v25, 0x0

    :goto_13
    const/16 v28, 0x3

    move/from16 v0, v25

    move/from16 v1, v28

    if-gt v0, v1, :cond_13

    .line 354
    rsub-int/lit8 v28, v25, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v25

    aget-byte v29, v29, v30

    aput-byte v29, v26, v28

    .line 353
    add-int/lit8 v25, v25, 0x1

    goto :goto_13

    .line 355
    :cond_13
    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v24

    .line 356
    .local v24, "WidthDevPixels":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v25

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 357
    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v28, v0

    invoke-static/range {v28 .. v28}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v28

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->setWidthDevPixels(I)V

    .line 359
    const/16 v25, 0x0

    :goto_14
    const/16 v28, 0x3

    move/from16 v0, v25

    move/from16 v1, v28

    if-gt v0, v1, :cond_14

    .line 360
    rsub-int/lit8 v28, v25, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v25

    aget-byte v29, v29, v30

    aput-byte v29, v26, v28

    .line 359
    add-int/lit8 v25, v25, 0x1

    goto :goto_14

    .line 361
    :cond_14
    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v11

    .line 362
    .local v11, "HeightDevPixels":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v25

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 363
    int-to-float v0, v11

    move/from16 v28, v0

    invoke-static/range {v28 .. v28}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v28

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->setHeightDevPixels(I)V

    .line 365
    const/16 v25, 0x0

    :goto_15
    const/16 v28, 0x3

    move/from16 v0, v25

    move/from16 v1, v28

    if-gt v0, v1, :cond_15

    .line 366
    rsub-int/lit8 v28, v25, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v25

    aget-byte v29, v29, v30

    aput-byte v29, v26, v28

    .line 365
    add-int/lit8 v25, v25, 0x1

    goto :goto_15

    .line 367
    :cond_15
    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v23

    .line 368
    .local v23, "WidthDevMM":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v25

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 369
    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v28, v0

    invoke-static/range {v28 .. v28}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v28

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->setWidthDevMM(I)V

    .line 371
    const/16 v25, 0x0

    :goto_16
    const/16 v28, 0x3

    move/from16 v0, v25

    move/from16 v1, v28

    if-gt v0, v1, :cond_16

    .line 372
    rsub-int/lit8 v28, v25, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v25

    aget-byte v29, v29, v30

    aput-byte v29, v26, v28

    .line 371
    add-int/lit8 v25, v25, 0x1

    goto :goto_16

    .line 373
    :cond_16
    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v10

    .line 374
    .local v10, "HeightDevMM":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v25

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 375
    int-to-float v0, v10

    move/from16 v28, v0

    invoke-static/range {v28 .. v28}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v28

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->setHeightDevMM(I)V

    .line 377
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->getRecordSize()I

    move-result v28

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->offset:I

    .line 379
    return-void
.end method

.method public setBoundsBottom(I)V
    .locals 0
    .param p1, "BoundBottom"    # I

    .prologue
    .line 89
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->BoundsBottom:I

    .line 90
    return-void
.end method

.method public setBoundsLeft(I)V
    .locals 0
    .param p1, "BoundLeft"    # I

    .prologue
    .line 65
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->BoundsLeft:I

    .line 66
    return-void
.end method

.method public setBoundsRight(I)V
    .locals 0
    .param p1, "BoundRight"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->BoundsRight:I

    .line 74
    return-void
.end method

.method public setBoundsTop(I)V
    .locals 0
    .param p1, "BoundTop"    # I

    .prologue
    .line 81
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->BoundsTop:I

    .line 82
    return-void
.end method

.method public setFrameBottom(I)V
    .locals 0
    .param p1, "FramBottom"    # I

    .prologue
    .line 121
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->FrameBottom:I

    .line 122
    return-void
.end method

.method public setFrameLeft(I)V
    .locals 0
    .param p1, "FramLeft"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->FrameLeft:I

    .line 98
    return-void
.end method

.method public setFrameRight(I)V
    .locals 0
    .param p1, "FramRight"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->FrameRight:I

    .line 106
    return-void
.end method

.method public setFrameTop(I)V
    .locals 0
    .param p1, "FramTop"    # I

    .prologue
    .line 113
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->FrameTop:I

    .line 114
    return-void
.end method

.method public setHeightDevMM(I)V
    .locals 0
    .param p1, "HeightDvMM"    # I

    .prologue
    .line 225
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->HeightDevMM:I

    .line 226
    return-void
.end method

.method public setHeightDevPixels(I)V
    .locals 0
    .param p1, "HeightDvPixels"    # I

    .prologue
    .line 209
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->HeightDevPixels:I

    .line 210
    return-void
.end method

.method public setNumOfHandles(I)V
    .locals 0
    .param p1, "NumOfHandls"    # I

    .prologue
    .line 161
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->NumOfHandles:I

    .line 162
    return-void
.end method

.method public setNumOfRecords(I)V
    .locals 0
    .param p1, "NumOfRecrds"    # I

    .prologue
    .line 153
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->NumOfRecords:I

    .line 154
    return-void
.end method

.method public setNumPalEntries(I)V
    .locals 0
    .param p1, "NmPalEntries"    # I

    .prologue
    .line 193
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->NumPalEntries:I

    .line 194
    return-void
.end method

.method public setOffsOfDescrip(I)V
    .locals 0
    .param p1, "OffOfDescrip"    # I

    .prologue
    .line 185
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->OffsOfDescrip:I

    .line 186
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "RecrdSize"    # I

    .prologue
    .line 57
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->RecordSize:I

    .line 58
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "RecrdType"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->RecordType:I

    .line 50
    return-void
.end method

.method public setReserved(I)V
    .locals 0
    .param p1, "Resrved"    # I

    .prologue
    .line 169
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->Reserved:I

    .line 170
    return-void
.end method

.method public setSignature(I)V
    .locals 0
    .param p1, "Signture"    # I

    .prologue
    .line 129
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->Signature:I

    .line 130
    return-void
.end method

.method public setSize(I)V
    .locals 0
    .param p1, "Sze"    # I

    .prologue
    .line 145
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->Size:I

    .line 146
    return-void
.end method

.method public setSizeOfDescrip(I)V
    .locals 0
    .param p1, "SizOfDescrip"    # I

    .prologue
    .line 177
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->SizeOfDescrip:I

    .line 178
    return-void
.end method

.method public setVersion(I)V
    .locals 0
    .param p1, "Vrsion"    # I

    .prologue
    .line 137
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->Version:I

    .line 138
    return-void
.end method

.method public setWidthDevMM(I)V
    .locals 0
    .param p1, "WidthDvMM"    # I

    .prologue
    .line 217
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->WidthDevMM:I

    .line 218
    return-void
.end method

.method public setWidthDevPixels(I)V
    .locals 0
    .param p1, "WidthDvPixels"    # I

    .prologue
    .line 201
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMFHeader;->WidthDevPixels:I

    .line 202
    return-void
.end method
