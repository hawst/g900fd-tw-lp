.class public Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;
.super Ljava/lang/Object;
.source "XDocTblProperties.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;
    }
.end annotation


# instance fields
.field private border:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

.field private borderProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

.field private gridSpan:I

.field private hMerge:Z

.field private jc:Ljava/lang/String;

.field private rowSpan:I

.field private shade:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

.field private style:Ljava/lang/String;

.field private tblCellMargins:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;",
            ">;"
        }
    .end annotation
.end field

.field private tblInd:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;

.field private vAlign:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

.field private vMerge:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

.field private width:I

.field private widthType:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

.field private xPos:Ljava/lang/String;

.field private yPos:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->gridSpan:I

    .line 29
    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->rowSpan:I

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->tblCellMargins:Ljava/util/ArrayList;

    .line 39
    return-void
.end method


# virtual methods
.method public addRowSpan()V
    .locals 1

    .prologue
    .line 145
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->rowSpan:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->rowSpan:I

    .line 146
    return-void
.end method

.method public getAlign()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->jc:Ljava/lang/String;

    return-object v0
.end method

.method public getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->borderProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    return-object v0
.end method

.method public getBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->border:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    return-object v0
.end method

.method public getCellMargin()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->tblCellMargins:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getGridSpan()I
    .locals 1

    .prologue
    .line 141
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->gridSpan:I

    return v0
.end method

.method public getHMerge()Z
    .locals 1

    .prologue
    .line 137
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->hMerge:Z

    return v0
.end method

.method public getRowSpan()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->rowSpan:I

    return v0
.end method

.method public getShade()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->shade:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    return-object v0
.end method

.method public getStyle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->style:Ljava/lang/String;

    return-object v0
.end method

.method public getTblInd()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->tblInd:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;

    return-object v0
.end method

.method public getVAlign()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->vAlign:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    return-object v0
.end method

.method public getVMerge()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->vMerge:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->width:I

    return v0
.end method

.method public getWidthType()Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->widthType:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    return-object v0
.end method

.method public getXPosition()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->xPos:Ljava/lang/String;

    return-object v0
.end method

.method public getYPosition()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->yPos:Ljava/lang/String;

    return-object v0
.end method

.method public setAlign(Ljava/lang/String;)V
    .locals 0
    .param p1, "jc"    # Ljava/lang/String;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->jc:Ljava/lang/String;

    .line 70
    return-void
.end method

.method public setBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V
    .locals 0
    .param p1, "border"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->border:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 126
    return-void
.end method

.method public setBorders(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V
    .locals 1
    .param p1, "borderType"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    .param p2, "border"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->borderProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->borderProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->borderProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->setBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    .line 54
    return-void
.end method

.method public setCellMargin(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;)V
    .locals 1
    .param p1, "tblCellMar"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->tblCellMargins:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 62
    return-void
.end method

.method public setGridSpan(I)V
    .locals 0
    .param p1, "gridSpan"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->gridSpan:I

    .line 86
    return-void
.end method

.method public setHMerge(Z)V
    .locals 0
    .param p1, "hMerge"    # Z

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->hMerge:Z

    .line 94
    return-void
.end method

.method public setShade(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;)V
    .locals 0
    .param p1, "shade"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->shade:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    .line 130
    return-void
.end method

.method public setStyle(Ljava/lang/String;)V
    .locals 0
    .param p1, "style"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->style:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public setTblInd(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;)V
    .locals 0
    .param p1, "tblInd"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->tblInd:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;

    .line 78
    return-void
.end method

.method public setVAlign(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;)V
    .locals 0
    .param p1, "vAlign"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->vAlign:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    .line 122
    return-void
.end method

.method public setVMerge(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;)V
    .locals 0
    .param p1, "vMerge"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->vMerge:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    .line 90
    return-void
.end method

.method public setWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 113
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->width:I

    .line 114
    return-void
.end method

.method public setWidthType(Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;)V
    .locals 0
    .param p1, "widthType"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->widthType:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    .line 118
    return-void
.end method

.method public setXPosition(Ljava/lang/String;)V
    .locals 0
    .param p1, "xPos"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->xPos:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public setYPosition(Ljava/lang/String;)V
    .locals 0
    .param p1, "yPos"    # Ljava/lang/String;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->yPos:Ljava/lang/String;

    .line 102
    return-void
.end method
