.class public Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;
.super Ljava/lang/Object;
.source "XSLFTableRow.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;",
        ">;"
    }
.end annotation


# instance fields
.field private cells:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;",
            ">;"
        }
    .end annotation
.end field

.field private height:J

.field private table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;)V
    .locals 1
    .param p1, "table"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;->table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;->cells:Ljava/util/ArrayList;

    .line 24
    return-void
.end method


# virtual methods
.method public addCell(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;)V
    .locals 1
    .param p1, "cell"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;->cells:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 28
    return-void
.end method

.method public getCells()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;->cells:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getHeight()D
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;->height:J

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->toPoints(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public getTable()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;->table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;->cells:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public setHeight(J)V
    .locals 1
    .param p1, "height"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;->height:J

    .line 44
    return-void
.end method
