.class public Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XDocAbstractNumHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$IXDocLvlConsumer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler$NumStyleLink;,
        Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler$MultiLvlType;
    }
.end annotation


# instance fields
.field private abstractNum:Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    const-string/jumbo v0, "abstractNum"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;->init()V

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;->abstractNum:Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;

    return-object v0
.end method

.method private init()V
    .locals 10

    .prologue
    .line 38
    const/4 v9, 0x3

    new-array v8, v9, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 40
    .local v8, "seqDes":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler$MultiLvlType;

    invoke-direct {v3, p0}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler$MultiLvlType;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;)V

    .line 41
    .local v3, "multiLvlType":Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler$MultiLvlType;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;

    const-string/jumbo v9, "multiLevelType"

    invoke-direct {v2, v9, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 43
    .local v2, "multiLvlHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v9, "multiLevelType"

    invoke-direct {v4, v9, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 45
    .local v4, "mutliSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v9, 0x0

    aput-object v4, v8, v9

    .line 47
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler$NumStyleLink;

    invoke-direct {v6, p0}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler$NumStyleLink;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;)V

    .line 48
    .local v6, "numStyleLink":Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler$NumStyleLink;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;

    const-string/jumbo v9, "numStyleLink"

    invoke-direct {v5, v9, v6}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 50
    .local v5, "numStyleHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v9, "numStyleLink"

    invoke-direct {v7, v9, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 52
    .local v7, "numStyleSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v9, 0x1

    aput-object v7, v8, v9

    .line 54
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;

    invoke-direct {v0, p0}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$IXDocLvlConsumer;)V

    .line 55
    .local v0, "lvlHandler":Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v9, "lvl"

    invoke-direct {v1, v9, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 57
    .local v1, "lvlSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v9, 0x2

    aput-object v1, v8, v9

    .line 59
    iput-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 60
    return-void
.end method


# virtual methods
.method public consumeLvl(Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;)V
    .locals 1
    .param p1, "numLvl"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;->abstractNum:Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;->addNumLvl(Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;)V

    .line 106
    return-void
.end method

.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 75
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 77
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;->abstractNum:Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;->addAbstractNum(Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;)I

    .line 78
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 65
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 66
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;->abstractNum:Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;

    .line 68
    const-string/jumbo v1, "abstractNumId"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 70
    .local v0, "val":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocAbstractNumHandler;->abstractNum:Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;->setAbstractNumId(I)V

    .line 71
    return-void
.end method
