.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableGridColHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;
.source "TableGridColHandler.java"


# instance fields
.field private table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;)V
    .locals 2
    .param p1, "table"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    .prologue
    .line 17
    const/16 v0, 0x1f

    const-string/jumbo v1, "gridCol"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;-><init>(ILjava/lang/String;)V

    .line 18
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableGridColHandler;->table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    .line 19
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 6
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 24
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 26
    const-string/jumbo v4, "w"

    invoke-virtual {p0, p3, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableGridColHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    .line 27
    .local v2, "width":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 28
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->toPoints(J)D

    move-result-wide v4

    double-to-long v0, v4

    .line 29
    .local v0, "cellWidth":J
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableGridColHandler;->table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    invoke-virtual {v4, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->addCellWidth(J)V

    .line 32
    .end local v0    # "cellWidth":J
    :cond_0
    const-string/jumbo v4, "type"

    invoke-virtual {p0, p3, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableGridColHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v3

    .line 33
    .local v3, "widthType":Ljava/lang/String;
    if-nez v3, :cond_1

    .line 34
    const-string/jumbo v3, "dxa"

    .line 36
    :cond_1
    return-void
.end method
