.class public Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/SchemeClrHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "SchemeClrHandler.java"


# instance fields
.field background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;)V
    .locals 2
    .param p1, "background"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    .prologue
    .line 16
    const/16 v0, 0x1f

    const-string/jumbo v1, "schemeClr"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 17
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/SchemeClrHandler;->background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    .line 18
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 23
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 24
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/SchemeClrHandler;->background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    const-string/jumbo v1, "val"

    invoke-virtual {p0, p3, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/SchemeClrHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->setBgRefClr(Ljava/lang/String;)V

    .line 25
    return-void
.end method
