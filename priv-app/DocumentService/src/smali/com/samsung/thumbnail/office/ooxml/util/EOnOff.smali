.class public final enum Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;
.super Ljava/lang/Enum;
.source "EOnOff.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

.field public static final enum FALSE:Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

.field public static final enum OFF:Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

.field public static final enum ON:Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

.field public static final enum ONE:Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

.field public static final enum TRUE:Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

.field public static final enum ZERO:Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 9
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    const-string/jumbo v1, "ON"

    const-string/jumbo v2, "on"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;->ON:Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    const-string/jumbo v1, "OFF"

    const-string/jumbo v2, "off"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;->OFF:Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    const-string/jumbo v1, "TRUE"

    const-string/jumbo v2, "true"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;->TRUE:Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    const-string/jumbo v1, "FALSE"

    const-string/jumbo v2, "false"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;->FALSE:Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    const-string/jumbo v1, "ONE"

    const-string/jumbo v2, "1"

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;->ONE:Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    const-string/jumbo v1, "ZERO"

    const/4 v2, 0x5

    const-string/jumbo v3, "0"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;->ZERO:Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    .line 7
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;->ON:Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;->OFF:Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;->TRUE:Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;->FALSE:Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;->ONE:Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;->ZERO:Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 14
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;->value:Ljava/lang/String;

    .line 15
    return-void
.end method

.method public static convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;->values()[Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 23
    .local v2, "inst":Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 27
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;
    :goto_1
    return-object v2

    .line 22
    .restart local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 27
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;

    return-object v0
.end method


# virtual methods
.method public xmlValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/util/EOnOff;->value:Ljava/lang/String;

    return-object v0
.end method
