.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "PresetGeometryHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GeometryGuideHandler$GeometryGuideObserver;


# instance fields
.field formulaNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field formulas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mPrstGeom:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IPrstGeomHandler;

.field private shapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;


# direct methods
.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 1
    .param p1, "nameSpaceId"    # I
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 38
    const-string/jumbo v0, "prstGeom"

    invoke-direct {p0, p1, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 39
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;->shapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 40
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;->init()V

    .line 41
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IPrstGeomHandler;)V
    .locals 2
    .param p1, "prstGeom"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IPrstGeomHandler;

    .prologue
    .line 32
    const/16 v0, 0x1f

    const-string/jumbo v1, "prstGeom"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 33
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;->mPrstGeom:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IPrstGeomHandler;

    .line 34
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;->init()V

    .line 35
    return-void
.end method

.method private init()V
    .locals 6

    .prologue
    .line 44
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;->formulaNames:Ljava/util/ArrayList;

    .line 45
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;->formulas:Ljava/util/ArrayList;

    .line 47
    const/4 v4, 0x1

    new-array v3, v4, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 49
    .local v3, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GeometryGuideHandler;

    invoke-direct {v1, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GeometryGuideHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GeometryGuideHandler$GeometryGuideObserver;)V

    .line 50
    .local v1, "guideHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GeometryGuideHandler;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/OOXMLArrayHandler;

    const/16 v4, 0x1f

    const-string/jumbo v5, "avLst"

    invoke-direct {v2, v4, v5, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLArrayHandler;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 53
    .local v2, "guideLst":Lcom/samsung/thumbnail/office/ooxml/OOXMLArrayHandler;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v4, "avLst"

    invoke-direct {v0, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 55
    .local v0, "avSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v4, 0x0

    aput-object v0, v3, v4

    .line 57
    iput-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 58
    return-void
.end method


# virtual methods
.method public addGd(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "formulaName"    # Ljava/lang/String;
    .param p2, "formula"    # Ljava/lang/String;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;->formulaNames:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;->formulas:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    return-void
.end method

.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 80
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;->shapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;->shapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;->formulaNames:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;->formulas:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setAdjustValues(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 84
    :cond_0
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 63
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 65
    const-string/jumbo v1, "prst"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 66
    .local v0, "val":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 67
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;->mPrstGeom:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IPrstGeomHandler;

    if-eqz v1, :cond_0

    .line 68
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;->mPrstGeom:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IPrstGeomHandler;

    invoke-interface {v1, p1, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IPrstGeomHandler;->setShapeName(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 71
    :cond_0
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;->shapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    if-eqz v1, :cond_1

    .line 72
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;->shapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setPrstShapeName(Ljava/lang/String;)V

    .line 73
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;->shapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setPrstName(Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;)V

    .line 76
    :cond_1
    return-void
.end method
