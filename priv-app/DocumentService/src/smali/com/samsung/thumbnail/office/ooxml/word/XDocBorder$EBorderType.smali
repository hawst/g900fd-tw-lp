.class public final enum Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
.super Ljava/lang/Enum;
.source "XDocBorder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EBorderType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

.field public static final enum BAR:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

.field public static final enum BDR:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

.field public static final enum BETWEEN:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

.field public static final enum BOTTOM:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

.field public static final enum INSIDE_H:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

.field public static final enum INSIDE_V:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

.field public static final enum LEFT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

.field public static final enum RIGHT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

.field public static final enum TL2BR:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

.field public static final enum TOP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

.field public static final enum TR2BL:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 153
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    const-string/jumbo v1, "TOP"

    const-string/jumbo v2, "top"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->TOP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    const-string/jumbo v1, "LEFT"

    const-string/jumbo v2, "left"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->LEFT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    const-string/jumbo v1, "RIGHT"

    const-string/jumbo v2, "right"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->RIGHT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    const-string/jumbo v1, "BOTTOM"

    const-string/jumbo v2, "bottom"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->BOTTOM:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    const-string/jumbo v1, "BETWEEN"

    const-string/jumbo v2, "between"

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->BETWEEN:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    .line 154
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    const-string/jumbo v1, "BAR"

    const/4 v2, 0x5

    const-string/jumbo v3, "bar"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->BAR:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    const-string/jumbo v1, "BDR"

    const/4 v2, 0x6

    const-string/jumbo v3, "bdr"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->BDR:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    .line 159
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    const-string/jumbo v1, "INSIDE_H"

    const/4 v2, 0x7

    const-string/jumbo v3, "insideH"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->INSIDE_H:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    const-string/jumbo v1, "INSIDE_V"

    const/16 v2, 0x8

    const-string/jumbo v3, "insideV"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->INSIDE_V:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    const-string/jumbo v1, "TL2BR"

    const/16 v2, 0x9

    const-string/jumbo v3, "tl2br"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->TL2BR:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    const-string/jumbo v1, "TR2BL"

    const/16 v2, 0xa

    const-string/jumbo v3, "tr2bl"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->TR2BL:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    .line 152
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->TOP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->LEFT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->RIGHT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->BOTTOM:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->BETWEEN:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->BAR:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->BDR:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->INSIDE_H:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->INSIDE_V:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->TL2BR:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->TR2BL:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 163
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 164
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->value:Ljava/lang/String;

    .line 165
    return-void
.end method

.method public static convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 172
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->values()[Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 173
    .local v2, "inst":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 177
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    :goto_1
    return-object v2

    .line 172
    .restart local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 177
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 152
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    .locals 1

    .prologue
    .line 152
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    return-object v0
.end method


# virtual methods
.method public xmlValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->value:Ljava/lang/String;

    return-object v0
.end method
