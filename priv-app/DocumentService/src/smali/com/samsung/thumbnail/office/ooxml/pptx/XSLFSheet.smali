.class public abstract Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;
.super Lorg/apache/poi/POIXMLDocumentPart;
.source "XSLFSheet.java"


# instance fields
.field protected background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

.field private context:Landroid/content/Context;

.field private groupShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

.field protected shapes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;",
            ">;"
        }
    .end annotation
.end field

.field private showMasterShape:Z

.field protected theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lorg/apache/poi/POIXMLDocumentPart;-><init>()V

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->showMasterShape:Z

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->shapes:Ljava/util/List;

    .line 48
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V
    .locals 1
    .param p1, "part"    # Lorg/apache/poi/openxml4j/opc/PackagePart;
    .param p2, "rel"    # Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/POIXMLDocumentPart;-><init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->showMasterShape:Z

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->shapes:Ljava/util/List;

    .line 53
    return-void
.end method


# virtual methods
.method public addShapes(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;)V
    .locals 1
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->groupShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->groupShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->addShapes(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;)V

    .line 89
    :goto_0
    return-void

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->shapes:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected buildShapes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 109
    .local v0, "shapes":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;>;"
    return-object v0
.end method

.method public getBackground()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->context:Landroid/content/Context;

    return-object v0
.end method

.method protected abstract getRootElementName()Ljava/lang/String;
.end method

.method public getShapeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->shapes:Ljava/util/List;

    if-nez v0, :cond_0

    .line 114
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->buildShapes()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->shapes:Ljava/util/List;

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->shapes:Ljava/util/List;

    return-object v0
.end method

.method public getShapes()[Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;
    .locals 2

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->getShapeList()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->shapes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;

    return-object v0
.end method

.method public getSlideShow()Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;
    .locals 2

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->getParent()Lorg/apache/poi/POIXMLDocumentPart;

    move-result-object v0

    .line 67
    .local v0, "part":Lorg/apache/poi/POIXMLDocumentPart;
    :goto_0
    instance-of v1, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;

    if-eqz v1, :cond_0

    .line 68
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;

    .end local v0    # "part":Lorg/apache/poi/POIXMLDocumentPart;
    return-object v0

    .line 70
    .restart local v0    # "part":Lorg/apache/poi/POIXMLDocumentPart;
    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/POIXMLDocumentPart;->getParent()Lorg/apache/poi/POIXMLDocumentPart;

    move-result-object v0

    goto :goto_0
.end method

.method public setBackground(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;)V
    .locals 0
    .param p1, "background"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    .line 93
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->context:Landroid/content/Context;

    .line 101
    return-void
.end method

.method public setGroupshape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;)V
    .locals 1
    .param p1, "groupShape"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    .prologue
    .line 77
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->groupShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->groupShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->processGroupShapes()V

    .line 80
    :cond_0
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->groupShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    .line 81
    return-void
.end method

.method public setShowMasterShape(Z)V
    .locals 0
    .param p1, "showMasterShape"    # Z

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->showMasterShape:Z

    .line 57
    return-void
.end method

.method public showMasterShape()Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->showMasterShape:Z

    return v0
.end method
