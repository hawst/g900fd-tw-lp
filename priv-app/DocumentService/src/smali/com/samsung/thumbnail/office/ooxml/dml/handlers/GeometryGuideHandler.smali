.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GeometryGuideHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;
.source "GeometryGuideHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GeometryGuideHandler$GeometryGuideObserver;
    }
.end annotation


# instance fields
.field private guideObserver:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GeometryGuideHandler$GeometryGuideObserver;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GeometryGuideHandler$GeometryGuideObserver;)V
    .locals 2
    .param p1, "guideObserver"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GeometryGuideHandler$GeometryGuideObserver;

    .prologue
    .line 15
    const/16 v0, 0x1f

    const-string/jumbo v1, "gd"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;-><init>(ILjava/lang/String;)V

    .line 16
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GeometryGuideHandler;->guideObserver:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GeometryGuideHandler$GeometryGuideObserver;

    .line 17
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 22
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 24
    const-string/jumbo v2, "name"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GeometryGuideHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 25
    .local v1, "formulaName":Ljava/lang/String;
    const-string/jumbo v2, "fmla"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GeometryGuideHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 26
    .local v0, "formula":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GeometryGuideHandler;->guideObserver:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GeometryGuideHandler$GeometryGuideObserver;

    invoke-interface {v2, v1, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GeometryGuideHandler$GeometryGuideObserver;->addGd(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    return-void
.end method
