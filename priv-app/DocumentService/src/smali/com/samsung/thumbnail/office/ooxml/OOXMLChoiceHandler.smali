.class public Lcom/samsung/thumbnail/office/ooxml/OOXMLChoiceHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "OOXMLChoiceHandler.java"


# instance fields
.field protected choiceHandlerMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1, "expectedNameSpaceId"    # I
    .param p2, "elementName"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLChoiceHandler;->choiceHandlerMap:Ljava/util/HashMap;

    .line 22
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "elementName"    # Ljava/lang/String;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 16
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLChoiceHandler;->choiceHandlerMap:Ljava/util/HashMap;

    .line 17
    return-void
.end method


# virtual methods
.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 6
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 28
    invoke-virtual {p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getNameSpace(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v2

    .line 30
    .local v2, "nameSpace":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    invoke-static {p2, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)Ljava/lang/String;

    move-result-object v3

    .line 33
    .local v3, "stripedElement":Ljava/lang/String;
    const/4 v1, 0x0

    .line 34
    .local v1, "handlerMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;>;"
    if-eqz v2, :cond_0

    .line 35
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLChoiceHandler;->choiceHandlerMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->getID()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "handlerMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;>;"
    check-cast v1, Ljava/util/HashMap;

    .line 37
    .restart local v1    # "handlerMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;>;"
    :cond_0
    if-eqz v1, :cond_1

    .line 38
    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    .line 39
    .local v0, "eleHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    if-eqz v0, :cond_1

    .line 40
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->getID()I

    move-result v4

    iput v4, v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->expectedNameSpaceId:I

    .line 41
    invoke-virtual {p1, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->pushHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 42
    invoke-virtual {v0, p1, v3, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 48
    .end local v0    # "eleHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    :goto_0
    return-void

    .line 47
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->startUnknownElement()V

    goto :goto_0
.end method
