.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ColorConsumer;
.super Ljava/lang/Object;
.source "TcTxStyleHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "ColorConsumer"
.end annotation


# instance fields
.field color:Ljava/lang/String;

.field fillConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ITcTxStylesFntClr;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ITcTxStylesFntClr;)V
    .locals 0
    .param p1, "fillConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ITcTxStylesFntClr;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ColorConsumer;->fillConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ITcTxStylesFntClr;

    .line 58
    return-void
.end method


# virtual methods
.method public consumeColor(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 0
    .param p1, "clrScheme"    # Ljava/lang/String;
    .param p2, "val"    # Ljava/lang/String;
    .param p3, "fillProp"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    .param p4, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 63
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ColorConsumer;->color:Ljava/lang/String;

    .line 64
    return-void
.end method
