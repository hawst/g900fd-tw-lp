.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "FillStyleLstHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;


# instance fields
.field private styleName:Ljava/lang/String;

.field private theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V
    .locals 7
    .param p1, "consumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;
    .param p2, "styleName"    # Ljava/lang/String;
    .param p3, "theme"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .prologue
    const/4 v6, 0x0

    .line 35
    invoke-direct {p0, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 37
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .line 38
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;->styleName:Ljava/lang/String;

    .line 40
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;

    invoke-direct {v2, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 41
    .local v2, "noFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "noFill"

    invoke-virtual {v4, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;

    invoke-direct {v3, p1, v6, p3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 45
    .local v3, "solidFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "solidFill"

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;

    invoke-direct {v1, p0, v6, p3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 48
    .local v1, "gradFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "gradFill"

    invoke-virtual {v4, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler;

    const/16 v4, 0x1f

    invoke-direct {v0, v4, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;)V

    .line 52
    .local v0, "blipFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "blipFill"

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    return-void
.end method


# virtual methods
.method public consumeGS(Ljava/lang/String;FLjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 11
    .param p1, "element"    # Ljava/lang/String;
    .param p2, "pos"    # F
    .param p3, "val"    # Ljava/lang/String;
    .param p4, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    const/4 v10, 0x0

    .line 59
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v1

    .line 61
    .local v1, "fillProperty":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 63
    .local v0, "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    if-nez v1, :cond_0

    .line 64
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .end local v1    # "fillProperty":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;-><init>()V

    .line 65
    .restart local v1    # "fillProperty":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-virtual {v9, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->consumeFill(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V

    .line 68
    :cond_0
    if-eqz p3, :cond_1

    .line 69
    invoke-virtual {v0, p3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 72
    :cond_1
    const/high16 v9, -0x40800000    # -1.0f

    cmpl-float v9, p2, v9

    if-eqz v9, :cond_2

    .line 73
    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setPosition(F)V

    .line 76
    :cond_2
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_3

    .line 77
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v7

    .line 78
    .local v7, "themeSolidColor":Ljava/lang/String;
    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setThemeColor(Ljava/lang/String;)V

    .line 79
    invoke-virtual {v1, v10}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setForeColor(Ljava/lang/String;)V

    .line 82
    .end local v7    # "themeSolidColor":Ljava/lang/String;
    :cond_3
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_4

    .line 83
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v2

    .line 84
    .local v2, "lumMod":Ljava/lang/String;
    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setLumMod(Ljava/lang/String;)V

    .line 85
    invoke-virtual {v1, v10}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setLumMod(Ljava/lang/String;)V

    .line 88
    .end local v2    # "lumMod":Ljava/lang/String;
    :cond_4
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumOff()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_5

    .line 89
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumOff()Ljava/lang/String;

    move-result-object v3

    .line 90
    .local v3, "lumOff":Ljava/lang/String;
    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setLumOff(Ljava/lang/String;)V

    .line 91
    invoke-virtual {v1, v10}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setLumOff(Ljava/lang/String;)V

    .line 94
    .end local v3    # "lumOff":Ljava/lang/String;
    :cond_5
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getSatMod()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_6

    .line 95
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getSatMod()Ljava/lang/String;

    move-result-object v4

    .line 96
    .local v4, "satMod":Ljava/lang/String;
    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setSatMod(Ljava/lang/String;)V

    .line 97
    invoke-virtual {v1, v10}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setSatMod(Ljava/lang/String;)V

    .line 100
    .end local v4    # "satMod":Ljava/lang/String;
    :cond_6
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getSatOff()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_7

    .line 101
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getSatOff()Ljava/lang/String;

    move-result-object v5

    .line 102
    .local v5, "satOff":Ljava/lang/String;
    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setSatOff(Ljava/lang/String;)V

    .line 103
    invoke-virtual {v1, v10}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setSatOff(Ljava/lang/String;)V

    .line 106
    .end local v5    # "satOff":Ljava/lang/String;
    :cond_7
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getShade()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_8

    .line 107
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getShade()Ljava/lang/String;

    move-result-object v6

    .line 108
    .local v6, "shd":Ljava/lang/String;
    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setShade(Ljava/lang/String;)V

    .line 109
    invoke-virtual {v1, v10}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setShade(Ljava/lang/String;)V

    .line 112
    .end local v6    # "shd":Ljava/lang/String;
    :cond_8
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getTint()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_9

    .line 113
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getTint()Ljava/lang/String;

    move-result-object v8

    .line 114
    .local v8, "tnt":Ljava/lang/String;
    invoke-virtual {v0, v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setTint(Ljava/lang/String;)V

    .line 115
    invoke-virtual {v1, v10}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setTint(Ljava/lang/String;)V

    .line 119
    .end local v8    # "tnt":Ljava/lang/String;
    :cond_9
    invoke-virtual {v1, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->addPositionVal(F)V

    .line 120
    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->addXDocColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    .line 123
    return-void
.end method

.method public setBlipEmbed(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "blipEmbed"    # Ljava/lang/String;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;->styleName:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;->styleName:Ljava/lang/String;

    const-string/jumbo v1, "bgFillStyleLst"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 131
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->setBgBlipFillId(Ljava/lang/String;)V

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;->styleName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;->styleName:Ljava/lang/String;

    const-string/jumbo v1, "fillStyleLst"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->setBlipFill(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setTile(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "flip"    # Ljava/lang/String;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;->styleName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;->styleName:Ljava/lang/String;

    const-string/jumbo v1, "bgFillStyleLst"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillStyleLstHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->setIsBgBlipTile(Z)V

    .line 147
    :cond_0
    return-void
.end method
