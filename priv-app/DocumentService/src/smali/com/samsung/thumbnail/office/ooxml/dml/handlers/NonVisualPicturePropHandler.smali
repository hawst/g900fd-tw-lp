.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualPicturePropHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "NonVisualPicturePropHandler.java"


# direct methods
.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 4
    .param p1, "nameSpaceId"    # I
    .param p2, "shapeProperties"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 11
    const-string/jumbo v3, "nvPicPr"

    invoke-direct {p0, p1, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 13
    const/4 v3, 0x1

    new-array v2, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 14
    .local v2, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualPropHandlers;

    invoke-direct {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualPropHandlers;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 16
    .local v0, "nvDrawHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualPropHandlers;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    const-string/jumbo v3, "nvPr"

    invoke-direct {v1, p1, v3, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 19
    .local v1, "nvDrawSeq":Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    const/4 v3, 0x0

    aput-object v1, v2, v3

    .line 20
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualPicturePropHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 21
    return-void
.end method
