.class public Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;
.super Lorg/apache/poi/POIXMLDocumentPart;
.source "XWPFNumbering.java"


# instance fields
.field protected abstractNums:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;",
            ">;"
        }
    .end annotation
.end field

.field isNew:Z

.field protected nums:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Lorg/apache/poi/POIXMLDocumentPart;-><init>()V

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;->abstractNums:Ljava/util/List;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;->nums:Ljava/util/List;

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;->abstractNums:Ljava/util/List;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;->nums:Ljava/util/List;

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;->isNew:Z

    .line 60
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V
    .locals 1
    .param p1, "part"    # Lorg/apache/poi/openxml4j/opc/PackagePart;
    .param p2, "rel"    # Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/poi/openxml4j/exceptions/OpenXML4JException;
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/POIXMLDocumentPart;-><init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;->abstractNums:Ljava/util/List;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;->nums:Ljava/util/List;

    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;->isNew:Z

    .line 51
    return-void
.end method


# virtual methods
.method public addAbstractNum(Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;)I
    .locals 1
    .param p1, "abstractNum"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;

    .prologue
    .line 193
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;->abstractNums:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;->getAbstractNumId()I

    move-result v0

    return v0
.end method

.method public addNum(Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;)V
    .locals 1
    .param p1, "num"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;->nums:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    return-void
.end method

.method public getAbstractNum(I)Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;
    .locals 3
    .param p1, "abstractNumID"    # I

    .prologue
    .line 141
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;->abstractNums:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;

    .line 142
    .local v0, "abstractNum":Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;->getAbstractNumId()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 146
    .end local v0    # "abstractNum":Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAbstractNum()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;->abstractNums:Ljava/util/List;

    return-object v0
.end method

.method public getIdOfAbstractNum(Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;)Ljava/math/BigInteger;
    .locals 1
    .param p1, "abstractNum"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;

    .prologue
    .line 175
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNum(I)Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;
    .locals 3
    .param p1, "numID"    # I

    .prologue
    .line 126
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;->nums:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;

    .line 127
    .local v1, "num":Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;->getNumId()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 130
    .end local v1    # "num":Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNumLst()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;->nums:Ljava/util/List;

    return-object v0
.end method

.method protected onDocumentRead()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumberingParser;

    invoke-direct {v1, p0}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumberingParser;-><init>(Ljava/lang/Object;)V

    .line 69
    .local v1, "numberingParser":Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumberingParser;
    const/4 v0, 0x0

    .line 71
    .local v0, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 72
    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumberingParser;->parse(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 89
    :cond_0
    return-void

    .line 74
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_1

    .line 75
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    :cond_1
    throw v2
.end method
