.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleXFHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelStyleXFHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 21
    const-string/jumbo v1, "xf"

    invoke-direct {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 23
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleXFAlignmentHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleXFAlignmentHandler;-><init>()V

    .line 24
    .local v0, "alignmentHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleXFAlignmentHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleXFHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "alignment"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 12
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    const/4 v11, 0x0

    .line 31
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 32
    const-string/jumbo v8, "numFmtId"

    invoke-virtual {p0, p3, v8, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleXFHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v6

    .line 34
    .local v6, "numFmtId":Ljava/lang/String;
    const-string/jumbo v8, "fontId"

    invoke-virtual {p0, p3, v8, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleXFHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v4

    .line 36
    .local v4, "fontId":Ljava/lang/String;
    const-string/jumbo v8, "fillId"

    invoke-virtual {p0, p3, v8, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleXFHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v3

    .line 38
    .local v3, "fillId":Ljava/lang/String;
    const-string/jumbo v8, "borderId"

    invoke-virtual {p0, p3, v8, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleXFHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 40
    .local v1, "bordeId":Ljava/lang/String;
    const-string/jumbo v8, "xfId"

    invoke-interface {p3, v8}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 42
    .local v5, "id":Ljava/lang/String;
    const-string/jumbo v8, "applyNumberFormat"

    invoke-interface {p3, v8}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 44
    .local v0, "applyNumberFormat":Ljava/lang/String;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellXfsHandler;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellXfsHandler;-><init>()V

    .line 45
    .local v2, "cellxfobj":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellXfsHandler;
    iget-boolean v8, v2, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellXfsHandler;->cellxfs:Z

    const/4 v9, 0x1

    if-ne v8, v9, :cond_0

    .line 46
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v7

    .line 48
    .local v7, "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v9, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->cellnum:I

    invoke-virtual {v8, v6, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellXFNumFmtId(Ljava/lang/String;I)V

    .line 50
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v9, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->cellnum:I

    invoke-virtual {v8, v4, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellXFFontid(Ljava/lang/String;I)V

    .line 52
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v9, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->cellnum:I

    invoke-virtual {v8, v1, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellXFBorderId(Ljava/lang/String;I)V

    .line 54
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v9, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->cellnum:I

    invoke-virtual {v8, v3, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellXFFillid(Ljava/lang/String;I)V

    .line 56
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v9, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->cellnum:I

    invoke-virtual {v8, v5, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellXFid(Ljava/lang/String;I)V

    .line 58
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v9, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->cellnum:I

    invoke-virtual {v8, v0, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addApplynumberformatval(Ljava/lang/String;I)V

    .line 61
    iget v8, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->cellnum:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->cellnum:I

    .line 63
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v9, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->numalignment:I

    invoke-virtual {v8, v11, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addAlingmentHorizontal(Ljava/lang/String;I)V

    .line 65
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v9, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->numalignment:I

    invoke-virtual {v8, v11, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellXFWraptext(Ljava/lang/String;I)V

    .line 67
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    const-string/jumbo v9, "bottom"

    iget v10, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->numalignment:I

    invoke-virtual {v8, v9, v10}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addAlingmentVertical(Ljava/lang/String;I)V

    .line 69
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v9, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->numalignment:I

    invoke-virtual {v8, v11, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addShrinkToFont(Ljava/lang/String;I)V

    .line 71
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v9, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->numalignment:I

    invoke-virtual {v8, v11, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addStyleTextAlignmentIndent(Ljava/lang/String;I)V

    .line 74
    iget v8, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->numalignment:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->numalignment:I

    .line 78
    .end local v7    # "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    :cond_0
    return-void
.end method
