.class public Lcom/samsung/thumbnail/office/ooxml/dml/wp/XDocPositionVHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XDocPositionVHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/wp/PosOffsetHandler$IOffsetPosObserver;


# instance fields
.field mOffsetRefPos:Ljava/lang/String;

.field mParser:Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

.field mPosOffestObserver:Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler$IPositionOffsetObserver;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler$IPositionOffsetObserver;)V
    .locals 2
    .param p1, "posOffsetObserver"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler$IPositionOffsetObserver;

    .prologue
    .line 21
    const/16 v0, 0x1e

    const-string/jumbo v1, "positionV"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 22
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/XDocPositionVHandler;->mPosOffestObserver:Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler$IPositionOffsetObserver;

    .line 23
    return-void
.end method

.method private init()V
    .locals 3

    .prologue
    .line 26
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/PosOffsetHandler;

    invoke-direct {v0, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/PosOffsetHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/wp/PosOffsetHandler$IOffsetPosObserver;)V

    .line 27
    .local v0, "posOffsetHandler":Lcom/samsung/thumbnail/office/ooxml/dml/wp/PosOffsetHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/XDocPositionVHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "posOffset"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    return-void
.end method


# virtual methods
.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 45
    const/16 v2, 0x1e

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v2

    invoke-static {p2, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/XDocPositionVHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "ele":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/XDocPositionVHandler;->handlerMap:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    .line 50
    .local v1, "eleHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    if-eqz v1, :cond_0

    .line 51
    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->pushHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 52
    invoke-virtual {v1, p1, v0, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 54
    :cond_0
    return-void
.end method

.method public setOffsetVal(J)V
    .locals 3
    .param p1, "offsetVal"    # J

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/XDocPositionVHandler;->mPosOffestObserver:Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler$IPositionOffsetObserver;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/XDocPositionVHandler;->mOffsetRefPos:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/XDocPositionVHandler;->mParser:Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    invoke-interface {v0, v1, p1, p2, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler$IPositionOffsetObserver;->setOffsetPositionValue(Ljava/lang/String;JLcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V

    .line 61
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 33
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 34
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/XDocPositionVHandler;->init()V

    .line 36
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/XDocPositionVHandler;->mParser:Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .line 37
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    const-string/jumbo v1, "relativeFrom"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/XDocPositionVHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;-><init>(Ljava/lang/String;)V

    .line 39
    .local v0, "relativeFromPos":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/XDocPositionVHandler;->mOffsetRefPos:Ljava/lang/String;

    .line 40
    return-void
.end method
