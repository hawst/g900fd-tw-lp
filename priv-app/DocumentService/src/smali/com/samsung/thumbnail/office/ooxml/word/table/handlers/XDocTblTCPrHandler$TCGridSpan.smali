.class public Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCGridSpan;
.super Ljava/lang/Object;
.source "XDocTblTCPrHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TCGridSpan"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCGridSpan;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V
    .locals 3
    .param p1, "value"    # Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;

    .prologue
    .line 155
    instance-of v1, p1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 156
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;

    .line 157
    .local v0, "intVal":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCGridSpan;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;

    # getter for: Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;->tblCellProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;
    invoke-static {v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;->access$000(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;)Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;->getValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->setGridSpan(I)V

    .line 159
    .end local v0    # "intVal":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;
    :cond_0
    return-void
.end method
