.class public Lcom/samsung/thumbnail/office/ooxml/excel/table/XExceTableColumnsHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExceTableColumnsHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 17
    const/16 v1, 0xc8

    const-string/jumbo v2, "tableColumns"

    invoke-direct {p0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 20
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/table/XExceTableColumnHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/table/XExceTableColumnHandler;-><init>()V

    .line 21
    .local v0, "tableColumnHandler":Lcom/samsung/thumbnail/office/ooxml/excel/table/XExceTableColumnHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/table/XExceTableColumnsHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "tableColumn"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 29
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 30
    const-string/jumbo v1, "count"

    invoke-interface {p3, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 31
    .local v0, "count":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/table/XWPFExcelTableParser;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/table/XWPFExcelTableParser;->addExTableColumnscount(I)V

    .line 34
    return-void
.end method
