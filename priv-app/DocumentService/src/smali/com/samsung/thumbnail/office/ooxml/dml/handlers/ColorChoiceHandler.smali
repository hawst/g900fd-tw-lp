.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "ColorChoiceHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;
    }
.end annotation


# instance fields
.field protected colorConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;

.field colorName:Ljava/lang/String;

.field private fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

.field private isSchemeClr:Z


# direct methods
.method public constructor <init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V
    .locals 1
    .param p1, "val"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "iColorConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;
    .param p4, "shInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p5, "theme"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->isSchemeClr:Z

    .line 32
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->colorConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;

    .line 33
    if-eqz p4, :cond_1

    .line 34
    invoke-virtual {p4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 35
    invoke-virtual {p4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .line 51
    :goto_0
    return-void

    .line 37
    :cond_0
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    goto :goto_0

    .line 40
    :cond_1
    if-eqz p5, :cond_3

    .line 41
    invoke-virtual {p5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 42
    invoke-virtual {p5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    goto :goto_0

    .line 44
    :cond_2
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .line 45
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-virtual {p5, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->consumeFill(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V

    goto :goto_0

    .line 48
    :cond_3
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;)V
    .locals 1
    .param p1, "element"    # Ljava/lang/String;
    .param p2, "iColorConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->isSchemeClr:Z

    .line 25
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->colorConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;

    .line 26
    return-void
.end method

.method private getPrstColor(Lorg/xml/sax/Attributes;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;
    .locals 2
    .param p1, "attrs"    # Lorg/xml/sax/Attributes;
    .param p2, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 116
    const-string/jumbo v1, "val"

    invoke-virtual {p0, p1, v1, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 118
    .local v0, "value":Ljava/lang/String;
    return-object v0
.end method

.method private getSchemeClr(Lorg/xml/sax/Attributes;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;
    .locals 2
    .param p1, "attrs"    # Lorg/xml/sax/Attributes;
    .param p2, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 99
    const-string/jumbo v1, "val"

    invoke-virtual {p0, p1, v1, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 101
    .local v0, "schemeClrVal":Ljava/lang/String;
    return-object v0
.end method

.method private getSrgbColor(Lorg/xml/sax/Attributes;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;
    .locals 2
    .param p1, "attrs"    # Lorg/xml/sax/Attributes;
    .param p2, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 105
    const-string/jumbo v1, "val"

    invoke-virtual {p0, p1, v1, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 106
    .local v0, "value":Ljava/lang/String;
    return-object v0
.end method

.method private getSysColor(Lorg/xml/sax/Attributes;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;
    .locals 2
    .param p1, "attrs"    # Lorg/xml/sax/Attributes;
    .param p2, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 110
    const-string/jumbo v1, "lastClr"

    invoke-virtual {p0, p1, v1, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 112
    .local v0, "lastClr":Ljava/lang/String;
    return-object v0
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 4
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 123
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->colorName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->colorConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->getElementName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->colorName:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-interface {v0, v1, v2, v3, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;->consumeColor(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V

    .line 128
    :cond_0
    return-void
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 56
    invoke-virtual {p0, p2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 60
    .local v0, "elementName":Ljava/lang/String;
    const-string/jumbo v1, "schemeClr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 61
    invoke-direct {p0, p3, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->getSchemeClr(Lorg/xml/sax/Attributes;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->colorName:Ljava/lang/String;

    .line 62
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->isSchemeClr:Z

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    const-string/jumbo v1, "sysClr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 65
    invoke-direct {p0, p3, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->getSysColor(Lorg/xml/sax/Attributes;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->colorName:Ljava/lang/String;

    goto :goto_0

    .line 66
    :cond_2
    const-string/jumbo v1, "prstClr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 67
    invoke-direct {p0, p3, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->getPrstColor(Lorg/xml/sax/Attributes;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->colorName:Ljava/lang/String;

    goto :goto_0

    .line 68
    :cond_3
    const-string/jumbo v1, "srgbClr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 70
    invoke-direct {p0, p3, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->getSrgbColor(Lorg/xml/sax/Attributes;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->colorName:Ljava/lang/String;

    goto :goto_0

    .line 71
    :cond_4
    const-string/jumbo v1, "lumMod"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 72
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    const-string/jumbo v2, "val"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setLumMod(Ljava/lang/String;)V

    goto :goto_0

    .line 74
    :cond_5
    const-string/jumbo v1, "lumOff"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 75
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    const-string/jumbo v2, "val"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setLumOff(Ljava/lang/String;)V

    goto :goto_0

    .line 77
    :cond_6
    const-string/jumbo v1, "shade"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 78
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    const-string/jumbo v2, "val"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setShade(Ljava/lang/String;)V

    goto :goto_0

    .line 80
    :cond_7
    const-string/jumbo v1, "tint"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 81
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    const-string/jumbo v2, "val"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setTint(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 83
    :cond_8
    const-string/jumbo v1, "satMod"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 84
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    const-string/jumbo v2, "val"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setSatMod(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 86
    :cond_9
    const-string/jumbo v1, "satOff"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 87
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    const-string/jumbo v2, "val"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setSatOff(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 89
    :cond_a
    const-string/jumbo v1, "alpha"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 90
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    const-string/jumbo v2, "val"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setAlpha(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 92
    :cond_b
    const-string/jumbo v1, "alphaOff"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 93
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    const-string/jumbo v2, "val"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setAlphaOff(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public isSchemeClr()Z
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->isSchemeClr:Z

    return v0
.end method
