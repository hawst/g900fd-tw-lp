.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/LineHeadTailHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "LineHeadTailHandler.java"


# static fields
.field private static final ARROWSIZE_HIGH:Ljava/lang/String; = "lg"

.field private static final ARROWSIZE_LOW:Ljava/lang/String; = "sm"

.field private static final ARROWSIZE_MEDIUM:Ljava/lang/String; = "med"

.field private static final ARROWTYPE_ARROW:Ljava/lang/String; = "arrow"

.field private static final ARROWTYPE_DIAMOND:Ljava/lang/String; = "diamond"

.field private static final ARROWTYPE_NONE:Ljava/lang/String; = "none"

.field private static final ARROWTYPE_OVAL:Ljava/lang/String; = "oval"

.field private static final ARROWTYPE_STEALTH:Ljava/lang/String; = "stealth"

.field private static final ARROWTYPE_TRIANGLE:Ljava/lang/String; = "triangle"


# instance fields
.field private consumerLineEnds:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/ILineEndsConsumer;

.field private isTailElem:Z


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/ILineEndsConsumer;Ljava/lang/String;)V
    .locals 1
    .param p1, "lineEndConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/ILineEndsConsumer;
    .param p2, "elem"    # Ljava/lang/String;

    .prologue
    .line 28
    const/16 v0, 0x1f

    invoke-direct {p0, v0, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/LineHeadTailHandler;->isTailElem:Z

    .line 30
    const-string/jumbo v0, "tailEnd"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/LineHeadTailHandler;->isTailElem:Z

    .line 34
    :cond_0
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/LineHeadTailHandler;->consumerLineEnds:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/ILineEndsConsumer;

    .line 36
    return-void
.end method

.method private getSizeValue(Ljava/lang/String;)I
    .locals 2
    .param p1, "inputVal"    # Ljava/lang/String;

    .prologue
    .line 39
    const/16 v0, 0x9

    .line 40
    .local v0, "sizeVal":I
    const-string/jumbo v1, "lg"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 41
    const/16 v0, 0xc

    .line 47
    :cond_0
    :goto_0
    return v0

    .line 42
    :cond_1
    const-string/jumbo v1, "med"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 43
    const/16 v0, 0x9

    goto :goto_0

    .line 44
    :cond_2
    const-string/jumbo v1, "sm"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 45
    const/4 v0, 0x6

    goto :goto_0
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 7
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 53
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 55
    const-string/jumbo v6, "type"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/LineHeadTailHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v4

    .line 57
    .local v4, "typeVal":Ljava/lang/String;
    const-string/jumbo v6, "w"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/LineHeadTailHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v5

    .line 59
    .local v5, "widthVal":Ljava/lang/String;
    const-string/jumbo v6, "len"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/LineHeadTailHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v3

    .line 62
    .local v3, "lengthVal":Ljava/lang/String;
    const/4 v1, 0x0

    .line 63
    .local v1, "arrowType":I
    const/16 v2, 0x9

    .line 64
    .local v2, "arrowWidth":I
    const/16 v0, 0x9

    .line 66
    .local v0, "arrowLength":I
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/LineHeadTailHandler;->consumerLineEnds:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/ILineEndsConsumer;

    if-nez v6, :cond_0

    .line 101
    :goto_0
    return-void

    .line 70
    :cond_0
    if-eqz v4, :cond_1

    .line 71
    const-string/jumbo v6, "oval"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 72
    const/4 v1, 0x4

    .line 86
    :cond_1
    :goto_1
    if-eqz v5, :cond_2

    .line 87
    invoke-direct {p0, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/LineHeadTailHandler;->getSizeValue(Ljava/lang/String;)I

    move-result v2

    .line 90
    :cond_2
    if-eqz v3, :cond_3

    .line 91
    invoke-direct {p0, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/LineHeadTailHandler;->getSizeValue(Ljava/lang/String;)I

    move-result v0

    .line 94
    :cond_3
    iget-boolean v6, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/LineHeadTailHandler;->isTailElem:Z

    if-eqz v6, :cond_9

    .line 95
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/LineHeadTailHandler;->consumerLineEnds:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/ILineEndsConsumer;

    invoke-interface {v6, v1, v0, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/ILineEndsConsumer;->consumeLineTailEnd(III)V

    goto :goto_0

    .line 73
    :cond_4
    const-string/jumbo v6, "diamond"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 74
    const/4 v1, 0x3

    goto :goto_1

    .line 75
    :cond_5
    const-string/jumbo v6, "stealth"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 76
    const/4 v1, 0x2

    goto :goto_1

    .line 77
    :cond_6
    const-string/jumbo v6, "arrow"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 78
    const/4 v1, 0x5

    goto :goto_1

    .line 79
    :cond_7
    const-string/jumbo v6, "triangle"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 80
    const/4 v1, 0x1

    goto :goto_1

    .line 81
    :cond_8
    const-string/jumbo v6, "none"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 82
    const/4 v1, 0x0

    goto :goto_1

    .line 98
    :cond_9
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/LineHeadTailHandler;->consumerLineEnds:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/ILineEndsConsumer;

    invoke-interface {v6, v1, v0, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/ILineEndsConsumer;->consumeLineHeadEnd(III)V

    goto :goto_0
.end method
