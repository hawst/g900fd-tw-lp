.class public Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;
.super Ljava/lang/Object;
.source "EMR_SelectObject.java"


# instance fields
.field private RecordSize:I

.field private RecordType:I

.field fileContent:[B

.field private ihObject:I

.field private offset:I


# direct methods
.method public constructor <init>([BI)V
    .locals 0
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->fileContent:[B

    .line 17
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->offset:I

    .line 18
    return-void
.end method


# virtual methods
.method public getIhObject()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->ihObject:I

    return v0
.end method

.method public getRecordSize()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->RecordSize:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->RecordType:I

    return v0
.end method

.method public readEmf_SelectObject(Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V
    .locals 12
    .param p1, "emrGlobals"    # Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x0

    const/high16 v8, -0x80000000

    .line 45
    const/4 v2, 0x0

    .line 47
    .local v2, "i":I
    new-array v4, v11, [B

    .line 49
    .local v4, "intConvert":[B
    const/4 v2, 0x0

    :goto_0
    if-gt v2, v10, :cond_0

    .line 50
    rsub-int/lit8 v5, v2, 0x3

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->fileContent:[B

    iget v7, p0, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->offset:I

    add-int/2addr v7, v2

    aget-byte v6, v6, v7

    aput-byte v6, v4, v5

    .line 49
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 51
    :cond_0
    invoke-static {v4, v9}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 52
    .local v1, "Type":I
    iget v5, p0, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->offset:I

    add-int/2addr v5, v2

    iput v5, p0, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->offset:I

    .line 53
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->setRecordType(I)V

    .line 55
    const/4 v2, 0x0

    :goto_1
    if-gt v2, v10, :cond_1

    .line 56
    rsub-int/lit8 v5, v2, 0x3

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->fileContent:[B

    iget v7, p0, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->offset:I

    add-int/2addr v7, v2

    aget-byte v6, v6, v7

    aput-byte v6, v4, v5

    .line 55
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 57
    :cond_1
    invoke-static {v4, v9}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 58
    .local v0, "Size":I
    iget v5, p0, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->offset:I

    add-int/2addr v5, v2

    iput v5, p0, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->offset:I

    .line 59
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->setRecordSize(I)V

    .line 61
    const/4 v2, 0x0

    :goto_2
    if-gt v2, v10, :cond_2

    .line 62
    rsub-int/lit8 v5, v2, 0x3

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->fileContent:[B

    iget v7, p0, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->offset:I

    add-int/2addr v7, v2

    aget-byte v6, v6, v7

    aput-byte v6, v4, v5

    .line 61
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 63
    :cond_2
    invoke-static {v4, v9}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v3

    .line 64
    .local v3, "ihObjct":I
    iget v5, p0, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->offset:I

    add-int/2addr v5, v2

    iput v5, p0, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->offset:I

    .line 66
    and-int v5, v3, v8

    if-ne v5, v8, :cond_9

    .line 67
    const/4 v5, 0x1

    iput-boolean v5, p1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->stockObject:Z

    .line 69
    xor-int v5, v3, v8

    if-nez v5, :cond_4

    .line 70
    const/4 v3, 0x0

    .line 84
    :cond_3
    :goto_3
    invoke-virtual {p0, v3}, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->setIhObject(I)V

    .line 85
    return-void

    .line 71
    :cond_4
    xor-int v5, v3, v8

    if-ne v5, v11, :cond_5

    .line 72
    const/4 v3, 0x4

    goto :goto_3

    .line 73
    :cond_5
    xor-int v5, v3, v8

    const/4 v6, 0x5

    if-ne v5, v6, :cond_6

    .line 74
    const/4 v3, 0x5

    goto :goto_3

    .line 75
    :cond_6
    xor-int v5, v3, v8

    const/4 v6, 0x6

    if-ne v5, v6, :cond_7

    .line 76
    const/4 v3, 0x6

    goto :goto_3

    .line 77
    :cond_7
    xor-int v5, v3, v8

    const/4 v6, 0x7

    if-ne v5, v6, :cond_8

    .line 78
    const/4 v3, 0x7

    goto :goto_3

    .line 79
    :cond_8
    xor-int v5, v3, v8

    const/16 v6, 0x8

    if-ne v5, v6, :cond_3

    .line 80
    const/16 v3, 0x8

    goto :goto_3

    .line 82
    :cond_9
    iput-boolean v9, p1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->stockObject:Z

    goto :goto_3
.end method

.method public setIhObject(I)V
    .locals 0
    .param p1, "ihObjct"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->ihObject:I

    .line 42
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "RecrdSize"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->RecordSize:I

    .line 34
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "RecrdType"    # I

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfObjectManipulationRecords/EMR_SelectObject;->RecordType:I

    .line 26
    return-void
.end method
