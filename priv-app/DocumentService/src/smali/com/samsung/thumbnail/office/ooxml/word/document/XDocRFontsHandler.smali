.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRFontsHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XDocRFontsHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRFontsHandler$IXDocRFontsObserver;
    }
.end annotation


# instance fields
.field private fontsObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRFontsHandler$IXDocRFontsObserver;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRFontsHandler$IXDocRFontsObserver;)V
    .locals 1
    .param p1, "fontsObserver"    # Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRFontsHandler$IXDocRFontsObserver;

    .prologue
    .line 16
    const-string/jumbo v0, "rFonts"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 17
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRFontsHandler;->fontsObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRFontsHandler$IXDocRFontsObserver;

    .line 18
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 23
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRFontsHandler;->fontsObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRFontsHandler$IXDocRFontsObserver;

    if-nez v1, :cond_1

    .line 37
    :cond_0
    :goto_0
    return-void

    .line 26
    :cond_1
    const-string/jumbo v1, "ascii"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRFontsHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 28
    .local v0, "attrVal":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 29
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRFontsHandler;->fontsObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRFontsHandler$IXDocRFontsObserver;

    invoke-interface {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRFontsHandler$IXDocRFontsObserver;->setFontName(Ljava/lang/String;)V

    .line 31
    :cond_2
    const-string/jumbo v1, "asciiTheme"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRFontsHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 33
    if-eqz v0, :cond_0

    .line 36
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRFontsHandler;->fontsObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRFontsHandler$IXDocRFontsObserver;

    invoke-interface {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRFontsHandler$IXDocRFontsObserver;->setThemeFont(Ljava/lang/String;)V

    goto :goto_0
.end method
