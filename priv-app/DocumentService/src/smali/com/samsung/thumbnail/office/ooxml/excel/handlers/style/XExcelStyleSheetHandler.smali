.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleSheetHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelStyleSheetHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 14
    const-string/jumbo v9, "styleSheet"

    invoke-direct {p0, v9}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 16
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleNumFmtsHandler;

    invoke-direct {v7}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleNumFmtsHandler;-><init>()V

    .line 17
    .local v7, "numfmtsHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleNumFmtsHandler;
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleSheetHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v10, "numFmts"

    invoke-virtual {v9, v10, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontsHandler;

    invoke-direct {v6}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontsHandler;-><init>()V

    .line 20
    .local v6, "fontsHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontsHandler;
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleSheetHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v10, "fonts"

    invoke-virtual {v9, v10, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFillsHandler;

    invoke-direct {v5}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFillsHandler;-><init>()V

    .line 23
    .local v5, "fillsHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFillsHandler;
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleSheetHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v10, "fills"

    invoke-virtual {v9, v10, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBordersHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBordersHandler;-><init>()V

    .line 26
    .local v0, "bordersHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBordersHandler;
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleSheetHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v10, "borders"

    invoke-virtual {v9, v10, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellStyleXfsHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellStyleXfsHandler;-><init>()V

    .line 29
    .local v1, "cellStyleXfsHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellStyleXfsHandler;
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleSheetHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v10, "cellStyleXfs"

    invoke-virtual {v9, v10, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellXfsHandler;

    invoke-direct {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellXfsHandler;-><init>()V

    .line 33
    .local v3, "cellXfsHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellXfsHandler;
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleSheetHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v10, "cellXfs"

    invoke-virtual {v9, v10, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelCellStylesHandler;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelCellStylesHandler;-><init>()V

    .line 36
    .local v2, "cellStylesHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelCellStylesHandler;
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleSheetHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v10, "cellStyles"

    invoke-virtual {v9, v10, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelDxfsHandler;

    invoke-direct {v4}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelDxfsHandler;-><init>()V

    .line 40
    .local v4, "dxfsHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelDxfsHandler;
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleSheetHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v10, "dxfs"

    invoke-virtual {v9, v10, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelTablestylesHandler;

    invoke-direct {v8}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelTablestylesHandler;-><init>()V

    .line 43
    .local v8, "tablestylesHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelTablestylesHandler;
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleSheetHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v10, "tableStyles"

    invoke-virtual {v9, v10, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    return-void
.end method
