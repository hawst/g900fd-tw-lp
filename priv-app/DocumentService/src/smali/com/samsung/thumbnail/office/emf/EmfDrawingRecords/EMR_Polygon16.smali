.class public Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;
.super Ljava/lang/Object;
.source "EMR_Polygon16.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;


# instance fields
.field private Count:I

.field private RecordSize:I

.field private RecordType:I

.field private aPoints:[B

.field arrayOfPoint:[Landroid/graphics/Point;

.field private emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

.field fileContent:[B

.field private leftTopX:I

.field private leftTopY:I

.field private localPath:Landroid/graphics/Path;

.field private offset:I

.field private rightBottomX:I

.field private rightBottomY:I


# direct methods
.method public constructor <init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V
    .locals 1
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I
    .param p3, "emrGlobals"    # Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .prologue
    const/4 v0, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->localPath:Landroid/graphics/Path;

    .line 60
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->arrayOfPoint:[Landroid/graphics/Point;

    .line 73
    iput-object p3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .line 74
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->fileContent:[B

    .line 75
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    .line 76
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->Count:I

    return v0
.end method

.method public getLeftTopX()I
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->leftTopX:I

    return v0
.end method

.method public getLeftTopY()I
    .locals 1

    .prologue
    .line 141
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->leftTopY:I

    return v0
.end method

.method public getPath()Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->localPath:Landroid/graphics/Path;

    return-object v0
.end method

.method public getRecordSize()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->RecordSize:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->RecordType:I

    return v0
.end method

.method public getRightBottomX()I
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->rightBottomX:I

    return v0
.end method

.method public getRightBottomY()I
    .locals 1

    .prologue
    .line 179
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->rightBottomY:I

    return v0
.end method

.method public getaPoints()[B
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->aPoints:[B

    return-object v0
.end method

.method public prepare()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 308
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->getaPoints()[B

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->setPoints([B)V

    .line 309
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->arrayOfPoint:[Landroid/graphics/Point;

    array-length v6, v6

    const/4 v7, 0x1

    if-gt v6, v7, :cond_0

    .line 335
    :goto_0
    return-void

    .line 312
    :cond_0
    new-instance v6, Landroid/graphics/Path;

    invoke-direct {v6}, Landroid/graphics/Path;-><init>()V

    iput-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->localPath:Landroid/graphics/Path;

    .line 314
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v6, v6, v8

    iget v6, v6, Landroid/graphics/Point;->x:I

    int-to-float v0, v6

    .line 315
    .local v0, "f1":F
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v6, v6, v8

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v1, v6

    .line 316
    .local v1, "f2":F
    const/4 v2, 0x0

    .line 317
    .local v2, "f3":F
    const/4 v3, 0x0

    .line 319
    .local v3, "f4":F
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->localPath:Landroid/graphics/Path;

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v7, v0, v7

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v8, v1, v8

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 320
    const/4 v4, 0x1

    .line 323
    .local v4, "i":I
    :goto_1
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->arrayOfPoint:[Landroid/graphics/Point;

    array-length v5, v6

    .line 324
    .local v5, "j":I
    if-lt v4, v5, :cond_1

    .line 332
    invoke-virtual {p0, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->setCurrentXY(FF)V

    .line 333
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->localPath:Landroid/graphics/Path;

    invoke-virtual {v6}, Landroid/graphics/Path;->close()V

    goto :goto_0

    .line 326
    :cond_1
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v6, v6, v4

    iget v6, v6, Landroid/graphics/Point;->x:I

    int-to-float v2, v6

    .line 327
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v6, v6, v4

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v3, v6

    .line 329
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->localPath:Landroid/graphics/Path;

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v7, v2, v7

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v8, v3, v8

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 330
    add-int/lit8 v4, v4, 0x1

    .line 331
    goto :goto_1
.end method

.method public readEmfPolygon16()V
    .locals 14

    .prologue
    .line 234
    const/4 v4, 0x0

    .line 236
    .local v4, "i":I
    const/4 v11, 0x4

    new-array v5, v11, [B

    .line 238
    .local v5, "intConvert":[B
    const/4 v4, 0x0

    :goto_0
    const/4 v11, 0x3

    if-gt v4, v11, :cond_0

    .line 239
    rsub-int/lit8 v11, v4, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    add-int/2addr v13, v4

    aget-byte v12, v12, v13

    aput-byte v12, v5, v11

    .line 238
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 240
    :cond_0
    const/4 v11, 0x0

    invoke-static {v5, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v2

    .line 241
    .local v2, "Type":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    add-int/2addr v11, v4

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    .line 242
    invoke-virtual {p0, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->setRecordType(I)V

    .line 244
    const/4 v4, 0x0

    :goto_1
    const/4 v11, 0x3

    if-gt v4, v11, :cond_1

    .line 245
    rsub-int/lit8 v11, v4, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    add-int/2addr v13, v4

    aget-byte v12, v12, v13

    aput-byte v12, v5, v11

    .line 244
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 246
    :cond_1
    const/4 v11, 0x0

    invoke-static {v5, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 247
    .local v1, "Size":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    add-int/2addr v11, v4

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    .line 248
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->setRecordSize(I)V

    .line 250
    const/4 v4, 0x0

    :goto_2
    const/4 v11, 0x3

    if-gt v4, v11, :cond_2

    .line 251
    rsub-int/lit8 v11, v4, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    add-int/2addr v13, v4

    aget-byte v12, v12, v13

    aput-byte v12, v5, v11

    .line 250
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 252
    :cond_2
    const/4 v11, 0x0

    invoke-static {v5, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v6

    .line 253
    .local v6, "leftTopX":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    add-int/2addr v11, v4

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    .line 254
    int-to-float v11, v6

    invoke-static {v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->setLeftTopX(I)V

    .line 256
    const/4 v4, 0x0

    :goto_3
    const/4 v11, 0x3

    if-gt v4, v11, :cond_3

    .line 257
    rsub-int/lit8 v11, v4, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    add-int/2addr v13, v4

    aget-byte v12, v12, v13

    aput-byte v12, v5, v11

    .line 256
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 258
    :cond_3
    const/4 v11, 0x0

    invoke-static {v5, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v7

    .line 259
    .local v7, "leftTopY":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    add-int/2addr v11, v4

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    .line 260
    int-to-float v11, v7

    invoke-static {v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->setLeftTopY(I)V

    .line 262
    const/4 v4, 0x0

    :goto_4
    const/4 v11, 0x3

    if-gt v4, v11, :cond_4

    .line 263
    rsub-int/lit8 v11, v4, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    add-int/2addr v13, v4

    aget-byte v12, v12, v13

    aput-byte v12, v5, v11

    .line 262
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 264
    :cond_4
    const/4 v11, 0x0

    invoke-static {v5, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v9

    .line 265
    .local v9, "rightBottomX":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    add-int/2addr v11, v4

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    .line 266
    int-to-float v11, v9

    invoke-static {v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->setRightBottomX(I)V

    .line 268
    const/4 v4, 0x0

    :goto_5
    const/4 v11, 0x3

    if-gt v4, v11, :cond_5

    .line 269
    rsub-int/lit8 v11, v4, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    add-int/2addr v13, v4

    aget-byte v12, v12, v13

    aput-byte v12, v5, v11

    .line 268
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 270
    :cond_5
    const/4 v11, 0x0

    invoke-static {v5, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v10

    .line 271
    .local v10, "rightBottomY":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    add-int/2addr v11, v4

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    .line 272
    int-to-float v11, v10

    invoke-static {v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->setRightBottomY(I)V

    .line 274
    const/4 v4, 0x0

    :goto_6
    const/4 v11, 0x3

    if-gt v4, v11, :cond_6

    .line 275
    rsub-int/lit8 v11, v4, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    add-int/2addr v13, v4

    aget-byte v12, v12, v13

    aput-byte v12, v5, v11

    .line 274
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 276
    :cond_6
    const/4 v11, 0x0

    invoke-static {v5, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 277
    .local v0, "Count":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    add-int/2addr v11, v4

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    .line 278
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->setCount(I)V

    .line 280
    add-int/lit8 v3, v1, -0x1c

    .line 281
    .local v3, "buffsize":I
    new-array v8, v3, [B

    .line 282
    .local v8, "pointsBuffer":[B
    const/4 v4, 0x0

    :goto_7
    if-ge v4, v3, :cond_7

    .line 283
    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->fileContent:[B

    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    add-int/2addr v12, v4

    aget-byte v11, v11, v12

    aput-byte v11, v8, v4

    .line 282
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 285
    :cond_7
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    add-int/2addr v11, v4

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->offset:I

    .line 286
    invoke-virtual {p0, v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->setaPoints([B)V

    .line 287
    return-void
.end method

.method public render(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 354
    const/4 v3, 0x0

    .line 355
    .local v3, "i":I
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;-><init>()V

    .line 356
    .local v0, "PaintPolygon16PropertiesBrushObject":Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;
    new-instance v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;-><init>()V

    .line 357
    .local v1, "PaintPolygon16PropertiesPenObject":Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;
    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v5}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 359
    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 361
    .local v2, "emrSize":I
    if-lez v2, :cond_1

    .line 362
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    .line 363
    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v6, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandleBrush:I

    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->gethandleNumber()I

    move-result v5

    if-ne v6, v5, :cond_8

    .line 368
    :cond_0
    if-ge v3, v2, :cond_1

    .line 369
    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->getColor()I

    move-result v5

    invoke-static {v5}, Lcom/samsung/thumbnail/office/emf/Util/toColorAndroid;->getColor(I)I

    move-result v4

    .line 372
    .local v4, "myColor":I
    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 373
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->getPath()Landroid/graphics/Path;

    move-result-object v5

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 378
    .end local v4    # "myColor":I
    :cond_1
    if-eq v3, v2, :cond_2

    if-nez v2, :cond_3

    .line 380
    :cond_2
    const/4 v5, 0x5

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v6, v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v5, v6, :cond_9

    .line 391
    :cond_3
    :goto_1
    if-lez v2, :cond_5

    .line 392
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v2, :cond_4

    .line 393
    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v6, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandlePen:I

    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->gethandleNumber()I

    move-result v5

    if-ne v6, v5, :cond_a

    .line 397
    :cond_4
    if-ge v3, v2, :cond_5

    .line 398
    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getColor()I

    move-result v5

    invoke-static {v5}, Lcom/samsung/thumbnail/office/emf/Util/toColorAndroid;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 401
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->getPath()Landroid/graphics/Path;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 406
    :cond_5
    if-eq v3, v2, :cond_6

    if-nez v2, :cond_7

    .line 408
    :cond_6
    const/16 v5, 0x8

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v6, v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v5, v6, :cond_b

    .line 416
    :cond_7
    :goto_3
    return-void

    .line 362
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 382
    :cond_9
    const/4 v5, 0x4

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v6, v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v5, v6, :cond_3

    .line 384
    const/high16 v5, -0x1000000

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 385
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->getPath()Landroid/graphics/Path;

    move-result-object v5

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 392
    :cond_a
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 411
    :cond_b
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->getPath()Landroid/graphics/Path;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_3
.end method

.method public setCount(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 208
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->Count:I

    .line 209
    return-void
.end method

.method public setCurrentXY(FF)V
    .locals 2
    .param p1, "currX"    # F
    .param p2, "currY"    # F

    .prologue
    .line 298
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v1, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v1, p1, v1

    iput v1, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->currentX:F

    .line 299
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v1, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v1, p2, v1

    iput v1, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->currentY:F

    .line 300
    return-void
.end method

.method public setLeftTopX(I)V
    .locals 0
    .param p1, "leftTopX"    # I

    .prologue
    .line 132
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->leftTopX:I

    .line 133
    return-void
.end method

.method public setLeftTopY(I)V
    .locals 0
    .param p1, "leftTopY"    # I

    .prologue
    .line 151
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->leftTopY:I

    .line 152
    return-void
.end method

.method public setPoints([B)V
    .locals 13
    .param p1, "points"    # [B

    .prologue
    const v12, 0xffff

    const/4 v11, 0x1

    const/4 v10, 0x0

    const v9, 0x8000

    .line 425
    const/4 v2, 0x0

    .line 426
    .local v2, "i":I
    const/4 v4, 0x0

    .line 427
    .local v4, "j":I
    const/4 v5, 0x0

    .line 428
    .local v5, "k":I
    const/4 v6, 0x0

    .line 430
    .local v6, "offset":I
    array-length v7, p1

    shr-int/lit8 v7, v7, 0x2

    new-array v7, v7, [Landroid/graphics/Point;

    iput-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->arrayOfPoint:[Landroid/graphics/Point;

    .line 432
    const/4 v7, 0x2

    new-array v3, v7, [B

    .line 434
    .local v3, "intConvert":[B
    :goto_0
    array-length v7, p1

    if-ge v6, v7, :cond_4

    .line 435
    const/4 v2, 0x0

    :goto_1
    if-gt v2, v11, :cond_0

    .line 436
    rsub-int/lit8 v7, v2, 0x1

    add-int v8, v6, v2

    aget-byte v8, p1, v8

    aput-byte v8, v3, v7

    .line 435
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 437
    :cond_0
    invoke-static {v3, v10}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getSmallInt([BI)I

    move-result v0

    .line 438
    .local v0, "X":I
    add-int/2addr v6, v2

    .line 440
    and-int v7, v0, v9

    if-ne v7, v9, :cond_1

    .line 441
    xor-int/lit8 v7, v0, -0x1

    and-int/2addr v7, v12

    add-int/lit8 v7, v7, 0x1

    mul-int/lit8 v0, v7, -0x1

    .line 444
    :cond_1
    const/4 v4, 0x0

    :goto_2
    if-gt v4, v11, :cond_2

    .line 445
    rsub-int/lit8 v7, v4, 0x1

    add-int v8, v6, v4

    aget-byte v8, p1, v8

    aput-byte v8, v3, v7

    .line 444
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 446
    :cond_2
    invoke-static {v3, v10}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getSmallInt([BI)I

    move-result v1

    .line 448
    .local v1, "Y":I
    and-int v7, v1, v9

    if-ne v7, v9, :cond_3

    .line 449
    xor-int/lit8 v7, v1, -0x1

    and-int/2addr v7, v12

    add-int/lit8 v7, v7, 0x1

    mul-int/lit8 v1, v7, -0x1

    .line 452
    :cond_3
    add-int/2addr v6, v4

    .line 453
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->arrayOfPoint:[Landroid/graphics/Point;

    new-instance v8, Landroid/graphics/Point;

    invoke-direct {v8, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    aput-object v8, v7, v5

    .line 455
    add-int/lit8 v5, v5, 0x1

    .line 456
    goto :goto_0

    .line 458
    .end local v0    # "X":I
    .end local v1    # "Y":I
    :cond_4
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 113
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->RecordSize:I

    .line 114
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 94
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->RecordType:I

    .line 95
    return-void
.end method

.method public setRightBottomX(I)V
    .locals 0
    .param p1, "rightBottomX"    # I

    .prologue
    .line 170
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->rightBottomX:I

    .line 171
    return-void
.end method

.method public setRightBottomY(I)V
    .locals 0
    .param p1, "rightBottomY"    # I

    .prologue
    .line 189
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->rightBottomY:I

    .line 190
    return-void
.end method

.method public setaPoints([B)V
    .locals 0
    .param p1, "aPoints"    # [B

    .prologue
    .line 227
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon16;->aPoints:[B

    .line 228
    return-void
.end method
