.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TblTCPrHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "TblTCPrHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;


# instance fields
.field private shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 21
    .param p1, "shInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 27
    const/16 v19, 0x1f

    const-string/jumbo v20, "tcPr"

    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 29
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TblTCPrHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 31
    const/16 v19, 0x7

    move/from16 v0, v19

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    move-object/from16 v16, v0

    .line 33
    .local v16, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;

    const-string/jumbo v19, "lnL"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-direct {v8, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Ljava/lang/String;)V

    .line 35
    .local v8, "lnLHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v19, "lnL"

    move-object/from16 v0, v19

    invoke-direct {v9, v0, v8}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 37
    .local v9, "lnLSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v19, 0x0

    aput-object v9, v16, v19

    .line 39
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;

    const-string/jumbo v19, "lnR"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Ljava/lang/String;)V

    .line 41
    .local v10, "lnRHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v19, "lnR"

    move-object/from16 v0, v19

    invoke-direct {v11, v0, v10}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 43
    .local v11, "lnRSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v19, 0x1

    aput-object v11, v16, v19

    .line 45
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;

    const-string/jumbo v19, "lnT"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-direct {v12, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Ljava/lang/String;)V

    .line 47
    .local v12, "lnTHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;
    new-instance v13, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v19, "lnT"

    move-object/from16 v0, v19

    invoke-direct {v13, v0, v12}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 49
    .local v13, "lnTSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v19, 0x2

    aput-object v13, v16, v19

    .line 51
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;

    const-string/jumbo v19, "lnB"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-direct {v4, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Ljava/lang/String;)V

    .line 53
    .local v4, "lnBHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v19, "lnB"

    move-object/from16 v0, v19

    invoke-direct {v5, v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 55
    .local v5, "lnBSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v19, 0x3

    aput-object v5, v16, v19

    .line 57
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;

    const-string/jumbo v19, "lnBlToTr"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-direct {v6, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Ljava/lang/String;)V

    .line 59
    .local v6, "lnBlToTrHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v19, "lnBlToTr"

    move-object/from16 v0, v19

    invoke-direct {v7, v0, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 61
    .local v7, "lnBlToTrSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v19, 0x4

    aput-object v7, v16, v19

    .line 63
    new-instance v14, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;

    const-string/jumbo v19, "lnTlToBr"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-direct {v14, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Ljava/lang/String;)V

    .line 65
    .local v14, "lnTlToBrHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;
    new-instance v15, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v19, "lnTlToBr"

    move-object/from16 v0, v19

    invoke-direct {v15, v0, v14}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 67
    .local v15, "lnTlToBrSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v19, 0x5

    aput-object v15, v16, v19

    .line 69
    new-instance v17, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;

    const/16 v19, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 71
    .local v17, "solidFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v19, "solidFill"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 73
    .local v18, "solidFillSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v19, 0x6

    aput-object v18, v16, v19

    .line 106
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TblTCPrHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 107
    return-void
.end method


# virtual methods
.method public consumeFill(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 3
    .param p1, "fillProperties"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 146
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TblTCPrHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    if-nez v1, :cond_0

    .line 158
    :goto_0
    return-void

    .line 149
    :cond_0
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 150
    .local v0, "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 151
    iget-boolean v1, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->isSchemeClr:Z

    if-eqz v1, :cond_1

    .line 152
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setSchemeClr(Z)V

    .line 154
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 155
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TblTCPrHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setLumMod(Ljava/lang/String;)V

    .line 157
    :cond_2
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TblTCPrHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setCellColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    goto :goto_0
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 10
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 112
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 114
    const-string/jumbo v9, "marL"

    invoke-virtual {p0, p3, v9, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TblTCPrHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v8

    .line 115
    .local v8, "value":Ljava/lang/String;
    if-eqz v8, :cond_0

    .line 116
    invoke-static {v8}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v9

    invoke-static {v9}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints(F)F

    move-result v3

    .line 117
    .local v3, "marL":F
    invoke-static {v3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    .line 118
    .local v1, "leftMargin":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TblTCPrHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v9, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setMarginLeft(Ljava/lang/String;)V

    .line 120
    .end local v1    # "leftMargin":Ljava/lang/String;
    .end local v3    # "marL":F
    :cond_0
    const-string/jumbo v9, "marR"

    invoke-virtual {p0, p3, v9, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TblTCPrHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v8

    .line 121
    if-eqz v8, :cond_1

    .line 122
    invoke-static {v8}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v9

    invoke-static {v9}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints(F)F

    move-result v4

    .line 123
    .local v4, "marR":F
    invoke-static {v4}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v6

    .line 124
    .local v6, "rightMargin":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TblTCPrHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v9, v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setMarginRight(Ljava/lang/String;)V

    .line 126
    .end local v4    # "marR":F
    .end local v6    # "rightMargin":Ljava/lang/String;
    :cond_1
    const-string/jumbo v9, "marT"

    invoke-virtual {p0, p3, v9, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TblTCPrHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v8

    .line 127
    if-eqz v8, :cond_2

    .line 128
    invoke-static {v8}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v9

    invoke-static {v9}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints(F)F

    move-result v5

    .line 129
    .local v5, "marT":F
    invoke-static {v5}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v7

    .line 130
    .local v7, "topargin":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TblTCPrHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v9, v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setMarginTop(Ljava/lang/String;)V

    .line 132
    .end local v5    # "marT":F
    .end local v7    # "topargin":Ljava/lang/String;
    :cond_2
    const-string/jumbo v9, "marB"

    invoke-virtual {p0, p3, v9, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TblTCPrHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v8

    .line 133
    if-eqz v8, :cond_3

    .line 134
    invoke-static {v8}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v9

    invoke-static {v9}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints(F)F

    move-result v2

    .line 135
    .local v2, "marB":F
    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v0

    .line 136
    .local v0, "bottomMargin":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TblTCPrHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setMarginBottom(Ljava/lang/String;)V

    .line 138
    .end local v0    # "bottomMargin":Ljava/lang/String;
    .end local v2    # "marB":F
    :cond_3
    const-string/jumbo v9, "anchor"

    invoke-virtual {p0, p3, v9, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TblTCPrHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v8

    .line 139
    if-eqz v8, :cond_4

    .line 140
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TblTCPrHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v9, v8}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setAnchor(Ljava/lang/String;)V

    .line 142
    :cond_4
    return-void
.end method
