.class public Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;
.super Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;
.source "XSLFGraphicFrame.java"


# instance fields
.field private isChart:Z

.field private isDiagram:Z

.field relId:Ljava/lang/String;

.field private shapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

.field private sheet:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;-><init>()V

    .line 31
    return-void
.end method

.method constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;)V
    .locals 0
    .param p1, "sheet"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;->sheet:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    .line 23
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;->shapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 27
    return-void
.end method


# virtual methods
.method public getAnchor()Lorg/apache/poi/java/awt/geom/Rectangle2D;
    .locals 2

    .prologue
    .line 42
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "NotImplemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getIsChart()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;->isChart:Z

    return v0
.end method

.method public getPrstShapeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;->shapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getPrstShapeName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRelId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;->relId:Ljava/lang/String;

    return-object v0
.end method

.method public getShapeId()I
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;->shapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeId()I

    move-result v0

    return v0
.end method

.method public getShapeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;->shapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;->shapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeName()Ljava/lang/String;

    move-result-object v0

    .line 70
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;->shapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    return-object v0
.end method

.method public getShapeType()I
    .locals 2

    .prologue
    .line 38
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "NotImplemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getSheet()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;->sheet:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    return-object v0
.end method

.method public isDiagram()Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;->isDiagram:Z

    return v0
.end method

.method public setAnchor(Lorg/apache/poi/java/awt/geom/Rectangle2D;)V
    .locals 2
    .param p1, "anchor"    # Lorg/apache/poi/java/awt/geom/Rectangle2D;

    .prologue
    .line 46
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "NotImplemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setIsChart(Z)V
    .locals 0
    .param p1, "isChart"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;->isChart:Z

    .line 51
    return-void
.end method

.method public setIsDiagram(Z)V
    .locals 0
    .param p1, "isDiagram"    # Z

    .prologue
    .line 91
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;->isDiagram:Z

    .line 92
    return-void
.end method

.method public setRelId(Ljava/lang/String;)V
    .locals 0
    .param p1, "relId"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;->relId:Ljava/lang/String;

    .line 55
    return-void
.end method
