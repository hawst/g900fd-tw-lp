.class public Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;
.super Ljava/lang/Object;
.source "EMR_ExtTextOutA.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;


# instance fields
.field private DxBuffer:[B

.field private Size:I

.field private StringBuffer:[B

.field private Type:I

.field private emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

.field private exScale:F

.field private eyScale:F

.field fileContent:[B

.field private fontBrush:I

.field private fontIndirectIndex:I

.field private iGraphicsMode:I

.field private leftTopX:I

.field private leftTopY:I

.field private numberOfChars:I

.field private offDx:I

.field private offString:I

.field private offset:I

.field private options:I

.field private rightBottomX:I

.field private rightBottomY:I

.field private textLeftTopX:I

.field private textLeftTopY:I

.field private textRightBottomX:I

.field private textRightBottomY:I

.field private textX:I

.field private textY:I


# direct methods
.method public constructor <init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V
    .locals 0
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I
    .param p3, "emrGlobals"    # Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    iput-object p3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .line 132
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fileContent:[B

    .line 133
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    .line 134
    return-void
.end method


# virtual methods
.method public getDxBuffer()[B
    .locals 1

    .prologue
    .line 522
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->DxBuffer:[B

    return-object v0
.end method

.method public getExScale()F
    .locals 1

    .prologue
    .line 275
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->exScale:F

    return v0
.end method

.method public getEyScale()F
    .locals 1

    .prologue
    .line 294
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->eyScale:F

    return v0
.end method

.method public getFontBrush()I
    .locals 1

    .prologue
    .line 560
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fontBrush:I

    return v0
.end method

.method public getFontIndirectIndex()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fontIndirectIndex:I

    return v0
.end method

.method public getLeftTopX()I
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->leftTopX:I

    return v0
.end method

.method public getLeftTopY()I
    .locals 1

    .prologue
    .line 199
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->leftTopY:I

    return v0
.end method

.method public getNumberOfChars()I
    .locals 1

    .prologue
    .line 351
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->numberOfChars:I

    return v0
.end method

.method public getOffDx()I
    .locals 1

    .prologue
    .line 484
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offDx:I

    return v0
.end method

.method public getOffString()I
    .locals 1

    .prologue
    .line 370
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offString:I

    return v0
.end method

.method public getOffset()I
    .locals 1

    .prologue
    .line 541
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    return v0
.end method

.method public getOptions()I
    .locals 1

    .prologue
    .line 389
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->options:I

    return v0
.end method

.method public getRightBottomX()I
    .locals 1

    .prologue
    .line 218
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->rightBottomX:I

    return v0
.end method

.method public getRightBottomY()I
    .locals 1

    .prologue
    .line 237
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->rightBottomY:I

    return v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->Size:I

    return v0
.end method

.method public getStringBuffer()[B
    .locals 1

    .prologue
    .line 503
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->StringBuffer:[B

    return-object v0
.end method

.method public getTextLeftTopX()I
    .locals 1

    .prologue
    .line 408
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->textLeftTopX:I

    return v0
.end method

.method public getTextLeftTopY()I
    .locals 1

    .prologue
    .line 427
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->textLeftTopY:I

    return v0
.end method

.method public getTextRightBottomX()I
    .locals 1

    .prologue
    .line 446
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->textRightBottomX:I

    return v0
.end method

.method public getTextRightBottomY()I
    .locals 1

    .prologue
    .line 465
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->textRightBottomY:I

    return v0
.end method

.method public getTextX()I
    .locals 1

    .prologue
    .line 313
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->textX:I

    return v0
.end method

.method public getTextY()I
    .locals 1

    .prologue
    .line 332
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->textY:I

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->Type:I

    return v0
.end method

.method public getiGraphicsMode()I
    .locals 1

    .prologue
    .line 256
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->iGraphicsMode:I

    return v0
.end method

.method public prepare()V
    .locals 0

    .prologue
    .line 745
    return-void
.end method

.method public readEmfExtTextOutW()V
    .locals 31

    .prologue
    .line 577
    const/4 v8, 0x0

    .line 578
    .local v8, "i":I
    const/16 v28, 0x4

    move/from16 v0, v28

    new-array v10, v0, [B

    .line 580
    .local v10, "intConvert":[B
    const/4 v8, 0x0

    :goto_0
    const/16 v28, 0x3

    move/from16 v0, v28

    if-gt v8, v0, :cond_0

    .line 581
    rsub-int/lit8 v28, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v8

    aget-byte v29, v29, v30

    aput-byte v29, v10, v28

    .line 580
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 582
    :cond_0
    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-static {v10, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v3

    .line 583
    .local v3, "Type":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v8

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    .line 584
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->setType(I)V

    .line 586
    const/4 v8, 0x0

    :goto_1
    const/16 v28, 0x3

    move/from16 v0, v28

    if-gt v8, v0, :cond_1

    .line 587
    rsub-int/lit8 v28, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v8

    aget-byte v29, v29, v30

    aput-byte v29, v10, v28

    .line 586
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 588
    :cond_1
    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-static {v10, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v2

    .line 589
    .local v2, "Size":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v8

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    .line 590
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->setSize(I)V

    .line 592
    const/4 v8, 0x0

    :goto_2
    const/16 v28, 0x3

    move/from16 v0, v28

    if-gt v8, v0, :cond_2

    .line 593
    rsub-int/lit8 v28, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v8

    aget-byte v29, v29, v30

    aput-byte v29, v10, v28

    .line 592
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 594
    :cond_2
    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-static {v10, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v13

    .line 595
    .local v13, "leftTopX":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v8

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    .line 596
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->setLeftTopX(I)V

    .line 598
    const/4 v8, 0x0

    :goto_3
    const/16 v28, 0x3

    move/from16 v0, v28

    if-gt v8, v0, :cond_3

    .line 599
    rsub-int/lit8 v28, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v8

    aget-byte v29, v29, v30

    aput-byte v29, v10, v28

    .line 598
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 600
    :cond_3
    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-static {v10, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v14

    .line 601
    .local v14, "leftTopY":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v8

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    .line 602
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->setLeftTopY(I)V

    .line 604
    const/4 v8, 0x0

    :goto_4
    const/16 v28, 0x3

    move/from16 v0, v28

    if-gt v8, v0, :cond_4

    .line 605
    rsub-int/lit8 v28, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v8

    aget-byte v29, v29, v30

    aput-byte v29, v10, v28

    .line 604
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    .line 606
    :cond_4
    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-static {v10, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v20

    .line 607
    .local v20, "rightBottomX":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v8

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    .line 608
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->setRightBottomX(I)V

    .line 610
    const/4 v8, 0x0

    :goto_5
    const/16 v28, 0x3

    move/from16 v0, v28

    if-gt v8, v0, :cond_5

    .line 611
    rsub-int/lit8 v28, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v8

    aget-byte v29, v29, v30

    aput-byte v29, v10, v28

    .line 610
    add-int/lit8 v8, v8, 0x1

    goto :goto_5

    .line 612
    :cond_5
    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-static {v10, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v21

    .line 613
    .local v21, "rightBottomY":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v8

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    .line 614
    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->setRightBottomY(I)V

    .line 616
    const/4 v8, 0x0

    :goto_6
    const/16 v28, 0x3

    move/from16 v0, v28

    if-gt v8, v0, :cond_6

    .line 617
    rsub-int/lit8 v28, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v8

    aget-byte v29, v29, v30

    aput-byte v29, v10, v28

    .line 616
    add-int/lit8 v8, v8, 0x1

    goto :goto_6

    .line 618
    :cond_6
    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-static {v10, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v9

    .line 619
    .local v9, "iGraphicsMode":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v8

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    .line 620
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->setiGraphicsMode(I)V

    .line 622
    const/4 v8, 0x0

    :goto_7
    const/16 v28, 0x3

    move/from16 v0, v28

    if-gt v8, v0, :cond_7

    .line 623
    rsub-int/lit8 v28, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v8

    aget-byte v29, v29, v30

    aput-byte v29, v10, v28

    .line 622
    add-int/lit8 v8, v8, 0x1

    goto :goto_7

    .line 624
    :cond_7
    invoke-static {v10}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v28

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v6

    .line 626
    .local v6, "exScale":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v8

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    .line 627
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->setExScale(F)V

    .line 629
    const/4 v8, 0x0

    :goto_8
    const/16 v28, 0x3

    move/from16 v0, v28

    if-gt v8, v0, :cond_8

    .line 630
    rsub-int/lit8 v28, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v8

    aget-byte v29, v29, v30

    aput-byte v29, v10, v28

    .line 629
    add-int/lit8 v8, v8, 0x1

    goto :goto_8

    .line 631
    :cond_8
    invoke-static {v10}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v28

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v7

    .line 633
    .local v7, "eyScale":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v8

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    .line 634
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->setEyScale(F)V

    .line 636
    const/4 v8, 0x0

    :goto_9
    const/16 v28, 0x3

    move/from16 v0, v28

    if-gt v8, v0, :cond_9

    .line 637
    rsub-int/lit8 v28, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v8

    aget-byte v29, v29, v30

    aput-byte v29, v10, v28

    .line 636
    add-int/lit8 v8, v8, 0x1

    goto :goto_9

    .line 638
    :cond_9
    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-static {v10, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v26

    .line 639
    .local v26, "textX":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v8

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    .line 640
    const/high16 v28, -0x80000000

    and-int v28, v28, v26

    const/high16 v29, -0x80000000

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_a

    .line 641
    xor-int/lit8 v28, v26, -0x1

    and-int/lit8 v28, v28, -0x1

    add-int/lit8 v28, v28, 0x1

    mul-int/lit8 v26, v28, -0x1

    .line 642
    if-gez v26, :cond_a

    .line 643
    move/from16 v0, v26

    neg-int v0, v0

    move/from16 v26, v0

    .line 645
    :cond_a
    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->setTextX(I)V

    .line 647
    const/4 v8, 0x0

    :goto_a
    const/16 v28, 0x3

    move/from16 v0, v28

    if-gt v8, v0, :cond_b

    .line 648
    rsub-int/lit8 v28, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v8

    aget-byte v29, v29, v30

    aput-byte v29, v10, v28

    .line 647
    add-int/lit8 v8, v8, 0x1

    goto :goto_a

    .line 649
    :cond_b
    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-static {v10, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v27

    .line 650
    .local v27, "textY":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v8

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    .line 652
    const/high16 v28, -0x80000000

    and-int v28, v28, v27

    const/high16 v29, -0x80000000

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_c

    .line 653
    xor-int/lit8 v28, v27, -0x1

    and-int/lit8 v28, v28, -0x1

    add-int/lit8 v28, v28, 0x1

    mul-int/lit8 v27, v28, -0x1

    .line 654
    if-gez v27, :cond_c

    .line 655
    move/from16 v0, v27

    neg-int v0, v0

    move/from16 v27, v0

    .line 658
    :cond_c
    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->setTextY(I)V

    .line 660
    const/4 v8, 0x0

    :goto_b
    const/16 v28, 0x3

    move/from16 v0, v28

    if-gt v8, v0, :cond_d

    .line 661
    rsub-int/lit8 v28, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v8

    aget-byte v29, v29, v30

    aput-byte v29, v10, v28

    .line 660
    add-int/lit8 v8, v8, 0x1

    goto :goto_b

    .line 662
    :cond_d
    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-static {v10, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v15

    .line 663
    .local v15, "numberOfChars":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v8

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    .line 664
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->setNumberOfChars(I)V

    .line 666
    const/4 v8, 0x0

    :goto_c
    const/16 v28, 0x3

    move/from16 v0, v28

    if-gt v8, v0, :cond_e

    .line 667
    rsub-int/lit8 v28, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v8

    aget-byte v29, v29, v30

    aput-byte v29, v10, v28

    .line 666
    add-int/lit8 v8, v8, 0x1

    goto :goto_c

    .line 668
    :cond_e
    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-static {v10, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v17

    .line 669
    .local v17, "offSet":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v8

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    .line 670
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->setOffString(I)V

    .line 672
    const/4 v8, 0x0

    :goto_d
    const/16 v28, 0x3

    move/from16 v0, v28

    if-gt v8, v0, :cond_f

    .line 673
    rsub-int/lit8 v28, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v8

    aget-byte v29, v29, v30

    aput-byte v29, v10, v28

    .line 672
    add-int/lit8 v8, v8, 0x1

    goto :goto_d

    .line 674
    :cond_f
    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-static {v10, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v18

    .line 675
    .local v18, "options":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v8

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    .line 676
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->setOptions(I)V

    .line 678
    const/4 v8, 0x0

    :goto_e
    const/16 v28, 0x3

    move/from16 v0, v28

    if-gt v8, v0, :cond_10

    .line 679
    rsub-int/lit8 v28, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v8

    aget-byte v29, v29, v30

    aput-byte v29, v10, v28

    .line 678
    add-int/lit8 v8, v8, 0x1

    goto :goto_e

    .line 680
    :cond_10
    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-static {v10, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v22

    .line 681
    .local v22, "textLeftTopX":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v8

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    .line 682
    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->setTextLeftTopX(I)V

    .line 684
    const/4 v8, 0x0

    :goto_f
    const/16 v28, 0x3

    move/from16 v0, v28

    if-gt v8, v0, :cond_11

    .line 685
    rsub-int/lit8 v28, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v8

    aget-byte v29, v29, v30

    aput-byte v29, v10, v28

    .line 684
    add-int/lit8 v8, v8, 0x1

    goto :goto_f

    .line 686
    :cond_11
    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-static {v10, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v23

    .line 687
    .local v23, "textLeftTopY":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v8

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    .line 688
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->setTextLeftTopY(I)V

    .line 690
    const/4 v8, 0x0

    :goto_10
    const/16 v28, 0x3

    move/from16 v0, v28

    if-gt v8, v0, :cond_12

    .line 691
    rsub-int/lit8 v28, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v8

    aget-byte v29, v29, v30

    aput-byte v29, v10, v28

    .line 690
    add-int/lit8 v8, v8, 0x1

    goto :goto_10

    .line 692
    :cond_12
    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-static {v10, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v24

    .line 693
    .local v24, "textRightBottomX":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v8

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    .line 694
    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->setTextRightBottomX(I)V

    .line 696
    const/4 v8, 0x0

    :goto_11
    const/16 v28, 0x3

    move/from16 v0, v28

    if-gt v8, v0, :cond_13

    .line 697
    rsub-int/lit8 v28, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v8

    aget-byte v29, v29, v30

    aput-byte v29, v10, v28

    .line 696
    add-int/lit8 v8, v8, 0x1

    goto :goto_11

    .line 698
    :cond_13
    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-static {v10, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v25

    .line 699
    .local v25, "textRightBottomY":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v8

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    .line 700
    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->setTextRightBottomY(I)V

    .line 702
    const/4 v8, 0x0

    :goto_12
    const/16 v28, 0x3

    move/from16 v0, v28

    if-gt v8, v0, :cond_14

    .line 703
    rsub-int/lit8 v28, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fileContent:[B

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v30, v0

    add-int v30, v30, v8

    aget-byte v29, v29, v30

    aput-byte v29, v10, v28

    .line 702
    add-int/lit8 v8, v8, 0x1

    goto :goto_12

    .line 704
    :cond_14
    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-static {v10, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v16

    .line 705
    .local v16, "offDx":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v8

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    .line 706
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->setOffDx(I)V

    .line 708
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offString:I

    move/from16 v29, v0

    add-int v28, v28, v29

    add-int/lit8 v28, v28, -0x4c

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    .line 709
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offString:I

    move/from16 v28, v0

    sub-int v28, v2, v28

    move/from16 v0, v28

    new-array v5, v0, [B

    .line 710
    .local v5, "charBuffer":[B
    const/4 v8, 0x0

    :goto_13
    array-length v0, v5

    move/from16 v28, v0

    move/from16 v0, v28

    if-ge v8, v0, :cond_15

    .line 711
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fileContent:[B

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v29, v0

    add-int v29, v29, v8

    aget-byte v28, v28, v29

    aput-byte v28, v5, v8

    .line 710
    add-int/lit8 v8, v8, 0x1

    goto :goto_13

    .line 712
    :cond_15
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    move/from16 v28, v0

    add-int v28, v28, v8

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    .line 716
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offString:I

    move/from16 v28, v0

    sub-int v28, v2, v28

    move/from16 v0, v28

    new-array v0, v0, [B

    move-object/from16 v19, v0

    .line 717
    .local v19, "reqCharBuffer":[B
    const/4 v8, 0x0

    const/4 v11, 0x0

    .local v11, "k":I
    :goto_14
    array-length v0, v5

    move/from16 v28, v0

    move/from16 v0, v28

    if-ge v8, v0, :cond_17

    .line 718
    aget-byte v28, v5, v8

    if-eqz v28, :cond_16

    .line 719
    add-int/lit8 v12, v11, 0x1

    .end local v11    # "k":I
    .local v12, "k":I
    aget-byte v28, v5, v8

    aput-byte v28, v19, v11

    move v11, v12

    .line 717
    .end local v12    # "k":I
    .restart local v11    # "k":I
    :cond_16
    add-int/lit8 v8, v8, 0x1

    goto :goto_14

    .line 723
    :cond_17
    new-array v4, v15, [B

    .line 724
    .local v4, "actualCharBuffer":[B
    const/4 v8, 0x0

    :goto_15
    if-ge v8, v15, :cond_18

    .line 725
    aget-byte v28, v19, v8

    aput-byte v28, v4, v8

    .line 724
    add-int/lit8 v8, v8, 0x1

    goto :goto_15

    .line 727
    :cond_18
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->setStringBuffer([B)V

    .line 735
    return-void
.end method

.method public render(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 755
    const/4 v4, 0x0

    .line 756
    .local v4, "i":I
    new-instance v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;-><init>()V

    .line 767
    .local v2, "PaintTextPropertiesObject":Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;
    const/4 v6, 0x0

    .line 768
    .local v6, "myString":Ljava/lang/String;
    new-instance v6, Ljava/lang/String;

    .end local v6    # "myString":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->getStringBuffer()[B

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>([B)V

    .line 770
    .restart local v6    # "myString":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 771
    .local v3, "emrSize":I
    if-lez v3, :cond_5

    .line 772
    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_0

    .line 773
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v10, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandleFont:I

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->gethandleNumber()I

    move-result v7

    if-ne v10, v7, :cond_8

    .line 778
    :cond_0
    if-ge v4, v3, :cond_5

    .line 779
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;

    .line 781
    .local v0, "CurrentFontObject":Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v1

    .line 783
    .local v1, "MyPaintBrush":Landroid/graphics/Paint;
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->getLogFontEx()Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->getHeight()I

    move-result v7

    int-to-float v7, v7

    iget-object v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v10, v10, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v5, v7, v10

    .line 785
    .local v5, "mySize":F
    const/4 v7, 0x0

    cmpg-float v7, v5, v7

    if-gez v7, :cond_1

    .line 786
    neg-float v5, v5

    .line 791
    :cond_1
    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 793
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->getLogFontEx()Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->getStrikeOut()B

    move-result v7

    if-ne v7, v8, :cond_9

    move v7, v8

    :goto_1
    invoke-virtual {v1, v7}, Landroid/graphics/Paint;->setStrikeThruText(Z)V

    .line 795
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->getLogFontEx()Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->getUnderline()B

    move-result v7

    if-ne v7, v8, :cond_2

    move v9, v8

    :cond_2
    invoke-virtual {v1, v9}, Landroid/graphics/Paint;->setUnderlineText(Z)V

    .line 797
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->getLogFontEx()Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->getItalic()B

    move-result v7

    if-ne v7, v8, :cond_3

    .line 798
    const/high16 v7, -0x41800000    # -0.25f

    invoke-virtual {v1, v7}, Landroid/graphics/Paint;->setTextSkewX(F)V

    .line 800
    :cond_3
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EMR_TextAlignObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;

    if-eqz v7, :cond_b

    .line 801
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EMR_TextAlignObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->getTextAlignmentMode()I

    move-result v7

    sget-object v8, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->TA_CENTER:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->getTextAlignmentModeFlag()I

    move-result v8

    and-int/2addr v7, v8

    sget-object v8, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->TA_CENTER:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->getTextAlignmentModeFlag()I

    move-result v8

    if-ne v7, v8, :cond_a

    .line 804
    sget-object v7, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v7}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 811
    :cond_4
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->getTextX()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float/2addr v7, v8

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->getTextY()I

    move-result v8

    int-to-float v8, v8

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v9, v9, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float/2addr v8, v9

    invoke-virtual {p1, v6, v7, v8, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 822
    .end local v0    # "CurrentFontObject":Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;
    .end local v1    # "MyPaintBrush":Landroid/graphics/Paint;
    .end local v5    # "mySize":F
    :cond_5
    :goto_3
    if-eq v4, v3, :cond_6

    if-nez v3, :cond_7

    .line 824
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->getTextX()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float/2addr v7, v8

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->getTextY()I

    move-result v8

    int-to-float v8, v8

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v9, v9, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float/2addr v8, v9

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v9

    invoke-virtual {p1, v6, v7, v8, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 829
    :cond_7
    return-void

    .line 772
    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .restart local v0    # "CurrentFontObject":Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;
    .restart local v1    # "MyPaintBrush":Landroid/graphics/Paint;
    .restart local v5    # "mySize":F
    :cond_9
    move v7, v9

    .line 793
    goto :goto_1

    .line 805
    :cond_a
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EMR_TextAlignObject:Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->getTextAlignmentMode()I

    move-result v7

    sget-object v8, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->TA_RIGHT:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->getTextAlignmentModeFlag()I

    move-result v8

    and-int/2addr v7, v8

    sget-object v8, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->TA_RIGHT:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->getTextAlignmentModeFlag()I

    move-result v8

    if-ne v7, v8, :cond_4

    .line 809
    sget-object v7, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v7}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto :goto_2

    .line 814
    :cond_b
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->getTextX()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float/2addr v7, v8

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->getTextY()I

    move-result v8

    int-to-float v8, v8

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v9, v9, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float/2addr v8, v9

    invoke-virtual {v1}, Landroid/graphics/Paint;->ascent()F

    move-result v9

    sub-float/2addr v8, v9

    invoke-virtual {p1, v6, v7, v8, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_3
.end method

.method public setDxBuffer([B)V
    .locals 0
    .param p1, "dxBuffer"    # [B

    .prologue
    .line 532
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->DxBuffer:[B

    .line 533
    return-void
.end method

.method public setExScale(F)V
    .locals 0
    .param p1, "exScale"    # F

    .prologue
    .line 285
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->exScale:F

    .line 286
    return-void
.end method

.method public setEyScale(F)V
    .locals 0
    .param p1, "eyScale"    # F

    .prologue
    .line 304
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->eyScale:F

    .line 305
    return-void
.end method

.method public setFontBrush(I)V
    .locals 0
    .param p1, "Brsh"    # I

    .prologue
    .line 570
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fontBrush:I

    .line 571
    return-void
.end method

.method public setFontIndirectIndex(I)V
    .locals 0
    .param p1, "fontIndirectIndex"    # I

    .prologue
    .line 118
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->fontIndirectIndex:I

    .line 119
    return-void
.end method

.method public setLeftTopX(I)V
    .locals 0
    .param p1, "leftTopX"    # I

    .prologue
    .line 190
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->leftTopX:I

    .line 191
    return-void
.end method

.method public setLeftTopY(I)V
    .locals 0
    .param p1, "leftTopY"    # I

    .prologue
    .line 209
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->leftTopY:I

    .line 210
    return-void
.end method

.method public setNumberOfChars(I)V
    .locals 0
    .param p1, "numberOfChars"    # I

    .prologue
    .line 361
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->numberOfChars:I

    .line 362
    return-void
.end method

.method public setOffDx(I)V
    .locals 0
    .param p1, "offDx"    # I

    .prologue
    .line 494
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offDx:I

    .line 495
    return-void
.end method

.method public setOffString(I)V
    .locals 0
    .param p1, "offSet"    # I

    .prologue
    .line 380
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offString:I

    .line 381
    return-void
.end method

.method public setOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 551
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->offset:I

    .line 552
    return-void
.end method

.method public setOptions(I)V
    .locals 0
    .param p1, "options"    # I

    .prologue
    .line 399
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->options:I

    .line 400
    return-void
.end method

.method public setRightBottomX(I)V
    .locals 0
    .param p1, "rightBottomX"    # I

    .prologue
    .line 228
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->rightBottomX:I

    .line 229
    return-void
.end method

.method public setRightBottomY(I)V
    .locals 0
    .param p1, "rightBottomY"    # I

    .prologue
    .line 247
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->rightBottomY:I

    .line 248
    return-void
.end method

.method public setSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 171
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->Size:I

    .line 172
    return-void
.end method

.method public setStringBuffer([B)V
    .locals 0
    .param p1, "stringBuffer"    # [B

    .prologue
    .line 513
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->StringBuffer:[B

    .line 514
    return-void
.end method

.method public setTextLeftTopX(I)V
    .locals 0
    .param p1, "textLeftTopX"    # I

    .prologue
    .line 418
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->textLeftTopX:I

    .line 419
    return-void
.end method

.method public setTextLeftTopY(I)V
    .locals 0
    .param p1, "textLeftTopY"    # I

    .prologue
    .line 437
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->textLeftTopY:I

    .line 438
    return-void
.end method

.method public setTextRightBottomX(I)V
    .locals 0
    .param p1, "textRightBottomX"    # I

    .prologue
    .line 456
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->textRightBottomX:I

    .line 457
    return-void
.end method

.method public setTextRightBottomY(I)V
    .locals 0
    .param p1, "textRightBottomY"    # I

    .prologue
    .line 475
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->textRightBottomY:I

    .line 476
    return-void
.end method

.method public setTextX(I)V
    .locals 0
    .param p1, "textX"    # I

    .prologue
    .line 323
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->textX:I

    .line 324
    return-void
.end method

.method public setTextY(I)V
    .locals 0
    .param p1, "textY"    # I

    .prologue
    .line 342
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->textY:I

    .line 343
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 152
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->Type:I

    .line 153
    return-void
.end method

.method public setiGraphicsMode(I)V
    .locals 0
    .param p1, "iGraphicsMode"    # I

    .prologue
    .line 266
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_ExtTextOutA;->iGraphicsMode:I

    .line 267
    return-void
.end method
