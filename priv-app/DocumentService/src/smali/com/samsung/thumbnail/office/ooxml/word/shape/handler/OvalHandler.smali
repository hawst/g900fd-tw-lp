.class public Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/OvalHandler;
.super Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;
.source "OvalHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 15
    const/16 v0, 0x28

    const-string/jumbo v1, "oval"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;-><init>(ILjava/lang/String;)V

    .line 16
    return-void
.end method


# virtual methods
.method public getColor(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "style"    # Ljava/lang/String;

    .prologue
    .line 40
    if-eqz p1, :cond_0

    const-string/jumbo v0, "#"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    const-string/jumbo v0, "#"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const-string/jumbo v1, "#"

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x7

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 43
    :cond_0
    return-object p1
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 31
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 37
    return-void
.end method

.method protected init()V
    .locals 0

    .prologue
    .line 19
    invoke-super {p0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->init()V

    .line 20
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 25
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 26
    return-void
.end method
