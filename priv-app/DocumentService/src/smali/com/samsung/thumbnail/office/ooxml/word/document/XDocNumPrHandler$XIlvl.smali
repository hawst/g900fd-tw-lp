.class Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler$XIlvl;
.super Ljava/lang/Object;
.source "XDocNumPrHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "XIlvl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler;


# direct methods
.method constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler$XIlvl;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V
    .locals 3
    .param p1, "value"    # Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;

    .prologue
    .line 70
    instance-of v1, p1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 71
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;

    .line 72
    .local v0, "ilvl":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler$XIlvl;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler;

    # getter for: Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler;->numberPropery:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;
    invoke-static {v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler;->access$000(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler;)Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;->getValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;->setIlvl(I)V

    .line 74
    .end local v0    # "ilvl":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;
    :cond_0
    return-void
.end method
