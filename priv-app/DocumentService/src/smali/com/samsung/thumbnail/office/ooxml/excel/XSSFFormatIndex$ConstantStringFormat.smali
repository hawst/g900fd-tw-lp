.class Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex$ConstantStringFormat;
.super Ljava/text/Format;
.source "XSSFFormatIndex.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ConstantStringFormat"
.end annotation


# instance fields
.field private df:Ljava/text/DecimalFormat;

.field private final str:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 339
    invoke-direct {p0}, Ljava/text/Format;-><init>()V

    .line 336
    const-string/jumbo v0, "##########"

    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->createIntegerOnlyFormat(Ljava/lang/String;)Ljava/text/DecimalFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex$ConstantStringFormat;->df:Ljava/text/DecimalFormat;

    .line 340
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex$ConstantStringFormat;->str:Ljava/lang/String;

    .line 342
    return-void
.end method


# virtual methods
.method public format(Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "toAppendTo"    # Ljava/lang/StringBuffer;
    .param p3, "pos"    # Ljava/text/FieldPosition;

    .prologue
    .line 347
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex$ConstantStringFormat;->str:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public parseObject(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Object;
    .locals 1
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "pos"    # Ljava/text/ParsePosition;

    .prologue
    .line 352
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex$ConstantStringFormat;->df:Ljava/text/DecimalFormat;

    invoke-virtual {v0, p1, p2}, Ljava/text/DecimalFormat;->parseObject(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
