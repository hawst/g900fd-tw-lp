.class public Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;
.super Ljava/lang/Object;
.source "XSSFExcelSharedString.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "XSSFExcelSharedString"


# instance fields
.field private currentSI:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

.field private excelVar:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

.field private extextvalue:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;",
            ">;"
        }
    .end annotation
.end field

.field private fileName:Ljava/io/File;

.field private workSheet:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;)V
    .locals 2
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "workSheet"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;
    .param p3, "excelVar"    # Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->currentSI:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->extextvalue:Ljava/util/HashMap;

    .line 60
    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->workSheet:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    .line 76
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->excelVar:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    .line 78
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->workSheet:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    .line 79
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;)V
    .locals 2
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "mContext"    # Landroid/content/Context;
    .param p3, "excelVar"    # Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->currentSI:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->extextvalue:Ljava/util/HashMap;

    .line 60
    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->workSheet:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    .line 68
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->excelVar:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    .line 70
    return-void
.end method

.method private parseExcel(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "is"    # Ljava/io/InputStream;

    .prologue
    .line 90
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringParser;

    invoke-direct {v0, p0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;)V

    .line 92
    .local v0, "excelParser":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringParser;
    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringParser;->parse(Ljava/io/InputStream;)V

    .line 93
    return-void
.end method


# virtual methods
.method public addExSharedStringSIValue(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;I)V
    .locals 2
    .param p1, "extext"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;
    .param p2, "index"    # I

    .prologue
    .line 127
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->extextvalue:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    return-void
.end method

.method public getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->currentSI:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    return-object v0
.end method

.method public getExSharedStringSIValue()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->extextvalue:Ljava/util/HashMap;

    return-object v0
.end method

.method public getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->excelVar:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    return-object v0
.end method

.method public getSheet()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->workSheet:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    return-object v0
.end method

.method protected onSharedStringRead(Lorg/apache/poi/openxml4j/opc/PackagePart;)V
    .locals 3
    .param p1, "part"    # Lorg/apache/poi/openxml4j/opc/PackagePart;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->excelVar:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    const/4 v2, 0x0

    iput v2, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    .line 97
    invoke-virtual {p1}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 99
    .local v0, "is":Ljava/io/InputStream;
    if-eqz v0, :cond_1

    .line 100
    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->parseExcel(Ljava/io/InputStream;)V

    .line 106
    :goto_0
    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 109
    :cond_0
    return-void

    .line 102
    :cond_1
    invoke-virtual {p1}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getEntryName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->fileName:Ljava/io/File;

    invoke-static {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getInputStream(Ljava/lang/String;Ljava/io/File;)Ljava/io/InputStream;

    move-result-object v0

    .line 103
    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->parseExcel(Ljava/io/InputStream;)V

    goto :goto_0
.end method

.method public setCurrentSI(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;)V
    .locals 0
    .param p1, "inCurrentSI"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->currentSI:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    .line 118
    return-void
.end method

.method public setFileName(Ljava/io/File;)V
    .locals 0
    .param p1, "fileName"    # Ljava/io/File;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->fileName:Ljava/io/File;

    .line 83
    return-void
.end method
