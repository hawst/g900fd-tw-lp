.class public Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDrawingHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DiagramDrawingHandler.java"


# instance fields
.field dataModel:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

.field private xwpfDiagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;


# direct methods
.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V
    .locals 4
    .param p1, "ID"    # I
    .param p2, "xwpfDiagram"    # Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;
    .param p3, "dataModel"    # Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;
    .param p4, "theme"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    .param p5, "docType"    # Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    .prologue
    .line 25
    const-string/jumbo v3, "drawing"

    invoke-direct {p0, p1, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 27
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDrawingHandler;->dataModel:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

    .line 29
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDrawingHandler;->xwpfDiagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    .line 31
    const/4 v3, 0x1

    new-array v1, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 33
    .local v1, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpTreeHandler;

    invoke-direct {v2, p1, p2, p4, p5}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpTreeHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 35
    .local v2, "spTreeHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpTreeHandler;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;

    const-string/jumbo v3, "spTree"

    invoke-direct {v0, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 37
    .local v0, "dsp1Seq":Lcom/samsung/thumbnail/office/ooxml/diagram/DspSeqDescriptor;
    const/4 v3, 0x0

    aput-object v0, v1, v3

    .line 39
    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDrawingHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 40
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDrawingHandler;->dataModel:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDrawingHandler;->xwpfDiagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;->setDiagram(Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;)V

    .line 52
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 45
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 46
    return-void
.end method
