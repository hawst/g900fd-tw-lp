.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathArcToHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "PathArcToHandler.java"


# instance fields
.field private pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V
    .locals 2
    .param p1, "pathDesc"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    .prologue
    .line 15
    const/16 v0, 0x1f

    const-string/jumbo v1, "arcTo"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 16
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathArcToHandler;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    .line 17
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 8
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    const/4 v7, 0x1

    .line 22
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 25
    const/4 v5, 0x4

    new-array v0, v5, [Ljava/lang/String;

    .line 26
    .local v0, "attrsArray":[Ljava/lang/String;
    const-string/jumbo v5, "wR"

    invoke-virtual {p0, p3, v5, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathArcToHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v4

    .line 27
    .local v4, "widthRadius":Ljava/lang/String;
    const/4 v5, 0x0

    aput-object v4, v0, v5

    .line 28
    const-string/jumbo v5, "hR"

    invoke-virtual {p0, p3, v5, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathArcToHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 29
    .local v1, "heightRadius":Ljava/lang/String;
    aput-object v1, v0, v7

    .line 30
    const-string/jumbo v5, "stAng"

    invoke-virtual {p0, p3, v5, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathArcToHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    .line 32
    .local v2, "startAngle":Ljava/lang/String;
    const/4 v5, 0x2

    aput-object v2, v0, v5

    .line 33
    const-string/jumbo v5, "swAng"

    invoke-virtual {p0, p3, v5, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathArcToHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v3

    .line 35
    .local v3, "swingAngle":Ljava/lang/String;
    const/4 v5, 0x3

    aput-object v3, v0, v5

    .line 36
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathArcToHandler;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    const/4 v6, 0x5

    invoke-virtual {v5, v6, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->addSegmentstoPath(I[Ljava/lang/String;)V

    .line 37
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathArcToHandler;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    invoke-virtual {v5, v7}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->setArcTo(Z)V

    .line 38
    return-void
.end method
