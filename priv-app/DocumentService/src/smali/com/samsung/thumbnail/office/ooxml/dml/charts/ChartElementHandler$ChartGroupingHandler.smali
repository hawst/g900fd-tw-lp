.class public Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler$ChartGroupingHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLValueHandler;
.source "ChartElementHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ChartGroupingHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;)V
    .locals 2

    .prologue
    .line 75
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler$ChartGroupingHandler;->this$0:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;

    .line 76
    const/16 v0, 0x23

    const-string/jumbo v1, "grouping"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLValueHandler;-><init>(ILjava/lang/String;)V

    .line 77
    return-void
.end method


# virtual methods
.method public handleValue(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler$ChartGroupingHandler;->this$0:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;

    # getter for: Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;->chart:Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;
    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;->access$000(Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;)Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler$ChartGroupingHandler;->this$0:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;

    # getter for: Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;->chart:Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;
    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;->access$000(Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;)Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    move-result-object v0

    invoke-static {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->setGrouping(Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;)V

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler$ChartGroupingHandler;->this$0:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;

    # getter for: Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;->xssfChart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;
    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;->access$100(Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;)Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 86
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler$ChartGroupingHandler;->this$0:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;

    # getter for: Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;->xssfChart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;
    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;->access$100(Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;)Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    move-result-object v0

    invoke-static {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->setGrouping(Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;)V

    .line 88
    :cond_1
    return-void
.end method
