.class public Lcom/samsung/thumbnail/office/point/wordart/TexturedText;
.super Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;
.source "TexturedText.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private assets:Landroid/content/res/AssetManager;

.field private mDrawOnCanvas:Z

.field private mLeftMarg:I

.field private mTopMarg:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/io/File;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "folderPath"    # Ljava/io/File;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;-><init>()V

    .line 32
    const-string/jumbo v0, "TexturedText"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->TAG:Ljava/lang/String;

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->mDrawOnCanvas:Z

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->assets:Landroid/content/res/AssetManager;

    .line 42
    iput-object p2, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->folderPath:Ljava/io/File;

    .line 43
    return-void
.end method


# virtual methods
.method protected applyTexturedTextEffect(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;Landroid/graphics/Bitmap;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p3, "texture"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v7, 0x0

    .line 107
    iget-object v4, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->assets:Landroid/content/res/AssetManager;

    const-string/jumbo v5, "impact.ttf"

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    .line 109
    .local v3, "tf":Landroid/graphics/Typeface;
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 111
    .local v2, "paint":Landroid/graphics/Paint;
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 112
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 113
    const v4, -0xffff01

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 115
    iget v4, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->adjval0:I

    if-nez v4, :cond_0

    .line 116
    const/16 v4, 0x2a30

    iput v4, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->adjval0:I

    .line 127
    :cond_0
    iget v4, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->width:F

    float-to-int v4, v4

    iget v5, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->height:F

    float-to-int v5, v5

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 129
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 130
    .local v1, "canvasTemp":Landroid/graphics/Canvas;
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 135
    new-instance v4, Landroid/graphics/PorterDuffXfermode;

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v4, v5}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 136
    invoke-virtual {v1, p3, v7, v7, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 137
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 139
    invoke-virtual {p1, v0, v7, v7, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 141
    return-void
.end method

.method protected draw(Landroid/graphics/Canvas;I)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeCount"    # I

    .prologue
    const/4 v1, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    .line 82
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->mDrawOnCanvas:Z

    if-nez v0, :cond_0

    .line 83
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 85
    .local v5, "mtx":Landroid/graphics/Matrix;
    iget v0, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->rotation:I

    int-to-float v0, v0

    iget v2, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->width:F

    div-float/2addr v2, v4

    iget v3, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->height:F

    div-float/2addr v3, v4

    invoke-virtual {v5, v0, v2, v3}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 87
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->bitmap:Landroid/graphics/Bitmap;

    iget v3, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->bitmapWidth:I

    iget v4, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->bitmapHight:I

    const/4 v6, 0x1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 91
    .local v9, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v8, Ljava/io/FileOutputStream;

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->folderPath:Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "pic_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->shapeName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".png"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v8, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 95
    .local v8, "out":Ljava/io/FileOutputStream;
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v1, 0x64

    invoke-virtual {v9, v0, v1, v8}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 96
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    .end local v5    # "mtx":Landroid/graphics/Matrix;
    .end local v8    # "out":Ljava/io/FileOutputStream;
    .end local v9    # "rotatedBMP":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-void

    .line 97
    .restart local v5    # "mtx":Landroid/graphics/Matrix;
    .restart local v9    # "rotatedBMP":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v7

    .line 98
    .local v7, "e":Ljava/lang/Exception;
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Exception while writing pictures to out file"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V
    .locals 0
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 47
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 48
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->draw(I)V

    .line 49
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;I)V
    .locals 1
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 52
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 53
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->draw(I)V

    .line 54
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIII)V
    .locals 2
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # I
    .param p7, "height"    # I

    .prologue
    .line 58
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->mDrawOnCanvas:Z

    .line 60
    iput p4, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->mLeftMarg:I

    .line 61
    iput p5, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->mTopMarg:I

    .line 62
    int-to-float v0, p6

    iput v0, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->width:F

    .line 63
    int-to-float v0, p7

    iput v0, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->height:F

    .line 64
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 70
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->draw(Landroid/graphics/Canvas;I)V

    .line 71
    iget v0, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/wordart/TexturedText;->mDrawOnCanvas:Z

    .line 73
    return-void
.end method
