.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabsHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XDocTabsHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabHandler$ITabContObserver;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 17
    const-string/jumbo v3, "tabs"

    invoke-direct {p0, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 19
    const/4 v3, 0x1

    new-array v0, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 21
    .local v0, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabHandler;

    invoke-direct {v1, p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabHandler$ITabContObserver;)V

    .line 22
    .local v1, "tabHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabHandler;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v3, "tab"

    invoke-direct {v2, v3, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 24
    .local v2, "tabsSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v3, 0x0

    aput-object v2, v0, v3

    .line 26
    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabsHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 27
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 41
    return-void
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 34
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 35
    return-void
.end method

.method public setTabContent(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;I)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "position"    # I

    .prologue
    .line 45
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->addTabCharacter(Ljava/lang/String;I)V

    .line 48
    :cond_0
    return-void
.end method
