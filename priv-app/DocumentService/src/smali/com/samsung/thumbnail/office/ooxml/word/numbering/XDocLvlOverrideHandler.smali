.class public Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XDocLvlOverrideHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$IXDocLvlConsumer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler$1;,
        Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler$ILvlOverride;,
        Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler$StartOverride;
    }
.end annotation


# instance fields
.field private consumer:Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler$ILvlOverride;

.field private lvlOverride:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler$ILvlOverride;)V
    .locals 7
    .param p1, "consumer"    # Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler$ILvlOverride;

    .prologue
    .line 31
    const-string/jumbo v6, "lvlOverride"

    invoke-direct {p0, v6}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 32
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler$ILvlOverride;

    .line 34
    const/4 v6, 0x2

    new-array v2, v6, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 36
    .local v2, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler$StartOverride;

    const/4 v6, 0x0

    invoke-direct {v3, p0, v6}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler$StartOverride;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler$1;)V

    .line 37
    .local v3, "startOverride":Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler$StartOverride;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;

    const-string/jumbo v6, "startOverride"

    invoke-direct {v4, v6, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 39
    .local v4, "startOverrideHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v6, "startOverride"

    invoke-direct {v5, v6, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 41
    .local v5, "startOverrideSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v6, 0x0

    aput-object v5, v2, v6

    .line 43
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;

    invoke-direct {v0, p0}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$IXDocLvlConsumer;)V

    .line 44
    .local v0, "lvlHandler":Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v6, "lvl"

    invoke-direct {v1, v6, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 46
    .local v1, "lvlSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v6, 0x1

    aput-object v1, v2, v6

    .line 48
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 49
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;->lvlOverride:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;

    return-object v0
.end method


# virtual methods
.method public consumeLvl(Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;)V
    .locals 1
    .param p1, "numLvl"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;->lvlOverride:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;->setLvl(Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;)V

    .line 82
    return-void
.end method

.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 65
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler$ILvlOverride;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;->lvlOverride:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;

    invoke-interface {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler$ILvlOverride;->addLvlOverride(Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;)V

    .line 67
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 54
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 55
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;->lvlOverride:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;

    .line 57
    const-string/jumbo v1, "ilvl"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 58
    .local v0, "val":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 59
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;->lvlOverride:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;->setIlvl(I)V

    .line 61
    :cond_0
    return-void
.end method
