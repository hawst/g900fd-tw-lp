.class public Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyPrHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DiagramTxBodyPrHandler.java"


# instance fields
.field private bodyPr:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;

.field private iBodyPr:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler$IBodyPrHandler;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler$IBodyPrHandler;)V
    .locals 2
    .param p1, "iBodyPr"    # Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler$IBodyPrHandler;

    .prologue
    .line 21
    const/16 v0, 0x1f

    const-string/jumbo v1, "bodyPr"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 22
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyPrHandler;->iBodyPr:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler$IBodyPrHandler;

    .line 23
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyPrHandler;->iBodyPr:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler$IBodyPrHandler;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyPrHandler;->bodyPr:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;

    invoke-interface {v0, p1, v1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyHandler$IBodyPrHandler;->setBodyProperty(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;)V

    .line 53
    return-void
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 39
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 40
    invoke-virtual {p0, p2, p1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyPrHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "normAutofit"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42
    const-string/jumbo v1, "fontScale"

    invoke-virtual {p0, p3, v1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyPrHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 43
    .local v0, "val":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 44
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyPrHandler;->bodyPr:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    div-int/lit16 v2, v2, 0x3e8

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;->setFontScale(I)V

    .line 47
    .end local v0    # "val":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 28
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 29
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyPrHandler;->bodyPr:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;

    .line 30
    const-string/jumbo v1, "anchor"

    invoke-virtual {p0, p3, v1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyPrHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 31
    .local v0, "vAlign":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 32
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyPrHandler;->bodyPr:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;->setvAlign(Ljava/lang/String;)V

    .line 34
    :cond_0
    return-void
.end method
