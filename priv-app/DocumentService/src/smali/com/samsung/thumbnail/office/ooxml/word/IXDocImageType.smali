.class public interface abstract Lcom/samsung/thumbnail/office/ooxml/word/IXDocImageType;
.super Ljava/lang/Object;
.source "IXDocImageType.java"


# static fields
.field public static final PICTURE_TYPE_DIB:I = 0x7

.field public static final PICTURE_TYPE_EMF:I = 0x2

.field public static final PICTURE_TYPE_GIF:I = 0x8

.field public static final PICTURE_TYPE_JPEG:I = 0x5

.field public static final PICTURE_TYPE_PICT:I = 0x4

.field public static final PICTURE_TYPE_PNG:I = 0x6

.field public static final PICTURE_TYPE_WMF:I = 0x3
