.class public Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;
.super Ljava/lang/Object;
.source "ExcelChartInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/excel/ExcelChartInfo$1;
    }
.end annotation


# static fields
.field public static final AREAGRAPH:Ljava/lang/String; = "Area"

.field public static final BARGRAPH:Ljava/lang/String; = "Bar"

.field public static final LINEGRAPH:Ljava/lang/String; = "Line"

.field public static final PIEGRAPH:Ljava/lang/String; = "Pie"

.field public static final SCATTERGRAPH:Ljava/lang/String; = "Scatter"

.field public static final UNKNOWN:Ljava/lang/String; = "unknown"


# instance fields
.field public chartCategoryNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public chartSeriesNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public chartType:Ljava/lang/String;

.field public chartValues:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private field_height:I

.field private field_width:I

.field private field_x:I

.field private field_y:I


# direct methods
.method public constructor <init>(Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFChartType;)V
    .locals 2
    .param p1, "hssfcharttype"    # Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFChartType;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartValues:Ljava/util/ArrayList;

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartSeriesNames:Ljava/util/ArrayList;

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartCategoryNames:Ljava/util/ArrayList;

    .line 30
    sget-object v0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo$1;->$SwitchMap$org$apache$poi$hssf$usermodel$HSSFChart$HSSFChartType:[I

    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFChartType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 47
    const-string/jumbo v0, "unknown"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartType:Ljava/lang/String;

    .line 49
    :goto_0
    return-void

    .line 32
    :pswitch_0
    const-string/jumbo v0, "Bar"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartType:Ljava/lang/String;

    goto :goto_0

    .line 35
    :pswitch_1
    const-string/jumbo v0, "Pie"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartType:Ljava/lang/String;

    goto :goto_0

    .line 38
    :pswitch_2
    const-string/jumbo v0, "Line"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartType:Ljava/lang/String;

    goto :goto_0

    .line 41
    :pswitch_3
    const-string/jumbo v0, "Area"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartType:Ljava/lang/String;

    goto :goto_0

    .line 44
    :pswitch_4
    const-string/jumbo v0, "Scatter"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartType:Ljava/lang/String;

    goto :goto_0

    .line 30
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public getHeight()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->field_height:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->field_width:I

    return v0
.end method

.method public getX()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->field_x:I

    return v0
.end method

.method public getY()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->field_y:I

    return v0
.end method

.method public setHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 104
    iput p1, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->field_height:I

    .line 105
    return-void
.end method

.method public setWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 90
    iput p1, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->field_width:I

    .line 91
    return-void
.end method

.method public setX(I)V
    .locals 0
    .param p1, "x"    # I

    .prologue
    .line 62
    iput p1, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->field_x:I

    .line 63
    return-void
.end method

.method public setY(I)V
    .locals 0
    .param p1, "y"    # I

    .prologue
    .line 76
    iput p1, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->field_y:I

    .line 77
    return-void
.end method
