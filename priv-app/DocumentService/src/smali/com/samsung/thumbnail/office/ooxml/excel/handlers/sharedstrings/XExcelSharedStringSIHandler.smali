.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringSIHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelSharedStringSIHandler.java"


# instance fields
.field private siElement:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;)V
    .locals 4
    .param p1, "sharedstringobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    .prologue
    .line 23
    const-string/jumbo v2, "si"

    invoke-direct {p0, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 19
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringSIHandler;->siElement:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    .line 24
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringRHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringRHandler;-><init>()V

    .line 25
    .local v0, "rhandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringRHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringSIHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "r"

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringTHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringTHandler;-><init>()V

    .line 27
    .local v1, "thandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringTHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringSIHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "t"

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v2

    const/4 v3, 0x0

    iput v3, v2, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    .line 30
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;-><init>()V

    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringSIHandler;->siElement:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    .line 31
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringSIHandler;->siElement:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->setCurrentSI(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;)V

    .line 33
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 5
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 38
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v1

    .line 40
    .local v1, "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    const-string/jumbo v0, ""

    .line 41
    .local v0, "text":Ljava/lang/String;
    iget-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tempString:Ljava/lang/String;

    .line 43
    const-string/jumbo v2, ""

    iput-object v2, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tempString:Ljava/lang/String;

    .line 46
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getSheet()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetVTextid()Ljava/util/ArrayList;

    move-result-object v2

    iget v3, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->hashIndex:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 49
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->setText(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v3

    iget v4, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->hashIndex:I

    invoke-virtual {v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->addExSharedStringSIValue(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;I)V

    .line 56
    :cond_0
    iget v2, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->hashIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->hashIndex:I

    .line 58
    const/4 v2, 0x0

    iput v2, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    .line 59
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;-><init>()V

    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringSIHandler;->siElement:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    .line 60
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringSIHandler;->siElement:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->setCurrentSI(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;)V

    .line 62
    return-void
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 67
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 68
    return-void
.end method
