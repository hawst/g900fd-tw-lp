.class public Lcom/samsung/thumbnail/office/point/shapes/BraceShape;
.super Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;
.source "BraceShape.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mDrawOnCanvas:Z

.field private mLeftMarg:I

.field private mTopMarg:I


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "folderPath"    # Ljava/io/File;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;-><init>()V

    .line 28
    const-string/jumbo v0, "BraceShape"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->TAG:Ljava/lang/String;

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mDrawOnCanvas:Z

    .line 34
    iput-object p1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->folderPath:Ljava/io/File;

    .line 35
    return-void
.end method


# virtual methods
.method protected draw(Landroid/graphics/Canvas;I)V
    .locals 13
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeCount"    # I

    .prologue
    .line 161
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->width:F

    const v1, 0x46a8c000    # 21600.0f

    div-float v9, v0, v1

    .line 162
    .local v9, "mulFactor_Width":F
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->height:F

    const v1, 0x46a8c000    # 21600.0f

    div-float v8, v0, v1

    .line 164
    .local v8, "mulFactor_Height":F
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->shapeName:Ljava/lang/String;

    const-string/jumbo v1, "LeftBrace"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 173
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    if-nez v0, :cond_0

    .line 174
    const/16 v0, 0x708

    iput v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    .line 175
    :cond_0
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval1:I

    if-nez v0, :cond_1

    .line 176
    const/16 v0, 0x2a30

    iput v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval1:I

    .line 179
    :cond_1
    new-instance v11, Landroid/graphics/Path;

    invoke-direct {v11}, Landroid/graphics/Path;-><init>()V

    .line 180
    .local v11, "path":Landroid/graphics/Path;
    const v0, 0x46a8c000    # 21600.0f

    mul-float/2addr v0, v9

    const/4 v1, 0x0

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 181
    const v0, 0x4628c000    # 10800.0f

    mul-float/2addr v0, v9

    const/4 v1, 0x0

    const v2, 0x4628c000    # 10800.0f

    mul-float/2addr v2, v9

    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v3, v3

    mul-float/2addr v3, v8

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 183
    const v0, 0x4628c000    # 10800.0f

    mul-float/2addr v0, v9

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval1:I

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 185
    const v0, 0x4628c000    # 10800.0f

    mul-float/2addr v0, v9

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval1:I

    int-to-float v1, v1

    mul-float/2addr v1, v8

    const/4 v2, 0x0

    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval1:I

    int-to-float v3, v3

    mul-float/2addr v3, v8

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 187
    const v0, 0x4628c000    # 10800.0f

    mul-float/2addr v0, v9

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval1:I

    int-to-float v1, v1

    mul-float/2addr v1, v8

    const v2, 0x4628c000    # 10800.0f

    mul-float/2addr v2, v9

    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval1:I

    iget v4, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v3, v8

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 190
    const v0, 0x4628c000    # 10800.0f

    mul-float/2addr v0, v9

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    rsub-int v1, v1, 0x5460

    int-to-float v1, v1

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 192
    const v0, 0x4628c000    # 10800.0f

    mul-float/2addr v0, v9

    const v1, 0x46a8c000    # 21600.0f

    mul-float/2addr v1, v8

    const v2, 0x46a8c000    # 21600.0f

    mul-float/2addr v2, v9

    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v8

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 195
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->paintLine:Landroid/graphics/Paint;

    if-eqz v0, :cond_2

    .line 196
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->paintLine:Landroid/graphics/Paint;

    invoke-virtual {p1, v11, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 200
    :cond_2
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->textArea:Landroid/graphics/RectF;

    const v1, 0x46680800    # 14850.0f

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->width:F

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 201
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->textArea:Landroid/graphics/RectF;

    const v1, 0x44fd2000    # 2025.0f

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->height:F

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 202
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->textArea:Landroid/graphics/RectF;

    const v1, 0x469e3400    # 20250.0f

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->width:F

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 203
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->textArea:Landroid/graphics/RectF;

    const v1, 0x4698ee00    # 19575.0f

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->height:F

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 301
    .end local v11    # "path":Landroid/graphics/Path;
    :cond_3
    :goto_0
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mDrawOnCanvas:Z

    if-nez v0, :cond_4

    .line 303
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 306
    .local v5, "mtx":Landroid/graphics/Matrix;
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->rotation:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->width:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->height:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    invoke-virtual {v5, v0, v1, v2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 308
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->bitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->bitmapWidth:I

    iget v4, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->bitmapHight:I

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 313
    .local v12, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v10, Ljava/io/FileOutputStream;

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->folderPath:Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "pic_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->shapeName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".png"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v10, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 317
    .local v10, "out":Ljava/io/FileOutputStream;
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v1, 0x64

    invoke-virtual {v12, v0, v1, v10}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 318
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 323
    .end local v5    # "mtx":Landroid/graphics/Matrix;
    .end local v10    # "out":Ljava/io/FileOutputStream;
    .end local v12    # "rotatedBMP":Landroid/graphics/Bitmap;
    :cond_4
    :goto_1
    return-void

    .line 206
    :cond_5
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->shapeName:Ljava/lang/String;

    const-string/jumbo v1, "RightBrace"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 215
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    if-nez v0, :cond_6

    .line 216
    const/16 v0, 0x708

    iput v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    .line 217
    :cond_6
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval1:I

    if-nez v0, :cond_7

    .line 218
    const/16 v0, 0x2a30

    iput v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval1:I

    .line 221
    :cond_7
    new-instance v11, Landroid/graphics/Path;

    invoke-direct {v11}, Landroid/graphics/Path;-><init>()V

    .line 222
    .restart local v11    # "path":Landroid/graphics/Path;
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 223
    const v0, 0x4628c000    # 10800.0f

    mul-float/2addr v0, v9

    const/4 v1, 0x0

    const v2, 0x4628c000    # 10800.0f

    mul-float/2addr v2, v9

    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v3, v3

    mul-float/2addr v3, v8

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 225
    const v0, 0x4628c000    # 10800.0f

    mul-float/2addr v0, v9

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval1:I

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 227
    const v0, 0x4628c000    # 10800.0f

    mul-float/2addr v0, v9

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval1:I

    int-to-float v1, v1

    mul-float/2addr v1, v8

    const v2, 0x46a8c000    # 21600.0f

    mul-float/2addr v2, v9

    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval1:I

    int-to-float v3, v3

    mul-float/2addr v3, v8

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 229
    const v0, 0x4628c000    # 10800.0f

    mul-float/2addr v0, v9

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval1:I

    int-to-float v1, v1

    mul-float/2addr v1, v8

    const v2, 0x4628c000    # 10800.0f

    mul-float/2addr v2, v9

    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval1:I

    iget v4, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v3, v8

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 232
    const v0, 0x4628c000    # 10800.0f

    mul-float/2addr v0, v9

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    rsub-int v1, v1, 0x5460

    int-to-float v1, v1

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 234
    const v0, 0x4628c000    # 10800.0f

    mul-float/2addr v0, v9

    const v1, 0x46a8c000    # 21600.0f

    mul-float/2addr v1, v8

    const/4 v2, 0x0

    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v8

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 237
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->paintLine:Landroid/graphics/Paint;

    if-eqz v0, :cond_8

    .line 238
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->paintLine:Landroid/graphics/Paint;

    invoke-virtual {p1, v11, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 242
    :cond_8
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->textArea:Landroid/graphics/RectF;

    const v1, 0x44a8c000    # 1350.0f

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->width:F

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 243
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->textArea:Landroid/graphics/RectF;

    const v1, 0x44fd2000    # 2025.0f

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->height:F

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 244
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->textArea:Landroid/graphics/RectF;

    const v1, 0x45d2f000    # 6750.0f

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->width:F

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 245
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->textArea:Landroid/graphics/RectF;

    const v1, 0x4698ee00    # 19575.0f

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->height:F

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 248
    .end local v11    # "path":Landroid/graphics/Path;
    :cond_9
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->shapeName:Ljava/lang/String;

    const-string/jumbo v1, "BracePair"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 254
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    if-nez v0, :cond_a

    .line 255
    const/16 v0, 0x708

    iput v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    .line 258
    :cond_a
    new-instance v11, Landroid/graphics/Path;

    invoke-direct {v11}, Landroid/graphics/Path;-><init>()V

    .line 259
    .restart local v11    # "path":Landroid/graphics/Path;
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    mul-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    mul-float/2addr v0, v9

    const/4 v1, 0x0

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 260
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v0, v0

    mul-float/2addr v0, v9

    const/4 v1, 0x0

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v2, v2

    mul-float/2addr v2, v9

    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v3, v3

    mul-float/2addr v3, v8

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 262
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v0, v0

    mul-float/2addr v0, v9

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->height:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v2, v2

    mul-float/2addr v2, v8

    sub-float/2addr v1, v2

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 264
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v0, v0

    mul-float/2addr v0, v9

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->height:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    const/4 v2, 0x0

    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->height:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 265
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v0, v0

    mul-float/2addr v0, v9

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->height:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v2, v2

    mul-float/2addr v2, v9

    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->height:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v4, v4

    mul-float/2addr v4, v8

    add-float/2addr v3, v4

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 267
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v0, v0

    mul-float/2addr v0, v9

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->height:F

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v2, v2

    mul-float/2addr v2, v8

    sub-float/2addr v1, v2

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 269
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v0, v0

    mul-float/2addr v0, v9

    const v1, 0x46a8c000    # 21600.0f

    mul-float/2addr v1, v8

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    mul-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    mul-float/2addr v2, v9

    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v8

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 273
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->width:F

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    mul-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    mul-float/2addr v1, v9

    sub-float/2addr v0, v1

    const/4 v1, 0x0

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 274
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->width:F

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v1, v1

    mul-float/2addr v1, v9

    sub-float/2addr v0, v1

    const/4 v1, 0x0

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->width:F

    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v3, v3

    mul-float/2addr v3, v9

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v3, v3

    mul-float/2addr v3, v8

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 276
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->width:F

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v1, v1

    mul-float/2addr v1, v9

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->height:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v2, v2

    mul-float/2addr v2, v8

    sub-float/2addr v1, v2

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 278
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->width:F

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v1, v1

    mul-float/2addr v1, v9

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->height:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    mul-float/2addr v2, v9

    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->height:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 280
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->width:F

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v1, v1

    mul-float/2addr v1, v9

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->height:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->width:F

    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v3, v3

    mul-float/2addr v3, v9

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->height:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v4, v4

    mul-float/2addr v4, v8

    add-float/2addr v3, v4

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 283
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->width:F

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v1, v1

    mul-float/2addr v1, v9

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->height:F

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v2, v2

    mul-float/2addr v2, v8

    sub-float/2addr v1, v2

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 285
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->width:F

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    int-to-float v1, v1

    mul-float/2addr v1, v9

    sub-float/2addr v0, v1

    const v1, 0x46a8c000    # 21600.0f

    mul-float/2addr v1, v8

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->width:F

    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->adjval0:I

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    mul-float/2addr v3, v9

    sub-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    mul-float/2addr v3, v8

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 289
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->paintLine:Landroid/graphics/Paint;

    if-eqz v0, :cond_b

    .line 290
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->paintLine:Landroid/graphics/Paint;

    invoke-virtual {p1, v11, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 294
    :cond_b
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->textArea:Landroid/graphics/RectF;

    const v1, 0x457d2000    # 4050.0f

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->width:F

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 295
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->textArea:Landroid/graphics/RectF;

    const v1, 0x4593a800    # 4725.0f

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->height:F

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 296
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->textArea:Landroid/graphics/RectF;

    const v1, 0x46891c00    # 17550.0f

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->width:F

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 297
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->textArea:Landroid/graphics/RectF;

    const v1, 0x4683d600    # 16875.0f

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->height:F

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 319
    .end local v11    # "path":Landroid/graphics/Path;
    .restart local v5    # "mtx":Landroid/graphics/Matrix;
    .restart local v12    # "rotatedBMP":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v7

    .line 320
    .local v7, "e":Ljava/lang/Exception;
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Exception while writing pictures to out file"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F

    .prologue
    .line 80
    invoke-super {p0, p3, p4, p7, p8}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IFF)V

    .line 81
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mDrawOnCanvas:Z

    .line 82
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mLeftMarg:I

    .line 83
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mTopMarg:I

    .line 84
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 85
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->draw(Landroid/graphics/Canvas;I)V

    .line 86
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mDrawOnCanvas:Z

    .line 88
    return-void
.end method

.method public drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p2, "shapeCount"    # I

    .prologue
    .line 71
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V

    .line 73
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->draw(I)V

    .line 74
    return-void
.end method

.method public drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 146
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;I)V

    .line 147
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mDrawOnCanvas:Z

    .line 148
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mLeftMarg:I

    .line 149
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mTopMarg:I

    .line 150
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->width:F

    .line 151
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->height:F

    .line 152
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 153
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->draw(Landroid/graphics/Canvas;I)V

    .line 154
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 155
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mDrawOnCanvas:Z

    .line 156
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V
    .locals 0
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 39
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 40
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->draw(I)V

    .line 41
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V
    .locals 2
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I

    .prologue
    .line 45
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mDrawOnCanvas:Z

    .line 47
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mLeftMarg:I

    .line 48
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mTopMarg:I

    .line 49
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 50
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->draw(Landroid/graphics/Canvas;I)V

    .line 51
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mDrawOnCanvas:Z

    .line 53
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 101
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 102
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mDrawOnCanvas:Z

    .line 103
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mLeftMarg:I

    .line 104
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mTopMarg:I

    .line 105
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 106
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->draw(Landroid/graphics/Canvas;I)V

    .line 107
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mDrawOnCanvas:Z

    .line 109
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 113
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V

    .line 114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mDrawOnCanvas:Z

    .line 115
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mLeftMarg:I

    .line 116
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mTopMarg:I

    .line 117
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 118
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->draw(Landroid/graphics/Canvas;I)V

    .line 119
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 120
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mDrawOnCanvas:Z

    .line 121
    return-void
.end method

.method public drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .prologue
    .line 95
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 96
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->draw(I)V

    .line 97
    return-void
.end method

.method public drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 58
    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p7

    move v4, p8

    move-object v5, p9

    invoke-super/range {v0 .. v5}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSSExcelShape(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mDrawOnCanvas:Z

    .line 61
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mLeftMarg:I

    .line 62
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mTopMarg:I

    .line 63
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 64
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->draw(Landroid/graphics/Canvas;I)V

    .line 65
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mDrawOnCanvas:Z

    .line 67
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;I)V
    .locals 1
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 124
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 125
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->draw(I)V

    .line 126
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 130
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 131
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mDrawOnCanvas:Z

    .line 132
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mLeftMarg:I

    .line 133
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mTopMarg:I

    .line 134
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->width:F

    .line 135
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->height:F

    .line 136
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 137
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->draw(Landroid/graphics/Canvas;I)V

    .line 138
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 139
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->mDrawOnCanvas:Z

    .line 140
    return-void
.end method
