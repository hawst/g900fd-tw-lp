.class public Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;
.super Ljava/lang/Object;
.source "XWPFShapeProp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "XWPFBodyProperty"
.end annotation


# instance fields
.field private fontScale:I

.field private vAlign:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;->fontScale:I

    .line 384
    const-string/jumbo v0, "t"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;->vAlign:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getFontScale()I
    .locals 1

    .prologue
    .line 388
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;->fontScale:I

    return v0
.end method

.method public getvAlign()Ljava/lang/String;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;->vAlign:Ljava/lang/String;

    return-object v0
.end method

.method public setFontScale(I)V
    .locals 0
    .param p1, "fontScale"    # I

    .prologue
    .line 392
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;->fontScale:I

    .line 393
    return-void
.end method

.method public setvAlign(Ljava/lang/String;)V
    .locals 0
    .param p1, "vAlign"    # Ljava/lang/String;

    .prologue
    .line 400
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;->vAlign:Ljava/lang/String;

    .line 401
    return-void
.end method
