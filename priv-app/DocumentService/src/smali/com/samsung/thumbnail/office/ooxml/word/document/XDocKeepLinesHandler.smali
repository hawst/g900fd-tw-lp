.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocKeepLinesHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;
.source "XDocKeepLinesHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocKeepLinesHandler$IXKeepLines;
    }
.end annotation


# instance fields
.field keepLinesCallback:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocKeepLinesHandler$IXKeepLines;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocKeepLinesHandler$IXKeepLines;)V
    .locals 1
    .param p1, "pgBrBefCallback"    # Lcom/samsung/thumbnail/office/ooxml/word/document/XDocKeepLinesHandler$IXKeepLines;

    .prologue
    .line 21
    const-string/jumbo v0, "keepLines"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;-><init>(Ljava/lang/String;)V

    .line 22
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocKeepLinesHandler;->keepLinesCallback:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocKeepLinesHandler$IXKeepLines;

    .line 23
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 28
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 29
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocKeepLinesHandler;->keepLinesCallback:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocKeepLinesHandler$IXKeepLines;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocKeepLinesHandler$IXKeepLines;->setKeepLinesStatus(Z)V

    .line 30
    return-void
.end method
