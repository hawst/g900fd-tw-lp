.class public Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XDocTblTCPrHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTextDirHandler$ITxtDirObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler$IXTblWOberver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$ITblCellPropObserver;,
        Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCGridSpan;,
        Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCVMerge;,
        Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCVAlign;,
        Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCShade;
    }
.end annotation


# instance fields
.field private observer:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$ITblCellPropObserver;

.field private tblCellProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$ITblCellPropObserver;)V
    .locals 1
    .param p1, "observer"    # Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$ITblCellPropObserver;

    .prologue
    .line 36
    const-string/jumbo v0, "tcPr"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 37
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;->observer:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$ITblCellPropObserver;

    .line 38
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;)Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;->tblCellProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    return-object v0
.end method

.method private init()V
    .locals 25

    .prologue
    .line 41
    const/16 v24, 0x8

    move/from16 v0, v24

    new-array v8, v0, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 42
    .local v8, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v22, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;

    const-string/jumbo v24, "tcW"

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    move-object/from16 v2, p0

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler$IXTblWOberver;)V

    .line 44
    .local v22, "widthHandler":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;
    new-instance v23, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v24, "tcW"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 46
    .local v23, "widthSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v24, 0x0

    aput-object v23, v8, v24

    .line 48
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCGridSpan;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCGridSpan;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;)V

    .line 49
    .local v5, "gridSpan":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCGridSpan;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;

    const-string/jumbo v24, "gridSpan"

    move-object/from16 v0, v24

    invoke-direct {v6, v0, v5}, Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 51
    .local v6, "gridSpanHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v24, "gridSpan"

    move-object/from16 v0, v24

    invoke-direct {v7, v0, v6}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 53
    .local v7, "gridSpanSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v24, 0x1

    aput-object v7, v8, v24

    .line 55
    new-instance v19, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCVMerge;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCVMerge;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;)V

    .line 56
    .local v19, "vMerge":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCVMerge;
    new-instance v20, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;

    const-string/jumbo v24, "vMerge"

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 58
    .local v20, "vMergeHanlder":Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;
    new-instance v21, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v24, "vMerge"

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 60
    .local v21, "vMergeSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v24, 0x2

    aput-object v21, v8, v24

    .line 62
    new-instance v16, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCVAlign;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCVAlign;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;)V

    .line 63
    .local v16, "vAlign":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCVAlign;
    new-instance v17, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;

    const-string/jumbo v24, "vAlign"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 65
    .local v17, "vAlignHanlder":Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v24, "vAlign"

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 67
    .local v18, "vAlignSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v24, 0x3

    aput-object v18, v8, v24

    .line 69
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCShade;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCShade;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;)V

    .line 70
    .local v9, "shade":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCShade;
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocShadeHandler;

    invoke-direct {v10, v9}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocShadeHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 71
    .local v10, "shadeHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocShadeHandler;
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v24, "shd"

    move-object/from16 v0, v24

    invoke-direct {v11, v0, v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 73
    .local v11, "shadeSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v24, 0x4

    aput-object v11, v8, v24

    .line 75
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;

    const-string/jumbo v24, "cnfStyle"

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v3, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 77
    .local v3, "cnfHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v24, "cnfStyle"

    move-object/from16 v0, v24

    invoke-direct {v4, v0, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 79
    .local v4, "cnfSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v24, 0x5

    aput-object v4, v8, v24

    .line 81
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTCBorderHandler;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTCBorderHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;)V

    .line 82
    .local v12, "tcBordersHandler":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTCBorderHandler;
    new-instance v13, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v24, "tcBorders"

    move-object/from16 v0, v24

    invoke-direct {v13, v0, v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 84
    .local v13, "tcBordersSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v24, 0x6

    aput-object v13, v8, v24

    .line 86
    new-instance v14, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTextDirHandler;

    const-string/jumbo v24, "textDirection"

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v14, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTextDirHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTextDirHandler$ITxtDirObserver;)V

    .line 88
    .local v14, "tcTxtDirHandler":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTextDirHandler;
    new-instance v15, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v24, "textDirection"

    move-object/from16 v0, v24

    invoke-direct {v15, v0, v14}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 90
    .local v15, "tcTxtDirSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/16 v24, 0x7

    aput-object v15, v8, v24

    .line 92
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 93
    return-void
.end method


# virtual methods
.method public consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V
    .locals 3
    .param p1, "value"    # Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;

    .prologue
    .line 184
    instance-of v1, p1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 185
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    .line 186
    .local v0, "val":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;->tblCellProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->setCNFStyle(Ljava/lang/String;)V

    .line 188
    .end local v0    # "val":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    :cond_0
    return-void
.end method

.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 105
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;->observer:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$ITblCellPropObserver;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;->tblCellProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    invoke-interface {v0, p1, v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$ITblCellPropObserver;->setTblCellProp(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;)V

    .line 107
    return-void
.end method

.method public setBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V
    .locals 1
    .param p1, "borderType"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    .param p2, "border"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .prologue
    .line 192
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;->tblCellProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->setBorders(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    .line 193
    return-void
.end method

.method public setElement(Ljava/lang/String;)V
    .locals 0
    .param p1, "element"    # Ljava/lang/String;

    .prologue
    .line 175
    return-void
.end method

.method public setTblWidth(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;I)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "width"    # I

    .prologue
    .line 164
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;->tblCellProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->setWidth(I)V

    .line 165
    return-void
.end method

.method public setTextDirection(Ljava/lang/String;)V
    .locals 1
    .param p1, "txtDir"    # Ljava/lang/String;

    .prologue
    .line 197
    if-eqz p1, :cond_0

    .line 198
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;->tblCellProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->setTextDirection(Ljava/lang/String;)V

    .line 200
    :cond_0
    return-void
.end method

.method public setWidthType(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "widthType"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    .prologue
    .line 169
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;->tblCellProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->setWidthType(Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;)V

    .line 170
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 98
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 99
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;->init()V

    .line 100
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;->tblCellProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    .line 101
    return-void
.end method
