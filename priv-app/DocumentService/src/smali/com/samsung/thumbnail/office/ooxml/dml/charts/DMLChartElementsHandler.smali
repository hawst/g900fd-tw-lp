.class public Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartElementsHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DMLChartElementsHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const/16 v6, 0x23

    .line 17
    const-string/jumbo v5, "chart"

    invoke-direct {p0, v6, v5}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 19
    const/4 v5, 0x2

    new-array v2, v5, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 21
    .local v2, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPlotAreaHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPlotAreaHandler;-><init>()V

    .line 22
    .local v0, "plotAreaHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPlotAreaHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v5, "plotArea"

    invoke-direct {v1, v5, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 24
    .local v1, "plotAreaSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/4 v5, 0x0

    aput-object v1, v2, v5

    .line 26
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartView3DHandler;

    const-string/jumbo v5, "view3D"

    invoke-direct {v3, v6, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartView3DHandler;-><init>(ILjava/lang/String;)V

    .line 28
    .local v3, "view3DHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartView3DHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v5, "view3D"

    invoke-direct {v4, v5, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 30
    .local v4, "view3DSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/4 v5, 0x1

    aput-object v4, v2, v5

    .line 32
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartElementsHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 33
    return-void
.end method
