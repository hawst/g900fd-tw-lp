.class public Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicFromRowColHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelDrawingPicFromRowColHandler.java"


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;)V
    .locals 6
    .param p1, "cellAnchor"    # Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    .prologue
    .line 18
    const/16 v4, 0xc9

    const-string/jumbo v5, "from"

    invoke-direct {p0, v4, v5}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 21
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicColHandler;

    const-string/jumbo v4, "from"

    invoke-direct {v0, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicColHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;)V

    .line 23
    .local v0, "fromcolHandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicColHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicFromRowColHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "col"

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicColOffHandler;

    const-string/jumbo v4, "from"

    invoke-direct {v1, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicColOffHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;)V

    .line 27
    .local v1, "fromcolOffHandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicColOffHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicFromRowColHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "colOff"

    invoke-virtual {v4, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowHandler;

    const-string/jumbo v4, "from"

    invoke-direct {v2, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;)V

    .line 31
    .local v2, "fromrowHandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicFromRowColHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "row"

    invoke-virtual {v4, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowOffHandler;

    const-string/jumbo v4, "from"

    invoke-direct {v3, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowOffHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;)V

    .line 35
    .local v3, "fromrowOffHandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowOffHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicFromRowColHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "rowOff"

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 43
    return-void
.end method
