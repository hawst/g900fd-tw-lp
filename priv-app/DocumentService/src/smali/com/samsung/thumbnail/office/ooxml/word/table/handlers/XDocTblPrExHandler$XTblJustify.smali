.class Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler$XTblJustify;
.super Ljava/lang/Object;
.source "XDocTblPrExHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "XTblJustify"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;


# direct methods
.method constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler$XTblJustify;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V
    .locals 3
    .param p1, "value"    # Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;

    .prologue
    .line 94
    instance-of v2, p1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    if-eqz v2, :cond_2

    move-object v0, p1

    .line 95
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    .line 96
    .local v0, "justify":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 97
    .local v1, "val":Ljava/lang/String;
    const-string/jumbo v2, "both"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "distribute"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 99
    :cond_0
    const-string/jumbo v1, "justify"

    .line 101
    :cond_1
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler$XTblJustify;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;

    iget-object v2, v2, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;->tableProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    invoke-virtual {v2, v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->setAlign(Ljava/lang/String;)V

    .line 103
    .end local v0    # "justify":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    .end local v1    # "val":Ljava/lang/String;
    :cond_2
    return-void
.end method
