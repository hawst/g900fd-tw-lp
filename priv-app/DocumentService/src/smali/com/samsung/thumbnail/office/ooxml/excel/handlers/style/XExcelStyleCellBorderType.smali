.class public final enum Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;
.super Ljava/lang/Enum;
.source "XExcelStyleCellBorderType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;

.field public static final enum BAR:Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;

.field public static final enum MEDIUMDASHED:Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;

.field public static final enum THIN:Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 8
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;

    const-string/jumbo v1, "THIN"

    const-string/jumbo v2, "thin"

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;->THIN:Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;

    const-string/jumbo v1, "MEDIUMDASHED"

    const-string/jumbo v2, "mediumDashed"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;->MEDIUMDASHED:Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;

    const-string/jumbo v1, "BAR"

    const-string/jumbo v2, "bar"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;->BAR:Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;

    .line 7
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;->THIN:Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;->MEDIUMDASHED:Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;->BAR:Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 11
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellBorderType;

    return-object v0
.end method
