.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocParaBorderHandler;
.super Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderExHandler;
.source "XDocParaBorderHandler.java"


# instance fields
.field private borderObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;)V
    .locals 1
    .param p1, "borderObserver"    # Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;

    .prologue
    .line 18
    const-string/jumbo v0, "pBdr"

    invoke-direct {p0, v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderExHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;)V

    .line 19
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocParaBorderHandler;->borderObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;

    .line 20
    return-void
.end method


# virtual methods
.method protected init()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 23
    invoke-super {p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderExHandler;->init()V

    .line 24
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocParaBorderHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    array-length v5, v5

    add-int/lit8 v5, v5, 0x2

    new-array v4, v5, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 26
    .local v4, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;

    const-string/jumbo v5, "between"

    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocParaBorderHandler;->borderObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;

    invoke-direct {v2, v5, v6}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;)V

    .line 28
    .local v2, "betweenBorder":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v5, "between"

    invoke-direct {v3, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 30
    .local v3, "betweenSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    aput-object v3, v4, v8

    .line 32
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;

    const-string/jumbo v5, "bar"

    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocParaBorderHandler;->borderObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;

    invoke-direct {v0, v5, v6}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;)V

    .line 34
    .local v0, "barBorder":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v5, "bar"

    invoke-direct {v1, v5, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 36
    .local v1, "barSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v5, 0x1

    aput-object v1, v4, v5

    .line 38
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocParaBorderHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocParaBorderHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    array-length v7, v7

    invoke-static {v5, v8, v4, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 40
    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocParaBorderHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 41
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 46
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderExHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 47
    return-void
.end method
