.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocFldSimpleHandler;
.super Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPContent;
.source "XDocFldSimpleHandler.java"


# instance fields
.field private fldSimpleObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/IFldSimpleObserver;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/document/IFldSimpleObserver;)V
    .locals 1
    .param p1, "fldSimpleObserver"    # Lcom/samsung/thumbnail/office/ooxml/word/document/IFldSimpleObserver;

    .prologue
    .line 16
    const-string/jumbo v0, "fldSimple"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPContent;-><init>(Ljava/lang/String;)V

    .line 17
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocFldSimpleHandler;->fldSimpleObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/IFldSimpleObserver;

    .line 18
    return-void
.end method


# virtual methods
.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 32
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPContent;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 33
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 23
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    const-string/jumbo v1, "w:instr"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocFldSimpleHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;-><init>(Ljava/lang/String;)V

    .line 25
    .local v0, "instr":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocFldSimpleHandler;->fldSimpleObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/IFldSimpleObserver;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/IFldSimpleObserver;->setFldSimple(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V

    .line 26
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPContent;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 27
    return-void
.end method
