.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/FontSchemeHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "FontSchemeHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    .line 16
    const-string/jumbo v5, "fontScheme"

    invoke-direct {p0, v5}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 18
    const/4 v5, 0x2

    new-array v4, v5, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 24
    .local v4, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeFontHandler;

    const-string/jumbo v5, "majorFont"

    invoke-direct {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeFontHandler;-><init>(Ljava/lang/String;)V

    .line 26
    .local v0, "majorFontHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeFontHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v5, "majorFont"

    invoke-direct {v1, v5, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 28
    .local v1, "majorTxtCharPropsSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v5, 0x0

    aput-object v1, v4, v5

    .line 34
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeFontHandler;

    const-string/jumbo v5, "minorFont"

    invoke-direct {v2, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeFontHandler;-><init>(Ljava/lang/String;)V

    .line 36
    .local v2, "minorFontHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeFontHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v5, "minorFont"

    invoke-direct {v3, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 38
    .local v3, "minorTxtCharPropsSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v5, 0x1

    aput-object v3, v4, v5

    .line 40
    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FontSchemeHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 41
    return-void
.end method
