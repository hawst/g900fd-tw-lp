.class public Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;
.super Ljava/lang/Object;
.source "XWPFChartBuilder.java"


# static fields
.field private static chartBuilder:Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;


# instance fields
.field private color:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mColorArray:[I

.field private mMaxDataXValue:D

.field private mMaxDataYValue:D

.field private mMinDataXValue:D

.field private mMinDataYValue:D

.field private random:Ljava/util/Random;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    const/16 v0, 0xa

    new-array v0, v0, [I

    const/4 v1, 0x0

    const-string/jumbo v2, "#ff4169e1"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "#ffa52a2a"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "#ff6b8e23"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "#ff8a2be2"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "#ff00bfff"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "#ffcd853f"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "#ff87cefa"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "#ffb8860b"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "#ffbdb76b"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "#ff9370db"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mColorArray:[I

    .line 73
    return-void
.end method

.method private buildCategoryDatasetForPieChart(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/model/CategorySeries;
    .locals 12
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getChartCatagoryForCanvas(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Ljava/lang/String;

    move-result-object v0

    .line 1449
    .local v0, "cat":Ljava/lang/String;
    const-string/jumbo v7, ","

    invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1450
    .local v1, "catArray":[Ljava/lang/String;
    new-instance v6, Lorg/achartengine/model/CategorySeries;

    const-string/jumbo v7, "Project budget"

    invoke-direct {v6, v7}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 1451
    .local v6, "series":Lorg/achartengine/model/CategorySeries;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v3, v7, :cond_1

    .line 1453
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v8, "val"

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v5

    .line 1456
    .local v5, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v4, v7, :cond_0

    .line 1458
    :try_start_0
    aget-object v8, v1, v4

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    invoke-virtual {v6, v8, v10, v11}, Lorg/achartengine/model/CategorySeries;->add(Ljava/lang/String;D)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1456
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1460
    :catch_0
    move-exception v2

    .line 1461
    .local v2, "ex":Ljava/lang/NumberFormatException;
    const-wide/16 v8, 0x0

    invoke-virtual {v6, v8, v9}, Lorg/achartengine/model/CategorySeries;->add(D)V

    goto :goto_2

    .line 1451
    .end local v2    # "ex":Ljava/lang/NumberFormatException;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1465
    .end local v4    # "j":I
    .end local v5    # "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    :cond_1
    return-object v6
.end method

.method private buildCategoryDatasetForPieChart(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/model/CategorySeries;
    .locals 12
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    .line 1426
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getChartCatagoryForCanvas(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Ljava/lang/String;

    move-result-object v0

    .line 1427
    .local v0, "cat":Ljava/lang/String;
    const-string/jumbo v7, ","

    invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1428
    .local v1, "catArray":[Ljava/lang/String;
    new-instance v6, Lorg/achartengine/model/CategorySeries;

    const-string/jumbo v7, "Project budget"

    invoke-direct {v6, v7}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 1429
    .local v6, "series":Lorg/achartengine/model/CategorySeries;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v3, v7, :cond_1

    .line 1431
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v8, "val"

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v5

    .line 1434
    .local v5, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v4, v7, :cond_0

    .line 1436
    :try_start_0
    aget-object v8, v1, v4

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    invoke-virtual {v6, v8, v10, v11}, Lorg/achartengine/model/CategorySeries;->add(Ljava/lang/String;D)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1434
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1438
    :catch_0
    move-exception v2

    .line 1439
    .local v2, "ex":Ljava/lang/NumberFormatException;
    const-wide/16 v8, 0x0

    invoke-virtual {v6, v8, v9}, Lorg/achartengine/model/CategorySeries;->add(D)V

    goto :goto_2

    .line 1429
    .end local v2    # "ex":Ljava/lang/NumberFormatException;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1443
    .end local v4    # "j":I
    .end local v5    # "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    :cond_1
    return-object v6
.end method

.method private buildHorizontalBarDataset(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;
    .locals 14
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .prologue
    const-wide/16 v12, 0x0

    .line 605
    iput-wide v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 606
    iput-wide v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 608
    new-instance v2, Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-direct {v2}, Lorg/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    .line 610
    .local v2, "dataset":Lorg/achartengine/model/XYMultipleSeriesDataset;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v4, v9, :cond_3

    .line 611
    new-instance v7, Lorg/achartengine/model/CategorySeries;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v10, "tx"

    invoke-virtual {v9, v10}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 613
    .local v7, "series":Lorg/achartengine/model/CategorySeries;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v10, "val"

    invoke-virtual {v9, v10}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v6

    .line 615
    .local v6, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 617
    .local v8, "val":Ljava/lang/String;
    :try_start_0
    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 618
    .local v0, "dVal":D
    iget-wide v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    cmpl-double v9, v0, v10

    if-lez v9, :cond_0

    .line 619
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 620
    :cond_0
    iget-wide v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    cmpg-double v9, v0, v10

    if-gez v9, :cond_1

    cmpg-double v9, v0, v12

    if-gez v9, :cond_1

    .line 621
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 622
    :cond_1
    invoke-virtual {v7, v0, v1}, Lorg/achartengine/model/CategorySeries;->add(D)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 623
    .end local v0    # "dVal":D
    :catch_0
    move-exception v3

    .line 624
    .local v3, "ex":Ljava/lang/NumberFormatException;
    invoke-virtual {v7, v12, v13}, Lorg/achartengine/model/CategorySeries;->add(D)V

    goto :goto_1

    .line 627
    .end local v3    # "ex":Ljava/lang/NumberFormatException;
    .end local v8    # "val":Ljava/lang/String;
    :cond_2
    invoke-virtual {v7}, Lorg/achartengine/model/CategorySeries;->toXYSeries()Lorg/achartengine/model/XYSeries;

    move-result-object v9

    invoke-virtual {v2, v9}, Lorg/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lorg/achartengine/model/XYSeries;)V

    .line 610
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 629
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    .end local v7    # "series":Lorg/achartengine/model/CategorySeries;
    :cond_3
    return-object v2
.end method

.method private buildHorizontalBarDataset(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;
    .locals 14
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    const-wide/16 v12, 0x0

    .line 576
    iput-wide v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 577
    iput-wide v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 579
    new-instance v2, Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-direct {v2}, Lorg/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    .line 581
    .local v2, "dataset":Lorg/achartengine/model/XYMultipleSeriesDataset;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v4, v9, :cond_3

    .line 582
    new-instance v7, Lorg/achartengine/model/CategorySeries;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v10, "tx"

    invoke-virtual {v9, v10}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 584
    .local v7, "series":Lorg/achartengine/model/CategorySeries;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v10, "val"

    invoke-virtual {v9, v10}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v6

    .line 586
    .local v6, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 588
    .local v8, "val":Ljava/lang/String;
    :try_start_0
    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 589
    .local v0, "dVal":D
    iget-wide v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    cmpl-double v9, v0, v10

    if-lez v9, :cond_0

    .line 590
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 591
    :cond_0
    iget-wide v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    cmpg-double v9, v0, v10

    if-gez v9, :cond_1

    cmpg-double v9, v0, v12

    if-gez v9, :cond_1

    .line 592
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 593
    :cond_1
    invoke-virtual {v7, v0, v1}, Lorg/achartengine/model/CategorySeries;->add(D)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 594
    .end local v0    # "dVal":D
    :catch_0
    move-exception v3

    .line 595
    .local v3, "ex":Ljava/lang/NumberFormatException;
    invoke-virtual {v7, v12, v13}, Lorg/achartengine/model/CategorySeries;->add(D)V

    goto :goto_1

    .line 598
    .end local v3    # "ex":Ljava/lang/NumberFormatException;
    .end local v8    # "val":Ljava/lang/String;
    :cond_2
    invoke-virtual {v7}, Lorg/achartengine/model/CategorySeries;->toXYSeries()Lorg/achartengine/model/XYSeries;

    move-result-object v9

    invoke-virtual {v2, v9}, Lorg/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lorg/achartengine/model/XYSeries;)V

    .line 581
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 600
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    .end local v7    # "series":Lorg/achartengine/model/CategorySeries;
    :cond_3
    return-object v2
.end method

.method private buildHorizontalBarRenderer(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 10
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .prologue
    .line 689
    new-instance v3, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 690
    .local v3, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v6, 0x41800000    # 16.0f

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 691
    const/high16 v6, 0x41a00000    # 20.0f

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 692
    const/high16 v6, 0x40e00000    # 7.0f

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 693
    const/high16 v6, 0x41700000    # 15.0f

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 695
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_0

    .line 696
    new-instance v2, Lorg/achartengine/renderer/SimpleSeriesRenderer;

    invoke-direct {v2}, Lorg/achartengine/renderer/SimpleSeriesRenderer;-><init>()V

    .line 697
    .local v2, "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mColorArray:[I

    aget v6, v6, v1

    invoke-virtual {v2, v6}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setColor(I)V

    .line 698
    invoke-virtual {v3, v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 695
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 701
    .end local v2    # "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    :cond_0
    sget-object v6, Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;->VERTICAL:Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setOrientation(Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;)V

    .line 703
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    const-wide/16 v8, 0x0

    cmpg-double v6, v6, v8

    if-gez v6, :cond_1

    .line 704
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 705
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 706
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 708
    :cond_1
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 710
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    const-wide/16 v8, 0x0

    cmpg-double v6, v6, v8

    if-gez v6, :cond_2

    .line 711
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 712
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 713
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 716
    :cond_2
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 718
    const-wide/16 v6, 0x0

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 719
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v7, "val"

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    int-to-double v6, v6

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMax(D)V

    .line 722
    const v6, -0x777778

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxesColor(I)V

    .line 723
    const v6, -0x333334

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsColor(I)V

    .line 724
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    .line 725
    .local v0, "chartSer":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;
    const-string/jumbo v6, "cat"

    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v4

    .line 727
    .local v4, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_3

    .line 728
    add-int/lit8 v6, v1, 0x1

    int-to-double v8, v6

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v3, v8, v9, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    .line 727
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 730
    :cond_3
    const v6, -0x777778

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setGridColor(I)V

    .line 731
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setShowGridX(Z)V

    .line 732
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_0

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 734
    const-wide v6, 0x3fd3333333333333L    # 0.3

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setBarSpacing(D)V

    .line 735
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_4

    .line 736
    invoke-virtual {v3, v1}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getSeriesRendererAt(I)Lorg/achartengine/renderer/SimpleSeriesRenderer;

    move-result-object v5

    .line 738
    .local v5, "seriesRenderer":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setDisplayChartValues(Z)V

    .line 735
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 740
    .end local v5    # "seriesRenderer":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    :cond_4
    return-object v3

    .line 732
    :array_0
    .array-data 4
        0x14
        0x28
        0x46
        0xa
    .end array-data
.end method

.method private buildHorizontalBarRenderer(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 10
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    .line 633
    new-instance v3, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 634
    .local v3, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v6, 0x41800000    # 16.0f

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 635
    const/high16 v6, 0x41a00000    # 20.0f

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 636
    const/high16 v6, 0x40e00000    # 7.0f

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 637
    const/high16 v6, 0x41700000    # 15.0f

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 639
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_0

    .line 640
    new-instance v2, Lorg/achartengine/renderer/SimpleSeriesRenderer;

    invoke-direct {v2}, Lorg/achartengine/renderer/SimpleSeriesRenderer;-><init>()V

    .line 641
    .local v2, "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mColorArray:[I

    aget v6, v6, v1

    invoke-virtual {v2, v6}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setColor(I)V

    .line 642
    invoke-virtual {v3, v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 639
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 645
    .end local v2    # "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    :cond_0
    sget-object v6, Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;->VERTICAL:Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setOrientation(Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;)V

    .line 647
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    const-wide/16 v8, 0x0

    cmpg-double v6, v6, v8

    if-gez v6, :cond_1

    .line 648
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 649
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 650
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 652
    :cond_1
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 654
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    const-wide/16 v8, 0x0

    cmpg-double v6, v6, v8

    if-gez v6, :cond_2

    .line 655
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 656
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 657
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 660
    :cond_2
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 662
    const-wide/16 v6, 0x0

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 663
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v7, "val"

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    int-to-double v6, v6

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMax(D)V

    .line 666
    const v6, -0x777778

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxesColor(I)V

    .line 667
    const v6, -0x333334

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsColor(I)V

    .line 668
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    .line 669
    .local v0, "chartSer":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;
    const-string/jumbo v6, "cat"

    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v4

    .line 671
    .local v4, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_3

    .line 672
    add-int/lit8 v6, v1, 0x1

    int-to-double v8, v6

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v3, v8, v9, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    .line 671
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 674
    :cond_3
    const v6, -0x777778

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setGridColor(I)V

    .line 675
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setShowGridX(Z)V

    .line 676
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_0

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 678
    const-wide v6, 0x3fd3333333333333L    # 0.3

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setBarSpacing(D)V

    .line 679
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_4

    .line 680
    invoke-virtual {v3, v1}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getSeriesRendererAt(I)Lorg/achartengine/renderer/SimpleSeriesRenderer;

    move-result-object v5

    .line 682
    .local v5, "seriesRenderer":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setDisplayChartValues(Z)V

    .line 679
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 684
    .end local v5    # "seriesRenderer":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    :cond_4
    return-object v3

    .line 676
    :array_0
    .array-data 4
        0x14
        0x28
        0x46
        0xa
    .end array-data
.end method

.method private getAreaChartDateDataset(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;
    .locals 14
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .prologue
    .line 1508
    new-instance v2, Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-direct {v2}, Lorg/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    .line 1510
    .local v2, "dataset":Lorg/achartengine/model/XYMultipleSeriesDataset;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1511
    .local v8, "valueList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Double;>;"
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v7, v10, [Lorg/achartengine/model/CategorySeries;

    .line 1514
    .local v7, "seriesArray":[Lorg/achartengine/model/CategorySeries;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-ge v3, v10, :cond_5

    .line 1515
    new-instance v6, Lorg/achartengine/model/CategorySeries;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v11, "tx"

    invoke-virtual {v10, v11}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v6, v10}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 1517
    .local v6, "series":Lorg/achartengine/model/CategorySeries;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v11, "val"

    invoke-virtual {v10, v11}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v5

    .line 1520
    .local v5, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-ge v4, v10, :cond_4

    .line 1521
    const-wide/16 v0, 0x0

    .line 1522
    .local v0, "addedValue":D
    if-nez v3, :cond_3

    .line 1523
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 1527
    :goto_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-le v10, v4, :cond_0

    .line 1528
    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1529
    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v8, v4, v10}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1530
    iget-wide v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    cmpl-double v10, v0, v10

    if-lez v10, :cond_1

    .line 1531
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1532
    :cond_1
    iget-wide v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    cmpg-double v10, v0, v10

    if-gez v10, :cond_2

    const-wide/16 v10, 0x0

    cmpg-double v10, v0, v10

    if-gez v10, :cond_2

    .line 1533
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1534
    :cond_2
    invoke-virtual {v6, v0, v1}, Lorg/achartengine/model/CategorySeries;->add(D)V

    .line 1520
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1525
    :cond_3
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v12

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Double;

    invoke-virtual {v10}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    add-double v0, v12, v10

    goto :goto_2

    .line 1536
    .end local v0    # "addedValue":D
    :cond_4
    aput-object v6, v7, v3

    .line 1514
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 1539
    .end local v4    # "j":I
    .end local v5    # "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    .end local v6    # "series":Lorg/achartengine/model/CategorySeries;
    :cond_5
    array-length v10, v7

    add-int/lit8 v9, v10, -0x1

    .local v9, "x":I
    :goto_3
    if-ltz v9, :cond_6

    .line 1540
    aget-object v10, v7, v9

    invoke-virtual {v10}, Lorg/achartengine/model/CategorySeries;->toXYSeries()Lorg/achartengine/model/XYSeries;

    move-result-object v10

    invoke-virtual {v2, v10}, Lorg/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lorg/achartengine/model/XYSeries;)V

    .line 1539
    add-int/lit8 v9, v9, -0x1

    goto :goto_3

    .line 1541
    :cond_6
    return-object v2
.end method

.method private getAreaChartDateDataset(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;
    .locals 14
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    .line 1470
    new-instance v2, Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-direct {v2}, Lorg/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    .line 1472
    .local v2, "dataset":Lorg/achartengine/model/XYMultipleSeriesDataset;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1473
    .local v8, "valueList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Double;>;"
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v7, v10, [Lorg/achartengine/model/CategorySeries;

    .line 1476
    .local v7, "seriesArray":[Lorg/achartengine/model/CategorySeries;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-ge v3, v10, :cond_5

    .line 1477
    new-instance v6, Lorg/achartengine/model/CategorySeries;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v11, "tx"

    invoke-virtual {v10, v11}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v6, v10}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 1479
    .local v6, "series":Lorg/achartengine/model/CategorySeries;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v11, "val"

    invoke-virtual {v10, v11}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v5

    .line 1482
    .local v5, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-ge v4, v10, :cond_4

    .line 1483
    const-wide/16 v0, 0x0

    .line 1484
    .local v0, "addedValue":D
    if-nez v3, :cond_3

    .line 1485
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 1489
    :goto_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-le v10, v4, :cond_0

    .line 1490
    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1491
    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v8, v4, v10}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1492
    iget-wide v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    cmpl-double v10, v0, v10

    if-lez v10, :cond_1

    .line 1493
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1494
    :cond_1
    iget-wide v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    cmpg-double v10, v0, v10

    if-gez v10, :cond_2

    const-wide/16 v10, 0x0

    cmpg-double v10, v0, v10

    if-gez v10, :cond_2

    .line 1495
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1496
    :cond_2
    invoke-virtual {v6, v0, v1}, Lorg/achartengine/model/CategorySeries;->add(D)V

    .line 1482
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1487
    :cond_3
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v12

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Double;

    invoke-virtual {v10}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    add-double v0, v12, v10

    goto :goto_2

    .line 1498
    .end local v0    # "addedValue":D
    :cond_4
    aput-object v6, v7, v3

    .line 1476
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 1501
    .end local v4    # "j":I
    .end local v5    # "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    .end local v6    # "series":Lorg/achartengine/model/CategorySeries;
    :cond_5
    array-length v10, v7

    add-int/lit8 v9, v10, -0x1

    .local v9, "x":I
    :goto_3
    if-ltz v9, :cond_6

    .line 1502
    aget-object v10, v7, v9

    invoke-virtual {v10}, Lorg/achartengine/model/CategorySeries;->toXYSeries()Lorg/achartengine/model/XYSeries;

    move-result-object v10

    invoke-virtual {v2, v10}, Lorg/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lorg/achartengine/model/XYSeries;)V

    .line 1501
    add-int/lit8 v9, v9, -0x1

    goto :goto_3

    .line 1503
    :cond_6
    return-object v2
.end method

.method private getAreaChartRenderer(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 14
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .prologue
    const-wide/16 v12, 0x0

    const/4 v10, 0x1

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    .line 1592
    new-instance v3, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 1593
    .local v3, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v5, 0x41800000    # 16.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 1594
    const/high16 v5, 0x41a00000    # 20.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 1595
    const/high16 v5, 0x41200000    # 10.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 1596
    const/high16 v5, 0x41700000    # 15.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 1597
    const/high16 v5, 0x40a00000    # 5.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setPointSize(F)V

    .line 1598
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    cmpg-double v5, v6, v12

    if-gez v5, :cond_0

    .line 1599
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1600
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1601
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1603
    :cond_0
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 1605
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    cmpg-double v5, v6, v12

    if-gez v5, :cond_1

    .line 1606
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1607
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1608
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1610
    :cond_1
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMax(D)V

    .line 1612
    const/4 v5, 0x4

    new-array v5, v5, [I

    fill-array-data v5, :array_0

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 1614
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_2

    .line 1615
    new-instance v2, Lorg/achartengine/renderer/XYSeriesRenderer;

    invoke-direct {v2}, Lorg/achartengine/renderer/XYSeriesRenderer;-><init>()V

    .line 1616
    .local v2, "r":Lorg/achartengine/renderer/XYSeriesRenderer;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mColorArray:[I

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v7, v1, 0x1

    sub-int/2addr v6, v7

    aget v5, v5, v6

    invoke-virtual {v2, v5}, Lorg/achartengine/renderer/XYSeriesRenderer;->setColor(I)V

    .line 1617
    invoke-virtual {v2, v10}, Lorg/achartengine/renderer/XYSeriesRenderer;->setFillBelowLine(Z)V

    .line 1618
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mColorArray:[I

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v7, v1, 0x1

    sub-int/2addr v6, v7

    aget v5, v5, v6

    invoke-virtual {v2, v5}, Lorg/achartengine/renderer/XYSeriesRenderer;->setFillBelowLineColor(I)V

    .line 1620
    invoke-virtual {v2, v10}, Lorg/achartengine/renderer/XYSeriesRenderer;->setFillPoints(Z)V

    .line 1621
    invoke-virtual {v3, v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 1614
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1624
    .end local v2    # "r":Lorg/achartengine/renderer/XYSeriesRenderer;
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    .line 1625
    .local v0, "chartSer":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;
    const-string/jumbo v5, "cat"

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v4

    .line 1627
    .local v4, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_3

    .line 1629
    add-int/lit8 v5, v1, 0x1

    int-to-double v6, v5

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v3, v6, v7, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    .line 1627
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1632
    :cond_3
    const v5, -0xbbbbbc

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxesColor(I)V

    .line 1633
    const v5, -0x333334

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsColor(I)V

    .line 1634
    invoke-virtual {v3, v10}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setShowGrid(Z)V

    .line 1635
    return-object v3

    .line 1612
    :array_0
    .array-data 4
        0x14
        0x1e
        0x1e
        0x1e
    .end array-data
.end method

.method private getAreaChartRenderer(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 14
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    const-wide/16 v12, 0x0

    const/4 v10, 0x1

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    .line 1545
    new-instance v3, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 1546
    .local v3, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v5, 0x41800000    # 16.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 1547
    const/high16 v5, 0x41a00000    # 20.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 1548
    const/high16 v5, 0x41200000    # 10.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 1549
    const/high16 v5, 0x41700000    # 15.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 1550
    const/high16 v5, 0x40a00000    # 5.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setPointSize(F)V

    .line 1551
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    cmpg-double v5, v6, v12

    if-gez v5, :cond_0

    .line 1552
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1553
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1554
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1556
    :cond_0
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 1558
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    cmpg-double v5, v6, v12

    if-gez v5, :cond_1

    .line 1559
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1560
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1561
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1563
    :cond_1
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMax(D)V

    .line 1565
    const/4 v5, 0x4

    new-array v5, v5, [I

    fill-array-data v5, :array_0

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 1567
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_2

    .line 1568
    new-instance v2, Lorg/achartengine/renderer/XYSeriesRenderer;

    invoke-direct {v2}, Lorg/achartengine/renderer/XYSeriesRenderer;-><init>()V

    .line 1569
    .local v2, "r":Lorg/achartengine/renderer/XYSeriesRenderer;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mColorArray:[I

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v7, v1, 0x1

    sub-int/2addr v6, v7

    aget v5, v5, v6

    invoke-virtual {v2, v5}, Lorg/achartengine/renderer/XYSeriesRenderer;->setColor(I)V

    .line 1570
    invoke-virtual {v2, v10}, Lorg/achartengine/renderer/XYSeriesRenderer;->setFillBelowLine(Z)V

    .line 1571
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mColorArray:[I

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v7, v1, 0x1

    sub-int/2addr v6, v7

    aget v5, v5, v6

    invoke-virtual {v2, v5}, Lorg/achartengine/renderer/XYSeriesRenderer;->setFillBelowLineColor(I)V

    .line 1573
    invoke-virtual {v2, v10}, Lorg/achartengine/renderer/XYSeriesRenderer;->setFillPoints(Z)V

    .line 1574
    invoke-virtual {v3, v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 1567
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1577
    .end local v2    # "r":Lorg/achartengine/renderer/XYSeriesRenderer;
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    .line 1578
    .local v0, "chartSer":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;
    const-string/jumbo v5, "cat"

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v4

    .line 1580
    .local v4, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_3

    .line 1582
    add-int/lit8 v5, v1, 0x1

    int-to-double v6, v5

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v3, v6, v7, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    .line 1580
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1585
    :cond_3
    const v5, -0xbbbbbc

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxesColor(I)V

    .line 1586
    const v5, -0x333334

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsColor(I)V

    .line 1587
    invoke-virtual {v3, v10}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setShowGrid(Z)V

    .line 1588
    return-object v3

    .line 1565
    :array_0
    .array-data 4
        0x14
        0x1e
        0x1e
        0x1e
    .end array-data
.end method

.method private getDataSetForBubbleChart(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;
    .locals 22
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .prologue
    .line 988
    new-instance v21, Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-direct/range {v21 .. v21}, Lorg/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    .line 990
    .local v21, "series":Lorg/achartengine/model/XYMultipleSeriesDataset;
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    move/from16 v0, v16

    if-ge v0, v8, :cond_5

    .line 991
    new-instance v3, Lorg/achartengine/model/XYValueSeries;

    const-string/jumbo v8, "Y-Values"

    invoke-direct {v3, v8}, Lorg/achartengine/model/XYValueSeries;-><init>(Ljava/lang/String;)V

    .line 992
    .local v3, "xyValueSeries":Lorg/achartengine/model/XYValueSeries;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v8

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v9, "cat"

    invoke-virtual {v8, v9}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v19

    .line 994
    .local v19, "serXVal":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v8

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v9, "val"

    invoke-virtual {v8, v9}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v20

    .line 996
    .local v20, "serYVal":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v8

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v9, "size"

    invoke-virtual {v8, v9}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v18

    .line 999
    .local v18, "serSize":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    const/16 v17, 0x0

    .local v17, "j":I
    :goto_1
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    move/from16 v0, v17

    if-ge v0, v8, :cond_4

    .line 1003
    :try_start_0
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v8

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 1005
    .local v4, "dXVal":D
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    cmpl-double v8, v4, v8

    if-lez v8, :cond_0

    .line 1006
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    .line 1007
    :cond_0
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    cmpg-double v8, v4, v8

    if-gez v8, :cond_1

    const-wide/16 v8, 0x0

    cmpg-double v8, v4, v8

    if-gez v8, :cond_1

    .line 1008
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    .line 1010
    :cond_1
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v8

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    .line 1012
    .local v6, "dYVal":D
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    cmpl-double v8, v6, v8

    if-lez v8, :cond_2

    .line 1013
    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1014
    :cond_2
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    cmpg-double v8, v6, v8

    if-gez v8, :cond_3

    const-wide/16 v8, 0x0

    cmpg-double v8, v6, v8

    if-gez v8, :cond_3

    .line 1015
    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1017
    :cond_3
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v8

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-virtual/range {v3 .. v9}, Lorg/achartengine/model/XYValueSeries;->add(DDD)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 999
    .end local v4    # "dXVal":D
    .end local v6    # "dYVal":D
    :goto_2
    add-int/lit8 v17, v17, 0x1

    goto :goto_1

    .line 1019
    :catch_0
    move-exception v2

    .line 1020
    .local v2, "e":Ljava/lang/Exception;
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1021
    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    move-object v9, v3

    invoke-virtual/range {v9 .. v15}, Lorg/achartengine/model/XYValueSeries;->add(DDD)V

    goto :goto_2

    .line 1024
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_4
    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lorg/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lorg/achartengine/model/XYSeries;)V

    .line 990
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_0

    .line 1027
    .end local v3    # "xyValueSeries":Lorg/achartengine/model/XYValueSeries;
    .end local v17    # "j":I
    .end local v18    # "serSize":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    .end local v19    # "serXVal":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    .end local v20    # "serYVal":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    :cond_5
    return-object v21
.end method

.method private getDataSetForBubbleChart(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;
    .locals 22
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    .line 945
    new-instance v21, Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-direct/range {v21 .. v21}, Lorg/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    .line 947
    .local v21, "series":Lorg/achartengine/model/XYMultipleSeriesDataset;
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    move/from16 v0, v16

    if-ge v0, v8, :cond_5

    .line 948
    new-instance v3, Lorg/achartengine/model/XYValueSeries;

    const-string/jumbo v8, "Y-Values"

    invoke-direct {v3, v8}, Lorg/achartengine/model/XYValueSeries;-><init>(Ljava/lang/String;)V

    .line 949
    .local v3, "xyValueSeries":Lorg/achartengine/model/XYValueSeries;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v8

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v9, "cat"

    invoke-virtual {v8, v9}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v19

    .line 951
    .local v19, "serXVal":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v8

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v9, "val"

    invoke-virtual {v8, v9}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v20

    .line 953
    .local v20, "serYVal":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v8

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v9, "size"

    invoke-virtual {v8, v9}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v18

    .line 956
    .local v18, "serSize":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    const/16 v17, 0x0

    .local v17, "j":I
    :goto_1
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    move/from16 v0, v17

    if-ge v0, v8, :cond_4

    .line 960
    :try_start_0
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v8

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 962
    .local v4, "dXVal":D
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    cmpl-double v8, v4, v8

    if-lez v8, :cond_0

    .line 963
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    .line 964
    :cond_0
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    cmpg-double v8, v4, v8

    if-gez v8, :cond_1

    const-wide/16 v8, 0x0

    cmpg-double v8, v4, v8

    if-gez v8, :cond_1

    .line 965
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    .line 967
    :cond_1
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v8

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    .line 969
    .local v6, "dYVal":D
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    cmpl-double v8, v6, v8

    if-lez v8, :cond_2

    .line 970
    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 971
    :cond_2
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    cmpg-double v8, v6, v8

    if-gez v8, :cond_3

    const-wide/16 v8, 0x0

    cmpg-double v8, v6, v8

    if-gez v8, :cond_3

    .line 972
    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 974
    :cond_3
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v8

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-virtual/range {v3 .. v9}, Lorg/achartengine/model/XYValueSeries;->add(DDD)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 956
    .end local v4    # "dXVal":D
    .end local v6    # "dYVal":D
    :goto_2
    add-int/lit8 v17, v17, 0x1

    goto :goto_1

    .line 976
    :catch_0
    move-exception v2

    .line 977
    .local v2, "e":Ljava/lang/Exception;
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 978
    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    move-object v9, v3

    invoke-virtual/range {v9 .. v15}, Lorg/achartengine/model/XYValueSeries;->add(DDD)V

    goto :goto_2

    .line 981
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_4
    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lorg/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lorg/achartengine/model/XYSeries;)V

    .line 947
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_0

    .line 984
    .end local v3    # "xyValueSeries":Lorg/achartengine/model/XYValueSeries;
    .end local v17    # "j":I
    .end local v18    # "serSize":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    .end local v19    # "serXVal":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    .end local v20    # "serYVal":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    :cond_5
    return-object v21
.end method

.method private getDataSetForStockChart(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;
    .locals 22
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .prologue
    .line 395
    const-wide/16 v4, 0x0

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 396
    const-wide/16 v4, 0x0

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 398
    new-instance v14, Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-direct {v14}, Lorg/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    .line 400
    .local v14, "dataset":Lorg/achartengine/model/XYMultipleSeriesDataset;
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 401
    .local v19, "minValArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 402
    .local v18, "maxValArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 404
    .local v2, "clsValArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v15, v4, :cond_1

    .line 406
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v5, "val"

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v20

    .line 408
    .local v20, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v21

    .line 409
    .local v21, "serArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .line 411
    .local v16, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 413
    packed-switch v15, :pswitch_data_0

    .line 427
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_1

    .line 415
    :pswitch_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 419
    :pswitch_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 423
    :pswitch_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 404
    :cond_0
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 433
    .end local v16    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v20    # "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    .end local v21    # "serArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    const/4 v15, 0x0

    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v15, v4, :cond_5

    .line 434
    new-instance v3, Lorg/achartengine/model/RangeCategorySeries;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v5, "tx"

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/achartengine/model/RangeCategorySeries;-><init>(Ljava/lang/String;)V

    .line 438
    .local v3, "series":Lorg/achartengine/model/RangeCategorySeries;
    const/16 v17, 0x0

    .local v17, "j":I
    :goto_3
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v17

    if-ge v0, v4, :cond_4

    .line 439
    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    .line 440
    .local v10, "dMaxVal":D
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    cmpl-double v4, v10, v4

    if-lez v4, :cond_2

    .line 441
    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 442
    :cond_2
    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v12

    .line 443
    .local v12, "dMinVal":D
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    cmpg-double v4, v12, v4

    if-gez v4, :cond_3

    const-wide/16 v4, 0x0

    cmpg-double v4, v12, v4

    if-gez v4, :cond_3

    .line 444
    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 445
    :cond_3
    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-virtual/range {v3 .. v9}, Lorg/achartengine/model/RangeCategorySeries;->add(DDD)V

    .line 438
    add-int/lit8 v17, v17, 0x1

    goto :goto_3

    .line 450
    .end local v10    # "dMaxVal":D
    .end local v12    # "dMinVal":D
    :cond_4
    invoke-virtual {v3}, Lorg/achartengine/model/RangeCategorySeries;->toXYSeries()Lorg/achartengine/model/XYSeries;

    move-result-object v4

    invoke-virtual {v14, v4}, Lorg/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lorg/achartengine/model/XYSeries;)V

    .line 433
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_2

    .line 453
    .end local v3    # "series":Lorg/achartengine/model/RangeCategorySeries;
    .end local v17    # "j":I
    :cond_5
    return-object v14

    .line 413
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getDataSetForStockChart(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;
    .locals 22
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    .line 332
    const-wide/16 v4, 0x0

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 333
    const-wide/16 v4, 0x0

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 335
    new-instance v14, Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-direct {v14}, Lorg/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    .line 337
    .local v14, "dataset":Lorg/achartengine/model/XYMultipleSeriesDataset;
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 338
    .local v19, "minValArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 339
    .local v18, "maxValArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 341
    .local v2, "clsValArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v15, v4, :cond_1

    .line 343
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v5, "val"

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v20

    .line 345
    .local v20, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v21

    .line 346
    .local v21, "serArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .line 348
    .local v16, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 350
    packed-switch v15, :pswitch_data_0

    .line 364
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_1

    .line 352
    :pswitch_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 356
    :pswitch_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 360
    :pswitch_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 341
    :cond_0
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 370
    .end local v16    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v20    # "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    .end local v21    # "serArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    const/4 v15, 0x0

    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v15, v4, :cond_5

    .line 371
    new-instance v3, Lorg/achartengine/model/RangeCategorySeries;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v5, "tx"

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/achartengine/model/RangeCategorySeries;-><init>(Ljava/lang/String;)V

    .line 375
    .local v3, "series":Lorg/achartengine/model/RangeCategorySeries;
    const/16 v17, 0x0

    .local v17, "j":I
    :goto_3
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v17

    if-ge v0, v4, :cond_4

    .line 376
    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    .line 377
    .local v10, "dMaxVal":D
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    cmpl-double v4, v10, v4

    if-lez v4, :cond_2

    .line 378
    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 379
    :cond_2
    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v12

    .line 380
    .local v12, "dMinVal":D
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    cmpg-double v4, v12, v4

    if-gez v4, :cond_3

    const-wide/16 v4, 0x0

    cmpg-double v4, v12, v4

    if-gez v4, :cond_3

    .line 381
    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 382
    :cond_3
    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-virtual/range {v3 .. v9}, Lorg/achartengine/model/RangeCategorySeries;->add(DDD)V

    .line 375
    add-int/lit8 v17, v17, 0x1

    goto :goto_3

    .line 387
    .end local v10    # "dMaxVal":D
    .end local v12    # "dMinVal":D
    :cond_4
    invoke-virtual {v3}, Lorg/achartengine/model/RangeCategorySeries;->toXYSeries()Lorg/achartengine/model/XYSeries;

    move-result-object v4

    invoke-virtual {v14, v4}, Lorg/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lorg/achartengine/model/XYSeries;)V

    .line 370
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_2

    .line 390
    .end local v3    # "series":Lorg/achartengine/model/RangeCategorySeries;
    .end local v17    # "j":I
    :cond_5
    return-object v14

    .line 350
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getDoughnutChartDataset(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/model/CategorySeries;
    .locals 12
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .prologue
    .line 1664
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getChartCatagoryForCanvas(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Ljava/lang/String;

    move-result-object v0

    .line 1665
    .local v0, "cat":Ljava/lang/String;
    const-string/jumbo v7, ","

    invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1666
    .local v1, "catArray":[Ljava/lang/String;
    new-instance v6, Lorg/achartengine/model/CategorySeries;

    const-string/jumbo v7, "Project budget"

    invoke-direct {v6, v7}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 1667
    .local v6, "series":Lorg/achartengine/model/CategorySeries;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v3, v7, :cond_1

    .line 1669
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v8, "val"

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v5

    .line 1672
    .local v5, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v4, v7, :cond_0

    .line 1674
    :try_start_0
    aget-object v8, v1, v4

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    invoke-virtual {v6, v8, v10, v11}, Lorg/achartengine/model/CategorySeries;->add(Ljava/lang/String;D)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1672
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1676
    :catch_0
    move-exception v2

    .line 1677
    .local v2, "e":Ljava/lang/Exception;
    aget-object v7, v1, v4

    const-wide/16 v8, 0x0

    invoke-virtual {v6, v7, v8, v9}, Lorg/achartengine/model/CategorySeries;->add(Ljava/lang/String;D)V

    goto :goto_2

    .line 1667
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1683
    .end local v4    # "j":I
    .end local v5    # "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    :cond_1
    return-object v6
.end method

.method private getDoughnutChartDataset(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/model/CategorySeries;
    .locals 12
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    .line 1640
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getChartCatagoryForCanvas(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Ljava/lang/String;

    move-result-object v0

    .line 1641
    .local v0, "cat":Ljava/lang/String;
    const-string/jumbo v7, ","

    invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1642
    .local v1, "catArray":[Ljava/lang/String;
    new-instance v6, Lorg/achartengine/model/CategorySeries;

    const-string/jumbo v7, "Project budget"

    invoke-direct {v6, v7}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 1643
    .local v6, "series":Lorg/achartengine/model/CategorySeries;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v3, v7, :cond_1

    .line 1645
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v8, "val"

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v5

    .line 1648
    .local v5, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v4, v7, :cond_0

    .line 1650
    :try_start_0
    aget-object v8, v1, v4

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    invoke-virtual {v6, v8, v10, v11}, Lorg/achartengine/model/CategorySeries;->add(Ljava/lang/String;D)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1648
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1652
    :catch_0
    move-exception v2

    .line 1653
    .local v2, "e":Ljava/lang/Exception;
    aget-object v7, v1, v4

    const-wide/16 v8, 0x0

    invoke-virtual {v6, v7, v8, v9}, Lorg/achartengine/model/CategorySeries;->add(Ljava/lang/String;D)V

    goto :goto_2

    .line 1643
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1659
    .end local v4    # "j":I
    .end local v5    # "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    :cond_1
    return-object v6
.end method

.method private getDoughnutChartRendrer(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/renderer/DefaultRenderer;
    .locals 8
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .prologue
    const/4 v7, 0x0

    const/high16 v6, 0x41700000    # 15.0f

    .line 1704
    new-instance v5, Lorg/achartengine/renderer/DefaultRenderer;

    invoke-direct {v5}, Lorg/achartengine/renderer/DefaultRenderer;-><init>()V

    .line 1705
    .local v5, "renderer":Lorg/achartengine/renderer/DefaultRenderer;
    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setLabelsTextSize(F)V

    .line 1706
    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setLegendTextSize(F)V

    .line 1707
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_0

    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setMargins([I)V

    .line 1708
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mColorArray:[I

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget v1, v0, v2

    .line 1709
    .local v1, "color":I
    new-instance v4, Lorg/achartengine/renderer/SimpleSeriesRenderer;

    invoke-direct {v4}, Lorg/achartengine/renderer/SimpleSeriesRenderer;-><init>()V

    .line 1710
    .local v4, "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    invoke-virtual {v4, v1}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setColor(I)V

    .line 1711
    invoke-virtual {v5, v4}, Lorg/achartengine/renderer/DefaultRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 1708
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1713
    .end local v1    # "color":I
    .end local v4    # "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    :cond_0
    const/high16 v6, 0x41a00000    # 20.0f

    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setChartTitleTextSize(F)V

    .line 1714
    invoke-virtual {v5, v7}, Lorg/achartengine/renderer/DefaultRenderer;->setDisplayValues(Z)V

    .line 1715
    invoke-virtual {v5, v7}, Lorg/achartengine/renderer/DefaultRenderer;->setShowLabels(Z)V

    .line 1717
    return-object v5

    .line 1707
    nop

    :array_0
    .array-data 4
        0x14
        0x1e
        0xf
        0x3c
    .end array-data
.end method

.method private getDoughnutChartRendrer(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/renderer/DefaultRenderer;
    .locals 8
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    const/4 v7, 0x0

    const/high16 v6, 0x41700000    # 15.0f

    .line 1687
    new-instance v5, Lorg/achartengine/renderer/DefaultRenderer;

    invoke-direct {v5}, Lorg/achartengine/renderer/DefaultRenderer;-><init>()V

    .line 1688
    .local v5, "renderer":Lorg/achartengine/renderer/DefaultRenderer;
    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setLabelsTextSize(F)V

    .line 1689
    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setLegendTextSize(F)V

    .line 1690
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_0

    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setMargins([I)V

    .line 1691
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mColorArray:[I

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget v1, v0, v2

    .line 1692
    .local v1, "color":I
    new-instance v4, Lorg/achartengine/renderer/SimpleSeriesRenderer;

    invoke-direct {v4}, Lorg/achartengine/renderer/SimpleSeriesRenderer;-><init>()V

    .line 1693
    .local v4, "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    invoke-virtual {v4, v1}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setColor(I)V

    .line 1694
    invoke-virtual {v5, v4}, Lorg/achartengine/renderer/DefaultRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 1691
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1696
    .end local v1    # "color":I
    .end local v4    # "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    :cond_0
    const/high16 v6, 0x41a00000    # 20.0f

    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setChartTitleTextSize(F)V

    .line 1697
    invoke-virtual {v5, v7}, Lorg/achartengine/renderer/DefaultRenderer;->setDisplayValues(Z)V

    .line 1698
    invoke-virtual {v5, v7}, Lorg/achartengine/renderer/DefaultRenderer;->setShowLabels(Z)V

    .line 1700
    return-object v5

    .line 1690
    nop

    :array_0
    .array-data 4
        0x14
        0x1e
        0xf
        0x3c
    .end array-data
.end method

.method private getItem(I)Ljava/lang/String;
    .locals 4
    .param p1, "i"    # I

    .prologue
    .line 2677
    move v0, p1

    .line 2678
    .local v0, "index":I
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->color:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt p1, v2, :cond_0

    .line 2679
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->random:Ljava/util/Random;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->color:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 2681
    :cond_0
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->color:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2682
    .local v1, "item":Ljava/lang/String;
    return-object v1
.end method

.method private getLineChartDataset(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;
    .locals 14
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .prologue
    const-wide/16 v12, 0x0

    .line 1306
    new-instance v2, Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-direct {v2}, Lorg/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    .line 1308
    .local v2, "dataset":Lorg/achartengine/model/XYMultipleSeriesDataset;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v4, v9, :cond_3

    .line 1309
    new-instance v7, Lorg/achartengine/model/CategorySeries;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v10, "tx"

    invoke-virtual {v9, v10}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 1311
    .local v7, "series":Lorg/achartengine/model/CategorySeries;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v10, "val"

    invoke-virtual {v9, v10}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v6

    .line 1313
    .local v6, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 1315
    .local v8, "val":Ljava/lang/String;
    :try_start_0
    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 1316
    .local v0, "dVal":D
    iget-wide v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    cmpl-double v9, v0, v10

    if-lez v9, :cond_0

    .line 1317
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1318
    :cond_0
    iget-wide v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    cmpg-double v9, v0, v10

    if-gez v9, :cond_1

    cmpg-double v9, v0, v12

    if-gez v9, :cond_1

    .line 1319
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1320
    :cond_1
    invoke-virtual {v7, v0, v1}, Lorg/achartengine/model/CategorySeries;->add(D)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1321
    .end local v0    # "dVal":D
    :catch_0
    move-exception v3

    .line 1322
    .local v3, "ex":Ljava/lang/NumberFormatException;
    invoke-virtual {v7, v12, v13}, Lorg/achartengine/model/CategorySeries;->add(D)V

    goto :goto_1

    .line 1325
    .end local v3    # "ex":Ljava/lang/NumberFormatException;
    .end local v8    # "val":Ljava/lang/String;
    :cond_2
    invoke-virtual {v7}, Lorg/achartengine/model/CategorySeries;->toXYSeries()Lorg/achartengine/model/XYSeries;

    move-result-object v9

    invoke-virtual {v2, v9}, Lorg/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lorg/achartengine/model/XYSeries;)V

    .line 1308
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1327
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    .end local v7    # "series":Lorg/achartengine/model/CategorySeries;
    :cond_3
    return-object v2
.end method

.method private getLineChartDataset(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;
    .locals 14
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    const-wide/16 v12, 0x0

    .line 1281
    new-instance v2, Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-direct {v2}, Lorg/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    .line 1283
    .local v2, "dataset":Lorg/achartengine/model/XYMultipleSeriesDataset;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v4, v9, :cond_3

    .line 1284
    new-instance v7, Lorg/achartengine/model/CategorySeries;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v10, "tx"

    invoke-virtual {v9, v10}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 1286
    .local v7, "series":Lorg/achartengine/model/CategorySeries;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v10, "val"

    invoke-virtual {v9, v10}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v6

    .line 1288
    .local v6, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 1290
    .local v8, "val":Ljava/lang/String;
    :try_start_0
    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 1291
    .local v0, "dVal":D
    iget-wide v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    cmpl-double v9, v0, v10

    if-lez v9, :cond_0

    .line 1292
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1293
    :cond_0
    iget-wide v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    cmpg-double v9, v0, v10

    if-gez v9, :cond_1

    cmpg-double v9, v0, v12

    if-gez v9, :cond_1

    .line 1294
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1295
    :cond_1
    invoke-virtual {v7, v0, v1}, Lorg/achartengine/model/CategorySeries;->add(D)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1296
    .end local v0    # "dVal":D
    :catch_0
    move-exception v3

    .line 1297
    .local v3, "ex":Ljava/lang/NumberFormatException;
    invoke-virtual {v7, v12, v13}, Lorg/achartengine/model/CategorySeries;->add(D)V

    goto :goto_1

    .line 1300
    .end local v3    # "ex":Ljava/lang/NumberFormatException;
    .end local v8    # "val":Ljava/lang/String;
    :cond_2
    invoke-virtual {v7}, Lorg/achartengine/model/CategorySeries;->toXYSeries()Lorg/achartengine/model/XYSeries;

    move-result-object v9

    invoke-virtual {v2, v9}, Lorg/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lorg/achartengine/model/XYSeries;)V

    .line 1283
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1302
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    .end local v7    # "series":Lorg/achartengine/model/CategorySeries;
    :cond_3
    return-object v2
.end method

.method private getLineChartRenderer(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 10
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .prologue
    .line 1378
    new-instance v3, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 1379
    .local v3, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v5, 0x41800000    # 16.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 1380
    const/high16 v5, 0x41a00000    # 20.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 1381
    const/high16 v5, 0x41200000    # 10.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 1382
    const/high16 v5, 0x41700000    # 15.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 1383
    const/high16 v5, 0x40a00000    # 5.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setPointSize(F)V

    .line 1384
    const-wide/16 v6, 0x0

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 1385
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v6, "val"

    invoke-virtual {v5, v6}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    int-to-double v6, v5

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 1388
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    const-wide/16 v8, 0x0

    cmpg-double v5, v6, v8

    if-gez v5, :cond_0

    .line 1389
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1390
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1391
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1393
    :cond_0
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 1395
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    const-wide/16 v8, 0x0

    cmpg-double v5, v6, v8

    if-gez v5, :cond_1

    .line 1396
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1397
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1398
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1401
    :cond_1
    const v5, -0x333334

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setGridColor(I)V

    .line 1402
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setShowGridX(Z)V

    .line 1403
    const/4 v5, 0x4

    new-array v5, v5, [I

    fill-array-data v5, :array_0

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 1404
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_2

    .line 1405
    new-instance v2, Lorg/achartengine/renderer/XYSeriesRenderer;

    invoke-direct {v2}, Lorg/achartengine/renderer/XYSeriesRenderer;-><init>()V

    .line 1406
    .local v2, "r":Lorg/achartengine/renderer/XYSeriesRenderer;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mColorArray:[I

    aget v5, v5, v1

    invoke-virtual {v2, v5}, Lorg/achartengine/renderer/XYSeriesRenderer;->setColor(I)V

    .line 1407
    sget-object v5, Lorg/achartengine/chart/PointStyle;->SQUARE:Lorg/achartengine/chart/PointStyle;

    invoke-virtual {v2, v5}, Lorg/achartengine/renderer/XYSeriesRenderer;->setPointStyle(Lorg/achartengine/chart/PointStyle;)V

    .line 1408
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Lorg/achartengine/renderer/XYSeriesRenderer;->setFillPoints(Z)V

    .line 1409
    invoke-virtual {v3, v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 1404
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1412
    .end local v2    # "r":Lorg/achartengine/renderer/XYSeriesRenderer;
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    .line 1413
    .local v0, "chartSer":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;
    const-string/jumbo v5, "cat"

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v4

    .line 1415
    .local v4, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_3

    .line 1416
    add-int/lit8 v5, v1, 0x1

    int-to-double v6, v5

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v3, v6, v7, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    .line 1415
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1419
    :cond_3
    const v5, -0xbbbbbc

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxesColor(I)V

    .line 1420
    const v5, -0x333334

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsColor(I)V

    .line 1421
    return-object v3

    .line 1403
    :array_0
    .array-data 4
        0x14
        0x1e
        0x1e
        0x1e
    .end array-data
.end method

.method private getLineChartRenderer(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 10
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    .line 1331
    new-instance v3, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 1332
    .local v3, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v5, 0x41800000    # 16.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 1333
    const/high16 v5, 0x41a00000    # 20.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 1334
    const/high16 v5, 0x41200000    # 10.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 1335
    const/high16 v5, 0x41700000    # 15.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 1336
    const/high16 v5, 0x40a00000    # 5.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setPointSize(F)V

    .line 1337
    const-wide/16 v6, 0x0

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 1338
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v6, "val"

    invoke-virtual {v5, v6}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    int-to-double v6, v5

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 1341
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    const-wide/16 v8, 0x0

    cmpg-double v5, v6, v8

    if-gez v5, :cond_0

    .line 1342
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1343
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1344
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1346
    :cond_0
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 1348
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    const-wide/16 v8, 0x0

    cmpg-double v5, v6, v8

    if-gez v5, :cond_1

    .line 1349
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1350
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1351
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1354
    :cond_1
    const v5, -0x333334

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setGridColor(I)V

    .line 1355
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setShowGridX(Z)V

    .line 1356
    const/4 v5, 0x4

    new-array v5, v5, [I

    fill-array-data v5, :array_0

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 1357
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_2

    .line 1358
    new-instance v2, Lorg/achartengine/renderer/XYSeriesRenderer;

    invoke-direct {v2}, Lorg/achartengine/renderer/XYSeriesRenderer;-><init>()V

    .line 1359
    .local v2, "r":Lorg/achartengine/renderer/XYSeriesRenderer;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mColorArray:[I

    aget v5, v5, v1

    invoke-virtual {v2, v5}, Lorg/achartengine/renderer/XYSeriesRenderer;->setColor(I)V

    .line 1360
    sget-object v5, Lorg/achartengine/chart/PointStyle;->SQUARE:Lorg/achartengine/chart/PointStyle;

    invoke-virtual {v2, v5}, Lorg/achartengine/renderer/XYSeriesRenderer;->setPointStyle(Lorg/achartengine/chart/PointStyle;)V

    .line 1361
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Lorg/achartengine/renderer/XYSeriesRenderer;->setFillPoints(Z)V

    .line 1362
    invoke-virtual {v3, v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 1357
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1365
    .end local v2    # "r":Lorg/achartengine/renderer/XYSeriesRenderer;
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    .line 1366
    .local v0, "chartSer":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;
    const-string/jumbo v5, "cat"

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v4

    .line 1368
    .local v4, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_3

    .line 1369
    add-int/lit8 v5, v1, 0x1

    int-to-double v6, v5

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v3, v6, v7, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    .line 1368
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1372
    :cond_3
    const v5, -0xbbbbbc

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxesColor(I)V

    .line 1373
    const v5, -0x333334

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsColor(I)V

    .line 1374
    return-object v3

    .line 1356
    :array_0
    .array-data 4
        0x14
        0x1e
        0x1e
        0x1e
    .end array-data
.end method

.method private getRendererForPieChart(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/renderer/DefaultRenderer;
    .locals 12
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .prologue
    const/high16 v6, 0x41700000    # 15.0f

    const-wide/16 v10, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1746
    new-instance v5, Lorg/achartengine/renderer/DefaultRenderer;

    invoke-direct {v5}, Lorg/achartengine/renderer/DefaultRenderer;-><init>()V

    .line 1747
    .local v5, "renderer":Lorg/achartengine/renderer/DefaultRenderer;
    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setLabelsTextSize(F)V

    .line 1748
    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setLegendTextSize(F)V

    .line 1749
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_0

    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setMargins([I)V

    .line 1751
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mColorArray:[I

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget v1, v0, v2

    .line 1752
    .local v1, "color":I
    new-instance v4, Lorg/achartengine/renderer/SimpleSeriesRenderer;

    invoke-direct {v4}, Lorg/achartengine/renderer/SimpleSeriesRenderer;-><init>()V

    .line 1753
    .local v4, "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    invoke-virtual {v4, v1}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setColor(I)V

    .line 1754
    invoke-virtual {v5, v4}, Lorg/achartengine/renderer/DefaultRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 1751
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1756
    .end local v1    # "color":I
    .end local v4    # "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    :cond_0
    invoke-virtual {v5, v7}, Lorg/achartengine/renderer/DefaultRenderer;->setZoomButtonsVisible(Z)V

    .line 1757
    invoke-virtual {v5, v7}, Lorg/achartengine/renderer/DefaultRenderer;->setZoomEnabled(Z)V

    .line 1758
    const/high16 v6, 0x41a00000    # 20.0f

    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setChartTitleTextSize(F)V

    .line 1759
    invoke-virtual {v5, v8}, Lorg/achartengine/renderer/DefaultRenderer;->setDisplayValues(Z)V

    .line 1760
    invoke-virtual {v5, v8}, Lorg/achartengine/renderer/DefaultRenderer;->setShowLabels(Z)V

    .line 1761
    invoke-virtual {v5, v8}, Lorg/achartengine/renderer/DefaultRenderer;->getSeriesRendererAt(I)Lorg/achartengine/renderer/SimpleSeriesRenderer;

    move-result-object v4

    .line 1762
    .restart local v4    # "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    invoke-virtual {v4, v7}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setGradientEnabled(Z)V

    .line 1763
    const v6, -0xffff01

    invoke-virtual {v4, v10, v11, v6}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setGradientStart(DI)V

    .line 1764
    const v6, -0xff0100

    invoke-virtual {v4, v10, v11, v6}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setGradientStop(DI)V

    .line 1765
    invoke-virtual {v4, v7}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setHighlighted(Z)V

    .line 1767
    return-object v5

    .line 1749
    :array_0
    .array-data 4
        0x14
        0x1e
        0xf
        0x3c
    .end array-data
.end method

.method private getRendererForPieChart(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/renderer/DefaultRenderer;
    .locals 12
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    const/high16 v6, 0x41700000    # 15.0f

    const-wide/16 v10, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1721
    new-instance v5, Lorg/achartengine/renderer/DefaultRenderer;

    invoke-direct {v5}, Lorg/achartengine/renderer/DefaultRenderer;-><init>()V

    .line 1722
    .local v5, "renderer":Lorg/achartengine/renderer/DefaultRenderer;
    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setLabelsTextSize(F)V

    .line 1723
    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setLegendTextSize(F)V

    .line 1724
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_0

    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setMargins([I)V

    .line 1726
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mColorArray:[I

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget v1, v0, v2

    .line 1727
    .local v1, "color":I
    new-instance v4, Lorg/achartengine/renderer/SimpleSeriesRenderer;

    invoke-direct {v4}, Lorg/achartengine/renderer/SimpleSeriesRenderer;-><init>()V

    .line 1728
    .local v4, "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    invoke-virtual {v4, v1}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setColor(I)V

    .line 1729
    invoke-virtual {v5, v4}, Lorg/achartengine/renderer/DefaultRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 1726
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1731
    .end local v1    # "color":I
    .end local v4    # "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    :cond_0
    invoke-virtual {v5, v7}, Lorg/achartengine/renderer/DefaultRenderer;->setZoomButtonsVisible(Z)V

    .line 1732
    invoke-virtual {v5, v7}, Lorg/achartengine/renderer/DefaultRenderer;->setZoomEnabled(Z)V

    .line 1733
    const/high16 v6, 0x41a00000    # 20.0f

    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setChartTitleTextSize(F)V

    .line 1734
    invoke-virtual {v5, v8}, Lorg/achartengine/renderer/DefaultRenderer;->setDisplayValues(Z)V

    .line 1735
    invoke-virtual {v5, v8}, Lorg/achartengine/renderer/DefaultRenderer;->setShowLabels(Z)V

    .line 1736
    invoke-virtual {v5, v8}, Lorg/achartengine/renderer/DefaultRenderer;->getSeriesRendererAt(I)Lorg/achartengine/renderer/SimpleSeriesRenderer;

    move-result-object v4

    .line 1737
    .restart local v4    # "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    invoke-virtual {v4, v7}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setGradientEnabled(Z)V

    .line 1738
    const v6, -0xffff01

    invoke-virtual {v4, v10, v11, v6}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setGradientStart(DI)V

    .line 1739
    const v6, -0xff0100

    invoke-virtual {v4, v10, v11, v6}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setGradientStop(DI)V

    .line 1740
    invoke-virtual {v4, v7}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setHighlighted(Z)V

    .line 1742
    return-object v5

    .line 1724
    :array_0
    .array-data 4
        0x14
        0x1e
        0xf
        0x3c
    .end array-data
.end method

.method private getRendererForStockChart(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 14
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .prologue
    const-wide/16 v12, 0x0

    const/4 v10, 0x0

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    .line 516
    new-instance v3, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 517
    .local v3, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v6, 0x41800000    # 16.0f

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 518
    const/high16 v6, 0x41a00000    # 20.0f

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 519
    const/high16 v6, 0x41200000    # 10.0f

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 520
    const/high16 v6, 0x41700000    # 15.0f

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 522
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_0

    .line 523
    new-instance v2, Lorg/achartengine/renderer/SimpleSeriesRenderer;

    invoke-direct {v2}, Lorg/achartengine/renderer/SimpleSeriesRenderer;-><init>()V

    .line 524
    .local v2, "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    const/high16 v6, -0x1000000

    invoke-virtual {v2, v6}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setColor(I)V

    .line 525
    invoke-virtual {v3, v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 522
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 528
    .end local v2    # "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    :cond_0
    const-string/jumbo v6, "Month"

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXTitle(Ljava/lang/String;)V

    .line 530
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    cmpg-double v6, v6, v12

    if-gez v6, :cond_1

    .line 531
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 532
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 533
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 535
    :cond_1
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 537
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    cmpg-double v6, v6, v12

    if-gez v6, :cond_2

    .line 538
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 539
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 540
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 542
    :cond_2
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMax(D)V

    .line 543
    invoke-virtual {v3, v12, v13}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 544
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v7, "val"

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    int-to-double v6, v6

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 547
    const v6, -0x777778

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxesColor(I)V

    .line 548
    const v6, -0x333334

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsColor(I)V

    .line 549
    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setBarWidth(F)V

    .line 550
    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setBarSpacing(D)V

    .line 551
    invoke-virtual {v3, v10}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXLabels(I)V

    .line 552
    const/16 v6, 0xa

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYLabels(I)V

    .line 554
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    .line 555
    .local v0, "chartSer":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;
    const-string/jumbo v6, "cat"

    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v4

    .line 557
    .local v4, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_3

    .line 559
    add-int/lit8 v6, v1, 0x1

    int-to-double v8, v6

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v3, v8, v9, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    .line 557
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 562
    :cond_3
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_0

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 564
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_4

    .line 565
    invoke-virtual {v3, v1}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getSeriesRendererAt(I)Lorg/achartengine/renderer/SimpleSeriesRenderer;

    move-result-object v5

    .line 567
    .local v5, "seriesRenderer":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    invoke-virtual {v5, v10}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setDisplayChartValues(Z)V

    .line 568
    const/high16 v6, 0x40a00000    # 5.0f

    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setChartValuesSpacing(F)V

    .line 564
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 571
    .end local v5    # "seriesRenderer":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    :cond_4
    return-object v3

    .line 562
    :array_0
    .array-data 4
        0x1e
        0x46
        0x1e
        0x1e
    .end array-data
.end method

.method private getRendererForStockChart(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 14
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    const-wide/16 v12, 0x0

    const/4 v10, 0x0

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    .line 457
    new-instance v3, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 458
    .local v3, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v6, 0x41800000    # 16.0f

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 459
    const/high16 v6, 0x41a00000    # 20.0f

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 460
    const/high16 v6, 0x41200000    # 10.0f

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 461
    const/high16 v6, 0x41700000    # 15.0f

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 463
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_0

    .line 464
    new-instance v2, Lorg/achartengine/renderer/SimpleSeriesRenderer;

    invoke-direct {v2}, Lorg/achartengine/renderer/SimpleSeriesRenderer;-><init>()V

    .line 465
    .local v2, "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    const/high16 v6, -0x1000000

    invoke-virtual {v2, v6}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setColor(I)V

    .line 466
    invoke-virtual {v3, v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 463
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 469
    .end local v2    # "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    :cond_0
    const-string/jumbo v6, "Month"

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXTitle(Ljava/lang/String;)V

    .line 471
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    cmpg-double v6, v6, v12

    if-gez v6, :cond_1

    .line 472
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 473
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 474
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 476
    :cond_1
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 478
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    cmpg-double v6, v6, v12

    if-gez v6, :cond_2

    .line 479
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 480
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 481
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    mul-double/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 483
    :cond_2
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMax(D)V

    .line 484
    invoke-virtual {v3, v12, v13}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 485
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v7, "val"

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    int-to-double v6, v6

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 488
    const v6, -0x777778

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxesColor(I)V

    .line 489
    const v6, -0x333334

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsColor(I)V

    .line 490
    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setBarWidth(F)V

    .line 491
    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setBarSpacing(D)V

    .line 492
    invoke-virtual {v3, v10}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXLabels(I)V

    .line 493
    const/16 v6, 0xa

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYLabels(I)V

    .line 495
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    .line 496
    .local v0, "chartSer":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;
    const-string/jumbo v6, "cat"

    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v4

    .line 498
    .local v4, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_3

    .line 500
    add-int/lit8 v6, v1, 0x1

    int-to-double v8, v6

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v3, v8, v9, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    .line 498
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 503
    :cond_3
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_0

    invoke-virtual {v3, v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 505
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_4

    .line 506
    invoke-virtual {v3, v1}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getSeriesRendererAt(I)Lorg/achartengine/renderer/SimpleSeriesRenderer;

    move-result-object v5

    .line 508
    .local v5, "seriesRenderer":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    invoke-virtual {v5, v10}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setDisplayChartValues(Z)V

    .line 509
    const/high16 v6, 0x40a00000    # 5.0f

    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setChartValuesSpacing(F)V

    .line 505
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 512
    .end local v5    # "seriesRenderer":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    :cond_4
    return-object v3

    .line 503
    :array_0
    .array-data 4
        0x1e
        0x46
        0x1e
        0x1e
    .end array-data
.end method

.method private getRendrerForBubbleChart(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 10
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .prologue
    const/high16 v4, 0x41700000    # 15.0f

    const-wide/16 v8, 0x0

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    .line 1080
    new-instance v2, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 1081
    .local v2, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v3, 0x41800000    # 16.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 1082
    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 1083
    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 1084
    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 1085
    const/4 v3, 0x4

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 1086
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 1087
    new-instance v1, Lorg/achartengine/renderer/XYSeriesRenderer;

    invoke-direct {v1}, Lorg/achartengine/renderer/XYSeriesRenderer;-><init>()V

    .line 1088
    .local v1, "newTicketRenderer":Lorg/achartengine/renderer/XYSeriesRenderer;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mColorArray:[I

    aget v3, v3, v0

    invoke-virtual {v1, v3}, Lorg/achartengine/renderer/XYSeriesRenderer;->setColor(I)V

    .line 1089
    invoke-virtual {v2, v1}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 1086
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1092
    .end local v1    # "newTicketRenderer":Lorg/achartengine/renderer/XYSeriesRenderer;
    :cond_0
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_1

    .line 1093
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    .line 1094
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    .line 1095
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    .line 1097
    :cond_1
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 1099
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_2

    .line 1100
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    .line 1101
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    .line 1102
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    .line 1104
    :cond_2
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 1106
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_3

    .line 1107
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1108
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1109
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1111
    :cond_3
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 1113
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_4

    .line 1114
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1115
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1116
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1118
    :cond_4
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMax(D)V

    .line 1120
    const v3, -0x777778

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxesColor(I)V

    .line 1121
    const v3, -0x333334

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsColor(I)V

    .line 1122
    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXLabels(I)V

    .line 1123
    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYLabels(I)V

    .line 1124
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setShowGrid(Z)V

    .line 1125
    return-object v2

    .line 1085
    :array_0
    .array-data 4
        0x14
        0x1e
        0x1e
        0x14
    .end array-data
.end method

.method private getRendrerForBubbleChart(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 10
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    const/high16 v4, 0x41700000    # 15.0f

    const-wide/16 v8, 0x0

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    .line 1031
    new-instance v2, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 1032
    .local v2, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v3, 0x41800000    # 16.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 1033
    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 1034
    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 1035
    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 1036
    const/4 v3, 0x4

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 1037
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 1038
    new-instance v1, Lorg/achartengine/renderer/XYSeriesRenderer;

    invoke-direct {v1}, Lorg/achartengine/renderer/XYSeriesRenderer;-><init>()V

    .line 1039
    .local v1, "newTicketRenderer":Lorg/achartengine/renderer/XYSeriesRenderer;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mColorArray:[I

    aget v3, v3, v0

    invoke-virtual {v1, v3}, Lorg/achartengine/renderer/XYSeriesRenderer;->setColor(I)V

    .line 1040
    invoke-virtual {v2, v1}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 1037
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1043
    .end local v1    # "newTicketRenderer":Lorg/achartengine/renderer/XYSeriesRenderer;
    :cond_0
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_1

    .line 1044
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    .line 1045
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    .line 1046
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    .line 1048
    :cond_1
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 1050
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_2

    .line 1051
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    .line 1052
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    .line 1053
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    .line 1055
    :cond_2
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 1057
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_3

    .line 1058
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1059
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1060
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1062
    :cond_3
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 1064
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_4

    .line 1065
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1066
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1067
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1069
    :cond_4
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMax(D)V

    .line 1071
    const v3, -0x777778

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxesColor(I)V

    .line 1072
    const v3, -0x333334

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsColor(I)V

    .line 1073
    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXLabels(I)V

    .line 1074
    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYLabels(I)V

    .line 1075
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setShowGrid(Z)V

    .line 1076
    return-object v2

    .line 1036
    :array_0
    .array-data 4
        0x14
        0x1e
        0x1e
        0x14
    .end array-data
.end method

.method private getScatterChartDataset(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;
    .locals 22
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .prologue
    .line 795
    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    .line 796
    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    .line 797
    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 798
    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 800
    new-instance v8, Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-direct {v8}, Lorg/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    .line 802
    .local v8, "dataset":Lorg/achartengine/model/XYMultipleSeriesDataset;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v10, v0, :cond_5

    .line 803
    new-instance v15, Lorg/achartengine/model/XYSeries;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v19, "tx"

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v15, v0}, Lorg/achartengine/model/XYSeries;-><init>(Ljava/lang/String;)V

    .line 806
    .local v15, "series":Lorg/achartengine/model/XYSeries;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v19, "cat"

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v13

    .line 808
    .local v13, "serXVal":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v19, "val"

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v14

    .line 811
    .local v14, "serYVal":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .line 812
    .local v11, "it1":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .line 814
    .local v12, "it2":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_4

    .line 815
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 816
    .local v16, "str1":Ljava/lang/String;
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 819
    .local v17, "str2":Ljava/lang/String;
    :try_start_0
    invoke-static/range {v16 .. v16}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 820
    .local v4, "dXVal":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    move-wide/from16 v18, v0

    cmpl-double v18, v4, v18

    if-lez v18, :cond_0

    .line 821
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    .line 822
    :cond_0
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    move-wide/from16 v18, v0

    cmpg-double v18, v4, v18

    if-gez v18, :cond_1

    const-wide/16 v18, 0x0

    cmpg-double v18, v4, v18

    if-gez v18, :cond_1

    .line 823
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    .line 825
    :cond_1
    invoke-static/range {v17 .. v17}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    .line 826
    .local v6, "dYVal":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    move-wide/from16 v18, v0

    cmpl-double v18, v6, v18

    if-lez v18, :cond_2

    .line 827
    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 828
    :cond_2
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    move-wide/from16 v18, v0

    cmpg-double v18, v6, v18

    if-gez v18, :cond_3

    const-wide/16 v18, 0x0

    cmpg-double v18, v6, v18

    if-gez v18, :cond_3

    .line 829
    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 831
    :cond_3
    invoke-virtual {v15, v4, v5, v6, v7}, Lorg/achartengine/model/XYSeries;->add(DD)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 833
    .end local v4    # "dXVal":D
    .end local v6    # "dYVal":D
    :catch_0
    move-exception v9

    .line 834
    .local v9, "ex":Ljava/lang/NumberFormatException;
    const-wide/16 v18, 0x0

    const-wide/16 v20, 0x0

    move-wide/from16 v0, v18

    move-wide/from16 v2, v20

    invoke-virtual {v15, v0, v1, v2, v3}, Lorg/achartengine/model/XYSeries;->add(DD)V

    goto :goto_1

    .line 837
    .end local v9    # "ex":Ljava/lang/NumberFormatException;
    .end local v16    # "str1":Ljava/lang/String;
    .end local v17    # "str2":Ljava/lang/String;
    :cond_4
    invoke-virtual {v8, v15}, Lorg/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lorg/achartengine/model/XYSeries;)V

    .line 802
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 839
    .end local v11    # "it1":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v12    # "it2":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v13    # "serXVal":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    .end local v14    # "serYVal":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    .end local v15    # "series":Lorg/achartengine/model/XYSeries;
    :cond_5
    return-object v8
.end method

.method private getScatterChartDataset(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;
    .locals 22
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    .line 746
    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    .line 747
    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    .line 748
    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 749
    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 751
    new-instance v8, Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-direct {v8}, Lorg/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    .line 753
    .local v8, "dataset":Lorg/achartengine/model/XYMultipleSeriesDataset;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v10, v0, :cond_5

    .line 754
    new-instance v15, Lorg/achartengine/model/XYSeries;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v19, "tx"

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v15, v0}, Lorg/achartengine/model/XYSeries;-><init>(Ljava/lang/String;)V

    .line 757
    .local v15, "series":Lorg/achartengine/model/XYSeries;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v19, "cat"

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v13

    .line 759
    .local v13, "serXVal":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v19, "val"

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v14

    .line 762
    .local v14, "serYVal":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .line 763
    .local v11, "it1":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .line 765
    .local v12, "it2":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_4

    .line 766
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 767
    .local v16, "str1":Ljava/lang/String;
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 770
    .local v17, "str2":Ljava/lang/String;
    :try_start_0
    invoke-static/range {v16 .. v16}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 771
    .local v4, "dXVal":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    move-wide/from16 v18, v0

    cmpl-double v18, v4, v18

    if-lez v18, :cond_0

    .line 772
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    .line 773
    :cond_0
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    move-wide/from16 v18, v0

    cmpg-double v18, v4, v18

    if-gez v18, :cond_1

    const-wide/16 v18, 0x0

    cmpg-double v18, v4, v18

    if-gez v18, :cond_1

    .line 774
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    .line 776
    :cond_1
    invoke-static/range {v17 .. v17}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    .line 777
    .local v6, "dYVal":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    move-wide/from16 v18, v0

    cmpl-double v18, v6, v18

    if-lez v18, :cond_2

    .line 778
    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 779
    :cond_2
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    move-wide/from16 v18, v0

    cmpg-double v18, v6, v18

    if-gez v18, :cond_3

    const-wide/16 v18, 0x0

    cmpg-double v18, v6, v18

    if-gez v18, :cond_3

    .line 780
    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 782
    :cond_3
    invoke-virtual {v15, v4, v5, v6, v7}, Lorg/achartengine/model/XYSeries;->add(DD)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 784
    .end local v4    # "dXVal":D
    .end local v6    # "dYVal":D
    :catch_0
    move-exception v9

    .line 785
    .local v9, "ex":Ljava/lang/NumberFormatException;
    const-wide/16 v18, 0x0

    const-wide/16 v20, 0x0

    move-wide/from16 v0, v18

    move-wide/from16 v2, v20

    invoke-virtual {v15, v0, v1, v2, v3}, Lorg/achartengine/model/XYSeries;->add(DD)V

    goto :goto_1

    .line 788
    .end local v9    # "ex":Ljava/lang/NumberFormatException;
    .end local v16    # "str1":Ljava/lang/String;
    .end local v17    # "str2":Ljava/lang/String;
    :cond_4
    invoke-virtual {v8, v15}, Lorg/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lorg/achartengine/model/XYSeries;)V

    .line 753
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 790
    .end local v11    # "it1":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v12    # "it2":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v13    # "serXVal":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    .end local v14    # "serYVal":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    .end local v15    # "series":Lorg/achartengine/model/XYSeries;
    :cond_5
    return-object v8
.end method

.method private getScatterChartRenderer(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 11
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .prologue
    const/4 v10, 0x1

    const/high16 v4, 0x41700000    # 15.0f

    const-wide/16 v8, 0x0

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    .line 894
    new-instance v2, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 895
    .local v2, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v3, 0x41800000    # 16.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 896
    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 897
    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 898
    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 899
    const/high16 v3, 0x40a00000    # 5.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setPointSize(F)V

    .line 901
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_0

    .line 902
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    .line 903
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    .line 904
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    .line 906
    :cond_0
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 908
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_1

    .line 909
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    .line 910
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    .line 911
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    .line 913
    :cond_1
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 915
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_2

    .line 916
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 917
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 918
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 920
    :cond_2
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 922
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_3

    .line 923
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 924
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 925
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 928
    :cond_3
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMax(D)V

    .line 929
    const/4 v3, 0x4

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 931
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_4

    .line 932
    new-instance v1, Lorg/achartengine/renderer/XYSeriesRenderer;

    invoke-direct {v1}, Lorg/achartengine/renderer/XYSeriesRenderer;-><init>()V

    .line 933
    .local v1, "r":Lorg/achartengine/renderer/XYSeriesRenderer;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mColorArray:[I

    aget v3, v3, v0

    invoke-virtual {v1, v3}, Lorg/achartengine/renderer/XYSeriesRenderer;->setColor(I)V

    .line 934
    invoke-virtual {v1, v10}, Lorg/achartengine/renderer/XYSeriesRenderer;->setFillPoints(Z)V

    .line 935
    invoke-virtual {v2, v1}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 931
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 938
    .end local v1    # "r":Lorg/achartengine/renderer/XYSeriesRenderer;
    :cond_4
    invoke-virtual {v2, v10}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setShowGrid(Z)V

    .line 939
    const v3, -0xbbbbbc

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxesColor(I)V

    .line 940
    const v3, -0x333334

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsColor(I)V

    .line 941
    return-object v2

    .line 929
    :array_0
    .array-data 4
        0x14
        0x1e
        0x1e
        0x1e
    .end array-data
.end method

.method private getScatterChartRenderer(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 11
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    const/4 v10, 0x1

    const/high16 v4, 0x41700000    # 15.0f

    const-wide/16 v8, 0x0

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    .line 843
    new-instance v2, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 844
    .local v2, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v3, 0x41800000    # 16.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 845
    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 846
    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 847
    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 848
    const/high16 v3, 0x40a00000    # 5.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setPointSize(F)V

    .line 850
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_0

    .line 851
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    .line 852
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    .line 853
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    .line 855
    :cond_0
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 857
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_1

    .line 858
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    .line 859
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    .line 860
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    .line 862
    :cond_1
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 864
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_2

    .line 865
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 866
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 867
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 869
    :cond_2
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 871
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_3

    .line 872
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 873
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 874
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 877
    :cond_3
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMax(D)V

    .line 878
    const/4 v3, 0x4

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 880
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_4

    .line 881
    new-instance v1, Lorg/achartengine/renderer/XYSeriesRenderer;

    invoke-direct {v1}, Lorg/achartengine/renderer/XYSeriesRenderer;-><init>()V

    .line 882
    .local v1, "r":Lorg/achartengine/renderer/XYSeriesRenderer;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mColorArray:[I

    aget v3, v3, v0

    invoke-virtual {v1, v3}, Lorg/achartengine/renderer/XYSeriesRenderer;->setColor(I)V

    .line 883
    invoke-virtual {v1, v10}, Lorg/achartengine/renderer/XYSeriesRenderer;->setFillPoints(Z)V

    .line 884
    invoke-virtual {v2, v1}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 880
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 887
    .end local v1    # "r":Lorg/achartengine/renderer/XYSeriesRenderer;
    :cond_4
    invoke-virtual {v2, v10}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setShowGrid(Z)V

    .line 888
    const v3, -0xbbbbbc

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxesColor(I)V

    .line 889
    const v3, -0x333334

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsColor(I)V

    .line 890
    return-object v2

    .line 878
    :array_0
    .array-data 4
        0x14
        0x1e
        0x1e
        0x1e
    .end array-data
.end method

.method public static removeResource()V
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->chartBuilder:Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;

    .line 82
    return-void
.end method


# virtual methods
.method public addColor()V
    .locals 2

    .prologue
    .line 2661
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->color:Ljava/util/ArrayList;

    .line 2662
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->random:Ljava/util/Random;

    .line 2664
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->color:Ljava/util/ArrayList;

    const-string/jumbo v1, "#FF0000"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2665
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->color:Ljava/util/ArrayList;

    const-string/jumbo v1, "#0000FF"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2666
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->color:Ljava/util/ArrayList;

    const-string/jumbo v1, "#00FF00"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2667
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->color:Ljava/util/ArrayList;

    const-string/jumbo v1, "#FFFF00"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2668
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->color:Ljava/util/ArrayList;

    const-string/jumbo v1, "#FF00FF"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2669
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->color:Ljava/util/ArrayList;

    const-string/jumbo v1, "#00FFFF"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2670
    return-void
.end method

.method public alignChart(Ljava/lang/StringBuilder;FF)Ljava/lang/String;
    .locals 1
    .param p1, "scriptFunc"    # Ljava/lang/StringBuilder;
    .param p2, "width"    # F
    .param p3, "height"    # F

    .prologue
    .line 2235
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public drawChart(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;Ljava/lang/StringBuilder;)V
    .locals 0
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;
    .param p2, "htmlHolder"    # Ljava/lang/StringBuilder;

    .prologue
    .line 121
    return-void
.end method

.method public drawChartForCanvas(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lcom/samsung/thumbnail/customview/hslf/ChartContents;
    .locals 15
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .prologue
    .line 124
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getChartType()I

    move-result v4

    .line 125
    .local v4, "chartType":I
    new-instance v3, Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-direct {v3}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;-><init>()V

    .line 126
    .local v3, "chartContents":Lcom/samsung/thumbnail/customview/hslf/ChartContents;
    sget-object v12, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPTX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 127
    packed-switch v4, :pswitch_data_0

    .line 224
    :goto_0
    :pswitch_0
    return-object v3

    .line 130
    :pswitch_1
    new-instance v1, Lorg/achartengine/chart/BarChart;

    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getBarChartDataset(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v12

    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getBarChartRenderer(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v13

    sget-object v14, Lorg/achartengine/chart/BarChart$Type;->DEFAULT:Lorg/achartengine/chart/BarChart$Type;

    invoke-direct {v1, v12, v13, v14}, Lorg/achartengine/chart/BarChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;Lorg/achartengine/chart/BarChart$Type;)V

    .line 132
    .local v1, "barChart":Lorg/achartengine/chart/AbstractChart;
    const/16 v12, 0xb

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 133
    invoke-virtual {v3, v1}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setColumnBarChart(Lorg/achartengine/chart/AbstractChart;)V

    .line 134
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getHeight()F

    move-result v12

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartHeight(F)V

    .line 135
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getWidth()F

    move-result v12

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartWidth(F)V

    goto :goto_0

    .line 139
    .end local v1    # "barChart":Lorg/achartengine/chart/AbstractChart;
    :pswitch_2
    new-instance v5, Lorg/achartengine/chart/DoughnutChart;

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getDoughnutChartDataset(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/model/CategorySeries;

    move-result-object v12

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getDoughnutChartRendrer(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/renderer/DefaultRenderer;

    move-result-object v13

    invoke-direct {v5, v12, v13}, Lorg/achartengine/chart/DoughnutChart;-><init>(Lorg/achartengine/model/CategorySeries;Lorg/achartengine/renderer/DefaultRenderer;)V

    .line 142
    .local v5, "donutChart":Lorg/achartengine/chart/DoughnutChart;
    const/16 v12, 0x9

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 143
    invoke-virtual {v3, v5}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setDoughnutChart(Lorg/achartengine/chart/DoughnutChart;)V

    .line 144
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getHeight()F

    move-result v12

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartHeight(F)V

    .line 145
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getWidth()F

    move-result v12

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartWidth(F)V

    goto :goto_0

    .line 149
    .end local v5    # "donutChart":Lorg/achartengine/chart/DoughnutChart;
    :pswitch_3
    new-instance v8, Lorg/achartengine/chart/PieChart;

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->buildCategoryDatasetForPieChart(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/model/CategorySeries;

    move-result-object v12

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getRendererForPieChart(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/renderer/DefaultRenderer;

    move-result-object v13

    invoke-direct {v8, v12, v13}, Lorg/achartengine/chart/PieChart;-><init>(Lorg/achartengine/model/CategorySeries;Lorg/achartengine/renderer/DefaultRenderer;)V

    .line 152
    .local v8, "pieChart":Lorg/achartengine/chart/PieChart;
    const/4 v12, 0x7

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 153
    invoke-virtual {v3, v8}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setPieChart(Lorg/achartengine/chart/PieChart;)V

    .line 154
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getHeight()F

    move-result v12

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartHeight(F)V

    .line 155
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getWidth()F

    move-result v12

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartWidth(F)V

    goto :goto_0

    .line 158
    .end local v8    # "pieChart":Lorg/achartengine/chart/PieChart;
    :pswitch_4
    new-instance v10, Lorg/achartengine/chart/ScatterChart;

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getScatterChartDataset(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v12

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getScatterChartRenderer(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v13

    invoke-direct {v10, v12, v13}, Lorg/achartengine/chart/ScatterChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;)V

    .line 161
    .local v10, "scatterChart":Lorg/achartengine/chart/XYChart;
    const/4 v12, 0x6

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 162
    invoke-virtual {v3, v10}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setScatterChart(Lorg/achartengine/chart/XYChart;)V

    .line 163
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getHeight()F

    move-result v12

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartHeight(F)V

    .line 164
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getWidth()F

    move-result v12

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartWidth(F)V

    goto/16 :goto_0

    .line 167
    .end local v10    # "scatterChart":Lorg/achartengine/chart/XYChart;
    :pswitch_5
    new-instance v2, Lorg/achartengine/chart/BubbleChart;

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getDataSetForBubbleChart(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v12

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getRendrerForBubbleChart(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v13

    invoke-direct {v2, v12, v13}, Lorg/achartengine/chart/BubbleChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;)V

    .line 170
    .local v2, "bubbleChart":Lorg/achartengine/chart/XYChart;
    const/16 v12, 0xf

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 171
    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setBubbleChart(Lorg/achartengine/chart/XYChart;)V

    .line 172
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getHeight()F

    move-result v12

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartHeight(F)V

    .line 173
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getWidth()F

    move-result v12

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartWidth(F)V

    goto/16 :goto_0

    .line 177
    .end local v2    # "bubbleChart":Lorg/achartengine/chart/XYChart;
    :pswitch_6
    new-instance v0, Lorg/achartengine/chart/TimeChart;

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getAreaChartDateDataset(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v12

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getAreaChartRenderer(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v13

    invoke-direct {v0, v12, v13}, Lorg/achartengine/chart/TimeChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;)V

    .line 179
    .local v0, "areaChart":Lorg/achartengine/chart/TimeChart;
    const/4 v12, 0x0

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 180
    invoke-virtual {v3, v0}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setAreaChart(Lorg/achartengine/chart/TimeChart;)V

    .line 181
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getHeight()F

    move-result v12

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartHeight(F)V

    .line 182
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getWidth()F

    move-result v12

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartWidth(F)V

    goto/16 :goto_0

    .line 185
    .end local v0    # "areaChart":Lorg/achartengine/chart/TimeChart;
    :pswitch_7
    new-instance v7, Lorg/achartengine/chart/LineChart;

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getLineChartDataset(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v12

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getLineChartRenderer(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v13

    invoke-direct {v7, v12, v13}, Lorg/achartengine/chart/LineChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;)V

    .line 187
    .local v7, "lineChart":Lorg/achartengine/chart/XYChart;
    const/4 v12, 0x2

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 188
    invoke-virtual {v3, v7}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setLineChart(Lorg/achartengine/chart/XYChart;)V

    .line 189
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getHeight()F

    move-result v12

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartHeight(F)V

    .line 190
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getWidth()F

    move-result v12

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartWidth(F)V

    goto/16 :goto_0

    .line 193
    .end local v7    # "lineChart":Lorg/achartengine/chart/XYChart;
    :pswitch_8
    new-instance v11, Lorg/achartengine/chart/RangeBarChart;

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getDataSetForStockChart(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v12

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getRendererForStockChart(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v13

    sget-object v14, Lorg/achartengine/chart/BarChart$Type;->STACKED:Lorg/achartengine/chart/BarChart$Type;

    invoke-direct {v11, v12, v13, v14}, Lorg/achartengine/chart/RangeBarChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;Lorg/achartengine/chart/BarChart$Type;)V

    .line 196
    .local v11, "stockChart":Lorg/achartengine/chart/RangeBarChart;
    const/4 v12, 0x4

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 197
    invoke-virtual {v3, v11}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setStockChart(Lorg/achartengine/chart/RangeBarChart;)V

    .line 198
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getHeight()F

    move-result v12

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartHeight(F)V

    .line 199
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getWidth()F

    move-result v12

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartWidth(F)V

    goto/16 :goto_0

    .line 202
    .end local v11    # "stockChart":Lorg/achartengine/chart/RangeBarChart;
    :pswitch_9
    new-instance v6, Lorg/achartengine/chart/ColumnChart;

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->buildHorizontalBarDataset(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v12

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->buildHorizontalBarRenderer(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v13

    invoke-direct {v6, v12, v13}, Lorg/achartengine/chart/ColumnChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;)V

    .line 205
    .local v6, "hChart":Lorg/achartengine/chart/ColumnChart;
    const/16 v12, 0x10

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 206
    invoke-virtual {v3, v6}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setHorizontalBarChart(Lorg/achartengine/chart/ColumnChart;)V

    .line 207
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getHeight()F

    move-result v12

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartHeight(F)V

    .line 208
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getWidth()F

    move-result v12

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartWidth(F)V

    goto/16 :goto_0

    .line 211
    .end local v6    # "hChart":Lorg/achartengine/chart/ColumnChart;
    :pswitch_a
    new-instance v9, Lorg/achartengine/chart/RadarChart;

    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getSeriesDataForRadarCanvas(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Ljava/util/ArrayList;

    move-result-object v12

    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getLableForRadar(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Ljava/util/ArrayList;

    move-result-object v13

    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getRadarSeriesForCanvas(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Ljava/util/ArrayList;

    move-result-object v14

    invoke-direct {v9, v12, v13, v14}, Lorg/achartengine/chart/RadarChart;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 214
    .local v9, "radarChartView":Lorg/achartengine/chart/RadarChart;
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getLableForRadar(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Ljava/util/ArrayList;

    .line 215
    const/4 v12, 0x5

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 216
    invoke-virtual {v3, v9}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setRadarChart(Lorg/achartengine/chart/RadarChart;)V

    .line 217
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getHeight()F

    move-result v12

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartHeight(F)V

    .line 218
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getWidth()F

    move-result v12

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartWidth(F)V

    goto/16 :goto_0

    .line 127
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_a
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_9
    .end packed-switch
.end method

.method public drawChartForCanvas(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V
    .locals 16
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;
    .param p2, "paragraph"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .prologue
    .line 228
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getChartType()I

    move-result v5

    .line 229
    .local v5, "chartType":I
    new-instance v4, Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-direct {v4}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;-><init>()V

    .line 230
    .local v4, "chartContents":Lcom/samsung/thumbnail/customview/hslf/ChartContents;
    sget-object v13, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 231
    packed-switch v5, :pswitch_data_0

    .line 327
    :goto_0
    :pswitch_0
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 328
    return-void

    .line 234
    :pswitch_1
    new-instance v2, Lorg/achartengine/chart/BarChart;

    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getBarChartDataset(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v13

    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getBarChartRenderer(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v14

    sget-object v15, Lorg/achartengine/chart/BarChart$Type;->DEFAULT:Lorg/achartengine/chart/BarChart$Type;

    invoke-direct {v2, v13, v14, v15}, Lorg/achartengine/chart/BarChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;Lorg/achartengine/chart/BarChart$Type;)V

    .line 236
    .local v2, "barChart":Lorg/achartengine/chart/AbstractChart;
    const/16 v13, 0xb

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 237
    invoke-virtual {v4, v2}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setColumnBarChart(Lorg/achartengine/chart/AbstractChart;)V

    .line 238
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getHeight()F

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartHeight(F)V

    .line 239
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getWidth()F

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartWidth(F)V

    goto :goto_0

    .line 243
    .end local v2    # "barChart":Lorg/achartengine/chart/AbstractChart;
    :pswitch_2
    new-instance v6, Lorg/achartengine/chart/DoughnutChart;

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getDoughnutChartDataset(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/model/CategorySeries;

    move-result-object v13

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getDoughnutChartRendrer(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/renderer/DefaultRenderer;

    move-result-object v14

    invoke-direct {v6, v13, v14}, Lorg/achartengine/chart/DoughnutChart;-><init>(Lorg/achartengine/model/CategorySeries;Lorg/achartengine/renderer/DefaultRenderer;)V

    .line 246
    .local v6, "donutChart":Lorg/achartengine/chart/DoughnutChart;
    const/16 v13, 0x9

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 247
    invoke-virtual {v4, v6}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setDoughnutChart(Lorg/achartengine/chart/DoughnutChart;)V

    .line 248
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getHeight()F

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartHeight(F)V

    .line 249
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getWidth()F

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartWidth(F)V

    goto :goto_0

    .line 253
    .end local v6    # "donutChart":Lorg/achartengine/chart/DoughnutChart;
    :pswitch_3
    new-instance v9, Lorg/achartengine/chart/PieChart;

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->buildCategoryDatasetForPieChart(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/model/CategorySeries;

    move-result-object v13

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getRendererForPieChart(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/renderer/DefaultRenderer;

    move-result-object v14

    invoke-direct {v9, v13, v14}, Lorg/achartengine/chart/PieChart;-><init>(Lorg/achartengine/model/CategorySeries;Lorg/achartengine/renderer/DefaultRenderer;)V

    .line 256
    .local v9, "pieChart":Lorg/achartengine/chart/PieChart;
    const/4 v13, 0x7

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 257
    invoke-virtual {v4, v9}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setPieChart(Lorg/achartengine/chart/PieChart;)V

    .line 258
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getHeight()F

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartHeight(F)V

    .line 259
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getWidth()F

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartWidth(F)V

    goto :goto_0

    .line 262
    .end local v9    # "pieChart":Lorg/achartengine/chart/PieChart;
    :pswitch_4
    new-instance v11, Lorg/achartengine/chart/ScatterChart;

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getScatterChartDataset(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v13

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getScatterChartRenderer(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v14

    invoke-direct {v11, v13, v14}, Lorg/achartengine/chart/ScatterChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;)V

    .line 265
    .local v11, "scatterChart":Lorg/achartengine/chart/XYChart;
    const/4 v13, 0x6

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 266
    invoke-virtual {v4, v11}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setScatterChart(Lorg/achartengine/chart/XYChart;)V

    .line 267
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getHeight()F

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartHeight(F)V

    .line 268
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getWidth()F

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartWidth(F)V

    goto/16 :goto_0

    .line 271
    .end local v11    # "scatterChart":Lorg/achartengine/chart/XYChart;
    :pswitch_5
    new-instance v3, Lorg/achartengine/chart/BubbleChart;

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getDataSetForBubbleChart(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v13

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getRendrerForBubbleChart(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v14

    invoke-direct {v3, v13, v14}, Lorg/achartengine/chart/BubbleChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;)V

    .line 274
    .local v3, "bubbleChart":Lorg/achartengine/chart/XYChart;
    const/16 v13, 0xf

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 275
    invoke-virtual {v4, v3}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setBubbleChart(Lorg/achartengine/chart/XYChart;)V

    .line 276
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getHeight()F

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartHeight(F)V

    .line 277
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getWidth()F

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartWidth(F)V

    goto/16 :goto_0

    .line 281
    .end local v3    # "bubbleChart":Lorg/achartengine/chart/XYChart;
    :pswitch_6
    new-instance v1, Lorg/achartengine/chart/TimeChart;

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getAreaChartDateDataset(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v13

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getAreaChartRenderer(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v14

    invoke-direct {v1, v13, v14}, Lorg/achartengine/chart/TimeChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;)V

    .line 283
    .local v1, "areaChart":Lorg/achartengine/chart/TimeChart;
    const/4 v13, 0x0

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 284
    invoke-virtual {v4, v1}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setAreaChart(Lorg/achartengine/chart/TimeChart;)V

    .line 285
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getHeight()F

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartHeight(F)V

    .line 286
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getWidth()F

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartWidth(F)V

    goto/16 :goto_0

    .line 289
    .end local v1    # "areaChart":Lorg/achartengine/chart/TimeChart;
    :pswitch_7
    new-instance v8, Lorg/achartengine/chart/LineChart;

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getLineChartDataset(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v13

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getLineChartRenderer(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v14

    invoke-direct {v8, v13, v14}, Lorg/achartengine/chart/LineChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;)V

    .line 291
    .local v8, "lineChart":Lorg/achartengine/chart/XYChart;
    const/4 v13, 0x2

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 292
    invoke-virtual {v4, v8}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setLineChart(Lorg/achartengine/chart/XYChart;)V

    .line 293
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getHeight()F

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartHeight(F)V

    .line 294
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getWidth()F

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartWidth(F)V

    goto/16 :goto_0

    .line 297
    .end local v8    # "lineChart":Lorg/achartengine/chart/XYChart;
    :pswitch_8
    new-instance v12, Lorg/achartengine/chart/RangeBarChart;

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getDataSetForStockChart(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v13

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getRendererForStockChart(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v14

    sget-object v15, Lorg/achartengine/chart/BarChart$Type;->STACKED:Lorg/achartengine/chart/BarChart$Type;

    invoke-direct {v12, v13, v14, v15}, Lorg/achartengine/chart/RangeBarChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;Lorg/achartengine/chart/BarChart$Type;)V

    .line 300
    .local v12, "stockChart":Lorg/achartengine/chart/RangeBarChart;
    const/4 v13, 0x4

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 301
    invoke-virtual {v4, v12}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setStockChart(Lorg/achartengine/chart/RangeBarChart;)V

    .line 302
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getHeight()F

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartHeight(F)V

    .line 303
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getWidth()F

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartWidth(F)V

    goto/16 :goto_0

    .line 306
    .end local v12    # "stockChart":Lorg/achartengine/chart/RangeBarChart;
    :pswitch_9
    new-instance v7, Lorg/achartengine/chart/ColumnChart;

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->buildHorizontalBarDataset(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v13

    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->buildHorizontalBarRenderer(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v14

    invoke-direct {v7, v13, v14}, Lorg/achartengine/chart/ColumnChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;)V

    .line 309
    .local v7, "hChart":Lorg/achartengine/chart/ColumnChart;
    const/16 v13, 0x10

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 310
    invoke-virtual {v4, v7}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setHorizontalBarChart(Lorg/achartengine/chart/ColumnChart;)V

    .line 311
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getHeight()F

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartHeight(F)V

    .line 312
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getWidth()F

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartWidth(F)V

    goto/16 :goto_0

    .line 315
    .end local v7    # "hChart":Lorg/achartengine/chart/ColumnChart;
    :pswitch_a
    new-instance v10, Lorg/achartengine/chart/RadarChart;

    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getSeriesDataForRadarCanvas(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Ljava/util/ArrayList;

    move-result-object v13

    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getLableForRadar(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Ljava/util/ArrayList;

    move-result-object v14

    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getRadarSeriesForCanvas(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Ljava/util/ArrayList;

    move-result-object v15

    invoke-direct {v10, v13, v14, v15}, Lorg/achartengine/chart/RadarChart;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 318
    .local v10, "radarChartView":Lorg/achartengine/chart/RadarChart;
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getLableForRadar(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Ljava/util/ArrayList;

    .line 319
    const/4 v13, 0x5

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 320
    invoke-virtual {v4, v10}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setRadarChart(Lorg/achartengine/chart/RadarChart;)V

    .line 321
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getHeight()F

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartHeight(F)V

    .line 322
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getWidth()F

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartWidth(F)V

    goto/16 :goto_0

    .line 231
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_a
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_9
    .end packed-switch
.end method

.method public getAreaChartCatagory(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Ljava/lang/String;
    .locals 7
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    .line 1994
    const-string/jumbo v3, ""

    .line 1995
    .local v3, "str":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    .line 1996
    .local v0, "chartSer":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;
    const-string/jumbo v5, "cat"

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v2

    .line 1998
    .local v2, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x100

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1999
    .local v4, "strBuilder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 2000
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "],"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1999
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2003
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "xaxis : { ticks : ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "] },"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2005
    return-object v3
.end method

.method public getAreaDataset(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Ljava/lang/String;
    .locals 8
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    .line 2165
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v6, 0x100

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2166
    .local v0, "actVal":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_1

    .line 2167
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "var d"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    add-int/lit8 v7, v1, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " = ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2168
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v7, "val"

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v4

    .line 2171
    .local v4, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    const/4 v3, 0x1

    .line 2172
    .local v3, "j":I
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 2173
    .local v5, "val":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "],"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2174
    add-int/lit8 v3, v3, 0x1

    .line 2175
    goto :goto_1

    .line 2176
    .end local v5    # "val":Ljava/lang/String;
    :cond_0
    const-string/jumbo v6, "];"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2166
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 2178
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "j":I
    .end local v4    # "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method public getAreaLabel(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Ljava/lang/String;
    .locals 6
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    .line 2087
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x100

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2089
    .local v2, "serVal":Ljava/lang/StringBuilder;
    const-string/jumbo v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2090
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 2092
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v4, "tx"

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v1

    .line 2096
    .local v1, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "{ label: \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\", data : d"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", points :{ fillcolor :\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getItem(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\", size : 5 }, color : \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getItem(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\'},"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2090
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2101
    .end local v1    # "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    :cond_0
    const-string/jumbo v3, "];"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2103
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public getBarChartDataset(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;
    .locals 14
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .prologue
    const-wide/16 v12, 0x0

    .line 1130
    iput-wide v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1131
    iput-wide v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1133
    new-instance v2, Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-direct {v2}, Lorg/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    .line 1135
    .local v2, "dataset":Lorg/achartengine/model/XYMultipleSeriesDataset;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v4, v9, :cond_3

    .line 1136
    new-instance v7, Lorg/achartengine/model/CategorySeries;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v10, "tx"

    invoke-virtual {v9, v10}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 1138
    .local v7, "series":Lorg/achartengine/model/CategorySeries;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v10, "val"

    invoke-virtual {v9, v10}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v6

    .line 1140
    .local v6, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 1142
    .local v8, "val":Ljava/lang/String;
    :try_start_0
    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 1143
    .local v0, "dVal":D
    iget-wide v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    cmpl-double v9, v0, v10

    if-lez v9, :cond_0

    .line 1144
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1145
    :cond_0
    iget-wide v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    cmpg-double v9, v0, v10

    if-gez v9, :cond_1

    cmpg-double v9, v0, v12

    if-gez v9, :cond_1

    .line 1146
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1147
    :cond_1
    invoke-virtual {v7, v0, v1}, Lorg/achartengine/model/CategorySeries;->add(D)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1148
    .end local v0    # "dVal":D
    :catch_0
    move-exception v3

    .line 1149
    .local v3, "ex":Ljava/lang/NumberFormatException;
    invoke-virtual {v7, v12, v13}, Lorg/achartengine/model/CategorySeries;->add(D)V

    goto :goto_1

    .line 1152
    .end local v3    # "ex":Ljava/lang/NumberFormatException;
    .end local v8    # "val":Ljava/lang/String;
    :cond_2
    invoke-virtual {v7}, Lorg/achartengine/model/CategorySeries;->toXYSeries()Lorg/achartengine/model/XYSeries;

    move-result-object v9

    invoke-virtual {v2, v9}, Lorg/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lorg/achartengine/model/XYSeries;)V

    .line 1135
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1154
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    .end local v7    # "series":Lorg/achartengine/model/CategorySeries;
    :cond_3
    return-object v2
.end method

.method public getBarChartDataset(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/model/XYMultipleSeriesDataset;
    .locals 14
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    const-wide/16 v12, 0x0

    .line 1159
    iput-wide v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1160
    iput-wide v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1162
    new-instance v2, Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-direct {v2}, Lorg/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    .line 1164
    .local v2, "dataset":Lorg/achartengine/model/XYMultipleSeriesDataset;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v4, v9, :cond_3

    .line 1165
    new-instance v7, Lorg/achartengine/model/CategorySeries;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v10, "tx"

    invoke-virtual {v9, v10}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 1167
    .local v7, "series":Lorg/achartengine/model/CategorySeries;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v10, "val"

    invoke-virtual {v9, v10}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v6

    .line 1169
    .local v6, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 1171
    .local v8, "val":Ljava/lang/String;
    :try_start_0
    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 1172
    .local v0, "dVal":D
    iget-wide v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    cmpl-double v9, v0, v10

    if-lez v9, :cond_0

    .line 1173
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1174
    :cond_0
    iget-wide v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    cmpg-double v9, v0, v10

    if-gez v9, :cond_1

    cmpg-double v9, v0, v12

    if-gez v9, :cond_1

    .line 1175
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1176
    :cond_1
    invoke-virtual {v7, v0, v1}, Lorg/achartengine/model/CategorySeries;->add(D)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1177
    .end local v0    # "dVal":D
    :catch_0
    move-exception v3

    .line 1178
    .local v3, "ex":Ljava/lang/NumberFormatException;
    invoke-virtual {v7, v12, v13}, Lorg/achartengine/model/CategorySeries;->add(D)V

    goto :goto_1

    .line 1181
    .end local v3    # "ex":Ljava/lang/NumberFormatException;
    .end local v8    # "val":Ljava/lang/String;
    :cond_2
    invoke-virtual {v7}, Lorg/achartengine/model/CategorySeries;->toXYSeries()Lorg/achartengine/model/XYSeries;

    move-result-object v9

    invoke-virtual {v2, v9}, Lorg/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lorg/achartengine/model/XYSeries;)V

    .line 1164
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1183
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    .end local v7    # "series":Lorg/achartengine/model/CategorySeries;
    :cond_3
    return-object v2
.end method

.method public getBarChartRenderer(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 13
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .prologue
    const/4 v12, 0x0

    const-wide/high16 v10, -0x4010000000000000L    # -1.0

    const-wide/16 v8, 0x0

    .line 1187
    new-instance v3, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 1188
    .local v3, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v5, 0x41800000    # 16.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 1189
    const/high16 v5, 0x41a00000    # 20.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 1190
    const/high16 v5, 0x41200000    # 10.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 1191
    const/high16 v5, 0x41700000    # 15.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 1192
    const/4 v5, 0x4

    new-array v5, v5, [I

    fill-array-data v5, :array_0

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 1194
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 1195
    new-instance v2, Lorg/achartengine/renderer/SimpleSeriesRenderer;

    invoke-direct {v2}, Lorg/achartengine/renderer/SimpleSeriesRenderer;-><init>()V

    .line 1196
    .local v2, "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mColorArray:[I

    aget v5, v5, v1

    invoke-virtual {v2, v5}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setColor(I)V

    .line 1197
    invoke-virtual {v3, v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 1194
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1200
    .end local v2    # "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    :cond_0
    invoke-virtual {v3, v8, v9}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 1201
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v6, "val"

    invoke-virtual {v5, v6}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    int-to-double v6, v5

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 1204
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    cmpg-double v5, v6, v8

    if-gez v5, :cond_1

    .line 1205
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    mul-double/2addr v6, v10

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1206
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1207
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    mul-double/2addr v6, v10

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1209
    :cond_1
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 1211
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    cmpg-double v5, v6, v8

    if-gez v5, :cond_2

    .line 1212
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    mul-double/2addr v6, v10

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1213
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1214
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    mul-double/2addr v6, v10

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1217
    :cond_2
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMax(D)V

    .line 1218
    const v5, -0x333334

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setGridColor(I)V

    .line 1219
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setShowGridX(Z)V

    .line 1220
    const-wide v6, 0x3fd3333333333333L    # 0.3

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setBarSpacing(D)V

    .line 1222
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    .line 1223
    .local v0, "chartSer":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;
    const-string/jumbo v5, "cat"

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v4

    .line 1225
    .local v4, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_3

    .line 1226
    add-int/lit8 v5, v1, 0x1

    int-to-double v6, v5

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v3, v6, v7, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    .line 1225
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1228
    :cond_3
    iput-wide v8, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1229
    iput-wide v8, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1230
    return-object v3

    .line 1192
    :array_0
    .array-data 4
        0x1e
        0x32
        0x1e
        0x1e
    .end array-data
.end method

.method public getBarChartRenderer(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 13
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    const/4 v12, 0x0

    const-wide/high16 v10, -0x4010000000000000L    # -1.0

    const-wide/16 v8, 0x0

    .line 1234
    new-instance v3, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 1235
    .local v3, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v5, 0x41800000    # 16.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 1236
    const/high16 v5, 0x41a00000    # 20.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 1237
    const/high16 v5, 0x41200000    # 10.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 1238
    const/high16 v5, 0x41700000    # 15.0f

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 1239
    const/4 v5, 0x4

    new-array v5, v5, [I

    fill-array-data v5, :array_0

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 1241
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 1242
    new-instance v2, Lorg/achartengine/renderer/SimpleSeriesRenderer;

    invoke-direct {v2}, Lorg/achartengine/renderer/SimpleSeriesRenderer;-><init>()V

    .line 1243
    .local v2, "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mColorArray:[I

    aget v5, v5, v1

    invoke-virtual {v2, v5}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setColor(I)V

    .line 1244
    invoke-virtual {v3, v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 1241
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1247
    .end local v2    # "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    :cond_0
    invoke-virtual {v3, v8, v9}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 1248
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v6, "val"

    invoke-virtual {v5, v6}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    int-to-double v6, v5

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 1251
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    cmpg-double v5, v6, v8

    if-gez v5, :cond_1

    .line 1252
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    mul-double/2addr v6, v10

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1253
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1254
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    mul-double/2addr v6, v10

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1256
    :cond_1
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 1258
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    cmpg-double v5, v6, v8

    if-gez v5, :cond_2

    .line 1259
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    mul-double/2addr v6, v10

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1260
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1261
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    mul-double/2addr v6, v10

    iput-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1264
    :cond_2
    iget-wide v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    invoke-static {v6, v7}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMax(D)V

    .line 1265
    const v5, -0x333334

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setGridColor(I)V

    .line 1266
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setShowGridX(Z)V

    .line 1267
    const-wide v6, 0x3fd3333333333333L    # 0.3

    invoke-virtual {v3, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setBarSpacing(D)V

    .line 1269
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    .line 1270
    .local v0, "chartSer":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;
    const-string/jumbo v5, "cat"

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v4

    .line 1272
    .local v4, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_3

    .line 1273
    add-int/lit8 v5, v1, 0x1

    int-to-double v6, v5

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v3, v6, v7, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    .line 1272
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1275
    :cond_3
    iput-wide v8, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMaxDataYValue:D

    .line 1276
    iput-wide v8, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->mMinDataYValue:D

    .line 1277
    return-object v3

    .line 1239
    :array_0
    .array-data 4
        0x1e
        0x32
        0x1e
        0x1e
    .end array-data
.end method

.method public getBubbleDataset(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Ljava/lang/String;
    .locals 8
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    .line 2454
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v6, 0x100

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2455
    .local v0, "actVal":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_1

    .line 2456
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "var d"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    add-int/lit8 v7, v1, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " = ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2457
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v7, "cat"

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v4

    .line 2459
    .local v4, "serXVal":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v7, "val"

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v5

    .line 2461
    .local v5, "serYVal":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v7, "size"

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v3

    .line 2464
    .local v3, "serSize":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v2, v6, :cond_0

    .line 2465
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "],"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2464
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2470
    :cond_0
    const-string/jumbo v6, "];"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2455
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 2472
    .end local v2    # "j":I
    .end local v3    # "serSize":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    .end local v4    # "serXVal":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    .end local v5    # "serYVal":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method public getChartCatagory(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Ljava/lang/String;
    .locals 6
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    .line 2362
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    .line 2363
    .local v0, "chartSer":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;
    const-string/jumbo v4, "cat"

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v2

    .line 2365
    .local v2, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x100

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2366
    .local v3, "strBuilder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 2367
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "],"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2366
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2370
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public getChartCatagoryForCanvas(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Ljava/lang/String;
    .locals 6
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .prologue
    .line 2389
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    .line 2390
    .local v0, "chartSer":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;
    const-string/jumbo v4, "cat"

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v2

    .line 2392
    .local v2, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x100

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2393
    .local v3, "strBuilder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 2394
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2393
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2396
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public getChartCatagoryForCanvas(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Ljava/lang/String;
    .locals 6
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    .line 2378
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    .line 2379
    .local v0, "chartSer":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;
    const-string/jumbo v4, "cat"

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v2

    .line 2381
    .local v2, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x100

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2382
    .local v3, "strBuilder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 2383
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2382
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2385
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public getColor()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2673
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->color:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getLabelForRadar(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Ljava/lang/String;
    .locals 6
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    .line 2528
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v4, 0x100

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2530
    .local v2, "serVal":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v5, "cat"

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v1

    .line 2532
    .local v1, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    const-string/jumbo v4, "data: ["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2533
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2534
    .local v3, "val":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "{label: \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\"},"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2536
    .end local v3    # "val":Ljava/lang/String;
    :cond_0
    const-string/jumbo v4, "],"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2537
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public getLableForRadar(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Ljava/util/ArrayList;
    .locals 11
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2577
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2579
    .local v6, "serVal":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v9, "cat"

    invoke-virtual {v8, v9}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v5

    .line 2581
    .local v5, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 2584
    .local v7, "val":Ljava/lang/String;
    :try_start_0
    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 2585
    .local v1, "dateNum":Ljava/lang/Long;
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {v0, v8, v9}, Ljava/util/Date;-><init>(J)V

    .line 2586
    .local v0, "date":Ljava/util/Date;
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v8, "dd/MM/yyyy"

    invoke-direct {v2, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 2587
    .local v2, "df2":Ljava/text/SimpleDateFormat;
    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2588
    .end local v0    # "date":Ljava/util/Date;
    .end local v1    # "dateNum":Ljava/lang/Long;
    .end local v2    # "df2":Ljava/text/SimpleDateFormat;
    :catch_0
    move-exception v3

    .line 2589
    .local v3, "e":Ljava/lang/Exception;
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2590
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2593
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v7    # "val":Ljava/lang/String;
    :cond_0
    return-object v6
.end method

.method public getLableForRadar(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Ljava/util/ArrayList;
    .locals 11
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2557
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2559
    .local v6, "serVal":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v9, "cat"

    invoke-virtual {v8, v9}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v5

    .line 2561
    .local v5, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 2564
    .local v7, "val":Ljava/lang/String;
    :try_start_0
    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 2565
    .local v1, "dateNum":Ljava/lang/Long;
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {v0, v8, v9}, Ljava/util/Date;-><init>(J)V

    .line 2566
    .local v0, "date":Ljava/util/Date;
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v8, "dd/MM/yyyy"

    invoke-direct {v2, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 2567
    .local v2, "df2":Ljava/text/SimpleDateFormat;
    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2568
    .end local v0    # "date":Ljava/util/Date;
    .end local v1    # "dateNum":Ljava/lang/Long;
    .end local v2    # "df2":Ljava/text/SimpleDateFormat;
    :catch_0
    move-exception v3

    .line 2569
    .local v3, "e":Ljava/lang/Exception;
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2570
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2573
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v7    # "val":Ljava/lang/String;
    :cond_0
    return-object v6
.end method

.method public getLineDataset(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Ljava/lang/String;
    .locals 8
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    .line 2125
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v6, 0x100

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2126
    .local v0, "actVal":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_1

    .line 2127
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "var d"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    add-int/lit8 v7, v1, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " = ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2128
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v7, "val"

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v4

    .line 2131
    .local v4, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    const/4 v3, 0x1

    .line 2132
    .local v3, "j":I
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 2133
    .local v5, "val":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "],"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2134
    add-int/lit8 v3, v3, 0x1

    .line 2135
    goto :goto_1

    .line 2136
    .end local v5    # "val":Ljava/lang/String;
    :cond_0
    const-string/jumbo v6, "];"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2126
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 2138
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "j":I
    .end local v4    # "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method public getLineSeries(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Ljava/lang/String;
    .locals 6
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    .line 2022
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x100

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2024
    .local v2, "serVal":Ljava/lang/StringBuilder;
    const-string/jumbo v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2025
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 2027
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v4, "tx"

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v1

    .line 2031
    .local v1, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "{ label: \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\", data : d"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", points :{ fillcolor :\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getItem(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\", size : 5 }, color : \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getItem(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\'},"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2025
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2036
    .end local v1    # "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    :cond_0
    const-string/jumbo v3, "];"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2038
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public getPieChartDataset(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Ljava/lang/String;
    .locals 7
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    .line 2272
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v5, 0x100

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2273
    .local v0, "actVal":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_1

    .line 2274
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v6, "cat"

    invoke-virtual {v5, v6}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v3

    .line 2276
    .local v3, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v6, "val"

    invoke-virtual {v5, v6}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v4

    .line 2279
    .local v4, "serVal":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    const-string/jumbo v5, "["

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2280
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v2, v5, :cond_0

    .line 2281
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "{label:\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\',data:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "},"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2280
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2284
    :cond_0
    const-string/jumbo v5, "];"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2273
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 2286
    .end local v2    # "j":I
    .end local v3    # "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    .end local v4    # "serVal":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public getPlotData(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Ljava/lang/String;
    .locals 6
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    .line 2476
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x100

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2478
    .local v2, "serVal":Ljava/lang/StringBuilder;
    const-string/jumbo v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2479
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 2480
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v4, "tx"

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v1

    .line 2484
    .local v1, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "{data :d"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ",color :\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->getItem(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\', label: \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\'},"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2479
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2487
    .end local v1    # "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    :cond_0
    const-string/jumbo v3, "],"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2488
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public getRadarSeriesForCanvas(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Ljava/util/ArrayList;
    .locals 5
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2611
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2616
    .local v2, "serVal":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    .line 2617
    .local v0, "chartSeries":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;
    const-string/jumbo v3, "tx"

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2621
    .end local v0    # "chartSeries":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;
    :cond_0
    return-object v2
.end method

.method public getRadarSeriesForCanvas(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Ljava/util/ArrayList;
    .locals 5
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2597
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2602
    .local v2, "serVal":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    .line 2603
    .local v0, "chartSeries":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;
    const-string/jumbo v3, "tx"

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2607
    .end local v0    # "chartSeries":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;
    :cond_0
    return-object v2
.end method

.method public getSeriesDataForRadar(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Ljava/lang/String;
    .locals 6
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    .line 2541
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x100

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2543
    .local v2, "serVal":Ljava/lang/StringBuilder;
    const-string/jumbo v3, "data = ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2544
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 2545
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v4, "tx"

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v1

    .line 2549
    .local v1, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "{label: \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\", data :d"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", spider: {show :true, lineWidth: 12 } },"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2544
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2552
    .end local v1    # "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    :cond_0
    const-string/jumbo v3, "];"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2553
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public getSeriesDataForRadarCanvas(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Ljava/util/ArrayList;
    .locals 8
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;",
            ")",
            "Ljava/util/ArrayList",
            "<[D>;"
        }
    .end annotation

    .prologue
    .line 2644
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2645
    .local v0, "dataSet":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[D>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v2, v5, :cond_1

    .line 2647
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v6, "val"

    invoke-virtual {v5, v6}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v4

    .line 2649
    .local v4, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v1, v5, [D

    .line 2651
    .local v1, "doubleArray":[D
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v3, v5, :cond_0

    .line 2652
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    aput-wide v6, v1, v3

    .line 2651
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2654
    :cond_0
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2645
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2657
    .end local v1    # "doubleArray":[D
    .end local v3    # "j":I
    .end local v4    # "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    :cond_1
    return-object v0
.end method

.method public getSeriesDataForRadarCanvas(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Ljava/util/ArrayList;
    .locals 8
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;",
            ")",
            "Ljava/util/ArrayList",
            "<[D>;"
        }
    .end annotation

    .prologue
    .line 2626
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2627
    .local v0, "dataSet":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[D>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v2, v5, :cond_1

    .line 2629
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v6, "val"

    invoke-virtual {v5, v6}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v4

    .line 2631
    .local v4, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v1, v5, [D

    .line 2633
    .local v1, "doubleArray":[D
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v3, v5, :cond_0

    .line 2634
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    aput-wide v6, v1, v3

    .line 2633
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2636
    :cond_0
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2627
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2639
    .end local v1    # "doubleArray":[D
    .end local v3    # "j":I
    .end local v4    # "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    :cond_1
    return-object v0
.end method

.method public setChartDataset(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)Ljava/lang/String;
    .locals 9
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    .line 2303
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v6, 0x100

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2304
    .local v0, "actVal":Ljava/lang/StringBuilder;
    const-string/jumbo v6, "["

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2305
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_1

    .line 2307
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v7, "tx"

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v4

    .line 2310
    .local v4, "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "{\"label\":\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\",\"data\":["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2312
    const/4 v3, 0x0

    .line 2313
    .local v3, "j":I
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getSeries()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    const-string/jumbo v7, "val"

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->getSeries(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    move-result-object v4

    .line 2314
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->getPtVal()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 2315
    .local v5, "val":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "],"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2316
    add-int/lit8 v3, v3, 0x1

    .line 2317
    goto :goto_1

    .line 2318
    .end local v5    # "val":Ljava/lang/String;
    :cond_0
    const-string/jumbo v6, "]},"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2305
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 2320
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "j":I
    .end local v4    # "ser":Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;
    :cond_1
    const-string/jumbo v6, "];"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2322
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method
