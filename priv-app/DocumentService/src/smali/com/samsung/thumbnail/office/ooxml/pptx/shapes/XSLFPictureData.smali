.class public final Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;
.super Lorg/apache/poi/POIXMLDocumentPart;
.source "XSLFPictureData.java"


# static fields
.field public static final PICTURE_TYPE_DIB:I = 0x7

.field public static final PICTURE_TYPE_EMF:I = 0x2

.field public static final PICTURE_TYPE_GIF:I = 0x8

.field public static final PICTURE_TYPE_JPEG:I = 0x5

.field public static final PICTURE_TYPE_PICT:I = 0x4

.field public static final PICTURE_TYPE_PNG:I = 0x6

.field public static final PICTURE_TYPE_WMF:I = 0x3

.field protected static final RELATIONS:[Lorg/apache/poi/POIXMLRelation;

.field private static final TAG:Ljava/lang/String; = "XSLFPictureData"


# instance fields
.field private file:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 96
    const/16 v0, 0x9

    new-array v0, v0, [Lorg/apache/poi/POIXMLRelation;

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->RELATIONS:[Lorg/apache/poi/POIXMLRelation;

    .line 97
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->RELATIONS:[Lorg/apache/poi/POIXMLRelation;

    const/4 v1, 0x2

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->IMAGE_EMF:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    aput-object v2, v0, v1

    .line 98
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->RELATIONS:[Lorg/apache/poi/POIXMLRelation;

    const/4 v1, 0x3

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->IMAGE_WMF:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    aput-object v2, v0, v1

    .line 99
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->RELATIONS:[Lorg/apache/poi/POIXMLRelation;

    const/4 v1, 0x4

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->IMAGE_PICT:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    aput-object v2, v0, v1

    .line 100
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->RELATIONS:[Lorg/apache/poi/POIXMLRelation;

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->IMAGE_JPEG:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    aput-object v2, v0, v1

    .line 101
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->RELATIONS:[Lorg/apache/poi/POIXMLRelation;

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->IMAGE_PNG:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    aput-object v2, v0, v1

    .line 102
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->RELATIONS:[Lorg/apache/poi/POIXMLRelation;

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->IMAGE_DIB:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    aput-object v2, v0, v1

    .line 103
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->RELATIONS:[Lorg/apache/poi/POIXMLRelation;

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->IMAGE_GIF:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    aput-object v2, v0, v1

    .line 104
    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Lorg/apache/poi/POIXMLDocumentPart;-><init>()V

    .line 111
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/POIXMLDocumentPart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V
    .locals 1
    .param p1, "master"    # Lorg/apache/poi/POIXMLDocumentPart;
    .param p2, "rel"    # Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    .prologue
    .line 129
    invoke-virtual {p1}, Lorg/apache/poi/POIXMLDocumentPart;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/poi/POIXMLDocumentPart;-><init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V

    .line 130
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V
    .locals 0
    .param p1, "part"    # Lorg/apache/poi/openxml4j/opc/PackagePart;
    .param p2, "rel"    # Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    .prologue
    .line 125
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/POIXMLDocumentPart;-><init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V

    .line 126
    return-void
.end method


# virtual methods
.method public getData()[B
    .locals 3

    .prologue
    .line 151
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getEntryName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->getInputStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 153
    .local v1, "is":Ljava/io/InputStream;
    if-eqz v1, :cond_0

    .line 154
    invoke-static {v1}, Lorg/apache/poi/util/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v2

    .line 156
    :goto_0
    return-object v2

    :cond_0
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 158
    .end local v1    # "is":Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 159
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Lorg/apache/poi/POIXMLException;

    invoke-direct {v2, v0}, Lorg/apache/poi/POIXMLException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public getFileName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getPartName()Lorg/apache/poi/openxml4j/opc/PackagePartName;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->getName()Ljava/lang/String;

    move-result-object v0

    .line 198
    .local v0, "name":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 199
    const/4 v1, 0x0

    .line 200
    :goto_0
    return-object v1

    :cond_0
    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getInputStream(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 10
    .param p1, "entryName"    # Ljava/lang/String;

    .prologue
    .line 164
    const/4 v1, 0x0

    .line 166
    .local v1, "inputStream":Ljava/io/InputStream;
    if-eqz p1, :cond_0

    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->file:Ljava/io/File;

    if-nez v7, :cond_1

    :cond_0
    move-object v6, v1

    .line 188
    :goto_0
    return-object v6

    .line 170
    :cond_1
    const/4 v5, 0x0

    .line 172
    .local v5, "zipInput":Ljava/util/zip/ZipInputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->file:Ljava/io/File;

    invoke-direct {v2, v7}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 174
    .end local v1    # "inputStream":Ljava/io/InputStream;
    .local v2, "inputStream":Ljava/io/InputStream;
    :try_start_1
    new-instance v6, Ljava/util/zip/ZipInputStream;

    invoke-direct {v6, v2}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 177
    .end local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    .local v6, "zipInput":Ljava/util/zip/ZipInputStream;
    :cond_2
    :try_start_2
    invoke-virtual {v6}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v4

    .local v4, "zipEntry":Ljava/util/zip/ZipEntry;
    if-eqz v4, :cond_3

    .line 178
    invoke-virtual {v4}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v3

    .line 179
    .local v3, "name":Ljava/lang/String;
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-result v7

    if-eqz v7, :cond_2

    move-object v1, v2

    .line 180
    .end local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    goto :goto_0

    .end local v1    # "inputStream":Ljava/io/InputStream;
    .end local v3    # "name":Ljava/lang/String;
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    :cond_3
    move-object v5, v6

    .end local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    move-object v1, v2

    .end local v2    # "inputStream":Ljava/io/InputStream;
    .end local v4    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    :goto_1
    move-object v6, v5

    .line 188
    goto :goto_0

    .line 183
    :catch_0
    move-exception v0

    .line 184
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_2
    const-string/jumbo v7, "XSLFPictureData"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Exception: FNFE"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 185
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 186
    .local v0, "e":Ljava/io/IOException;
    :goto_3
    const-string/jumbo v7, "XSLFPictureData"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "IOException: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 185
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "inputStream":Ljava/io/InputStream;
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    :catch_2
    move-exception v0

    move-object v1, v2

    .end local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    goto :goto_3

    .end local v1    # "inputStream":Ljava/io/InputStream;
    .end local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    :catch_3
    move-exception v0

    move-object v5, v6

    .end local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    move-object v1, v2

    .end local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    goto :goto_3

    .line 183
    .end local v1    # "inputStream":Ljava/io/InputStream;
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    goto :goto_2

    .end local v1    # "inputStream":Ljava/io/InputStream;
    .end local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    :catch_5
    move-exception v0

    move-object v5, v6

    .end local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    move-object v1, v2

    .end local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    goto :goto_2
.end method

.method public getPictureType()I
    .locals 3

    .prologue
    .line 218
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getContentType()Ljava/lang/String;

    move-result-object v0

    .line 219
    .local v0, "contentType":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->RELATIONS:[Lorg/apache/poi/POIXMLRelation;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 220
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->RELATIONS:[Lorg/apache/poi/POIXMLRelation;

    aget-object v2, v2, v1

    if-nez v2, :cond_1

    .line 219
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 224
    :cond_1
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->RELATIONS:[Lorg/apache/poi/POIXMLRelation;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lorg/apache/poi/POIXMLRelation;->getContentType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 228
    .end local v1    # "i":I
    :goto_1
    return v1

    .restart local v1    # "i":I
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public setFile(Ljava/io/File;)V
    .locals 0
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->file:Ljava/io/File;

    .line 134
    return-void
.end method

.method public suggestFileExtension()Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getPartName()Lorg/apache/poi/openxml4j/opc/PackagePartName;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->getExtension()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
