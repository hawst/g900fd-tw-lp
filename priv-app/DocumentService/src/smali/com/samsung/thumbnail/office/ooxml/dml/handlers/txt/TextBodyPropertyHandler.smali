.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextBodyPropertyHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "TextBodyPropertyHandler.java"


# instance fields
.field private bodyPr:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

.field private textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;)V
    .locals 2
    .param p1, "textShape"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    .prologue
    .line 24
    const/16 v0, 0x1f

    const-string/jumbo v1, "bodyPr"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 25
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextBodyPropertyHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    .line 26
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 4
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 60
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextBodyPropertyHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    if-eqz v2, :cond_0

    .line 61
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextBodyPropertyHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextBodyPropertyHandler;->bodyPr:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;->setBodyProp(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;)V

    .line 63
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    if-eqz v2, :cond_1

    .line 64
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getCurrentCellAnchor()Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    move-result-object v0

    .line 66
    .local v0, "cellAnchor":Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->getPara()Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;

    move-result-object v1

    .line 67
    .local v1, "excelPara":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;
    if-nez v1, :cond_2

    .line 68
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;

    .end local v1    # "excelPara":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;
    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;-><init>()V

    .line 69
    .restart local v1    # "excelPara":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextBodyPropertyHandler;->bodyPr:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;->setBodyProp(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;)V

    .line 70
    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->setPara(Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;)V

    .line 75
    .end local v0    # "cellAnchor":Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;
    .end local v1    # "excelPara":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;
    :cond_1
    :goto_0
    return-void

    .line 72
    .restart local v0    # "cellAnchor":Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;
    .restart local v1    # "excelPara":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;
    :cond_2
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextBodyPropertyHandler;->bodyPr:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;->setBodyProp(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;)V

    goto :goto_0
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 42
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 43
    invoke-virtual {p0, p2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextBodyPropertyHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "normAutofit"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 45
    const-string/jumbo v1, "fontScale"

    invoke-virtual {p0, p3, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextBodyPropertyHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 46
    .local v0, "val":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 47
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextBodyPropertyHandler;->bodyPr:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    div-int/lit16 v2, v2, 0x3e8

    iput v2, v1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;->fontScale:I

    .line 55
    .end local v0    # "val":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 31
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 32
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextBodyPropertyHandler;->bodyPr:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    .line 33
    const-string/jumbo v1, "anchor"

    invoke-virtual {p0, p3, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextBodyPropertyHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "vAlign":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 35
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextBodyPropertyHandler;->bodyPr:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;->setvAlign(Ljava/lang/String;)V

    .line 37
    :cond_0
    return-void
.end method
