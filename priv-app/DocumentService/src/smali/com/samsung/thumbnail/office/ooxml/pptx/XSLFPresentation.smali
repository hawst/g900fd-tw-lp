.class public Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;
.super Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;
.source "XSLFPresentation.java"


# instance fields
.field private defTxtStyle:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;",
            ">;"
        }
    .end annotation
.end field

.field private divHeight:D

.field private mTheme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

.field private masterRelIdLst:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private part:Lorg/apache/poi/POIXMLDocumentPart;

.field private slideHeight:I

.field private slideRelIdLst:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private slideWidth:I


# direct methods
.method public constructor <init>(Lorg/apache/poi/POIXMLDocumentPart;)V
    .locals 2
    .param p1, "part"    # Lorg/apache/poi/POIXMLDocumentPart;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;-><init>()V

    .line 24
    const-wide/high16 v0, 0x402e000000000000L    # 15.0

    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->divHeight:D

    .line 30
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->part:Lorg/apache/poi/POIXMLDocumentPart;

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->slideRelIdLst:Ljava/util/List;

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->masterRelIdLst:Ljava/util/List;

    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->defTxtStyle:Ljava/util/Map;

    .line 35
    return-void
.end method


# virtual methods
.method public addDefTextStyle(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;)V
    .locals 1
    .param p1, "level"    # Ljava/lang/String;
    .param p2, "paraProp"    # Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->defTxtStyle:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    return-void
.end method

.method public addMasterSlideID(Ljava/lang/String;)V
    .locals 1
    .param p1, "slideId"    # Ljava/lang/String;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->masterRelIdLst:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    return-void
.end method

.method public addSlideID(Ljava/lang/String;)V
    .locals 1
    .param p1, "slideId"    # Ljava/lang/String;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->slideRelIdLst:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    return-void
.end method

.method public addSlideSize(DD)V
    .locals 1
    .param p1, "width"    # D
    .param p3, "height"    # D

    .prologue
    .line 63
    double-to-int v0, p1

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->slideWidth:I

    .line 64
    double-to-int v0, p3

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->slideHeight:I

    .line 65
    return-void
.end method

.method public getDefTextStyleProp(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;
    .locals 1
    .param p1, "level"    # Ljava/lang/String;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->defTxtStyle:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;

    return-object v0
.end method

.method public getDivHeight()D
    .locals 2

    .prologue
    .line 92
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->divHeight:D

    return-wide v0
.end method

.method public getMasterSlideList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->masterRelIdLst:Ljava/util/List;

    return-object v0
.end method

.method protected getRootElementName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSlideHeight()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->slideHeight:I

    return v0
.end method

.method public getSlideList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->slideRelIdLst:Ljava/util/List;

    return-object v0
.end method

.method public getSlideWidth()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->slideWidth:I

    return v0
.end method

.method public getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->mTheme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    return-object v0
.end method

.method public parsePresentation(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "inputStream"    # Ljava/io/InputStream;

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->setTheme()V

    .line 40
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/XPresentationParser;

    invoke-direct {v0, p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/XPresentationParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;)V

    .line 41
    .local v0, "documentParser":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/XPresentationParser;
    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/XPresentationParser;->parse(Ljava/io/InputStream;)V

    .line 42
    return-void
.end method

.method public setDivHeight(D)V
    .locals 1
    .param p1, "divHeight"    # D

    .prologue
    .line 88
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->divHeight:D

    .line 89
    return-void
.end method

.method public setTheme()V
    .locals 3

    .prologue
    .line 45
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->part:Lorg/apache/poi/POIXMLDocumentPart;

    invoke-virtual {v2}, Lorg/apache/poi/POIXMLDocumentPart;->getRelations()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/POIXMLDocumentPart;

    .line 46
    .local v1, "p":Lorg/apache/poi/POIXMLDocumentPart;
    instance-of v2, v1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    if-eqz v2, :cond_0

    .line 47
    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .end local v1    # "p":Lorg/apache/poi/POIXMLDocumentPart;
    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->mTheme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .line 48
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->mTheme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->parseTheme()V

    .line 52
    :cond_1
    return-void
.end method
