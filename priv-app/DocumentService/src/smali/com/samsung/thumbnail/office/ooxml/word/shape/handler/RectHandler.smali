.class public Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/RectHandler;
.super Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;
.source "RectHandler.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "eleName"    # Ljava/lang/String;

    .prologue
    .line 18
    const/16 v0, 0x28

    invoke-direct {p0, v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;-><init>(ILjava/lang/String;)V

    .line 19
    return-void
.end method


# virtual methods
.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 44
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 45
    invoke-virtual {p0, p2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/RectHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 46
    .local v0, "ele":Ljava/lang/String;
    const-string/jumbo v1, "shadow"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 47
    const-string/jumbo v1, "color"

    invoke-virtual {p0, p3, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/RectHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 48
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/RectHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getColor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShadow(Ljava/lang/String;)V

    .line 50
    :cond_0
    return-void
.end method

.method protected init()V
    .locals 4

    .prologue
    .line 22
    invoke-super {p0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->init()V

    .line 24
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/FillHandler;

    const/16 v1, 0x28

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/RectHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/FillHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 25
    .local v0, "fillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/FillHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/RectHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "fill"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 31
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 33
    const-string/jumbo v2, "arcsize"

    invoke-virtual {p0, p3, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/RectHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "style":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 35
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    .line 36
    .local v1, "val":F
    const v2, 0x3efced91    # 0.494f

    mul-float/2addr v1, v2

    .line 37
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/RectHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    float-to-int v3, v1

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setAdj(II)V

    .line 39
    .end local v1    # "val":F
    :cond_0
    return-void
.end method
