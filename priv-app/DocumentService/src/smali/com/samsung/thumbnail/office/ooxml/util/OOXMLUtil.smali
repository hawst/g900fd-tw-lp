.class public Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;
.super Ljava/lang/Object;
.source "OOXMLUtil.java"


# static fields
.field public static final EMU_PER_INCH:I = 0x16530

.field public static final EMU_PER_PIXEL:I = 0x2535

.field public static final EMU_PER_POINT:I = 0x319c

.field public static final POINTS_FOR_HTML:F = 7.48f

.field public static final POINTS_PER_INCH:I = 0x48

.field public static final PT:I = 0x70

.field public static final PX:I = 0x6f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static covertPntToPx(F)F
    .locals 2
    .param p0, "valInPt"    # F

    .prologue
    .line 210
    const v0, 0x3faa9fbe    # 1.333f

    .line 211
    .local v0, "val":F
    mul-float v1, p0, v0

    return v1
.end method

.method public static doubleTheDigits(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "color"    # Ljava/lang/String;

    .prologue
    .line 260
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x100

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 261
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 262
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 263
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 264
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 262
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 266
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static eightsOfPoint(F)F
    .locals 5
    .param p0, "val"    # F

    .prologue
    const/high16 v4, 0x41200000    # 10.0f

    const/high16 v3, 0x41000000    # 8.0f

    .line 100
    invoke-static {v3, p0}, Ljava/lang/Math;->max(FF)F

    move-result p0

    .line 101
    mul-float v1, p0, v4

    const/high16 v2, 0x40800000    # 4.0f

    add-float/2addr v1, v2

    div-float v0, v1, v3

    .line 103
    .local v0, "points":F
    div-float v1, v0, v4

    return v1
.end method

.method public static eightsOfPoint(I)Ljava/lang/String;
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 93
    const/16 v1, 0x8

    invoke-static {v1, p0}, Ljava/lang/Math;->max(II)I

    move-result p0

    .line 94
    mul-int/lit8 v1, p0, 0xa

    add-int/lit8 v1, v1, 0x4

    int-to-float v1, v1

    const/high16 v2, 0x41000000    # 8.0f

    div-float v0, v1, v2

    .line 96
    .local v0, "points":F
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/high16 v2, 0x41200000    # 10.0f

    div-float v2, v0, v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "pt"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static eightsOfPointsToPoints(I)F
    .locals 2
    .param p0, "val"    # I

    .prologue
    .line 62
    int-to-float v0, p0

    const/high16 v1, 0x41000000    # 8.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public static emuToMaterDpi(I)I
    .locals 4
    .param p0, "val"    # I

    .prologue
    .line 57
    int-to-long v0, p0

    const-wide/16 v2, 0x2

    mul-long/2addr v0, v2

    const-wide/16 v2, 0xc67

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public static emuToPixel(F)F
    .locals 2
    .param p0, "val"    # F

    .prologue
    .line 53
    const v0, 0x495f3e00    # 914400.0f

    div-float v0, p0, v0

    const/high16 v1, 0x42900000    # 72.0f

    mul-float/2addr v0, v1

    const v1, 0x3faa3d71    # 1.33f

    mul-float/2addr v0, v1

    return v0
.end method

.method public static emuToPoints(F)F
    .locals 2
    .param p0, "val"    # F

    .prologue
    .line 45
    const v0, 0x45c67000    # 6350.0f

    add-float/2addr v0, p0

    const v1, 0x46467000    # 12700.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public static emuToPoints(I)I
    .locals 1
    .param p0, "val"    # I

    .prologue
    .line 41
    add-int/lit16 v0, p0, 0x18ce

    div-int/lit16 v0, v0, 0x319c

    return v0
.end method

.method public static emuToPoints1(F)F
    .locals 2
    .param p0, "val"    # F

    .prologue
    .line 49
    const v0, 0x495f3e00    # 914400.0f

    div-float v0, p0, v0

    const/high16 v1, 0x42900000    # 72.0f

    mul-float/2addr v0, v1

    return v0
.end method

.method public static fiftiethsToPoints(F)F
    .locals 2
    .param p0, "val"    # F

    .prologue
    .line 173
    const/high16 v0, 0x42480000    # 50.0f

    div-float v0, p0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float p0, v0, v1

    .line 175
    invoke-static {p0}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twentiethsToPoints(F)F

    move-result v0

    return v0
.end method

.method public static fiftiethsToPoints(I)Ljava/lang/String;
    .locals 2
    .param p0, "val"    # I

    .prologue
    .line 167
    div-int/lit8 v0, p0, 0x32

    mul-int/lit8 p0, v0, 0x64

    .line 169
    int-to-float v0, p0

    const/16 v1, 0x70

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twentiethsToPoints(FI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static fontSizeToPointsStr(I)Ljava/lang/String;
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 118
    and-int/lit8 v1, p0, 0x1

    mul-int/lit8 v1, v1, 0x5

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 119
    .local v0, "value2":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "pt"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getFitFontScale(FF)I
    .locals 2
    .param p0, "fSize"    # F
    .param p1, "fitScale"    # F

    .prologue
    .line 205
    const/high16 v1, 0x42c80000    # 100.0f

    div-float v1, p0, v1

    mul-float v0, v1, p1

    .line 206
    .local v0, "val":F
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v1

    return v1
.end method

.method public static hexToRGB(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;
    .locals 6
    .param p0, "colorStr"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x4

    const/4 v2, 0x2

    const/16 v4, 0x10

    .line 199
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x6

    invoke-virtual {p0, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    return-object v0
.end method

.method public static lineSpaceToPercentage(I)Ljava/lang/String;
    .locals 4
    .param p0, "val"    # I

    .prologue
    .line 179
    const-string/jumbo v0, ""

    .line 180
    .local v0, "point":Ljava/lang/String;
    mul-int/lit8 v2, p0, 0x64

    div-int/lit16 v1, v2, 0xf0

    .line 187
    .local v1, "pts":I
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 188
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 190
    return-object v0
.end method

.method public static masterDPIToPoints(I)I
    .locals 1
    .param p0, "val"    # I

    .prologue
    .line 107
    mul-int/lit8 v0, p0, 0x48

    div-int/lit16 v0, v0, 0x240

    return v0
.end method

.method public static normalizeAngle(I)I
    .locals 2
    .param p0, "rotation"    # I

    .prologue
    .line 81
    if-gez p0, :cond_0

    .line 82
    neg-int v1, p0

    rem-int/lit16 v0, v1, 0x168

    .line 83
    .local v0, "i":I
    rsub-int p0, v0, 0x168

    .line 85
    .end local v0    # "i":I
    :cond_0
    const/16 v1, 0x168

    if-lt p0, v1, :cond_1

    .line 86
    rem-int/lit16 p0, p0, 0x168

    .line 89
    :cond_1
    return p0
.end method

.method public static rotationAngelToDegrees16_16(I)I
    .locals 3
    .param p0, "rot"    # I

    .prologue
    .line 71
    mul-int/lit8 v1, p0, 0x5a

    const v2, 0x5265c0

    div-int v0, v1, v2

    .line 73
    .local v0, "rt":I
    return v0
.end method

.method public static rotationAngleToDegree_97Format(I)I
    .locals 1
    .param p0, "rawRotation"    # I

    .prologue
    .line 77
    shr-int/lit8 v0, p0, 0x10

    return v0
.end method

.method public static roundTwoDecimals(D)D
    .locals 4
    .param p0, "d"    # D

    .prologue
    .line 194
    new-instance v0, Ljava/text/DecimalFormat;

    const-string/jumbo v1, "#.##"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 195
    .local v0, "twoDForm":Ljava/text/DecimalFormat;
    invoke-virtual {v0, p0, p1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    return-wide v2
.end method

.method public static shapeAdjustValue(I)I
    .locals 1
    .param p0, "adjValue"    # I

    .prologue
    .line 111
    mul-int/lit8 v0, p0, 0x1b

    div-int/lit8 v0, v0, 0x7d

    return v0
.end method

.method public static stringToColor(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;
    .locals 7
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 221
    if-nez p0, :cond_0

    .line 222
    sget-object v5, Lorg/apache/poi/java/awt/Color;->LIGHT_GRAY:Lorg/apache/poi/java/awt/Color;

    .line 254
    :goto_0
    return-object v5

    .line 225
    :cond_0
    const-string/jumbo v5, "white"

    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string/jumbo v5, "#white"

    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 226
    :cond_1
    sget-object v5, Lorg/apache/poi/java/awt/Color;->WHITE:Lorg/apache/poi/java/awt/Color;

    goto :goto_0

    .line 229
    :cond_2
    const-string/jumbo v5, "#"

    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x6

    if-ge v5, v6, :cond_3

    .line 230
    invoke-static {p0}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->doubleTheDigits(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 234
    :cond_3
    :try_start_0
    const-string/jumbo v5, "#"

    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0x9

    if-ne v5, v6, :cond_4

    .line 235
    const-string/jumbo v5, "#"

    const-string/jumbo v6, ""

    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x10

    invoke-static {v5, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    .line 236
    .local v4, "rgba":I
    new-instance v5, Lorg/apache/poi/java/awt/Color;

    const/4 v6, 0x1

    invoke-direct {v5, v4, v6}, Lorg/apache/poi/java/awt/Color;-><init>(IZ)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 238
    .end local v4    # "rgba":I
    :catch_0
    move-exception v1

    .line 240
    .local v1, "e":Ljava/lang/NumberFormatException;
    sget-object v5, Lorg/apache/poi/java/awt/Color;->WHITE:Lorg/apache/poi/java/awt/Color;

    goto :goto_0

    .line 245
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    :cond_4
    :try_start_1
    invoke-static {p0}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v5

    goto :goto_0

    .line 246
    :catch_1
    move-exception v3

    .line 250
    .local v3, "nfe":Ljava/lang/NumberFormatException;
    :try_start_2
    const-class v5, Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v5, p0}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 251
    .local v2, "f":Ljava/lang/reflect/Field;
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/java/awt/Color;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 252
    .end local v2    # "f":Ljava/lang/reflect/Field;
    :catch_2
    move-exception v0

    .line 254
    .local v0, "ce":Ljava/lang/Exception;
    sget-object v5, Lorg/apache/poi/java/awt/Color;->LIGHT_GRAY:Lorg/apache/poi/java/awt/Color;

    goto :goto_0
.end method

.method public static toEMU(D)I
    .locals 2
    .param p0, "value"    # D

    .prologue
    .line 33
    const-wide v0, 0x40c8ce0000000000L    # 12700.0

    mul-double/2addr v0, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public static toPoints(J)D
    .locals 4
    .param p0, "emu"    # J

    .prologue
    .line 37
    long-to-double v0, p0

    const-wide v2, 0x40c8ce0000000000L    # 12700.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public static twentiethsToPointInts(FI)I
    .locals 3
    .param p0, "val"    # F
    .param p1, "unit"    # I

    .prologue
    .line 145
    const/4 v0, 0x0

    .line 146
    .local v0, "point":I
    const/high16 v2, 0x41a00000    # 20.0f

    div-float v1, p0, v2

    .line 148
    .local v1, "pts":F
    packed-switch p1, :pswitch_data_0

    .line 154
    float-to-int v0, v1

    .line 158
    :goto_0
    return v0

    .line 150
    :pswitch_0
    float-to-int v0, v1

    .line 151
    goto :goto_0

    .line 148
    :pswitch_data_0
    .packed-switch 0x6f
        :pswitch_0
    .end packed-switch
.end method

.method public static twentiethsToPoints(F)F
    .locals 2
    .param p0, "val"    # F

    .prologue
    .line 162
    const/high16 v1, 0x41a00000    # 20.0f

    div-float v0, p0, v1

    .line 163
    .local v0, "pts":F
    return v0
.end method

.method public static twentiethsToPoints(FI)Ljava/lang/String;
    .locals 4
    .param p0, "val"    # F
    .param p1, "unit"    # I

    .prologue
    .line 128
    const-string/jumbo v0, ""

    .line 129
    .local v0, "point":Ljava/lang/String;
    const/high16 v2, 0x41a00000    # 20.0f

    div-float v1, p0, v2

    .line 131
    .local v1, "pts":F
    packed-switch p1, :pswitch_data_0

    .line 137
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "pt"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 141
    :goto_0
    return-object v0

    .line 133
    :pswitch_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "px"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 134
    goto :goto_0

    .line 131
    :pswitch_data_0
    .packed-switch 0x6f
        :pswitch_0
    .end packed-switch
.end method

.method public static twipsMeasure(I)F
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 123
    int-to-float v1, p0

    const/high16 v2, 0x41a00000    # 20.0f

    div-float v0, v1, v2

    .line 124
    .local v0, "pts":F
    return v0
.end method
