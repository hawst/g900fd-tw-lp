.class public Lcom/samsung/thumbnail/office/ooxml/util/OOXMLElements;
.super Ljava/lang/Object;
.source "OOXMLElements.java"


# static fields
.field public static final ABSTRACT_NUM:Ljava/lang/String; = "abstractNum"

.field public static final ABSTRACT_NUM_ID:Ljava/lang/String; = "abstractNumId"

.field public static final ACCENT1_ELE:Ljava/lang/String; = "accent1"

.field public static final ACCENT2_ELE:Ljava/lang/String; = "accent2"

.field public static final ACCENT3_ELE:Ljava/lang/String; = "accent3"

.field public static final ACCENT4_ELE:Ljava/lang/String; = "accent4"

.field public static final ACCENT5_ELE:Ljava/lang/String; = "accent5"

.field public static final ACCENT6_ELE:Ljava/lang/String; = "accent6"

.field public static final ADJ:Ljava/lang/String; = "adj"

.field public static final ADJUST_VALUES_LIST_ELE:Ljava/lang/String; = "avLst"

.field public static final AFTER_ATTR:Ljava/lang/String; = "after"

.field public static final ALGN_ATTR:Ljava/lang/String; = "algn"

.field public static final ALL_VAL:Ljava/lang/String; = "all"

.field public static final ALPHA_ELE:Ljava/lang/String; = "alpha"

.field public static final ALPHA_OFF_ELE:Ljava/lang/String; = "alphaOff"

.field public static final ALTERNATE_CONTENT:Ljava/lang/String; = "AlternateContent"

.field public static final ANCHOR_ATTR:Ljava/lang/String; = "anchor"

.field public static final ANCHOR_CTR_ATTR:Ljava/lang/String; = "anchorCtr"

.field public static final ANCHOR_DML_ELE:Ljava/lang/String; = "anchor"

.field public static final ANCHOR_X:Ljava/lang/String; = "anchorx"

.field public static final ANCHOR_Y:Ljava/lang/String; = "anchory"

.field public static final ARC:Ljava/lang/String; = "arc"

.field public static final ARC_SIZE:Ljava/lang/String; = "arcsize"

.field public static final ARC_TO:Ljava/lang/String; = "arcTo"

.field public static final ASCII_ATTR:Ljava/lang/String; = "ascii"

.field public static final ASCII_THEME_ATTR:Ljava/lang/String; = "asciiTheme"

.field public static final ATTR_CS:Ljava/lang/String; = "cs"

.field public static final ATTR_DM:Ljava/lang/String; = "dm"

.field public static final ATTR_ID:Ljava/lang/String; = "id"

.field public static final ATTR_LO:Ljava/lang/String; = "lo"

.field public static final ATTR_QS:Ljava/lang/String; = "qs"

.field public static final ATTR_REL_ID:Ljava/lang/String; = "relId"

.field public static final AUTO_COLOR:Ljava/lang/String; = "auto"

.field public static final BACKGROUND_ELE:Ljava/lang/String; = "background"

.field public static final BAND_1H_ELE:Ljava/lang/String; = "band1H"

.field public static final BAND_1V_ELE:Ljava/lang/String; = "band1V"

.field public static final BAND_2H_ELE:Ljava/lang/String; = "band2H"

.field public static final BAND_2V_ELE:Ljava/lang/String; = "band2V"

.field public static final BAND_COL_ELE:Ljava/lang/String; = "bandCol"

.field public static final BAND_ROW_ELE:Ljava/lang/String; = "bandRow"

.field public static final BAR_BDR_ELE:Ljava/lang/String; = "bar"

.field public static final BASED_ON_ELE:Ljava/lang/String; = "basedOn"

.field public static final BASELINE_ATTR:Ljava/lang/String; = "baseline"

.field public static final BEFORE_ATTR:Ljava/lang/String; = "before"

.field public static final BETWEEN_BDR_ELE:Ljava/lang/String; = "between"

.field public static final BG_ELE:Ljava/lang/String; = "bg"

.field public static final BG_FILL_STYLE_LST_ELE:Ljava/lang/String; = "bgFillStyleLst"

.field public static final BG_PR_ELE:Ljava/lang/String; = "bgPr"

.field public static final BG_REF_ELE:Ljava/lang/String; = "bgRef"

.field public static final BLIP_ELE:Ljava/lang/String; = "blip"

.field public static final BLIP_FILL_ELE:Ljava/lang/String; = "blipFill"

.field public static final BODY_ELE:Ljava/lang/String; = "body"

.field public static final BODY_PR_ELE:Ljava/lang/String; = "bodyPr"

.field public static final BODY_STYLE_ELE:Ljava/lang/String; = "bodyStyle"

.field public static final BOLD_ELE:Ljava/lang/String; = "b"

.field public static final BORDER_ELE:Ljava/lang/String; = "bdr"

.field public static final BOTH_JC:Ljava/lang/String; = "both"

.field public static final BOTTOM:Ljava/lang/String; = "bottom"

.field public static final BR_ELE:Ljava/lang/String; = "br"

.field public static final BUBBLE_SIZE_CHART:Ljava/lang/String; = "bubbleSize"

.field public static final BU_AUTO_NUM_ELE:Ljava/lang/String; = "buAutoNum"

.field public static final BU_CHAR_ELE:Ljava/lang/String; = "buChar"

.field public static final BU_COLOR_ELE:Ljava/lang/String; = "buClr"

.field public static final BU_FONT_ELE:Ljava/lang/String; = "buFont"

.field public static final BU_NONE_ELE:Ljava/lang/String; = "buNone"

.field public static final CAPS_ELE:Ljava/lang/String; = "caps"

.field public static final CAP_ATTR:Ljava/lang/String; = "cap"

.field public static final CENTER_ALGN:Ljava/lang/String; = "ctr"

.field public static final CHARTSPACE_ELE:Ljava/lang/String; = "chartSpace"

.field public static final CHART_AREA_3D_CHART_ELE:Ljava/lang/String; = "area3DChart"

.field public static final CHART_AREA_CHART_ELE:Ljava/lang/String; = "areaChart"

.field public static final CHART_BARCHART_ELE:Ljava/lang/String; = "barChart"

.field public static final CHART_BAR_3D_CHART_ELE:Ljava/lang/String; = "bar3DChart"

.field public static final CHART_BAR_DIR:Ljava/lang/String; = "barDir"

.field public static final CHART_BUBBLE_CHART_ELE:Ljava/lang/String; = "bubbleChart"

.field public static final CHART_CAT_ELE:Ljava/lang/String; = "cat"

.field public static final CHART_DOUGHNUT_ELE:Ljava/lang/String; = "doughnutChart"

.field public static final CHART_ELE:Ljava/lang/String; = "chart"

.field public static final CHART_GROUPING:Ljava/lang/String; = "grouping"

.field public static final CHART_LINECHART_ELE:Ljava/lang/String; = "lineChart"

.field public static final CHART_LINE_3D_CHART_ELE:Ljava/lang/String; = "line3DChart"

.field public static final CHART_NUMCACHE_ELE:Ljava/lang/String; = "numCache"

.field public static final CHART_NUMLET_ELE:Ljava/lang/String; = "numLit"

.field public static final CHART_NUMREF_ELE:Ljava/lang/String; = "numRef"

.field public static final CHART_OF_PIE_CHART_ELE:Ljava/lang/String; = "ofPieChart"

.field public static final CHART_ORDER_ELE:Ljava/lang/String; = "order"

.field public static final CHART_PIECHART_ELE:Ljava/lang/String; = "pieChart"

.field public static final CHART_PIE_3D_CHART_ELE:Ljava/lang/String; = "pie3DChart"

.field public static final CHART_PLOTAREA_ELE:Ljava/lang/String; = "plotArea"

.field public static final CHART_PTCNT_ELE:Ljava/lang/String; = "ptCount"

.field public static final CHART_PT_ELE:Ljava/lang/String; = "pt"

.field public static final CHART_PT_V_ELE:Ljava/lang/String; = "v"

.field public static final CHART_SCATTER_CHART_ELE:Ljava/lang/String; = "scatterChart"

.field public static final CHART_SER_ELE:Ljava/lang/String; = "ser"

.field public static final CHART_SER_F_ELE:Ljava/lang/String; = "f"

.field public static final CHART_STOCK_CHART_ELE:Ljava/lang/String; = "stockChart"

.field public static final CHART_STRCACHE_ELE:Ljava/lang/String; = "strCache"

.field public static final CHART_STRREF_ELE:Ljava/lang/String; = "strRef"

.field public static final CHART_SURFACE_3D_CHART_ELE:Ljava/lang/String; = "surface3DChart"

.field public static final CHART_SURFACE_CHART_ELE:Ljava/lang/String; = "surfaceChart"

.field public static final CHART_TX_ELE:Ljava/lang/String; = "tx"

.field public static final CHART_VAL_ELE:Ljava/lang/String; = "val"

.field public static final CHART_XVAL:Ljava/lang/String; = "xVal"

.field public static final CHART_YVAL:Ljava/lang/String; = "yVal"

.field public static final CHAR_ATTR:Ljava/lang/String; = "char"

.field public static final CHAR_SET_ATTR:Ljava/lang/String; = "charset"

.field public static final CHEXT_ELE:Ljava/lang/String; = "chExt"

.field public static final CHOFF_ELE:Ljava/lang/String; = "chOff"

.field public static final CLOSE:Ljava/lang/String; = "close"

.field public static final CLR_MAP_ELE:Ljava/lang/String; = "clrMap"

.field public static final CLR_MAP_OVR_ELE:Ljava/lang/String; = "clrMapOvr"

.field public static final CNF_STYLE:Ljava/lang/String; = "cnfStyle"

.field public static final COLOR_ELE:Ljava/lang/String; = "color"

.field public static final COLOR_SCHEME_ELE:Ljava/lang/String; = "clrScheme"

.field public static final COLS:Ljava/lang/String; = "cols"

.field public static final CONNECTOR_TYPE:Ljava/lang/String; = "connectortype"

.field public static final CONNECT_TYPE:Ljava/lang/String; = "connecttype"

.field public static final CONTENT_TYPE_ATTR:Ljava/lang/String; = "ContentType"

.field public static final CONTEXTUAL_SPAC_ELE:Ljava/lang/String; = "contextualSpacing"

.field public static final CSID_ELE:Ljava/lang/String; = "cSld"

.field public static final CUBIC_BEZ_TO:Ljava/lang/String; = "cubicBezTo"

.field public static final CURVE:Ljava/lang/String; = "curve"

.field public static final CUST_GEOM_ELE:Ljava/lang/String; = "custGeom"

.field public static final CXN_NVSPPR:Ljava/lang/String; = "nvCxnSpPr"

.field public static final CXN_SP_ELE:Ljava/lang/String; = "cxnSp"

.field public static final CX_ATTR:Ljava/lang/String; = "cx"

.field public static final CY_ATTR:Ljava/lang/String; = "cy"

.field public static final DBL_STRIKE_VAL:Ljava/lang/String; = "dblStrike"

.field public static final DEFAULT_ATTR:Ljava/lang/String; = "default"

.field public static final DEFAULT_ELE:Ljava/lang/String; = "Default"

.field public static final DEF_PPR_ELE:Ljava/lang/String; = "defPPr"

.field public static final DEF_RPR_ELE:Ljava/lang/String; = "defRPr"

.field public static final DEF_TAB_SIZE:Ljava/lang/String; = "defTabSz"

.field public static final DEF_TEXT_STYLE_ELE:Ljava/lang/String; = "defaultTextStyle"

.field public static final DIAGRAM_CNVGRPSPPR:Ljava/lang/String; = "cNvGrpSpPr"

.field public static final DIAGRAM_CNVPR:Ljava/lang/String; = "cNvPr"

.field public static final DIAGRAM_DATA_EXT:Ljava/lang/String; = "ext"

.field public static final DIAGRAM_DATA_EXT_LST:Ljava/lang/String; = "extLst"

.field public static final DIAGRAM_DATA_MODEL:Ljava/lang/String; = "dataModel"

.field public static final DIAGRAM_DATA_MODEL_EXT:Ljava/lang/String; = "dataModelExt"

.field public static final DIAGRAM_DRAWING:Ljava/lang/String; = "drawing"

.field public static final DIAGRAM_NVGRPSPPR:Ljava/lang/String; = "nvGrpSpPr"

.field public static final DIAGRAM_REL_IDS:Ljava/lang/String; = "relIds"

.field public static final DIAGRAM_SP:Ljava/lang/String; = "sp"

.field public static final DIAGRAM_SPTREE:Ljava/lang/String; = "spTree"

.field public static final DIAGRAM_SP_TXXFRM:Ljava/lang/String; = "txXfrm"

.field public static final DIRTY_ATTR:Ljava/lang/String; = "dirty"

.field public static final DISTRIBUTE_JC:Ljava/lang/String; = "distribute"

.field public static final DIST_ALGN:Ljava/lang/String; = "dist"

.field public static final DK1_ELE:Ljava/lang/String; = "dk1"

.field public static final DK2_ELE:Ljava/lang/String; = "dk2"

.field public static final DOCUMENT_ELE:Ljava/lang/String; = "document"

.field public static final DOC_DEFAULTS_ELE:Ljava/lang/String; = "docDefaults"

.field public static final DOC_GRID:Ljava/lang/String; = "docGrid"

.field public static final DOC_PART_GALLERY_ELE:Ljava/lang/String; = "docPartGallery"

.field public static final DOC_PART_OBJ_ELE:Ljava/lang/String; = "docPartObj"

.field public static final DOC_PR_DML_ELE:Ljava/lang/String; = "docPr"

.field public static final DRAWING_ELE:Ljava/lang/String; = "drawing"

.field public static final DSTRIKE_ELE:Ljava/lang/String; = "dstrike"

.field public static final EFFECT_EXTENT_DML_ELE:Ljava/lang/String; = "effectExtent"

.field public static final EFFECT_LST_ELE:Ljava/lang/String; = "effectLst"

.field public static final EMBOSS_ELE:Ljava/lang/String; = "emboss"

.field public static final END_ARROW:Ljava/lang/String; = "endarrow"

.field public static final EQUATION:Ljava/lang/String; = "eqn"

.field public static final EXTENSION_ATTR:Ljava/lang/String; = "Extension"

.field public static final EXTENT_DML_ELE:Ljava/lang/String; = "extent"

.field public static final EXT_ELE:Ljava/lang/String; = "ext"

.field public static final FALLBACK:Ljava/lang/String; = "Fallback"

.field public static final FILLED_STATUS_ATTR:Ljava/lang/String; = "filled"

.field public static final FILL_ATTR:Ljava/lang/String; = "fill"

.field public static final FILL_COLOR_ATTR:Ljava/lang/String; = "fillcolor"

.field public static final FILL_OVER_LAY_ELE:Ljava/lang/String; = "fillOverlay"

.field public static final FILL_REF_ELE:Ljava/lang/String; = "fillRef"

.field public static final FILL_STYLE_LST_ELE:Ljava/lang/String; = "fillStyleLst"

.field public static final FIRST_COL_ELE:Ljava/lang/String; = "firstCol"

.field public static final FIRST_LINE_ATTR:Ljava/lang/String; = "firstLine"

.field public static final FIRST_ROW_ELE:Ljava/lang/String; = "firstRow"

.field public static final FLD_DATA_ELE:Ljava/lang/String; = "fldData"

.field public static final FLD_ELE:Ljava/lang/String; = "fld"

.field public static final FLD_SIMPLE_ELE:Ljava/lang/String; = "fldSimple"

.field public static final FLIP_ATTR:Ljava/lang/String; = "flip"

.field public static final FLIP_H_ATTR:Ljava/lang/String; = "flipH"

.field public static final FLIP_V_ATTR:Ljava/lang/String; = "flipV"

.field public static final FMT_SCHEME_ELE:Ljava/lang/String; = "fmtScheme"

.field public static final FOLH_LINK_ELE:Ljava/lang/String; = "folHlink"

.field public static final FONT_ELE:Ljava/lang/String; = "font"

.field public static final FONT_REF_ELE:Ljava/lang/String; = "fontRef"

.field public static final FONT_SCALE_ATTR:Ljava/lang/String; = "fontScale"

.field public static final FONT_SCHEME_ELE:Ljava/lang/String; = "fontScheme"

.field public static final FOOTER_ATTR:Ljava/lang/String; = "footer"

.field public static final FOOTER_ELE:Ljava/lang/String; = "ftr"

.field public static final FOOTER_REF:Ljava/lang/String; = "footerReference"

.field public static final FORMAT_CODE:Ljava/lang/String; = "formatCode"

.field public static final FORMULAS:Ljava/lang/String; = "formulas"

.field public static final FORMULA_ATTR:Ljava/lang/String; = "fmla"

.field public static final FULL_ATTR:Ljava/lang/String; = "full"

.field public static final GRAD_FILL_ELE:Ljava/lang/String; = "gradFill"

.field public static final GRAPHIC_DATA_ELE:Ljava/lang/String; = "graphicData"

.field public static final GRAPHIC_DML_ELE:Ljava/lang/String; = "graphic"

.field public static final GRAPHIC_FRAME_ELE:Ljava/lang/String; = "graphicFrame"

.field public static final GROUP_ELE:Ljava/lang/String; = "group"

.field public static final GROUP_SP:Ljava/lang/String; = "grpSp"

.field public static final GRPSPPR_ELE:Ljava/lang/String; = "grpSpPr"

.field public static final GS_ELE:Ljava/lang/String; = "gs"

.field public static final GS_LST_ELE:Ljava/lang/String; = "gsLst"

.field public static final GUIDE_VALUE_ELE:Ljava/lang/String; = "gd"

.field public static final HALF_ATTR:Ljava/lang/String; = "half"

.field public static final HANGING_ATTR:Ljava/lang/String; = "hanging"

.field public static final HEADER_ATTR:Ljava/lang/String; = "header"

.field public static final HEADER_ELE:Ljava/lang/String; = "hdr"

.field public static final HEADER_REF:Ljava/lang/String; = "headerReference"

.field public static final HIGHLIGHT_ELE:Ljava/lang/String; = "highlight"

.field public static final HISTORY_ATTR:Ljava/lang/String; = "history"

.field public static final HLINK_CLICK_ELE:Ljava/lang/String; = "hlinkClick"

.field public static final HLINK_ELE:Ljava/lang/String; = "hlink"

.field public static final HRULE_ATTR:Ljava/lang/String; = "hRule"

.field public static final HR_ALIGN:Ljava/lang/String; = "o:hralign"

.field public static final HR_ATTR:Ljava/lang/String; = "hR"

.field public static final HSL_CLR_ELE:Ljava/lang/String; = "hslClr"

.field public static final HYPERLINK_ELE:Ljava/lang/String; = "hyperlink"

.field public static final H_ATTR:Ljava/lang/String; = "h"

.field public static final IDX:Ljava/lang/String; = "idx"

.field public static final ID_ATTR:Ljava/lang/String; = "id"

.field public static final ID_ATTR_R:Ljava/lang/String; = "r:id"

.field public static final ILVL_ELE:Ljava/lang/String; = "ilvl"

.field public static final IMAGE:Ljava/lang/String; = "image"

.field public static final IMG_DATA:Ljava/lang/String; = "imagedata"

.field public static final IMPRINT_ELE:Ljava/lang/String; = "imprint"

.field public static final INDENTATION_ELE:Ljava/lang/String; = "ind"

.field public static final INDENT_ATTR:Ljava/lang/String; = "indent"

.field public static final INLINE_DML_ELE:Ljava/lang/String; = "inline"

.field public static final INSIDE_H:Ljava/lang/String; = "insideH"

.field public static final INSIDE_V:Ljava/lang/String; = "insideV"

.field public static final IS_LGL:Ljava/lang/String; = "isLgl"

.field public static final ITALIC_ELE:Ljava/lang/String; = "i"

.field public static final JC_ELE:Ljava/lang/String; = "jc"

.field public static final JC_VAL:Ljava/lang/String; = "justify"

.field public static final JOIN_STYLE:Ljava/lang/String; = "joinstyle"

.field public static final JUST_ALGN:Ljava/lang/String; = "just"

.field public static final KEEP_LINES_ELE:Ljava/lang/String; = "keepLines"

.field public static final KEEP_NEXT_ELE:Ljava/lang/String; = "keepNext"

.field public static final LANG_ELE:Ljava/lang/String; = "lang"

.field public static final LAST_CLR_ATTR:Ljava/lang/String; = "lastClr"

.field public static final LAST_COL_ELE:Ljava/lang/String; = "lastCol"

.field public static final LAST_RENDERED_PAGEB_REAK:Ljava/lang/String; = "lastRenderedPageBreak"

.field public static final LAST_ROW_ELE:Ljava/lang/String; = "lastRow"

.field public static final LATIN_ELE:Ljava/lang/String; = "latin"

.field public static final LEFT:Ljava/lang/String; = "left"

.field public static final LEFT_ALGN:Ljava/lang/String; = "l"

.field public static final LINE:Ljava/lang/String; = "line"

.field public static final LINEEND_LEN:Ljava/lang/String; = "len"

.field public static final LINEEND_TYPE:Ljava/lang/String; = "type"

.field public static final LINEEND_WIDTH:Ljava/lang/String; = "w"

.field public static final LINE_HEAD:Ljava/lang/String; = "headEnd"

.field public static final LINE_REF_ELE:Ljava/lang/String; = "lnRef"

.field public static final LINE_RULE_ATTR:Ljava/lang/String; = "lineRule"

.field public static final LINE_SPC_REDU_ATTR:Ljava/lang/String; = "lnSpcReduction"

.field public static final LINE_STYLE:Ljava/lang/String; = "linestyle"

.field public static final LINE_TAIL:Ljava/lang/String; = "tailEnd"

.field public static final LINE_TO:Ljava/lang/String; = "lnTo"

.field public static final LINK_ELE:Ljava/lang/String; = "link"

.field public static final LN_ELE:Ljava/lang/String; = "ln"

.field public static final LN_SPC_ELE:Ljava/lang/String; = "lnSpc"

.field public static final LN_STYLE_LST_ELE:Ljava/lang/String; = "lnStyleLst"

.field public static final LST_STYLE_ELE:Ljava/lang/String; = "lstStyle"

.field public static final LT1_ELE:Ljava/lang/String; = "lt1"

.field public static final LT2_ELE:Ljava/lang/String; = "lt2"

.field public static final LUM_MOD:Ljava/lang/String; = "lumMod"

.field public static final LUM_MOD_ELE:Ljava/lang/String; = "lumMod"

.field public static final LUM_OFF_ELE:Ljava/lang/String; = "lumOff"

.field public static final LVL1_PPR_ELE:Ljava/lang/String; = "lvl1pPr"

.field public static final LVL2_PPR_ELE:Ljava/lang/String; = "lvl2pPr"

.field public static final LVL3_PPR_ELE:Ljava/lang/String; = "lvl3pPr"

.field public static final LVL4_PPR_ELE:Ljava/lang/String; = "lvl4pPr"

.field public static final LVL5_PPR_ELE:Ljava/lang/String; = "lvl5pPr"

.field public static final LVL6_PPR_ELE:Ljava/lang/String; = "lvl6pPr"

.field public static final LVL7_PPR_ELE:Ljava/lang/String; = "lvl7pPr"

.field public static final LVL8_PPR_ELE:Ljava/lang/String; = "lvl8pPr"

.field public static final LVL9_PPR_ELE:Ljava/lang/String; = "lvl9pPr"

.field public static final LVL_ATTR:Ljava/lang/String; = "lvl"

.field public static final LVL_JC:Ljava/lang/String; = "lvlJc"

.field public static final LVL_OVERRIDE:Ljava/lang/String; = "lvlOverride"

.field public static final LVL_RESTART:Ljava/lang/String; = "lvlRestart"

.field public static final LVL_TEXT:Ljava/lang/String; = "lvlText"

.field public static final MAJOR_ELE:Ljava/lang/String; = "major"

.field public static final MAJOR_FONT_ELE:Ljava/lang/String; = "majorFont"

.field public static final MARKER_ELE:Ljava/lang/String; = "marker"

.field public static final MAR_L_ATTR:Ljava/lang/String; = "marL"

.field public static final MAR_R_ATTR:Ljava/lang/String; = "marR"

.field public static final MINOR_ELE:Ljava/lang/String; = "minor"

.field public static final MINOR_FONT_ELE:Ljava/lang/String; = "minorFont"

.field public static final MOVE_TO:Ljava/lang/String; = "moveTo"

.field public static final MSO_LAYOUT_FLOW_ALT:Ljava/lang/String; = "mso-layout-flow-alt"

.field public static final MSTR_CLR_MAP:Ljava/lang/String; = "masterClrMapping"

.field public static final MULTI_LEVEL_TYPE:Ljava/lang/String; = "multiLevelType"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final NORMAL_AUTO_FIT_ELE:Ljava/lang/String; = "normAutofit"

.field public static final NO_FILL_ELE:Ljava/lang/String; = "noFill"

.field public static final NUM:Ljava/lang/String; = "num"

.field public static final NUMBERING:Ljava/lang/String; = "numbering"

.field public static final NUMID_ELE:Ljava/lang/String; = "numId"

.field public static final NUMPR_ELE:Ljava/lang/String; = "numPr"

.field public static final NUM_FMT:Ljava/lang/String; = "numFmt"

.field public static final NUM_ID:Ljava/lang/String; = "numId"

.field public static final NUM_ID_MACAT_CLEANUP:Ljava/lang/String; = "numIdMacAtCleanup"

.field public static final NUM_PIC_BULLET:Ljava/lang/String; = "numPicBullet"

.field public static final NUM_STYLE_LINK:Ljava/lang/String; = "numStyleLink"

.field public static final NVG_FRAME_PR_DML_ELE:Ljava/lang/String; = "cNvGraphicFramePr"

.field public static final NV_DRAW_PR:Ljava/lang/String; = "cNvPr"

.field public static final NV_DRAW_SP_PR:Ljava/lang/String; = "cNvSpPr"

.field public static final NV_GRAPHIC_FRAME_PR:Ljava/lang/String; = "nvGraphicFramePr"

.field public static final NV_GRAPHIC_FRAME_PR_ELE:Ljava/lang/String; = "nvGraphicFramePr"

.field public static final NV_PIC_PR_ELE:Ljava/lang/String; = "nvPicPr"

.field public static final NV_PR:Ljava/lang/String; = "nvPr"

.field public static final NV_SP_PR:Ljava/lang/String; = "nvSpPr"

.field public static final OBJECT_DEFAULTS_ELE:Ljava/lang/String; = "objectDefaults"

.field public static final OBJECT_ELE:Ljava/lang/String; = "object"

.field public static final OFF_ELE:Ljava/lang/String; = "off"

.field public static final ORIDE_CLR_MAP_ELE:Ljava/lang/String; = "overrideClrMapping"

.field public static final ORIENT:Ljava/lang/String; = "orient"

.field public static final OTHER_STYLE_ELE:Ljava/lang/String; = "otherStyle"

.field public static final OUTER_SHADOW_ELE:Ljava/lang/String; = "outerShdw"

.field public static final OUTLINE_ELE:Ljava/lang/String; = "outline"

.field public static final OVAL:Ljava/lang/String; = "oval"

.field public static final OVERRIDE_ELE:Ljava/lang/String; = "Override"

.field public static final PAGE_BORDERS:Ljava/lang/String; = "pgBorders"

.field public static final PAGE_ELE:Ljava/lang/String; = "page"

.field public static final PART_NAME_ATTR:Ljava/lang/String; = "PartName"

.field public static final PATH:Ljava/lang/String; = "path"

.field public static final PATH_LIST:Ljava/lang/String; = "pathLst"

.field public static final PATT_FILL_ELE:Ljava/lang/String; = "pattFill"

.field public static final PBDR_ELE:Ljava/lang/String; = "pBdr"

.field public static final PG_BR_BEF_ELE:Ljava/lang/String; = "pageBreakBefore"

.field public static final PG_DISPLAY:Ljava/lang/String; = "display"

.field public static final PG_MAR:Ljava/lang/String; = "pgMar"

.field public static final PG_NUM_TYPE:Ljava/lang/String; = "pgNumType"

.field public static final PG_OFFSET_FROM:Ljava/lang/String; = "offsetFrom"

.field public static final PG_SZ:Ljava/lang/String; = "pgSz"

.field public static final PH:Ljava/lang/String; = "ph"

.field public static final PICT_ELE:Ljava/lang/String; = "pict"

.field public static final PIC_BULLET_ID:Ljava/lang/String; = "lvlPicBulletId"

.field public static final PIC_ELE:Ljava/lang/String; = "pic"

.field public static final PITCH_FAMILY_ATTR:Ljava/lang/String; = "pitchFamily"

.field public static final POLYLINE:Ljava/lang/String; = "polyline"

.field public static final POSITION_H_DML_ELE:Ljava/lang/String; = "positionH"

.field public static final POSITION_OFFSET_ELE:Ljava/lang/String; = "posOffset"

.field public static final POSITION_V_DML_ELE:Ljava/lang/String; = "positionV"

.field public static final POS_ATTR:Ljava/lang/String; = "pos"

.field public static final PPR_DEF_ELE:Ljava/lang/String; = "pPrDefault"

.field public static final PPR_ELE:Ljava/lang/String; = "pPr"

.field public static final PRESENTATION_ELE:Ljava/lang/String; = "presentation"

.field public static final PRSTCLR_ELE:Ljava/lang/String; = "prstClr"

.field public static final PRST_ATTR:Ljava/lang/String; = "prst"

.field public static final PRST_GEOM_ELE:Ljava/lang/String; = "prstGeom"

.field public static final PSTYLE_ELE:Ljava/lang/String; = "pStyle"

.field public static final PTAB_ELE:Ljava/lang/String; = "ptab"

.field public static final P_ELE:Ljava/lang/String; = "p"

.field public static final QUAD_BEZ_TO:Ljava/lang/String; = "quadBezTo"

.field public static final QUARTER_ATTR:Ljava/lang/String; = "quarter"

.field public static final RADAR_CHART:Ljava/lang/String; = "radarChart"

.field public static final RECT:Ljava/lang/String; = "rect"

.field public static final RELATIONSHIPS_ELE:Ljava/lang/String; = "Relationships"

.field public static final RELATIONSHIP_ELE:Ljava/lang/String; = "Relationship"

.field public static final REL_ID_ATTR:Ljava/lang/String; = "Id"

.field public static final REL_TARGET_ATTR:Ljava/lang/String; = "Target"

.field public static final REL_TARGET_MODE_ATTR:Ljava/lang/String; = "TargetMode"

.field public static final REL_TYPE_ATTR:Ljava/lang/String; = "Type"

.field public static final RFONTS_ELE:Ljava/lang/String; = "rFonts"

.field public static final RIGHT:Ljava/lang/String; = "right"

.field public static final RIGHT_ALGN:Ljava/lang/String; = "r"

.field public static final ROT_ATTR:Ljava/lang/String; = "rot"

.field public static final ROUND_RECT:Ljava/lang/String; = "roundrect"

.field public static final RPR_DEF_ELE:Ljava/lang/String; = "rPrDefault"

.field public static final RPR_ELE:Ljava/lang/String; = "rPr"

.field public static final RSTYLE_ELE:Ljava/lang/String; = "rStyle"

.field public static final RTL_ELE:Ljava/lang/String; = "rtl"

.field public static final RUN_ELE:Ljava/lang/String; = "r"

.field public static final SAT_MOD_ELE:Ljava/lang/String; = "satMod"

.field public static final SAT_OFF_ELE:Ljava/lang/String; = "satOff"

.field public static final SCATTER_STYLE:Ljava/lang/String; = "scatterStyle"

.field public static final SCHEME_CLR_ELE:Ljava/lang/String; = "schemeClr"

.field public static final SDT_BLOCK:I = 0x64

.field public static final SDT_CELL:I = 0x66

.field public static final SDT_CONTENT_ELE:Ljava/lang/String; = "sdtContent"

.field public static final SDT_ELE:Ljava/lang/String; = "sdt"

.field public static final SDT_PR_ELE:Ljava/lang/String; = "sdtPr"

.field public static final SDT_ROW:I = 0x67

.field public static final SDT_RUN:I = 0x65

.field public static final SECT_PR_ELE:Ljava/lang/String; = "sectPr"

.field public static final SEP:Ljava/lang/String; = "sep"

.field public static final SETTINGS_ELE:Ljava/lang/String; = "settings"

.field public static final SHADE_ELE:Ljava/lang/String; = "shade"

.field public static final SHADOW_ELE:Ljava/lang/String; = "shadow"

.field public static final SHAPE:Ljava/lang/String; = "shape"

.field public static final SHAPE_TYPE:Ljava/lang/String; = "shapetype"

.field public static final SHD_ELE:Ljava/lang/String; = "shd"

.field public static final SHOW_MASTER_SP_ATTR:Ljava/lang/String; = "showMasterSp"

.field public static final SIZE_ELE:Ljava/lang/String; = "sz"

.field public static final SLD:Ljava/lang/String; = "sld"

.field public static final SLD_ID_ELE:Ljava/lang/String; = "sldId"

.field public static final SLD_ID_LIST_ELE:Ljava/lang/String; = "sldIdLst"

.field public static final SLD_LAYOUT_ELE:Ljava/lang/String; = "sldLayout"

.field public static final SLD_MSTR_ELE:Ljava/lang/String; = "sldMaster"

.field public static final SLD_MSTR_ID_ELE:Ljava/lang/String; = "sldMasterId"

.field public static final SLD_MSTR_ID_LST_ELE:Ljava/lang/String; = "sldMasterIdLst"

.field public static final SLD_SIZE_ELE:Ljava/lang/String; = "sldSz"

.field public static final SLIDE_NUM_ELE:Ljava/lang/String; = "slidenum"

.field public static final SMALL_CAPS_ELE:Ljava/lang/String; = "smallCaps"

.field public static final SMALL_VAL:Ljava/lang/String; = "small"

.field public static final SNG_STRIKE_VAL:Ljava/lang/String; = "sngStrike"

.field public static final SOLID_FILL_ELE:Ljava/lang/String; = "solidFill"

.field public static final SPACE:Ljava/lang/String; = "space"

.field public static final SPACING_ELE:Ljava/lang/String; = "spacing"

.field public static final SPC_AFT_ELE:Ljava/lang/String; = "spcAft"

.field public static final SPC_ATTR:Ljava/lang/String; = "spc"

.field public static final SPC_BEF_ELE:Ljava/lang/String; = "spcBef"

.field public static final SPC_PCTS_ELE:Ljava/lang/String; = "spcPcts"

.field public static final SPC_PCT_ELE:Ljava/lang/String; = "spcPct"

.field public static final SPTREE_ELE:Ljava/lang/String; = "spTree"

.field public static final SP_ELE:Ljava/lang/String; = "sp"

.field public static final SP_PR_ELE:Ljava/lang/String; = "spPr"

.field public static final SRC_RECT_ELE:Ljava/lang/String; = "srcRect"

.field public static final SRGB_CLR_ELE:Ljava/lang/String; = "srgbClr"

.field public static final START:Ljava/lang/String; = "start"

.field public static final START_OVERRIDE:Ljava/lang/String; = "startOverride"

.field public static final STRIKE_ELE:Ljava/lang/String; = "strike"

.field public static final STRING_ELE:Ljava/lang/String; = "string"

.field public static final STROKE:Ljava/lang/String; = "stroke"

.field public static final STROKED_STATUS_ATTR:Ljava/lang/String; = "stroked"

.field public static final STROKE_COLOR_ATTR:Ljava/lang/String; = "strokecolor"

.field public static final STROKE_WEIGHT_ATTR:Ljava/lang/String; = "strokeweight"

.field public static final STYLES_ELE:Ljava/lang/String; = "styles"

.field public static final STYLE_ELE:Ljava/lang/String; = "style"

.field public static final STYLE_ID_ATTR:Ljava/lang/String; = "styleId"

.field public static final STYLE_LINK:Ljava/lang/String; = "styleLink"

.field public static final ST_ANGLE_ATTR:Ljava/lang/String; = "stAng"

.field public static final SW_ANGLE_ATTR:Ljava/lang/String; = "swAng"

.field public static final SX_ATTR:Ljava/lang/String; = "sx"

.field public static final SYM_ELE:Ljava/lang/String; = "sym"

.field public static final SYS_CLR_ELE:Ljava/lang/String; = "sysClr"

.field public static final SY_ATTR:Ljava/lang/String; = "sy"

.field public static final TABLE_ELE:Ljava/lang/String; = "tbl"

.field public static final TABS_ELE:Ljava/lang/String; = "tabs"

.field public static final TAB_ELE:Ljava/lang/String; = "tab"

.field public static final TBL_BACKGROUND:Ljava/lang/String; = "tblBg"

.field public static final TBL_BORDER_ELE:Ljava/lang/String; = "tblBorders"

.field public static final TBL_CELL_MAR:Ljava/lang/String; = "tblCellMar"

.field public static final TBL_GRID_COL_ELE:Ljava/lang/String; = "gridCol"

.field public static final TBL_GRID_ELE:Ljava/lang/String; = "tblGrid"

.field public static final TBL_IND_ELE:Ljava/lang/String; = "tblInd"

.field public static final TBL_LAYOUT_ELE:Ljava/lang/String; = "tblLayout"

.field public static final TBL_LOOK_ELE:Ljava/lang/String; = "tblLook"

.field public static final TBL_PPR:Ljava/lang/String; = "tblpPr"

.field public static final TBL_PR_ELE:Ljava/lang/String; = "tblPr"

.field public static final TBL_PX:Ljava/lang/String; = "tblpX"

.field public static final TBL_PY:Ljava/lang/String; = "tblpY"

.field public static final TBL_SPEC_PX:Ljava/lang/String; = "tblpXSpec"

.field public static final TBL_SPEC_PY:Ljava/lang/String; = "tblpYSpec"

.field public static final TBL_STYLES_LST_ELE:Ljava/lang/String; = "tblStyleLst"

.field public static final TBL_STYLE_ELE:Ljava/lang/String; = "tblStyle"

.field public static final TBL_STYLE_ID:Ljava/lang/String; = "tableStyleId"

.field public static final TBL_STYLE_PR:Ljava/lang/String; = "tblStylePr"

.field public static final TBL_W_ELE:Ljava/lang/String; = "tblW"

.field public static final TC_ANCHOR_ATTR:Ljava/lang/String; = "anchor"

.field public static final TC_BDR_ELE:Ljava/lang/String; = "tcBdr"

.field public static final TC_BORDERS_ELE:Ljava/lang/String; = "tcBorders"

.field public static final TC_ELE:Ljava/lang/String; = "tc"

.field public static final TC_GRID_SPAN_ELE:Ljava/lang/String; = "gridSpan"

.field public static final TC_HMERGE_ATTR:Ljava/lang/String; = "hMerge"

.field public static final TC_MAR_B_ATTR:Ljava/lang/String; = "marB"

.field public static final TC_MAR_L_ATTR:Ljava/lang/String; = "marL"

.field public static final TC_MAR_R_ATTR:Ljava/lang/String; = "marR"

.field public static final TC_MAR_T_ATTR:Ljava/lang/String; = "marT"

.field public static final TC_PR_ELE:Ljava/lang/String; = "tcPr"

.field public static final TC_ROW_SPAN_ELE:Ljava/lang/String; = "rowSpan"

.field public static final TC_STYLE_ELE:Ljava/lang/String; = "tcStyle"

.field public static final TC_TX_STYLE_ELE:Ljava/lang/String; = "tcTxStyle"

.field public static final TC_VALIGN_ELE:Ljava/lang/String; = "vAlign"

.field public static final TC_VMERGE_ELE:Ljava/lang/String; = "vMerge"

.field public static final TC_WIDTH_ELE:Ljava/lang/String; = "tcW"

.field public static final TC_lnB_ELE:Ljava/lang/String; = "lnB"

.field public static final TC_lnBlToTr_ELE:Ljava/lang/String; = "lnBlToTr"

.field public static final TC_lnL_ELE:Ljava/lang/String; = "lnL"

.field public static final TC_lnR_ELE:Ljava/lang/String; = "lnR"

.field public static final TC_lnT_ELE:Ljava/lang/String; = "lnT"

.field public static final TC_lnTlToBr_ELE:Ljava/lang/String; = "lnTlToBr"

.field public static final TEXTPATH_ELE:Ljava/lang/String; = "textpath"

.field public static final TEXT_ELE:Ljava/lang/String; = "t"

.field public static final THEME_ADJACENCY:Ljava/lang/String; = "Adjacency"

.field public static final THEME_ASPECT:Ljava/lang/String; = "Aspect"

.field public static final THEME_CLARITY:Ljava/lang/String; = "Clarity"

.field public static final THEME_COLOR_ATTR:Ljava/lang/String; = "themeColor"

.field public static final THEME_COUTURE:Ljava/lang/String; = "Couture"

.field public static final THEME_ELE:Ljava/lang/String; = "theme"

.field public static final THEME_ELEMENTS_ELE:Ljava/lang/String; = "themeElements"

.field public static final THEME_ESSENTIAL:Ljava/lang/String; = "Essential"

.field public static final THEME_EXECUTIVE:Ljava/lang/String; = "Executive"

.field public static final THEME_FILL_ATTR:Ljava/lang/String; = "themeFill"

.field public static final THEME_FILL_TINT_ATTR:Ljava/lang/String; = "themeFillTint"

.field public static final THEME_FOUNDRY:Ljava/lang/String; = "Foundry"

.field public static final THEME_HARDCOVER:Ljava/lang/String; = "Hardcover"

.field public static final THEME_OPULENT:Ljava/lang/String; = "Opulent"

.field public static final THEME_PAPER:Ljava/lang/String; = "Paper"

.field public static final THEME_PERSPECTIVE:Ljava/lang/String; = "Perspective"

.field public static final THEME_PUSH_PIN:Ljava/lang/String; = "Pushpin"

.field public static final THEME_SHADE_ATTR:Ljava/lang/String; = "themeShade"

.field public static final THEME_SUMMER:Ljava/lang/String; = "Summer"

.field public static final THEME_TINT_ATTR:Ljava/lang/String; = "themeTint"

.field public static final THEME_TREK:Ljava/lang/String; = "Trek"

.field public static final THEME_VERVE:Ljava/lang/String; = "Verve"

.field public static final TILE_ELE:Ljava/lang/String; = "tile"

.field public static final TINT_ELE:Ljava/lang/String; = "tint"

.field public static final TITLE_STYLE_ELE:Ljava/lang/String; = "titleStyle"

.field public static final TMPL:Ljava/lang/String; = "tmpl"

.field public static final TOP:Ljava/lang/String; = "top"

.field public static final TR_ELE:Ljava/lang/String; = "tr"

.field public static final TR_HEIGHT:Ljava/lang/String; = "trHeight"

.field public static final TR_PR_ELE:Ljava/lang/String; = "trPr"

.field public static final TXT_DIRECTION:Ljava/lang/String; = "textDirection"

.field public static final TXT_STYLES_ELE:Ljava/lang/String; = "txStyles"

.field public static final TXXFRM_ELE:Ljava/lang/String; = "txXfrm"

.field public static final TX_ATTR:Ljava/lang/String; = "tx"

.field public static final TX_BODY_ELE:Ljava/lang/String; = "txBody"

.field public static final TX_BOX:Ljava/lang/String; = "txBox"

.field public static final TYPES_ELE:Ljava/lang/String; = "Types"

.field public static final TYPE_ATTR:Ljava/lang/String; = "type"

.field public static final TYPE_FACE_ATTR:Ljava/lang/String; = "typeface"

.field public static final TY_ATTR:Ljava/lang/String; = "ty"

.field public static final UNDERLINE_ELE:Ljava/lang/String; = "u"

.field public static final UNDERLINE_NONE:Ljava/lang/String; = "none"

.field public static final VAL_ATTR:Ljava/lang/String; = "val"

.field public static final VANISH_ELE:Ljava/lang/String; = "vanish"

.field public static final VER_ALIGN_ELE:Ljava/lang/String; = "vertAlign"

.field public static final VIEW_3D:Ljava/lang/String; = "view3D"

.field public static final VML_HREF_ATTR:Ljava/lang/String; = "href"

.field public static final VML_ID_ATTR:Ljava/lang/String; = "id"

.field public static final VML_TEXTBOX:Ljava/lang/String; = "textbox"

.field public static final VML_TXBX_CONTENT:Ljava/lang/String; = "txbxContent"

.field public static final W10_WRAP_ELE:Ljava/lang/String; = "w10:wrap"

.field public static final WHOLE_TBL_ELE:Ljava/lang/String; = "wholeTbl"

.field public static final WRAP_ATTR:Ljava/lang/String; = "wrap"

.field public static final WRAP_SQUARE_DML_ELE:Ljava/lang/String; = "wrapSquare"

.field public static final WR_ATTR:Ljava/lang/String; = "wR"

.field public static final W_ATTR:Ljava/lang/String; = "w"

.field public static final XFRM_ELE:Ljava/lang/String; = "xfrm"

.field public static final X_ATTR:Ljava/lang/String; = "x"

.field public static final Y_ATTR:Ljava/lang/String; = "y"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
