.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Size2DHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;
.source "Size2DHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Size2DHandler$ISizeConsumer;
    }
.end annotation


# instance fields
.field consumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Size2DHandler$ISizeConsumer;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Size2DHandler$ISizeConsumer;)V
    .locals 1
    .param p1, "eleName"    # Ljava/lang/String;
    .param p2, "sConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Size2DHandler$ISizeConsumer;

    .prologue
    .line 19
    const/16 v0, 0x1f

    invoke-direct {p0, v0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;-><init>(ILjava/lang/String;)V

    .line 20
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Size2DHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Size2DHandler$ISizeConsumer;

    .line 21
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 7
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 26
    const-string/jumbo v6, "cx"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Size2DHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 27
    .local v0, "cxVal":Ljava/lang/String;
    const-string/jumbo v6, "cy"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Size2DHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 32
    .local v1, "cyVal":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 33
    .local v2, "x":J
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 35
    .local v4, "y":J
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Size2DHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Size2DHandler$ISizeConsumer;

    if-eqz v6, :cond_0

    .line 36
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Size2DHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Size2DHandler$ISizeConsumer;

    invoke-interface {v6, v2, v3, v4, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Size2DHandler$ISizeConsumer;->setSize(JJ)V

    .line 38
    :cond_0
    return-void
.end method
