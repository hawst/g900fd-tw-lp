.class public Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;
.super Ljava/lang/Object;
.source "XSLFTextShape.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BodyProperty"
.end annotation


# instance fields
.field public fontScale:I

.field public vAlign:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 299
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;->fontScale:I

    return-void
.end method


# virtual methods
.method public getvAlign()Ljava/lang/String;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;->vAlign:Ljava/lang/String;

    return-object v0
.end method

.method public setvAlign(Ljava/lang/String;)V
    .locals 0
    .param p1, "vAlign"    # Ljava/lang/String;

    .prologue
    .line 307
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;->vAlign:Ljava/lang/String;

    .line 308
    return-void
.end method
