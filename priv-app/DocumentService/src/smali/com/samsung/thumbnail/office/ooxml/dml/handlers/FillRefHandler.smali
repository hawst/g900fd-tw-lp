.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler;
.super Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;
.source "FillRefHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler$ColorConsumer;
    }
.end annotation


# instance fields
.field shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 6
    .param p1, "consumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;
    .param p2, "shInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 23
    const/16 v1, 0x1f

    const-string/jumbo v2, "fillRef"

    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler$ColorConsumer;

    invoke-direct {v3, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler$ColorConsumer;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;)V

    const/4 v5, 0x0

    move-object v0, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 25
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 26
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler;->colorConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler$ColorConsumer;

    .line 50
    .local v0, "clrConsumer":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler$ColorConsumer;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    iget-boolean v1, v1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isShapeNoFill:Z

    if-nez v1, :cond_1

    .line 51
    iget-object v1, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler$ColorConsumer;->fillChConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;

    iget-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler$ColorConsumer;->fillProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-interface {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;->consumeFill(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;

    if-eqz v1, :cond_0

    .line 53
    iget-object v1, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler$ColorConsumer;->fillChConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;

    iget-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler$ColorConsumer;->fillProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-interface {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;->consumeFill(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V

    goto :goto_0
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 31
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 33
    const-string/jumbo v1, "idx"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "idx":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 39
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    if-eqz v1, :cond_0

    .line 41
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->setFillRefId(Ljava/lang/String;)V

    .line 44
    :cond_0
    return-void
.end method
