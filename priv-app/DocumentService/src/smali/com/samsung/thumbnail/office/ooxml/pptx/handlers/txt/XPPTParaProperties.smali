.class public Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;
.super Ljava/lang/Object;
.source "XPPTParaProperties.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final BULLET_TYPE_BLIP:I = 0x3

.field public static final BULLET_TYPE_CHAR:I = 0x2

.field public static final BULLET_TYPE_NONE:I = 0x0

.field public static final BULLET_TYPE_NUM:I = 0x1

.field public static final alphaLcParenBoth:S = 0x8s

.field public static final alphaLcParenR:S = 0x9s

.field public static final alphaLcPeriod:S = 0x0s

.field public static final alphaUcParenR:S = 0xbs

.field public static final alphaUcPeriod:S = 0x1s

.field public static final arabicParenBoth:S = 0xcs

.field public static final arabicPeriod:S = 0x3s

.field public static final arabicPlain:S = 0xds

.field public static final romanLcParenBoth:S = 0x4s

.field public static final romanLcParenR:S = 0x5s

.field public static final romanLcPeriod:S = 0x6s

.field public static final romanUcParenBoth:S = 0xes

.field public static final romanUcParenR:S = 0xfs

.field public static final romanUcPeriod:S = 0x7s


# instance fields
.field private charProperty:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

.field private paraProperty:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 78
    const/4 v2, 0x0

    .line 80
    .local v2, "paragraphProperties":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :goto_0
    return-object v2

    .line 89
    :catch_0
    move-exception v1

    .line 90
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/CloneNotSupportedException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getCharProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->charProperty:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    return-object v0
.end method

.method public getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->paraProperty:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    return-object v0
.end method

.method public setCharProperty(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V
    .locals 0
    .param p1, "charProperty"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->charProperty:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .line 63
    return-void
.end method

.method public setParaProperty(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V
    .locals 0
    .param p1, "paraProperty"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->paraProperty:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    .line 71
    return-void
.end method
