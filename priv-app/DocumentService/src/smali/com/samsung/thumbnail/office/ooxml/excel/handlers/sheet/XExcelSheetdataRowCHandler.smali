.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetdataRowCHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelSheetdataRowCHandler.java"


# instance fields
.field columindex:Ljava/lang/String;

.field s:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 27
    const-string/jumbo v1, "c"

    invoke-direct {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 28
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetdataRowCVHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetdataRowCVHandler;-><init>()V

    .line 29
    .local v0, "sheetrowCvalHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetdataRowCVHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetdataRowCHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "v"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 37
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 38
    const-string/jumbo v2, "r"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetdataRowCHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetdataRowCHandler;->columindex:Ljava/lang/String;

    .line 40
    const-string/jumbo v2, "t"

    invoke-interface {p3, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 41
    .local v0, "t":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v1

    .line 43
    .local v1, "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getBlankrow()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getBlankrow()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 46
    iget v2, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countv:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countv:I

    .line 47
    iget v2, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countc:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countc:I

    .line 48
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    const-string/jumbo v3, "false"

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->setBlankrow(Ljava/lang/String;)V

    .line 50
    :cond_0
    if-eqz v0, :cond_1

    .line 51
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    const-string/jumbo v3, "string"

    iget v4, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countc:I

    invoke-virtual {v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->addExWorkSheetTextDataType(Ljava/lang/String;I)V

    .line 58
    :goto_0
    iget v2, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countc:I

    iget v3, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->count:I

    sub-int/2addr v2, v3

    const/16 v3, 0x10

    if-lt v2, v3, :cond_2

    .line 59
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->popHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    .line 71
    :goto_1
    return-void

    .line 54
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    const-string/jumbo v3, "integer"

    iget v4, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countc:I

    invoke-virtual {v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->addExWorkSheetTextDataType(Ljava/lang/String;I)V

    goto :goto_0

    .line 63
    :cond_2
    const-string/jumbo v2, "s"

    invoke-interface {p3, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetdataRowCHandler;->s:Ljava/lang/String;

    .line 64
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetdataRowCHandler;->s:Ljava/lang/String;

    iget v4, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countc:I

    invoke-virtual {v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->addExWorkSheetStyleid(Ljava/lang/String;I)V

    .line 65
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    const-string/jumbo v3, ""

    iget v4, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countv:I

    invoke-virtual {v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->addExWorkSheetVTextid(Ljava/lang/String;I)V

    .line 66
    iget v2, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countv:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countv:I

    .line 67
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetdataRowCHandler;->columindex:Ljava/lang/String;

    iget v4, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countc:I

    invoke-virtual {v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->addExWorkSheetColumnid(Ljava/lang/String;I)V

    .line 69
    iget v2, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countc:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countc:I

    goto :goto_1
.end method
