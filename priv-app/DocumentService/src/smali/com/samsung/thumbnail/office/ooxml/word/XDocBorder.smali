.class public Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
.super Ljava/lang/Object;
.source "XDocBorder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$1;,
        Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;,
        Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    }
.end annotation


# instance fields
.field private color:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

.field private size:I

.field private space:I

.field private style:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181
    return-void
.end method


# virtual methods
.method public getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->style:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    return-object v0
.end method

.method public getBorderStyle(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;)Ljava/lang/String;
    .locals 3
    .param p1, "style"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .prologue
    .line 93
    const-string/jumbo v0, ""

    .line 94
    .local v0, "val":Ljava/lang/String;
    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$XDocBorder$EBorderStyle:[I

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 146
    const-string/jumbo v0, "solid"

    .line 149
    :goto_0
    return-object v0

    .line 96
    :pswitch_0
    const-string/jumbo v0, "none"

    .line 97
    goto :goto_0

    .line 99
    :pswitch_1
    const-string/jumbo v0, "none"

    .line 100
    goto :goto_0

    .line 102
    :pswitch_2
    const-string/jumbo v0, "solid"

    .line 103
    goto :goto_0

    .line 121
    :pswitch_3
    const-string/jumbo v0, "double"

    .line 122
    goto :goto_0

    .line 124
    :pswitch_4
    const-string/jumbo v0, "ridge"

    .line 125
    goto :goto_0

    .line 127
    :pswitch_5
    const-string/jumbo v0, "groove"

    .line 128
    goto :goto_0

    .line 131
    :pswitch_6
    const-string/jumbo v0, "dotted"

    .line 132
    goto :goto_0

    .line 137
    :pswitch_7
    const-string/jumbo v0, "dashed"

    .line 138
    goto :goto_0

    .line 140
    :pswitch_8
    const-string/jumbo v0, "outset"

    .line 141
    goto :goto_0

    .line 143
    :pswitch_9
    const-string/jumbo v0, "inset"

    .line 144
    goto :goto_0

    .line 94
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->color:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    return-object v0
.end method

.method public getPathEffect(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;)[F
    .locals 4
    .param p1, "style"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .prologue
    const/4 v3, 0x2

    .line 54
    const/4 v0, 0x0

    .line 55
    .local v0, "flArray":[F
    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$XDocBorder$EBorderStyle:[I

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 86
    new-array v0, v3, [F

    .end local v0    # "flArray":[F
    fill-array-data v0, :array_0

    .line 89
    .restart local v0    # "flArray":[F
    :goto_0
    return-object v0

    .line 60
    :pswitch_0
    new-array v0, v3, [F

    .end local v0    # "flArray":[F
    fill-array-data v0, :array_1

    .line 61
    .restart local v0    # "flArray":[F
    goto :goto_0

    .line 64
    :pswitch_1
    new-array v0, v3, [F

    .end local v0    # "flArray":[F
    fill-array-data v0, :array_2

    .line 65
    .restart local v0    # "flArray":[F
    goto :goto_0

    .line 68
    :pswitch_2
    new-array v0, v3, [F

    .end local v0    # "flArray":[F
    fill-array-data v0, :array_3

    .line 69
    .restart local v0    # "flArray":[F
    goto :goto_0

    .line 72
    :pswitch_3
    new-array v0, v3, [F

    .end local v0    # "flArray":[F
    fill-array-data v0, :array_4

    .line 73
    .restart local v0    # "flArray":[F
    goto :goto_0

    .line 77
    :pswitch_4
    const/4 v1, 0x4

    new-array v0, v1, [F

    .end local v0    # "flArray":[F
    fill-array-data v0, :array_5

    .line 78
    .restart local v0    # "flArray":[F
    goto :goto_0

    .line 82
    :pswitch_5
    const/4 v1, 0x6

    new-array v0, v1, [F

    .end local v0    # "flArray":[F
    fill-array-data v0, :array_6

    .line 83
    .restart local v0    # "flArray":[F
    goto :goto_0

    .line 55
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 86
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 60
    :array_1
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 64
    :array_2
    .array-data 4
        0x40000000    # 2.0f
        0x40000000    # 2.0f
    .end array-data

    .line 68
    :array_3
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data

    .line 72
    :array_4
    .array-data 4
        0x41200000    # 10.0f
        0x41200000    # 10.0f
    .end array-data

    .line 77
    :array_5
    .array-data 4
        0x41200000    # 10.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data

    .line 82
    :array_6
    .array-data 4
        0x41200000    # 10.0f
        0x40800000    # 4.0f
        0x40800000    # 4.0f
        0x40800000    # 4.0f
        0x40800000    # 4.0f
        0x40800000    # 4.0f
    .end array-data
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->size:I

    return v0
.end method

.method public getSpace()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->space:I

    return v0
.end method

.method public getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->style:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    return-object v0
.end method

.method public setBorderColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V
    .locals 0
    .param p1, "color"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .prologue
    .line 16
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->color:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .line 17
    return-void
.end method

.method public setBorderSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 28
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->size:I

    .line 29
    return-void
.end method

.method public setBorderSpace(I)V
    .locals 0
    .param p1, "space"    # I

    .prologue
    .line 32
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->space:I

    .line 33
    return-void
.end method

.method public setBorderStyle(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;)V
    .locals 0
    .param p1, "style"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->style:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 21
    return-void
.end method
