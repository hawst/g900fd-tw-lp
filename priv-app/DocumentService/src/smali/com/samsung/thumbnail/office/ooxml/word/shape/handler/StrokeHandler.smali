.class public Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "StrokeHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler$IStrokeConsumer;
    }
.end annotation


# instance fields
.field private strokeConsumer:Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler$IStrokeConsumer;

.field private strokeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler$IStrokeConsumer;)V
    .locals 2
    .param p1, "strokeConsumer"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler$IStrokeConsumer;

    .prologue
    .line 26
    const/16 v0, 0x28

    const-string/jumbo v1, "stroke"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 27
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler;->strokeConsumer:Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler$IStrokeConsumer;

    .line 28
    return-void
.end method

.method private init()V
    .locals 0

    .prologue
    .line 32
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 37
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 38
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler;->strokeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;

    .line 39
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler;->init()V

    .line 41
    const-string/jumbo v1, "linestyle"

    invoke-virtual {p0, p3, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, "style":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 43
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler;->strokeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;

    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->setStrokeStyle(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;)V

    .line 46
    :cond_0
    const-string/jumbo v1, "endarrow"

    invoke-virtual {p0, p3, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 47
    if-eqz v0, :cond_1

    .line 48
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler;->strokeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;

    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->setStrokeEndType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;)V

    .line 51
    :cond_1
    const-string/jumbo v1, "joinstyle"

    invoke-virtual {p0, p3, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 52
    if-eqz v0, :cond_2

    .line 53
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler;->strokeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;

    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->setStrokeJoinStyle(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;)V

    .line 56
    :cond_2
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler;->strokeConsumer:Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler$IStrokeConsumer;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler;->strokeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;

    invoke-interface {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler$IStrokeConsumer;->setStroke(Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;)V

    .line 57
    return-void
.end method
