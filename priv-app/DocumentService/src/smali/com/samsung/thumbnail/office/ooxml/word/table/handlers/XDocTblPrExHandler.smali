.class public Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XDocTblPrExHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblCellMargHandler$ITblCellMargin;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler$XTblIndentation;,
        Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler$XTblJustify;,
        Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler$Shade;
    }
.end annotation


# instance fields
.field protected tableProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "eleName"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 34
    return-void
.end method


# virtual methods
.method protected init()V
    .locals 14

    .prologue
    .line 37
    const/4 v13, 0x5

    new-array v8, v13, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 39
    .local v8, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblBorderHandler;

    invoke-direct {v11, p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblBorderHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;)V

    .line 40
    .local v11, "tblBorderHandler":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblBorderHandler;
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v13, "tblBorders"

    invoke-direct {v12, v13, v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 42
    .local v12, "tblBorderSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v13, 0x0

    aput-object v12, v8, v13

    .line 44
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblCellMargHandler;

    invoke-direct {v0, p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblCellMargHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblCellMargHandler$ITblCellMargin;)V

    .line 46
    .local v0, "cellMargHandler":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblCellMargHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v13, "tblCellMar"

    invoke-direct {v1, v13, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 48
    .local v1, "cellMargSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v13, 0x1

    aput-object v1, v8, v13

    .line 50
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocShadeHandler;

    new-instance v13, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler$Shade;

    invoke-direct {v13, p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler$Shade;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;)V

    invoke-direct {v9, v13}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocShadeHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 51
    .local v9, "shadeHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocShadeHandler;
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v13, "shd"

    invoke-direct {v10, v13, v9}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 53
    .local v10, "shadeSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v13, 0x2

    aput-object v10, v8, v13

    .line 55
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler$XTblJustify;

    invoke-direct {v6, p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler$XTblJustify;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;)V

    .line 56
    .local v6, "justify":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler$XTblJustify;
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;

    const-string/jumbo v13, "jc"

    invoke-direct {v7, v13, v6}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 58
    .local v7, "justifyHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v13, "jc"

    invoke-direct {v5, v13, v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 60
    .local v5, "jcSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v13, 0x3

    aput-object v5, v8, v13

    .line 62
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler$XTblIndentation;

    invoke-direct {v3, p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler$XTblIndentation;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;)V

    .line 63
    .local v3, "indent":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler$XTblIndentation;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;

    const-string/jumbo v13, "tblInd"

    invoke-direct {v4, v13, v3}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler$IXTblWOberver;)V

    .line 65
    .local v4, "indentationHandler":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v13, "tblInd"

    invoke-direct {v2, v13, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 67
    .local v2, "indSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v13, 0x4

    aput-object v2, v8, v13

    .line 69
    iput-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 70
    return-void
.end method

.method public setBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V
    .locals 1
    .param p1, "borderType"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    .param p2, "border"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;->tableProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;->tableProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->setBorders(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    .line 131
    :cond_0
    return-void
.end method

.method public setTblCellMargin(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;)V
    .locals 1
    .param p1, "tblCellMargin"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;

    .prologue
    .line 135
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;->tableProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->setCellMargin(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties$TblCellMargin;)V

    .line 136
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 75
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 76
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;->init()V

    .line 77
    return-void
.end method
