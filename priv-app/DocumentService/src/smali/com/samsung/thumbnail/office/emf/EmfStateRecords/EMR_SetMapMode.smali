.class public Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;
.super Ljava/lang/Object;
.source "EMR_SetMapMode.java"


# instance fields
.field private RecordSize:I

.field private RecordType:I

.field fileContent:[B

.field private mapMode:I

.field private offset:I


# direct methods
.method public constructor <init>([BI)V
    .locals 0
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->fileContent:[B

    .line 19
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->offset:I

    .line 20
    return-void
.end method


# virtual methods
.method public getMapMode()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->mapMode:I

    return v0
.end method

.method public getRecordSize()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->RecordSize:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->RecordType:I

    return v0
.end method

.method public readEmfSetMapMode()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x0

    .line 47
    const/4 v2, 0x0

    .line 49
    .local v2, "i":I
    const/4 v5, 0x4

    new-array v3, v5, [B

    .line 51
    .local v3, "intConvert":[B
    const/4 v2, 0x0

    :goto_0
    if-gt v2, v9, :cond_0

    .line 52
    rsub-int/lit8 v5, v2, 0x3

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->fileContent:[B

    iget v7, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->offset:I

    add-int/2addr v7, v2

    aget-byte v6, v6, v7

    aput-byte v6, v3, v5

    .line 51
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 53
    :cond_0
    invoke-static {v3, v8}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 54
    .local v1, "Type":I
    iget v5, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->offset:I

    add-int/2addr v5, v2

    iput v5, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->offset:I

    .line 55
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->setRecordType(I)V

    .line 57
    const/4 v2, 0x0

    :goto_1
    if-gt v2, v9, :cond_1

    .line 58
    rsub-int/lit8 v5, v2, 0x3

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->fileContent:[B

    iget v7, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->offset:I

    add-int/2addr v7, v2

    aget-byte v6, v6, v7

    aput-byte v6, v3, v5

    .line 57
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 59
    :cond_1
    invoke-static {v3, v8}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 60
    .local v0, "Size":I
    iget v5, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->offset:I

    add-int/2addr v5, v2

    iput v5, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->offset:I

    .line 61
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->setRecordSize(I)V

    .line 63
    const/4 v2, 0x0

    :goto_2
    if-gt v2, v9, :cond_2

    .line 64
    rsub-int/lit8 v5, v2, 0x3

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->fileContent:[B

    iget v7, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->offset:I

    add-int/2addr v7, v2

    aget-byte v6, v6, v7

    aput-byte v6, v3, v5

    .line 63
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 65
    :cond_2
    invoke-static {v3, v8}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v4

    .line 66
    .local v4, "mapMde":I
    iget v5, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->offset:I

    add-int/2addr v5, v2

    iput v5, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->offset:I

    .line 67
    invoke-virtual {p0, v4}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->setMapMode(I)V

    .line 68
    return-void
.end method

.method public setMapMode(I)V
    .locals 0
    .param p1, "mapMde"    # I

    .prologue
    .line 39
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->mapMode:I

    .line 40
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "RecrdSize"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->RecordSize:I

    .line 32
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "RecrdType"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetMapMode;->RecordType:I

    .line 24
    return-void
.end method
