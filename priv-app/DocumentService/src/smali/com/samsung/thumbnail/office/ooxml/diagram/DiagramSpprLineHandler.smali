.class public Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprLineHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DiagramSpprLineHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprLineHandler$ILineFillHandler;
    }
.end annotation


# instance fields
.field private fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

.field private lineFill:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprLineHandler$ILineFillHandler;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprLineHandler$ILineFillHandler;Ljava/lang/String;)V
    .locals 4
    .param p1, "lineFill"    # Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprLineHandler$ILineFillHandler;
    .param p2, "elementName"    # Ljava/lang/String;

    .prologue
    .line 23
    const/16 v3, 0x1f

    invoke-direct {p0, v3, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 25
    const/4 v3, 0x1

    new-array v0, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 27
    .local v0, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprLineHandler;->lineFill:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprLineHandler$ILineFillHandler;

    .line 28
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprSolidFillHandler;

    invoke-direct {v1, p0}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprSolidFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;)V

    .line 30
    .local v1, "solidFillHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprSolidFillHandler;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v3, "solidFill"

    invoke-direct {v2, v3, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 32
    .local v2, "solidFillSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v3, 0x0

    aput-object v2, v0, v3

    .line 34
    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprLineHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 35
    return-void
.end method


# virtual methods
.method public consumeFill(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 0
    .param p1, "fillProperties"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprLineHandler;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .line 47
    return-void
.end method

.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprLineHandler;->lineFill:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprLineHandler$ILineFillHandler;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprLineHandler;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-interface {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprLineHandler$ILineFillHandler;->setLineFillProperty(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V

    .line 42
    return-void
.end method
