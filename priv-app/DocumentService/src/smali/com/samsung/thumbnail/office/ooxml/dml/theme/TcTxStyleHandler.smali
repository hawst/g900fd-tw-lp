.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler;
.super Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;
.source "TcTxStyleHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ITcTxStylesFntClr;,
        Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ColorConsumer;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ITcTxStylesFntClr;)V
    .locals 2
    .param p1, "tctxConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ITcTxStylesFntClr;

    .prologue
    .line 18
    const-string/jumbo v0, "tcTxStyle"

    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ColorConsumer;

    invoke-direct {v1, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ColorConsumer;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ITcTxStylesFntClr;)V

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;)V

    .line 22
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler;->colorConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ColorConsumer;

    .line 26
    .local v0, "clrConsumer":Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ColorConsumer;
    iget-object v1, v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ColorConsumer;->fillConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ITcTxStylesFntClr;

    iget-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ColorConsumer;->color:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ITcTxStylesFntClr;->handleFontColor(Ljava/lang/String;)V

    .line 27
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 28
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 6
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 33
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 35
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler;->colorConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ColorConsumer;

    .line 37
    .local v1, "clrConsumer":Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ColorConsumer;
    const-string/jumbo v4, "b"

    invoke-virtual {p0, p3, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 38
    .local v3, "val":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 39
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;

    invoke-direct {v0, v3}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;-><init>(Ljava/lang/String;)V

    .line 40
    .local v0, "boldValue":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;
    iget-object v4, v1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ColorConsumer;->fillConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ITcTxStylesFntClr;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;->getValue()Z

    move-result v5

    invoke-interface {v4, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ITcTxStylesFntClr;->handleBold(Z)V

    .line 43
    .end local v0    # "boldValue":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;
    :cond_0
    const-string/jumbo v4, "i"

    invoke-virtual {p0, p3, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 44
    if-eqz v3, :cond_1

    .line 45
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;

    invoke-direct {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;-><init>(Ljava/lang/String;)V

    .line 46
    .local v2, "italicValue":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;
    iget-object v4, v1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ColorConsumer;->fillConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ITcTxStylesFntClr;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;->getValue()Z

    move-result v5

    invoke-interface {v4, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ITcTxStylesFntClr;->handleItalic(Z)V

    .line 48
    .end local v2    # "italicValue":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;
    :cond_1
    return-void
.end method
