.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocIndentationHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLValueHandler;
.source "XDocIndentationHandler.java"


# instance fields
.field private propery:Lcom/samsung/thumbnail/office/ooxml/word/properties/IXDocPropery;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/properties/IXDocPropery;)V
    .locals 1
    .param p1, "propery"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/IXDocPropery;

    .prologue
    .line 18
    const-string/jumbo v0, "ind"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLValueHandler;-><init>(Ljava/lang/String;)V

    .line 19
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocIndentationHandler;->propery:Lcom/samsung/thumbnail/office/ooxml/word/properties/IXDocPropery;

    .line 20
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 7
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 25
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;-><init>()V

    .line 26
    .local v2, "indentation":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;
    const/4 v0, 0x0

    .local v0, "firstLine":I
    const/4 v1, 0x0

    .local v1, "hanging":I
    const/4 v3, 0x0

    .local v3, "left":I
    const/4 v4, 0x0

    .line 28
    .local v4, "right":I
    const-string/jumbo v6, "left"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocIndentationHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v5

    .line 29
    .local v5, "value":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 30
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 32
    :cond_0
    const-string/jumbo v6, "right"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocIndentationHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v5

    .line 33
    if-eqz v5, :cond_1

    .line 34
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 36
    :cond_1
    const-string/jumbo v6, "hanging"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocIndentationHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v5

    .line 37
    if-eqz v5, :cond_2

    .line 38
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 41
    :cond_2
    const-string/jumbo v6, "firstLine"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocIndentationHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v5

    .line 42
    if-eqz v5, :cond_3

    .line 43
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 46
    :cond_3
    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->setIndentation(IIII)V

    .line 48
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocIndentationHandler;->propery:Lcom/samsung/thumbnail/office/ooxml/word/properties/IXDocPropery;

    invoke-interface {v6, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/IXDocPropery;->consumePropery(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty;)V

    .line 49
    return-void
.end method
