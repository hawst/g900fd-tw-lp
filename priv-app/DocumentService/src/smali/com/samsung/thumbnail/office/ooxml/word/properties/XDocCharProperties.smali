.class public Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
.super Ljava/lang/Object;
.source "XDocCharProperties.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties$EOnOff;
    }
.end annotation


# instance fields
.field private asciiThemeFontName:Ljava/lang/String;

.field private baseline:I

.field private bold:Z

.field private border:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

.field private caps:Z

.field private dStrike:Z

.field private emboss:Z

.field private engrave:Z

.field private fontColor:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

.field private fontName:Ljava/lang/String;

.field private fontSize:I

.field private hLinkId:Ljava/lang/String;

.field private hidden:Z

.field private highlightColor:Ljava/lang/String;

.field private isBaseline:Z

.field private italic:Z

.field private lumMod:Ljava/lang/String;

.field private outline:Z

.field private rtl:Z

.field private shadeProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

.field private shadow:Z

.field private shadowColor:Ljava/lang/String;

.field private smallCaps:Z

.field private spacing:F

.field private strike:Z

.field private styleId:Ljava/lang/String;

.field private underline:Z

.field private underlineProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

.field private underlineStyle:Ljava/lang/String;

.field private vertAlign:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontSize:I

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->isBaseline:Z

    .line 54
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V
    .locals 1
    .param p1, "charProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontSize:I

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->isBaseline:Z

    .line 45
    if-nez p1, :cond_0

    .line 50
    :goto_0
    return-void

    .line 48
    :cond_0
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->styleId:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->styleId:Ljava/lang/String;

    .line 49
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->assign(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V

    goto :goto_0
.end method


# virtual methods
.method public assign(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V
    .locals 1
    .param p1, "charProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .prologue
    .line 402
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->bold:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->bold:Z

    .line 403
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->italic:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->italic:Z

    .line 404
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->underline:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->underline:Z

    .line 405
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->caps:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->caps:Z

    .line 406
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->strike:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->strike:Z

    .line 407
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->dStrike:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->dStrike:Z

    .line 408
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->emboss:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->emboss:Z

    .line 409
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->engrave:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->engrave:Z

    .line 410
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontColor:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontColor:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .line 411
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->asciiThemeFontName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->asciiThemeFontName:Ljava/lang/String;

    .line 412
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontName:Ljava/lang/String;

    .line 413
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontSize:I

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontSize:I

    .line 414
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->highlightColor:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->highlightColor:Ljava/lang/String;

    .line 415
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->outline:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->outline:Z

    .line 416
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->rtl:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->rtl:Z

    .line 417
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->shadow:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->shadow:Z

    .line 418
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->spacing:F

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->spacing:F

    .line 419
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->underlineStyle:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->underlineStyle:Ljava/lang/String;

    .line 420
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->hidden:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->hidden:Z

    .line 421
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->smallCaps:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->smallCaps:Z

    .line 422
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->baseline:I

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->baseline:I

    .line 423
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->shadeProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->shadeProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    .line 424
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->underlineProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->underlineProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    .line 425
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->hLinkId:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->hLinkId:Ljava/lang/String;

    .line 426
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->vertAlign:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->vertAlign:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    .line 427
    return-void
.end method

.method public getAsciiThemeFontName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->asciiThemeFontName:Ljava/lang/String;

    return-object v0
.end method

.method public getBaseline()I
    .locals 1

    .prologue
    .line 290
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->baseline:I

    return v0
.end method

.method public getBold()Z
    .locals 1

    .prologue
    .line 196
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->bold:Z

    return v0
.end method

.method public getBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->border:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    return-object v0
.end method

.method public getCaps()Z
    .locals 1

    .prologue
    .line 217
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->caps:Z

    return v0
.end method

.method public getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontColor:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    return-object v0
.end method

.method public getDStrike()Z
    .locals 1

    .prologue
    .line 237
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->dStrike:Z

    return v0
.end method

.method public getEmboss()Z
    .locals 1

    .prologue
    .line 229
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->emboss:Z

    return v0
.end method

.method public getEngrave()Z
    .locals 1

    .prologue
    .line 253
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->engrave:Z

    return v0
.end method

.method public getFontColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontColor:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    return-object v0
.end method

.method public getFontName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontName:Ljava/lang/String;

    return-object v0
.end method

.method public getFontSize()I
    .locals 1

    .prologue
    .line 286
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontSize:I

    return v0
.end method

.method public getHLink()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->hLinkId:Ljava/lang/String;

    return-object v0
.end method

.method public getHTMLStyleFromCharProp(Ljava/lang/StringBuffer;)V
    .locals 0
    .param p1, "style"    # Ljava/lang/StringBuffer;

    .prologue
    .line 315
    return-void
.end method

.method public getHighlightColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->highlightColor:Ljava/lang/String;

    return-object v0
.end method

.method public getIsBaseline()Z
    .locals 1

    .prologue
    .line 294
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->isBaseline:Z

    return v0
.end method

.method public getItalic()Z
    .locals 1

    .prologue
    .line 200
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->italic:Z

    return v0
.end method

.method public getLumMod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->lumMod:Ljava/lang/String;

    return-object v0
.end method

.method public getOutline()Z
    .locals 1

    .prologue
    .line 241
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->outline:Z

    return v0
.end method

.method public getRtl()Z
    .locals 1

    .prologue
    .line 249
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->rtl:Z

    return v0
.end method

.method public getShadeColor()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->shadeProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    return-object v0
.end method

.method public getShadow()Z
    .locals 1

    .prologue
    .line 245
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->shadow:Z

    return v0
.end method

.method public getShadowColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->shadowColor:Ljava/lang/String;

    return-object v0
.end method

.method public getSmallCaps()Z
    .locals 1

    .prologue
    .line 225
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->smallCaps:Z

    return v0
.end method

.method public getSpacing()F
    .locals 1

    .prologue
    .line 298
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->spacing:F

    return v0
.end method

.method public getStrike()Z
    .locals 1

    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->strike:Z

    return v0
.end method

.method public getStyleId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->styleId:Ljava/lang/String;

    return-object v0
.end method

.method public getUnderline()Z
    .locals 1

    .prologue
    .line 209
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->underline:Z

    return v0
.end method

.method public getUnderlineProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->underlineProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    return-object v0
.end method

.method public getUnderlineStyle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->underlineStyle:Ljava/lang/String;

    return-object v0
.end method

.method public getVanish()Z
    .locals 1

    .prologue
    .line 257
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->hidden:Z

    return v0
.end method

.method public getVertAlign()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->vertAlign:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    return-object v0
.end method

.method public mergeCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V
    .locals 2
    .param p1, "charProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .prologue
    .line 342
    if-nez p1, :cond_1

    .line 399
    :cond_0
    :goto_0
    return-void

    .line 346
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->bold:Z

    if-nez v0, :cond_2

    .line 347
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->bold:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->bold:Z

    .line 348
    :cond_2
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->italic:Z

    if-nez v0, :cond_3

    .line 349
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->italic:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->italic:Z

    .line 350
    :cond_3
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->underline:Z

    if-nez v0, :cond_4

    .line 351
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->underline:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->underline:Z

    .line 352
    :cond_4
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->caps:Z

    if-nez v0, :cond_5

    .line 353
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->caps:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->caps:Z

    .line 354
    :cond_5
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->strike:Z

    if-nez v0, :cond_6

    .line 355
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->strike:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->strike:Z

    .line 356
    :cond_6
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->dStrike:Z

    if-nez v0, :cond_7

    .line 357
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->dStrike:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->dStrike:Z

    .line 358
    :cond_7
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->emboss:Z

    if-nez v0, :cond_8

    .line 359
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->emboss:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->emboss:Z

    .line 360
    :cond_8
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->engrave:Z

    if-nez v0, :cond_9

    .line 361
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->engrave:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->engrave:Z

    .line 362
    :cond_9
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontColor:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    if-nez v0, :cond_a

    .line 363
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontColor:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontColor:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .line 364
    :cond_a
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->asciiThemeFontName:Ljava/lang/String;

    if-nez v0, :cond_b

    .line 365
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->asciiThemeFontName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->asciiThemeFontName:Ljava/lang/String;

    .line 366
    :cond_b
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontName:Ljava/lang/String;

    if-nez v0, :cond_c

    .line 367
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontName:Ljava/lang/String;

    .line 368
    :cond_c
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontSize:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_d

    .line 369
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontSize:I

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontSize:I

    .line 370
    :cond_d
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->highlightColor:Ljava/lang/String;

    if-nez v0, :cond_e

    .line 371
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->highlightColor:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->highlightColor:Ljava/lang/String;

    .line 372
    :cond_e
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->outline:Z

    if-nez v0, :cond_f

    .line 373
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->outline:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->outline:Z

    .line 374
    :cond_f
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->rtl:Z

    if-nez v0, :cond_10

    .line 375
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->rtl:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->rtl:Z

    .line 376
    :cond_10
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->shadow:Z

    if-nez v0, :cond_11

    .line 377
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->shadow:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->shadow:Z

    .line 378
    :cond_11
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->spacing:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_12

    .line 379
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->spacing:F

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->spacing:F

    .line 380
    :cond_12
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->underlineStyle:Ljava/lang/String;

    if-nez v0, :cond_13

    .line 381
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->underlineStyle:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->underlineStyle:Ljava/lang/String;

    .line 382
    :cond_13
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->hidden:Z

    if-nez v0, :cond_14

    .line 383
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->hidden:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->hidden:Z

    .line 384
    :cond_14
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->smallCaps:Z

    if-nez v0, :cond_15

    .line 385
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->smallCaps:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->smallCaps:Z

    .line 386
    :cond_15
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->baseline:I

    if-nez v0, :cond_16

    .line 387
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->baseline:I

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->baseline:I

    .line 388
    :cond_16
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->shadeProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    if-nez v0, :cond_17

    .line 389
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->shadeProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->shadeProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    .line 390
    :cond_17
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->underlineProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    if-nez v0, :cond_18

    .line 391
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->underlineProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->underlineProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    .line 393
    :cond_18
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->hLinkId:Ljava/lang/String;

    if-nez v0, :cond_19

    .line 394
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->hLinkId:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->hLinkId:Ljava/lang/String;

    .line 397
    :cond_19
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->vertAlign:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    if-nez v0, :cond_0

    .line 398
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->vertAlign:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->vertAlign:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    goto/16 :goto_0
.end method

.method public setAsciiThemeFontName(Ljava/lang/String;)V
    .locals 0
    .param p1, "asciiThemeFontName"    # Ljava/lang/String;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->asciiThemeFontName:Ljava/lang/String;

    .line 127
    return-void
.end method

.method public setBaseLine(I)V
    .locals 1
    .param p1, "baseline"    # I

    .prologue
    .line 151
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->baseline:I

    .line 152
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->isBaseline:Z

    .line 153
    return-void
.end method

.method public setBold(Z)V
    .locals 0
    .param p1, "bold"    # Z

    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->bold:Z

    .line 70
    return-void
.end method

.method public setBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V
    .locals 0
    .param p1, "border"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->border:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 58
    return-void
.end method

.method public setCaps(Z)V
    .locals 0
    .param p1, "caps"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->caps:Z

    .line 62
    return-void
.end method

.method public setColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V
    .locals 0
    .param p1, "fontColor"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontColor:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .line 161
    return-void
.end method

.method public setDStrike(Z)V
    .locals 0
    .param p1, "dStrike"    # Z

    .prologue
    .line 98
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->dStrike:Z

    .line 99
    return-void
.end method

.method public setEmboss(Z)V
    .locals 0
    .param p1, "emboss"    # Z

    .prologue
    .line 102
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->emboss:Z

    .line 103
    return-void
.end method

.method public setEngrave(Z)V
    .locals 0
    .param p1, "engrave"    # Z

    .prologue
    .line 106
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->engrave:Z

    .line 107
    return-void
.end method

.method public setFontColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V
    .locals 0
    .param p1, "fontColor"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .prologue
    .line 434
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontColor:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .line 435
    return-void
.end method

.method public setFontName(Ljava/lang/String;)V
    .locals 0
    .param p1, "fontName"    # Ljava/lang/String;

    .prologue
    .line 130
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontName:Ljava/lang/String;

    .line 131
    return-void
.end method

.method public setFontSize(I)V
    .locals 0
    .param p1, "fontSize"    # I

    .prologue
    .line 147
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->fontSize:I

    .line 148
    return-void
.end method

.method public setHLink(Ljava/lang/String;)V
    .locals 0
    .param p1, "hLinkId"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->hLinkId:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public setHighlightColor(Ljava/lang/String;)V
    .locals 0
    .param p1, "highlightColor"    # Ljava/lang/String;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->highlightColor:Ljava/lang/String;

    .line 169
    return-void
.end method

.method public setItalic(Z)V
    .locals 0
    .param p1, "italic"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->italic:Z

    .line 74
    return-void
.end method

.method public setLumMod(Ljava/lang/String;)V
    .locals 0
    .param p1, "lumMod"    # Ljava/lang/String;

    .prologue
    .line 164
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->lumMod:Ljava/lang/String;

    .line 165
    return-void
.end method

.method public setOutline(Z)V
    .locals 0
    .param p1, "outline"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->outline:Z

    .line 111
    return-void
.end method

.method public setRtl(Z)V
    .locals 0
    .param p1, "rtl"    # Z

    .prologue
    .line 118
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->rtl:Z

    .line 119
    return-void
.end method

.method public setShadeColor(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;)V
    .locals 0
    .param p1, "shadeProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->shadeProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    .line 173
    return-void
.end method

.method public setShadow(Z)V
    .locals 0
    .param p1, "shadow"    # Z

    .prologue
    .line 122
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->shadow:Z

    .line 123
    return-void
.end method

.method public setShadowColor(Ljava/lang/String;)V
    .locals 0
    .param p1, "shadowColor"    # Ljava/lang/String;

    .prologue
    .line 180
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->shadowColor:Ljava/lang/String;

    .line 181
    return-void
.end method

.method public setSmallCaps(Z)V
    .locals 0
    .param p1, "smallCaps"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->smallCaps:Z

    .line 66
    return-void
.end method

.method public setSpacing(F)V
    .locals 0
    .param p1, "spacing"    # F

    .prologue
    .line 156
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->spacing:F

    .line 157
    return-void
.end method

.method public setStrike(Z)V
    .locals 0
    .param p1, "strike"    # Z

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->strike:Z

    .line 95
    return-void
.end method

.method public setStyleId(Ljava/lang/String;)V
    .locals 0
    .param p1, "styleId"    # Ljava/lang/String;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->styleId:Ljava/lang/String;

    .line 135
    return-void
.end method

.method public setUnderlinStyle(Ljava/lang/String;)V
    .locals 0
    .param p1, "underlineStyle"    # Ljava/lang/String;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->underlineStyle:Ljava/lang/String;

    .line 144
    return-void
.end method

.method public setUnderlineProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;)V
    .locals 0
    .param p1, "underlineProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->underlineProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    .line 91
    return-void
.end method

.method public setUndreline(Z)V
    .locals 0
    .param p1, "underline"    # Z

    .prologue
    .line 86
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->underline:Z

    .line 87
    return-void
.end method

.method public setVanish(Z)V
    .locals 0
    .param p1, "vanish"    # Z

    .prologue
    .line 114
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->hidden:Z

    .line 115
    return-void
.end method

.method public setVertAlign(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;)V
    .locals 0
    .param p1, "vertAlign"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    .prologue
    .line 176
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->vertAlign:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    .line 177
    return-void
.end method
