.class public interface abstract Lcom/samsung/thumbnail/office/ooxml/excel/display/IXExcelConsumer;
.super Ljava/lang/Object;
.source "IXExcelConsumer.java"


# virtual methods
.method public abstract addshapetext(Ljava/lang/String;I)V
.end method

.method public abstract endDocument()V
.end method

.method public abstract endHyperLink()V
.end method

.method public abstract endPicture(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;I)V
.end method

.method public abstract endRow()V
.end method

.method public abstract endTable()V
.end method

.method public abstract endTblCell()V
.end method

.method public abstract endTblRow()V
.end method

.method public abstract endText()V
.end method

.method public abstract linebreak()V
.end method

.method public abstract setCharacterProperty(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V
.end method

.method public abstract setImageRelId(Ljava/lang/String;)V
.end method

.method public abstract setPictStyle(Ljava/lang/String;)V
.end method

.method public abstract setPictureExtent(II)V
.end method

.method public abstract setTblCellProperty(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;)V
.end method

.method public abstract startBody()V
.end method

.method public abstract startPicture()V
.end method

.method public abstract startRow()V
.end method

.method public abstract startTable(I)V
.end method

.method public abstract startTblCell(I)V
.end method

.method public abstract startTblRow(I)V
.end method

.method public abstract starthyperlink(I)V
.end method

.method public abstract text(Ljava/lang/String;)V
.end method
