.class Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlNumFmt;
.super Ljava/lang/Object;
.source "XDocLvlHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LvlNumFmt"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;


# direct methods
.method constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlNumFmt;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V
    .locals 3
    .param p1, "value"    # Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;

    .prologue
    .line 147
    instance-of v1, p1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 148
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    .line 149
    .local v0, "numFmt":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$LvlNumFmt;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;

    # getter for: Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;->numLvl:Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;
    invoke-static {v1}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;->access$000(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;)Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->setNumFmt(Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;)V

    .line 151
    .end local v0    # "numFmt":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    :cond_0
    return-void
.end method
