.class public Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPrstGeomAvLstHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DiagramPrstGeomAvLstHandler.java"


# direct methods
.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IPstgeomHandler;)V
    .locals 4
    .param p1, "nsID"    # I
    .param p2, "pstGeom"    # Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IPstgeomHandler;

    .prologue
    .line 20
    const-string/jumbo v3, "avLst"

    invoke-direct {p0, p1, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 22
    const/4 v3, 0x1

    new-array v2, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 24
    .local v2, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPrstGeomAvLstGdHandler;

    const/16 v3, 0x1f

    invoke-direct {v0, v3, p2}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPrstGeomAvLstGdHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IPstgeomHandler;)V

    .line 26
    .local v0, "gdHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPrstGeomAvLstGdHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v3, "gd"

    invoke-direct {v1, v3, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 29
    .local v1, "gdSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v3, 0x0

    aput-object v1, v2, v3

    .line 31
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPrstGeomAvLstHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 32
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 43
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 37
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 38
    return-void
.end method
