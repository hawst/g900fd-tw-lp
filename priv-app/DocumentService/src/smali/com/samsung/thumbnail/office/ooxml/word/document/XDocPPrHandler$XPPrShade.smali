.class Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrShade;
.super Ljava/lang/Object;
.source "XDocPPrHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "XPPrShade"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;


# direct methods
.method constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrShade;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V
    .locals 3
    .param p1, "value"    # Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;

    .prologue
    .line 234
    instance-of v1, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadeValue;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 235
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadeValue;

    .line 236
    .local v0, "shade":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadeValue;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrShade;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;

    iget-object v1, v1, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;->paraProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadeValue;->getValue()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->setShadeColor(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;)V

    .line 238
    .end local v0    # "shade":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadeValue;
    :cond_0
    return-void
.end method
