.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsParser;
.super Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;
.source "XExcelWorkSheetsParser.java"


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;)V
    .locals 0
    .param p1, "worksheetobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;)V

    .line 16
    return-void
.end method


# virtual methods
.method public createRootElementHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsHandler;-><init>()V

    return-object v0
.end method

.method public getMainNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .locals 1

    .prologue
    .line 25
    const/16 v0, 0xc8

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v0

    return-object v0
.end method
