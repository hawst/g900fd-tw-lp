.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "SpprHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IPrstGeomHandler;,
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IXfrmHandler;
    }
.end annotation


# direct methods
.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IXfrmHandler;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IPrstGeomHandler;)V
    .locals 6
    .param p1, "nsID"    # I
    .param p2, "xfrm"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IXfrmHandler;
    .param p3, "prstGeom"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IPrstGeomHandler;

    .prologue
    .line 19
    const-string/jumbo v5, "spPr"

    invoke-direct {p0, p1, v5}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 21
    const/4 v5, 0x2

    new-array v3, v5, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 23
    .local v3, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XDocXfrmHandler;

    invoke-direct {v4, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XDocXfrmHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IXfrmHandler;)V

    .line 24
    .local v4, "xfrmHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XDocXfrmHandler;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v5, "xfrm"

    invoke-direct {v0, v5, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 26
    .local v0, "blipSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v5, 0x0

    aput-object v0, v3, v5

    .line 28
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;

    invoke-direct {v1, p3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IPrstGeomHandler;)V

    .line 29
    .local v1, "prstGeomHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PresetGeometryHandler;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v5, "prstGeom"

    invoke-direct {v2, v5, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 31
    .local v2, "prstSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v5, 0x1

    aput-object v2, v3, v5

    .line 33
    iput-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 34
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 39
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 40
    return-void
.end method
