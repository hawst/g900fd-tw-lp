.class public Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;
.super Lorg/apache/poi/POIXMLDocument;
.source "XWPFDocument.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "XWPFDocument"


# instance fields
.field private canvasStyles:Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

.field protected charts:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;",
            ">;"
        }
    .end annotation
.end field

.field protected dataModels:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;",
            ">;"
        }
    .end annotation
.end field

.field protected diagrams:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;",
            ">;"
        }
    .end annotation
.end field

.field private documentRelArray:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;",
            ">;"
        }
    .end annotation
.end field

.field private folderName:Ljava/lang/String;

.field private footer:Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;

.field private header:Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;

.field private hyperlinks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlink;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mFile:Ljava/io/File;

.field private mFooterRef:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$FooterRef;

.field private mHeaderRef:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;

.field private mSectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

.field private mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

.field private numbering:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;

.field protected picturesData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;",
            ">;"
        }
    .end annotation
.end field

.field private shapeTypes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;",
            ">;"
        }
    .end annotation
.end field

.field private themeObj:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 194
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->newPackage()Lorg/apache/poi/openxml4j/opc/OPCPackage;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/poi/POIXMLDocument;-><init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;)V

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    .line 195
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Landroid/content/Context;Lcom/samsung/thumbnail/customview/word/CustomView;Ljava/io/File;)V
    .locals 1
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "mContext"    # Landroid/content/Context;
    .param p3, "customView"    # Lcom/samsung/thumbnail/customview/word/CustomView;
    .param p4, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 126
    invoke-static {p1}, Lorg/apache/poi/util/PackageHelper;->open(Ljava/io/InputStream;)Lorg/apache/poi/openxml4j/opc/OPCPackage;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/poi/POIXMLDocument;-><init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;)V

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    .line 128
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mContext:Landroid/content/Context;

    .line 129
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getPackage()Lorg/apache/poi/openxml4j/opc/OPCPackage;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->getRelArray()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->documentRelArray:Ljava/util/List;

    .line 132
    new-instance v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    invoke-direct {v0, p0, p2, p3}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;Landroid/content/Context;Lcom/samsung/thumbnail/customview/word/CustomView;)V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    .line 133
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->hyperlinks:Ljava/util/ArrayList;

    .line 134
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->shapeTypes:Ljava/util/ArrayList;

    .line 135
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->charts:Ljava/util/HashMap;

    .line 136
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->dataModels:Ljava/util/HashMap;

    .line 137
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->diagrams:Ljava/util/HashMap;

    .line 138
    iput-object p4, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mFile:Ljava/io/File;

    .line 140
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;)V
    .locals 1
    .param p1, "pkg"    # Lorg/apache/poi/openxml4j/opc/OPCPackage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lorg/apache/poi/POIXMLDocument;-><init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;)V

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    .line 121
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFFactory;->getInstance()Lcom/samsung/thumbnail/office/ooxml/word/XWPFFactory;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->load(Lorg/apache/poi/POIXMLFactory;)V

    .line 122
    return-void
.end method

.method private getInputStream()Ljava/io/InputStream;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 498
    const/4 v1, 0x0

    .line 499
    .local v1, "inputStream":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 502
    .local v4, "zipInput":Ljava/util/zip/ZipInputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getFileInputStream()Ljava/io/FileInputStream;

    move-result-object v1

    .line 503
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setShowOnlyFirstSlide(Z)V

    .line 506
    new-instance v5, Ljava/util/zip/ZipInputStream;

    invoke-direct {v5, v1}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 510
    .end local v4    # "zipInput":Ljava/util/zip/ZipInputStream;
    .local v5, "zipInput":Ljava/util/zip/ZipInputStream;
    :cond_0
    :try_start_1
    invoke-virtual {v5}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v3

    .local v3, "zipEntry":Ljava/util/zip/ZipEntry;
    if-eqz v3, :cond_2

    .line 511
    invoke-virtual {v3}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v2

    .line 513
    .local v2, "name":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getEntryName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v6

    if-eqz v6, :cond_0

    .line 521
    if-eqz v1, :cond_1

    .line 522
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_1
    move-object v4, v5

    .line 524
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "zipEntry":Ljava/util/zip/ZipEntry;
    .end local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    :goto_0
    return-object v5

    .line 521
    .restart local v3    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    :cond_2
    if-eqz v1, :cond_5

    .line 522
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    move-object v4, v5

    .end local v3    # "zipEntry":Ljava/util/zip/ZipEntry;
    .end local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v4    # "zipInput":Ljava/util/zip/ZipInputStream;
    :cond_3
    :goto_1
    move-object v5, v4

    .line 524
    .local v5, "zipInput":Ljava/lang/Object;
    goto :goto_0

    .line 517
    .end local v5    # "zipInput":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 518
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_2
    const-string/jumbo v6, "XWPFDocument"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 521
    if-eqz v1, :cond_3

    .line 522
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    goto :goto_1

    .line 521
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    :goto_3
    if-eqz v1, :cond_4

    .line 522
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_4
    throw v6

    .line 521
    .end local v4    # "zipInput":Ljava/util/zip/ZipInputStream;
    .local v5, "zipInput":Ljava/util/zip/ZipInputStream;
    :catchall_1
    move-exception v6

    move-object v4, v5

    .end local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v4    # "zipInput":Ljava/util/zip/ZipInputStream;
    goto :goto_3

    .line 517
    .end local v4    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    :catch_1
    move-exception v0

    move-object v4, v5

    .end local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v4    # "zipInput":Ljava/util/zip/ZipInputStream;
    goto :goto_2

    .end local v4    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v3    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    :cond_5
    move-object v4, v5

    .end local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v4    # "zipInput":Ljava/util/zip/ZipInputStream;
    goto :goto_1
.end method

.method private getSectPrStream(Ljava/io/InputStream;J)Ljava/io/InputStream;
    .locals 20
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .param p2, "zipEntrySize"    # J

    .prologue
    .line 240
    const/4 v6, 0x0

    .line 241
    .local v6, "is":Ljava/io/ByteArrayInputStream;
    const-wide/16 v14, 0x0

    .line 243
    .local v14, "totalSize":J
    :try_start_0
    move-object/from16 v0, p1

    instance-of v0, v0, Ljava/util/zip/ZipInputStream;

    move/from16 v16, v0

    if-eqz v16, :cond_1

    .line 244
    move-wide/from16 v14, p2

    .line 250
    :goto_0
    const-wide/16 v16, 0x400

    sub-long v12, v14, v16

    .line 252
    .local v12, "sizeToReduce":J
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 253
    .local v2, "baos":Ljava/io/ByteArrayOutputStream;
    const/4 v8, 0x0

    .line 255
    .local v8, "read":I
    const/16 v16, 0x400

    move/from16 v0, v16

    new-array v3, v0, [B

    .line 256
    .local v3, "buffer":[B
    const/4 v11, 0x0

    .line 257
    .local v11, "totalRead":I
    :cond_0
    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/io/InputStream;->read([B)I

    move-result v8

    const/16 v16, -0x1

    move/from16 v0, v16

    if-eq v8, v0, :cond_2

    .line 259
    add-int/2addr v11, v8

    .line 260
    int-to-long v0, v11

    move-wide/from16 v16, v0

    cmp-long v16, v16, v12

    if-lez v16, :cond_0

    .line 261
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v2, v3, v0, v8}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 271
    .end local v2    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v3    # "buffer":[B
    .end local v8    # "read":I
    .end local v11    # "totalRead":I
    .end local v12    # "sizeToReduce":J
    :catch_0
    move-exception v4

    .line 272
    .local v4, "e":Ljava/lang/Exception;
    const-string/jumbo v16, "XWPFDocument"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Exception: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_2
    return-object v6

    .line 248
    :cond_1
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Ljava/io/InputStream;->available()I

    move-result v16

    move/from16 v0, v16

    int-to-long v14, v0

    goto :goto_0

    .line 264
    .restart local v2    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v3    # "buffer":[B
    .restart local v8    # "read":I
    .restart local v11    # "totalRead":I
    .restart local v12    # "sizeToReduce":J
    :cond_2
    new-instance v10, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v10, v0}, Ljava/lang/String;-><init>([B)V

    .line 265
    .local v10, "sectionString":Ljava/lang/String;
    const-string/jumbo v16, "<w:sectPr"

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v16

    const-string/jumbo v17, "</w:body>"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v17

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v10, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 269
    .local v9, "sectPart":Ljava/lang/String;
    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    .line 270
    .local v5, "finalArray":[B
    new-instance v7, Ljava/io/ByteArrayInputStream;

    invoke-direct {v7, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .end local v6    # "is":Ljava/io/ByteArrayInputStream;
    .local v7, "is":Ljava/io/ByteArrayInputStream;
    move-object v6, v7

    .line 273
    .end local v7    # "is":Ljava/io/ByteArrayInputStream;
    .restart local v6    # "is":Ljava/io/ByteArrayInputStream;
    goto :goto_2
.end method

.method private getZipEntrySize()J
    .locals 11

    .prologue
    .line 528
    const-wide/16 v4, 0x0

    .line 530
    .local v4, "size":J
    const/4 v1, 0x0

    .line 532
    .local v1, "inputStream":Ljava/io/InputStream;
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getFileInputStream()Ljava/io/FileInputStream;

    move-result-object v1

    .line 533
    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setShowOnlyFirstSlide(Z)V

    .line 536
    const/4 v6, 0x0

    .line 537
    .local v6, "zipInput":Ljava/util/zip/ZipInputStream;
    new-instance v6, Ljava/util/zip/ZipInputStream;

    .end local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    invoke-direct {v6, v1}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 539
    .restart local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    const/4 v3, 0x0

    .line 542
    .local v3, "zipEntry":Ljava/util/zip/ZipEntry;
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {v6}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 543
    invoke-virtual {v3}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v2

    .line 545
    .local v2, "name":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getEntryName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 546
    if-eqz v3, :cond_0

    .line 547
    invoke-virtual {v3}, Ljava/util/zip/ZipEntry;->getSize()J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    goto :goto_0

    .line 554
    .end local v2    # "name":Ljava/lang/String;
    :cond_1
    if-eqz v6, :cond_2

    .line 556
    :try_start_1
    invoke-virtual {v6}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 562
    :cond_2
    :goto_1
    if-eqz v1, :cond_3

    .line 564
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 571
    :cond_3
    :goto_2
    return-wide v4

    .line 557
    :catch_0
    move-exception v0

    .line 558
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v7, "XWPFDocument"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 565
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 566
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v7, "XWPFDocument"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 551
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 552
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_3
    const-string/jumbo v7, "XWPFDocument"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 554
    if-eqz v6, :cond_4

    .line 556
    :try_start_4
    invoke-virtual {v6}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 562
    :cond_4
    :goto_3
    if-eqz v1, :cond_3

    .line 564
    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_2

    .line 565
    :catch_3
    move-exception v0

    .line 566
    const-string/jumbo v7, "XWPFDocument"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 557
    :catch_4
    move-exception v0

    .line 558
    const-string/jumbo v7, "XWPFDocument"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 554
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    if-eqz v6, :cond_5

    .line 556
    :try_start_6
    invoke-virtual {v6}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 562
    :cond_5
    :goto_4
    if-eqz v1, :cond_6

    .line 564
    :try_start_7
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    .line 567
    :cond_6
    :goto_5
    throw v7

    .line 557
    :catch_5
    move-exception v0

    .line 558
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v8, "XWPFDocument"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 565
    .end local v0    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v0

    .line 566
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v8, "XWPFDocument"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method private initHyperlinks()V
    .locals 7

    .prologue
    .line 610
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v3

    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->HYPERLINK:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->getRelation()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getRelationshipsByType(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 614
    .local v2, "relIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/openxml4j/opc/PackageRelationship;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 615
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    .line 616
    .local v1, "rel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->hyperlinks:Ljava/util/ArrayList;

    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlink;

    invoke-virtual {v1}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    move-result-object v6

    invoke-virtual {v6}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlink;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 619
    .end local v1    # "rel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .end local v2    # "relIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/openxml4j/opc/PackageRelationship;>;"
    :catch_0
    move-exception v0

    .line 620
    .local v0, "e":Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
    new-instance v3, Lorg/apache/poi/POIXMLException;

    invoke-direct {v3, v0}, Lorg/apache/poi/POIXMLException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 622
    .end local v0    # "e":Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
    .restart local v2    # "relIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/openxml4j/opc/PackageRelationship;>;"
    :cond_0
    return-void
.end method

.method protected static newPackage()Lorg/apache/poi/openxml4j/opc/OPCPackage;
    .locals 5

    .prologue
    .line 730
    :try_start_0
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {v3}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->create(Ljava/io/OutputStream;)Lorg/apache/poi/openxml4j/opc/OPCPackage;

    move-result-object v2

    .line 732
    .local v2, "pkg":Lorg/apache/poi/openxml4j/opc/OPCPackage;
    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->DOCUMENT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->getDefaultFileName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/apache/poi/openxml4j/opc/PackagingURIHelper;->createPartName(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackagePartName;

    move-result-object v0

    .line 735
    .local v0, "corePartName":Lorg/apache/poi/openxml4j/opc/PackagePartName;
    sget-object v3, Lorg/apache/poi/openxml4j/opc/TargetMode;->INTERNAL:Lorg/apache/poi/openxml4j/opc/TargetMode;

    const-string/jumbo v4, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"

    invoke-virtual {v2, v0, v3, v4}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->addRelationship(Lorg/apache/poi/openxml4j/opc/PackagePartName;Lorg/apache/poi/openxml4j/opc/TargetMode;Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    .line 738
    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->DOCUMENT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->getContentType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->createPart(Lorg/apache/poi/openxml4j/opc/PackagePartName;Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackagePart;

    .line 740
    invoke-virtual {v2}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->getPackageProperties()Lorg/apache/poi/openxml4j/opc/PackageProperties;

    move-result-object v3

    const-string/jumbo v4, "Apache POI"

    invoke-interface {v3, v4}, Lorg/apache/poi/openxml4j/opc/PackageProperties;->setCreatorProperty(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 742
    return-object v2

    .line 743
    .end local v0    # "corePartName":Lorg/apache/poi/openxml4j/opc/PackagePartName;
    :catch_0
    move-exception v1

    .line 744
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Lorg/apache/poi/POIXMLException;

    invoke-direct {v3, v1}, Lorg/apache/poi/POIXMLException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method

.method private parseDocument(Ljava/io/InputStream;)V
    .locals 5
    .param p1, "inputStream"    # Ljava/io/InputStream;

    .prologue
    .line 198
    const/4 v0, 0x0

    .line 200
    .local v0, "documentParser":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentParser;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentParser;

    .end local v0    # "documentParser":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentParser;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    invoke-direct {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;)V

    .line 201
    .restart local v0    # "documentParser":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentParser;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setShowOnlyFirstSlide(Z)V

    .line 204
    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentParser;->parse(Ljava/io/InputStream;)V

    .line 207
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    :goto_0
    return-void

    .line 208
    :catch_0
    move-exception v1

    .line 209
    .local v1, "e":Ljava/io/IOException;
    const-string/jumbo v2, "XWPFDocument"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private parseDocumentForSectionPr(Ljava/io/InputStream;J)V
    .locals 10
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .param p2, "zipEntrySize"    # J

    .prologue
    .line 280
    const/4 v0, 0x0

    .line 282
    .local v0, "documentParser":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentSecPrParser;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentSecPrParser;

    .end local v0    # "documentParser":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentSecPrParser;
    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    invoke-direct {v0, v7}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentSecPrParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;)V

    .line 283
    .restart local v0    # "documentParser":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentSecPrParser;
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getSectPrStream(Ljava/io/InputStream;J)Ljava/io/InputStream;

    move-result-object v6

    .line 285
    .local v6, "is":Ljava/io/InputStream;
    if-eqz v6, :cond_1

    .line 286
    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentSecPrParser;->parse(Ljava/io/InputStream;)V

    .line 297
    :goto_0
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentSecPrParser;->getSectPr()Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mSectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    .line 299
    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mSectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    if-eqz v7, :cond_9

    .line 300
    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mSectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->isTitlePagePresence()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 301
    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mSectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->getHeaderRef()Ljava/util/ArrayList;

    move-result-object v4

    .line 302
    .local v4, "headerRef":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;>;"
    if-eqz v4, :cond_3

    .line 303
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v5, v7, :cond_3

    .line 304
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;

    iget-object v7, v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;->type:Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;

    sget-object v8, Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;->FIRST:Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;

    if-ne v7, v8, :cond_0

    .line 305
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;

    iput-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mHeaderRef:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;

    .line 303
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 289
    .end local v4    # "headerRef":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;>;"
    .end local v5    # "i":I
    :cond_1
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->markSupported()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 290
    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 294
    :cond_2
    :goto_2
    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentSecPrParser;->parse(Ljava/io/InputStream;)V

    goto :goto_0

    .line 291
    :catch_0
    move-exception v2

    .line 292
    .local v2, "e1":Ljava/io/IOException;
    const-string/jumbo v7, "XWPFDocument"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 310
    .end local v2    # "e1":Ljava/io/IOException;
    .restart local v4    # "headerRef":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;>;"
    :cond_3
    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mSectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->getFooterRef()Ljava/util/ArrayList;

    move-result-object v3

    .line 311
    .local v3, "footerRef":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$FooterRef;>;"
    if-eqz v3, :cond_9

    .line 312
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v5, v7, :cond_9

    .line 313
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$FooterRef;

    iget-object v7, v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$FooterRef;->type:Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;

    sget-object v8, Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;->FIRST:Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;

    if-ne v7, v8, :cond_4

    .line 314
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$FooterRef;

    iput-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mFooterRef:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$FooterRef;

    .line 312
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 320
    .end local v3    # "footerRef":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$FooterRef;>;"
    .end local v4    # "headerRef":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;>;"
    .end local v5    # "i":I
    :cond_5
    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mSectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->getHeaderRef()Ljava/util/ArrayList;

    move-result-object v4

    .line 321
    .restart local v4    # "headerRef":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;>;"
    if-eqz v4, :cond_7

    .line 322
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_4
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v5, v7, :cond_7

    .line 323
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;

    iget-object v7, v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;->type:Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;

    sget-object v8, Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;->DEFAULT:Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;

    if-ne v7, v8, :cond_6

    .line 324
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;

    iput-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mHeaderRef:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;

    .line 322
    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 329
    .end local v5    # "i":I
    :cond_7
    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mSectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->getFooterRef()Ljava/util/ArrayList;

    move-result-object v3

    .line 330
    .restart local v3    # "footerRef":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$FooterRef;>;"
    if-eqz v3, :cond_9

    .line 331
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_5
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v5, v7, :cond_9

    .line 332
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$FooterRef;

    iget-object v7, v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$FooterRef;->type:Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;

    sget-object v8, Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;->DEFAULT:Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;

    if-ne v7, v8, :cond_8

    .line 333
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$FooterRef;

    iput-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mFooterRef:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$FooterRef;

    .line 331
    :cond_8
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 341
    .end local v3    # "footerRef":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$FooterRef;>;"
    .end local v4    # "headerRef":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;>;"
    .end local v5    # "i":I
    :cond_9
    :try_start_1
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 343
    if-eqz v6, :cond_a

    .line 344
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 349
    :cond_a
    :goto_6
    return-void

    .line 346
    :catch_1
    move-exception v1

    .line 347
    .local v1, "e":Ljava/io/IOException;
    const-string/jumbo v7, "XWPFDocument"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6
.end method

.method private parseDocumentForSettings(Ljava/io/InputStream;)V
    .locals 5
    .param p1, "inputStream"    # Ljava/io/InputStream;

    .prologue
    .line 214
    const/4 v1, 0x0

    .line 216
    .local v1, "settingsParser":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentSetParser;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentSetParser;

    .end local v1    # "settingsParser":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentSetParser;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    invoke-direct {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentSetParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;)V

    .line 217
    .restart local v1    # "settingsParser":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentSetParser;
    invoke-virtual {v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentSetParser;->parse(Ljava/io/InputStream;)V

    .line 220
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    :goto_0
    return-void

    .line 221
    :catch_0
    move-exception v0

    .line 222
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v2, "XWPFDocument"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public addPicture(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;)V
    .locals 2
    .param p1, "picture"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;

    .prologue
    .line 648
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument$1;

    invoke-direct {v1, p0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument$1;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 655
    return-void
.end method

.method public addShapeTypes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V
    .locals 1
    .param p1, "shapeType"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    .prologue
    .line 575
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->shapeTypes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 576
    return-void
.end method

.method public getAllEmbedds()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/openxml4j/opc/PackagePart;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/OpenXML4JException;
        }
    .end annotation

    .prologue
    .line 765
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 768
    .local v0, "embedds":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/openxml4j/opc/PackagePart;>;"
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v3

    const-string/jumbo v4, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/oleObject"

    invoke-virtual {v3, v4}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getRelationshipsByType(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    .line 770
    .local v2, "rel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    invoke-virtual {p0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getTargetPart(Lorg/apache/poi/openxml4j/opc/PackageRelationship;)Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 772
    .end local v2    # "rel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v3

    const-string/jumbo v4, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/package"

    invoke-virtual {v3, v4}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getRelationshipsByType(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    .line 774
    .restart local v2    # "rel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    invoke-virtual {p0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getTargetPart(Lorg/apache/poi/openxml4j/opc/PackageRelationship;)Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 776
    .end local v2    # "rel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    :cond_1
    return-object v0
.end method

.method public getAllPictureData()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 698
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->picturesData:Ljava/util/HashMap;

    return-object v0
.end method

.method public getCanvas()Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getCanvas()Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    move-result-object v0

    return-object v0
.end method

.method public getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->canvasStyles:Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    return-object v0
.end method

.method public getChartById(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;
    .locals 1
    .param p1, "chartRelId"    # Ljava/lang/String;

    .prologue
    .line 592
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->charts:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    return-object v0
.end method

.method public getDiagramDataModelId(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;
    .locals 1
    .param p1, "dataModelId"    # Ljava/lang/String;

    .prologue
    .line 596
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->dataModels:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

    return-object v0
.end method

.method public getDiagramRelId(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;
    .locals 1
    .param p1, "diagramRelId"    # Ljava/lang/String;

    .prologue
    .line 600
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->diagrams:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    return-object v0
.end method

.method public getDocRelList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;",
            ">;"
        }
    .end annotation

    .prologue
    .line 182
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->documentRelArray:Ljava/util/List;

    return-object v0
.end method

.method public getElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v0

    return-object v0
.end method

.method public getFileInputStream()Ljava/io/FileInputStream;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 171
    :try_start_0
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mFile:Ljava/io/File;

    if-eqz v1, :cond_0

    .line 172
    new-instance v1, Ljava/io/FileInputStream;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mFile:Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    :goto_0
    return-object v1

    .line 174
    :catch_0
    move-exception v0

    .line 175
    .local v0, "e":Ljava/io/FileNotFoundException;
    const-string/jumbo v1, "XWPFDocument"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    .line 176
    goto :goto_0

    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :cond_0
    move-object v1, v2

    .line 178
    goto :goto_0
.end method

.method public getFolderPath()Ljava/io/File;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getFolderPath()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public getFooter()Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;
    .locals 1

    .prologue
    .line 643
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->footer:Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;

    return-object v0
.end method

.method public getHeader()Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;
    .locals 1

    .prologue
    .line 639
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->header:Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;

    return-object v0
.end method

.method public getHyperlinkByID(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlink;
    .locals 3
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 625
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->hyperlinks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 626
    .local v0, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlink;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 627
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlink;

    .line 628
    .local v1, "link":Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlink;
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlink;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 631
    .end local v1    # "link":Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlink;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getHyperlinks()[Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlink;
    .locals 2

    .prologue
    .line 635
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->hyperlinks:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->hyperlinks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlink;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlink;

    return-object v0
.end method

.method public getNumbering()Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;
    .locals 1

    .prologue
    .line 785
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->numbering:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;

    return-object v0
.end method

.method public getPartById(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackagePart;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 754
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCorePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v1

    invoke-virtual {v1, p1}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getRelationship(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getTargetPart(Lorg/apache/poi/openxml4j/opc/PackageRelationship;)Lorg/apache/poi/openxml4j/opc/PackagePart;
    :try_end_0
    .catch Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 755
    :catch_0
    move-exception v0

    .line 756
    .local v0, "e":Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public getShapeType(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .locals 3
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 579
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->shapeTypes:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    .line 580
    .local v1, "shapeType":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 584
    .end local v1    # "shapeType":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getShapeTypes()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;",
            ">;"
        }
    .end annotation

    .prologue
    .line 588
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->shapeTypes:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    .locals 1

    .prologue
    .line 604
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->themeObj:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    return-object v0
.end method

.method public getXWPFDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;
    .locals 0

    .prologue
    .line 789
    return-object p0
.end method

.method public load()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 190
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFFactory;->getInstance()Lcom/samsung/thumbnail/office/ooxml/word/XWPFFactory;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->load(Lorg/apache/poi/POIXMLFactory;)V

    .line 191
    return-void
.end method

.method protected onDocumentRead()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 361
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    iput-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->picturesData:Ljava/util/HashMap;

    .line 363
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getRelations()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/poi/POIXMLDocumentPart;

    .line 364
    .local v7, "p":Lorg/apache/poi/POIXMLDocumentPart;
    invoke-virtual {v7}, Lorg/apache/poi/POIXMLDocumentPart;->getPackageRelationship()Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v12

    invoke-virtual {v12}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getRelationshipType()Ljava/lang/String;

    move-result-object v12

    sget-object v13, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->STYLES:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->getRelation()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 372
    new-instance v12, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    invoke-virtual {v7}, Lorg/apache/poi/POIXMLDocumentPart;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v13

    invoke-virtual {v7}, Lorg/apache/poi/POIXMLDocumentPart;->getPackageRelationship()Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v14

    invoke-direct {v12, v13, v14}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;-><init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V

    iput-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->canvasStyles:Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    .line 374
    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v12

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setShowOnlyFirstSlide(Z)V

    .line 377
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v12

    invoke-virtual {v12}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 378
    .local v5, "is":Ljava/io/InputStream;
    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    sget-object v13, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->PARAGRAPH:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    invoke-virtual {v12, v13}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->setElementType(Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;)V

    .line 379
    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v12

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setShowOnlyFirstSlide(Z)V

    .line 382
    if-eqz v5, :cond_2

    .line 383
    const-wide/16 v12, 0x0

    invoke-direct {p0, v5, v12, v13}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->parseDocumentForSectionPr(Ljava/io/InputStream;J)V

    .line 392
    :cond_1
    :goto_1
    if-eqz v5, :cond_0

    .line 393
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    goto :goto_0

    .line 385
    :cond_2
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getInputStream()Ljava/io/InputStream;

    move-result-object v11

    check-cast v11, Ljava/util/zip/ZipInputStream;

    .line 386
    .local v11, "zis":Ljava/util/zip/ZipInputStream;
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getZipEntrySize()J

    move-result-wide v12

    invoke-direct {p0, v11, v12, v13}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->parseDocumentForSectionPr(Ljava/io/InputStream;J)V

    .line 387
    if-eqz v11, :cond_1

    .line 388
    invoke-virtual {v11}, Ljava/util/zip/ZipInputStream;->close()V

    goto :goto_1

    .line 396
    .end local v5    # "is":Ljava/io/InputStream;
    .end local v11    # "zis":Ljava/util/zip/ZipInputStream;
    :cond_3
    invoke-virtual {v7}, Lorg/apache/poi/POIXMLDocumentPart;->getPackageRelationship()Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v12

    invoke-virtual {v12}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getRelationshipType()Ljava/lang/String;

    move-result-object v12

    sget-object v13, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->SETTINGS:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->getRelation()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 398
    invoke-virtual {v7}, Lorg/apache/poi/POIXMLDocumentPart;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v12

    invoke-virtual {v12}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    .line 400
    .local v4, "inStream":Ljava/io/InputStream;
    if-eqz v4, :cond_5

    .line 401
    invoke-direct {p0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->parseDocumentForSettings(Ljava/io/InputStream;)V

    .line 409
    :cond_4
    :goto_2
    if-eqz v4, :cond_0

    .line 410
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    goto/16 :goto_0

    .line 403
    :cond_5
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getInputStream()Ljava/io/InputStream;

    move-result-object v11

    check-cast v11, Ljava/util/zip/ZipInputStream;

    .line 404
    .restart local v11    # "zis":Ljava/util/zip/ZipInputStream;
    invoke-direct {p0, v11}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->parseDocumentForSettings(Ljava/io/InputStream;)V

    .line 405
    if-eqz v11, :cond_4

    .line 406
    invoke-virtual {v11}, Ljava/util/zip/ZipInputStream;->close()V

    goto :goto_2

    .line 415
    .end local v4    # "inStream":Ljava/io/InputStream;
    .end local v7    # "p":Lorg/apache/poi/POIXMLDocumentPart;
    .end local v11    # "zis":Ljava/util/zip/ZipInputStream;
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getRelations()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_7
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/poi/POIXMLDocumentPart;

    .line 416
    .restart local v7    # "p":Lorg/apache/poi/POIXMLDocumentPart;
    invoke-virtual {v7}, Lorg/apache/poi/POIXMLDocumentPart;->getPackageRelationship()Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v9

    .line 417
    .local v9, "rel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    invoke-virtual {v9}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getRelationshipType()Ljava/lang/String;

    move-result-object v10

    .line 421
    .local v10, "relation":Ljava/lang/String;
    sget-object v12, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->IMAGES:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->getRelation()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    move-object v8, v7

    .line 422
    check-cast v8, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;

    .line 423
    .local v8, "pictData":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;
    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->picturesData:Ljava/util/HashMap;

    invoke-virtual {v9}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getId()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 424
    invoke-virtual {p0, v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->addPicture(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;)V

    goto :goto_3

    .line 426
    .end local v8    # "pictData":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;
    :cond_8
    sget-object v12, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->NUMBERING:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->getRelation()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 428
    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;

    .end local v7    # "p":Lorg/apache/poi/POIXMLDocumentPart;
    iput-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->numbering:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;

    .line 429
    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->numbering:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;->onDocumentRead()V

    goto :goto_3

    .line 430
    .restart local v7    # "p":Lorg/apache/poi/POIXMLDocumentPart;
    :cond_9
    sget-object v12, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->HEADER:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->getRelation()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 431
    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mHeaderRef:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;

    if-eqz v12, :cond_7

    .line 433
    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;

    .end local v7    # "p":Lorg/apache/poi/POIXMLDocumentPart;
    iput-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->header:Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;

    .line 434
    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mHeaderRef:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;

    iget-object v12, v12, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;->rID:Ljava/lang/String;

    if-eqz v12, :cond_7

    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->header:Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;->getPackageRelationship()Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v12

    invoke-virtual {v12}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getId()Ljava/lang/String;

    move-result-object v12

    iget-object v13, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mHeaderRef:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;

    iget-object v13, v13, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;->rID:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 437
    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    sget-object v13, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->HEADER:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    invoke-virtual {v12, v13}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->setElementType(Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;)V

    .line 438
    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->header:Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;

    iget-object v13, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    invoke-virtual {v12, v13}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;->onDocumentRead(Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;)V

    .line 439
    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v12

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setShowOnlyFirstSlide(Z)V

    goto/16 :goto_3

    .line 444
    .restart local v7    # "p":Lorg/apache/poi/POIXMLDocumentPart;
    :cond_a
    sget-object v12, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->FOOTER:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->getRelation()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 445
    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mFooterRef:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$FooterRef;

    if-eqz v12, :cond_7

    .line 447
    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;

    .end local v7    # "p":Lorg/apache/poi/POIXMLDocumentPart;
    iput-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->footer:Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;

    .line 448
    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mFooterRef:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$FooterRef;

    iget-object v12, v12, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$FooterRef;->rID:Ljava/lang/String;

    if-eqz v12, :cond_7

    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->footer:Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;->getPackageRelationship()Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v12

    invoke-virtual {v12}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getId()Ljava/lang/String;

    move-result-object v12

    iget-object v13, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mFooterRef:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$FooterRef;

    iget-object v13, v13, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$FooterRef;->rID:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 451
    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    sget-object v13, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->FOOTER:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    invoke-virtual {v12, v13}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->setElementType(Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;)V

    .line 452
    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->footer:Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;

    iget-object v13, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    invoke-virtual {v12, v13}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;->onDocumentRead(Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;)V

    .line 453
    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v12

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setShowOnlyFirstSlide(Z)V

    goto/16 :goto_3

    .line 458
    .restart local v7    # "p":Lorg/apache/poi/POIXMLDocumentPart;
    :cond_b
    sget-object v12, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->CHART:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->getRelation()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_c

    move-object v0, v7

    .line 460
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .line 461
    .local v0, "chart":Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;
    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->charts:Ljava/util/HashMap;

    invoke-virtual {v7}, Lorg/apache/poi/POIXMLDocumentPart;->getPackageRelationship()Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v13

    invoke-virtual {v13}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getId()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 462
    .end local v0    # "chart":Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;
    :cond_c
    sget-object v12, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->SMART_ART:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->getRelation()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_d

    move-object v2, v7

    .line 463
    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    .line 464
    .local v2, "diagram":Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;
    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->diagrams:Ljava/util/HashMap;

    invoke-virtual {v7}, Lorg/apache/poi/POIXMLDocumentPart;->getPackageRelationship()Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v13

    invoke-virtual {v13}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getId()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 465
    .end local v2    # "diagram":Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;
    :cond_d
    sget-object v12, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->DATA_MODEL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->getRelation()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_e

    move-object v1, v7

    .line 466
    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

    .line 467
    .local v1, "dataModel":Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;
    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->dataModels:Ljava/util/HashMap;

    invoke-virtual {v7}, Lorg/apache/poi/POIXMLDocumentPart;->getPackageRelationship()Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v13

    invoke-virtual {v13}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getId()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 468
    .end local v1    # "dataModel":Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;
    :cond_e
    sget-object v12, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->THEME:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->getRelation()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 469
    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .end local v7    # "p":Lorg/apache/poi/POIXMLDocumentPart;
    iput-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->themeObj:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .line 470
    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->themeObj:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->parseTheme()V

    goto/16 :goto_3

    .line 474
    .end local v9    # "rel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .end local v10    # "relation":Ljava/lang/String;
    :cond_f
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->initHyperlinks()V

    .line 476
    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    sget-object v13, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->PARAGRAPH:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    invoke-virtual {v12, v13}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->setElementType(Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;)V

    .line 477
    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v12

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setShowOnlyFirstSlide(Z)V

    .line 480
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v12

    invoke-virtual {v12}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    .line 482
    .local v6, "isDoc":Ljava/io/InputStream;
    if-eqz v6, :cond_12

    .line 483
    invoke-direct {p0, v6}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->parseDocument(Ljava/io/InputStream;)V

    .line 492
    :cond_10
    :goto_4
    if-eqz v6, :cond_11

    .line 493
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 495
    :cond_11
    return-void

    .line 485
    :cond_12
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getInputStream()Ljava/io/InputStream;

    move-result-object v11

    check-cast v11, Ljava/util/zip/ZipInputStream;

    .line 486
    .restart local v11    # "zis":Ljava/util/zip/ZipInputStream;
    invoke-direct {p0, v11}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->parseDocument(Ljava/io/InputStream;)V

    .line 487
    if-eqz v11, :cond_10

    .line 488
    invoke-virtual {v11}, Ljava/util/zip/ZipInputStream;->close()V

    goto :goto_4
.end method

.method public setEmailPreview(Z)V
    .locals 0
    .param p1, "isEmailPreview"    # Z

    .prologue
    .line 187
    return-void
.end method

.method public setFolderName()V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mViewConsumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getFolderName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->folderName:Ljava/lang/String;

    .line 144
    return-void
.end method

.method public writePicture(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;)V
    .locals 12
    .param p1, "picture"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;

    .prologue
    .line 659
    const/4 v4, 0x0

    .line 663
    .local v4, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;->getFileName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 665
    .local v3, "fileName":Ljava/lang/String;
    if-nez v3, :cond_1

    .line 689
    if-eqz v4, :cond_0

    .line 690
    :try_start_1
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 695
    .end local v3    # "fileName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 691
    .restart local v3    # "fileName":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 692
    .local v1, "e":Ljava/io/IOException;
    const-string/jumbo v8, "XWPFDocument"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 667
    .end local v1    # "e":Ljava/io/IOException;
    :cond_1
    :try_start_2
    iget-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->folderName:Ljava/lang/String;

    const/4 v10, 0x2

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    .line 669
    .local v0, "dir":Ljava/io/File;
    new-instance v5, Ljava/io/FileOutputStream;

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v5, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 677
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .local v5, "fos":Ljava/io/FileOutputStream;
    :try_start_3
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;->getData()[B

    move-result-object v7

    .line 678
    .local v7, "picData":[B
    if-eqz v7, :cond_2

    array-length v8, v7

    if-lez v8, :cond_2

    .line 679
    invoke-virtual {v5, v7}, Ljava/io/FileOutputStream;->write([B)V

    .line 680
    :cond_2
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->flush()V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 689
    if-eqz v5, :cond_3

    .line 690
    :try_start_4
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_3
    move-object v4, v5

    .line 693
    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 691
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v1

    .line 692
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v8, "XWPFDocument"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v5

    .line 694
    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 682
    .end local v0    # "dir":Ljava/io/File;
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "fileName":Ljava/lang/String;
    .end local v7    # "picData":[B
    :catch_2
    move-exception v2

    .line 683
    .local v2, "ex":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_5
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "FileNotFoundException : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 689
    if-eqz v4, :cond_0

    .line 690
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_0

    .line 691
    :catch_3
    move-exception v1

    .line 692
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v8, "XWPFDocument"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 684
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "ex":Ljava/io/FileNotFoundException;
    :catch_4
    move-exception v6

    .line 685
    .local v6, "ioe":Ljava/io/IOException;
    :goto_2
    :try_start_7
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "IOException : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 689
    if-eqz v4, :cond_0

    .line 690
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    goto/16 :goto_0

    .line 691
    :catch_5
    move-exception v1

    .line 692
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v8, "XWPFDocument"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 687
    .end local v1    # "e":Ljava/io/IOException;
    .end local v6    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 689
    :goto_3
    if-eqz v4, :cond_4

    .line 690
    :try_start_9
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 693
    :cond_4
    :goto_4
    throw v8

    .line 691
    :catch_6
    move-exception v1

    .line 692
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v9, "XWPFDocument"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Exception: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 687
    .end local v1    # "e":Ljava/io/IOException;
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v0    # "dir":Ljava/io/File;
    .restart local v3    # "fileName":Ljava/lang/String;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v8

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 684
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v6

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 682
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    :catch_8
    move-exception v2

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_1
.end method
