.class public Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XExcelDrawingPicHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;


# instance fields
.field private cellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 27
    const/16 v0, 0xc9

    const-string/jumbo v1, "pic"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 29
    return-void
.end method

.method private init(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 9
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    const/16 v8, 0xc9

    .line 32
    const/4 v7, 0x3

    new-array v2, v7, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 34
    .local v2, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler;

    invoke-direct {v0, v8, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;)V

    .line 36
    .local v0, "blipFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XdrSeqDescriptor;

    const-string/jumbo v7, "blipFill"

    invoke-direct {v6, v7, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XdrSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 38
    .local v6, "xdrSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XdrSeqDescriptor;
    const/4 v7, 0x0

    aput-object v6, v2, v7

    .line 41
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingNvPicPrHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingNvPicPrHandler;-><init>()V

    .line 42
    .local v1, "nvpicprhandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingNvPicPrHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XdrSeqDescriptor;

    const-string/jumbo v7, "nvPicPr"

    invoke-direct {v4, v7, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XdrSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 44
    .local v4, "xdr1Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XdrSeqDescriptor;
    const/4 v7, 0x1

    aput-object v4, v2, v7

    .line 47
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;

    invoke-direct {v3, v8, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 51
    .local v3, "spPrHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XdrSeqDescriptor;

    const-string/jumbo v7, "spPr"

    invoke-direct {v5, v7, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XdrSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 53
    .local v5, "xdr2Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XdrSeqDescriptor;
    const/4 v7, 0x2

    aput-object v5, v2, v7

    .line 57
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 58
    return-void
.end method


# virtual methods
.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 85
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 86
    return-void
.end method

.method public setBlipEmbed(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "blipEmbed"    # Ljava/lang/String;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicHandler;->cellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->setBlipId(Ljava/lang/String;)V

    .line 62
    return-void
.end method

.method public setTile(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "flip"    # Ljava/lang/String;

    .prologue
    .line 66
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 71
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 73
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getCurrentCellAnchor()Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicHandler;->cellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    .line 74
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicHandler;->cellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;->PICTURE:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->setType(Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;)V

    .line 76
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;-><init>()V

    .line 77
    .local v0, "shapeInfo":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicHandler;->cellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->setShapePropery(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 79
    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicHandler;->init(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 80
    return-void
.end method
