.class public Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumRefHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DMLChartNumRefHandler.java"


# instance fields
.field private ptVConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;)V
    .locals 2
    .param p1, "ptVConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;

    .prologue
    .line 23
    const/16 v0, 0x23

    const-string/jumbo v1, "numRef"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 24
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumRefHandler;->ptVConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;

    .line 25
    return-void
.end method

.method private init()V
    .locals 6

    .prologue
    .line 28
    const/4 v5, 0x2

    new-array v4, v5, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 30
    .local v4, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrFHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrFHandler;-><init>()V

    .line 31
    .local v0, "fHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrFHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v5, "f"

    invoke-direct {v1, v5, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 33
    .local v1, "fSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/4 v5, 0x0

    aput-object v1, v4, v5

    .line 35
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumCacheHandler;

    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumRefHandler;->ptVConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;

    invoke-direct {v2, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumCacheHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;)V

    .line 37
    .local v2, "numcacheHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumCacheHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v5, "numCache"

    invoke-direct {v3, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 39
    .local v3, "numcacheSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/4 v5, 0x1

    aput-object v3, v4, v5

    .line 41
    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumRefHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 42
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 47
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 48
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumRefHandler;->init()V

    .line 49
    return-void
.end method
