.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentSetParser;
.super Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;
.source "XDocumentSetParser.java"


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;)V
    .locals 0
    .param p1, "consumer"    # Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;)V

    .line 12
    return-void
.end method


# virtual methods
.method public createRootElementHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentSetHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentSetHandler;-><init>()V

    return-object v0
.end method

.method public getMainNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .locals 1

    .prologue
    .line 21
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentSetParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v0

    return-object v0
.end method
