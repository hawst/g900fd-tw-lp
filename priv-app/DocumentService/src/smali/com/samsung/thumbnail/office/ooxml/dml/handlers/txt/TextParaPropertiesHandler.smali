.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "TextParaPropertiesHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler$ITextFontConsumer;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/LineSpacingHandler$ILineSpacingConsumer;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/SpacingAfterHandler$ISpacingAfterConsumer;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler$ITxtCharPropConsumer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;
    }
.end annotation


# instance fields
.field private consumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;

.field private defParaProps:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

.field private paraProp:Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;)V
    .locals 8
    .param p1, "elementName"    # Ljava/lang/String;
    .param p2, "shInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "consumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;

    .prologue
    .line 38
    const/16 v7, 0x1f

    invoke-direct {p0, v7, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 39
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;

    .line 41
    const/4 v7, 0x3

    new-array v4, v7, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 43
    .local v4, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;

    const-string/jumbo v7, "defRPr"

    invoke-direct {v0, v7, p2, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler$ITxtCharPropConsumer;)V

    .line 45
    .local v0, "defRprHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v7, "defRPr"

    invoke-direct {v1, v7, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 47
    .local v1, "defRprSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v7, 0x0

    aput-object v1, v4, v7

    .line 49
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/LineSpacingHandler;

    invoke-direct {v2, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/LineSpacingHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/LineSpacingHandler$ILineSpacingConsumer;)V

    .line 50
    .local v2, "lnSpcHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/LineSpacingHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v7, "lnSpc"

    invoke-direct {v3, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 52
    .local v3, "lnSpcSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v7, 0x1

    aput-object v3, v4, v7

    .line 54
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/SpacingAfterHandler;

    invoke-direct {v5, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/SpacingAfterHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/SpacingAfterHandler$ISpacingAfterConsumer;)V

    .line 55
    .local v5, "spcAftHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/SpacingAfterHandler;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v7, "spcAft"

    invoke-direct {v6, v7, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 57
    .local v6, "spcAftSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v7, 0x2

    aput-object v6, v4, v7

    .line 65
    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 66
    return-void
.end method


# virtual methods
.method public consumeCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V
    .locals 1
    .param p1, "charProps"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->paraProp:Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->setCharProperty(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V

    .line 174
    return-void
.end method

.method public consumeFont(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 0
    .param p1, "typeFace"    # Ljava/lang/String;
    .param p2, "fontFamily"    # Ljava/lang/String;
    .param p3, "charSetFamily"    # Ljava/lang/String;
    .param p4, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 185
    return-void
.end method

.method public consumeLineSpacing(F)V
    .locals 1
    .param p1, "spacing"    # F

    .prologue
    .line 189
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->defParaProps:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->setLineSpacing(F)V

    .line 190
    return-void
.end method

.method public consumeSpacingAfter(F)V
    .locals 1
    .param p1, "spaceAfter"    # F

    .prologue
    .line 194
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->defParaProps:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->setSpaceAfter(F)V

    .line 195
    return-void
.end method

.method public getAlign(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 150
    const-string/jumbo v0, "ctr"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    const-string/jumbo p1, "center"

    .line 159
    :goto_0
    return-object p1

    .line 152
    :cond_0
    const-string/jumbo v0, "l"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 153
    const-string/jumbo p1, "left"

    goto :goto_0

    .line 154
    :cond_1
    const-string/jumbo v0, "r"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 155
    const-string/jumbo p1, "right"

    goto :goto_0

    .line 157
    :cond_2
    const-string/jumbo p1, "justify"

    goto :goto_0
.end method

.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 164
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 165
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;

    if-nez v0, :cond_0

    .line 169
    :goto_0
    return-void

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->getElementName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->paraProp:Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;

    invoke-interface {v0, v1, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;->setPropertiesForLvl(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V

    goto :goto_0
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 6
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 111
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 113
    invoke-virtual {p0, p2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "buChar"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 115
    const-string/jumbo v4, "char"

    invoke-virtual {p0, p3, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 117
    .local v3, "val":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 118
    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 119
    .local v1, "charArray":[C
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v1

    if-ge v2, v4, :cond_1

    .line 120
    aget-char v4, v1, v2

    invoke-static {v4}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    .line 121
    .local v0, "ch":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/thumbnail/customview/CustomUtils;->getDecimalCodeForBullet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 122
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->defParaProps:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-static {v0}, Lcom/samsung/thumbnail/customview/CustomUtils;->getDecimalCodeForBullet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->setBulletChar(Ljava/lang/String;)V

    .line 119
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 125
    :cond_0
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->defParaProps:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v4, v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->setBulletChar(Ljava/lang/String;)V

    goto :goto_1

    .line 131
    .end local v0    # "ch":Ljava/lang/String;
    .end local v1    # "charArray":[C
    .end local v2    # "i":I
    .end local v3    # "val":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0, p2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "buAutoNum"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 133
    const-string/jumbo v4, "type"

    invoke-virtual {p0, p3, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 134
    .restart local v3    # "val":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->defParaProps:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-static {v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->setBuAutoNum(Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;)V

    .line 137
    .end local v3    # "val":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0, p2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "buFont"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 141
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->defParaProps:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->setBullet()V

    .line 143
    :cond_3
    invoke-virtual {p0, p2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "buNone"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 145
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->defParaProps:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->setBuNone()V

    .line 147
    :cond_4
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 7
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 71
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;

    invoke-direct {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;-><init>()V

    iput-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->paraProp:Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;

    .line 72
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-direct {v5}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;-><init>()V

    iput-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->defParaProps:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    .line 73
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->paraProp:Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;

    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->defParaProps:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v5, v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->setParaProperty(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V

    .line 75
    const-string/jumbo v5, "algn"

    invoke-virtual {p0, p3, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 76
    .local v4, "val":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 77
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->defParaProps:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {p0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->getAlign(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->setJustify(Ljava/lang/String;)V

    .line 80
    :cond_0
    const-string/jumbo v5, "lvl"

    invoke-virtual {p0, p3, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 81
    if-eqz v4, :cond_1

    .line 82
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->defParaProps:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->setLevel(I)V

    .line 85
    :cond_1
    const-string/jumbo v5, "marL"

    invoke-virtual {p0, p3, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 86
    if-eqz v4, :cond_2

    .line 87
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints(I)I

    move-result v2

    .line 88
    .local v2, "marL":I
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->defParaProps:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v5, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->setMarginLeft(I)V

    .line 90
    .end local v2    # "marL":I
    :cond_2
    const-string/jumbo v5, "marR"

    invoke-virtual {p0, p3, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 91
    if-eqz v4, :cond_3

    .line 92
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints(I)I

    move-result v3

    .line 93
    .local v3, "marR":I
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->defParaProps:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v5, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->setMarginRight(I)V

    .line 95
    .end local v3    # "marR":I
    :cond_3
    const-string/jumbo v5, "indent"

    invoke-virtual {p0, p3, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 96
    if-eqz v4, :cond_4

    .line 97
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints(I)I

    move-result v1

    .line 98
    .local v1, "indent":I
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->defParaProps:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v5, v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->setIndent_PPT(I)V

    .line 101
    .end local v1    # "indent":I
    :cond_4
    const-string/jumbo v5, "defTabSz"

    invoke-virtual {p0, p3, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 102
    if-eqz v4, :cond_5

    .line 103
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints(I)I

    move-result v0

    .line 104
    .local v0, "defTabSz":I
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;->defParaProps:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v5, v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->setDefTabSize(I)V

    .line 106
    .end local v0    # "defTabSz":I
    :cond_5
    return-void
.end method
