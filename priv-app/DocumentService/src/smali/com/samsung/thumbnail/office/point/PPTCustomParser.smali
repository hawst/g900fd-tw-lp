.class public Lcom/samsung/thumbnail/office/point/PPTCustomParser;
.super Ljava/lang/Object;
.source "PPTCustomParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/point/PPTCustomParser$1;,
        Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;,
        Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;
    }
.end annotation


# static fields
.field private static final FONT_SIZE_FRACTION_HACK:F = 0.9f

.field private static final OPTIMUMVALUE:F = 1587.5f

.field private static final TAG:Ljava/lang/String; = "PPTParser"

.field private static final arc:I = 0x4

.field private static final arcTo:I = 0x3

.field private static final clockwiseArc:I = 0x6

.field private static final clockwiseArcTo:I = 0x5

.field private static count:I = 0x0

.field private static final pathClose:I = 0x3

.field private static final pathCurveTo:I = 0x1

.field private static final pathEnd:I = 0x4

.field private static final pathEscape:I = 0x5

.field private static final pathLineTo:I = 0x0

.field private static final pathMoveTo:I = 0x2

.field private static final quadraticBezier:I = 0x9


# instance fields
.field private bitmap:Landroid/graphics/Bitmap;

.field private canvas:Landroid/graphics/Canvas;

.field public currentSlideNumber:I

.field private customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

.field private elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

.field private folderPath:Ljava/io/File;

.field private lineDrakValue_1:I

.field private lineDrakValue_2:I

.field private lineLightValue:I

.field private mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

.field public mContext:Landroid/content/Context;

.field private mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

.field private out:Ljava/io/FileOutputStream;

.field private shape_Count:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    sput v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->count:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "showOnlyFirstSlide"    # Z

    .prologue
    const/4 v1, 0x0

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput v1, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    .line 103
    iput v1, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    .line 104
    iput v1, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_2:I

    .line 105
    iput v1, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    .line 125
    :try_start_0
    iput-object p1, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->mContext:Landroid/content/Context;

    .line 126
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    :goto_0
    return-void

    .line 131
    :catch_0
    move-exception v0

    .line 132
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "PPTParser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Exception in constructor"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private addShapeToHTMLCreater(Lorg/apache/poi/hslf/model/AutoShape;)V
    .locals 8
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;

    .prologue
    .line 2395
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/AutoShape;->getAnchor()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v7

    .line 2396
    .local v7, "anchor":Lorg/apache/poi/java/awt/Rectangle;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "/data/data/com.siso.office/files/pic_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/AutoShape;->getShapeName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".png"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2399
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    iget v1, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    iget v3, v7, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v4, v7, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v5, v7, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v6, v7, Lorg/apache/poi/java/awt/Rectangle;->height:I

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawBitMap(ILandroid/graphics/Bitmap;IIII)V

    .line 2402
    iget v0, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    .line 2403
    return-void
.end method

.method private checkLineSpace(Lorg/apache/poi/hslf/model/Line;)V
    .locals 20
    .param p1, "line"    # Lorg/apache/poi/hslf/model/Line;

    .prologue
    .line 1094
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getAnchor()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v19

    .line 1095
    .local v19, "rec":Lorg/apache/poi/java/awt/Rectangle;
    const-wide/16 v16, 0x0

    .local v16, "lineWidth":D
    const-wide/16 v14, 0x0

    .line 1096
    .local v14, "lineHeight":D
    const/4 v11, 0x0

    .line 1098
    .local v11, "isDark":Z
    move-object/from16 v0, v19

    iget v2, v0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    int-to-float v2, v2

    const v3, 0x44c67000    # 1587.5f

    cmpl-float v2, v2, v3

    if-gez v2, :cond_0

    move-object/from16 v0, v19

    iget v2, v0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    int-to-float v2, v2

    const v3, 0x44c67000    # 1587.5f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_1

    .line 1100
    :cond_0
    move-object/from16 v0, v19

    iget v2, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->getOriginalValue(F)F

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints1(F)F

    move-result v2

    float-to-int v2, v2

    move-object/from16 v0, v19

    iput v2, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    .line 1101
    move-object/from16 v0, v19

    iget v2, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->getOriginalValue(F)F

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints1(F)F

    move-result v2

    float-to-int v2, v2

    move-object/from16 v0, v19

    iput v2, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    .line 1102
    move-object/from16 v0, v19

    iget v2, v0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->getOriginalValue(F)F

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints1(F)F

    move-result v2

    float-to-int v2, v2

    move-object/from16 v0, v19

    iput v2, v0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    .line 1104
    move-object/from16 v0, v19

    iget v2, v0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->getOriginalValue(F)F

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints1(F)F

    move-result v2

    float-to-int v2, v2

    move-object/from16 v0, v19

    iput v2, v0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    .line 1108
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineDashing()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 1973
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1979
    :cond_2
    :goto_0
    return-void

    .line 1110
    :pswitch_0
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    .line 1111
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    .line 1112
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_4

    .line 1113
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v16

    .line 1114
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v14

    .line 1122
    :goto_1
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_8

    .line 1123
    const-wide/16 v12, 0x0

    .local v12, "i":D
    :goto_2
    cmpg-double v2, v12, v16

    if-gez v2, :cond_2

    .line 1124
    if-eqz v11, :cond_6

    .line 1125
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1126
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v4, v3

    add-double/2addr v4, v12

    double-to-int v3, v4

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    move-object/from16 v0, v19

    iget v5, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v6, v5

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v8, v5

    add-double/2addr v6, v8

    double-to-int v5, v6

    move-object/from16 v0, v19

    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1136
    :cond_3
    const/4 v11, 0x0

    .line 1123
    :goto_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    goto :goto_2

    .line 1115
    .end local v12    # "i":D
    :cond_4
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_5

    .line 1116
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v16

    .line 1117
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v14

    goto :goto_1

    .line 1119
    :cond_5
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v16

    .line 1120
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v14

    goto :goto_1

    .line 1138
    .restart local v12    # "i":D
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 1139
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v4, v3

    add-double/2addr v4, v12

    double-to-int v3, v4

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    move-object/from16 v0, v19

    iget v5, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v6, v5

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v8, v5

    add-double/2addr v6, v8

    double-to-int v5, v6

    move-object/from16 v0, v19

    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1149
    :cond_7
    const/4 v11, 0x1

    goto :goto_3

    .line 1152
    .end local v12    # "i":D
    :cond_8
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_c

    .line 1153
    const-wide/16 v12, 0x0

    .restart local v12    # "i":D
    :goto_4
    cmpg-double v2, v12, v14

    if-gez v2, :cond_2

    .line 1154
    if-eqz v11, :cond_a

    .line 1155
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 1156
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v4, v4

    add-double/2addr v4, v12

    double-to-int v4, v4

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    move-object/from16 v0, v19

    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v6, v6

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v8, v8

    add-double/2addr v6, v8

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1166
    :cond_9
    const/4 v11, 0x0

    .line 1153
    :goto_5
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    goto :goto_4

    .line 1168
    :cond_a
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_b

    .line 1169
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v4, v4

    add-double/2addr v4, v12

    double-to-int v4, v4

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    move-object/from16 v0, v19

    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v6, v6

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v8, v8

    add-double/2addr v6, v8

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1179
    :cond_b
    const/4 v11, 0x1

    goto :goto_5

    .line 1183
    .end local v12    # "i":D
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    goto/16 :goto_0

    .line 1192
    :pswitch_1
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    .line 1193
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_2:I

    .line 1194
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    .line 1195
    const/16 v18, 0x0

    .line 1196
    .local v18, "longDash":Z
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_e

    .line 1197
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v16

    .line 1198
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v14

    .line 1206
    :goto_6
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_13

    .line 1207
    const-wide/16 v12, 0x0

    .restart local v12    # "i":D
    :goto_7
    cmpg-double v2, v12, v16

    if-gez v2, :cond_2

    .line 1208
    if-eqz v11, :cond_10

    .line 1209
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_d

    .line 1210
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v4, v3

    add-double/2addr v4, v12

    double-to-int v3, v4

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    move-object/from16 v0, v19

    iget v5, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v6, v5

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v8, v5

    add-double/2addr v6, v8

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1221
    :cond_d
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1222
    const/4 v11, 0x0

    goto :goto_7

    .line 1199
    .end local v12    # "i":D
    :cond_e
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_f

    .line 1200
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v16

    .line 1201
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v14

    goto :goto_6

    .line 1203
    :cond_f
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v16

    .line 1204
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v14

    goto :goto_6

    .line 1224
    .restart local v12    # "i":D
    :cond_10
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_11

    .line 1225
    if-eqz v18, :cond_12

    .line 1226
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v4, v3

    add-double/2addr v4, v12

    double-to-int v3, v4

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    move-object/from16 v0, v19

    iget v5, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v6, v5

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_2:I

    int-to-double v8, v5

    add-double/2addr v6, v8

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1235
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1236
    const/16 v18, 0x0

    .line 1251
    :cond_11
    :goto_8
    const/4 v11, 0x1

    goto/16 :goto_7

    .line 1238
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v4, v3

    add-double/2addr v4, v12

    double-to-int v3, v4

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    move-object/from16 v0, v19

    iget v5, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v6, v5

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v8, v5

    add-double/2addr v6, v8

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1247
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_2:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1248
    const/16 v18, 0x1

    goto :goto_8

    .line 1254
    .end local v12    # "i":D
    :cond_13
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_18

    .line 1255
    const-wide/16 v12, 0x0

    .restart local v12    # "i":D
    :cond_14
    :goto_9
    cmpg-double v2, v12, v14

    if-gez v2, :cond_2

    .line 1256
    if-eqz v11, :cond_15

    .line 1257
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_14

    .line 1258
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v4, v4

    add-double/2addr v4, v12

    double-to-int v4, v4

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    move-object/from16 v0, v19

    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v6, v6

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v8, v8

    add-double/2addr v6, v8

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1267
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1268
    const/4 v11, 0x0

    goto :goto_9

    .line 1271
    :cond_15
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_16

    .line 1272
    if-eqz v18, :cond_17

    .line 1273
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v4, v4

    add-double/2addr v4, v12

    double-to-int v4, v4

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    move-object/from16 v0, v19

    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v6, v6

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_2:I

    int-to-double v8, v8

    add-double/2addr v6, v8

    double-to-int v6, v6

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1282
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1283
    const/16 v18, 0x0

    .line 1298
    :cond_16
    :goto_a
    const/4 v11, 0x1

    goto/16 :goto_9

    .line 1285
    :cond_17
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v4, v4

    add-double/2addr v4, v12

    double-to-int v4, v4

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    move-object/from16 v0, v19

    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v6, v6

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v8, v8

    add-double/2addr v6, v8

    double-to-int v6, v6

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1294
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_2:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1295
    const/16 v18, 0x1

    goto :goto_a

    .line 1302
    .end local v12    # "i":D
    :cond_18
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    goto/16 :goto_0

    .line 1311
    .end local v18    # "longDash":Z
    :pswitch_2
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    .line 1312
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_2:I

    .line 1313
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    .line 1314
    const/16 v18, 0x0

    .line 1315
    .local v18, "longDash":I
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_1a

    .line 1316
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v16

    .line 1317
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v14

    .line 1325
    :goto_b
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_20

    .line 1326
    const-wide/16 v12, 0x0

    .restart local v12    # "i":D
    :cond_19
    :goto_c
    cmpg-double v2, v12, v16

    if-gez v2, :cond_2

    .line 1327
    if-eqz v11, :cond_1c

    .line 1328
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_19

    .line 1329
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v4, v3

    add-double/2addr v4, v12

    double-to-int v3, v4

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    move-object/from16 v0, v19

    iget v5, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v6, v5

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v8, v5

    add-double/2addr v6, v8

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1339
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1340
    const/4 v11, 0x0

    goto :goto_c

    .line 1318
    .end local v12    # "i":D
    :cond_1a
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_1b

    .line 1319
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v16

    .line 1320
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v14

    goto :goto_b

    .line 1322
    :cond_1b
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v16

    .line 1323
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v14

    goto :goto_b

    .line 1343
    .restart local v12    # "i":D
    :cond_1c
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_1d

    .line 1344
    if-nez v18, :cond_1e

    .line 1345
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v4, v3

    add-double/2addr v4, v12

    double-to-int v3, v4

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    move-object/from16 v0, v19

    iget v5, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v6, v5

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v8, v5

    add-double/2addr v6, v8

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1354
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1355
    const/16 v18, 0x1

    .line 1356
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v4, v3

    add-double/2addr v4, v12

    double-to-int v3, v4

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    move-object/from16 v0, v19

    iget v5, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v6, v5

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v8, v5

    add-double/2addr v6, v8

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1382
    :cond_1d
    :goto_d
    const/4 v11, 0x1

    goto/16 :goto_c

    .line 1365
    :cond_1e
    const/4 v2, 0x1

    move/from16 v0, v18

    if-ne v0, v2, :cond_1f

    .line 1366
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_2:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1367
    const/16 v18, 0x2

    goto :goto_d

    .line 1369
    :cond_1f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v4, v3

    add-double/2addr v4, v12

    double-to-int v3, v4

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    move-object/from16 v0, v19

    iget v5, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v6, v5

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v8, v5

    add-double/2addr v6, v8

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1378
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_2:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1379
    const/16 v18, 0x0

    goto :goto_d

    .line 1385
    .end local v12    # "i":D
    :cond_20
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_26

    .line 1386
    const-wide/16 v12, 0x0

    .restart local v12    # "i":D
    :cond_21
    :goto_e
    cmpg-double v2, v12, v14

    if-gez v2, :cond_2

    .line 1387
    if-eqz v11, :cond_22

    .line 1388
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_21

    .line 1389
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v4, v4

    add-double/2addr v4, v12

    double-to-int v4, v4

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    move-object/from16 v0, v19

    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v6, v6

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v8, v8

    add-double/2addr v6, v8

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1398
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1399
    const/4 v11, 0x0

    goto :goto_e

    .line 1402
    :cond_22
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_23

    .line 1403
    if-nez v18, :cond_24

    .line 1404
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v4, v4

    add-double/2addr v4, v12

    double-to-int v4, v4

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    move-object/from16 v0, v19

    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v6, v6

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v8, v8

    add-double/2addr v6, v8

    double-to-int v6, v6

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1413
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1414
    const/16 v18, 0x1

    .line 1441
    :cond_23
    :goto_f
    const/4 v11, 0x1

    goto/16 :goto_e

    .line 1415
    :cond_24
    const/4 v2, 0x1

    move/from16 v0, v18

    if-ne v0, v2, :cond_25

    .line 1416
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v4, v4

    add-double/2addr v4, v12

    double-to-int v4, v4

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    move-object/from16 v0, v19

    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v6, v6

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v8, v8

    add-double/2addr v6, v8

    double-to-int v6, v6

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1425
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_2:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1426
    const/16 v18, 0x2

    goto :goto_f

    .line 1428
    :cond_25
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v4, v4

    add-double/2addr v4, v12

    double-to-int v4, v4

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    move-object/from16 v0, v19

    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v6, v6

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v8, v8

    add-double/2addr v6, v8

    double-to-int v6, v6

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1437
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_2:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1438
    const/16 v18, 0x0

    goto :goto_f

    .line 1445
    .end local v12    # "i":D
    :cond_26
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    goto/16 :goto_0

    .line 1454
    .end local v18    # "longDash":I
    :pswitch_3
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    .line 1455
    const/4 v2, 0x5

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    .line 1456
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_28

    .line 1457
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v16

    .line 1458
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v14

    .line 1466
    :goto_10
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_2b

    .line 1467
    const-wide/16 v12, 0x0

    .restart local v12    # "i":D
    :cond_27
    :goto_11
    cmpg-double v2, v12, v16

    if-gez v2, :cond_2

    .line 1468
    if-eqz v11, :cond_2a

    .line 1469
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_27

    .line 1470
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v4, v3

    add-double/2addr v4, v12

    double-to-int v3, v4

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    move-object/from16 v0, v19

    iget v5, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v6, v5

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v8, v5

    add-double/2addr v6, v8

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1480
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1481
    const/4 v11, 0x0

    goto :goto_11

    .line 1459
    .end local v12    # "i":D
    :cond_28
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_29

    .line 1460
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v16

    .line 1461
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v14

    goto :goto_10

    .line 1463
    :cond_29
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v16

    .line 1464
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v14

    goto :goto_10

    .line 1484
    .restart local v12    # "i":D
    :cond_2a
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_27

    .line 1485
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v4, v3

    add-double/2addr v4, v12

    double-to-int v3, v4

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    move-object/from16 v0, v19

    iget v5, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v6, v5

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v8, v5

    add-double/2addr v6, v8

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1494
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1495
    const/4 v11, 0x1

    goto/16 :goto_11

    .line 1499
    .end local v12    # "i":D
    :cond_2b
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_2e

    .line 1500
    const-wide/16 v12, 0x0

    .restart local v12    # "i":D
    :cond_2c
    :goto_12
    cmpg-double v2, v12, v14

    if-gez v2, :cond_2

    .line 1501
    if-eqz v11, :cond_2d

    .line 1502
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_2c

    .line 1503
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v4, v4

    add-double/2addr v4, v12

    double-to-int v4, v4

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    move-object/from16 v0, v19

    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v6, v6

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v8, v8

    add-double/2addr v6, v8

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1512
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1513
    const/4 v11, 0x0

    goto :goto_12

    .line 1516
    :cond_2d
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_2c

    .line 1517
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v4, v4

    add-double/2addr v4, v12

    double-to-int v4, v4

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    move-object/from16 v0, v19

    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v6, v6

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v8, v8

    add-double/2addr v6, v8

    double-to-int v6, v6

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1526
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1527
    const/4 v11, 0x1

    goto/16 :goto_12

    .line 1532
    .end local v12    # "i":D
    :cond_2e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    goto/16 :goto_0

    .line 1541
    :pswitch_4
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    .line 1542
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    .line 1543
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_30

    .line 1544
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v16

    .line 1545
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v14

    .line 1555
    :goto_13
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_34

    .line 1556
    const-wide/16 v12, 0x0

    .restart local v12    # "i":D
    :goto_14
    cmpg-double v2, v12, v16

    if-gez v2, :cond_2

    .line 1557
    if-eqz v11, :cond_32

    .line 1558
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_2f

    .line 1559
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v4, v3

    add-double/2addr v4, v12

    double-to-int v3, v4

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    move-object/from16 v0, v19

    iget v5, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v6, v5

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v8, v5

    add-double/2addr v6, v8

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1570
    :cond_2f
    const/4 v11, 0x0

    .line 1556
    :goto_15
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    goto :goto_14

    .line 1546
    .end local v12    # "i":D
    :cond_30
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_31

    .line 1547
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v16

    .line 1548
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v14

    goto :goto_13

    .line 1550
    :cond_31
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v16

    .line 1551
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v14

    goto :goto_13

    .line 1572
    .restart local v12    # "i":D
    :cond_32
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_33

    .line 1573
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v4, v3

    add-double/2addr v4, v12

    double-to-int v3, v4

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    move-object/from16 v0, v19

    iget v5, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v6, v5

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v8, v5

    add-double/2addr v6, v8

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1583
    :cond_33
    const/4 v11, 0x1

    goto :goto_15

    .line 1586
    .end local v12    # "i":D
    :cond_34
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_38

    .line 1587
    const-wide/16 v12, 0x0

    .restart local v12    # "i":D
    :goto_16
    cmpg-double v2, v12, v14

    if-gez v2, :cond_2

    .line 1588
    if-eqz v11, :cond_36

    .line 1589
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_35

    .line 1590
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v4, v4

    add-double/2addr v4, v12

    double-to-int v4, v4

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    move-object/from16 v0, v19

    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v6, v6

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v8, v8

    add-double/2addr v6, v8

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1600
    :cond_35
    const/4 v11, 0x0

    .line 1587
    :goto_17
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    goto :goto_16

    .line 1602
    :cond_36
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_37

    .line 1603
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v4, v4

    add-double/2addr v4, v12

    double-to-int v4, v4

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    move-object/from16 v0, v19

    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v6, v6

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v8, v8

    add-double/2addr v6, v8

    double-to-int v6, v6

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1613
    :cond_37
    const/4 v11, 0x1

    goto :goto_17

    .line 1617
    .end local v12    # "i":D
    :cond_38
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    goto/16 :goto_0

    .line 1642
    :pswitch_5
    const/16 v2, 0x8

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    .line 1643
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    .line 1644
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_3a

    .line 1645
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v16

    .line 1646
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v14

    .line 1654
    :goto_18
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_3e

    .line 1655
    const-wide/16 v12, 0x0

    .restart local v12    # "i":D
    :goto_19
    cmpg-double v2, v12, v16

    if-gez v2, :cond_2

    .line 1656
    if-eqz v11, :cond_3c

    .line 1657
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_39

    .line 1658
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v4, v3

    add-double/2addr v4, v12

    double-to-int v3, v4

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    move-object/from16 v0, v19

    iget v5, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v6, v5

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v8, v5

    add-double/2addr v6, v8

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1669
    :cond_39
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1670
    const/4 v11, 0x0

    goto :goto_19

    .line 1647
    .end local v12    # "i":D
    :cond_3a
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_3b

    .line 1648
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v16

    .line 1649
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v14

    goto :goto_18

    .line 1651
    :cond_3b
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v16

    .line 1652
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v14

    goto :goto_18

    .line 1672
    .restart local v12    # "i":D
    :cond_3c
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_3d

    .line 1673
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v4, v3

    add-double/2addr v4, v12

    double-to-int v3, v4

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    move-object/from16 v0, v19

    iget v5, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v6, v5

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v8, v5

    add-double/2addr v6, v8

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1683
    :cond_3d
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1684
    const/4 v11, 0x1

    goto/16 :goto_19

    .line 1687
    .end local v12    # "i":D
    :cond_3e
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_42

    .line 1688
    const-wide/16 v12, 0x0

    .restart local v12    # "i":D
    :goto_1a
    cmpg-double v2, v12, v14

    if-gez v2, :cond_2

    .line 1689
    if-eqz v11, :cond_40

    .line 1690
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_3f

    .line 1691
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v4, v4

    add-double/2addr v4, v12

    double-to-int v4, v4

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    move-object/from16 v0, v19

    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v6, v6

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v8, v8

    add-double/2addr v6, v8

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1701
    :cond_3f
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1702
    const/4 v11, 0x0

    goto :goto_1a

    .line 1704
    :cond_40
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_41

    .line 1705
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v4, v4

    add-double/2addr v4, v12

    double-to-int v4, v4

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    move-object/from16 v0, v19

    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v6, v6

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v8, v8

    add-double/2addr v6, v8

    double-to-int v6, v6

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1715
    :cond_41
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1716
    const/4 v11, 0x1

    goto/16 :goto_1a

    .line 1720
    .end local v12    # "i":D
    :cond_42
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    goto/16 :goto_0

    .line 1729
    :pswitch_6
    const/4 v2, 0x5

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    .line 1730
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_2:I

    .line 1731
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    .line 1732
    const/16 v18, 0x0

    .line 1733
    .local v18, "longDash":Z
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_44

    .line 1734
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v16

    .line 1735
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v14

    .line 1743
    :goto_1b
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_49

    .line 1744
    const-wide/16 v12, 0x0

    .restart local v12    # "i":D
    :goto_1c
    cmpg-double v2, v12, v16

    if-gez v2, :cond_2

    .line 1745
    if-eqz v11, :cond_46

    .line 1746
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_43

    .line 1747
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v4, v3

    add-double/2addr v4, v12

    double-to-int v3, v4

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    move-object/from16 v0, v19

    iget v5, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v6, v5

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v8, v5

    add-double/2addr v6, v8

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1757
    :cond_43
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1758
    const/4 v11, 0x0

    goto :goto_1c

    .line 1736
    .end local v12    # "i":D
    :cond_44
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_45

    .line 1737
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v16

    .line 1738
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v14

    goto :goto_1b

    .line 1740
    :cond_45
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v16

    .line 1741
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v14

    goto :goto_1b

    .line 1760
    .restart local v12    # "i":D
    :cond_46
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_47

    .line 1761
    if-eqz v18, :cond_48

    .line 1762
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v4, v3

    add-double/2addr v4, v12

    double-to-int v3, v4

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    move-object/from16 v0, v19

    iget v5, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v6, v5

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v8, v5

    add-double/2addr v6, v8

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    double-to-int v8, v8

    int-to-double v8, v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1772
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1773
    const/16 v18, 0x0

    .line 1789
    :cond_47
    :goto_1d
    const/4 v11, 0x1

    goto/16 :goto_1c

    .line 1775
    :cond_48
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v4, v3

    add-double/2addr v4, v12

    double-to-int v3, v4

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    move-object/from16 v0, v19

    iget v5, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v6, v5

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_2:I

    int-to-double v8, v5

    add-double/2addr v6, v8

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    double-to-int v8, v8

    int-to-double v8, v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1785
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_2:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1786
    const/16 v18, 0x1

    goto :goto_1d

    .line 1792
    .end local v12    # "i":D
    :cond_49
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_4e

    .line 1793
    const-wide/16 v12, 0x0

    .restart local v12    # "i":D
    :goto_1e
    cmpg-double v2, v12, v14

    if-gez v2, :cond_2

    .line 1794
    if-eqz v11, :cond_4b

    .line 1795
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_4a

    .line 1796
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v4, v4

    add-double/2addr v4, v12

    double-to-int v4, v4

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    move-object/from16 v0, v19

    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v6, v6

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v8, v8

    add-double/2addr v6, v8

    double-to-int v6, v6

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1806
    :cond_4a
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1807
    const/4 v11, 0x0

    goto :goto_1e

    .line 1809
    :cond_4b
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_4c

    .line 1810
    if-eqz v18, :cond_4d

    .line 1811
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v4, v4

    add-double/2addr v4, v12

    double-to-int v4, v4

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    move-object/from16 v0, v19

    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v6, v6

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v8, v8

    add-double/2addr v6, v8

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1821
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1822
    const/16 v18, 0x0

    .line 1838
    :cond_4c
    :goto_1f
    const/4 v11, 0x1

    goto/16 :goto_1e

    .line 1824
    :cond_4d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v4, v4

    add-double/2addr v4, v12

    double-to-int v4, v4

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    move-object/from16 v0, v19

    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v6, v6

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_2:I

    int-to-double v8, v8

    add-double/2addr v6, v8

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1834
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_2:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1835
    const/16 v18, 0x1

    goto :goto_1f

    .line 1842
    .end local v12    # "i":D
    :cond_4e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    goto/16 :goto_0

    .line 1851
    .end local v18    # "longDash":Z
    :pswitch_7
    const/16 v2, 0x8

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    .line 1852
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_2:I

    .line 1853
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    .line 1854
    const/16 v18, 0x0

    .line 1855
    .restart local v18    # "longDash":Z
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_50

    .line 1856
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v16

    .line 1857
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v14

    .line 1865
    :goto_20
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_55

    .line 1866
    const-wide/16 v12, 0x0

    .restart local v12    # "i":D
    :goto_21
    cmpg-double v2, v12, v16

    if-gez v2, :cond_2

    .line 1867
    if-eqz v11, :cond_52

    .line 1868
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_4f

    .line 1869
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v4, v3

    add-double/2addr v4, v12

    double-to-int v3, v4

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    move-object/from16 v0, v19

    iget v5, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v6, v5

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v8, v5

    add-double/2addr v6, v8

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1879
    :cond_4f
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1880
    const/4 v11, 0x0

    goto :goto_21

    .line 1858
    .end local v12    # "i":D
    :cond_50
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_51

    .line 1859
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v16

    .line 1860
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v14

    goto :goto_20

    .line 1862
    :cond_51
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v16

    .line 1863
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v14

    goto :goto_20

    .line 1882
    .restart local v12    # "i":D
    :cond_52
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_53

    .line 1883
    if-eqz v18, :cond_54

    .line 1884
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v4, v3

    add-double/2addr v4, v12

    double-to-int v3, v4

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    move-object/from16 v0, v19

    iget v5, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v6, v5

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v8, v5

    add-double/2addr v6, v8

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    double-to-int v8, v8

    int-to-double v8, v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1894
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1895
    const/16 v18, 0x0

    .line 1911
    :cond_53
    :goto_22
    const/4 v11, 0x1

    goto/16 :goto_21

    .line 1897
    :cond_54
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v4, v3

    add-double/2addr v4, v12

    double-to-int v3, v4

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    move-object/from16 v0, v19

    iget v5, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v6, v5

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_2:I

    int-to-double v8, v5

    add-double/2addr v6, v8

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    double-to-int v8, v8

    int-to-double v8, v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1907
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_2:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1908
    const/16 v18, 0x1

    goto :goto_22

    .line 1914
    .end local v12    # "i":D
    :cond_55
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_5a

    .line 1915
    const-wide/16 v12, 0x0

    .restart local v12    # "i":D
    :goto_23
    cmpg-double v2, v12, v14

    if-gez v2, :cond_2

    .line 1916
    if-eqz v11, :cond_57

    .line 1917
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_56

    .line 1918
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v4, v4

    add-double/2addr v4, v12

    double-to-int v4, v4

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    move-object/from16 v0, v19

    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v6, v6

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v8, v8

    add-double/2addr v6, v8

    double-to-int v6, v6

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1928
    :cond_56
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineLightValue:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1929
    const/4 v11, 0x0

    goto :goto_23

    .line 1931
    :cond_57
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_58

    .line 1932
    if-eqz v18, :cond_59

    .line 1933
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v4, v4

    add-double/2addr v4, v12

    double-to-int v4, v4

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    move-object/from16 v0, v19

    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v6, v6

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v8, v8

    add-double/2addr v6, v8

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1943
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_1:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1944
    const/16 v18, 0x0

    .line 1960
    :cond_58
    :goto_24
    const/4 v11, 0x1

    goto/16 :goto_23

    .line 1946
    :cond_59
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v4, v4

    add-double/2addr v4, v12

    double-to-int v4, v4

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    move-object/from16 v0, v19

    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v6, v6

    add-double/2addr v6, v12

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_2:I

    int-to-double v8, v8

    add-double/2addr v6, v8

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    .line 1956
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->lineDrakValue_2:I

    int-to-double v2, v2

    add-double/2addr v12, v2

    .line 1957
    const/16 v18, 0x1

    goto :goto_24

    .line 1964
    .end local v12    # "i":D
    :cond_5a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, v19

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxX()D

    move-result-wide v6

    double-to-int v5, v6

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Rectangle;->getMaxY()D

    move-result-wide v6

    double-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawLine(IIIIIDI)V

    goto/16 :goto_0

    .line 1108
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private createAutoShapes(Lorg/apache/poi/hslf/model/Shape;ZLorg/apache/poi/hslf/model/HeadersFooters;)V
    .locals 17
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/Shape;
    .param p2, "masterFlag"    # Z
    .param p3, "hdrFtrSet"    # Lorg/apache/poi/hslf/model/HeadersFooters;

    .prologue
    .line 2154
    move-object/from16 v4, p1

    check-cast v4, Lorg/apache/poi/hslf/model/AutoShape;

    .line 2155
    .local v4, "autoShape":Lorg/apache/poi/hslf/model/AutoShape;
    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/AutoShape;->getAnchor()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v12

    .line 2156
    .local v12, "anchor":Lorg/apache/poi/java/awt/Rectangle;
    const/4 v2, 0x0

    .line 2158
    .local v2, "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Shape;->getShapeName()Ljava/lang/String;

    move-result-object v16

    .line 2159
    .local v16, "shapeName":Ljava/lang/String;
    if-nez v16, :cond_1

    .line 2386
    :cond_0
    :goto_0
    return-void

    .line 2161
    :cond_1
    if-eqz v12, :cond_2

    .line 2162
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v6, v5

    invoke-virtual {v3, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v6

    double-to-int v3, v6

    iput v3, v12, Lorg/apache/poi/java/awt/Rectangle;->x:I

    .line 2164
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v6, v5

    invoke-virtual {v3, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v6

    double-to-int v3, v6

    iput v3, v12, Lorg/apache/poi/java/awt/Rectangle;->y:I

    .line 2166
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->width:I

    int-to-double v6, v5

    invoke-virtual {v3, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v6

    double-to-int v3, v6

    iput v3, v12, Lorg/apache/poi/java/awt/Rectangle;->width:I

    .line 2168
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->height:I

    int-to-double v6, v5

    invoke-virtual {v3, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v6

    double-to-int v3, v6

    iput v3, v12, Lorg/apache/poi/java/awt/Rectangle;->height:I

    .line 2171
    :cond_2
    const-string/jumbo v3, "Rectangle"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 2172
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v6, v12, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v7, v12, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v8, v12, Lorg/apache/poi/java/awt/Rectangle;->height:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    invoke-direct/range {v2 .. v11}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Lorg/apache/poi/hslf/model/Shape;IIIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Ljava/io/File;)V

    .line 2341
    .restart local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_3
    :goto_1
    if-eqz p2, :cond_2f

    .line 2342
    if-eqz v4, :cond_7

    .line 2344
    if-eqz p3, :cond_4

    :try_start_0
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hslf/model/HeadersFooters;->isSlideNumberVisible()Z

    move-result v3

    if-eqz v3, :cond_4

    const/16 v3, 0x8

    invoke-virtual {v4, v3}, Lorg/apache/poi/hslf/model/AutoShape;->isShapeType(I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2348
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v2}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->readTextFromSlides(Lorg/apache/poi/hslf/model/AutoShape;Lcom/samsung/thumbnail/customview/word/ShapesController;)V

    .line 2351
    :cond_4
    if-eqz p3, :cond_5

    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hslf/model/HeadersFooters;->isHeaderVisible()Z

    move-result v3

    if-eqz v3, :cond_5

    const/16 v3, 0xa

    invoke-virtual {v4, v3}, Lorg/apache/poi/hslf/model/AutoShape;->isShapeType(I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2355
    const-string/jumbo v3, "*"

    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/AutoShape;->getText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 2356
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v2}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->readTextFromSlides(Lorg/apache/poi/hslf/model/AutoShape;Lcom/samsung/thumbnail/customview/word/ShapesController;)V

    .line 2359
    :cond_5
    if-eqz p3, :cond_6

    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hslf/model/HeadersFooters;->isFooterVisible()Z

    move-result v3

    if-eqz v3, :cond_6

    const/16 v3, 0x9

    invoke-virtual {v4, v3}, Lorg/apache/poi/hslf/model/AutoShape;->isShapeType(I)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2364
    const-string/jumbo v3, "*"

    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/AutoShape;->getText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 2365
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v2}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->readTextFromSlides(Lorg/apache/poi/hslf/model/AutoShape;Lcom/samsung/thumbnail/customview/word/ShapesController;)V

    .line 2368
    :cond_6
    if-eqz p3, :cond_7

    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hslf/model/HeadersFooters;->isDateTimeVisible()Z

    move-result v3

    if-eqz v3, :cond_7

    const/4 v3, 0x7

    invoke-virtual {v4, v3}, Lorg/apache/poi/hslf/model/AutoShape;->isShapeType(I)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2373
    const-string/jumbo v3, "*"

    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/AutoShape;->getText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 2374
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v2}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->readTextFromSlides(Lorg/apache/poi/hslf/model/AutoShape;Lcom/samsung/thumbnail/customview/word/ShapesController;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2384
    :cond_7
    :goto_2
    if-eqz v2, :cond_0

    .line 2385
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_0

    .line 2175
    :cond_8
    const-string/jumbo v3, "StraightConnector1"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 2176
    const/16 v3, 0x1c0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hslf/model/Shape;->getEscherProperty(S)I

    move-result v14

    .line 2178
    .local v14, "colorValue":I
    if-eqz v14, :cond_3

    .line 2179
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Shape;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v13

    .line 2180
    .local v13, "color":Lorg/apache/poi/java/awt/Color;
    if-eqz v13, :cond_3

    goto/16 :goto_1

    .line 2184
    .end local v13    # "color":Lorg/apache/poi/java/awt/Color;
    .end local v14    # "colorValue":I
    :cond_9
    const-string/jumbo v3, "BentConnector3"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string/jumbo v3, "BentConnector2"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string/jumbo v3, "CurvedConnector3"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 2187
    :cond_a
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v6, v12, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v7, v12, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v8, v12, Lorg/apache/poi/java/awt/Rectangle;->height:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    invoke-direct/range {v2 .. v11}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Lorg/apache/poi/hslf/model/Shape;IIIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Ljava/io/File;)V

    .restart local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    goto/16 :goto_1

    .line 2190
    :cond_b
    const-string/jumbo v3, "RoundRectangle"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string/jumbo v3, "Diamond"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string/jumbo v3, "Ellipse"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string/jumbo v3, "IsocelesTriangle"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string/jumbo v3, "RightTriangle"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string/jumbo v3, "Parallelogram"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string/jumbo v3, "Pentagon"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string/jumbo v3, "Hexagon"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string/jumbo v3, "Octagon"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string/jumbo v3, "Plus"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string/jumbo v3, "Can"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 2198
    :cond_c
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v6, v12, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v7, v12, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v8, v12, Lorg/apache/poi/java/awt/Rectangle;->height:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    invoke-direct/range {v2 .. v11}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Lorg/apache/poi/hslf/model/Shape;IIIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Ljava/io/File;)V

    .restart local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    goto/16 :goto_1

    .line 2201
    :cond_d
    const-string/jumbo v3, "HomePlate"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    const-string/jumbo v3, "Chevron"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    const-string/jumbo v3, "Bevel"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    const-string/jumbo v3, "Plaque"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    const-string/jumbo v3, "Cube"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    const-string/jumbo v3, "Moon"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    const-string/jumbo v3, "FoldedCorner"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    const-string/jumbo v3, "LightningBolt"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    const-string/jumbo v3, "Sun"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    const-string/jumbo v3, "SmileyFace"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 2207
    :cond_e
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v6, v12, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v7, v12, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v8, v12, Lorg/apache/poi/java/awt/Rectangle;->height:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    invoke-direct/range {v2 .. v11}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Lorg/apache/poi/hslf/model/Shape;IIIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Ljava/io/File;)V

    .restart local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    goto/16 :goto_1

    .line 2210
    :cond_f
    const-string/jumbo v3, "ActionButtonHome"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 2211
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v6, v12, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v7, v12, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v8, v12, Lorg/apache/poi/java/awt/Rectangle;->height:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    invoke-direct/range {v2 .. v11}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Lorg/apache/poi/hslf/model/Shape;IIIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Ljava/io/File;)V

    .restart local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    goto/16 :goto_1

    .line 2214
    :cond_10
    const-string/jumbo v3, "DownArrow"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_11

    const-string/jumbo v3, "UpArrow"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_11

    const-string/jumbo v3, "Arrow"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_11

    const-string/jumbo v3, "Arrow"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_11

    const-string/jumbo v3, "LeftArrow"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_11

    const-string/jumbo v3, "LeftRightArrow"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_11

    const-string/jumbo v3, "UpDownArrow"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_11

    const-string/jumbo v3, "NotchedRightArrow"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 2220
    :cond_11
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v6, v12, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v7, v12, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v8, v12, Lorg/apache/poi/java/awt/Rectangle;->height:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    invoke-direct/range {v2 .. v11}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Lorg/apache/poi/hslf/model/Shape;IIIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Ljava/io/File;)V

    .restart local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    goto/16 :goto_1

    .line 2223
    :cond_12
    const-string/jumbo v3, "LeftArrowCallout"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_13

    const-string/jumbo v3, "DownArrowCallout"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_13

    const-string/jumbo v3, "RightArrowCallout"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_13

    const-string/jumbo v3, "UpArrowCallout"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_13

    const-string/jumbo v3, "LeftRightArrowCallout"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 2228
    :cond_13
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v6, v12, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v7, v12, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v8, v12, Lorg/apache/poi/java/awt/Rectangle;->height:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    invoke-direct/range {v2 .. v11}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Lorg/apache/poi/hslf/model/Shape;IIIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Ljava/io/File;)V

    .restart local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    goto/16 :goto_1

    .line 2231
    :cond_14
    const-string/jumbo v3, "BracketPair"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_15

    const-string/jumbo v3, "LeftBracket"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_15

    const-string/jumbo v3, "RightBracket"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_16

    .line 2234
    :cond_15
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v6, v12, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v7, v12, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v8, v12, Lorg/apache/poi/java/awt/Rectangle;->height:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    invoke-direct/range {v2 .. v11}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Lorg/apache/poi/hslf/model/Shape;IIIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Ljava/io/File;)V

    .restart local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    goto/16 :goto_1

    .line 2237
    :cond_16
    const-string/jumbo v3, "BracePair"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_17

    const-string/jumbo v3, "LeftBrace"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_17

    const-string/jumbo v3, "RightBrace"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 2240
    :cond_17
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v6, v12, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v7, v12, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v8, v12, Lorg/apache/poi/java/awt/Rectangle;->height:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    invoke-direct/range {v2 .. v11}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Lorg/apache/poi/hslf/model/Shape;IIIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Ljava/io/File;)V

    .restart local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    goto/16 :goto_1

    .line 2243
    :cond_18
    const-string/jumbo v3, "CurvedRightArrow"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_19

    const-string/jumbo v3, "CurvedLeftArrow"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_19

    const-string/jumbo v3, "CurvedUpArrow"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_19

    const-string/jumbo v3, "CurvedDownArrow"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 2247
    :cond_19
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v6, v12, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v7, v12, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v8, v12, Lorg/apache/poi/java/awt/Rectangle;->height:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    invoke-direct/range {v2 .. v11}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Lorg/apache/poi/hslf/model/Shape;IIIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Ljava/io/File;)V

    .restart local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    goto/16 :goto_1

    .line 2250
    :cond_1a
    const-string/jumbo v3, "FlowChartProcess"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartAlternateProcess"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartInputOutput"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartDecision"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartPredefinedProcess"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartInternalStorage"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartTerminator"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartPreparation"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartManualInput"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartManualOperation"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartConnector"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartOffpageConnector"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartPunchedCard"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartSummingJunction"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartOr"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartCollate"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartSort"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartExtract"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartMerge"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartOnlineStorage"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartDelay"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartMagneticTape"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartMagneticDisk"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartMagneticDrum"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartDisplay"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartPunchedTape"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartDocument"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string/jumbo v3, "FlowChartMultidocument"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1c

    .line 2278
    :cond_1b
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v6, v12, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v7, v12, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v8, v12, Lorg/apache/poi/java/awt/Rectangle;->height:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    invoke-direct/range {v2 .. v11}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Lorg/apache/poi/hslf/model/Shape;IIIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Ljava/io/File;)V

    .restart local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    goto/16 :goto_1

    .line 2281
    :cond_1c
    const-string/jumbo v3, "IrregularSeal1"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1d

    const-string/jumbo v3, "IrregularSeal2"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 2283
    :cond_1d
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v6, v12, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v7, v12, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v8, v12, Lorg/apache/poi/java/awt/Rectangle;->height:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    invoke-direct/range {v2 .. v11}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Lorg/apache/poi/hslf/model/Shape;IIIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Ljava/io/File;)V

    .restart local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    goto/16 :goto_1

    .line 2286
    :cond_1e
    const-string/jumbo v3, "Star4"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1f

    const-string/jumbo v3, "Star8"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1f

    const-string/jumbo v3, "Star16"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1f

    const-string/jumbo v3, "Star24"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1f

    const-string/jumbo v3, "Star32"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_20

    .line 2289
    :cond_1f
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v6, v12, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v7, v12, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v8, v12, Lorg/apache/poi/java/awt/Rectangle;->height:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    invoke-direct/range {v2 .. v11}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Lorg/apache/poi/hslf/model/Shape;IIIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Ljava/io/File;)V

    .restart local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    goto/16 :goto_1

    .line 2292
    :cond_20
    const-string/jumbo v3, "Ribbon2"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_21

    const-string/jumbo v3, "Ribbon"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_21

    const-string/jumbo v3, "EllipseRibbon2"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_21

    const-string/jumbo v3, "EllipseRibbon"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_22

    .line 2295
    :cond_21
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v6, v12, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v7, v12, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v8, v12, Lorg/apache/poi/java/awt/Rectangle;->height:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    invoke-direct/range {v2 .. v11}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Lorg/apache/poi/hslf/model/Shape;IIIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Ljava/io/File;)V

    .restart local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    goto/16 :goto_1

    .line 2298
    :cond_22
    const-string/jumbo v3, "VerticalScroll"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_23

    const-string/jumbo v3, "HorizontalScroll"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_24

    .line 2300
    :cond_23
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v6, v12, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v7, v12, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v8, v12, Lorg/apache/poi/java/awt/Rectangle;->height:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    invoke-direct/range {v2 .. v11}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Lorg/apache/poi/hslf/model/Shape;IIIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Ljava/io/File;)V

    .restart local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    goto/16 :goto_1

    .line 2303
    :cond_24
    const-string/jumbo v3, "Wave"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_25

    const-string/jumbo v3, "DoubleWave"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_26

    .line 2304
    :cond_25
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v6, v12, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v7, v12, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v8, v12, Lorg/apache/poi/java/awt/Rectangle;->height:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    invoke-direct/range {v2 .. v11}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Lorg/apache/poi/hslf/model/Shape;IIIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Ljava/io/File;)V

    .restart local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    goto/16 :goto_1

    .line 2307
    :cond_26
    const-string/jumbo v3, "WedgeRectCallout"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_27

    const-string/jumbo v3, "WedgeRRectCallout"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_27

    const-string/jumbo v3, "WedgeEllipseCallout"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_27

    const-string/jumbo v3, "CloudCallout"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_28

    .line 2311
    :cond_27
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v6, v12, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v7, v12, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v8, v12, Lorg/apache/poi/java/awt/Rectangle;->height:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    invoke-direct/range {v2 .. v11}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Lorg/apache/poi/hslf/model/Shape;IIIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Ljava/io/File;)V

    .restart local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    goto/16 :goto_1

    .line 2314
    :cond_28
    const-string/jumbo v3, "BorderCallout1"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_29

    const-string/jumbo v3, "Callout1"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_29

    const-string/jumbo v3, "AccentCallout1"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_29

    const-string/jumbo v3, "AccentBorderCallout1"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2a

    .line 2318
    :cond_29
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v6, v12, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v7, v12, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v8, v12, Lorg/apache/poi/java/awt/Rectangle;->height:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    invoke-direct/range {v2 .. v11}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Lorg/apache/poi/hslf/model/Shape;IIIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Ljava/io/File;)V

    .restart local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    goto/16 :goto_1

    .line 2321
    :cond_2a
    const-string/jumbo v3, "BorderCallout2"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2b

    const-string/jumbo v3, "Callout2"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2b

    const-string/jumbo v3, "AccentCallout2"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2b

    const-string/jumbo v3, "AccentBorderCallout2"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2c

    .line 2325
    :cond_2b
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v6, v12, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v7, v12, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v8, v12, Lorg/apache/poi/java/awt/Rectangle;->height:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    invoke-direct/range {v2 .. v11}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Lorg/apache/poi/hslf/model/Shape;IIIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Ljava/io/File;)V

    .restart local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    goto/16 :goto_1

    .line 2328
    :cond_2c
    const-string/jumbo v3, "BorderCallout3"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2d

    const-string/jumbo v3, "Callout3"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2d

    const-string/jumbo v3, "AccentCallout3"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2d

    const-string/jumbo v3, "AccentBorderCallout3"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2e

    .line 2332
    :cond_2d
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v6, v12, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v7, v12, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v8, v12, Lorg/apache/poi/java/awt/Rectangle;->height:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    invoke-direct/range {v2 .. v11}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Lorg/apache/poi/hslf/model/Shape;IIIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Ljava/io/File;)V

    .restart local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    goto/16 :goto_1

    .line 2335
    :cond_2e
    const-string/jumbo v3, "NotPrimitive"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2336
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    iget v5, v12, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v6, v12, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v7, v12, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v8, v12, Lorg/apache/poi/java/awt/Rectangle;->height:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    invoke-direct/range {v2 .. v11}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Lorg/apache/poi/hslf/model/Shape;IIIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Ljava/io/File;)V

    .restart local v2    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    goto/16 :goto_1

    .line 2378
    :cond_2f
    :try_start_1
    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/AutoShape;->getText()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 2379
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v2}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->readTextFromSlides(Lorg/apache/poi/hslf/model/AutoShape;Lcom/samsung/thumbnail/customview/word/ShapesController;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    .line 2381
    :catch_0
    move-exception v15

    .line 2382
    .local v15, "e":Ljava/lang/Exception;
    const-string/jumbo v3, "DocumentService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v15}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method private createNonPrimitiveShapes(Lorg/apache/poi/hslf/model/AutoShape;)V
    .locals 52
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;

    .prologue
    .line 2488
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getAnchor()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v12

    .line 2489
    .local v12, "anchor":Lorg/apache/poi/java/awt/Rectangle;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getRotation()I

    move-result v31

    .line 2490
    .local v31, "rotation":I
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getFlipVertical()Z

    move-result v22

    .line 2491
    .local v22, "flipVertical":Z
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getFlipHorizontal()Z

    move-result v21

    .line 2492
    .local v21, "flipHorizontal":Z
    const/4 v14, 0x0

    .local v14, "bitmapHight":I
    const/4 v15, 0x0

    .line 2493
    .local v15, "bitmapWidth":I
    if-nez v31, :cond_4

    .line 2494
    invoke-virtual {v12}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v6

    double-to-int v5, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v6

    double-to-int v6, v6

    mul-int/lit8 v6, v6, 0x2

    add-int v15, v5, v6

    .line 2496
    invoke-virtual {v12}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v6

    double-to-int v5, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v6

    double-to-int v6, v6

    mul-int/lit8 v6, v6, 0x2

    add-int v14, v5, v6

    .line 2512
    :goto_0
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v15, v14, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 2514
    .local v13, "bitmap":Landroid/graphics/Bitmap;
    new-instance v25, Landroid/graphics/Paint;

    invoke-direct/range {v25 .. v25}, Landroid/graphics/Paint;-><init>()V

    .line 2515
    .local v25, "paintLine":Landroid/graphics/Paint;
    const/4 v5, 0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2516
    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2517
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v6

    double-to-float v5, v6

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2518
    new-instance v24, Landroid/graphics/Paint;

    invoke-direct/range {v24 .. v24}, Landroid/graphics/Paint;-><init>()V

    .line 2519
    .local v24, "paintFill":Landroid/graphics/Paint;
    const/4 v5, 0x1

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2520
    sget-object v5, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2521
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 2522
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v5

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v8

    move-object/from16 v0, v25

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 2525
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getFill()Lorg/apache/poi/hslf/model/Fill;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/Fill;->getForegroundColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v23

    .line 2526
    .local v23, "foregroundColor":Lorg/apache/poi/java/awt/Color;
    if-eqz v23, :cond_1

    .line 2527
    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v5

    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v6

    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v7

    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v8

    move-object/from16 v0, v24

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 2530
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getFill()Lorg/apache/poi/hslf/model/Fill;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/Fill;->getForegroundColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 2531
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getFill()Lorg/apache/poi/hslf/model/Fill;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/Fill;->getForegroundColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v17

    .line 2532
    .local v17, "color":Lorg/apache/poi/java/awt/Color;
    if-eqz v17, :cond_2

    .line 2533
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v5

    add-int/lit8 v5, v5, -0x14

    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v6

    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v7

    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v8

    move-object/from16 v0, v24

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 2537
    .end local v17    # "color":Lorg/apache/poi/java/awt/Color;
    :cond_2
    const/16 v5, 0x145

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lorg/apache/poi/hslf/model/AutoShape;->getEscherProperty(S)I

    move-result v37

    .line 2539
    .local v37, "value":I
    const/16 v5, 0x146

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lorg/apache/poi/hslf/model/AutoShape;->getSegmentType(S)[I

    move-result-object v34

    .line 2541
    .local v34, "segmentType":[I
    const/16 v5, 0x146

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lorg/apache/poi/hslf/model/AutoShape;->getSegmentCount(S)[I

    move-result-object v32

    .line 2544
    .local v32, "segmentCount":[I
    if-eqz v32, :cond_3

    if-nez v34, :cond_a

    .line 2689
    :cond_3
    :goto_1
    return-void

    .line 2498
    .end local v13    # "bitmap":Landroid/graphics/Bitmap;
    .end local v23    # "foregroundColor":Lorg/apache/poi/java/awt/Color;
    .end local v24    # "paintFill":Landroid/graphics/Paint;
    .end local v25    # "paintLine":Landroid/graphics/Paint;
    .end local v32    # "segmentCount":[I
    .end local v34    # "segmentType":[I
    .end local v37    # "value":I
    :cond_4
    if-lez v31, :cond_5

    const/16 v5, 0x2d

    move/from16 v0, v31

    if-lt v0, v5, :cond_8

    :cond_5
    const/16 v5, 0x86

    move/from16 v0, v31

    if-le v0, v5, :cond_6

    const/16 v5, 0xb4

    move/from16 v0, v31

    if-lt v0, v5, :cond_8

    :cond_6
    const/16 v5, -0xb4

    move/from16 v0, v31

    if-le v0, v5, :cond_7

    const/16 v5, -0x87

    move/from16 v0, v31

    if-lt v0, v5, :cond_8

    :cond_7
    const/16 v5, -0x2d

    move/from16 v0, v31

    if-le v0, v5, :cond_9

    if-gez v31, :cond_9

    .line 2502
    :cond_8
    invoke-virtual {v12}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v6

    double-to-int v5, v6

    add-int/lit8 v5, v5, 0x1

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v6

    double-to-int v6, v6

    mul-int/lit8 v6, v6, 0x2

    add-int v15, v5, v6

    .line 2504
    invoke-virtual {v12}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v6

    double-to-int v5, v6

    add-int/lit8 v5, v5, 0x1

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v6

    double-to-int v6, v6

    mul-int/lit8 v6, v6, 0x2

    add-int v14, v5, v6

    goto/16 :goto_0

    .line 2507
    :cond_9
    invoke-virtual {v12}, Lorg/apache/poi/java/awt/Rectangle;->getHeight()D

    move-result-wide v6

    double-to-int v5, v6

    add-int/lit8 v5, v5, 0x1

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v6

    double-to-int v6, v6

    mul-int/lit8 v6, v6, 0x2

    add-int v15, v5, v6

    .line 2509
    invoke-virtual {v12}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v6

    double-to-int v5, v6

    add-int/lit8 v5, v5, 0x1

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v6

    double-to-int v6, v6

    mul-int/lit8 v6, v6, 0x2

    add-int v14, v5, v6

    goto/16 :goto_0

    .line 2547
    .restart local v13    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v23    # "foregroundColor":Lorg/apache/poi/java/awt/Color;
    .restart local v24    # "paintFill":Landroid/graphics/Paint;
    .restart local v25    # "paintLine":Landroid/graphics/Paint;
    .restart local v32    # "segmentCount":[I
    .restart local v34    # "segmentType":[I
    .restart local v37    # "value":I
    :cond_a
    new-instance v16, Landroid/graphics/Canvas;

    move-object/from16 v0, v16

    invoke-direct {v0, v13}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2548
    .local v16, "canvas":Landroid/graphics/Canvas;
    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 2549
    .local v4, "path":Landroid/graphics/Path;
    const/16 v27, 0x0

    .line 2550
    .local v27, "point_count":I
    if-eqz v37, :cond_15

    .line 2551
    const/16 v5, 0x145

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lorg/apache/poi/hslf/model/AutoShape;->getVertices(S)[Landroid/graphics/Point;

    move-result-object v26

    .line 2553
    .local v26, "point":[Landroid/graphics/Point;
    if-eqz v26, :cond_3

    .line 2558
    const/16 v33, 0x0

    .local v33, "segmentInfo_count":I
    :goto_2
    move-object/from16 v0, v34

    array-length v5, v0

    move/from16 v0, v33

    if-ge v0, v5, :cond_15

    .line 2559
    aget v5, v34, v33

    packed-switch v5, :pswitch_data_0

    .line 2558
    :cond_b
    :goto_3
    :pswitch_0
    add-int/lit8 v33, v33, 0x1

    goto :goto_2

    .line 2561
    :pswitch_1
    aget v5, v32, v33

    if-nez v5, :cond_b

    .line 2562
    aget-object v5, v26, v27

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v6

    double-to-float v6, v6

    add-float/2addr v5, v6

    add-int/lit8 v28, v27, 0x1

    .end local v27    # "point_count":I
    .local v28, "point_count":I
    aget-object v6, v26, v27

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v8

    double-to-float v7, v8

    add-float/2addr v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    move/from16 v27, v28

    .end local v28    # "point_count":I
    .restart local v27    # "point_count":I
    goto :goto_3

    .line 2570
    :goto_4
    :pswitch_2
    aget v5, v32, v33

    if-eqz v5, :cond_b

    .line 2571
    aget-object v5, v26, v27

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v6

    double-to-float v6, v6

    add-float/2addr v5, v6

    add-int/lit8 v28, v27, 0x1

    .end local v27    # "point_count":I
    .restart local v28    # "point_count":I
    aget-object v6, v26, v27

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v8

    double-to-float v7, v8

    add-float/2addr v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2576
    aget v5, v32, v33

    add-int/lit8 v5, v5, -0x1

    aput v5, v32, v33

    move/from16 v27, v28

    .end local v28    # "point_count":I
    .restart local v27    # "point_count":I
    goto :goto_4

    .line 2580
    :goto_5
    :pswitch_3
    aget v5, v32, v33

    if-eqz v5, :cond_b

    .line 2581
    aget-object v5, v26, v27

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v6

    double-to-float v6, v6

    add-float/2addr v5, v6

    add-int/lit8 v28, v27, 0x1

    .end local v27    # "point_count":I
    .restart local v28    # "point_count":I
    aget-object v6, v26, v27

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v8

    double-to-float v7, v8

    add-float/2addr v6, v7

    aget-object v7, v26, v28

    iget v7, v7, Landroid/graphics/Point;->x:I

    int-to-float v7, v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v8

    double-to-float v8, v8

    add-float/2addr v7, v8

    add-int/lit8 v27, v28, 0x1

    .end local v28    # "point_count":I
    .restart local v27    # "point_count":I
    aget-object v8, v26, v28

    iget v8, v8, Landroid/graphics/Point;->y:I

    int-to-float v8, v8

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v50

    move-wide/from16 v0, v50

    double-to-float v9, v0

    add-float/2addr v8, v9

    aget-object v9, v26, v27

    iget v9, v9, Landroid/graphics/Point;->x:I

    int-to-float v9, v9

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v50

    move-wide/from16 v0, v50

    double-to-float v11, v0

    add-float/2addr v9, v11

    add-int/lit8 v28, v27, 0x1

    .end local v27    # "point_count":I
    .restart local v28    # "point_count":I
    aget-object v11, v26, v27

    iget v11, v11, Landroid/graphics/Point;->y:I

    int-to-float v11, v11

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v50

    move-wide/from16 v0, v50

    double-to-float v0, v0

    move/from16 v50, v0

    add-float v10, v11, v50

    invoke-virtual/range {v4 .. v10}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 2594
    aget v5, v32, v33

    add-int/lit8 v5, v5, -0x1

    aput v5, v32, v33

    move/from16 v27, v28

    .end local v28    # "point_count":I
    .restart local v27    # "point_count":I
    goto :goto_5

    .line 2598
    :pswitch_4
    aget v5, v32, v33

    const/4 v6, 0x1

    if-ne v5, v6, :cond_b

    .line 2599
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    goto/16 :goto_3

    .line 2605
    :pswitch_5
    aget v5, v32, v33

    shr-int/lit8 v20, v5, 0x8

    .line 2606
    .local v20, "escape_code":I
    const/4 v5, 0x4

    move/from16 v0, v20

    if-eq v0, v5, :cond_c

    const/4 v5, 0x3

    move/from16 v0, v20

    if-eq v0, v5, :cond_c

    const/4 v5, 0x6

    move/from16 v0, v20

    if-eq v0, v5, :cond_c

    const/4 v5, 0x5

    move/from16 v0, v20

    if-ne v0, v5, :cond_14

    .line 2609
    :cond_c
    new-instance v29, Landroid/graphics/RectF;

    aget-object v5, v26, v27

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v6

    double-to-float v6, v6

    add-float/2addr v5, v6

    add-int/lit8 v28, v27, 0x1

    .end local v27    # "point_count":I
    .restart local v28    # "point_count":I
    aget-object v6, v26, v27

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v8

    double-to-float v7, v8

    add-float/2addr v6, v7

    aget-object v7, v26, v28

    iget v7, v7, Landroid/graphics/Point;->x:I

    int-to-float v7, v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v8

    double-to-float v8, v8

    add-float/2addr v7, v8

    add-int/lit8 v27, v28, 0x1

    .end local v28    # "point_count":I
    .restart local v27    # "point_count":I
    aget-object v8, v26, v28

    iget v8, v8, Landroid/graphics/Point;->y:I

    int-to-float v8, v8

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v50

    move-wide/from16 v0, v50

    double-to-float v9, v0

    add-float/2addr v8, v9

    move-object/from16 v0, v29

    invoke-direct {v0, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2617
    .local v29, "rectF":Landroid/graphics/RectF;
    invoke-virtual/range {v29 .. v29}, Landroid/graphics/RectF;->centerX()F

    move-result v5

    float-to-double v0, v5

    move-wide/from16 v42, v0

    .line 2618
    .local v42, "xc":D
    invoke-virtual/range {v29 .. v29}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    float-to-double v0, v5

    move-wide/from16 v48, v0

    .line 2619
    .local v48, "yc":D
    aget-object v5, v26, v27

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v6

    double-to-float v6, v6

    add-float/2addr v5, v6

    float-to-double v0, v5

    move-wide/from16 v38, v0

    .line 2621
    .local v38, "x1":D
    add-int/lit8 v28, v27, 0x1

    .end local v27    # "point_count":I
    .restart local v28    # "point_count":I
    aget-object v5, v26, v27

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v6

    double-to-float v6, v6

    add-float/2addr v5, v6

    float-to-double v0, v5

    move-wide/from16 v44, v0

    .line 2623
    .local v44, "y1":D
    sub-double v6, v44, v48

    sub-double v8, v38, v42

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v6

    double-to-float v0, v6

    move/from16 v35, v0

    .line 2625
    .local v35, "startA":F
    aget-object v5, v26, v28

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v6

    double-to-float v6, v6

    add-float/2addr v5, v6

    float-to-double v0, v5

    move-wide/from16 v40, v0

    .line 2627
    .local v40, "x2":D
    add-int/lit8 v27, v28, 0x1

    .end local v28    # "point_count":I
    .restart local v27    # "point_count":I
    aget-object v5, v26, v28

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v6

    double-to-float v6, v6

    add-float/2addr v5, v6

    float-to-double v0, v5

    move-wide/from16 v46, v0

    .line 2629
    .local v46, "y2":D
    sub-double v6, v46, v48

    sub-double v8, v40, v42

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v6

    double-to-float v0, v6

    move/from16 v19, v0

    .line 2631
    .local v19, "endA":F
    sub-float v36, v19, v35

    .line 2632
    .local v36, "sweepA":F
    const/4 v5, 0x4

    move/from16 v0, v20

    if-ne v0, v5, :cond_e

    .line 2633
    const/4 v5, 0x0

    cmpl-float v5, v36, v5

    if-lez v5, :cond_d

    .line 2634
    const/high16 v5, 0x43b40000    # 360.0f

    sub-float v36, v36, v5

    .line 2635
    :cond_d
    move-object/from16 v0, v29

    move/from16 v1, v35

    move/from16 v2, v36

    invoke-virtual {v4, v0, v1, v2}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    goto/16 :goto_3

    .line 2636
    :cond_e
    const/4 v5, 0x3

    move/from16 v0, v20

    if-ne v0, v5, :cond_10

    .line 2637
    const/4 v5, 0x0

    cmpl-float v5, v36, v5

    if-lez v5, :cond_f

    .line 2638
    const/high16 v5, 0x43b40000    # 360.0f

    sub-float v36, v36, v5

    .line 2639
    :cond_f
    move-object/from16 v0, v29

    move/from16 v1, v35

    move/from16 v2, v36

    invoke-virtual {v4, v0, v1, v2}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    goto/16 :goto_3

    .line 2640
    :cond_10
    const/4 v5, 0x6

    move/from16 v0, v20

    if-ne v0, v5, :cond_12

    .line 2641
    const/4 v5, 0x0

    cmpg-float v5, v36, v5

    if-gez v5, :cond_11

    .line 2642
    const/high16 v5, 0x43b40000    # 360.0f

    add-float v36, v36, v5

    .line 2643
    :cond_11
    move-object/from16 v0, v29

    move/from16 v1, v35

    move/from16 v2, v36

    invoke-virtual {v4, v0, v1, v2}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    goto/16 :goto_3

    .line 2644
    :cond_12
    const/4 v5, 0x5

    move/from16 v0, v20

    if-ne v0, v5, :cond_b

    .line 2645
    const/4 v5, 0x0

    cmpg-float v5, v36, v5

    if-gez v5, :cond_13

    .line 2646
    const/high16 v5, 0x43b40000    # 360.0f

    add-float v36, v36, v5

    .line 2647
    :cond_13
    move-object/from16 v0, v29

    move/from16 v1, v35

    move/from16 v2, v36

    invoke-virtual {v4, v0, v1, v2}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    goto/16 :goto_3

    .line 2649
    .end local v19    # "endA":F
    .end local v29    # "rectF":Landroid/graphics/RectF;
    .end local v35    # "startA":F
    .end local v36    # "sweepA":F
    .end local v38    # "x1":D
    .end local v40    # "x2":D
    .end local v42    # "xc":D
    .end local v44    # "y1":D
    .end local v46    # "y2":D
    .end local v48    # "yc":D
    :cond_14
    const/16 v5, 0x9

    move/from16 v0, v20

    if-ne v0, v5, :cond_b

    .line 2650
    add-int/lit8 v27, v27, 0x1

    goto/16 :goto_3

    .line 2662
    .end local v20    # "escape_code":I
    .end local v26    # "point":[Landroid/graphics/Point;
    .end local v33    # "segmentInfo_count":I
    :cond_15
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getFill()Lorg/apache/poi/hslf/model/Fill;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/Fill;->getForegroundColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    if-eqz v5, :cond_16

    .line 2663
    move-object/from16 v0, v16

    move-object/from16 v1, v24

    invoke-virtual {v0, v4, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2664
    :cond_16
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    if-eqz v5, :cond_17

    .line 2665
    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-virtual {v0, v4, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2666
    :cond_17
    new-instance v10, Landroid/graphics/Matrix;

    invoke-direct {v10}, Landroid/graphics/Matrix;-><init>()V

    .line 2667
    .local v10, "mtx":Landroid/graphics/Matrix;
    if-eqz v21, :cond_19

    if-nez v22, :cond_19

    .line 2668
    const/high16 v5, -0x40800000    # -1.0f

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v10, v5, v6}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 2674
    :cond_18
    :goto_6
    move/from16 v0, v31

    int-to-float v5, v0

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    invoke-virtual {v10, v5, v6, v7}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 2676
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    const/4 v11, 0x1

    move-object v5, v13

    invoke-static/range {v5 .. v11}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v30

    .line 2679
    .local v30, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v5, Ljava/io/FileOutputStream;

    new-instance v6, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "pic_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getShapeName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ".png"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v5, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->out:Ljava/io/FileOutputStream;

    .line 2683
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x64

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->out:Ljava/io/FileOutputStream;

    move-object/from16 v0, v30

    invoke-virtual {v0, v5, v6, v7}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 2684
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->out:Ljava/io/FileOutputStream;

    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2688
    :goto_7
    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->addShapeToHTMLCreater(Lorg/apache/poi/hslf/model/AutoShape;)V

    goto/16 :goto_1

    .line 2669
    .end local v30    # "rotatedBMP":Landroid/graphics/Bitmap;
    :cond_19
    if-eqz v22, :cond_1a

    if-nez v21, :cond_1a

    .line 2670
    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v6, -0x40800000    # -1.0f

    invoke-virtual {v10, v5, v6}, Landroid/graphics/Matrix;->preScale(FF)Z

    goto/16 :goto_6

    .line 2671
    :cond_1a
    if-eqz v21, :cond_18

    if-eqz v22, :cond_18

    .line 2672
    const/high16 v5, -0x40800000    # -1.0f

    const/high16 v6, -0x40800000    # -1.0f

    invoke-virtual {v10, v5, v6}, Landroid/graphics/Matrix;->preScale(FF)Z

    goto/16 :goto_6

    .line 2685
    .restart local v30    # "rotatedBMP":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v18

    .line 2686
    .local v18, "e":Ljava/lang/Exception;
    const-string/jumbo v5, "PPTParser"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Exception while writing pictures to out file"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 2559
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method private createPicture(Lorg/apache/poi/hslf/model/Picture;)V
    .locals 20
    .param p1, "picture"    # Lorg/apache/poi/hslf/model/Picture;

    .prologue
    .line 2698
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Picture;->getPictureData()Lorg/apache/poi/hslf/usermodel/PictureData;

    move-result-object v14

    .line 2699
    .local v14, "ptempdata":Lorg/apache/poi/hslf/usermodel/PictureData;
    const/4 v12, 0x0

    .line 2700
    .local v12, "imgPath":Ljava/lang/String;
    const/4 v11, 0x0

    .line 2701
    .local v11, "ext":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/Picture;->getAnchor()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v9

    .line 2705
    .local v9, "anchor":Lorg/apache/poi/java/awt/Rectangle;
    if-eqz v14, :cond_0

    .line 2706
    :try_start_0
    invoke-virtual {v14}, Lorg/apache/poi/hslf/usermodel/PictureData;->getType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 2726
    const-string/jumbo v11, ".gif"

    .line 2729
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "/pict_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->count:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 2730
    new-instance v2, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "pict_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->count:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->out:Ljava/io/FileOutputStream;

    .line 2737
    if-eqz v11, :cond_4

    const-string/jumbo v2, ".emf"

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2738
    new-instance v15, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;

    invoke-virtual {v14}, Lorg/apache/poi/hslf/usermodel/PictureData;->getData()[B

    move-result-object v2

    invoke-direct {v15, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;-><init>([B)V

    .line 2740
    .local v15, "readEMFFile":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    iget v3, v9, Lorg/apache/poi/java/awt/Rectangle;->width:I

    int-to-double v6, v3

    invoke-virtual {v2, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v2

    double-to-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v15, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->setActualWidth(F)V

    .line 2742
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    iget v3, v9, Lorg/apache/poi/java/awt/Rectangle;->height:I

    int-to-double v6, v3

    invoke-virtual {v2, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v2

    double-to-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v15, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->setActualHeight(F)V

    .line 2746
    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawImage()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2747
    .local v4, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v4, :cond_0

    .line 2748
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->out:Ljava/io/FileOutputStream;

    invoke-virtual {v4, v2, v3, v5}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2770
    .end local v4    # "bitmap":Landroid/graphics/Bitmap;
    .end local v15    # "readEMFFile":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;
    :cond_0
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->out:Ljava/io/FileOutputStream;

    if-eqz v2, :cond_1

    .line 2772
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->out:Ljava/io/FileOutputStream;

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 2784
    :cond_1
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    iget v3, v9, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v6, v3

    invoke-virtual {v2, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v2

    double-to-int v13, v2

    .line 2786
    .local v13, "leftMargin":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    iget v3, v9, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v6, v3

    invoke-virtual {v2, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v2

    double-to-int v0, v2

    move/from16 v16, v0

    .line 2791
    .local v16, "topMargin":I
    int-to-double v2, v13

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v6

    cmpl-double v2, v2, v6

    if-gez v2, :cond_2

    move/from16 v0, v16

    int-to-double v2, v0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v6

    cmpl-double v2, v2, v6

    if-ltz v2, :cond_3

    .line 2796
    :cond_2
    iget v2, v9, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->getOriginalValue(F)F

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints1(F)F

    move-result v2

    float-to-int v2, v2

    iput v2, v9, Lorg/apache/poi/java/awt/Rectangle;->x:I

    .line 2797
    iget v2, v9, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->getOriginalValue(F)F

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints1(F)F

    move-result v2

    float-to-int v2, v2

    iput v2, v9, Lorg/apache/poi/java/awt/Rectangle;->y:I

    .line 2798
    iget v2, v9, Lorg/apache/poi/java/awt/Rectangle;->width:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->getOriginalValue(F)F

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints1(F)F

    move-result v2

    float-to-int v2, v2

    iput v2, v9, Lorg/apache/poi/java/awt/Rectangle;->width:I

    .line 2800
    iget v2, v9, Lorg/apache/poi/java/awt/Rectangle;->height:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->getOriginalValue(F)F

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints1(F)F

    move-result v2

    float-to-int v2, v2

    iput v2, v9, Lorg/apache/poi/java/awt/Rectangle;->height:I

    .line 2804
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    iget v3, v9, Lorg/apache/poi/java/awt/Rectangle;->width:I

    int-to-double v6, v3

    invoke-virtual {v2, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v2

    double-to-int v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    iget v5, v9, Lorg/apache/poi/java/awt/Rectangle;->height:I

    int-to-double v6, v5

    invoke-virtual {v3, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v6

    double-to-int v3, v6

    invoke-static {v12, v2, v3}, Lcom/samsung/thumbnail/util/Utils;->decodeSampledBitmapFromFile(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2811
    .restart local v4    # "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v5

    iget v6, v9, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v6, v6

    invoke-virtual {v5, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v6

    double-to-int v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v6

    iget v7, v9, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v0, v7

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v6, v0, v1}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v6

    double-to-int v6, v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v7

    iget v8, v9, Lorg/apache/poi/java/awt/Rectangle;->width:I

    int-to-double v0, v8

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v7, v0, v1}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-int v7, v0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v8

    iget v0, v9, Lorg/apache/poi/java/awt/Rectangle;->height:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-double v0, v0

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v8, v0, v1}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-int v8, v0

    invoke-virtual/range {v2 .. v8}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawBitMap(ILandroid/graphics/Bitmap;IIII)V

    .line 2822
    sget v2, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->count:I

    add-int/lit8 v2, v2, 0x1

    sput v2, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->count:I

    .line 2823
    return-void

    .line 2708
    .end local v4    # "bitmap":Landroid/graphics/Bitmap;
    .end local v13    # "leftMargin":I
    .end local v16    # "topMargin":I
    :pswitch_0
    :try_start_2
    const-string/jumbo v11, ".jpg"

    .line 2709
    goto/16 :goto_0

    .line 2711
    :pswitch_1
    const-string/jumbo v11, ".png"

    .line 2712
    goto/16 :goto_0

    .line 2714
    :pswitch_2
    const-string/jumbo v11, ".bmp"

    .line 2715
    goto/16 :goto_0

    .line 2717
    :pswitch_3
    const-string/jumbo v11, ".wmf"

    .line 2718
    goto/16 :goto_0

    .line 2720
    :pswitch_4
    const-string/jumbo v11, ".emf"

    .line 2721
    goto/16 :goto_0

    .line 2723
    :pswitch_5
    const-string/jumbo v11, ".pict"

    .line 2724
    goto/16 :goto_0

    .line 2750
    :cond_4
    if-eqz v11, :cond_5

    const-string/jumbo v2, ".wmf"

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2751
    new-instance v15, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;

    invoke-virtual {v14}, Lorg/apache/poi/hslf/usermodel/PictureData;->getData()[B

    move-result-object v2

    invoke-direct {v15, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;-><init>([B)V

    .line 2753
    .local v15, "readEMFFile":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    iget v3, v9, Lorg/apache/poi/java/awt/Rectangle;->width:I

    int-to-double v6, v3

    invoke-virtual {v2, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v2

    double-to-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v15, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->setActWidth(F)V

    .line 2755
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    iget v3, v9, Lorg/apache/poi/java/awt/Rectangle;->height:I

    int-to-double v6, v3

    invoke-virtual {v2, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v2

    double-to-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v15, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->setActHeight(F)V

    .line 2759
    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawImage()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2760
    .restart local v4    # "bitmap":Landroid/graphics/Bitmap;
    if-eqz v4, :cond_0

    .line 2761
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->out:Ljava/io/FileOutputStream;

    invoke-virtual {v4, v2, v3, v5}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 2767
    .end local v4    # "bitmap":Landroid/graphics/Bitmap;
    .end local v15    # "readEMFFile":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
    :catch_0
    move-exception v10

    .line 2768
    .local v10, "e":Ljava/lang/Exception;
    :try_start_3
    const-string/jumbo v2, "PPTParser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception while writing pictures to out file"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2770
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->out:Ljava/io/FileOutputStream;

    if-eqz v2, :cond_1

    .line 2772
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->out:Ljava/io/FileOutputStream;

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_2

    .line 2773
    :catch_1
    move-exception v10

    .line 2774
    .local v10, "e":Ljava/io/IOException;
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v10}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 2763
    .end local v10    # "e":Ljava/io/IOException;
    :cond_5
    :try_start_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->out:Ljava/io/FileOutputStream;

    invoke-virtual {v14}, Lorg/apache/poi/hslf/usermodel/PictureData;->getData()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 2770
    :catchall_0
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->out:Ljava/io/FileOutputStream;

    if-eqz v3, :cond_6

    .line 2772
    :try_start_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->out:Ljava/io/FileOutputStream;

    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 2775
    :cond_6
    :goto_3
    throw v2

    .line 2773
    :catch_2
    move-exception v10

    .line 2774
    .restart local v10    # "e":Ljava/io/IOException;
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v10}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 2773
    .end local v10    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v10

    .line 2774
    .restart local v10    # "e":Ljava/io/IOException;
    const-string/jumbo v3, "DocumentService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v10}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 2706
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getOriginalValue(F)F
    .locals 2
    .param p1, "input"    # F

    .prologue
    .line 120
    const/high16 v0, 0x44100000    # 576.0f

    mul-float/2addr v0, p1

    const/high16 v1, 0x42900000    # 72.0f

    div-float/2addr v0, v1

    return v0
.end method

.method private readPPT(Lorg/apache/poi/hslf/usermodel/SlideShow;)V
    .locals 32
    .param p1, "slideShow"    # Lorg/apache/poi/hslf/usermodel/SlideShow;

    .prologue
    .line 207
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getPageSize()Lorg/apache/poi/java/awt/Dimension;

    move-result-object v25

    .line 208
    .local v25, "dimension":Lorg/apache/poi/java/awt/Dimension;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getSlides()[Lorg/apache/poi/hslf/model/Slide;

    move-result-object v30

    .line 209
    .local v30, "slide":[Lorg/apache/poi/hslf/model/Slide;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v31

    .line 211
    .local v31, "slideDimention":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    invoke-virtual/range {v25 .. v25}, Lorg/apache/poi/java/awt/Dimension;->getHeight()D

    move-result-wide v6

    move-object/from16 v0, v31

    invoke-virtual {v0, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setSlideHeight(D)V

    .line 212
    invoke-virtual/range {v25 .. v25}, Lorg/apache/poi/java/awt/Dimension;->getWidth()D

    move-result-wide v6

    move-object/from16 v0, v31

    invoke-virtual {v0, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setSlideWidth(D)V

    .line 213
    const-wide/16 v20, 0x0

    .line 214
    .local v20, "height":D
    const/16 v28, 0x0

    .local v28, "i":I
    :goto_0
    move-object/from16 v0, v30

    array-length v2, v0

    move/from16 v0, v28

    if-ge v0, v2, :cond_e

    .line 215
    if-lez v28, :cond_0

    .line 323
    :goto_1
    return-void

    .line 217
    :cond_0
    aget-object v2, v30, v28

    invoke-virtual {v2}, Lorg/apache/poi/hslf/model/Slide;->getBackground()Lorg/apache/poi/hslf/model/Background;

    move-result-object v16

    .line 218
    .local v16, "bg":Lorg/apache/poi/hslf/model/Background;
    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/hslf/model/Background;->getFill()Lorg/apache/poi/hslf/model/Fill;

    move-result-object v27

    .line 221
    .local v27, "fill_ppt":Lorg/apache/poi/hslf/model/Fill;
    const/4 v4, 0x0

    .line 222
    .local v4, "bgBitMap":Landroid/graphics/Bitmap;
    const/4 v3, 0x0

    .line 223
    .local v3, "isBackgroundImage":Z
    const/4 v5, -0x1

    .line 225
    .local v5, "bgColor":I
    invoke-virtual/range {v27 .. v27}, Lorg/apache/poi/hslf/model/Fill;->getFillType()I

    move-result v2

    if-nez v2, :cond_6

    .line 226
    invoke-virtual/range {v27 .. v27}, Lorg/apache/poi/hslf/model/Fill;->getForegroundColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 230
    invoke-virtual/range {v27 .. v27}, Lorg/apache/poi/hslf/model/Fill;->getForegroundColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v5

    .line 231
    const/4 v3, 0x0

    .line 290
    :cond_1
    :goto_2
    invoke-virtual/range {v27 .. v27}, Lorg/apache/poi/hslf/model/Fill;->getFillType()I

    move-result v2

    const/4 v6, 0x7

    if-eq v2, v6, :cond_2

    invoke-virtual/range {v27 .. v27}, Lorg/apache/poi/hslf/model/Fill;->getFillType()I

    move-result v2

    const/4 v6, 0x4

    if-ne v2, v6, :cond_d

    .line 292
    :cond_2
    const/4 v8, -0x1

    .line 293
    .local v8, "foregorundcolor":I
    invoke-virtual/range {v27 .. v27}, Lorg/apache/poi/hslf/model/Fill;->getBackgroundColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 294
    invoke-virtual/range {v27 .. v27}, Lorg/apache/poi/hslf/model/Fill;->getBackgroundColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v8

    .line 297
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    const/4 v7, 0x1

    invoke-virtual/range {v2 .. v8}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawBackGroundContent(ZLandroid/graphics/Bitmap;IIII)V

    .line 309
    .end local v8    # "foregorundcolor":I
    :goto_3
    aget-object v2, v30, v28

    invoke-virtual {v2}, Lorg/apache/poi/hslf/model/Slide;->getHeadersFooters()Lorg/apache/poi/hslf/model/HeadersFooters;

    move-result-object v15

    .line 310
    .local v15, "headerFooter":Lorg/apache/poi/hslf/model/HeadersFooters;
    aget-object v2, v30, v28

    invoke-virtual {v2}, Lorg/apache/poi/hslf/model/Slide;->getMasterSheet()Lorg/apache/poi/hslf/model/MasterSheet;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 313
    aget-object v2, v30, v28

    invoke-virtual {v2}, Lorg/apache/poi/hslf/model/Slide;->getMasterSheet()Lorg/apache/poi/hslf/model/MasterSheet;

    move-result-object v10

    .line 314
    .local v10, "masterSheet":Lorg/apache/poi/hslf/model/MasterSheet;
    const/4 v11, 0x0

    const/4 v14, 0x1

    move-object/from16 v9, p0

    move-wide/from16 v12, v20

    invoke-direct/range {v9 .. v15}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->readPicturesFromSlides(Lorg/apache/poi/hslf/model/MasterSheet;Lorg/apache/poi/hslf/model/Slide;DZLorg/apache/poi/hslf/model/HeadersFooters;)V

    .line 317
    .end local v10    # "masterSheet":Lorg/apache/poi/hslf/model/MasterSheet;
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getPictureData()[Lorg/apache/poi/hslf/usermodel/PictureData;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 318
    const/16 v18, 0x0

    aget-object v19, v30, v28

    const/16 v22, 0x0

    move-object/from16 v17, p0

    move-object/from16 v23, v15

    invoke-direct/range {v17 .. v23}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->readPicturesFromSlides(Lorg/apache/poi/hslf/model/MasterSheet;Lorg/apache/poi/hslf/model/Slide;DZLorg/apache/poi/hslf/model/HeadersFooters;)V

    .line 214
    :cond_5
    add-int/lit8 v28, v28, 0x1

    goto/16 :goto_0

    .line 237
    .end local v15    # "headerFooter":Lorg/apache/poi/hslf/model/HeadersFooters;
    :cond_6
    invoke-virtual/range {v27 .. v27}, Lorg/apache/poi/hslf/model/Fill;->getFillType()I

    move-result v2

    const/4 v6, 0x3

    if-eq v2, v6, :cond_7

    invoke-virtual/range {v27 .. v27}, Lorg/apache/poi/hslf/model/Fill;->getFillType()I

    move-result v2

    const/4 v6, 0x2

    if-ne v2, v6, :cond_a

    .line 239
    :cond_7
    invoke-virtual/range {v27 .. v27}, Lorg/apache/poi/hslf/model/Fill;->getFillType()I

    move-result v2

    const/4 v6, 0x3

    if-ne v2, v6, :cond_8

    .line 242
    :cond_8
    invoke-virtual/range {v27 .. v27}, Lorg/apache/poi/hslf/model/Fill;->getPictureData()Lorg/apache/poi/hslf/usermodel/PictureData;

    move-result-object v29

    .line 244
    .local v29, "pictureData":Lorg/apache/poi/hslf/usermodel/PictureData;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    new-instance v6, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "BG_"

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v28

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v11, ".png"

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v7, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v2, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->out:Ljava/io/FileOutputStream;

    .line 249
    if-eqz v29, :cond_9

    .line 250
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->out:Ljava/io/FileOutputStream;

    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/hslf/usermodel/PictureData;->getData()[B

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/io/FileOutputStream;->write([B)V

    .line 251
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->out:Ljava/io/FileOutputStream;

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 257
    :goto_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "BG_"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, ".png"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 261
    .local v24, "bgSrc":Ljava/lang/String;
    const/4 v3, 0x1

    .line 264
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, "/"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 267
    goto/16 :goto_2

    .line 252
    .end local v24    # "bgSrc":Ljava/lang/String;
    :catch_0
    move-exception v26

    .line 253
    .local v26, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "PPTParser"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, " BG Exception while writing pictures to out file"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v26

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 267
    .end local v26    # "e":Ljava/lang/Exception;
    .end local v29    # "pictureData":Lorg/apache/poi/hslf/usermodel/PictureData;
    :cond_a
    invoke-virtual/range {v27 .. v27}, Lorg/apache/poi/hslf/model/Fill;->getFillType()I

    move-result v2

    const/4 v6, 0x7

    if-eq v2, v6, :cond_b

    invoke-virtual/range {v27 .. v27}, Lorg/apache/poi/hslf/model/Fill;->getFillType()I

    move-result v2

    const/4 v6, 0x4

    if-ne v2, v6, :cond_c

    .line 269
    :cond_b
    invoke-virtual/range {v27 .. v27}, Lorg/apache/poi/hslf/model/Fill;->getForegroundColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 273
    invoke-virtual/range {v27 .. v27}, Lorg/apache/poi/hslf/model/Fill;->getForegroundColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v5

    .line 274
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 284
    :cond_c
    const/4 v5, -0x1

    .line 285
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 304
    :cond_d
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    const/4 v14, 0x0

    const/4 v15, -0x1

    move v10, v3

    move-object v11, v4

    move v12, v5

    invoke-virtual/range {v9 .. v15}, Lcom/samsung/thumbnail/customview/hslf/CustomController;->drawBackGroundContent(ZLandroid/graphics/Bitmap;IIII)V

    goto/16 :goto_3

    .line 322
    .end local v3    # "isBackgroundImage":Z
    .end local v4    # "bgBitMap":Landroid/graphics/Bitmap;
    .end local v5    # "bgColor":I
    .end local v16    # "bg":Lorg/apache/poi/hslf/model/Background;
    .end local v27    # "fill_ppt":Lorg/apache/poi/hslf/model/Fill;
    :cond_e
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->currentSlideNumber:I

    goto/16 :goto_1
.end method

.method private readPicturesFromSlides(Lorg/apache/poi/hslf/model/MasterSheet;Lorg/apache/poi/hslf/model/Slide;DZLorg/apache/poi/hslf/model/HeadersFooters;)V
    .locals 5
    .param p1, "masterSheet"    # Lorg/apache/poi/hslf/model/MasterSheet;
    .param p2, "slide"    # Lorg/apache/poi/hslf/model/Slide;
    .param p3, "height"    # D
    .param p5, "masterFlag"    # Z
    .param p6, "headerFooter"    # Lorg/apache/poi/hslf/model/HeadersFooters;

    .prologue
    .line 989
    const/4 v1, 0x0

    .line 990
    .local v1, "shapesArray":[Lorg/apache/poi/hslf/model/Shape;
    if-eqz p5, :cond_2

    .line 992
    if-eqz p1, :cond_0

    .line 993
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/MasterSheet;->getShapes()[Lorg/apache/poi/hslf/model/Shape;

    move-result-object v1

    .line 999
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 1000
    :try_start_0
    invoke-direct {p0, v1, p5, p6}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shapesToCanvasObjects([Lorg/apache/poi/hslf/model/Shape;ZLorg/apache/poi/hslf/model/HeadersFooters;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1005
    :cond_1
    :goto_1
    return-void

    .line 995
    :cond_2
    if-eqz p2, :cond_0

    .line 996
    invoke-virtual {p2}, Lorg/apache/poi/hslf/model/Slide;->getShapes()[Lorg/apache/poi/hslf/model/Shape;

    move-result-object v1

    goto :goto_0

    .line 1002
    :catch_0
    move-exception v0

    .line 1003
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private readTextFromSlides(Lorg/apache/poi/hslf/model/AutoShape;Lcom/samsung/thumbnail/customview/word/ShapesController;)V
    .locals 38
    .param p1, "txtRun"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "spCont"    # Lcom/samsung/thumbnail/customview/word/ShapesController;

    .prologue
    .line 475
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getTextRun()Lorg/apache/poi/hslf/model/TextRun;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/model/TextRun;->getRichTextRuns()[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    move-result-object v22

    .line 476
    .local v22, "rTxt":[Lorg/apache/poi/hslf/usermodel/RichTextRun;
    const/16 v18, 0x0

    .line 477
    .local v18, "isHyperLink":Z
    const/16 v17, 0x0

    .line 478
    .local v17, "indentLevel":I
    const/4 v9, 0x1

    .line 479
    .local v9, "createList":Z
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getTextRun()Lorg/apache/poi/hslf/model/TextRun;

    move-result-object v35

    invoke-static/range {v35 .. v35}, Lorg/apache/poi/hslf/model/Hyperlink;->find(Lorg/apache/poi/hslf/model/TextRun;)[Lorg/apache/poi/hslf/model/Hyperlink;

    move-result-object v12

    .line 480
    .local v12, "hypLinkArray":[Lorg/apache/poi/hslf/model/Hyperlink;
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 483
    .local v13, "hypLinkNameAndAddress":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v14, 0x0

    .line 484
    .local v14, "hyperLink":[Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;
    if-eqz v12, :cond_4

    array-length v0, v12

    move/from16 v35, v0

    if-lez v35, :cond_4

    .line 485
    array-length v0, v12

    move/from16 v35, v0

    move/from16 v0, v35

    new-array v14, v0, [Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;

    .line 486
    const/4 v7, 0x0

    .line 487
    .local v7, "count":I
    move-object v4, v12

    .local v4, "arr$":[Lorg/apache/poi/hslf/model/Hyperlink;
    array-length v0, v4

    move/from16 v21, v0

    .local v21, "len$":I
    const/16 v16, 0x0

    .local v16, "i$":I
    :goto_0
    move/from16 v0, v16

    move/from16 v1, v21

    if-ge v0, v1, :cond_1

    aget-object v11, v4, v16

    .line 488
    .local v11, "hslflink":Lorg/apache/poi/hslf/model/Hyperlink;
    invoke-virtual {v11}, Lorg/apache/poi/hslf/model/Hyperlink;->getEndIndex()I

    move-result v35

    invoke-virtual {v11}, Lorg/apache/poi/hslf/model/Hyperlink;->getStartIndex()I

    move-result v36

    sub-int v35, v35, v36

    if-lez v35, :cond_0

    .line 489
    new-instance v35, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;

    invoke-virtual {v11}, Lorg/apache/poi/hslf/model/Hyperlink;->getStartIndex()I

    move-result v36

    invoke-virtual {v11}, Lorg/apache/poi/hslf/model/Hyperlink;->getEndIndex()I

    move-result v37

    add-int/lit8 v37, v37, -0x1

    invoke-direct/range {v35 .. v37}, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;-><init>(II)V

    aput-object v35, v14, v7

    .line 494
    :goto_1
    add-int/lit8 v7, v7, 0x1

    .line 487
    add-int/lit8 v16, v16, 0x1

    goto :goto_0

    .line 492
    :cond_0
    const/16 v35, 0x0

    aput-object v35, v14, v7

    goto :goto_1

    .line 496
    .end local v11    # "hslflink":Lorg/apache/poi/hslf/model/Hyperlink;
    :cond_1
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_2
    array-length v0, v12

    move/from16 v35, v0

    move/from16 v0, v35

    if-ge v15, v0, :cond_4

    .line 497
    move/from16 v19, v15

    .local v19, "j":I
    :goto_3
    array-length v0, v12

    move/from16 v35, v0

    move/from16 v0, v19

    move/from16 v1, v35

    if-ge v0, v1, :cond_3

    .line 498
    aget-object v35, v14, v19

    move-object/from16 v0, v35

    iget v0, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->start:I

    move/from16 v35, v0

    aget-object v36, v14, v15

    move-object/from16 v0, v36

    iget v0, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->start:I

    move/from16 v36, v0

    move/from16 v0, v35

    move/from16 v1, v36

    if-ge v0, v1, :cond_2

    .line 499
    aget-object v31, v14, v15

    .line 500
    .local v31, "temp":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;
    aget-object v35, v14, v19

    aput-object v35, v14, v15

    .line 501
    aput-object v31, v14, v19

    .line 497
    .end local v31    # "temp":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;
    :cond_2
    add-int/lit8 v19, v19, 0x1

    goto :goto_3

    .line 496
    :cond_3
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 508
    .end local v4    # "arr$":[Lorg/apache/poi/hslf/model/Hyperlink;
    .end local v7    # "count":I
    .end local v15    # "i":I
    .end local v16    # "i$":I
    .end local v19    # "j":I
    .end local v21    # "len$":I
    :cond_4
    if-eqz v12, :cond_5

    .line 509
    const/4 v15, 0x0

    .restart local v15    # "i":I
    :goto_4
    array-length v0, v12

    move/from16 v35, v0

    move/from16 v0, v35

    if-ge v15, v0, :cond_5

    .line 510
    aget-object v35, v12, v15

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/model/Hyperlink;->getTitle()Ljava/lang/String;

    move-result-object v35

    aget-object v36, v12, v15

    invoke-virtual/range {v36 .. v36}, Lorg/apache/poi/hslf/model/Hyperlink;->getAddress()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-interface {v13, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 509
    add-int/lit8 v15, v15, 0x1

    goto :goto_4

    .line 520
    .end local v15    # "i":I
    :cond_5
    sget-object v6, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->TOP:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    .line 521
    .local v6, "canvasValign":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getVerticalAlignment()I

    move-result v32

    .line 522
    .local v32, "valign":I
    packed-switch v32, :pswitch_data_0

    .line 541
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v25

    .line 543
    .local v25, "slideDim":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getAnchor()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v23

    .line 544
    .local v23, "rect":Lorg/apache/poi/java/awt/Rectangle;
    new-instance v10, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-direct {v10}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    .line 545
    .local v10, "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/java/awt/Rectangle;->getX()D

    move-result-wide v36

    move-object/from16 v0, v25

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v36

    move-wide/from16 v0, v36

    double-to-int v0, v0

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v35, v0

    move/from16 v0, v35

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    .line 546
    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v36

    move-object/from16 v0, v25

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v36

    move-wide/from16 v0, v36

    double-to-int v0, v0

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v35, v0

    move/from16 v0, v35

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setRightMargin(F)V

    .line 547
    invoke-virtual {v10, v6}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    .line 551
    const/16 v28, 0x0

    .line 554
    .local v28, "stringprocessed":I
    const/16 v20, 0x0

    .local v20, "k":I
    :goto_6
    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v35, v0

    move/from16 v0, v20

    move/from16 v1, v35

    if-ge v0, v1, :cond_1e

    .line 555
    aget-object v35, v22, v20

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getBulletColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v35

    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    .line 557
    .local v5, "bulletRGB":Ljava/lang/String;
    const/16 v35, 0x2

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v36

    move/from16 v0, v35

    move/from16 v1, v36

    invoke-virtual {v5, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 558
    new-instance v27, Ljava/util/StringTokenizer;

    aget-object v35, v22, v20

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getText()Ljava/lang/String;

    move-result-object v35

    const-string/jumbo v36, "\n"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    .local v27, "strToken":Ljava/util/StringTokenizer;
    aget-object v35, v22, v20

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getText()Ljava/lang/String;

    move-result-object v35

    if-eqz v35, :cond_7

    aget-object v35, v22, v20

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getText()Ljava/lang/String;

    move-result-object v35

    const-string/jumbo v36, "\n"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v35

    if-eqz v35, :cond_7

    .line 561
    if-eqz p2, :cond_6

    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getContents()Ljava/util/ArrayList;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/util/ArrayList;->size()I

    move-result v35

    if-eqz v35, :cond_6

    .line 562
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 563
    :cond_6
    new-instance v10, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .end local v10    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-direct {v10}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    .line 564
    .restart local v10    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/java/awt/Rectangle;->getX()D

    move-result-wide v36

    move-object/from16 v0, v25

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v36

    move-wide/from16 v0, v36

    double-to-int v0, v0

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v35, v0

    move/from16 v0, v35

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    .line 565
    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v36

    move-object/from16 v0, v25

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v36

    move-wide/from16 v0, v36

    double-to-int v0, v0

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v35, v0

    move/from16 v0, v35

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setRightMargin(F)V

    .line 567
    invoke-virtual {v10, v6}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    .line 571
    :cond_7
    invoke-virtual/range {v27 .. v27}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v35

    const/16 v36, 0x1

    move/from16 v0, v35

    move/from16 v1, v36

    if-le v0, v1, :cond_13

    .line 572
    :cond_8
    :goto_7
    invoke-virtual/range {v27 .. v27}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v35

    if-eqz v35, :cond_1d

    .line 573
    invoke-virtual/range {v27 .. v27}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v33

    .line 575
    .local v33, "value":Ljava/lang/String;
    aget-object v35, v22, v20

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isBullet()Z

    move-result v35

    if-eqz v35, :cond_d

    .line 579
    aget-object v35, v22, v20

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getIndentLevel()I

    move-result v35

    move/from16 v0, v35

    move/from16 v1, v17

    if-ge v0, v1, :cond_9

    .line 580
    aget-object v35, v22, v20

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getIndentLevel()I

    move-result v17

    .line 583
    :cond_9
    if-eqz v9, :cond_c

    .line 585
    const/4 v9, 0x0

    .line 590
    :cond_a
    :goto_8
    new-instance v24, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-direct/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/word/Run;-><init>()V

    .line 592
    .local v24, "run":Lcom/samsung/thumbnail/customview/word/Run;
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v36, "\u2022 "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v24

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    .line 593
    aget-object v35, v22, v20

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getFontColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v35

    move-object/from16 v0, v24

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 594
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v35

    aget-object v36, v22, v20

    invoke-virtual/range {v36 .. v36}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getFontSize()I

    move-result v36

    move/from16 v0, v36

    int-to-double v0, v0

    move-wide/from16 v36, v0

    invoke-virtual/range {v35 .. v37}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v36

    move-wide/from16 v0, v36

    double-to-float v0, v0

    move/from16 v35, v0

    const v36, 0x3f666666    # 0.9f

    mul-float v35, v35, v36

    move-object/from16 v0, v24

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 599
    move-object/from16 v0, v24

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 600
    if-eqz p2, :cond_b

    .line 601
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 602
    :cond_b
    new-instance v10, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .end local v10    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-direct {v10}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    .line 603
    .restart local v10    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/java/awt/Rectangle;->getX()D

    move-result-wide v36

    move-object/from16 v0, v25

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v36

    move-wide/from16 v0, v36

    double-to-int v0, v0

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v35, v0

    move/from16 v0, v35

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    .line 605
    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v36

    move-object/from16 v0, v25

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v36

    move-wide/from16 v0, v36

    double-to-int v0, v0

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v35, v0

    move/from16 v0, v35

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setRightMargin(F)V

    .line 607
    invoke-virtual {v10, v6}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    goto/16 :goto_7

    .line 527
    .end local v5    # "bulletRGB":Ljava/lang/String;
    .end local v10    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v20    # "k":I
    .end local v23    # "rect":Lorg/apache/poi/java/awt/Rectangle;
    .end local v24    # "run":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v25    # "slideDim":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    .end local v27    # "strToken":Ljava/util/StringTokenizer;
    .end local v28    # "stringprocessed":I
    .end local v33    # "value":Ljava/lang/String;
    :pswitch_0
    sget-object v6, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->TOP:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    .line 528
    goto/16 :goto_5

    .line 533
    :pswitch_1
    sget-object v6, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->BOTTOM:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    .line 534
    goto/16 :goto_5

    .line 537
    :pswitch_2
    sget-object v6, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->CENTERED:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    goto/16 :goto_5

    .line 586
    .restart local v5    # "bulletRGB":Ljava/lang/String;
    .restart local v10    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .restart local v20    # "k":I
    .restart local v23    # "rect":Lorg/apache/poi/java/awt/Rectangle;
    .restart local v25    # "slideDim":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    .restart local v27    # "strToken":Ljava/util/StringTokenizer;
    .restart local v28    # "stringprocessed":I
    .restart local v33    # "value":Ljava/lang/String;
    :cond_c
    aget-object v35, v22, v20

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getIndentLevel()I

    move-result v35

    move/from16 v0, v17

    move/from16 v1, v35

    if-ge v0, v1, :cond_a

    .line 588
    aget-object v35, v22, v20

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getIndentLevel()I

    move-result v17

    goto/16 :goto_8

    .line 612
    :cond_d
    if-nez v18, :cond_8

    .line 615
    new-instance v24, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-direct/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/word/Run;-><init>()V

    .line 616
    .restart local v24    # "run":Lcom/samsung/thumbnail/customview/word/Run;
    move-object/from16 v0, v24

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    .line 617
    aget-object v35, v22, v20

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getFontColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v35

    move-object/from16 v0, v24

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 618
    aget-object v35, v22, v20

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getAlignment()I

    move-result v35

    packed-switch v35, :pswitch_data_1

    .line 633
    sget-object v35, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->LEFT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v35

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    .line 635
    :goto_9
    aget-object v35, v22, v20

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isBold()Z

    move-result v35

    if-eqz v35, :cond_e

    .line 636
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/word/Run;->setTextBold()V

    .line 638
    :cond_e
    aget-object v35, v22, v20

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isItalic()Z

    move-result v35

    if-eqz v35, :cond_f

    .line 639
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/word/Run;->setTextItalics()V

    .line 641
    :cond_f
    aget-object v35, v22, v20

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isUnderlined()Z

    move-result v35

    if-eqz v35, :cond_10

    .line 642
    const/16 v35, 0x1

    move-object/from16 v0, v24

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setUnderlineText(Z)V

    .line 644
    :cond_10
    aget-object v35, v22, v20

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isStrikethrough()Z

    move-result v35

    if-eqz v35, :cond_11

    .line 645
    const/16 v35, 0x1

    move-object/from16 v0, v24

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setStrikeThruText(Z)V

    .line 647
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v35

    aget-object v36, v22, v20

    invoke-virtual/range {v36 .. v36}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getFontSize()I

    move-result v36

    move/from16 v0, v36

    int-to-double v0, v0

    move-wide/from16 v36, v0

    invoke-virtual/range {v35 .. v37}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v36

    move-wide/from16 v0, v36

    double-to-float v0, v0

    move/from16 v35, v0

    const v36, 0x3f666666    # 0.9f

    mul-float v35, v35, v36

    move-object/from16 v0, v24

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 651
    move-object/from16 v0, v24

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 652
    invoke-virtual/range {v27 .. v27}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v35

    if-eqz v35, :cond_8

    .line 653
    if-eqz p2, :cond_12

    .line 654
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 655
    :cond_12
    new-instance v10, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .end local v10    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-direct {v10}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    .line 656
    .restart local v10    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/java/awt/Rectangle;->getX()D

    move-result-wide v36

    move-object/from16 v0, v25

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v36

    move-wide/from16 v0, v36

    double-to-int v0, v0

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v35, v0

    move/from16 v0, v35

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    .line 658
    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v36

    move-object/from16 v0, v25

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v36

    move-wide/from16 v0, v36

    double-to-int v0, v0

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v35, v0

    move/from16 v0, v35

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setRightMargin(F)V

    .line 660
    invoke-virtual {v10, v6}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    goto/16 :goto_7

    .line 622
    :pswitch_3
    sget-object v35, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->LEFT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v35

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_9

    .line 627
    :pswitch_4
    sget-object v35, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->CENTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v35

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_9

    .line 630
    :pswitch_5
    sget-object v35, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->RIGHT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v35

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_9

    .line 668
    .end local v24    # "run":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v33    # "value":Ljava/lang/String;
    :cond_13
    invoke-virtual/range {v27 .. v27}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v35

    const/16 v36, 0x1

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_1d

    .line 669
    invoke-virtual/range {v27 .. v27}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v33

    .line 672
    .restart local v33    # "value":Ljava/lang/String;
    move/from16 v26, v28

    .line 673
    .local v26, "startprocess":I
    invoke-virtual/range {v33 .. v33}, Ljava/lang/String;->length()I

    move-result v35

    add-int v35, v35, v28

    add-int/lit8 v28, v35, -0x1

    .line 674
    move-object/from16 v0, p0

    move/from16 v1, v26

    move/from16 v2, v28

    move-object/from16 v3, v33

    invoke-virtual {v0, v1, v2, v14, v3}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->getRuns(II[Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v30

    .line 676
    .local v30, "subruns":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;>;"
    const/4 v8, 0x0

    .line 677
    .local v8, "countruns":I
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :goto_a
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v35

    if-eqz v35, :cond_1a

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;

    .line 678
    .local v29, "subrun":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    move-object/from16 v0, v29

    iget-object v0, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    move-object/from16 v34, v0

    .line 679
    .local v34, "valueInRun":Ljava/lang/String;
    new-instance v24, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-direct/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/word/Run;-><init>()V

    .line 680
    .restart local v24    # "run":Lcom/samsung/thumbnail/customview/word/Run;
    aget-object v35, v22, v20

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isBullet()Z

    move-result v35

    if-eqz v35, :cond_18

    if-nez v8, :cond_18

    .line 681
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v36, "\u2022 "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v24

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    .line 685
    :goto_b
    move-object/from16 v0, v29

    iget-boolean v0, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->isHyperLink:Z

    move/from16 v35, v0

    if-eqz v35, :cond_19

    .line 686
    sget-object v35, Lorg/apache/poi/java/awt/Color;->BLUE:Lorg/apache/poi/java/awt/Color;

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v35

    move-object/from16 v0, v24

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 687
    const/16 v35, 0x1

    move-object/from16 v0, v24

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setUnderlineText(Z)V

    .line 691
    :goto_c
    aget-object v35, v22, v20

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getAlignment()I

    move-result v35

    packed-switch v35, :pswitch_data_2

    .line 706
    sget-object v35, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->LEFT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v35

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    .line 708
    :goto_d
    aget-object v35, v22, v20

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isBold()Z

    move-result v35

    if-eqz v35, :cond_14

    .line 709
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/word/Run;->setTextBold()V

    .line 711
    :cond_14
    aget-object v35, v22, v20

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isItalic()Z

    move-result v35

    if-eqz v35, :cond_15

    .line 712
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/word/Run;->setTextItalics()V

    .line 714
    :cond_15
    aget-object v35, v22, v20

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isUnderlined()Z

    move-result v35

    if-eqz v35, :cond_16

    .line 715
    const/16 v35, 0x1

    move-object/from16 v0, v24

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setUnderlineText(Z)V

    .line 717
    :cond_16
    aget-object v35, v22, v20

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isStrikethrough()Z

    move-result v35

    if-eqz v35, :cond_17

    .line 718
    const/16 v35, 0x1

    move-object/from16 v0, v24

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setStrikeThruText(Z)V

    .line 720
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v35

    aget-object v36, v22, v20

    invoke-virtual/range {v36 .. v36}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getFontSize()I

    move-result v36

    move/from16 v0, v36

    int-to-double v0, v0

    move-wide/from16 v36, v0

    invoke-virtual/range {v35 .. v37}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v36

    move-wide/from16 v0, v36

    double-to-float v0, v0

    move/from16 v35, v0

    const v36, 0x3f666666    # 0.9f

    mul-float v35, v35, v36

    move-object/from16 v0, v24

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 723
    move-object/from16 v0, v24

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 724
    add-int/lit8 v8, v8, 0x1

    .line 725
    goto/16 :goto_a

    .line 683
    :cond_18
    move-object/from16 v0, v24

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    goto/16 :goto_b

    .line 689
    :cond_19
    aget-object v35, v22, v20

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getFontColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v35

    move-object/from16 v0, v24

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    goto/16 :goto_c

    .line 695
    :pswitch_6
    sget-object v35, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->LEFT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v35

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_d

    .line 700
    :pswitch_7
    sget-object v35, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->CENTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v35

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_d

    .line 703
    :pswitch_8
    sget-object v35, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->RIGHT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v35

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_d

    .line 728
    .end local v24    # "run":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v29    # "subrun":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    .end local v34    # "valueInRun":Ljava/lang/String;
    :cond_1a
    aget-object v35, v22, v20

    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getText()Ljava/lang/String;

    move-result-object v35

    const-string/jumbo v36, "\n"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v35

    if-eqz v35, :cond_1c

    .line 729
    if-eqz p2, :cond_1b

    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getContents()Ljava/util/ArrayList;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/util/ArrayList;->size()I

    move-result v35

    if-eqz v35, :cond_1b

    .line 730
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 731
    :cond_1b
    new-instance v10, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .end local v10    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-direct {v10}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    .line 732
    .restart local v10    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/java/awt/Rectangle;->getX()D

    move-result-wide v36

    move-object/from16 v0, v25

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v36

    move-wide/from16 v0, v36

    double-to-int v0, v0

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v35, v0

    move/from16 v0, v35

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    .line 734
    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v36

    move-object/from16 v0, v25

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v36

    move-wide/from16 v0, v36

    double-to-int v0, v0

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v35, v0

    move/from16 v0, v35

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setRightMargin(F)V

    .line 736
    invoke-virtual {v10, v6}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    .line 740
    :cond_1c
    add-int/lit8 v28, v28, 0x1

    .line 554
    .end local v8    # "countruns":I
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v26    # "startprocess":I
    .end local v30    # "subruns":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;>;"
    .end local v33    # "value":Ljava/lang/String;
    :cond_1d
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_6

    .line 743
    .end local v5    # "bulletRGB":Ljava/lang/String;
    .end local v27    # "strToken":Ljava/util/StringTokenizer;
    :cond_1e
    if-eqz p2, :cond_1f

    .line 744
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 746
    :cond_1f
    return-void

    .line 522
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 618
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 691
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private readTextFromSlides(Lorg/apache/poi/hslf/model/TextBox;Lcom/samsung/thumbnail/customview/word/ShapesController;)V
    .locals 26
    .param p1, "txtRun"    # Lorg/apache/poi/hslf/model/TextBox;
    .param p2, "spCon"    # Lcom/samsung/thumbnail/customview/word/ShapesController;

    .prologue
    .line 756
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/TextBox;->getTextRun()Lorg/apache/poi/hslf/model/TextRun;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/model/TextRun;->getRichTextRuns()[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    move-result-object v14

    .line 757
    .local v14, "rTxt":[Lorg/apache/poi/hslf/usermodel/RichTextRun;
    const/4 v12, 0x0

    .line 758
    .local v12, "isHyperLink":Z
    const/4 v11, 0x0

    .line 759
    .local v11, "indentLevel":I
    const/4 v6, 0x1

    .line 760
    .local v6, "createList":Z
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/TextBox;->getTextRun()Lorg/apache/poi/hslf/model/TextRun;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lorg/apache/poi/hslf/model/Hyperlink;->find(Lorg/apache/poi/hslf/model/TextRun;)[Lorg/apache/poi/hslf/model/Hyperlink;

    move-result-object v8

    .line 761
    .local v8, "hypLinkArray":[Lorg/apache/poi/hslf/model/Hyperlink;
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 762
    .local v9, "hypLinkNameAndAddress":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v8, :cond_0

    .line 763
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    array-length v0, v8

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v10, v0, :cond_0

    .line 764
    aget-object v22, v8, v10

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/model/Hyperlink;->getTitle()Ljava/lang/String;

    move-result-object v22

    aget-object v23, v8, v10

    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/hslf/model/Hyperlink;->getAddress()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-interface {v9, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 763
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 774
    .end local v10    # "i":I
    :cond_0
    sget-object v5, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->TOP:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    .line 775
    .local v5, "canvasValign":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/TextBox;->getVerticalAlignment()I

    move-result v20

    .line 776
    .local v20, "valign":I
    packed-switch v20, :pswitch_data_0

    .line 795
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v18

    .line 797
    .local v18, "slideDim":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/TextBox;->getAnchor()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v16

    .line 798
    .local v16, "rect1":Lorg/apache/poi/java/awt/Rectangle;
    new-instance v7, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-direct {v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    .line 799
    .local v7, "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/java/awt/Rectangle;->getX()D

    move-result-wide v22

    move-object/from16 v0, v18

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    .line 800
    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v22

    move-object/from16 v0, v18

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setRightMargin(F)V

    .line 801
    const/4 v13, 0x0

    .local v13, "k":I
    :goto_2
    array-length v0, v14

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v13, v0, :cond_16

    .line 802
    aget-object v22, v14, v13

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getBulletColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    .line 804
    .local v4, "bulletRGB":Ljava/lang/String;
    const/16 v22, 0x2

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v4, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 805
    new-instance v19, Ljava/util/StringTokenizer;

    aget-object v22, v14, v13

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getText()Ljava/lang/String;

    move-result-object v22

    const-string/jumbo v23, "\n"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 807
    .local v19, "strToken":Ljava/util/StringTokenizer;
    aget-object v22, v14, v13

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getText()Ljava/lang/String;

    move-result-object v22

    if-eqz v22, :cond_2

    aget-object v22, v14, v13

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getText()Ljava/lang/String;

    move-result-object v22

    const-string/jumbo v23, "\n"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_2

    .line 808
    if-eqz p2, :cond_1

    .line 809
    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 810
    :cond_1
    new-instance v7, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .end local v7    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-direct {v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    .line 811
    .restart local v7    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/java/awt/Rectangle;->getX()D

    move-result-wide v22

    move-object/from16 v0, v18

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    .line 813
    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v22

    move-object/from16 v0, v18

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setRightMargin(F)V

    .line 815
    invoke-virtual {v7, v5}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    .line 819
    :cond_2
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_e

    .line 820
    :cond_3
    :goto_3
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v22

    if-eqz v22, :cond_14

    .line 821
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v21

    .line 823
    .local v21, "value":Ljava/lang/String;
    aget-object v22, v14, v13

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isBullet()Z

    move-result v22

    if-eqz v22, :cond_8

    .line 825
    aget-object v22, v14, v13

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getIndentLevel()I

    move-result v22

    move/from16 v0, v22

    if-ge v0, v11, :cond_4

    .line 826
    aget-object v22, v14, v13

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getIndentLevel()I

    move-result v11

    .line 829
    :cond_4
    if-eqz v6, :cond_7

    .line 831
    const/4 v6, 0x0

    .line 836
    :cond_5
    :goto_4
    new-instance v17, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/word/Run;-><init>()V

    .line 838
    .local v17, "run":Lcom/samsung/thumbnail/customview/word/Run;
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v23, "\u2022 "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    .line 839
    aget-object v22, v14, v13

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getFontColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v22

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 840
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v22

    aget-object v23, v14, v13

    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getFontSize()I

    move-result v23

    move/from16 v0, v23

    int-to-double v0, v0

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-float v0, v0

    move/from16 v22, v0

    const v23, 0x3f666666    # 0.9f

    mul-float v22, v22, v23

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 845
    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 846
    if-eqz p2, :cond_6

    .line 847
    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 848
    :cond_6
    new-instance v7, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .end local v7    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-direct {v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    .line 849
    .restart local v7    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/java/awt/Rectangle;->getX()D

    move-result-wide v22

    move-object/from16 v0, v18

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    .line 851
    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v22

    move-object/from16 v0, v18

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setRightMargin(F)V

    .line 853
    invoke-virtual {v7, v5}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    goto/16 :goto_3

    .line 781
    .end local v4    # "bulletRGB":Ljava/lang/String;
    .end local v7    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v13    # "k":I
    .end local v16    # "rect1":Lorg/apache/poi/java/awt/Rectangle;
    .end local v17    # "run":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v18    # "slideDim":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    .end local v19    # "strToken":Ljava/util/StringTokenizer;
    .end local v21    # "value":Ljava/lang/String;
    :pswitch_0
    sget-object v5, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->TOP:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    .line 782
    goto/16 :goto_1

    .line 787
    :pswitch_1
    sget-object v5, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->BOTTOM:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    .line 788
    goto/16 :goto_1

    .line 791
    :pswitch_2
    sget-object v5, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->CENTERED:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    goto/16 :goto_1

    .line 832
    .restart local v4    # "bulletRGB":Ljava/lang/String;
    .restart local v7    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .restart local v13    # "k":I
    .restart local v16    # "rect1":Lorg/apache/poi/java/awt/Rectangle;
    .restart local v18    # "slideDim":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    .restart local v19    # "strToken":Ljava/util/StringTokenizer;
    .restart local v21    # "value":Ljava/lang/String;
    :cond_7
    aget-object v22, v14, v13

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getIndentLevel()I

    move-result v22

    move/from16 v0, v22

    if-ge v11, v0, :cond_5

    .line 834
    aget-object v22, v14, v13

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getIndentLevel()I

    move-result v11

    goto/16 :goto_4

    .line 858
    :cond_8
    if-nez v12, :cond_3

    .line 861
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/TextBox;->getAnchor()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v15

    .line 862
    .local v15, "rect":Lorg/apache/poi/java/awt/Rectangle;
    new-instance v17, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/word/Run;-><init>()V

    .line 863
    .restart local v17    # "run":Lcom/samsung/thumbnail/customview/word/Run;
    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    .line 864
    aget-object v22, v14, v13

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getFontColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v22

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 865
    aget-object v22, v14, v13

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getAlignment()I

    move-result v22

    packed-switch v22, :pswitch_data_1

    .line 880
    sget-object v22, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->LEFT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    .line 882
    :goto_5
    aget-object v22, v14, v13

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isBold()Z

    move-result v22

    if-eqz v22, :cond_9

    .line 883
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/word/Run;->setTextBold()V

    .line 885
    :cond_9
    aget-object v22, v14, v13

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isItalic()Z

    move-result v22

    if-eqz v22, :cond_a

    .line 886
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/word/Run;->setTextItalics()V

    .line 888
    :cond_a
    aget-object v22, v14, v13

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isUnderlined()Z

    move-result v22

    if-eqz v22, :cond_b

    .line 889
    const/16 v22, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setUnderlineText(Z)V

    .line 891
    :cond_b
    aget-object v22, v14, v13

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isStrikethrough()Z

    move-result v22

    if-eqz v22, :cond_c

    .line 892
    const/16 v22, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setStrikeThruText(Z)V

    .line 894
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v22

    aget-object v23, v14, v13

    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getFontSize()I

    move-result v23

    move/from16 v0, v23

    int-to-double v0, v0

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-float v0, v0

    move/from16 v22, v0

    const v23, 0x3f666666    # 0.9f

    mul-float v22, v22, v23

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 898
    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 899
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v22

    if-eqz v22, :cond_3

    .line 900
    if-eqz p2, :cond_d

    .line 901
    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 902
    :cond_d
    new-instance v7, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .end local v7    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-direct {v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    .line 903
    .restart local v7    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-virtual {v15}, Lorg/apache/poi/java/awt/Rectangle;->getX()D

    move-result-wide v22

    move-object/from16 v0, v18

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    .line 905
    invoke-virtual {v15}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v22

    move-object/from16 v0, v18

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setRightMargin(F)V

    .line 907
    invoke-virtual {v7, v5}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    goto/16 :goto_3

    .line 869
    :pswitch_3
    sget-object v22, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->LEFT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_5

    .line 874
    :pswitch_4
    sget-object v22, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->CENTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_5

    .line 877
    :pswitch_5
    sget-object v22, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->RIGHT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_5

    .line 915
    .end local v15    # "rect":Lorg/apache/poi/java/awt/Rectangle;
    .end local v17    # "run":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v21    # "value":Ljava/lang/String;
    :cond_e
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_14

    .line 916
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v21

    .line 917
    .restart local v21    # "value":Ljava/lang/String;
    new-instance v17, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/word/Run;-><init>()V

    .line 918
    .restart local v17    # "run":Lcom/samsung/thumbnail/customview/word/Run;
    aget-object v22, v14, v13

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isBullet()Z

    move-result v22

    if-eqz v22, :cond_15

    .line 919
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v23, "\u2022 "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    .line 923
    :goto_6
    aget-object v22, v14, v13

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getFontColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v22

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 924
    aget-object v22, v14, v13

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getAlignment()I

    move-result v22

    packed-switch v22, :pswitch_data_2

    .line 939
    sget-object v22, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->LEFT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    .line 941
    :goto_7
    aget-object v22, v14, v13

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isBold()Z

    move-result v22

    if-eqz v22, :cond_f

    .line 942
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/word/Run;->setTextBold()V

    .line 944
    :cond_f
    aget-object v22, v14, v13

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isItalic()Z

    move-result v22

    if-eqz v22, :cond_10

    .line 945
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/word/Run;->setTextItalics()V

    .line 947
    :cond_10
    aget-object v22, v14, v13

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isUnderlined()Z

    move-result v22

    if-eqz v22, :cond_11

    .line 948
    const/16 v22, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setUnderlineText(Z)V

    .line 950
    :cond_11
    aget-object v22, v14, v13

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isStrikethrough()Z

    move-result v22

    if-eqz v22, :cond_12

    .line 951
    const/16 v22, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setStrikeThruText(Z)V

    .line 953
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v22

    aget-object v23, v14, v13

    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getFontSize()I

    move-result v23

    move/from16 v0, v23

    int-to-double v0, v0

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-float v0, v0

    move/from16 v22, v0

    const v23, 0x3f666666    # 0.9f

    mul-float v22, v22, v23

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 956
    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 957
    aget-object v22, v14, v13

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getText()Ljava/lang/String;

    move-result-object v22

    const-string/jumbo v23, "\n"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_14

    .line 958
    if-eqz p2, :cond_13

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getContents()Ljava/util/ArrayList;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v22

    if-eqz v22, :cond_13

    .line 959
    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 960
    :cond_13
    new-instance v7, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .end local v7    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-direct {v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    .line 961
    .restart local v7    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/java/awt/Rectangle;->getX()D

    move-result-wide v22

    move-object/from16 v0, v18

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    .line 963
    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/java/awt/Rectangle;->getWidth()D

    move-result-wide v22

    move-object/from16 v0, v18

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setRightMargin(F)V

    .line 965
    invoke-virtual {v7, v5}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    .line 801
    .end local v17    # "run":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v21    # "value":Ljava/lang/String;
    :cond_14
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_2

    .line 921
    .restart local v17    # "run":Lcom/samsung/thumbnail/customview/word/Run;
    .restart local v21    # "value":Ljava/lang/String;
    :cond_15
    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 928
    :pswitch_6
    sget-object v22, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->LEFT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_7

    .line 933
    :pswitch_7
    sget-object v22, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->CENTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_7

    .line 936
    :pswitch_8
    sget-object v22, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->RIGHT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_7

    .line 971
    .end local v4    # "bulletRGB":Ljava/lang/String;
    .end local v17    # "run":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v19    # "strToken":Ljava/util/StringTokenizer;
    .end local v21    # "value":Ljava/lang/String;
    :cond_16
    if-eqz p2, :cond_17

    .line 972
    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 974
    :cond_17
    return-void

    .line 776
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 865
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 924
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private shapesToCanvasObjects([Lorg/apache/poi/hslf/model/Shape;ZLorg/apache/poi/hslf/model/HeadersFooters;)V
    .locals 23
    .param p1, "shapesArray"    # [Lorg/apache/poi/hslf/model/Shape;
    .param p2, "masterFlag"    # Z
    .param p3, "headerFooter"    # Lorg/apache/poi/hslf/model/HeadersFooters;

    .prologue
    .line 1017
    const/16 v20, 0x0

    .local v20, "shapeCount":I
    :goto_0
    move-object/from16 v0, p1

    array-length v5, v0

    move/from16 v0, v20

    if-ge v0, v5, :cond_9

    .line 1018
    new-instance v16, Lorg/apache/poi/hslf/model/Fill;

    aget-object v5, p1, v20

    move-object/from16 v0, v16

    invoke-direct {v0, v5}, Lorg/apache/poi/hslf/model/Fill;-><init>(Lorg/apache/poi/hslf/model/Shape;)V

    .line 1022
    .local v16, "fill_shape":Lorg/apache/poi/hslf/model/Fill;
    if-eqz p2, :cond_1

    .line 1023
    aget-object v5, p1, v20

    invoke-static {v5}, Lorg/apache/poi/hslf/model/MasterSheet;->isPlaceholder(Lorg/apache/poi/hslf/model/Shape;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1017
    :cond_0
    :goto_1
    add-int/lit8 v20, v20, 0x1

    goto :goto_0

    .line 1029
    :cond_1
    aget-object v5, p1, v20

    instance-of v5, v5, Lorg/apache/poi/hslf/model/Line;

    if-eqz v5, :cond_2

    .line 1030
    aget-object v18, p1, v20

    check-cast v18, Lorg/apache/poi/hslf/model/Line;

    .line 1031
    .local v18, "line":Lorg/apache/poi/hslf/model/Line;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->checkLineSpace(Lorg/apache/poi/hslf/model/Line;)V

    goto :goto_1

    .line 1032
    .end local v18    # "line":Lorg/apache/poi/hslf/model/Line;
    :cond_2
    aget-object v5, p1, v20

    instance-of v5, v5, Lorg/apache/poi/hslf/model/AutoShape;

    if-eqz v5, :cond_3

    .line 1033
    aget-object v5, p1, v20

    move-object/from16 v0, p0

    move/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v5, v1, v2}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->createAutoShapes(Lorg/apache/poi/hslf/model/Shape;ZLorg/apache/poi/hslf/model/HeadersFooters;)V

    goto :goto_1

    .line 1035
    :cond_3
    aget-object v5, p1, v20

    instance-of v5, v5, Lorg/apache/poi/hslf/model/TextBox;

    if-eqz v5, :cond_6

    .line 1036
    aget-object v6, p1, v20

    check-cast v6, Lorg/apache/poi/hslf/model/TextBox;

    .line 1037
    .local v6, "shape":Lorg/apache/poi/hslf/model/TextBox;
    const/16 v22, 0x0

    .line 1038
    .local v22, "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual {v6}, Lorg/apache/poi/hslf/model/TextBox;->getAnchor()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v14

    .line 1040
    .local v14, "anchor":Lorg/apache/poi/java/awt/Rectangle;
    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/hslf/model/Fill;->getForegroundColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 1044
    :cond_4
    :try_start_0
    invoke-virtual {v6}, Lorg/apache/poi/hslf/model/TextBox;->getText()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_a

    .line 1045
    if-eqz v14, :cond_5

    .line 1046
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v5

    iget v7, v14, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v8, v7

    invoke-virtual {v5, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v8

    double-to-int v5, v8

    iput v5, v14, Lorg/apache/poi/java/awt/Rectangle;->x:I

    .line 1048
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v5

    iget v7, v14, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v8, v7

    invoke-virtual {v5, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v8

    double-to-int v5, v8

    iput v5, v14, Lorg/apache/poi/java/awt/Rectangle;->y:I

    .line 1050
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v5

    iget v7, v14, Lorg/apache/poi/java/awt/Rectangle;->width:I

    int-to-double v8, v7

    invoke-virtual {v5, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v8

    double-to-int v5, v8

    iput v5, v14, Lorg/apache/poi/java/awt/Rectangle;->width:I

    .line 1053
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v5

    iget v7, v14, Lorg/apache/poi/java/awt/Rectangle;->height:I

    int-to-double v8, v7

    invoke-virtual {v5, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v8

    double-to-int v5, v8

    iput v5, v14, Lorg/apache/poi/java/awt/Rectangle;->height:I

    .line 1057
    :cond_5
    new-instance v4, Lcom/samsung/thumbnail/customview/word/ShapesController;

    sget-object v5, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    iget v7, v14, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v8, v14, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v9, v14, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v10, v14, Lorg/apache/poi/java/awt/Rectangle;->height:I

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shape_Count:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    invoke-direct/range {v4 .. v13}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Lorg/apache/poi/hslf/model/Shape;IIIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1061
    .end local v22    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    .local v4, "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :try_start_1
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->readTextFromSlides(Lorg/apache/poi/hslf/model/TextBox;Lcom/samsung/thumbnail/customview/word/ShapesController;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1066
    :goto_2
    if-eqz v4, :cond_0

    .line 1067
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v5, v4}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 1063
    .end local v4    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    .restart local v22    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :catch_0
    move-exception v15

    move-object/from16 v4, v22

    .line 1064
    .end local v22    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    .restart local v4    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    .local v15, "e":Ljava/lang/Exception;
    :goto_3
    const-string/jumbo v5, "DocumentService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v15}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1068
    .end local v4    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    .end local v6    # "shape":Lorg/apache/poi/hslf/model/TextBox;
    .end local v14    # "anchor":Lorg/apache/poi/java/awt/Rectangle;
    .end local v15    # "e":Ljava/lang/Exception;
    :cond_6
    aget-object v5, p1, v20

    instance-of v5, v5, Lorg/apache/poi/hslf/model/Picture;

    if-nez v5, :cond_7

    aget-object v5, p1, v20

    instance-of v5, v5, Lorg/apache/poi/hslf/model/OLEShape;

    if-eqz v5, :cond_8

    .line 1070
    :cond_7
    aget-object v19, p1, v20

    check-cast v19, Lorg/apache/poi/hslf/model/Picture;

    .line 1071
    .local v19, "picture":Lorg/apache/poi/hslf/model/Picture;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->createPicture(Lorg/apache/poi/hslf/model/Picture;)V

    goto/16 :goto_1

    .line 1072
    .end local v19    # "picture":Lorg/apache/poi/hslf/model/Picture;
    :cond_8
    aget-object v5, p1, v20

    instance-of v5, v5, Lorg/apache/poi/hslf/model/ShapeGroup;

    if-eqz v5, :cond_0

    .line 1079
    aget-object v17, p1, v20

    check-cast v17, Lorg/apache/poi/hslf/model/ShapeGroup;

    .line 1080
    .local v17, "group":Lorg/apache/poi/hslf/model/ShapeGroup;
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/hslf/model/ShapeGroup;->getShapes()[Lorg/apache/poi/hslf/model/Shape;

    move-result-object v21

    .line 1081
    .local v21, "shapeGroup_shapes":[Lorg/apache/poi/hslf/model/Shape;
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->shapesToCanvasObjects([Lorg/apache/poi/hslf/model/Shape;ZLorg/apache/poi/hslf/model/HeadersFooters;)V

    goto/16 :goto_1

    .line 1085
    .end local v16    # "fill_shape":Lorg/apache/poi/hslf/model/Fill;
    .end local v17    # "group":Lorg/apache/poi/hslf/model/ShapeGroup;
    .end local v21    # "shapeGroup_shapes":[Lorg/apache/poi/hslf/model/Shape;
    :cond_9
    return-void

    .line 1063
    .restart local v4    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    .restart local v6    # "shape":Lorg/apache/poi/hslf/model/TextBox;
    .restart local v14    # "anchor":Lorg/apache/poi/java/awt/Rectangle;
    .restart local v16    # "fill_shape":Lorg/apache/poi/hslf/model/Fill;
    :catch_1
    move-exception v15

    goto :goto_3

    .end local v4    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    .restart local v22    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_a
    move-object/from16 v4, v22

    .end local v22    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    .restart local v4    # "spCon":Lcom/samsung/thumbnail/customview/word/ShapesController;
    goto :goto_2
.end method


# virtual methods
.method public getCanvasEleCreater()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    return-object v0
.end method

.method public getFirstPageCanvas()Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    return-object v0
.end method

.method public getFolderPath()Ljava/io/File;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->folderPath:Ljava/io/File;

    return-object v0
.end method

.method public getRuns(II[Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 11
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "hyperlinks"    # [Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;
    .param p4, "str"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II[",
            "Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 352
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 353
    .local v5, "retList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;>;"
    if-eqz p3, :cond_0

    array-length v7, p3

    if-nez v7, :cond_2

    .line 354
    :cond_0
    new-instance v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;

    invoke-direct {v6, v9}, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;-><init>(Lcom/samsung/thumbnail/office/point/PPTCustomParser$1;)V

    .line 355
    .local v6, "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    iput-object p4, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    .line 356
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 465
    .end local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    :cond_1
    :goto_0
    return-object v5

    .line 359
    :cond_2
    move v4, p1

    .line 360
    .local v4, "offsetinstr":I
    move-object v0, p3

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_f

    aget-object v3, v0, v1

    .line 362
    .local v3, "link":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;
    if-eqz v3, :cond_3

    iget v7, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->end:I

    if-lt v7, p1, :cond_3

    iget v7, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->start:I

    if-le v7, p2, :cond_4

    .line 360
    :cond_3
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 366
    :cond_4
    iget v7, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->start:I

    if-le v4, v7, :cond_5

    iget v7, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->end:I

    if-ne v7, p2, :cond_5

    .line 367
    new-instance v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;

    invoke-direct {v6, v9}, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;-><init>(Lcom/samsung/thumbnail/office/point/PPTCustomParser$1;)V

    .line 368
    .restart local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    sub-int v7, v4, p1

    iget v8, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->end:I

    sub-int/2addr v8, p1

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {p4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    .line 370
    iget-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v4, v7

    .line 371
    iput-boolean v10, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->isHyperLink:Z

    .line 372
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 373
    .end local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    :cond_5
    iget v7, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->start:I

    if-le v4, v7, :cond_6

    iget v7, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->end:I

    if-ge v7, p2, :cond_6

    .line 374
    new-instance v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;

    invoke-direct {v6, v9}, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;-><init>(Lcom/samsung/thumbnail/office/point/PPTCustomParser$1;)V

    .line 375
    .restart local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    sub-int v7, v4, p1

    iget v8, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->end:I

    sub-int/2addr v8, p1

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {p4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    .line 377
    iget-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v4, v7

    .line 378
    iput-boolean v10, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->isHyperLink:Z

    .line 379
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 380
    .end local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    :cond_6
    iget v7, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->start:I

    if-ge v7, v4, :cond_7

    iget v7, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->end:I

    if-ne v7, v4, :cond_7

    .line 381
    new-instance v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;

    invoke-direct {v6, v9}, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;-><init>(Lcom/samsung/thumbnail/office/point/PPTCustomParser$1;)V

    .line 382
    .restart local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    sub-int v7, v4, p1

    iget v8, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->end:I

    sub-int/2addr v8, p1

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {p4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    .line 384
    iget-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v4, v7

    .line 385
    iput-boolean v10, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->isHyperLink:Z

    .line 386
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 387
    .end local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    :cond_7
    iget v7, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->start:I

    if-ne v4, v7, :cond_8

    iget v7, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->end:I

    if-ge v7, p2, :cond_8

    .line 388
    new-instance v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;

    invoke-direct {v6, v9}, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;-><init>(Lcom/samsung/thumbnail/office/point/PPTCustomParser$1;)V

    .line 389
    .restart local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    sub-int v7, v4, p1

    iget v8, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->end:I

    sub-int/2addr v8, p1

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {p4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    .line 391
    iget-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v4, v7

    .line 392
    iput-boolean v10, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->isHyperLink:Z

    .line 393
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 394
    .end local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    :cond_8
    iget v7, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->start:I

    if-ne v4, v7, :cond_9

    iget v7, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->end:I

    if-ne p2, v7, :cond_9

    .line 395
    new-instance v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;

    invoke-direct {v6, v9}, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;-><init>(Lcom/samsung/thumbnail/office/point/PPTCustomParser$1;)V

    .line 396
    .restart local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    sub-int v7, v4, p1

    sub-int v8, p2, p1

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {p4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    .line 397
    iget-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v4, v7

    .line 398
    iput-boolean v10, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->isHyperLink:Z

    .line 399
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 400
    .end local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    :cond_9
    iget v7, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->start:I

    if-ge v7, v4, :cond_a

    iget v7, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->end:I

    if-le v7, p2, :cond_a

    .line 401
    new-instance v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;

    invoke-direct {v6, v9}, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;-><init>(Lcom/samsung/thumbnail/office/point/PPTCustomParser$1;)V

    .line 402
    .restart local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    sub-int v7, v4, p1

    sub-int v8, p2, p1

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {p4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    .line 403
    iget-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v4, v7

    .line 404
    iput-boolean v10, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->isHyperLink:Z

    .line 405
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 406
    .end local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    :cond_a
    iget v7, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->start:I

    if-le v7, v4, :cond_b

    iget v7, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->end:I

    if-ne p2, v7, :cond_b

    .line 407
    new-instance v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;

    invoke-direct {v6, v9}, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;-><init>(Lcom/samsung/thumbnail/office/point/PPTCustomParser$1;)V

    .line 408
    .restart local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    sub-int v7, v4, p1

    iget v8, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->start:I

    sub-int/2addr v8, p1

    invoke-virtual {p4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    .line 410
    iget-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v4, v7

    .line 411
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 412
    new-instance v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;

    .end local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    invoke-direct {v6, v9}, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;-><init>(Lcom/samsung/thumbnail/office/point/PPTCustomParser$1;)V

    .line 413
    .restart local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    sub-int v7, v4, p1

    sub-int v8, p2, p1

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {p4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    .line 414
    iget-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v4, v7

    .line 415
    iput-boolean v10, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->isHyperLink:Z

    .line 416
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 417
    .end local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    :cond_b
    iget v7, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->start:I

    if-le v7, v4, :cond_c

    iget v7, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->end:I

    if-ge p2, v7, :cond_c

    .line 418
    new-instance v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;

    invoke-direct {v6, v9}, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;-><init>(Lcom/samsung/thumbnail/office/point/PPTCustomParser$1;)V

    .line 419
    .restart local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    sub-int v7, v4, p1

    iget v8, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->start:I

    sub-int/2addr v8, p1

    invoke-virtual {p4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    .line 421
    iget-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v4, v7

    .line 422
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 423
    new-instance v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;

    .end local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    invoke-direct {v6, v9}, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;-><init>(Lcom/samsung/thumbnail/office/point/PPTCustomParser$1;)V

    .line 424
    .restart local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    sub-int v7, v4, p1

    sub-int v8, p2, p1

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {p4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    .line 425
    iget-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v4, v7

    .line 426
    iput-boolean v10, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->isHyperLink:Z

    .line 427
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 428
    .end local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    :cond_c
    iget v7, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->start:I

    if-ne v7, p2, :cond_d

    iget v7, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->end:I

    if-lt v7, p2, :cond_d

    .line 429
    new-instance v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;

    invoke-direct {v6, v9}, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;-><init>(Lcom/samsung/thumbnail/office/point/PPTCustomParser$1;)V

    .line 430
    .restart local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    sub-int v7, v4, p1

    iget v8, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->start:I

    sub-int/2addr v8, p1

    invoke-virtual {p4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    .line 432
    iget-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v4, v7

    .line 433
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 434
    new-instance v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;

    .end local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    invoke-direct {v6, v9}, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;-><init>(Lcom/samsung/thumbnail/office/point/PPTCustomParser$1;)V

    .line 435
    .restart local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    sub-int v7, v4, p1

    sub-int v8, p2, p1

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {p4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    .line 436
    iget-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v4, v7

    .line 437
    iput-boolean v10, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->isHyperLink:Z

    .line 438
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 439
    .end local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    :cond_d
    iget v7, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->start:I

    if-le v7, v4, :cond_e

    iget v7, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->end:I

    if-ge v7, p2, :cond_e

    .line 440
    new-instance v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;

    invoke-direct {v6, v9}, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;-><init>(Lcom/samsung/thumbnail/office/point/PPTCustomParser$1;)V

    .line 441
    .restart local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    sub-int v7, v4, p1

    iget v8, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->start:I

    sub-int/2addr v8, p1

    invoke-virtual {p4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    .line 443
    iget-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v4, v7

    .line 444
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 445
    new-instance v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;

    .end local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    invoke-direct {v6, v9}, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;-><init>(Lcom/samsung/thumbnail/office/point/PPTCustomParser$1;)V

    .line 446
    .restart local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    sub-int v7, v4, p1

    iget v8, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->end:I

    sub-int/2addr v8, p1

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {p4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    .line 448
    iget-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v4, v7

    .line 449
    iput-boolean v10, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->isHyperLink:Z

    .line 450
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 451
    .end local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    :cond_e
    iget v7, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->start:I

    if-ne v7, v4, :cond_3

    iget v7, v3, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;->end:I

    if-le v7, p2, :cond_3

    .line 452
    new-instance v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;

    invoke-direct {v6, v9}, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;-><init>(Lcom/samsung/thumbnail/office/point/PPTCustomParser$1;)V

    .line 453
    .restart local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    sub-int v7, v4, p1

    sub-int v8, p2, p1

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {p4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    .line 454
    iget-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v4, v7

    .line 455
    iput-boolean v10, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->isHyperLink:Z

    .line 456
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 459
    .end local v3    # "link":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubHyperlink;
    .end local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    :cond_f
    if-gt v4, p2, :cond_1

    .line 460
    new-instance v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;

    invoke-direct {v6, v9}, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;-><init>(Lcom/samsung/thumbnail/office/point/PPTCustomParser$1;)V

    .line 461
    .restart local v6    # "run":Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;
    sub-int v7, v4, p1

    sub-int v8, p2, p1

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {p4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    .line 462
    iget-object v7, v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser$SubRun;->str:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v4, v7

    .line 463
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public readMySlideShow(Ljava/io/FileInputStream;)V
    .locals 12
    .param p1, "fileName"    # Ljava/io/FileInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 159
    const/4 v0, 0x0

    .line 160
    .local v0, "fs":Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    new-instance v0, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    .end local v0    # "fs":Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    invoke-direct {v0, p1}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/InputStream;)V

    .line 161
    .restart local v0    # "fs":Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    new-instance v2, Lorg/apache/poi/hslf/HSLFSlideShow;

    invoke-direct {v2, v0}, Lorg/apache/poi/hslf/HSLFSlideShow;-><init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V

    .line 162
    .local v2, "hslfSlidShow":Lorg/apache/poi/hslf/HSLFSlideShow;
    new-instance v4, Lorg/apache/poi/hslf/usermodel/SlideShow;

    invoke-direct {v4, v2}, Lorg/apache/poi/hslf/usermodel/SlideShow;-><init>(Lorg/apache/poi/hslf/HSLFSlideShow;)V

    .line 163
    .local v4, "ppt":Lorg/apache/poi/hslf/usermodel/SlideShow;
    iget-object v7, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 164
    .local v3, "metrics":Landroid/util/DisplayMetrics;
    new-instance v5, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-direct {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;-><init>()V

    .line 165
    .local v5, "slideDimention":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    iget v7, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-double v8, v7

    invoke-virtual {v5, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setSlideWidth(D)V

    .line 166
    iget v7, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v8, v7

    invoke-virtual {v5, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setSlideHeight(D)V

    .line 167
    invoke-virtual {v4}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getPageSize()Lorg/apache/poi/java/awt/Dimension;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Dimension;->getHeight()D

    move-result-wide v8

    double-to-long v8, v8

    invoke-virtual {v5, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setPPTHeight(J)V

    .line 168
    invoke-virtual {v4}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getPageSize()Lorg/apache/poi/java/awt/Dimension;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Dimension;->getWidth()D

    move-result-wide v8

    double-to-long v8, v8

    invoke-virtual {v5, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setPPTWidth(J)V

    .line 169
    new-instance v7, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-direct {v7, v5}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;-><init>(Lcom/samsung/thumbnail/customview/hslf/SlideDimention;)V

    iput-object v7, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .line 170
    new-instance v7, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    iget-object v8, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-direct {v7, v8}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;-><init>(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    iput-object v7, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    .line 171
    const/4 v6, 0x0

    .line 172
    .local v6, "width":I
    const/4 v1, 0x0

    .line 173
    .local v1, "height":I
    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getSlideWidth()D

    move-result-wide v8

    double-to-int v7, v8

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getSlideHeight()D

    move-result-wide v8

    double-to-int v8, v8

    if-ge v7, v8, :cond_0

    .line 175
    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPPTWidth()J

    move-result-wide v8

    long-to-int v6, v8

    .line 176
    int-to-long v8, v6

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPPTHeight()J

    move-result-wide v10

    mul-long/2addr v8, v10

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPPTWidth()J

    move-result-wide v10

    div-long/2addr v8, v10

    long-to-int v1, v8

    .line 183
    :goto_0
    int-to-double v8, v6

    invoke-virtual {v5, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setCustomViewWidth(D)V

    .line 184
    int-to-double v8, v1

    invoke-virtual {v5, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setCustomViewHeight(D)V

    .line 185
    int-to-double v8, v6

    invoke-virtual {v5, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v8

    double-to-int v7, v8

    int-to-double v8, v1

    invoke-virtual {v5, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v8

    double-to-int v8, v8

    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->bitmap:Landroid/graphics/Bitmap;

    .line 189
    new-instance v7, Landroid/graphics/Canvas;

    iget-object v8, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->bitmap:Landroid/graphics/Bitmap;

    invoke-direct {v7, v8}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v7, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->canvas:Landroid/graphics/Canvas;

    .line 190
    iget-object v7, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->canvas:Landroid/graphics/Canvas;

    const/4 v8, -0x1

    invoke-virtual {v7, v8}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 191
    iget-object v7, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    iget-object v8, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->setCanvas(Landroid/graphics/Canvas;)V

    .line 192
    iget-object v7, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    iget-object v8, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->setCanvasBitmap(Landroid/graphics/Bitmap;)V

    .line 193
    new-instance v7, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-direct {v7, v5}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;-><init>(Lcom/samsung/thumbnail/customview/hslf/SlideDimention;)V

    iput-object v7, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .line 194
    new-instance v7, Lcom/samsung/thumbnail/customview/hslf/CustomController;

    iget-object v8, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-direct {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/CustomController;-><init>(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    iput-object v7, p0, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->customController:Lcom/samsung/thumbnail/customview/hslf/CustomController;

    .line 196
    invoke-direct {p0, v4}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->readPPT(Lorg/apache/poi/hslf/usermodel/SlideShow;)V

    .line 198
    return-void

    .line 179
    :cond_0
    const-wide v8, 0x3fe999999999999aL    # 0.8

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPPTHeight()J

    move-result-wide v10

    long-to-double v10, v10

    mul-double/2addr v8, v10

    double-to-int v1, v8

    .line 180
    int-to-long v8, v1

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPPTWidth()J

    move-result-wide v10

    mul-long/2addr v8, v10

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPPTHeight()J

    move-result-wide v10

    div-long/2addr v8, v10

    long-to-int v6, v8

    goto :goto_0
.end method

.method public setCustomView(Landroid/widget/LinearLayout;)V
    .locals 0
    .param p1, "parentLinearLayout"    # Landroid/widget/LinearLayout;

    .prologue
    .line 2827
    return-void
.end method
