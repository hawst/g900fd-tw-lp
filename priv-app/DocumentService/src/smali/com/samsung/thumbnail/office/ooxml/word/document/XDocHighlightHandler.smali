.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocHighlightHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLValueHandler;
.source "XDocHighlightHandler.java"


# instance fields
.field private consumer:Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V
    .locals 1
    .param p1, "consumer"    # Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;

    .prologue
    .line 15
    const-string/jumbo v0, "highlight"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLValueHandler;-><init>(Ljava/lang/String;)V

    .line 16
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocHighlightHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;

    .line 17
    return-void
.end method


# virtual methods
.method public getHexFromColor(Ljava/lang/String;)V
    .locals 0
    .param p1, "colorName"    # Ljava/lang/String;

    .prologue
    .line 30
    return-void
.end method

.method public handleValue(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 21
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocHighlightHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;

    if-nez v1, :cond_0

    .line 26
    :goto_0
    return-void

    .line 24
    :cond_0
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    invoke-direct {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;-><init>(Ljava/lang/String;)V

    .line 25
    .local v0, "highlight":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocHighlightHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;

    invoke-interface {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;->consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V

    goto :goto_0
.end method
