.class public Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/SlideSizeHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;
.source "SlideSizeHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    const-string/jumbo v0, "sldSz"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;-><init>(Ljava/lang/String;)V

    .line 15
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 8
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 20
    const-string/jumbo v6, "cx"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/SlideSizeHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    .line 21
    .local v2, "cxVal":Ljava/lang/String;
    const-string/jumbo v6, "cy"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/SlideSizeHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v3

    .line 24
    .local v3, "cyVal":Ljava/lang/String;
    if-nez v2, :cond_1

    .line 49
    :cond_0
    :goto_0
    return-void

    .line 27
    :cond_1
    if-eqz v3, :cond_0

    .line 34
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->toPoints(J)D

    move-result-wide v0

    .line 35
    .local v0, "cx":D
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->toPoints(J)D

    move-result-wide v4

    .line 37
    .local v4, "cy":D
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;

    invoke-virtual {v6, v0, v1, v4, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFPresentation;->addSlideSize(DD)V

    goto :goto_0
.end method
