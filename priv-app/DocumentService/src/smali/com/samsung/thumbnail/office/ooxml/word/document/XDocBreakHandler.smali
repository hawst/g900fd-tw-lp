.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBreakHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;
.source "XDocBreakHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    const-string/jumbo v0, "br"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;-><init>(Ljava/lang/String;)V

    .line 23
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 28
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 30
    const-string/jumbo v2, "type"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBreakHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 32
    .local v1, "val":Ljava/lang/String;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBreak;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBreak;-><init>()V

    .line 33
    .local v0, "br":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBreak;
    if-eqz v1, :cond_0

    .line 34
    invoke-static {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBreak$EBrType;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBreak$EBrType;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBreak;->setType(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBreak$EBrType;)V

    .line 36
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->addBreak(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBreak;)V

    .line 37
    return-void
.end method
