.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLChoiceHandler;
.source "TableHandler.java"


# instance fields
.field private graphicFrame:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;

.field private table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;)V
    .locals 2
    .param p1, "graphicFrame"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;

    .prologue
    .line 25
    const/16 v0, 0x1f

    const-string/jumbo v1, "tbl"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLChoiceHandler;-><init>(ILjava/lang/String;)V

    .line 26
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableHandler;->graphicFrame:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;

    .line 28
    return-void
.end method

.method private init()V
    .locals 6

    .prologue
    .line 31
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 33
    .local v1, "handlerMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;>;"
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableRowHandler;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableHandler;->table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    invoke-direct {v2, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableRowHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;)V

    .line 34
    .local v2, "rowHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableRowHandler;
    const-string/jumbo v4, "tr"

    invoke-virtual {v1, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableGridHandler;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableHandler;->table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableGridHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;)V

    .line 37
    .local v0, "gridHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableGridHandler;
    const-string/jumbo v4, "tblGrid"

    invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TablePrhandler;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableHandler;->table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    invoke-direct {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TablePrhandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;)V

    .line 40
    .local v3, "tblPrhandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TablePrhandler;
    const-string/jumbo v4, "tblPr"

    invoke-virtual {v1, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableHandler;->choiceHandlerMap:Ljava/util/HashMap;

    const/16 v5, 0x1f

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 50
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLChoiceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 52
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    .line 53
    .local v0, "slide":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    invoke-direct {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;)V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableHandler;->table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    .line 54
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableHandler;->init()V

    .line 55
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableHandler;->table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableHandler;->graphicFrame:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->setShapeProperties(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 56
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableHandler;->table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->addShapes(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;)V

    .line 58
    return-void
.end method
