.class public Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;
.super Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;
.source "XSLFTableCell.java"


# static fields
.field static defaultBorderWidth:D


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    sput-wide v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;->defaultBorderWidth:D

    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;)V
    .locals 0
    .param p1, "shapeProperties"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "sheet"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;)V

    .line 19
    return-void
.end method
