.class public Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
.super Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;
.source "ReadWmfRawFile.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "WrongCall"
    }
.end annotation


# static fields
.field private static final INTEGER_0:Ljava/lang/Integer;


# instance fields
.field private _bbottom:I

.field private _bleft:I

.field private _bright:I

.field private _btop:I

.field private _ibottom:I

.field private _ileft:I

.field private _iright:I

.field private _itop:I

.field private _objIndex:I

.field private actImgHigt:F

.field private actImgWid:F

.field protected bottom:I

.field private drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

.field private emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

.field fileContent:[B

.field protected height:I

.field protected inch:I

.field protected left:I

.field mBitmap:Landroid/graphics/Bitmap;

.field private mFlipTheImage:Z

.field protected mtHeaderSize:I

.field protected mtMaxRecord:I

.field protected mtNoObjects:I

.field protected mtNoParameters:I

.field protected mtSize:I

.field protected mtType:I

.field protected mtVersion:I

.field private polyPaint:Landroid/graphics/Paint;

.field protected right:I

.field protected scaleXY:F

.field protected scaleY:F

.field protected top:I

.field private wf:Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFFont;

.field protected width:I

.field protected xSign:I

.field protected ySign:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->INTEGER_0:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>([B)V
    .locals 2
    .param p1, "stream"    # [B

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 83
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;-><init>()V

    .line 31
    iput-object v1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->fileContent:[B

    .line 39
    iput v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->xSign:I

    .line 40
    iput v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->ySign:I

    .line 73
    iput-object v1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->wf:Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFFont;

    .line 84
    iput-object p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->fileContent:[B

    .line 85
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    .line 86
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .line 87
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->initialise()V

    .line 88
    return-void
.end method

.method public constructor <init>([BFF)V
    .locals 2
    .param p1, "stream"    # [B
    .param p2, "actImgWid"    # F
    .param p3, "actImgHigt"    # F

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 90
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;-><init>()V

    .line 31
    iput-object v1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->fileContent:[B

    .line 39
    iput v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->xSign:I

    .line 40
    iput v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->ySign:I

    .line 73
    iput-object v1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->wf:Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFFont;

    .line 91
    iput-object p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->fileContent:[B

    .line 92
    iput p2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->actImgWid:F

    .line 93
    iput p3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->actImgHigt:F

    .line 95
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    .line 96
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .line 98
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->initialise()V

    .line 99
    return-void
.end method

.method public constructor <init>([BIIII)V
    .locals 2
    .param p1, "strm"    # [B
    .param p2, "left"    # I
    .param p3, "right"    # I
    .param p4, "top"    # I
    .param p5, "bottom"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 115
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;-><init>()V

    .line 31
    iput-object v1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->fileContent:[B

    .line 39
    iput v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->xSign:I

    .line 40
    iput v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->ySign:I

    .line 73
    iput-object v1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->wf:Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFFont;

    .line 116
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    .line 117
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .line 118
    iput-object p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->fileContent:[B

    .line 119
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->initialise()V

    .line 120
    iput p2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->left:I

    .line 121
    iput p3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->right:I

    .line 122
    iput p4, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->top:I

    .line 123
    iput p5, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->bottom:I

    .line 125
    return-void
.end method

.method public constructor <init>([BLcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;)V
    .locals 4
    .param p1, "stream"    # [B
    .param p2, "pictInfo"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 101
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;-><init>()V

    .line 31
    iput-object v2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->fileContent:[B

    .line 39
    iput v1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->xSign:I

    .line 40
    iput v1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->ySign:I

    .line 73
    iput-object v2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->wf:Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFFont;

    .line 103
    iput-object p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->fileContent:[B

    .line 104
    new-instance v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    .line 105
    new-instance v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .line 107
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->initialise()V

    .line 108
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->read([B)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    :goto_0
    return-void

    .line 109
    :catch_0
    move-exception v0

    .line 110
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v1, "DocumentService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getColorFromObject(I)Landroid/graphics/Color;
    .locals 2
    .param p1, "brushObject"    # I

    .prologue
    .line 1494
    if-ltz p1, :cond_0

    .line 1495
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getObject(I)Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;

    move-result-object v0

    .line 1496
    .local v0, "gdiObj":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;
    iget-object v1, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Color;

    .line 1498
    .end local v0    # "gdiObj":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getResizedHeight(F)F
    .locals 2
    .param p1, "heightParam"    # F

    .prologue
    .line 1540
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->actImgHigt:F

    mul-float/2addr v0, p1

    iget v1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->height:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method private getResizedWidth(F)F
    .locals 2
    .param p1, "widthParam"    # F

    .prologue
    .line 1536
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->actImgWid:F

    mul-float/2addr v0, p1

    iget v1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->width:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method private initialise()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, -0x1

    .line 129
    iput v1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleY:F

    .line 130
    iput v1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    .line 131
    iput v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->left:I

    .line 132
    iput v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->top:I

    .line 133
    iput v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->width:I

    .line 134
    iput v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->height:I

    .line 135
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->left:I

    iget v1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->width:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->right:I

    .line 136
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->top:I

    iget v1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->height:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->bottom:I

    .line 137
    iput v2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->numObjects:I

    .line 138
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->objectVector:Ljava/util/List;

    .line 139
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->polyPaint:Landroid/graphics/Paint;

    .line 140
    iput-boolean v2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mFlipTheImage:Z

    .line 141
    return-void
.end method

.method private resizeBounds(II)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v1, -0x1

    .line 1454
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_bleft:I

    if-ne v0, v1, :cond_4

    .line 1455
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_bleft:I

    .line 1458
    :cond_0
    :goto_0
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_bright:I

    if-ne v0, v1, :cond_5

    .line 1459
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_bright:I

    .line 1463
    :cond_1
    :goto_1
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_btop:I

    if-ne v0, v1, :cond_6

    .line 1464
    iput p2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_btop:I

    .line 1467
    :cond_2
    :goto_2
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_bbottom:I

    if-ne v0, v1, :cond_7

    .line 1468
    iput p2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_bbottom:I

    .line 1471
    :cond_3
    :goto_3
    return-void

    .line 1456
    :cond_4
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_bleft:I

    if-ge p1, v0, :cond_0

    .line 1457
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_bleft:I

    goto :goto_0

    .line 1460
    :cond_5
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_bright:I

    if-le p1, v0, :cond_1

    .line 1461
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_bright:I

    goto :goto_1

    .line 1465
    :cond_6
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_btop:I

    if-ge p2, v0, :cond_2

    .line 1466
    iput p2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_btop:I

    goto :goto_2

    .line 1469
    :cond_7
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_bbottom:I

    if-le p2, v0, :cond_3

    .line 1470
    iput p2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_bbottom:I

    goto :goto_3
.end method

.method private resizeImageBounds(II)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v1, -0x1

    .line 1474
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_ileft:I

    if-ne v0, v1, :cond_4

    .line 1475
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_ileft:I

    .line 1478
    :cond_0
    :goto_0
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_iright:I

    if-ne v0, v1, :cond_5

    .line 1479
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_iright:I

    .line 1483
    :cond_1
    :goto_1
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_itop:I

    if-ne v0, v1, :cond_6

    .line 1484
    iput p2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_itop:I

    .line 1487
    :cond_2
    :goto_2
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_ibottom:I

    if-ne v0, v1, :cond_7

    .line 1488
    iput p2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_ibottom:I

    .line 1491
    :cond_3
    :goto_3
    return-void

    .line 1476
    :cond_4
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_ileft:I

    if-ge p1, v0, :cond_0

    .line 1477
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_ileft:I

    goto :goto_0

    .line 1480
    :cond_5
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_iright:I

    if-le p1, v0, :cond_1

    .line 1481
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_iright:I

    goto :goto_1

    .line 1485
    :cond_6
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_itop:I

    if-ge p2, v0, :cond_2

    .line 1486
    iput p2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_itop:I

    goto :goto_2

    .line 1489
    :cond_7
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_ibottom:I

    if-le p2, v0, :cond_3

    .line 1490
    iput p2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_ibottom:I

    goto :goto_3
.end method


# virtual methods
.method public drawImage()Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    .line 152
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    .line 153
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->onDraw(Landroid/graphics/Canvas;)V

    .line 155
    :try_start_0
    iget-object v2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->fileContent:[B

    invoke-virtual {p0, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->read([B)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    :goto_0
    iget-object v2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mBitmap:Landroid/graphics/Bitmap;

    return-object v2

    .line 156
    :catch_0
    move-exception v1

    .line 157
    .local v1, "e":Ljava/io/IOException;
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public drawImageDoc()Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    .line 848
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    .line 849
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->onDraw(Landroid/graphics/Canvas;)V

    .line 851
    :try_start_0
    iget-object v2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->fileContent:[B

    invoke-virtual {p0, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readWmfDoc([B)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 855
    :goto_0
    iget-object v2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mBitmap:Landroid/graphics/Bitmap;

    return-object v2

    .line 852
    :catch_0
    move-exception v1

    .line 853
    .local v1, "e":Ljava/io/IOException;
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public flipTheImage()Z
    .locals 1

    .prologue
    .line 1406
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mFlipTheImage:Z

    return v0
.end method

.method getImage([B)Landroid/graphics/Bitmap;
    .locals 22
    .param p1, "bit"    # [B

    .prologue
    .line 714
    const/4 v3, 0x7

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x18

    const/4 v5, 0x6

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v3, v5

    const/4 v5, 0x5

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v3, v5

    const/4 v5, 0x4

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    or-int v4, v3, v5

    .line 717
    .local v4, "_width":I
    const/16 v3, 0xb

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x18

    const/16 v5, 0xa

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v3, v5

    const/16 v5, 0x9

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v3, v5

    const/16 v5, 0x8

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    or-int v8, v3, v5

    .line 722
    .local v8, "_height":I
    mul-int v3, v4, v8

    new-array v2, v3, [I

    .line 723
    .local v2, "bitI":[I
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v8, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 728
    .local v1, "img":Landroid/graphics/Bitmap;
    const/4 v3, 0x3

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x18

    const/4 v5, 0x2

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v3, v5

    const/4 v5, 0x1

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v3, v5

    const/4 v5, 0x0

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    or-int v10, v3, v5

    .line 732
    .local v10, "_headerSize":I
    const/16 v3, 0xd

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    const/16 v5, 0xc

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    or-int v12, v3, v5

    .line 734
    .local v12, "_planes":I
    const/16 v3, 0xf

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    const/16 v5, 0xe

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    or-int v11, v3, v5

    .line 737
    .local v11, "_nbit":I
    const/16 v3, 0x17

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x18

    const/16 v5, 0x16

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v3, v5

    const/16 v5, 0x15

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v3, v5

    const/16 v5, 0x14

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    or-int v13, v3, v5

    .line 741
    .local v13, "_size":I
    if-nez v13, :cond_0

    .line 742
    mul-int v3, v4, v11

    add-int/lit8 v3, v3, 0x1f

    and-int/lit8 v3, v3, -0x20

    shr-int/lit8 v3, v3, 0x3

    mul-int v13, v3, v8

    .line 745
    :cond_0
    const/16 v3, 0x23

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x18

    const/16 v5, 0x22

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v3, v5

    const/16 v5, 0x21

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v3, v5

    const/16 v5, 0x20

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    or-int v9, v3, v5

    .line 750
    .local v9, "_clrused":I
    const/16 v3, 0x18

    if-ne v11, v3, :cond_2

    .line 752
    div-int v3, v13, v8

    mul-int/lit8 v5, v4, 0x3

    sub-int v19, v3, v5

    .line 753
    .local v19, "pad":I
    move/from16 v18, v10

    .line 755
    .local v18, "offset":I
    const/16 v16, 0x0

    .local v16, "j":I
    :goto_0
    move/from16 v0, v16

    if-ge v0, v8, :cond_c

    .line 756
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_1
    if-ge v15, v4, :cond_1

    .line 757
    sub-int v3, v8, v16

    add-int/lit8 v3, v3, -0x1

    mul-int/2addr v3, v4

    add-int/2addr v3, v15

    const/high16 v5, -0x1000000

    add-int/lit8 v6, v18, 0x2

    aget-byte v6, p1, v6

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x10

    or-int/2addr v5, v6

    add-int/lit8 v6, v18, 0x1

    aget-byte v6, p1, v6

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x8

    or-int/2addr v5, v6

    aget-byte v6, p1, v18

    and-int/lit16 v6, v6, 0xff

    or-int/2addr v5, v6

    aput v5, v2, v3

    .line 761
    add-int/lit8 v18, v18, 0x3

    .line 756
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 763
    :cond_1
    add-int v18, v18, v19

    .line 755
    add-int/lit8 v16, v16, 0x1

    goto :goto_0

    .line 766
    .end local v15    # "i":I
    .end local v16    # "j":I
    .end local v18    # "offset":I
    .end local v19    # "pad":I
    :cond_2
    const/16 v3, 0x8

    if-ne v11, v3, :cond_6

    .line 768
    const/16 v17, 0x0

    .line 769
    .local v17, "nbColors":I
    if-lez v9, :cond_3

    .line 770
    move/from16 v17, v9

    .line 774
    :goto_2
    move/from16 v18, v10

    .line 775
    .restart local v18    # "offset":I
    move/from16 v0, v17

    new-array v0, v0, [I

    move-object/from16 v20, v0

    .line 776
    .local v20, "palette":[I
    const/4 v15, 0x0

    .restart local v15    # "i":I
    :goto_3
    move/from16 v0, v17

    if-ge v15, v0, :cond_4

    .line 777
    const/high16 v3, -0x1000000

    add-int/lit8 v5, v18, 0x2

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v3, v5

    add-int/lit8 v5, v18, 0x1

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v3, v5

    aget-byte v5, p1, v18

    and-int/lit16 v5, v5, 0xff

    or-int/2addr v3, v5

    aput v3, v20, v15

    .line 781
    add-int/lit8 v18, v18, 0x4

    .line 776
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 772
    .end local v15    # "i":I
    .end local v18    # "offset":I
    .end local v20    # "palette":[I
    :cond_3
    const/16 v17, 0x100

    goto :goto_2

    .line 790
    .restart local v15    # "i":I
    .restart local v18    # "offset":I
    .restart local v20    # "palette":[I
    :cond_4
    move-object/from16 v0, p1

    array-length v3, v0

    sub-int v13, v3, v18

    .line 791
    div-int v3, v13, v8

    sub-int v19, v3, v4

    .line 792
    .restart local v19    # "pad":I
    const/16 v16, 0x0

    .restart local v16    # "j":I
    :goto_4
    move/from16 v0, v16

    if-ge v0, v8, :cond_c

    .line 793
    const/4 v15, 0x0

    :goto_5
    if-ge v15, v4, :cond_5

    .line 794
    sub-int v3, v8, v16

    add-int/lit8 v3, v3, -0x1

    mul-int/2addr v3, v4

    add-int/2addr v3, v15

    aget-byte v5, p1, v18

    and-int/lit16 v5, v5, 0xff

    aget v5, v20, v5

    aput v5, v2, v3

    .line 795
    add-int/lit8 v18, v18, 0x1

    .line 793
    add-int/lit8 v15, v15, 0x1

    goto :goto_5

    .line 797
    :cond_5
    add-int v18, v18, v19

    .line 792
    add-int/lit8 v16, v16, 0x1

    goto :goto_4

    .line 800
    .end local v15    # "i":I
    .end local v16    # "j":I
    .end local v17    # "nbColors":I
    .end local v18    # "offset":I
    .end local v19    # "pad":I
    .end local v20    # "palette":[I
    :cond_6
    const/4 v3, 0x1

    if-ne v11, v3, :cond_c

    .line 802
    const/16 v17, 0x2

    .line 804
    .restart local v17    # "nbColors":I
    move/from16 v18, v10

    .line 805
    .restart local v18    # "offset":I
    move/from16 v0, v17

    new-array v0, v0, [I

    move-object/from16 v20, v0

    .line 806
    .restart local v20    # "palette":[I
    const/4 v15, 0x0

    .restart local v15    # "i":I
    :goto_6
    move/from16 v0, v17

    if-ge v15, v0, :cond_7

    .line 807
    const/high16 v3, -0x1000000

    add-int/lit8 v5, v18, 0x2

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v3, v5

    add-int/lit8 v5, v18, 0x1

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v3, v5

    aget-byte v5, p1, v18

    and-int/lit16 v5, v5, 0xff

    or-int/2addr v3, v5

    aput v3, v20, v15

    .line 811
    add-int/lit8 v18, v18, 0x4

    .line 806
    add-int/lit8 v15, v15, 0x1

    goto :goto_6

    .line 816
    :cond_7
    const/16 v21, 0x7

    .line 817
    .local v21, "pos":I
    aget-byte v14, p1, v18

    .line 819
    .local v14, "currentByte":B
    div-int v3, v13, v8

    div-int/lit8 v5, v4, 0x8

    sub-int v19, v3, v5

    .line 820
    .restart local v19    # "pad":I
    const/16 v16, 0x0

    .restart local v16    # "j":I
    :goto_7
    move/from16 v0, v16

    if-ge v0, v8, :cond_c

    .line 821
    const/4 v15, 0x0

    :goto_8
    if-ge v15, v4, :cond_a

    .line 822
    const/4 v3, 0x1

    shl-int v3, v3, v21

    and-int/2addr v3, v14

    if-eqz v3, :cond_9

    .line 823
    sub-int v3, v8, v16

    add-int/lit8 v3, v3, -0x1

    mul-int/2addr v3, v4

    add-int/2addr v3, v15

    const/4 v5, 0x1

    aget v5, v20, v5

    aput v5, v2, v3

    .line 826
    :goto_9
    add-int/lit8 v21, v21, -0x1

    .line 827
    const/4 v3, -0x1

    move/from16 v0, v21

    if-ne v0, v3, :cond_8

    .line 828
    const/16 v21, 0x7

    .line 829
    add-int/lit8 v18, v18, 0x1

    .line 830
    move-object/from16 v0, p1

    array-length v3, v0

    move/from16 v0, v18

    if-ge v0, v3, :cond_8

    .line 831
    aget-byte v14, p1, v18

    .line 821
    :cond_8
    add-int/lit8 v15, v15, 0x1

    goto :goto_8

    .line 825
    :cond_9
    sub-int v3, v8, v16

    add-int/lit8 v3, v3, -0x1

    mul-int/2addr v3, v4

    add-int/2addr v3, v15

    const/4 v5, 0x0

    aget v5, v20, v5

    aput v5, v2, v3

    goto :goto_9

    .line 834
    :cond_a
    add-int v18, v18, v19

    .line 835
    const/16 v21, 0x7

    .line 836
    move-object/from16 v0, p1

    array-length v3, v0

    move/from16 v0, v18

    if-ge v0, v3, :cond_b

    .line 837
    aget-byte v14, p1, v18

    .line 820
    :cond_b
    add-int/lit8 v16, v16, 0x1

    goto :goto_7

    .line 841
    .end local v14    # "currentByte":B
    .end local v15    # "i":I
    .end local v16    # "j":I
    .end local v17    # "nbColors":I
    .end local v18    # "offset":I
    .end local v19    # "pad":I
    .end local v20    # "palette":[I
    .end local v21    # "pos":I
    :cond_c
    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v7, v4

    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 843
    return-object v1
.end method

.method public read([B)Landroid/graphics/Bitmap;
    .locals 92
    .param p1, "is"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 164
    const/16 v85, 0x0

    move-object/from16 v0, v85

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mBitmap:Landroid/graphics/Bitmap;

    .line 166
    const/16 v85, 0x0

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mOffset:I

    .line 167
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v85, v0

    new-instance v86, Ljava/util/ArrayList;

    invoke-direct/range {v86 .. v86}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v86

    move-object/from16 v1, v85

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    .line 169
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v85, v0

    move-object/from16 v0, v85

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v85, v0

    if-nez v85, :cond_0

    .line 170
    const/16 v85, 0x0

    .line 709
    :goto_0
    return-object v85

    .line 172
    :cond_0
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readInt([B)I

    move-result v23

    .line 173
    .local v23, "dwIsAldus":I
    const v85, -0x65393229

    move/from16 v0, v23

    move/from16 v1, v85

    if-ne v0, v1, :cond_5

    .line 177
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 178
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->left:I

    .line 179
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->top:I

    .line 180
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->right:I

    .line 182
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->left:I

    move/from16 v85, v0

    if-gez v85, :cond_1

    .line 183
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->right:I

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->left:I

    move/from16 v86, v0

    mul-int/lit8 v86, v86, -0x1

    add-int v85, v85, v86

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->right:I

    .line 184
    const/16 v85, 0x1

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mFlipTheImage:Z

    .line 187
    :cond_1
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->bottom:I

    .line 188
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->top:I

    move/from16 v85, v0

    if-gez v85, :cond_2

    .line 189
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->bottom:I

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->top:I

    move/from16 v86, v0

    mul-int/lit8 v86, v86, -0x1

    add-int v85, v85, v86

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->bottom:I

    .line 190
    const/16 v85, 0x1

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mFlipTheImage:Z

    .line 192
    :cond_2
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->inch:I

    .line 193
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readInt([B)I

    move-result v63

    .line 194
    .local v63, "reserved":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v16

    .line 196
    .local v16, "checksum":S
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->left:I

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->right:I

    move/from16 v86, v0

    move/from16 v0, v85

    move/from16 v1, v86

    if-le v0, v1, :cond_3

    .line 197
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->right:I

    .line 198
    .local v5, "_i":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->left:I

    move/from16 v85, v0

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->right:I

    .line 199
    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->left:I

    .line 200
    const/16 v85, -0x1

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->xSign:I

    .line 202
    .end local v5    # "_i":I
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->top:I

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->bottom:I

    move/from16 v86, v0

    move/from16 v0, v85

    move/from16 v1, v86

    if-le v0, v1, :cond_4

    .line 203
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->bottom:I

    .line 204
    .restart local v5    # "_i":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->top:I

    move/from16 v85, v0

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->bottom:I

    .line 205
    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->top:I

    .line 206
    const/16 v85, -0x1

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->ySign:I

    .line 209
    .end local v5    # "_i":I
    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->right:I

    move/from16 v85, v0

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->width:I

    .line 210
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->bottom:I

    move/from16 v85, v0

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->height:I

    .line 212
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->top:I

    move/from16 v85, v0

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v85, v0

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->top:I

    .line 213
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->bottom:I

    move/from16 v85, v0

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v85, v0

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->bottom:I

    .line 214
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->left:I

    move/from16 v85, v0

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v85, v0

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->left:I

    .line 215
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->right:I

    move/from16 v85, v0

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v85, v0

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->right:I

    .line 217
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mtType:I

    .line 218
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mtHeaderSize:I

    .line 220
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->width:I

    move/from16 v85, v0

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->height:I

    move/from16 v86, v0

    move/from16 v0, v86

    int-to-float v0, v0

    move/from16 v86, v0

    move-object/from16 v0, p0

    move/from16 v1, v86

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v86

    move/from16 v0, v86

    float-to-int v0, v0

    move/from16 v86, v0

    sget-object v87, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static/range {v85 .. v87}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v85

    move-object/from16 v0, v85

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mBitmap:Landroid/graphics/Bitmap;

    .line 223
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v85, v0

    move-object/from16 v0, v85

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v85, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v86, v0

    invoke-virtual/range {v85 .. v86}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 224
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v85, v0

    move-object/from16 v0, v85

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v85, v0

    const/16 v86, -0x1

    invoke-virtual/range {v85 .. v86}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 230
    .end local v16    # "checksum":S
    .end local v63    # "reserved":I
    :goto_1
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mtVersion:I

    .line 231
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readInt([B)I

    move-result v85

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mtSize:I

    .line 232
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mtNoObjects:I

    .line 233
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readInt([B)I

    move-result v85

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mtMaxRecord:I

    .line 234
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mtNoParameters:I

    .line 236
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mtNoObjects:I

    move/from16 v85, v0

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->numObjects:I

    .line 237
    new-instance v71, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->numObjects:I

    move/from16 v85, v0

    move-object/from16 v0, v71

    move/from16 v1, v85

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 238
    .local v71, "tempList":Ljava/util/List;
    const/16 v41, 0x0

    .local v41, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->numObjects:I

    move/from16 v85, v0

    move/from16 v0, v41

    move/from16 v1, v85

    if-ge v0, v1, :cond_6

    .line 239
    new-instance v85, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;

    const/16 v86, 0x0

    move-object/from16 v0, v85

    move/from16 v1, v41

    move/from16 v2, v86

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;-><init>(IZ)V

    move-object/from16 v0, v71

    move-object/from16 v1, v85

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 238
    add-int/lit8 v41, v41, 0x1

    goto :goto_2

    .line 226
    .end local v41    # "i":I
    .end local v71    # "tempList":Ljava/util/List;
    :cond_5
    shl-int/lit8 v85, v23, 0x10

    shr-int/lit8 v85, v85, 0x10

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mtType:I

    .line 227
    shr-int/lit8 v85, v23, 0x10

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mtHeaderSize:I

    goto/16 :goto_1

    .line 241
    .restart local v41    # "i":I
    .restart local v71    # "tempList":Ljava/util/List;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->objectVector:Ljava/util/List;

    move-object/from16 v85, v0

    move-object/from16 v0, v85

    move-object/from16 v1, v71

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 243
    const/16 v32, 0x1

    .line 244
    .local v32, "functionId":S
    const/16 v61, 0x0

    .line 246
    .local v61, "recSize":I
    const/4 v13, -0x1

    .line 247
    .local v13, "brushObject":I
    const/16 v54, -0x1

    .line 248
    .local v54, "penObject":I
    const/16 v31, -0x1

    .line 250
    .local v31, "fontObject":I
    :cond_7
    :goto_3
    :sswitch_0
    if-lez v32, :cond_8

    .line 252
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readInt([B)I

    move-result v61

    .line 254
    add-int/lit8 v61, v61, -0x3

    .line 256
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v32

    .line 257
    if-gtz v32, :cond_9

    .line 708
    :cond_8
    const/16 v85, 0x0

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mOffset:I

    .line 709
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v85, v0

    goto/16 :goto_0

    .line 262
    :cond_9
    sparse-switch v32, :sswitch_data_0

    .line 702
    const/16 v42, 0x0

    .local v42, "j":I
    :goto_4
    move/from16 v0, v42

    move/from16 v1, v61

    if-ge v0, v1, :cond_7

    .line 703
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 702
    add-int/lit8 v42, v42, 0x1

    goto :goto_4

    .line 265
    .end local v42    # "j":I
    :sswitch_1
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v50

    .line 267
    .local v50, "mapmode":I
    const/16 v85, 0x8

    move/from16 v0, v50

    move/from16 v1, v85

    if-ne v0, v1, :cond_7

    .line 268
    const/16 v85, 0x0

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->isotropic:Z

    goto :goto_3

    .line 271
    .end local v50    # "mapmode":I
    :sswitch_2
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->vpY:I

    .line 272
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->vpX:I

    goto :goto_3

    .line 276
    :sswitch_3
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->vpH:I

    .line 277
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->vpW:I

    .line 278
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->isotropic:Z

    move/from16 v85, v0

    if-nez v85, :cond_a

    .line 279
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->vpW:I

    move/from16 v85, v0

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->vpH:I

    move/from16 v86, v0

    move/from16 v0, v86

    int-to-float v0, v0

    move/from16 v86, v0

    div-float v85, v85, v86

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    .line 280
    :cond_a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->vpW:I

    move/from16 v85, v0

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v86, v0

    mul-float v85, v85, v86

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v85, v0

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->vpW:I

    .line 284
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v85, v0

    if-nez v85, :cond_7

    .line 285
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->vpY:I

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->vpH:I

    move/from16 v86, v0

    add-int v85, v85, v86

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->height:I

    .line 286
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->vpX:I

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->vpW:I

    move/from16 v86, v0

    add-int v85, v85, v86

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->width:I

    .line 287
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->width:I

    move/from16 v85, v0

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->height:I

    move/from16 v86, v0

    move/from16 v0, v86

    int-to-float v0, v0

    move/from16 v86, v0

    move-object/from16 v0, p0

    move/from16 v1, v86

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v86

    move/from16 v0, v86

    float-to-int v0, v0

    move/from16 v86, v0

    sget-object v87, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static/range {v85 .. v87}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v85

    move-object/from16 v0, v85

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mBitmap:Landroid/graphics/Bitmap;

    .line 291
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v85, v0

    move-object/from16 v0, v85

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v85, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v86, v0

    invoke-virtual/range {v85 .. v86}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 292
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v85, v0

    move-object/from16 v0, v85

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v85, v0

    const/16 v86, -0x1

    invoke-virtual/range {v85 .. v86}, Landroid/graphics/Canvas;->drawColor(I)V

    goto/16 :goto_3

    .line 299
    :sswitch_4
    const/16 v85, 0x0

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    .line 300
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v55

    .line 302
    .local v55, "penStyle":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readInt([B)I

    .line 304
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readInt([B)I

    move-result v20

    .line 305
    .local v20, "colorref":I
    move/from16 v0, v20

    and-int/lit16 v0, v0, 0xff

    move/from16 v62, v0

    .line 306
    .local v62, "red":I
    const v85, 0xff00

    and-int v85, v85, v20

    shr-int/lit8 v35, v85, 0x8

    .line 307
    .local v35, "green":I
    const/high16 v85, 0xff0000

    and-int v85, v85, v20

    shr-int/lit8 v10, v85, 0x10

    .line 308
    .local v10, "blue":I
    new-instance v18, Lorg/apache/poi/java/awt/Color;

    move-object/from16 v0, v18

    move/from16 v1, v62

    move/from16 v2, v35

    invoke-direct {v0, v1, v2, v10}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    .line 311
    .local v18, "color":Lorg/apache/poi/java/awt/Color;
    const/16 v85, 0x6

    move/from16 v0, v61

    move/from16 v1, v85

    if-ne v0, v1, :cond_b

    .line 312
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 313
    :cond_b
    const/16 v85, 0x5

    move/from16 v0, v55

    move/from16 v1, v85

    if-ne v0, v1, :cond_c

    .line 314
    const/16 v85, 0x4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    move/from16 v86, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    move-object/from16 v2, v18

    move/from16 v3, v86

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->addObjectAt(ILjava/lang/Object;I)I

    move-result v85

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    .line 318
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->polyPaint:Landroid/graphics/Paint;

    move-object/from16 v85, v0

    const/16 v86, 0xff

    move-object/from16 v0, v85

    move/from16 v1, v86

    move/from16 v2, v62

    move/from16 v3, v35

    invoke-virtual {v0, v1, v2, v3, v10}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_3

    .line 316
    :cond_c
    const/16 v85, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    move/from16 v86, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    move-object/from16 v2, v18

    move/from16 v3, v86

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->addObjectAt(ILjava/lang/Object;I)I

    move-result v85

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    goto :goto_5

    .line 322
    .end local v10    # "blue":I
    .end local v18    # "color":Lorg/apache/poi/java/awt/Color;
    .end local v20    # "colorref":I
    .end local v35    # "green":I
    .end local v55    # "penStyle":I
    .end local v62    # "red":I
    :sswitch_5
    const/16 v85, 0x0

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    .line 323
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v14

    .line 325
    .local v14, "brushStyle":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readInt([B)I

    move-result v20

    .line 326
    .restart local v20    # "colorref":I
    move/from16 v0, v20

    and-int/lit16 v0, v0, 0xff

    move/from16 v62, v0

    .line 327
    .restart local v62    # "red":I
    const v85, 0xff00

    and-int v85, v85, v20

    shr-int/lit8 v35, v85, 0x8

    .line 328
    .restart local v35    # "green":I
    const/high16 v85, 0xff0000

    and-int v85, v85, v20

    shr-int/lit8 v10, v85, 0x10

    .line 329
    .restart local v10    # "blue":I
    new-instance v18, Lorg/apache/poi/java/awt/Color;

    move-object/from16 v0, v18

    move/from16 v1, v62

    move/from16 v2, v35

    invoke-direct {v0, v1, v2, v10}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    .line 331
    .restart local v18    # "color":Lorg/apache/poi/java/awt/Color;
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 332
    const/16 v85, 0x5

    move/from16 v0, v85

    if-ne v14, v0, :cond_d

    .line 333
    const/16 v85, 0x5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    move/from16 v86, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    move-object/from16 v2, v18

    move/from16 v3, v86

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->addObjectAt(ILjava/lang/Object;I)I

    move-result v85

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    .line 337
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->polyPaint:Landroid/graphics/Paint;

    move-object/from16 v85, v0

    const/16 v86, 0xff

    move-object/from16 v0, v85

    move/from16 v1, v86

    move/from16 v2, v62

    move/from16 v3, v35

    invoke-virtual {v0, v1, v2, v3, v10}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_3

    .line 335
    :cond_d
    const/16 v85, 0x2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    move/from16 v86, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    move-object/from16 v2, v18

    move/from16 v3, v86

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->addObjectAt(ILjava/lang/Object;I)I

    move-result v85

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    goto :goto_6

    .line 340
    .end local v10    # "blue":I
    .end local v14    # "brushStyle":I
    .end local v18    # "color":Lorg/apache/poi/java/awt/Color;
    .end local v20    # "colorref":I
    .end local v35    # "green":I
    .end local v62    # "red":I
    :sswitch_6
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v8

    .line 342
    .local v8, "align":I
    const/16 v85, 0x1

    move/from16 v0, v61

    move/from16 v1, v85

    if-le v0, v1, :cond_7

    .line 343
    const/16 v41, 0x1

    :goto_7
    move/from16 v0, v41

    move/from16 v1, v61

    if-ge v0, v1, :cond_7

    .line 344
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 343
    add-int/lit8 v41, v41, 0x1

    goto :goto_7

    .line 351
    .end local v8    # "align":I
    :sswitch_7
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v82, v0

    .line 352
    .local v82, "y":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v86, v0

    mul-float v85, v85, v86

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v85, v0

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v79, v0

    .line 353
    .local v79, "x":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v45

    .line 354
    .local v45, "lenText":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v30

    .line 355
    .local v30, "flag":I
    const/16 v60, 0x4

    .line 356
    .local v60, "read":I
    const/16 v17, 0x0

    .line 357
    .local v17, "clipped":Z
    const/16 v80, 0x0

    .line 358
    .local v80, "x1":I
    const/16 v83, 0x0

    .line 359
    .local v83, "y1":I
    const/16 v81, 0x0

    .line 360
    .local v81, "x2":I
    const/16 v84, 0x0

    .line 362
    .local v84, "y2":I
    and-int/lit8 v85, v30, 0x4

    if-eqz v85, :cond_e

    .line 363
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v86, v0

    mul-float v85, v85, v86

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v80, v0

    .line 364
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v83, v0

    .line 365
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v86, v0

    mul-float v85, v85, v86

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v81, v0

    .line 366
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v84, v0

    .line 367
    add-int/lit8 v60, v60, 0x4

    .line 368
    const/16 v17, 0x1

    .line 371
    :cond_e
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readBits([BI)[B

    move-result-object v15

    .line 372
    .local v15, "bstr":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->wf:Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFFont;

    move-object/from16 v85, v0

    move-object/from16 v0, v85

    invoke-static {v0, v15}, Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFUtilities;->decodeString(Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFFont;[B)Ljava/lang/String;

    move-result-object v66

    .line 374
    .local v66, "sr":Ljava/lang/String;
    add-int/lit8 v85, v45, 0x1

    div-int/lit8 v85, v85, 0x2

    add-int v60, v60, v85

    .line 376
    rem-int/lit8 v85, v45, 0x2

    if-eqz v85, :cond_f

    .line 377
    const/16 v85, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v85

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readBits([BI)[B

    move-result-object v15

    .line 379
    :cond_f
    move/from16 v0, v60

    move/from16 v1, v61

    if-ge v0, v1, :cond_7

    .line 380
    move/from16 v42, v60

    .restart local v42    # "j":I
    :goto_8
    move/from16 v0, v42

    move/from16 v1, v61

    if-ge v0, v1, :cond_7

    .line 381
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 380
    add-int/lit8 v42, v42, 0x1

    goto :goto_8

    .line 387
    .end local v15    # "bstr":[B
    .end local v17    # "clipped":Z
    .end local v30    # "flag":I
    .end local v42    # "j":I
    .end local v45    # "lenText":I
    .end local v60    # "read":I
    .end local v66    # "sr":Ljava/lang/String;
    .end local v79    # "x":I
    .end local v80    # "x1":I
    .end local v81    # "x2":I
    .end local v82    # "y":I
    .end local v83    # "y1":I
    .end local v84    # "y2":I
    :sswitch_8
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v44

    .line 388
    .local v44, "len":I
    const/16 v60, 0x1

    .line 389
    .restart local v60    # "read":I
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v44

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readBits([BI)[B

    move-result-object v15

    .line 390
    .restart local v15    # "bstr":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->wf:Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFFont;

    move-object/from16 v85, v0

    move-object/from16 v0, v85

    invoke-static {v0, v15}, Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFUtilities;->decodeString(Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFFont;[B)Ljava/lang/String;

    move-result-object v66

    .line 392
    .restart local v66    # "sr":Ljava/lang/String;
    rem-int/lit8 v85, v44, 0x2

    if-eqz v85, :cond_10

    .line 393
    const/16 v85, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v85

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readBits([BI)[B

    move-result-object v15

    .line 394
    :cond_10
    add-int/lit8 v85, v44, 0x1

    div-int/lit8 v85, v85, 0x2

    add-int v60, v60, v85

    .line 396
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v82

    .line 397
    .restart local v82    # "y":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v86, v0

    mul-float v85, v85, v86

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v79, v0

    .line 398
    .restart local v79    # "x":I
    add-int/lit8 v60, v60, 0x2

    .line 400
    move/from16 v0, v60

    move/from16 v1, v61

    if-ge v0, v1, :cond_7

    .line 401
    move/from16 v42, v60

    .restart local v42    # "j":I
    :goto_9
    move/from16 v0, v42

    move/from16 v1, v61

    if-ge v0, v1, :cond_7

    .line 402
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 401
    add-int/lit8 v42, v42, 0x1

    goto :goto_9

    .line 407
    .end local v15    # "bstr":[B
    .end local v42    # "j":I
    .end local v44    # "len":I
    .end local v60    # "read":I
    .end local v66    # "sr":Ljava/lang/String;
    .end local v79    # "x":I
    .end local v82    # "y":I
    :sswitch_9
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v48

    .line 408
    .local v48, "lfHeight":S
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleY:F

    move/from16 v85, v0

    move/from16 v0, v48

    int-to-float v0, v0

    move/from16 v86, v0

    mul-float v85, v85, v86

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v85, v0

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v65, v0

    .line 409
    .local v65, "size":F
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v49

    .line 410
    .local v49, "lfWidth":S
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v28

    .line 411
    .local v28, "escape":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v53

    .line 412
    .local v53, "orient":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v73

    .line 414
    .local v73, "weight":I
    add-int/lit8 v85, v61, -0x9

    mul-int/lit8 v44, v85, 0x2

    .line 415
    .restart local v44    # "len":I
    move/from16 v0, v44

    new-array v0, v0, [B

    move-object/from16 v47, v0

    .line 417
    .local v47, "lfFaceName":[B
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v44

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readBits([BI)[B

    move-result-object v47

    .line 419
    new-instance v29, Ljava/lang/String;

    move-object/from16 v0, v29

    move-object/from16 v1, v47

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 421
    .local v29, "face":Ljava/lang/String;
    const/16 v22, 0x0

    .line 423
    .local v22, "d":I
    :goto_a
    invoke-virtual/range {v29 .. v29}, Ljava/lang/String;->length()I

    move-result v85

    move/from16 v0, v22

    move/from16 v1, v85

    if-ge v0, v1, :cond_12

    move-object/from16 v0, v29

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v85

    invoke-static/range {v85 .. v85}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v85

    if-nez v85, :cond_11

    move-object/from16 v0, v29

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v85

    invoke-static/range {v85 .. v85}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v85

    if-eqz v85, :cond_12

    .line 425
    :cond_11
    add-int/lit8 v22, v22, 0x1

    goto :goto_a

    .line 427
    :cond_12
    if-lez v22, :cond_14

    .line 428
    const/16 v85, 0x0

    move-object/from16 v0, v29

    move/from16 v1, v85

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v29

    .line 432
    :goto_b
    const/16 v85, 0x0

    cmpg-float v85, v65, v85

    if-gez v85, :cond_13

    .line 433
    move/from16 v0, v65

    neg-float v0, v0

    move/from16 v65, v0

    .line 434
    :cond_13
    const/16 v85, 0x0

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    goto/16 :goto_3

    .line 430
    :cond_14
    const-string/jumbo v29, "System"

    goto :goto_b

    .line 438
    .end local v22    # "d":I
    .end local v28    # "escape":I
    .end local v29    # "face":Ljava/lang/String;
    .end local v44    # "len":I
    .end local v47    # "lfFaceName":[B
    .end local v48    # "lfHeight":S
    .end local v49    # "lfWidth":S
    .end local v53    # "orient":I
    .end local v65    # "size":F
    .end local v73    # "weight":I
    :sswitch_a
    const/16 v85, 0x0

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    .line 439
    const/16 v42, 0x0

    .restart local v42    # "j":I
    :goto_c
    move/from16 v0, v42

    move/from16 v1, v61

    if-ge v0, v1, :cond_15

    .line 440
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 439
    add-int/lit8 v42, v42, 0x1

    goto :goto_c

    .line 441
    :cond_15
    const/16 v85, 0x6

    sget-object v86, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->INTEGER_0:Ljava/lang/Integer;

    const/16 v87, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v85

    move-object/from16 v2, v86

    move/from16 v3, v87

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->addObjectAt(ILjava/lang/Object;I)I

    move-result v85

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    goto/16 :goto_3

    .line 445
    .end local v42    # "j":I
    :sswitch_b
    const/16 v85, 0x0

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    .line 446
    const/16 v42, 0x0

    .restart local v42    # "j":I
    :goto_d
    move/from16 v0, v42

    move/from16 v1, v61

    if-ge v0, v1, :cond_16

    .line 447
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 446
    add-int/lit8 v42, v42, 0x1

    goto :goto_d

    .line 448
    :cond_16
    const/16 v85, 0x8

    sget-object v86, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->INTEGER_0:Ljava/lang/Integer;

    const/16 v87, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v85

    move-object/from16 v2, v86

    move/from16 v3, v87

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->addObjectAt(ILjava/lang/Object;I)I

    move-result v85

    move/from16 v0, v85

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    goto/16 :goto_3

    .line 452
    .end local v42    # "j":I
    :sswitch_c
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v33

    .line 453
    .local v33, "gdiIndex":I
    const/high16 v85, -0x80000000

    and-int v85, v85, v33

    if-nez v85, :cond_7

    .line 454
    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getObject(I)Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;

    move-result-object v34

    .line 455
    .local v34, "gdiObj":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;
    move-object/from16 v0, v34

    iget-boolean v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->used:Z

    move/from16 v85, v0

    if-eqz v85, :cond_7

    .line 456
    move-object/from16 v0, v34

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->type:I

    move/from16 v85, v0

    packed-switch v85, :pswitch_data_0

    goto/16 :goto_3

    .line 458
    :pswitch_0
    move-object/from16 v0, v34

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->obj:Ljava/lang/Object;

    move-object/from16 v85, v0

    move-object/from16 v0, v85

    instance-of v0, v0, Lorg/apache/poi/java/awt/Color;

    move/from16 v85, v0

    if-eqz v85, :cond_17

    .line 459
    move-object/from16 v0, v34

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->obj:Ljava/lang/Object;

    move-object/from16 v19, v0

    check-cast v19, Lorg/apache/poi/java/awt/Color;

    .line 460
    .local v19, "colorTest":Lorg/apache/poi/java/awt/Color;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->polyPaint:Landroid/graphics/Paint;

    move-object/from16 v85, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v86

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v87

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v88

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v89

    invoke-virtual/range {v85 .. v89}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 466
    .end local v19    # "colorTest":Lorg/apache/poi/java/awt/Color;
    :cond_17
    move/from16 v54, v33

    .line 467
    goto/16 :goto_3

    .line 469
    :pswitch_1
    move-object/from16 v0, v34

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->obj:Ljava/lang/Object;

    move-object/from16 v85, v0

    move-object/from16 v0, v85

    instance-of v0, v0, Lorg/apache/poi/java/awt/Color;

    move/from16 v85, v0

    if-eqz v85, :cond_18

    .line 470
    move-object/from16 v0, v34

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->obj:Ljava/lang/Object;

    move-object/from16 v19, v0

    check-cast v19, Lorg/apache/poi/java/awt/Color;

    .line 471
    .restart local v19    # "colorTest":Lorg/apache/poi/java/awt/Color;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->polyPaint:Landroid/graphics/Paint;

    move-object/from16 v85, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v86

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v87

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v88

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v89

    invoke-virtual/range {v85 .. v89}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 476
    .end local v19    # "colorTest":Lorg/apache/poi/java/awt/Color;
    :cond_18
    move/from16 v13, v33

    .line 477
    goto/16 :goto_3

    .line 479
    :pswitch_2
    move-object/from16 v0, v34

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->obj:Ljava/lang/Object;

    move-object/from16 v85, v0

    check-cast v85, Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFFont;

    move-object/from16 v0, v85

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->wf:Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFFont;

    .line 480
    move/from16 v31, v33

    .line 482
    goto/16 :goto_3

    .line 484
    :pswitch_3
    const/16 v54, -0x1

    .line 485
    goto/16 :goto_3

    .line 487
    :pswitch_4
    const/4 v13, -0x1

    goto/16 :goto_3

    .line 493
    .end local v33    # "gdiIndex":I
    .end local v34    # "gdiObj":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;
    :sswitch_d
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v33

    .line 494
    .restart local v33    # "gdiIndex":I
    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getObject(I)Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;

    move-result-object v34

    .line 495
    .restart local v34    # "gdiObj":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;
    move/from16 v0, v33

    if-ne v0, v13, :cond_1a

    .line 496
    const/4 v13, -0x1

    .line 501
    :cond_19
    :goto_e
    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->clear()V

    goto/16 :goto_3

    .line 497
    :cond_1a
    move/from16 v0, v33

    move/from16 v1, v54

    if-ne v0, v1, :cond_1b

    .line 498
    const/16 v54, -0x1

    goto :goto_e

    .line 499
    :cond_1b
    move/from16 v0, v33

    move/from16 v1, v31

    if-ne v0, v1, :cond_19

    .line 500
    const/16 v31, -0x1

    goto :goto_e

    .line 504
    .end local v33    # "gdiIndex":I
    .end local v34    # "gdiObj":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;
    :sswitch_e
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v82

    .line 505
    .restart local v82    # "y":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v86, v0

    mul-float v85, v85, v86

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v79, v0

    .line 510
    .restart local v79    # "x":I
    goto/16 :goto_3

    .line 515
    .end local v79    # "x":I
    .end local v82    # "y":I
    :sswitch_f
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v21

    .line 516
    .local v21, "count":I
    move/from16 v0, v21

    new-array v0, v0, [I

    move-object/from16 v59, v0

    .line 517
    .local v59, "pts":[I
    const/16 v58, 0x0

    .line 518
    .local v58, "ptCount":I
    const/16 v41, 0x0

    :goto_f
    move/from16 v0, v41

    move/from16 v1, v21

    if-ge v0, v1, :cond_1c

    .line 519
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    aput v85, v59, v41

    .line 520
    aget v85, v59, v41

    add-int v58, v58, v85

    .line 518
    add-int/lit8 v41, v41, 0x1

    goto :goto_f

    .line 523
    :cond_1c
    add-int/lit8 v52, v21, 0x1

    .line 524
    .local v52, "offset":I
    const/16 v41, 0x0

    :goto_10
    move/from16 v0, v41

    move/from16 v1, v21

    if-ge v0, v1, :cond_7

    .line 525
    const/16 v42, 0x0

    .restart local v42    # "j":I
    :goto_11
    aget v85, v59, v41

    move/from16 v0, v42

    move/from16 v1, v85

    if-ge v0, v1, :cond_1f

    .line 526
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v86, v0

    mul-float v85, v85, v86

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v79, v0

    .line 527
    .restart local v79    # "x":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v82, v0

    .line 528
    .restart local v82    # "y":I
    if-gez v13, :cond_1d

    if-ltz v54, :cond_1e

    .line 529
    :cond_1d
    move-object/from16 v0, p0

    move/from16 v1, v79

    move/from16 v2, v82

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->resizeBounds(II)V

    .line 525
    :cond_1e
    add-int/lit8 v42, v42, 0x1

    goto :goto_11

    .line 524
    .end local v79    # "x":I
    .end local v82    # "y":I
    :cond_1f
    add-int/lit8 v41, v41, 0x1

    goto :goto_10

    .line 536
    .end local v21    # "count":I
    .end local v42    # "j":I
    .end local v52    # "offset":I
    .end local v58    # "ptCount":I
    .end local v59    # "pts":[I
    :sswitch_10
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v21

    .line 537
    .restart local v21    # "count":I
    add-int/lit8 v85, v21, 0x1

    move/from16 v0, v85

    new-array v6, v0, [F

    .line 538
    .local v6, "_xpts":[F
    add-int/lit8 v85, v21, 0x1

    move/from16 v0, v85

    new-array v7, v0, [F

    .line 539
    .local v7, "_ypts":[F
    const/16 v41, 0x0

    :goto_12
    move/from16 v0, v41

    move/from16 v1, v21

    if-ge v0, v1, :cond_22

    .line 540
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v86, v0

    mul-float v85, v85, v86

    aput v85, v6, v41

    .line 541
    aget v85, v6, v41

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v85, v0

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    aput v85, v6, v41

    .line 542
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->left:I

    move/from16 v85, v0

    if-gez v85, :cond_20

    .line 543
    aget v85, v6, v41

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->left:I

    move/from16 v86, v0

    mul-int/lit8 v86, v86, -0x1

    move/from16 v0, v86

    int-to-float v0, v0

    move/from16 v86, v0

    add-float v85, v85, v86

    aput v85, v6, v41

    .line 545
    :cond_20
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    aput v85, v7, v41

    .line 546
    aget v85, v7, v41

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v85, v0

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    aput v85, v7, v41

    .line 547
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->top:I

    move/from16 v85, v0

    if-gez v85, :cond_21

    .line 548
    aget v85, v7, v41

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->top:I

    move/from16 v86, v0

    mul-int/lit8 v86, v86, -0x1

    move/from16 v0, v86

    int-to-float v0, v0

    move/from16 v86, v0

    add-float v85, v85, v86

    aput v85, v7, v41

    .line 539
    :cond_21
    add-int/lit8 v41, v41, 0x1

    goto/16 :goto_12

    .line 551
    :cond_22
    const/16 v85, 0x0

    aget v85, v6, v85

    aput v85, v6, v21

    .line 552
    const/16 v85, 0x0

    aget v85, v7, v85

    aput v85, v7, v21

    .line 554
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->polyPaint:Landroid/graphics/Paint;

    move-object/from16 v85, v0

    sget-object v86, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual/range {v85 .. v86}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 557
    new-instance v57, Landroid/graphics/Path;

    invoke-direct/range {v57 .. v57}, Landroid/graphics/Path;-><init>()V

    .line 558
    .local v57, "polyPath":Landroid/graphics/Path;
    const/16 v85, 0x0

    aget v85, v6, v85

    const/16 v86, 0x0

    aget v86, v7, v86

    move-object/from16 v0, v57

    move/from16 v1, v85

    move/from16 v2, v86

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 559
    const/16 v41, 0x0

    :goto_13
    move/from16 v0, v41

    move/from16 v1, v21

    if-ge v0, v1, :cond_23

    .line 560
    aget v85, v6, v41

    aget v86, v7, v41

    move-object/from16 v0, v57

    move/from16 v1, v85

    move/from16 v2, v86

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 559
    add-int/lit8 v41, v41, 0x1

    goto :goto_13

    .line 562
    :cond_23
    const/16 v85, 0x0

    aget v85, v6, v85

    const/16 v86, 0x0

    aget v86, v7, v86

    move-object/from16 v0, v57

    move/from16 v1, v85

    move/from16 v2, v86

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 565
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v85, v0

    move-object/from16 v0, v85

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v85, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->polyPaint:Landroid/graphics/Paint;

    move-object/from16 v86, v0

    move-object/from16 v0, v85

    move-object/from16 v1, v57

    move-object/from16 v2, v86

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 569
    .end local v6    # "_xpts":[F
    .end local v7    # "_ypts":[F
    .end local v21    # "count":I
    .end local v57    # "polyPath":Landroid/graphics/Path;
    :sswitch_11
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v21

    .line 570
    .restart local v21    # "count":I
    move/from16 v0, v21

    new-array v6, v0, [F

    .line 571
    .restart local v6    # "_xpts":[F
    move/from16 v0, v21

    new-array v7, v0, [F

    .line 572
    .restart local v7    # "_ypts":[F
    const/16 v41, 0x0

    :goto_14
    move/from16 v0, v41

    move/from16 v1, v21

    if-ge v0, v1, :cond_24

    .line 573
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v86, v0

    mul-float v85, v85, v86

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v85, v0

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    aput v85, v6, v41

    .line 574
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v85, v0

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    aput v85, v7, v41

    .line 572
    add-int/lit8 v41, v41, 0x1

    goto :goto_14

    .line 576
    :cond_24
    const/16 v85, 0x0

    aget v85, v6, v85

    aput v85, v6, v21

    .line 577
    const/16 v85, 0x0

    aget v85, v7, v85

    aput v85, v7, v21

    .line 579
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->polyPaint:Landroid/graphics/Paint;

    move-object/from16 v85, v0

    sget-object v86, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual/range {v85 .. v86}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 582
    new-instance v56, Landroid/graphics/Path;

    invoke-direct/range {v56 .. v56}, Landroid/graphics/Path;-><init>()V

    .line 583
    .local v56, "polyLine":Landroid/graphics/Path;
    const/16 v85, 0x0

    aget v85, v6, v85

    const/16 v86, 0x0

    aget v86, v7, v86

    move-object/from16 v0, v56

    move/from16 v1, v85

    move/from16 v2, v86

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 584
    const/16 v41, 0x0

    :goto_15
    move/from16 v0, v41

    move/from16 v1, v21

    if-ge v0, v1, :cond_25

    .line 585
    aget v85, v6, v41

    aget v86, v7, v41

    move-object/from16 v0, v56

    move/from16 v1, v85

    move/from16 v2, v86

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 584
    add-int/lit8 v41, v41, 0x1

    goto :goto_15

    .line 587
    :cond_25
    const/16 v85, 0x0

    aget v85, v6, v85

    const/16 v86, 0x0

    aget v86, v7, v86

    move-object/from16 v0, v56

    move/from16 v1, v85

    move/from16 v2, v86

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 590
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v85, v0

    move-object/from16 v0, v85

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v85, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->polyPaint:Landroid/graphics/Paint;

    move-object/from16 v86, v0

    move-object/from16 v0, v85

    move-object/from16 v1, v56

    move-object/from16 v2, v86

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 596
    .end local v6    # "_xpts":[F
    .end local v7    # "_ypts":[F
    .end local v21    # "count":I
    .end local v56    # "polyLine":Landroid/graphics/Path;
    :sswitch_12
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v12, v0

    .line 597
    .local v12, "bot":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v86, v0

    mul-float v85, v85, v86

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v64, v0

    .line 598
    .local v64, "right":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v72, v0

    .line 599
    .local v72, "top":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v86, v0

    mul-float v85, v85, v86

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v43, v0

    .line 600
    .local v43, "left":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v85, v0

    move-object/from16 v0, v85

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v85, v0

    new-instance v86, Landroid/graphics/Rect;

    sub-int v87, v64, v43

    sub-int v88, v12, v72

    move-object/from16 v0, v86

    move/from16 v1, v43

    move/from16 v2, v72

    move/from16 v3, v87

    move/from16 v4, v88

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->polyPaint:Landroid/graphics/Paint;

    move-object/from16 v87, v0

    invoke-virtual/range {v85 .. v87}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 605
    .end local v12    # "bot":I
    .end local v43    # "left":I
    .end local v64    # "right":I
    .end local v72    # "top":I
    :sswitch_13
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 606
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 607
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v12, v0

    .line 608
    .restart local v12    # "bot":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v86, v0

    mul-float v85, v85, v86

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v64, v0

    .line 609
    .restart local v64    # "right":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v72, v0

    .line 610
    .restart local v72    # "top":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v86, v0

    mul-float v85, v85, v86

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v43, v0

    .line 611
    .restart local v43    # "left":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v85, v0

    move-object/from16 v0, v85

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v85, v0

    new-instance v86, Landroid/graphics/RectF;

    move/from16 v0, v43

    int-to-float v0, v0

    move/from16 v87, v0

    move/from16 v0, v72

    int-to-float v0, v0

    move/from16 v88, v0

    sub-int v89, v64, v43

    move/from16 v0, v89

    int-to-float v0, v0

    move/from16 v89, v0

    sub-int v90, v12, v72

    move/from16 v0, v90

    int-to-float v0, v0

    move/from16 v90, v0

    invoke-direct/range {v86 .. v90}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v87, 0x3f000000    # 0.5f

    const/high16 v88, 0x3f000000    # 0.5f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->polyPaint:Landroid/graphics/Paint;

    move-object/from16 v89, v0

    invoke-virtual/range {v85 .. v89}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 618
    .end local v12    # "bot":I
    .end local v43    # "left":I
    .end local v64    # "right":I
    .end local v72    # "top":I
    :sswitch_14
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 619
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 620
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 621
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 622
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v12, v0

    .line 623
    .restart local v12    # "bot":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v86, v0

    mul-float v85, v85, v86

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v64, v0

    .line 624
    .restart local v64    # "right":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v72, v0

    .line 625
    .restart local v72    # "top":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v86, v0

    mul-float v85, v85, v86

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v43, v0

    .line 626
    .restart local v43    # "left":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v85, v0

    move-object/from16 v0, v85

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v85, v0

    new-instance v86, Landroid/graphics/Rect;

    sub-int v87, v64, v43

    sub-int v88, v12, v72

    move-object/from16 v0, v86

    move/from16 v1, v43

    move/from16 v2, v72

    move/from16 v3, v87

    move/from16 v4, v88

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->polyPaint:Landroid/graphics/Paint;

    move-object/from16 v87, v0

    invoke-virtual/range {v85 .. v87}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 631
    .end local v12    # "bot":I
    .end local v43    # "left":I
    .end local v64    # "right":I
    .end local v72    # "top":I
    :sswitch_15
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readInt([B)I

    .line 632
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v36, v0

    .line 633
    .local v36, "height":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v86, v0

    mul-float v85, v85, v86

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v74, v0

    .line 634
    .local v74, "width":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v86, v0

    mul-float v85, v85, v86

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v43, v0

    .line 635
    .restart local v43    # "left":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v72, v0

    .line 636
    .restart local v72    # "top":I
    if-ltz v54, :cond_26

    .line 637
    move-object/from16 v0, p0

    move/from16 v1, v43

    move/from16 v2, v72

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->resizeBounds(II)V

    .line 638
    :cond_26
    if-ltz v54, :cond_7

    .line 639
    add-int v85, v43, v74

    add-int v86, v72, v36

    move-object/from16 v0, p0

    move/from16 v1, v85

    move/from16 v2, v86

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->resizeBounds(II)V

    goto/16 :goto_3

    .line 643
    .end local v36    # "height":I
    .end local v43    # "left":I
    .end local v72    # "top":I
    .end local v74    # "width":I
    :sswitch_16
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readInt([B)I

    move-result v85

    move/from16 v0, v85

    and-int/lit16 v0, v0, 0xff

    move/from16 v51, v0

    .line 644
    .local v51, "mode":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->ySign:I

    move/from16 v86, v0

    mul-int v40, v85, v86

    .line 645
    .local v40, "heightSrc":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->xSign:I

    move/from16 v86, v0

    mul-int v78, v85, v86

    .line 646
    .local v78, "widthSrc":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->ySign:I

    move/from16 v86, v0

    mul-int v69, v85, v86

    .line 647
    .local v69, "sy":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->xSign:I

    move/from16 v86, v0

    mul-int v67, v85, v86

    .line 648
    .local v67, "sx":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->ySign:I

    move/from16 v86, v0

    mul-int v38, v85, v86

    .line 649
    .local v38, "heightDst":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->xSign:I

    move/from16 v86, v0

    mul-int v85, v85, v86

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v86, v0

    mul-float v85, v85, v86

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v76, v0

    .line 650
    .local v76, "widthDst":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->ySign:I

    move/from16 v86, v0

    mul-int v26, v85, v86

    .line 651
    .local v26, "dy":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->xSign:I

    move/from16 v86, v0

    mul-int v85, v85, v86

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v86, v0

    mul-float v85, v85, v86

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v24, v0

    .line 653
    .local v24, "dx":I
    mul-int/lit8 v85, v61, 0x2

    add-int/lit8 v46, v85, -0x14

    .line 654
    .local v46, "lenbitmap":I
    move/from16 v0, v46

    new-array v9, v0, [B

    .line 655
    .local v9, "bitmap":[B
    const/16 v41, 0x0

    :goto_16
    move/from16 v0, v41

    move/from16 v1, v46

    if-ge v0, v1, :cond_27

    .line 656
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readByte([B)B

    move-result v85

    aput-byte v85, v9, v41

    .line 655
    add-int/lit8 v41, v41, 0x1

    goto :goto_16

    .line 658
    :cond_27
    move/from16 v0, v40

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v37, v0

    .line 659
    .local v37, "heightA":I
    move/from16 v0, v78

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v75, v0

    .line 660
    .local v75, "widthA":I
    move/from16 v0, v69

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v70, v0

    .line 661
    .local v70, "syA":I
    move/from16 v0, v67

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v85

    move/from16 v0, v85

    float-to-int v0, v0

    move/from16 v68, v0

    .line 662
    .local v68, "sxA":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->vpY:I

    move/from16 v86, v0

    add-int v86, v86, v26

    move/from16 v0, v86

    int-to-float v0, v0

    move/from16 v86, v0

    mul-float v85, v85, v86

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v27

    .line 663
    .local v27, "dyA":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->vpX:I

    move/from16 v86, v0

    add-int v86, v86, v24

    move/from16 v0, v86

    int-to-float v0, v0

    move/from16 v86, v0

    mul-float v85, v85, v86

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v25

    .line 664
    .local v25, "dxA":F
    move/from16 v0, v38

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v39

    .line 665
    .local v39, "heightDstA":F
    move/from16 v0, v76

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v77

    .line 666
    .local v77, "widthDstA":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v85, v0

    mul-float v77, v77, v85

    .line 667
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v85, v0

    mul-float v39, v39, v85

    .line 669
    const/4 v11, 0x0

    .line 671
    .local v11, "bmp":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getImage([B)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 673
    if-eqz v11, :cond_7

    .line 675
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v85, v0

    move-object/from16 v0, v85

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v85, v0

    new-instance v86, Landroid/graphics/Rect;

    add-int v87, v68, v75

    add-int v88, v70, v37

    move-object/from16 v0, v86

    move/from16 v1, v68

    move/from16 v2, v70

    move/from16 v3, v87

    move/from16 v4, v88

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v87, Landroid/graphics/Rect;

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v88, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v89, v0

    add-float v90, v25, v77

    move/from16 v0, v90

    float-to-int v0, v0

    move/from16 v90, v0

    add-float v91, v27, v39

    move/from16 v0, v91

    float-to-int v0, v0

    move/from16 v91, v0

    invoke-direct/range {v87 .. v91}, Landroid/graphics/Rect;-><init>(IIII)V

    const/16 v88, 0x0

    move-object/from16 v0, v85

    move-object/from16 v1, v86

    move-object/from16 v2, v87

    move-object/from16 v3, v88

    invoke-virtual {v0, v11, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 683
    .end local v9    # "bitmap":[B
    .end local v11    # "bmp":Landroid/graphics/Bitmap;
    .end local v24    # "dx":I
    .end local v25    # "dxA":F
    .end local v26    # "dy":I
    .end local v27    # "dyA":F
    .end local v37    # "heightA":I
    .end local v38    # "heightDst":I
    .end local v39    # "heightDstA":F
    .end local v40    # "heightSrc":I
    .end local v46    # "lenbitmap":I
    .end local v51    # "mode":I
    .end local v67    # "sx":I
    .end local v68    # "sxA":I
    .end local v69    # "sy":I
    .end local v70    # "syA":I
    .end local v75    # "widthA":I
    .end local v76    # "widthDst":I
    .end local v77    # "widthDstA":F
    .end local v78    # "widthSrc":I
    :sswitch_17
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readInt([B)I

    .line 684
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 685
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 686
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 687
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 688
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 689
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v38

    .line 690
    .local v38, "heightDst":F
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v85

    move/from16 v0, v85

    int-to-float v0, v0

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v86, v0

    mul-float v85, v85, v86

    move-object/from16 v0, p0

    move/from16 v1, v85

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    goto/16 :goto_3

    .line 695
    .end local v38    # "heightDst":F
    :sswitch_18
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readInt([B)I

    .line 696
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 697
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 698
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    goto/16 :goto_3

    .line 262
    :sswitch_data_0
    .sparse-switch
        0xf7 -> :sswitch_b
        0x103 -> :sswitch_1
        0x12d -> :sswitch_c
        0x12e -> :sswitch_6
        0x1f0 -> :sswitch_d
        0x20b -> :sswitch_2
        0x20c -> :sswitch_3
        0x213 -> :sswitch_e
        0x214 -> :sswitch_0
        0x2fa -> :sswitch_4
        0x2fb -> :sswitch_9
        0x2fc -> :sswitch_5
        0x324 -> :sswitch_10
        0x325 -> :sswitch_11
        0x416 -> :sswitch_12
        0x418 -> :sswitch_12
        0x41b -> :sswitch_12
        0x521 -> :sswitch_8
        0x538 -> :sswitch_f
        0x61c -> :sswitch_13
        0x61d -> :sswitch_15
        0x62f -> :sswitch_8
        0x6ff -> :sswitch_a
        0x817 -> :sswitch_14
        0x81a -> :sswitch_14
        0x830 -> :sswitch_14
        0x940 -> :sswitch_18
        0xa32 -> :sswitch_7
        0xb41 -> :sswitch_16
        0xf43 -> :sswitch_17
    .end sparse-switch

    .line 456
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected readBits([BI)[B
    .locals 5
    .param p1, "is"    # [B
    .param p2, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1443
    new-array v1, p2, [B

    .line 1444
    .local v1, "shortConvert":[B
    const/4 v0, 0x0

    .line 1446
    .local v0, "i":I
    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v2, p2, -0x1

    if-gt v0, v2, :cond_0

    .line 1447
    add-int/lit8 v2, p2, -0x1

    sub-int/2addr v2, v0

    iget-object v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->fileContent:[B

    iget v4, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mOffset:I

    add-int/2addr v4, v0

    aget-byte v3, v3, v4

    aput-byte v3, v1, v2

    .line 1446
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1450
    :cond_0
    return-object v1
.end method

.method public readWmfDoc([B)Landroid/graphics/Bitmap;
    .locals 90
    .param p1, "is"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 860
    const/16 v83, 0x0

    move-object/from16 v0, v83

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mBitmap:Landroid/graphics/Bitmap;

    .line 862
    const/16 v50, 0x0

    .line 863
    .local v50, "numberOfRecord":I
    const/16 v83, 0x0

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mOffset:I

    .line 864
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    move-object/from16 v83, v0

    new-instance v84, Ljava/util/ArrayList;

    invoke-direct/range {v84 .. v84}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v84

    move-object/from16 v1, v83

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    .line 866
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->left:I

    move/from16 v83, v0

    if-gez v83, :cond_0

    .line 867
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->right:I

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->left:I

    move/from16 v84, v0

    mul-int/lit8 v84, v84, -0x1

    add-int v83, v83, v84

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->right:I

    .line 868
    const/16 v83, 0x1

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mFlipTheImage:Z

    .line 871
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->top:I

    move/from16 v83, v0

    if-gez v83, :cond_1

    .line 872
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->bottom:I

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->top:I

    move/from16 v84, v0

    mul-int/lit8 v84, v84, -0x1

    add-int v83, v83, v84

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->bottom:I

    .line 873
    const/16 v83, 0x1

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mFlipTheImage:Z

    .line 876
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->left:I

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->right:I

    move/from16 v84, v0

    move/from16 v0, v83

    move/from16 v1, v84

    if-le v0, v1, :cond_2

    .line 877
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->right:I

    .line 878
    .local v5, "_i":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->left:I

    move/from16 v83, v0

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->right:I

    .line 879
    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->left:I

    .line 880
    const/16 v83, -0x1

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->xSign:I

    .line 882
    .end local v5    # "_i":I
    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->top:I

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->bottom:I

    move/from16 v84, v0

    move/from16 v0, v83

    move/from16 v1, v84

    if-le v0, v1, :cond_3

    .line 883
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->bottom:I

    .line 884
    .restart local v5    # "_i":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->top:I

    move/from16 v83, v0

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->bottom:I

    .line 885
    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->top:I

    .line 886
    const/16 v83, -0x1

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->ySign:I

    .line 889
    .end local v5    # "_i":I
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->right:I

    move/from16 v83, v0

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->width:I

    .line 890
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->bottom:I

    move/from16 v83, v0

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->height:I

    .line 892
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->top:I

    move/from16 v83, v0

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v83, v0

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->top:I

    .line 893
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->bottom:I

    move/from16 v83, v0

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v83, v0

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->bottom:I

    .line 894
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->left:I

    move/from16 v83, v0

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v83, v0

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->left:I

    .line 895
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->right:I

    move/from16 v83, v0

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v83, v0

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->right:I

    .line 897
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v83, v0

    move-object/from16 v0, v83

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v83, v0

    if-nez v83, :cond_4

    .line 898
    const/16 v83, 0x0

    .line 1402
    :goto_0
    return-object v83

    .line 930
    :cond_4
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mtType:I

    .line 931
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mtHeaderSize:I

    .line 933
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mtVersion:I

    .line 934
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readInt([B)I

    move-result v83

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mtSize:I

    .line 935
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mtNoObjects:I

    .line 936
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readInt([B)I

    move-result v83

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mtMaxRecord:I

    .line 937
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mtNoParameters:I

    .line 939
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mtNoObjects:I

    move/from16 v83, v0

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->numObjects:I

    .line 940
    new-instance v69, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->numObjects:I

    move/from16 v83, v0

    move-object/from16 v0, v69

    move/from16 v1, v83

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 941
    .local v69, "tempList":Ljava/util/List;
    const/16 v39, 0x0

    .local v39, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->numObjects:I

    move/from16 v83, v0

    move/from16 v0, v39

    move/from16 v1, v83

    if-ge v0, v1, :cond_5

    .line 942
    new-instance v83, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;

    const/16 v84, 0x0

    move-object/from16 v0, v83

    move/from16 v1, v39

    move/from16 v2, v84

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;-><init>(IZ)V

    move-object/from16 v0, v69

    move-object/from16 v1, v83

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 941
    add-int/lit8 v39, v39, 0x1

    goto :goto_1

    .line 944
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->objectVector:Ljava/util/List;

    move-object/from16 v83, v0

    move-object/from16 v0, v83

    move-object/from16 v1, v69

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 945
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->width:I

    move/from16 v83, v0

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->height:I

    move/from16 v84, v0

    move/from16 v0, v84

    int-to-float v0, v0

    move/from16 v84, v0

    move-object/from16 v0, p0

    move/from16 v1, v84

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v84

    move/from16 v0, v84

    float-to-int v0, v0

    move/from16 v84, v0

    sget-object v85, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static/range {v83 .. v85}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v83

    move-object/from16 v0, v83

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mBitmap:Landroid/graphics/Bitmap;

    .line 948
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v83, v0

    move-object/from16 v0, v83

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v83, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v84, v0

    invoke-virtual/range {v83 .. v84}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 949
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v83, v0

    move-object/from16 v0, v83

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v83, v0

    const/16 v84, -0x1

    invoke-virtual/range {v83 .. v84}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 951
    const/16 v30, 0x1

    .line 952
    .local v30, "functionId":S
    const/16 v60, 0x0

    .line 954
    .local v60, "recSize":I
    const/4 v13, -0x1

    .line 955
    .local v13, "brushObject":I
    const/16 v53, -0x1

    .line 956
    .local v53, "penObject":I
    const/16 v29, -0x1

    .line 958
    .local v29, "fontObject":I
    :cond_6
    :goto_2
    :sswitch_0
    if-lez v30, :cond_7

    .line 960
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readInt([B)I

    move-result v60

    .line 962
    add-int/lit8 v60, v60, -0x3

    .line 964
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v30

    .line 965
    if-gtz v30, :cond_8

    .line 1401
    :cond_7
    const/16 v83, 0x0

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mOffset:I

    .line 1402
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v83, v0

    goto/16 :goto_0

    .line 970
    :cond_8
    sparse-switch v30, :sswitch_data_0

    .line 1395
    const/16 v40, 0x0

    .local v40, "j":I
    :goto_3
    move/from16 v0, v40

    move/from16 v1, v60

    if-ge v0, v1, :cond_6

    .line 1396
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 1395
    add-int/lit8 v40, v40, 0x1

    goto :goto_3

    .line 973
    .end local v40    # "j":I
    :sswitch_1
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v48

    .line 975
    .local v48, "mapmode":I
    const/16 v83, 0x8

    move/from16 v0, v48

    move/from16 v1, v83

    if-ne v0, v1, :cond_6

    .line 976
    const/16 v83, 0x0

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->isotropic:Z

    goto :goto_2

    .line 979
    .end local v48    # "mapmode":I
    :sswitch_2
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->vpY:I

    .line 980
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->vpX:I

    goto :goto_2

    .line 984
    :sswitch_3
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->vpH:I

    .line 985
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->vpW:I

    .line 987
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->isotropic:Z

    move/from16 v83, v0

    if-nez v83, :cond_9

    .line 988
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->vpW:I

    move/from16 v83, v0

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->vpH:I

    move/from16 v84, v0

    move/from16 v0, v84

    int-to-float v0, v0

    move/from16 v84, v0

    div-float v83, v83, v84

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    .line 989
    :cond_9
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->vpW:I

    move/from16 v83, v0

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v84, v0

    mul-float v83, v83, v84

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v83, v0

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->vpW:I

    goto/16 :goto_2

    .line 993
    :sswitch_4
    const/16 v83, 0x0

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    .line 994
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v54

    .line 996
    .local v54, "penStyle":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readInt([B)I

    .line 998
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readInt([B)I

    move-result v19

    .line 999
    .local v19, "colorref":I
    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v61, v0

    .line 1000
    .local v61, "red":I
    const v83, 0xff00

    and-int v83, v83, v19

    shr-int/lit8 v33, v83, 0x8

    .line 1001
    .local v33, "green":I
    const/high16 v83, 0xff0000

    and-int v83, v83, v19

    shr-int/lit8 v10, v83, 0x10

    .line 1002
    .local v10, "blue":I
    new-instance v17, Lorg/apache/poi/java/awt/Color;

    move-object/from16 v0, v17

    move/from16 v1, v61

    move/from16 v2, v33

    invoke-direct {v0, v1, v2, v10}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    .line 1005
    .local v17, "color":Lorg/apache/poi/java/awt/Color;
    const/16 v83, 0x6

    move/from16 v0, v60

    move/from16 v1, v83

    if-ne v0, v1, :cond_a

    .line 1006
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 1007
    :cond_a
    const/16 v83, 0x5

    move/from16 v0, v54

    move/from16 v1, v83

    if-ne v0, v1, :cond_b

    .line 1008
    const/16 v83, 0x4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    move/from16 v84, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    move-object/from16 v2, v17

    move/from16 v3, v84

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->addObjectAt(ILjava/lang/Object;I)I

    move-result v83

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    .line 1012
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->polyPaint:Landroid/graphics/Paint;

    move-object/from16 v83, v0

    const/16 v84, 0xff

    move-object/from16 v0, v83

    move/from16 v1, v84

    move/from16 v2, v61

    move/from16 v3, v33

    invoke-virtual {v0, v1, v2, v3, v10}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_2

    .line 1010
    :cond_b
    const/16 v83, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    move/from16 v84, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    move-object/from16 v2, v17

    move/from16 v3, v84

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->addObjectAt(ILjava/lang/Object;I)I

    move-result v83

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    goto :goto_4

    .line 1016
    .end local v10    # "blue":I
    .end local v17    # "color":Lorg/apache/poi/java/awt/Color;
    .end local v19    # "colorref":I
    .end local v33    # "green":I
    .end local v54    # "penStyle":I
    .end local v61    # "red":I
    :sswitch_5
    const/16 v83, 0x0

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    .line 1017
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v14

    .line 1019
    .local v14, "brushStyle":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readInt([B)I

    move-result v19

    .line 1020
    .restart local v19    # "colorref":I
    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v61, v0

    .line 1021
    .restart local v61    # "red":I
    const v83, 0xff00

    and-int v83, v83, v19

    shr-int/lit8 v33, v83, 0x8

    .line 1022
    .restart local v33    # "green":I
    const/high16 v83, 0xff0000

    and-int v83, v83, v19

    shr-int/lit8 v10, v83, 0x10

    .line 1023
    .restart local v10    # "blue":I
    new-instance v17, Lorg/apache/poi/java/awt/Color;

    move-object/from16 v0, v17

    move/from16 v1, v61

    move/from16 v2, v33

    invoke-direct {v0, v1, v2, v10}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    .line 1025
    .restart local v17    # "color":Lorg/apache/poi/java/awt/Color;
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 1026
    const/16 v83, 0x5

    move/from16 v0, v83

    if-ne v14, v0, :cond_c

    .line 1027
    const/16 v83, 0x5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    move/from16 v84, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    move-object/from16 v2, v17

    move/from16 v3, v84

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->addObjectAt(ILjava/lang/Object;I)I

    move-result v83

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    .line 1031
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->polyPaint:Landroid/graphics/Paint;

    move-object/from16 v83, v0

    const/16 v84, 0xff

    move-object/from16 v0, v83

    move/from16 v1, v84

    move/from16 v2, v61

    move/from16 v3, v33

    invoke-virtual {v0, v1, v2, v3, v10}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_2

    .line 1029
    :cond_c
    const/16 v83, 0x2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    move/from16 v84, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    move-object/from16 v2, v17

    move/from16 v3, v84

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->addObjectAt(ILjava/lang/Object;I)I

    move-result v83

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    goto :goto_5

    .line 1034
    .end local v10    # "blue":I
    .end local v14    # "brushStyle":I
    .end local v17    # "color":Lorg/apache/poi/java/awt/Color;
    .end local v19    # "colorref":I
    .end local v33    # "green":I
    .end local v61    # "red":I
    :sswitch_6
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v8

    .line 1036
    .local v8, "align":I
    const/16 v83, 0x1

    move/from16 v0, v60

    move/from16 v1, v83

    if-le v0, v1, :cond_6

    .line 1037
    const/16 v39, 0x1

    :goto_6
    move/from16 v0, v39

    move/from16 v1, v60

    if-ge v0, v1, :cond_6

    .line 1038
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 1037
    add-int/lit8 v39, v39, 0x1

    goto :goto_6

    .line 1045
    .end local v8    # "align":I
    :sswitch_7
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v80, v0

    .line 1046
    .local v80, "y":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v84, v0

    mul-float v83, v83, v84

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v83, v0

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v77, v0

    .line 1047
    .local v77, "x":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v43

    .line 1048
    .local v43, "lenText":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v28

    .line 1049
    .local v28, "flag":I
    const/16 v59, 0x4

    .line 1050
    .local v59, "read":I
    const/16 v16, 0x0

    .line 1051
    .local v16, "clipped":Z
    const/16 v78, 0x0

    .line 1052
    .local v78, "x1":I
    const/16 v81, 0x0

    .line 1053
    .local v81, "y1":I
    const/16 v79, 0x0

    .line 1054
    .local v79, "x2":I
    const/16 v82, 0x0

    .line 1056
    .local v82, "y2":I
    and-int/lit8 v83, v28, 0x4

    if-eqz v83, :cond_d

    .line 1057
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v84, v0

    mul-float v83, v83, v84

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v78, v0

    .line 1058
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v81, v0

    .line 1059
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v84, v0

    mul-float v83, v83, v84

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v79, v0

    .line 1060
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v82, v0

    .line 1061
    add-int/lit8 v59, v59, 0x4

    .line 1062
    const/16 v16, 0x1

    .line 1065
    :cond_d
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v43

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readBits([BI)[B

    move-result-object v15

    .line 1066
    .local v15, "bstr":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->wf:Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFFont;

    move-object/from16 v83, v0

    move-object/from16 v0, v83

    invoke-static {v0, v15}, Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFUtilities;->decodeString(Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFFont;[B)Ljava/lang/String;

    move-result-object v64

    .line 1068
    .local v64, "sr":Ljava/lang/String;
    add-int/lit8 v83, v43, 0x1

    div-int/lit8 v83, v83, 0x2

    add-int v59, v59, v83

    .line 1070
    rem-int/lit8 v83, v43, 0x2

    if-eqz v83, :cond_e

    .line 1071
    const/16 v83, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v83

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readBits([BI)[B

    move-result-object v15

    .line 1073
    :cond_e
    move/from16 v0, v59

    move/from16 v1, v60

    if-ge v0, v1, :cond_6

    .line 1074
    move/from16 v40, v59

    .restart local v40    # "j":I
    :goto_7
    move/from16 v0, v40

    move/from16 v1, v60

    if-ge v0, v1, :cond_6

    .line 1075
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 1074
    add-int/lit8 v40, v40, 0x1

    goto :goto_7

    .line 1081
    .end local v15    # "bstr":[B
    .end local v16    # "clipped":Z
    .end local v28    # "flag":I
    .end local v40    # "j":I
    .end local v43    # "lenText":I
    .end local v59    # "read":I
    .end local v64    # "sr":Ljava/lang/String;
    .end local v77    # "x":I
    .end local v78    # "x1":I
    .end local v79    # "x2":I
    .end local v80    # "y":I
    .end local v81    # "y1":I
    .end local v82    # "y2":I
    :sswitch_8
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v42

    .line 1082
    .local v42, "len":I
    const/16 v59, 0x1

    .line 1083
    .restart local v59    # "read":I
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v42

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readBits([BI)[B

    move-result-object v15

    .line 1084
    .restart local v15    # "bstr":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->wf:Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFFont;

    move-object/from16 v83, v0

    move-object/from16 v0, v83

    invoke-static {v0, v15}, Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFUtilities;->decodeString(Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFFont;[B)Ljava/lang/String;

    move-result-object v64

    .line 1086
    .restart local v64    # "sr":Ljava/lang/String;
    rem-int/lit8 v83, v42, 0x2

    if-eqz v83, :cond_f

    .line 1087
    const/16 v83, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v83

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readBits([BI)[B

    move-result-object v15

    .line 1088
    :cond_f
    add-int/lit8 v83, v42, 0x1

    div-int/lit8 v83, v83, 0x2

    add-int v59, v59, v83

    .line 1090
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v80

    .line 1091
    .restart local v80    # "y":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v84, v0

    mul-float v83, v83, v84

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v77, v0

    .line 1092
    .restart local v77    # "x":I
    add-int/lit8 v59, v59, 0x2

    .line 1094
    move/from16 v0, v59

    move/from16 v1, v60

    if-ge v0, v1, :cond_6

    .line 1095
    move/from16 v40, v59

    .restart local v40    # "j":I
    :goto_8
    move/from16 v0, v40

    move/from16 v1, v60

    if-ge v0, v1, :cond_6

    .line 1096
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 1095
    add-int/lit8 v40, v40, 0x1

    goto :goto_8

    .line 1101
    .end local v15    # "bstr":[B
    .end local v40    # "j":I
    .end local v42    # "len":I
    .end local v59    # "read":I
    .end local v64    # "sr":Ljava/lang/String;
    .end local v77    # "x":I
    .end local v80    # "y":I
    :sswitch_9
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v46

    .line 1102
    .local v46, "lfHeight":S
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleY:F

    move/from16 v83, v0

    move/from16 v0, v46

    int-to-float v0, v0

    move/from16 v84, v0

    mul-float v83, v83, v84

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v83, v0

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v63, v0

    .line 1103
    .local v63, "size":F
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v47

    .line 1104
    .local v47, "lfWidth":S
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v26

    .line 1105
    .local v26, "escape":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v52

    .line 1106
    .local v52, "orient":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v71

    .line 1108
    .local v71, "weight":I
    add-int/lit8 v83, v60, -0x9

    mul-int/lit8 v42, v83, 0x2

    .line 1109
    .restart local v42    # "len":I
    move/from16 v0, v42

    new-array v0, v0, [B

    move-object/from16 v45, v0

    .line 1111
    .local v45, "lfFaceName":[B
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v42

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readBits([BI)[B

    move-result-object v45

    .line 1113
    new-instance v27, Ljava/lang/String;

    move-object/from16 v0, v27

    move-object/from16 v1, v45

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 1115
    .local v27, "face":Ljava/lang/String;
    const/16 v21, 0x0

    .line 1117
    .local v21, "d":I
    :goto_9
    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->length()I

    move-result v83

    move/from16 v0, v21

    move/from16 v1, v83

    if-ge v0, v1, :cond_11

    move-object/from16 v0, v27

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v83

    invoke-static/range {v83 .. v83}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v83

    if-nez v83, :cond_10

    move-object/from16 v0, v27

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v83

    invoke-static/range {v83 .. v83}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v83

    if-eqz v83, :cond_11

    .line 1119
    :cond_10
    add-int/lit8 v21, v21, 0x1

    goto :goto_9

    .line 1121
    :cond_11
    if-lez v21, :cond_13

    .line 1122
    const/16 v83, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v83

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v27

    .line 1126
    :goto_a
    const/16 v83, 0x0

    cmpg-float v83, v63, v83

    if-gez v83, :cond_12

    .line 1127
    move/from16 v0, v63

    neg-float v0, v0

    move/from16 v63, v0

    .line 1128
    :cond_12
    const/16 v83, 0x0

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    goto/16 :goto_2

    .line 1124
    :cond_13
    const-string/jumbo v27, "System"

    goto :goto_a

    .line 1132
    .end local v21    # "d":I
    .end local v26    # "escape":I
    .end local v27    # "face":Ljava/lang/String;
    .end local v42    # "len":I
    .end local v45    # "lfFaceName":[B
    .end local v46    # "lfHeight":S
    .end local v47    # "lfWidth":S
    .end local v52    # "orient":I
    .end local v63    # "size":F
    .end local v71    # "weight":I
    :sswitch_a
    const/16 v83, 0x0

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    .line 1133
    const/16 v40, 0x0

    .restart local v40    # "j":I
    :goto_b
    move/from16 v0, v40

    move/from16 v1, v60

    if-ge v0, v1, :cond_14

    .line 1134
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 1133
    add-int/lit8 v40, v40, 0x1

    goto :goto_b

    .line 1135
    :cond_14
    const/16 v83, 0x6

    sget-object v84, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->INTEGER_0:Ljava/lang/Integer;

    const/16 v85, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v83

    move-object/from16 v2, v84

    move/from16 v3, v85

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->addObjectAt(ILjava/lang/Object;I)I

    move-result v83

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    goto/16 :goto_2

    .line 1139
    .end local v40    # "j":I
    :sswitch_b
    const/16 v83, 0x0

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    .line 1140
    const/16 v40, 0x0

    .restart local v40    # "j":I
    :goto_c
    move/from16 v0, v40

    move/from16 v1, v60

    if-ge v0, v1, :cond_15

    .line 1141
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 1140
    add-int/lit8 v40, v40, 0x1

    goto :goto_c

    .line 1142
    :cond_15
    const/16 v83, 0x8

    sget-object v84, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->INTEGER_0:Ljava/lang/Integer;

    const/16 v85, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v83

    move-object/from16 v2, v84

    move/from16 v3, v85

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->addObjectAt(ILjava/lang/Object;I)I

    move-result v83

    move/from16 v0, v83

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->_objIndex:I

    goto/16 :goto_2

    .line 1146
    .end local v40    # "j":I
    :sswitch_c
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v31

    .line 1147
    .local v31, "gdiIndex":I
    const/high16 v83, -0x80000000

    and-int v83, v83, v31

    if-nez v83, :cond_6

    .line 1148
    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getObject(I)Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;

    move-result-object v32

    .line 1149
    .local v32, "gdiObj":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;
    move-object/from16 v0, v32

    iget-boolean v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->used:Z

    move/from16 v83, v0

    if-eqz v83, :cond_6

    .line 1150
    move-object/from16 v0, v32

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->type:I

    move/from16 v83, v0

    packed-switch v83, :pswitch_data_0

    goto/16 :goto_2

    .line 1152
    :pswitch_0
    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->obj:Ljava/lang/Object;

    move-object/from16 v83, v0

    move-object/from16 v0, v83

    instance-of v0, v0, Lorg/apache/poi/java/awt/Color;

    move/from16 v83, v0

    if-eqz v83, :cond_16

    .line 1153
    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->obj:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Lorg/apache/poi/java/awt/Color;

    .line 1154
    .local v18, "colorTest":Lorg/apache/poi/java/awt/Color;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->polyPaint:Landroid/graphics/Paint;

    move-object/from16 v83, v0

    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v84

    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v85

    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v86

    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v87

    invoke-virtual/range {v83 .. v87}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 1160
    .end local v18    # "colorTest":Lorg/apache/poi/java/awt/Color;
    :cond_16
    move/from16 v53, v31

    .line 1161
    goto/16 :goto_2

    .line 1163
    :pswitch_1
    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->obj:Ljava/lang/Object;

    move-object/from16 v83, v0

    move-object/from16 v0, v83

    instance-of v0, v0, Lorg/apache/poi/java/awt/Color;

    move/from16 v83, v0

    if-eqz v83, :cond_17

    .line 1164
    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->obj:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Lorg/apache/poi/java/awt/Color;

    .line 1165
    .restart local v18    # "colorTest":Lorg/apache/poi/java/awt/Color;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->polyPaint:Landroid/graphics/Paint;

    move-object/from16 v83, v0

    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v84

    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v85

    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v86

    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v87

    invoke-virtual/range {v83 .. v87}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 1170
    .end local v18    # "colorTest":Lorg/apache/poi/java/awt/Color;
    :cond_17
    move/from16 v13, v31

    .line 1171
    goto/16 :goto_2

    .line 1173
    :pswitch_2
    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->obj:Ljava/lang/Object;

    move-object/from16 v83, v0

    check-cast v83, Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFFont;

    move-object/from16 v0, v83

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->wf:Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFFont;

    .line 1174
    move/from16 v29, v31

    .line 1176
    goto/16 :goto_2

    .line 1178
    :pswitch_3
    const/16 v53, -0x1

    .line 1179
    goto/16 :goto_2

    .line 1181
    :pswitch_4
    const/4 v13, -0x1

    goto/16 :goto_2

    .line 1187
    .end local v31    # "gdiIndex":I
    .end local v32    # "gdiObj":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;
    :sswitch_d
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v31

    .line 1188
    .restart local v31    # "gdiIndex":I
    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getObject(I)Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;

    move-result-object v32

    .line 1189
    .restart local v32    # "gdiObj":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;
    move/from16 v0, v31

    if-ne v0, v13, :cond_19

    .line 1190
    const/4 v13, -0x1

    .line 1195
    :cond_18
    :goto_d
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->clear()V

    goto/16 :goto_2

    .line 1191
    :cond_19
    move/from16 v0, v31

    move/from16 v1, v53

    if-ne v0, v1, :cond_1a

    .line 1192
    const/16 v53, -0x1

    goto :goto_d

    .line 1193
    :cond_1a
    move/from16 v0, v31

    move/from16 v1, v29

    if-ne v0, v1, :cond_18

    .line 1194
    const/16 v29, -0x1

    goto :goto_d

    .line 1198
    .end local v31    # "gdiIndex":I
    .end local v32    # "gdiObj":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;
    :sswitch_e
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v80

    .line 1199
    .restart local v80    # "y":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v84, v0

    mul-float v83, v83, v84

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v77, v0

    .line 1204
    .restart local v77    # "x":I
    goto/16 :goto_2

    .line 1209
    .end local v77    # "x":I
    .end local v80    # "y":I
    :sswitch_f
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v20

    .line 1210
    .local v20, "count":I
    move/from16 v0, v20

    new-array v0, v0, [I

    move-object/from16 v58, v0

    .line 1211
    .local v58, "pts":[I
    const/16 v57, 0x0

    .line 1212
    .local v57, "ptCount":I
    const/16 v39, 0x0

    :goto_e
    move/from16 v0, v39

    move/from16 v1, v20

    if-ge v0, v1, :cond_1b

    .line 1213
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    aput v83, v58, v39

    .line 1214
    aget v83, v58, v39

    add-int v57, v57, v83

    .line 1212
    add-int/lit8 v39, v39, 0x1

    goto :goto_e

    .line 1217
    :cond_1b
    add-int/lit8 v51, v20, 0x1

    .line 1218
    .local v51, "offset":I
    const/16 v39, 0x0

    :goto_f
    move/from16 v0, v39

    move/from16 v1, v20

    if-ge v0, v1, :cond_6

    .line 1219
    const/16 v40, 0x0

    .restart local v40    # "j":I
    :goto_10
    aget v83, v58, v39

    move/from16 v0, v40

    move/from16 v1, v83

    if-ge v0, v1, :cond_1e

    .line 1220
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v84, v0

    mul-float v83, v83, v84

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v77, v0

    .line 1221
    .restart local v77    # "x":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v80, v0

    .line 1222
    .restart local v80    # "y":I
    if-gez v13, :cond_1c

    if-ltz v53, :cond_1d

    .line 1223
    :cond_1c
    move-object/from16 v0, p0

    move/from16 v1, v77

    move/from16 v2, v80

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->resizeBounds(II)V

    .line 1219
    :cond_1d
    add-int/lit8 v40, v40, 0x1

    goto :goto_10

    .line 1218
    .end local v77    # "x":I
    .end local v80    # "y":I
    :cond_1e
    add-int/lit8 v39, v39, 0x1

    goto :goto_f

    .line 1230
    .end local v20    # "count":I
    .end local v40    # "j":I
    .end local v51    # "offset":I
    .end local v57    # "ptCount":I
    .end local v58    # "pts":[I
    :sswitch_10
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v20

    .line 1231
    .restart local v20    # "count":I
    add-int/lit8 v83, v20, 0x1

    move/from16 v0, v83

    new-array v6, v0, [F

    .line 1232
    .local v6, "_xpts":[F
    add-int/lit8 v83, v20, 0x1

    move/from16 v0, v83

    new-array v7, v0, [F

    .line 1233
    .local v7, "_ypts":[F
    const/16 v39, 0x0

    :goto_11
    move/from16 v0, v39

    move/from16 v1, v20

    if-ge v0, v1, :cond_21

    .line 1234
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v84, v0

    mul-float v83, v83, v84

    aput v83, v6, v39

    .line 1235
    aget v83, v6, v39

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v83, v0

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    aput v83, v6, v39

    .line 1236
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->left:I

    move/from16 v83, v0

    if-gez v83, :cond_1f

    .line 1237
    aget v83, v6, v39

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->left:I

    move/from16 v84, v0

    mul-int/lit8 v84, v84, -0x1

    move/from16 v0, v84

    int-to-float v0, v0

    move/from16 v84, v0

    add-float v83, v83, v84

    aput v83, v6, v39

    .line 1239
    :cond_1f
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    aput v83, v7, v39

    .line 1240
    aget v83, v7, v39

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v83, v0

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    aput v83, v7, v39

    .line 1241
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->top:I

    move/from16 v83, v0

    if-gez v83, :cond_20

    .line 1242
    aget v83, v7, v39

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->top:I

    move/from16 v84, v0

    mul-int/lit8 v84, v84, -0x1

    move/from16 v0, v84

    int-to-float v0, v0

    move/from16 v84, v0

    add-float v83, v83, v84

    aput v83, v7, v39

    .line 1233
    :cond_20
    add-int/lit8 v39, v39, 0x1

    goto/16 :goto_11

    .line 1245
    :cond_21
    const/16 v83, 0x0

    aget v83, v6, v83

    aput v83, v6, v20

    .line 1246
    const/16 v83, 0x0

    aget v83, v7, v83

    aput v83, v7, v20

    .line 1248
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->polyPaint:Landroid/graphics/Paint;

    move-object/from16 v83, v0

    sget-object v84, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual/range {v83 .. v84}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1251
    new-instance v56, Landroid/graphics/Path;

    invoke-direct/range {v56 .. v56}, Landroid/graphics/Path;-><init>()V

    .line 1252
    .local v56, "polyPath":Landroid/graphics/Path;
    const/16 v83, 0x0

    aget v83, v6, v83

    const/16 v84, 0x0

    aget v84, v7, v84

    move-object/from16 v0, v56

    move/from16 v1, v83

    move/from16 v2, v84

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1253
    const/16 v39, 0x0

    :goto_12
    move/from16 v0, v39

    move/from16 v1, v20

    if-ge v0, v1, :cond_22

    .line 1254
    aget v83, v6, v39

    aget v84, v7, v39

    move-object/from16 v0, v56

    move/from16 v1, v83

    move/from16 v2, v84

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1253
    add-int/lit8 v39, v39, 0x1

    goto :goto_12

    .line 1256
    :cond_22
    const/16 v83, 0x0

    aget v83, v6, v83

    const/16 v84, 0x0

    aget v84, v7, v84

    move-object/from16 v0, v56

    move/from16 v1, v83

    move/from16 v2, v84

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1259
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v83, v0

    move-object/from16 v0, v83

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v83, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->polyPaint:Landroid/graphics/Paint;

    move-object/from16 v84, v0

    move-object/from16 v0, v83

    move-object/from16 v1, v56

    move-object/from16 v2, v84

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 1263
    .end local v6    # "_xpts":[F
    .end local v7    # "_ypts":[F
    .end local v20    # "count":I
    .end local v56    # "polyPath":Landroid/graphics/Path;
    :sswitch_11
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v20

    .line 1264
    .restart local v20    # "count":I
    move/from16 v0, v20

    new-array v6, v0, [F

    .line 1265
    .restart local v6    # "_xpts":[F
    move/from16 v0, v20

    new-array v7, v0, [F

    .line 1266
    .restart local v7    # "_ypts":[F
    const/16 v39, 0x0

    :goto_13
    move/from16 v0, v39

    move/from16 v1, v20

    if-ge v0, v1, :cond_23

    .line 1267
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v84, v0

    mul-float v83, v83, v84

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v83, v0

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    aput v83, v6, v39

    .line 1268
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v83, v0

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    aput v83, v7, v39

    .line 1266
    add-int/lit8 v39, v39, 0x1

    goto :goto_13

    .line 1270
    :cond_23
    add-int/lit8 v83, v20, -0x1

    const/16 v84, 0x0

    aget v84, v6, v84

    aput v84, v6, v83

    .line 1271
    add-int/lit8 v83, v20, -0x1

    const/16 v84, 0x0

    aget v84, v7, v84

    aput v84, v7, v83

    .line 1273
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->polyPaint:Landroid/graphics/Paint;

    move-object/from16 v83, v0

    sget-object v84, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual/range {v83 .. v84}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1276
    new-instance v55, Landroid/graphics/Path;

    invoke-direct/range {v55 .. v55}, Landroid/graphics/Path;-><init>()V

    .line 1277
    .local v55, "polyLine":Landroid/graphics/Path;
    const/16 v83, 0x0

    aget v83, v6, v83

    const/16 v84, 0x0

    aget v84, v7, v84

    move-object/from16 v0, v55

    move/from16 v1, v83

    move/from16 v2, v84

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1278
    const/16 v39, 0x0

    :goto_14
    move/from16 v0, v39

    move/from16 v1, v20

    if-ge v0, v1, :cond_24

    .line 1279
    aget v83, v6, v39

    aget v84, v7, v39

    move-object/from16 v0, v55

    move/from16 v1, v83

    move/from16 v2, v84

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1278
    add-int/lit8 v39, v39, 0x1

    goto :goto_14

    .line 1281
    :cond_24
    const/16 v83, 0x0

    aget v83, v6, v83

    const/16 v84, 0x0

    aget v84, v7, v84

    move-object/from16 v0, v55

    move/from16 v1, v83

    move/from16 v2, v84

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1284
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v83, v0

    move-object/from16 v0, v83

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v83, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->polyPaint:Landroid/graphics/Paint;

    move-object/from16 v84, v0

    move-object/from16 v0, v83

    move-object/from16 v1, v55

    move-object/from16 v2, v84

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 1290
    .end local v6    # "_xpts":[F
    .end local v7    # "_ypts":[F
    .end local v20    # "count":I
    .end local v55    # "polyLine":Landroid/graphics/Path;
    :sswitch_12
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v12, v0

    .line 1291
    .local v12, "bot":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v84, v0

    mul-float v83, v83, v84

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v62, v0

    .line 1292
    .local v62, "right":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v70, v0

    .line 1293
    .local v70, "top":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v84, v0

    mul-float v83, v83, v84

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v41, v0

    .line 1294
    .local v41, "left":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v83, v0

    move-object/from16 v0, v83

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v83, v0

    new-instance v84, Landroid/graphics/Rect;

    sub-int v85, v62, v41

    sub-int v86, v12, v70

    move-object/from16 v0, v84

    move/from16 v1, v41

    move/from16 v2, v70

    move/from16 v3, v85

    move/from16 v4, v86

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->polyPaint:Landroid/graphics/Paint;

    move-object/from16 v85, v0

    invoke-virtual/range {v83 .. v85}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 1299
    .end local v12    # "bot":I
    .end local v41    # "left":I
    .end local v62    # "right":I
    .end local v70    # "top":I
    :sswitch_13
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 1300
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 1301
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v12, v0

    .line 1302
    .restart local v12    # "bot":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v84, v0

    mul-float v83, v83, v84

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v62, v0

    .line 1303
    .restart local v62    # "right":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v70, v0

    .line 1304
    .restart local v70    # "top":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v84, v0

    mul-float v83, v83, v84

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v41, v0

    .line 1305
    .restart local v41    # "left":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v83, v0

    move-object/from16 v0, v83

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v83, v0

    new-instance v84, Landroid/graphics/RectF;

    move/from16 v0, v41

    int-to-float v0, v0

    move/from16 v85, v0

    move/from16 v0, v70

    int-to-float v0, v0

    move/from16 v86, v0

    sub-int v87, v62, v41

    move/from16 v0, v87

    int-to-float v0, v0

    move/from16 v87, v0

    sub-int v88, v12, v70

    move/from16 v0, v88

    int-to-float v0, v0

    move/from16 v88, v0

    invoke-direct/range {v84 .. v88}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v85, 0x3f000000    # 0.5f

    const/high16 v86, 0x3f000000    # 0.5f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->polyPaint:Landroid/graphics/Paint;

    move-object/from16 v87, v0

    invoke-virtual/range {v83 .. v87}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 1312
    .end local v12    # "bot":I
    .end local v41    # "left":I
    .end local v62    # "right":I
    .end local v70    # "top":I
    :sswitch_14
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 1313
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 1314
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 1315
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 1316
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v12, v0

    .line 1317
    .restart local v12    # "bot":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v84, v0

    mul-float v83, v83, v84

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v62, v0

    .line 1318
    .restart local v62    # "right":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v70, v0

    .line 1319
    .restart local v70    # "top":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v84, v0

    mul-float v83, v83, v84

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v41, v0

    .line 1320
    .restart local v41    # "left":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v83, v0

    move-object/from16 v0, v83

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v83, v0

    new-instance v84, Landroid/graphics/Rect;

    sub-int v85, v62, v41

    sub-int v86, v12, v70

    move-object/from16 v0, v84

    move/from16 v1, v41

    move/from16 v2, v70

    move/from16 v3, v85

    move/from16 v4, v86

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->polyPaint:Landroid/graphics/Paint;

    move-object/from16 v85, v0

    invoke-virtual/range {v83 .. v85}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 1325
    .end local v12    # "bot":I
    .end local v41    # "left":I
    .end local v62    # "right":I
    .end local v70    # "top":I
    :sswitch_15
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readInt([B)I

    .line 1326
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v34, v0

    .line 1327
    .local v34, "height":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v84, v0

    mul-float v83, v83, v84

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v72, v0

    .line 1328
    .local v72, "width":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v84, v0

    mul-float v83, v83, v84

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v41, v0

    .line 1329
    .restart local v41    # "left":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v70, v0

    .line 1330
    .restart local v70    # "top":I
    if-ltz v53, :cond_25

    .line 1331
    move-object/from16 v0, p0

    move/from16 v1, v41

    move/from16 v2, v70

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->resizeBounds(II)V

    .line 1332
    :cond_25
    if-ltz v53, :cond_6

    .line 1333
    add-int v83, v41, v72

    add-int v84, v70, v34

    move-object/from16 v0, p0

    move/from16 v1, v83

    move/from16 v2, v84

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->resizeBounds(II)V

    goto/16 :goto_2

    .line 1337
    .end local v34    # "height":I
    .end local v41    # "left":I
    .end local v70    # "top":I
    .end local v72    # "width":I
    :sswitch_16
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readInt([B)I

    move-result v83

    move/from16 v0, v83

    and-int/lit16 v0, v0, 0xff

    move/from16 v49, v0

    .line 1338
    .local v49, "mode":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->ySign:I

    move/from16 v84, v0

    mul-int v38, v83, v84

    .line 1339
    .local v38, "heightSrc":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->xSign:I

    move/from16 v84, v0

    mul-int v76, v83, v84

    .line 1340
    .local v76, "widthSrc":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->ySign:I

    move/from16 v84, v0

    mul-int v67, v83, v84

    .line 1341
    .local v67, "sy":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->xSign:I

    move/from16 v84, v0

    mul-int v65, v83, v84

    .line 1342
    .local v65, "sx":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->ySign:I

    move/from16 v84, v0

    mul-int v36, v83, v84

    .line 1343
    .local v36, "heightDst":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->xSign:I

    move/from16 v84, v0

    mul-int v83, v83, v84

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v84, v0

    mul-float v83, v83, v84

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v74, v0

    .line 1344
    .local v74, "widthDst":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->ySign:I

    move/from16 v84, v0

    mul-int v24, v83, v84

    .line 1345
    .local v24, "dy":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->xSign:I

    move/from16 v84, v0

    mul-int v83, v83, v84

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v84, v0

    mul-float v83, v83, v84

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v22, v0

    .line 1347
    .local v22, "dx":I
    mul-int/lit8 v83, v60, 0x2

    add-int/lit8 v44, v83, -0x14

    .line 1348
    .local v44, "lenbitmap":I
    move/from16 v0, v44

    new-array v9, v0, [B

    .line 1349
    .local v9, "bitmap":[B
    const/16 v39, 0x0

    :goto_15
    move/from16 v0, v39

    move/from16 v1, v44

    if-ge v0, v1, :cond_26

    .line 1350
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readByte([B)B

    move-result v83

    aput-byte v83, v9, v39

    .line 1349
    add-int/lit8 v39, v39, 0x1

    goto :goto_15

    .line 1352
    :cond_26
    move/from16 v0, v38

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v35, v0

    .line 1353
    .local v35, "heightA":I
    move/from16 v0, v76

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v73, v0

    .line 1354
    .local v73, "widthA":I
    move/from16 v0, v67

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v68, v0

    .line 1355
    .local v68, "syA":I
    move/from16 v0, v65

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v83

    move/from16 v0, v83

    float-to-int v0, v0

    move/from16 v66, v0

    .line 1356
    .local v66, "sxA":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->vpY:I

    move/from16 v84, v0

    add-int v84, v84, v24

    move/from16 v0, v84

    int-to-float v0, v0

    move/from16 v84, v0

    mul-float v83, v83, v84

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v25

    .line 1357
    .local v25, "dyA":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->vpX:I

    move/from16 v84, v0

    add-int v84, v84, v22

    move/from16 v0, v84

    int-to-float v0, v0

    move/from16 v84, v0

    mul-float v83, v83, v84

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v23

    .line 1358
    .local v23, "dxA":F
    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v37

    .line 1359
    .local v37, "heightDstA":F
    move/from16 v0, v74

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v75

    .line 1360
    .local v75, "widthDstA":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v83, v0

    mul-float v75, v75, v83

    .line 1361
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v83, v0

    mul-float v37, v37, v83

    .line 1363
    const/4 v11, 0x0

    .line 1365
    .local v11, "bmp":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getImage([B)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 1367
    if-eqz v11, :cond_6

    .line 1369
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawingCanvas:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;

    move-object/from16 v83, v0

    move-object/from16 v0, v83

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/DrawonCanvas;->mycanvas:Landroid/graphics/Canvas;

    move-object/from16 v83, v0

    new-instance v84, Landroid/graphics/Rect;

    add-int v85, v66, v73

    add-int v86, v68, v35

    move-object/from16 v0, v84

    move/from16 v1, v66

    move/from16 v2, v68

    move/from16 v3, v85

    move/from16 v4, v86

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v85, Landroid/graphics/Rect;

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v86, v0

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v87, v0

    add-float v88, v23, v75

    move/from16 v0, v88

    float-to-int v0, v0

    move/from16 v88, v0

    add-float v89, v25, v37

    move/from16 v0, v89

    float-to-int v0, v0

    move/from16 v89, v0

    invoke-direct/range {v85 .. v89}, Landroid/graphics/Rect;-><init>(IIII)V

    const/16 v86, 0x0

    move-object/from16 v0, v83

    move-object/from16 v1, v84

    move-object/from16 v2, v85

    move-object/from16 v3, v86

    invoke-virtual {v0, v11, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 1377
    .end local v9    # "bitmap":[B
    .end local v11    # "bmp":Landroid/graphics/Bitmap;
    .end local v22    # "dx":I
    .end local v23    # "dxA":F
    .end local v24    # "dy":I
    .end local v25    # "dyA":F
    .end local v35    # "heightA":I
    .end local v36    # "heightDst":I
    .end local v37    # "heightDstA":F
    .end local v38    # "heightSrc":I
    .end local v44    # "lenbitmap":I
    .end local v49    # "mode":I
    .end local v65    # "sx":I
    .end local v66    # "sxA":I
    .end local v67    # "sy":I
    .end local v68    # "syA":I
    .end local v73    # "widthA":I
    .end local v74    # "widthDst":I
    .end local v75    # "widthDstA":F
    .end local v76    # "widthSrc":I
    :sswitch_17
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readInt([B)I

    .line 1378
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 1379
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 1380
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 1381
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 1382
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 1383
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedHeight(F)F

    move-result v36

    .line 1384
    .local v36, "heightDst":F
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    move-result v83

    move/from16 v0, v83

    int-to-float v0, v0

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->scaleXY:F

    move/from16 v84, v0

    mul-float v83, v83, v84

    move-object/from16 v0, p0

    move/from16 v1, v83

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->getResizedWidth(F)F

    move-result v74

    .line 1386
    .local v74, "widthDst":F
    goto/16 :goto_2

    .line 1388
    .end local v36    # "heightDst":F
    .end local v74    # "widthDst":F
    :sswitch_18
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readInt([B)I

    .line 1389
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 1390
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    .line 1391
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->readShort([B)S

    goto/16 :goto_2

    .line 970
    nop

    :sswitch_data_0
    .sparse-switch
        0xf7 -> :sswitch_b
        0x103 -> :sswitch_1
        0x12d -> :sswitch_c
        0x12e -> :sswitch_6
        0x1f0 -> :sswitch_d
        0x20b -> :sswitch_2
        0x20c -> :sswitch_3
        0x213 -> :sswitch_e
        0x214 -> :sswitch_0
        0x2fa -> :sswitch_4
        0x2fb -> :sswitch_9
        0x2fc -> :sswitch_5
        0x324 -> :sswitch_10
        0x325 -> :sswitch_11
        0x416 -> :sswitch_12
        0x418 -> :sswitch_12
        0x41b -> :sswitch_12
        0x521 -> :sswitch_8
        0x538 -> :sswitch_f
        0x61c -> :sswitch_13
        0x61d -> :sswitch_15
        0x62f -> :sswitch_8
        0x6ff -> :sswitch_a
        0x817 -> :sswitch_14
        0x81a -> :sswitch_14
        0x830 -> :sswitch_14
        0x940 -> :sswitch_18
        0xa32 -> :sswitch_7
        0xb41 -> :sswitch_16
        0xf43 -> :sswitch_17
    .end sparse-switch

    .line 1150
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setActHeight(F)V
    .locals 0
    .param p1, "actHeight"    # F

    .prologue
    .line 148
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->actImgHigt:F

    .line 149
    return-void
.end method

.method public setActWidth(F)V
    .locals 0
    .param p1, "actWidth"    # F

    .prologue
    .line 144
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->actImgWid:F

    .line 145
    return-void
.end method
