.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PictureHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "PictureHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IPrstGeomHandler;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IXfrmHandler;


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 22
    const-string/jumbo v5, "pic"

    invoke-direct {p0, v6, v5}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 24
    const/4 v5, 0x2

    new-array v2, v5, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 26
    .local v2, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler;

    invoke-direct {v0, v6, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;)V

    .line 28
    .local v0, "blipFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PictureSeqDescriptor;

    const-string/jumbo v5, "blipFill"

    invoke-direct {v1, v5, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PictureSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 30
    .local v1, "blipPicSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PictureSeqDescriptor;
    const/4 v5, 0x0

    aput-object v1, v2, v5

    .line 32
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler;

    invoke-direct {v3, v6, p0, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IXfrmHandler;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler$IPrstGeomHandler;)V

    .line 34
    .local v3, "spprHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PictureSeqDescriptor;

    const-string/jumbo v5, "spPr"

    invoke-direct {v4, v5, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PictureSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 36
    .local v4, "spprPicSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PictureSeqDescriptor;
    const/4 v5, 0x1

    aput-object v4, v2, v5

    .line 38
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PictureHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 39
    return-void
.end method


# virtual methods
.method public setBlipEmbed(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "blipEmbed"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;

    invoke-interface {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;->setImageRelId(Ljava/lang/String;)V

    .line 44
    return-void
.end method

.method public setRotation(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;I)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "rotation"    # I

    .prologue
    .line 53
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;

    invoke-interface {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;->setRotation(I)V

    .line 54
    return-void
.end method

.method public setShapeName(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "shapeName"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;

    invoke-interface {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;->setDrSpeNme(Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method public setTile(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "flip"    # Ljava/lang/String;

    .prologue
    .line 49
    return-void
.end method
