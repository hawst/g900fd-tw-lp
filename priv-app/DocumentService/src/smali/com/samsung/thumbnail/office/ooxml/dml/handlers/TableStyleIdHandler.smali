.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableStyleIdHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedTagNoChildrenHandler;
.source "TableStyleIdHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableStyleIdHandler$IStyleIdConsumer;
    }
.end annotation


# instance fields
.field private consumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableStyleIdHandler$IStyleIdConsumer;

.field private styleId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableStyleIdHandler$IStyleIdConsumer;)V
    .locals 2
    .param p1, "consumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableStyleIdHandler$IStyleIdConsumer;

    .prologue
    .line 16
    const/16 v0, 0x1f

    const-string/jumbo v1, "tableStyleId"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedTagNoChildrenHandler;-><init>(ILjava/lang/String;)V

    .line 17
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableStyleIdHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableStyleIdHandler$IStyleIdConsumer;

    .line 18
    return-void
.end method


# virtual methods
.method public charactersHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;[CII)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "content"    # [C
    .param p3, "start"    # I
    .param p4, "length"    # I

    .prologue
    .line 28
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2, p3, p4}, Ljava/lang/String;-><init>([CII)V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableStyleIdHandler;->styleId:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedTagNoChildrenHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableStyleIdHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableStyleIdHandler$IStyleIdConsumer;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableStyleIdHandler;->styleId:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableStyleIdHandler$IStyleIdConsumer;->setStyleId(Ljava/lang/String;)V

    .line 35
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 24
    return-void
.end method
