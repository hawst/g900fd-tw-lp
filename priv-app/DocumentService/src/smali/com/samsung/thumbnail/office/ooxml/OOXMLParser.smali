.class public abstract Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "OOXMLParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/OOXMLParser$UnknownElementHandler;,
        Lcom/samsung/thumbnail/office/ooxml/OOXMLParser$OOXMLInputStream;
    }
.end annotation


# static fields
.field private static final FORCE_STOP_EXCEPTION_MESSAGE:Ljava/lang/String; = "Stop parsing after processing the first page."

.field private static final TAG:Ljava/lang/String; = "OOXMLParser"


# instance fields
.field protected consumer:Ljava/lang/Object;

.field private handlerStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;",
            ">;"
        }
    .end annotation
.end field

.field private lastNameSpace:Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

.field private mSectProp:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

.field private nameSpaceBaseIndex:I

.field private nameSpaceStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;",
            ">;"
        }
    .end annotation
.end field

.field private preDefinedNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

.field private saxParser:Ljavax/xml/parsers/SAXParser;

.field private xmlReader:Lorg/xml/sax/XMLReader;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 68
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 3
    .param p1, "consumer"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    .line 55
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 56
    iput v2, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->nameSpaceBaseIndex:I

    .line 57
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    .line 58
    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->handlerStack:Ljava/util/Stack;

    .line 59
    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->nameSpaceStack:Ljava/util/Stack;

    .line 61
    new-array v1, v2, [Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->preDefinedNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    .line 62
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>()V

    .line 63
    .local v0, "ns":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->preDefinedNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    .line 64
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Lorg/xml/sax/XMLReader;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->xmlReader:Lorg/xml/sax/XMLReader;

    return-object v0
.end method

.method private updateDefaultNameSpace(Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;)Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;
    .locals 16
    .param p1, "nSStack"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;

    .prologue
    .line 462
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-direct {v4}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>()V

    .line 463
    .local v4, "nameSpace20":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const/16 v15, 0x14

    invoke-virtual {v4, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setID(I)V

    .line 464
    const-string/jumbo v15, "http://schemas.openxmlformats.org/wordprocessingml/2006/main"

    invoke-virtual {v4, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setNameSpaceUri(Ljava/lang/String;)V

    .line 466
    const-string/jumbo v15, "w"

    invoke-virtual {v4, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setPrefix(Ljava/lang/String;)V

    .line 467
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->addNameSpace(Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 469
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-direct {v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>()V

    .line 470
    .local v3, "nameSpace2":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const/4 v15, 0x2

    invoke-virtual {v3, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setID(I)V

    .line 471
    const-string/jumbo v15, "http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"

    invoke-virtual {v3, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setNameSpaceUri(Ljava/lang/String;)V

    .line 473
    const-string/jumbo v15, "wpc"

    invoke-virtual {v3, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setPrefix(Ljava/lang/String;)V

    .line 474
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->addNameSpace(Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 476
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-direct {v5}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>()V

    .line 477
    .local v5, "nameSpace3":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const/4 v15, 0x3

    invoke-virtual {v5, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setID(I)V

    .line 478
    const-string/jumbo v15, "http://schemas.openxmlformats.org/markup-compatibility/2006"

    invoke-virtual {v5, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setNameSpaceUri(Ljava/lang/String;)V

    .line 480
    const-string/jumbo v15, "mc"

    invoke-virtual {v5, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setPrefix(Ljava/lang/String;)V

    .line 481
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->addNameSpace(Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 483
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-direct {v10}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>()V

    .line 484
    .local v10, "nameSpace50":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const/16 v15, 0x32

    invoke-virtual {v10, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setID(I)V

    .line 485
    const-string/jumbo v15, "urn:schemas-microsoft-com:office:office"

    invoke-virtual {v10, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setNameSpaceUri(Ljava/lang/String;)V

    .line 486
    const-string/jumbo v15, "o"

    invoke-virtual {v10, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setPrefix(Ljava/lang/String;)V

    .line 487
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->addNameSpace(Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 489
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>()V

    .line 490
    .local v1, "nameSpace10":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const/16 v15, 0xa

    invoke-virtual {v1, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setID(I)V

    .line 491
    const-string/jumbo v15, "http://schemas.openxmlformats.org/officeDocument/2006/relationships"

    invoke-virtual {v1, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setNameSpaceUri(Ljava/lang/String;)V

    .line 493
    const-string/jumbo v15, "r"

    invoke-virtual {v1, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setPrefix(Ljava/lang/String;)V

    .line 494
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->addNameSpace(Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 496
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-direct {v7}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>()V

    .line 497
    .local v7, "nameSpace4":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const/4 v15, 0x4

    invoke-virtual {v7, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setID(I)V

    .line 498
    const-string/jumbo v15, "http://schemas.openxmlformats.org/officeDocument/2006/math"

    invoke-virtual {v7, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setNameSpaceUri(Ljava/lang/String;)V

    .line 500
    const-string/jumbo v15, "m"

    invoke-virtual {v7, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setPrefix(Ljava/lang/String;)V

    .line 501
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->addNameSpace(Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 503
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-direct {v8}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>()V

    .line 504
    .local v8, "nameSpace40":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const/16 v15, 0x28

    invoke-virtual {v8, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setID(I)V

    .line 505
    const-string/jumbo v15, "urn:schemas-microsoft-com:vml"

    invoke-virtual {v8, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setNameSpaceUri(Ljava/lang/String;)V

    .line 506
    const-string/jumbo v15, "v"

    invoke-virtual {v8, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setPrefix(Ljava/lang/String;)V

    .line 507
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->addNameSpace(Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 509
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-direct {v9}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>()V

    .line 510
    .local v9, "nameSpace5":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const/4 v15, 0x5

    invoke-virtual {v9, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setID(I)V

    .line 511
    const-string/jumbo v15, "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"

    invoke-virtual {v9, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setNameSpaceUri(Ljava/lang/String;)V

    .line 513
    const-string/jumbo v15, "wp14"

    invoke-virtual {v9, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setPrefix(Ljava/lang/String;)V

    .line 514
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->addNameSpace(Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 516
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-direct {v6}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>()V

    .line 517
    .local v6, "nameSpace30":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const/16 v15, 0x1e

    invoke-virtual {v6, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setID(I)V

    .line 518
    const-string/jumbo v15, "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"

    invoke-virtual {v6, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setNameSpaceUri(Ljava/lang/String;)V

    .line 520
    const-string/jumbo v15, "wp"

    invoke-virtual {v6, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setPrefix(Ljava/lang/String;)V

    .line 521
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->addNameSpace(Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 523
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-direct {v11}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>()V

    .line 524
    .local v11, "nameSpace6":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const/4 v15, 0x6

    invoke-virtual {v11, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setID(I)V

    .line 525
    const-string/jumbo v15, "urn:schemas-microsoft-com:office:word"

    invoke-virtual {v11, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setNameSpaceUri(Ljava/lang/String;)V

    .line 526
    const-string/jumbo v15, "w10"

    invoke-virtual {v11, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setPrefix(Ljava/lang/String;)V

    .line 527
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->addNameSpace(Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 529
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-direct {v12}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>()V

    .line 530
    .local v12, "nameSpace7":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const/4 v15, 0x7

    invoke-virtual {v12, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setID(I)V

    .line 531
    const-string/jumbo v15, "http://schemas.microsoft.com/office/word/2010/wordml"

    invoke-virtual {v12, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setNameSpaceUri(Ljava/lang/String;)V

    .line 533
    const-string/jumbo v15, "w14"

    invoke-virtual {v12, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setPrefix(Ljava/lang/String;)V

    .line 534
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->addNameSpace(Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 536
    new-instance v13, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-direct {v13}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>()V

    .line 537
    .local v13, "nameSpace8":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const/16 v15, 0x8

    invoke-virtual {v13, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setID(I)V

    .line 538
    const-string/jumbo v15, "http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"

    invoke-virtual {v13, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setNameSpaceUri(Ljava/lang/String;)V

    .line 540
    const-string/jumbo v15, "wpg"

    invoke-virtual {v13, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setPrefix(Ljava/lang/String;)V

    .line 541
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->addNameSpace(Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 543
    new-instance v14, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-direct {v14}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>()V

    .line 544
    .local v14, "nameSpace9":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const/16 v15, 0x9

    invoke-virtual {v14, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setID(I)V

    .line 545
    const-string/jumbo v15, "http://schemas.microsoft.com/office/word/2010/wordprocessingInk"

    invoke-virtual {v14, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setNameSpaceUri(Ljava/lang/String;)V

    .line 547
    const-string/jumbo v15, "wpi"

    invoke-virtual {v14, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setPrefix(Ljava/lang/String;)V

    .line 548
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->addNameSpace(Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 550
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>()V

    .line 551
    .local v2, "nameSpace11":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const/16 v15, 0xb

    invoke-virtual {v2, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setID(I)V

    .line 552
    const-string/jumbo v15, "http://schemas.microsoft.com/office/word/2010/wordprocessingShape"

    invoke-virtual {v2, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setNameSpaceUri(Ljava/lang/String;)V

    .line 554
    const-string/jumbo v15, "wps"

    invoke-virtual {v2, v15}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setPrefix(Ljava/lang/String;)V

    .line 555
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->addNameSpace(Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 557
    return-object p1
.end method


# virtual methods
.method public addNameSpace(Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 9
    .param p1, "qName"    # Ljava/lang/String;
    .param p2, "atts"    # Lorg/xml/sax/Attributes;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x5

    .line 276
    const/4 v2, 0x0

    .line 277
    .local v2, "nSStack":Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;
    invoke-interface {p2}, Lorg/xml/sax/Attributes;->getLength()I

    move-result v0

    .line 278
    .local v0, "attrLength":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_4

    .line 279
    invoke-interface {p2, v1}, Lorg/xml/sax/Attributes;->getQName(I)Ljava/lang/String;

    move-result-object v4

    .line 281
    .local v4, "qVal":Ljava/lang/String;
    const-string/jumbo v5, "w:background"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string/jumbo v5, "w:color"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 283
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {p2, v1}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->setBackgroundColor(Ljava/lang/String;)V

    .line 288
    :cond_0
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v7, :cond_3

    const-string/jumbo v5, "xmlns"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 290
    if-nez v2, :cond_1

    .line 291
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;

    .end local v2    # "nSStack":Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;
    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;-><init>()V

    .line 293
    .restart local v2    # "nSStack":Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;
    :cond_1
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-direct {v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>()V

    .line 294
    .local v3, "nameSpace":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    invoke-interface {p2, v1}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setNameSpaceUri(Ljava/lang/String;)V

    .line 295
    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x3a

    if-ne v5, v6, :cond_2

    .line 296
    const/4 v5, 0x6

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setPrefix(Ljava/lang/String;)V

    .line 299
    :cond_2
    invoke-virtual {p0, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->updateNSId(Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 300
    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->addNameSpace(Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 278
    .end local v3    # "nameSpace":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 304
    .end local v4    # "qVal":Ljava/lang/String;
    :cond_4
    if-eqz v2, :cond_5

    .line 305
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->nameSpaceStack:Ljava/util/Stack;

    invoke-virtual {v5, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    iput-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->lastNameSpace:Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    .line 317
    :goto_1
    return-void

    .line 309
    :cond_5
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->nameSpaceStack:Ljava/util/Stack;

    invoke-virtual {v5}, Ljava/util/Stack;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_6

    .line 310
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->nameSpaceStack:Ljava/util/Stack;

    invoke-virtual {v5}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->incrementLevel()V

    goto :goto_1

    .line 314
    :cond_6
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;

    .end local v2    # "nSStack":Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;
    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;-><init>()V

    .line 315
    .restart local v2    # "nSStack":Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->nameSpaceStack:Ljava/util/Stack;

    invoke-direct {p0, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->updateDefaultNameSpace(Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;)Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 316
    iput-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->lastNameSpace:Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    goto :goto_1
.end method

.method public characters([CII)V
    .locals 1
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 204
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->handlerStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    :goto_0
    return-void

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->handlerStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->charactersHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;[CII)V

    goto :goto_0
.end method

.method public abstract createRootElementHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
.end method

.method public endDocument()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 92
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "namespaceURI"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 130
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->handlerStack:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 195
    :cond_0
    return-void

    .line 138
    :cond_1
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->handlerStack:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    .line 139
    .local v2, "elementHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    invoke-virtual {v2, p3, p0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 155
    .local v0, "ele":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->getElementName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->getElementName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 159
    invoke-virtual {v2, p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 164
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->removeNameSpace()V

    .line 165
    const/4 v3, 0x0

    .line 166
    .local v3, "isOnlyFirstSlide":Z
    const/4 v1, 0x0

    .line 167
    .local v1, "elementCreator":Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    instance-of v4, v4, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    if-eqz v4, :cond_5

    .line 168
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v4}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v1

    .line 170
    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->isShowOnlyFirstSlide()Z

    move-result v3

    .line 178
    :cond_3
    :goto_0
    if-eqz v3, :cond_0

    .line 179
    const-string/jumbo v4, "w:p"

    invoke-virtual {p3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 180
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v4}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->endDocument()V

    .line 183
    :cond_4
    if-eqz v1, :cond_0

    .line 184
    iget-boolean v4, v1, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    if-eqz v4, :cond_0

    .line 185
    new-instance v4, Lorg/xml/sax/SAXException;

    const-string/jumbo v5, "Stop parsing after processing the first page."

    invoke-direct {v4, v5}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 172
    :cond_5
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    instance-of v4, v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    if-eqz v4, :cond_3

    .line 173
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v1

    .line 174
    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->isShowOnlyFirstSlide()Z

    move-result v3

    goto :goto_0
.end method

.method public getConsumer()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    return-object v0
.end method

.method public getDefaultNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .locals 3

    .prologue
    .line 396
    const/4 v0, 0x0

    .line 398
    .local v0, "nameSpace":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->nameSpaceStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->elements()Ljava/util/Enumeration;

    move-result-object v1

    .line 400
    .local v1, "stackEnum":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 401
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->getDefaultNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v0

    .line 402
    if-eqz v0, :cond_0

    .line 405
    :cond_1
    return-object v0
.end method

.method public abstract getMainNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
.end method

.method public getNameSpace(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .locals 8
    .param p1, "element"    # Ljava/lang/String;

    .prologue
    .line 357
    const-string/jumbo v5, ""

    .line 358
    .local v5, "prefix":Ljava/lang/String;
    const/4 v2, 0x0

    .line 360
    .local v2, "nameSpace":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const/16 v7, 0x3a

    invoke-virtual {p1, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 361
    .local v0, "i":I
    const/4 v7, -0x1

    if-eq v0, v7, :cond_0

    .line 362
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 365
    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->lastNameSpace:Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->lastNameSpace:Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->getPrefix()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 367
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->lastNameSpace:Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-object v3, v2

    .line 392
    .end local v2    # "nameSpace":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .local v3, "nameSpace":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    :goto_0
    return-object v3

    .line 372
    .end local v3    # "nameSpace":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .restart local v2    # "nameSpace":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    :cond_0
    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->nameSpaceStack:Ljava/util/Stack;

    invoke-virtual {v7}, Ljava/util/Stack;->elements()Ljava/util/Enumeration;

    move-result-object v6

    .line 374
    .local v6, "stackEnum":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;>;"
    :cond_1
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 375
    const/4 v1, 0x0

    .line 376
    .local v1, "localNS":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;

    iget-object v7, v7, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->mNameSpace:Ljava/util/Vector;

    invoke-virtual {v7}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v4

    .line 379
    .local v4, "nsEnum":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;>;"
    :cond_2
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 380
    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "localNS":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    .line 382
    .restart local v1    # "localNS":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->getPrefix()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 383
    move-object v2, v1

    .line 388
    :cond_3
    if-eqz v2, :cond_1

    .end local v1    # "localNS":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .end local v4    # "nsEnum":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;>;"
    :cond_4
    move-object v3, v2

    .line 392
    .end local v2    # "nameSpace":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .restart local v3    # "nameSpace":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    goto :goto_0
.end method

.method public getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 341
    const/4 v0, 0x0

    .line 343
    .local v0, "nameSpace":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->nameSpaceStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->elements()Ljava/util/Enumeration;

    move-result-object v1

    .line 345
    .local v1, "stackEnum":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 346
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;

    invoke-virtual {v2, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->getNameSpaceByID(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v0

    .line 348
    if-eqz v0, :cond_0

    .line 351
    :cond_1
    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->lastNameSpace:Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    .line 353
    return-object v0
.end method

.method public getSectPr()Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->mSectProp:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    return-object v0
.end method

.method public parse(Ljava/io/InputStream;)V
    .locals 7
    .param p1, "inputStream"    # Ljava/io/InputStream;

    .prologue
    const/4 v6, 0x0

    .line 214
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v2

    .line 215
    .local v2, "spf":Ljavax/xml/parsers/SAXParserFactory;
    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->saxParser:Ljavax/xml/parsers/SAXParser;

    .line 217
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->saxParser:Ljavax/xml/parsers/SAXParser;

    invoke-virtual {v3}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->xmlReader:Lorg/xml/sax/XMLReader;

    .line 218
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->xmlReader:Lorg/xml/sax/XMLReader;

    invoke-interface {v3, p0}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 219
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->xmlReader:Lorg/xml/sax/XMLReader;

    const-string/jumbo v4, "http://xml.org/sax/features/namespaces"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Lorg/xml/sax/XMLReader;->setFeature(Ljava/lang/String;Z)V

    .line 221
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->xmlReader:Lorg/xml/sax/XMLReader;

    const-string/jumbo v4, "http://xml.org/sax/features/namespace-prefixes"

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, Lorg/xml/sax/XMLReader;->setFeature(Ljava/lang/String;Z)V

    .line 223
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->xmlReader:Lorg/xml/sax/XMLReader;

    new-instance v4, Lorg/xml/sax/InputSource;

    invoke-direct {v4, p1}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v3, v4}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V
    :try_end_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 238
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    instance-of v3, v3, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    if-eqz v3, :cond_1

    .line 239
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v3}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v3

    iput-boolean v6, v3, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    .line 240
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v3}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setShowOnlyFirstSlide(Z)V

    .line 247
    .end local v2    # "spf":Ljavax/xml/parsers/SAXParserFactory;
    :cond_0
    :goto_0
    return-void

    .line 242
    .restart local v2    # "spf":Ljavax/xml/parsers/SAXParserFactory;
    :cond_1
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    instance-of v3, v3, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    if-eqz v3, :cond_0

    .line 243
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setShowOnlyFirstSlide(Z)V

    goto :goto_0

    .line 225
    .end local v2    # "spf":Ljavax/xml/parsers/SAXParserFactory;
    :catch_0
    move-exception v0

    .line 226
    .local v0, "e":Ljavax/xml/parsers/ParserConfigurationException;
    :try_start_1
    const-string/jumbo v3, "OOXMLParser"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "ParserConfigurationException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljavax/xml/parsers/ParserConfigurationException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 238
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    instance-of v3, v3, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    if-eqz v3, :cond_2

    .line 239
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v3}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v3

    iput-boolean v6, v3, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    .line 240
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v3}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setShowOnlyFirstSlide(Z)V

    goto :goto_0

    .line 242
    :cond_2
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    instance-of v3, v3, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    if-eqz v3, :cond_0

    .line 243
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setShowOnlyFirstSlide(Z)V

    goto :goto_0

    .line 227
    .end local v0    # "e":Ljavax/xml/parsers/ParserConfigurationException;
    :catch_1
    move-exception v0

    .line 228
    .local v0, "e":Lorg/xml/sax/SAXException;
    :try_start_2
    invoke-virtual {v0}, Lorg/xml/sax/SAXException;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 229
    .local v1, "msg":Ljava/lang/String;
    if-eqz v1, :cond_3

    const-string/jumbo v3, "Stop parsing after processing the first page."

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 230
    :cond_3
    const-string/jumbo v3, "OOXMLParser"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "SAXException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 238
    :cond_4
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    instance-of v3, v3, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    if-eqz v3, :cond_5

    .line 239
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v3}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v3

    iput-boolean v6, v3, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    .line 240
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v3}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setShowOnlyFirstSlide(Z)V

    goto/16 :goto_0

    .line 242
    :cond_5
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    instance-of v3, v3, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    if-eqz v3, :cond_0

    .line 243
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setShowOnlyFirstSlide(Z)V

    goto/16 :goto_0

    .line 232
    .end local v0    # "e":Lorg/xml/sax/SAXException;
    .end local v1    # "msg":Ljava/lang/String;
    :catch_2
    move-exception v0

    .line 233
    .local v0, "e":Ljava/io/IOException;
    :try_start_3
    const-string/jumbo v3, "OOXMLParser"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "IOException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 238
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    instance-of v3, v3, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    if-eqz v3, :cond_6

    .line 239
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v3}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v3

    iput-boolean v6, v3, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    .line 240
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v3}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setShowOnlyFirstSlide(Z)V

    goto/16 :goto_0

    .line 242
    :cond_6
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    instance-of v3, v3, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    if-eqz v3, :cond_0

    .line 243
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setShowOnlyFirstSlide(Z)V

    goto/16 :goto_0

    .line 234
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 235
    .local v0, "e":Ljava/lang/Exception;
    :try_start_4
    const-string/jumbo v3, "OOXMLParser"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v4, "Parser is not handled properly"

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 238
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    move-object v4, v3

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    instance-of v3, v3, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    if-eqz v3, :cond_8

    .line 239
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v3}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v3

    iput-boolean v6, v3, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    .line 240
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v3}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setShowOnlyFirstSlide(Z)V

    .line 243
    :cond_7
    :goto_1
    throw v4

    .line 242
    :cond_8
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    instance-of v3, v3, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    if-eqz v3, :cond_7

    .line 243
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->consumer:Ljava/lang/Object;

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setShowOnlyFirstSlide(Z)V

    goto :goto_1
.end method

.method public popHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    .locals 2

    .prologue
    .line 257
    const/4 v0, 0x0

    .line 259
    .local v0, "ele":Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->handlerStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 260
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->handlerStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "ele":Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    .line 263
    .restart local v0    # "ele":Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    :cond_0
    return-object v0
.end method

.method public pushHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V
    .locals 1
    .param p1, "elementHandler"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    .prologue
    .line 253
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->handlerStack:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    return-void
.end method

.method public removeNameSpace()V
    .locals 2

    .prologue
    .line 409
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->nameSpaceStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;

    .line 410
    .local v0, "nsStack":Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;
    iget v1, v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->mLevel:I

    if-nez v1, :cond_0

    .line 412
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->nameSpaceStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 416
    :goto_0
    return-void

    .line 415
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNSStack;->decrementLevel()V

    goto :goto_0
.end method

.method public setPredefinedNameSpaces([Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V
    .locals 0
    .param p1, "preDefinedNameSpaces"    # [Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    .prologue
    .line 271
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->preDefinedNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    .line 272
    return-void
.end method

.method public setSectPr(Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;)V
    .locals 0
    .param p1, "sectPr"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->mSectProp:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    .line 81
    return-void
.end method

.method public setSectPrParser()V
    .locals 0

    .prologue
    .line 76
    return-void
.end method

.method public startDocument()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 86
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "namespaceURI"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "atts"    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 102
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->handlerStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 104
    invoke-virtual {p0, p3, p4}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->addNameSpace(Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 105
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->createRootElementHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    move-result-object v0

    .line 106
    .local v0, "elementHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    invoke-virtual {v0, p3, p0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->checkElement(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 107
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->pushHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 108
    invoke-virtual {v0, p0, p3, p4}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 112
    .end local v0    # "elementHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    :cond_1
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->handlerStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    .line 113
    .restart local v0    # "elementHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    invoke-virtual {p0, p3, p4}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->addNameSpace(Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 117
    if-eqz v0, :cond_0

    .line 118
    invoke-virtual {v0, p0, p3, p4}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    goto :goto_0
.end method

.method public startUnknownElement()V
    .locals 2

    .prologue
    .line 428
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->xmlReader:Lorg/xml/sax/XMLReader;

    .line 429
    .local v0, "reader":Lorg/xml/sax/XMLReader;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser$UnknownElementHandler;

    invoke-direct {v1, p0, p0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser$UnknownElementHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V

    .line 430
    .local v1, "unknown":Lcom/samsung/thumbnail/office/ooxml/OOXMLParser$UnknownElementHandler;
    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 431
    return-void
.end method

.method public updateNSId(Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V
    .locals 4
    .param p1, "nameSpace"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    .prologue
    .line 320
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->preDefinedNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 321
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->preDefinedNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    aget-object v1, v3, v0

    .line 322
    .local v1, "ns":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->getNameSpaceUri()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    .line 320
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 326
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->getNameSpaceUri()Ljava/lang/String;

    move-result-object v2

    .line 327
    .local v2, "nsURI":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->getXMLNS()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->getXMLNS()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->getXMLCatagory()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 329
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->getID()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setID(I)V

    .line 338
    .end local v1    # "ns":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .end local v2    # "nsURI":Ljava/lang/String;
    :goto_1
    return-void

    .line 331
    .restart local v1    # "ns":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .restart local v2    # "nsURI":Ljava/lang/String;
    :cond_2
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->getNameSpaceUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 332
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->getID()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setID(I)V

    goto :goto_1

    .line 336
    .end local v1    # "ns":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .end local v2    # "nsURI":Ljava/lang/String;
    :cond_3
    iget v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->nameSpaceBaseIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->nameSpaceBaseIndex:I

    .line 337
    iget v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->nameSpaceBaseIndex:I

    invoke-virtual {p1, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setID(I)V

    goto :goto_1
.end method
