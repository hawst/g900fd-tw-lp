.class public final enum Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;
.super Ljava/lang/Enum;
.source "StockObject.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

.field public static final enum ANSI_FIXED_FONT:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

.field public static final enum ANSI_VAR_FONT:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

.field public static final enum BLACK_BRUSH:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

.field public static final enum BLACK_PEN:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

.field public static final enum DC_BRUSH:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

.field public static final enum DC_PEN:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

.field public static final enum DEFAULT_GUI_FONT:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

.field public static final enum DEFAULT_PALETTE:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

.field public static final enum DEVICE_DEFAULT_FONT:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

.field public static final enum DKGRAY_BRUSH:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

.field public static final enum GRAY_BRUSH:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

.field public static final enum LTGRAY_BRUSH:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

.field public static final enum NULL_BRUSH:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

.field public static final enum NULL_PEN:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

.field public static final enum OEM_FIXED_FONT:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

.field public static final enum SYSTEM_FIXED_FONT:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

.field public static final enum SYSTEM_FONT:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

.field public static final enum WHITE_BRUSH:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

.field public static final enum WHITE_PEN:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;


# instance fields
.field private StockObjectType:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 5
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    const-string/jumbo v1, "WHITE_BRUSH"

    const/high16 v2, -0x80000000

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->WHITE_BRUSH:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    const-string/jumbo v1, "LTGRAY_BRUSH"

    const v2, -0x7fffffff

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->LTGRAY_BRUSH:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    const-string/jumbo v1, "GRAY_BRUSH"

    const v2, -0x7ffffffe

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->GRAY_BRUSH:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    const-string/jumbo v1, "DKGRAY_BRUSH"

    const v2, -0x7ffffffd

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->DKGRAY_BRUSH:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    .line 6
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    const-string/jumbo v1, "BLACK_BRUSH"

    const v2, -0x7ffffffc

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->BLACK_BRUSH:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    const-string/jumbo v1, "NULL_BRUSH"

    const/4 v2, 0x5

    const v3, -0x7ffffffb

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->NULL_BRUSH:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    const-string/jumbo v1, "WHITE_PEN"

    const/4 v2, 0x6

    const v3, -0x7ffffffa

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->WHITE_PEN:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    .line 7
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    const-string/jumbo v1, "BLACK_PEN"

    const/4 v2, 0x7

    const v3, -0x7ffffff9

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->BLACK_PEN:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    const-string/jumbo v1, "NULL_PEN"

    const/16 v2, 0x8

    const v3, -0x7ffffff8

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->NULL_PEN:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    const-string/jumbo v1, "OEM_FIXED_FONT"

    const/16 v2, 0x9

    const v3, -0x7ffffff6

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->OEM_FIXED_FONT:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    .line 8
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    const-string/jumbo v1, "ANSI_FIXED_FONT"

    const/16 v2, 0xa

    const v3, -0x7ffffff5

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->ANSI_FIXED_FONT:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    const-string/jumbo v1, "ANSI_VAR_FONT"

    const/16 v2, 0xb

    const v3, -0x7ffffff4

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->ANSI_VAR_FONT:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    const-string/jumbo v1, "SYSTEM_FONT"

    const/16 v2, 0xc

    const v3, -0x7ffffff3

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->SYSTEM_FONT:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    .line 9
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    const-string/jumbo v1, "DEVICE_DEFAULT_FONT"

    const/16 v2, 0xd

    const v3, -0x7ffffff2

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->DEVICE_DEFAULT_FONT:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    const-string/jumbo v1, "DEFAULT_PALETTE"

    const/16 v2, 0xe

    const v3, -0x7ffffff1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->DEFAULT_PALETTE:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    .line 10
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    const-string/jumbo v1, "SYSTEM_FIXED_FONT"

    const/16 v2, 0xf

    const v3, -0x7ffffff0

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->SYSTEM_FIXED_FONT:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    const-string/jumbo v1, "DEFAULT_GUI_FONT"

    const/16 v2, 0x10

    const v3, -0x7fffffef

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->DEFAULT_GUI_FONT:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    .line 11
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    const-string/jumbo v1, "DC_BRUSH"

    const/16 v2, 0x11

    const v3, -0x7fffffee

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->DC_BRUSH:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    const-string/jumbo v1, "DC_PEN"

    const/16 v2, 0x12

    const v3, -0x7fffffed

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->DC_PEN:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    .line 3
    const/16 v0, 0x13

    new-array v0, v0, [Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    sget-object v1, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->WHITE_BRUSH:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->LTGRAY_BRUSH:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->GRAY_BRUSH:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->DKGRAY_BRUSH:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->BLACK_BRUSH:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->NULL_BRUSH:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->WHITE_PEN:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->BLACK_PEN:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->NULL_PEN:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->OEM_FIXED_FONT:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->ANSI_FIXED_FONT:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->ANSI_VAR_FONT:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->SYSTEM_FONT:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->DEVICE_DEFAULT_FONT:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->DEFAULT_PALETTE:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->SYSTEM_FIXED_FONT:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->DEFAULT_GUI_FONT:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->DC_BRUSH:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->DC_PEN:Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->$VALUES:[Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 16
    iput p3, p0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->StockObjectType:I

    .line 17
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->$VALUES:[Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;

    return-object v0
.end method


# virtual methods
.method public getStockObjectType()I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfEnums/StockObject;->StockObjectType:I

    return v0
.end method
