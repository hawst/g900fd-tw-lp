.class public Lcom/samsung/thumbnail/office/ooxml/excel/util/OOExcelXMLElements;
.super Ljava/lang/Object;
.source "OOExcelXMLElements.java"


# static fields
.field public static final ALIGMENT_HORIZONTAL:Ljava/lang/String; = "horizontal"

.field public static final ALIGMENT_SHRINK_TO_FIT:Ljava/lang/String; = "shrinkToFit"

.field public static final ALIGMENT_VERTICAL:Ljava/lang/String; = "vertical"

.field public static final ALIGMENT_WRAPTEXT:Ljava/lang/String; = "wrapText"

.field public static final ALIGNMENT_INDENT:Ljava/lang/String; = "indent"

.field public static final BORDER_BOTTOM:Ljava/lang/String; = "bottom"

.field public static final BORDER_DIAGONAL:Ljava/lang/String; = "diagonal"

.field public static final BORDER_LEFT:Ljava/lang/String; = "left"

.field public static final BORDER_RIGHT:Ljava/lang/String; = "right"

.field public static final BORDER_STYLE:Ljava/lang/String; = "style"

.field public static final BORDER_TOP:Ljava/lang/String; = "top"

.field public static final CALPR_CALCID:Ljava/lang/String; = "calcId"

.field public static final CELLSTYLE_BUILTINID:Ljava/lang/String; = "builtinId"

.field public static final CELLSTYLE_NAME:Ljava/lang/String; = "name"

.field public static final CELLSTYLE_XFID:Ljava/lang/String; = "xfId"

.field public static final COL_CUSTWDTH:Ljava/lang/String; = "customWidth"

.field public static final COL_MAX:Ljava/lang/String; = "max"

.field public static final COL_MIN:Ljava/lang/String; = "min"

.field public static final COL_WIDHTH:Ljava/lang/String; = "width"

.field public static final COUNT:Ljava/lang/String; = "count"

.field public static final DIMENSN_REF:Ljava/lang/String; = "ref"

.field public static final DRAWING_ID:Ljava/lang/String; = "id"

.field public static final EXCEL_DRAWING:Ljava/lang/String; = "wsDr"

.field public static final EXCEL_DRAWING_BLIP_ELE:Ljava/lang/String; = "blip"

.field public static final EXCEL_DRAWING_BLIP_FILL_ELE:Ljava/lang/String; = "blipFill"

.field public static final EXCEL_DRAWING_CNVPR:Ljava/lang/String; = "cNvPr"

.field public static final EXCEL_DRAWING_CNVPR_DESCR:Ljava/lang/String; = "descr"

.field public static final EXCEL_DRAWING_CNVPR_ID:Ljava/lang/String; = "id"

.field public static final EXCEL_DRAWING_CNVPR_NAME:Ljava/lang/String; = "name"

.field public static final EXCEL_DRAWING_EDITAS:Ljava/lang/String; = "editAs"

.field public static final EXCEL_DRAWING_GRPSP:Ljava/lang/String; = "sp"

.field public static final EXCEL_DRAWING_NVGRAPHICFRAMEPR:Ljava/lang/String; = "nvGraphicFramePr"

.field public static final EXCEL_DRAWING_NVPICPR:Ljava/lang/String; = "nvPicPr"

.field public static final EXCEL_DRAWING_NVSPPR:Ljava/lang/String; = "nvSpPr"

.field public static final EXCEL_DRAWING_ONECELLANCHR_EXT_ELE:Ljava/lang/String; = "ext"

.field public static final EXCEL_DRAWING_ONE_CELLANCHOR:Ljava/lang/String; = "oneCellAnchor"

.field public static final EXCEL_DRAWING_PIC:Ljava/lang/String; = "pic"

.field public static final EXCEL_DRAWING_PIC_COL:Ljava/lang/String; = "col"

.field public static final EXCEL_DRAWING_PIC_COL_OFF:Ljava/lang/String; = "colOff"

.field public static final EXCEL_DRAWING_PIC_FROM:Ljava/lang/String; = "from"

.field public static final EXCEL_DRAWING_PIC_ROW:Ljava/lang/String; = "row"

.field public static final EXCEL_DRAWING_PIC_ROW_OFF:Ljava/lang/String; = "rowOff"

.field public static final EXCEL_DRAWING_PIC_TO:Ljava/lang/String; = "to"

.field public static final EXCEL_DRAWING_SP:Ljava/lang/String; = "sp"

.field public static final EXCEL_DRAWING_SPPR_ELE:Ljava/lang/String; = "spPr"

.field public static final EXCEL_DRAWING_SPPR_ELE_XSSF:Ljava/lang/String; = "ExcelspPr"

.field public static final EXCEL_DRAWING_SPPR_XFRM_ELE:Ljava/lang/String; = "xfrm"

.field public static final EXCEL_DRAWING_SPPR_XFRM_EXT_CX_ELE:Ljava/lang/String; = "cx"

.field public static final EXCEL_DRAWING_SPPR_XFRM_EXT_CY_ELE:Ljava/lang/String; = "cy"

.field public static final EXCEL_DRAWING_SPPR_XFRM_OFF_ELE:Ljava/lang/String; = "Off"

.field public static final EXCEL_DRAWING_SPPR_XFRM_OFF_X_ELE:Ljava/lang/String; = "x"

.field public static final EXCEL_DRAWING_SPPR_XFRM_OFF_Y_ELE:Ljava/lang/String; = "y"

.field public static final EXCEL_DRAWING_SPPR_XFRM_PSTGEOM:Ljava/lang/String; = "prstGeom"

.field public static final EXCEL_DRAWING_SPPR_XFRM_PSTGEOM_AVLST:Ljava/lang/String; = "avLst"

.field public static final EXCEL_DRAWING_SPPR_XFRM_PSTGEOM_AVLST_GD:Ljava/lang/String; = "gd"

.field public static final EXCEL_DRAWING_SPPR_XFRM_PSTGEOM_AVLST_GD_FMLA:Ljava/lang/String; = "fmla"

.field public static final EXCEL_DRAWING_SPPR_XFRM_PSTGEOM_AVLST_GD_NAME:Ljava/lang/String; = "name"

.field public static final EXCEL_DRAWING_SPPR_XFRM_PSTGEOM_PRST:Ljava/lang/String; = "prst"

.field public static final EXCEL_DRAWING_STYLE_EFFECTREF_ELE:Ljava/lang/String; = "effectRef"

.field public static final EXCEL_DRAWING_STYLE_ELE:Ljava/lang/String; = "style"

.field public static final EXCEL_DRAWING_STYLE_FILLREF_ELE:Ljava/lang/String; = "fillRef"

.field public static final EXCEL_DRAWING_STYLE_FONTREF_ELE:Ljava/lang/String; = "fontRef"

.field public static final EXCEL_DRAWING_STYLE_IDX_ELE:Ljava/lang/String; = "idx"

.field public static final EXCEL_DRAWING_STYLE_INREF_ELE:Ljava/lang/String; = "lnRef"

.field public static final EXCEL_DRAWING_STYLE_SCHEMECLR_ELE:Ljava/lang/String; = "schemeClr"

.field public static final EXCEL_DRAWING_TWO_CELLANCHOR:Ljava/lang/String; = "twoCellAnchor"

.field public static final EXCEL_TABLE:Ljava/lang/String; = "table"

.field public static final EXCEL_TABLE_COLUMN:Ljava/lang/String; = "tableColumn"

.field public static final EXCEL_TABLE_COLUMNS:Ljava/lang/String; = "tableColumns"

.field public static final EXCEL_TABLE_COLUMN_ID:Ljava/lang/String; = "id"

.field public static final EXCEL_TABLE_COLUMN_NAME:Ljava/lang/String; = "name"

.field public static final EXCEL_TABLE_DISPLAYNAME:Ljava/lang/String; = "displayName"

.field public static final EXCEL_TABLE_ID:Ljava/lang/String; = "id"

.field public static final EXCEL_TABLE_NAME:Ljava/lang/String; = "name"

.field public static final EXCEL_TABLE_REF:Ljava/lang/String; = "ref"

.field public static final EXCEL_TABLE_TOTALROW_SHOWN:Ljava/lang/String; = "totalsRowShown"

.field public static final FGCOLOR_THEME:Ljava/lang/String; = "theme"

.field public static final FILEBOOKVIEWS_ELE:Ljava/lang/String; = "bookViews"

.field public static final FILECALCPR_ELE:Ljava/lang/String; = "calcPr"

.field public static final FILECUSTWRKVIEW_ELE:Ljava/lang/String; = "customWorkbookViews"

.field public static final FILEDEFNAME_ELE:Ljava/lang/String; = "definedNames"

.field public static final FILEEXTLST_ELE:Ljava/lang/String; = "extLst"

.field public static final FILEEXTREF_ELE:Ljava/lang/String; = "externalReferences"

.field public static final FILEFUNGRP_ELE:Ljava/lang/String; = "functionGroups"

.field public static final FILEOLESIZE_ELE:Ljava/lang/String; = "oleSize"

.field public static final FILEPIVOTCACHE_ELE:Ljava/lang/String; = "pivotCaches"

.field public static final FILERECPRLIST_ELE:Ljava/lang/String; = "fileRecoveryPrList"

.field public static final FILESHARING_ELE:Ljava/lang/String; = "fileSharing"

.field public static final FILESHEETS_ELE:Ljava/lang/String; = "sheets"

.field public static final FILESHEET_ELE:Ljava/lang/String; = "sheet"

.field public static final FILESMARTTAG_ELE:Ljava/lang/String; = "smartTagPr"

.field public static final FILEVERSION_ELE:Ljava/lang/String; = "fileVersion"

.field public static final FILEWEBPUBLSH_ELE:Ljava/lang/String; = "webPublishing"

.field public static final FILEWEBPUBOBJ_ELE:Ljava/lang/String; = "webPublishObjects"

.field public static final FILEWORKBOOKVIEW_ELE:Ljava/lang/String; = "workbookView"

.field public static final FILEWORKBUKPRPROC_ELE:Ljava/lang/String; = "workbookProtection"

.field public static final FILEWORKBUKPR_ELE:Ljava/lang/String; = "workbookPr"

.field public static final FONT_COLOR_INDEXED:Ljava/lang/String; = "indexed"

.field public static final FONT_COLOR_RGB:Ljava/lang/String; = "rgb"

.field public static final FONT_COLOR_THEME:Ljava/lang/String; = "theme"

.field public static final FONT_COLOR_TINT:Ljava/lang/String; = "tint"

.field public static final FV_APPNAME:Ljava/lang/String; = "appName"

.field public static final FV_CODENAME:Ljava/lang/String; = "codeName"

.field public static final FV_LASTEDITED:Ljava/lang/String; = "lastEdited"

.field public static final FV_LOWEDITED:Ljava/lang/String; = "lowestEdited"

.field public static final FV_RUPBUILD:Ljava/lang/String; = "rupBuild"

.field public static final GRADIENT_FILL:Ljava/lang/String; = "gradientFill"

.field public static final GRADIENT_FILL_DEGREE:Ljava/lang/String; = "degree"

.field public static final GRADIENT_FILL_STOP:Ljava/lang/String; = "stop"

.field public static final GRADIENT_FILL_STOP_POSITION:Ljava/lang/String; = "position"

.field public static final GRADIENT_FILL_TYPE:Ljava/lang/String; = "type"

.field public static final HIDDEN:Ljava/lang/String; = "hidden"

.field public static final MERGECELLS_VAL:Ljava/lang/String; = "ref"

.field public static final PAGEMAR_BOTTOM:Ljava/lang/String; = "bottom"

.field public static final PAGEMAR_FOOTER:Ljava/lang/String; = "footer"

.field public static final PAGEMAR_HEADER:Ljava/lang/String; = "header"

.field public static final PAGEMAR_LEFT:Ljava/lang/String; = "left"

.field public static final PAGEMAR_RIGHT:Ljava/lang/String; = "right"

.field public static final PAGEMAR_TOP:Ljava/lang/String; = "top"

.field public static final PAGESETUP_ID:Ljava/lang/String; = "id"

.field public static final PAGESETUP_ORIENT:Ljava/lang/String; = "orientation"

.field public static final PAGESETUP_VERTICALDPI:Ljava/lang/String; = "verticalDpi"

.field public static final PATTERNFILLS_TYPE:Ljava/lang/String; = "patternType"

.field public static final ROW_CUSTHEIGHT:Ljava/lang/String; = "customHeight"

.field public static final SELECTION_ACTVCELL:Ljava/lang/String; = "activeCell"

.field public static final SELECTION_SQREF:Ljava/lang/String; = "sqref"

.field public static final SHARED_STRING:Ljava/lang/String; = "sst"

.field public static final SHARED_STRING_B:Ljava/lang/String; = "b"

.field public static final SHARED_STRING_COLOR:Ljava/lang/String; = "color"

.field public static final SHARED_STRING_FAMILY:Ljava/lang/String; = "family"

.field public static final SHARED_STRING_Italic:Ljava/lang/String; = "i"

.field public static final SHARED_STRING_R:Ljava/lang/String; = "r"

.field public static final SHARED_STRING_RFONT:Ljava/lang/String; = "rFont"

.field public static final SHARED_STRING_RPR:Ljava/lang/String; = "rPr"

.field public static final SHARED_STRING_SCHEME:Ljava/lang/String; = "scheme"

.field public static final SHARED_STRING_SI:Ljava/lang/String; = "si"

.field public static final SHARED_STRING_STRIKE:Ljava/lang/String; = "strike"

.field public static final SHARED_STRING_SZ:Ljava/lang/String; = "sz"

.field public static final SHARED_STRING_T:Ljava/lang/String; = "t"

.field public static final SHARED_STRING_U:Ljava/lang/String; = "u"

.field public static final SHARED_STRING_VERTALIGN:Ljava/lang/String; = "vertAlign"

.field public static final SHEETDATA_ROW_C_R:Ljava/lang/String; = "r"

.field public static final SHEETDATA_ROW_C_R_V:Ljava/lang/String; = "v"

.field public static final SHEETDATA_ROW_C_S:Ljava/lang/String; = "s"

.field public static final SHEETDATA_ROW_C_T:Ljava/lang/String; = "t"

.field public static final SHEETDATA_ROW_HT:Ljava/lang/String; = "ht"

.field public static final SHEETDATA_ROW_R:Ljava/lang/String; = "r"

.field public static final SHEETDATA_ROW_SPANS:Ljava/lang/String; = "spans"

.field public static final SHEETFORPR_DEFROWHGT:Ljava/lang/String; = "defaultRowHeight"

.field public static final SHEETVIEW_WBVIEWID:Ljava/lang/String; = "workbookViewId"

.field public static final SHEET_NAME:Ljava/lang/String; = "name"

.field public static final SHEET_RID:Ljava/lang/String; = "id"

.field public static final SHEET_SHTID:Ljava/lang/String; = "sheetId"

.field public static final SHOW_GRID_LINES:Ljava/lang/String; = "showGridLines"

.field public static final SHOW_ROWCOL_HEADERS:Ljava/lang/String; = "showRowColHeaders"

.field public static final STYLESHET_ALIGMENT:Ljava/lang/String; = "alignment"

.field public static final STYLESHET_B:Ljava/lang/String; = "b"

.field public static final STYLESHET_BORDER:Ljava/lang/String; = "border"

.field public static final STYLESHET_BORDERS:Ljava/lang/String; = "borders"

.field public static final STYLESHET_CELLSTYLE:Ljava/lang/String; = "cellStyle"

.field public static final STYLESHET_CELLSTYLES:Ljava/lang/String; = "cellStyles"

.field public static final STYLESHET_CELLSTYLEXFS:Ljava/lang/String; = "cellStyleXfs"

.field public static final STYLESHET_CELLXF:Ljava/lang/String; = "cellXfs"

.field public static final STYLESHET_COLOR:Ljava/lang/String; = "color"

.field public static final STYLESHET_DXFS:Ljava/lang/String; = "dxfs"

.field public static final STYLESHET_ELE:Ljava/lang/String; = "styleSheet"

.field public static final STYLESHET_FAMILY:Ljava/lang/String; = "family"

.field public static final STYLESHET_FILL:Ljava/lang/String; = "fill"

.field public static final STYLESHET_FILLS:Ljava/lang/String; = "fills"

.field public static final STYLESHET_FONT:Ljava/lang/String; = "font"

.field public static final STYLESHET_FONTS:Ljava/lang/String; = "fonts"

.field public static final STYLESHET_ITALIC:Ljava/lang/String; = "i"

.field public static final STYLESHET_NAME:Ljava/lang/String; = "name"

.field public static final STYLESHET_NUMFMT:Ljava/lang/String; = "numFmt"

.field public static final STYLESHET_NUMFMTS:Ljava/lang/String; = "numFmts"

.field public static final STYLESHET_PATTERNFILL:Ljava/lang/String; = "patternFill"

.field public static final STYLESHET_PATTERNFILL_FGCOLOR:Ljava/lang/String; = "fgColor"

.field public static final STYLESHET_PATTERNFILL_FGCOLOR_TINT:Ljava/lang/String; = "tint"

.field public static final STYLESHET_SCHEME:Ljava/lang/String; = "scheme"

.field public static final STYLESHET_SZ:Ljava/lang/String; = "sz"

.field public static final STYLESHET_TABLESTYLES:Ljava/lang/String; = "tableStyles"

.field public static final STYLESHET_XF:Ljava/lang/String; = "xf"

.field public static final TABLESTYLES_DEFAULTPIVOTSTYLE:Ljava/lang/String; = "defaultPivotStyle"

.field public static final TABLESTYLES_DEFAULTSTYLE:Ljava/lang/String; = "defaultTableStyle"

.field public static final VAL:Ljava/lang/String; = "val"

.field public static final WBPR_DEFALTTHEMVERSN:Ljava/lang/String; = "defaultThemeVersion"

.field public static final WBVIEW_ACTIVETAB:Ljava/lang/String; = "activeTab"

.field public static final WBVIEW_WINDOWHGT:Ljava/lang/String; = "windowHeight"

.field public static final WBVIEW_WINDOWWIDTH:Ljava/lang/String; = "windowWidth"

.field public static final WBVIEW_XWINDOW:Ljava/lang/String; = "xWindow"

.field public static final WBVIEW_YWINDOW:Ljava/lang/String; = "yWindow"

.field public static final WORKBOOK_ELE:Ljava/lang/String; = "workbook"

.field public static final WORKSHEETS_ELE:Ljava/lang/String; = "worksheet"

.field public static final WRKSHTS_COL:Ljava/lang/String; = "col"

.field public static final WRKSHTS_COLS:Ljava/lang/String; = "cols"

.field public static final WRKSHTS_DIMNSN:Ljava/lang/String; = "dimension"

.field public static final WRKSHTS_DRAWINGS:Ljava/lang/String; = "drawing"

.field public static final WRKSHTS_FORMATPR:Ljava/lang/String; = "sheetFormatPr"

.field public static final WRKSHTS_HYPERLINK:Ljava/lang/String; = "hyperlink"

.field public static final WRKSHTS_HYPERLINKS:Ljava/lang/String; = "hyperlinks"

.field public static final WRKSHTS_MERGECELL:Ljava/lang/String; = "mergeCell"

.field public static final WRKSHTS_MERGECELLS:Ljava/lang/String; = "mergeCells"

.field public static final WRKSHTS_PAGEMARGN:Ljava/lang/String; = "pageMargins"

.field public static final WRKSHTS_PAGESETUP:Ljava/lang/String; = "pageSetup"

.field public static final WRKSHTS_ROW:Ljava/lang/String; = "row"

.field public static final WRKSHTS_ROWC:Ljava/lang/String; = "c"

.field public static final WRKSHTS_ROWCV:Ljava/lang/String; = "v"

.field public static final WRKSHTS_SELECTION:Ljava/lang/String; = "selection"

.field public static final WRKSHTS_SHEETDATA:Ljava/lang/String; = "sheetData"

.field public static final WRKSHTS_SHTVIEW:Ljava/lang/String; = "sheetView"

.field public static final WRKSHTS_SHTVIEWS:Ljava/lang/String; = "sheetViews"

.field public static final XFS_ALIGNMNT:Ljava/lang/String; = "applyAlignment"

.field public static final XFS_APPLY_FONT:Ljava/lang/String; = "applyFont"

.field public static final XFS_APPLY_NUMBERFORMAT:Ljava/lang/String; = "applyNumberFormat"

.field public static final XFS_BORDERID:Ljava/lang/String; = "borderId"

.field public static final XFS_FILLID:Ljava/lang/String; = "fillId"

.field public static final XFS_FONTID:Ljava/lang/String; = "fontId"

.field public static final XFS_FORMATCODE:Ljava/lang/String; = "formatCode"

.field public static final XFS_ID:Ljava/lang/String; = "xfId"

.field public static final XFS_NUMFMTID:Ljava/lang/String; = "numFmtId"

.field public static final definedRPR:Ljava/lang/String; = "~"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
