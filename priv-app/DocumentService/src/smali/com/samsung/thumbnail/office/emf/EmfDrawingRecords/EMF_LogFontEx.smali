.class public Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;
.super Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;
.source "EMF_LogFontEx.java"


# instance fields
.field private FullName:[B

.field private Script:[B

.field private Style:[B

.field private fileContent:[B

.field private offset:I

.field private sizeofLogFont:I


# direct methods
.method public constructor <init>([BI)V
    .locals 1
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFont;-><init>([BI)V

    .line 22
    const/16 v0, 0x52

    iput v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->sizeofLogFont:I

    .line 40
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->fileContent:[B

    .line 41
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->offset:I

    .line 42
    return-void
.end method


# virtual methods
.method public getFullName()[B
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->FullName:[B

    return-object v0
.end method

.method public getOffset()I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->offset:I

    return v0
.end method

.method public getScript()[B
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->Script:[B

    return-object v0
.end method

.method public getStyle()[B
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->Style:[B

    return-object v0
.end method

.method public readEmrLogFontEx()V
    .locals 7

    .prologue
    .line 124
    const/4 v3, 0x0

    .line 125
    .local v3, "i":I
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->readEmrLogFont()V

    .line 126
    iget v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->offset:I

    iget v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->sizeofLogFont:I

    add-int/2addr v5, v6

    iput v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->offset:I

    .line 128
    const/16 v1, 0x80

    .line 129
    .local v1, "buffsize":I
    new-array v2, v1, [B

    .line 130
    .local v2, "fullName":[B
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    .line 131
    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->fileContent:[B

    iget v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->offset:I

    add-int/2addr v6, v3

    aget-byte v5, v5, v6

    aput-byte v5, v2, v3

    .line 130
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 133
    :cond_0
    iget v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->offset:I

    add-int/2addr v5, v3

    iput v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->offset:I

    .line 134
    invoke-virtual {p0, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->setFullName([B)V

    .line 136
    const/16 v1, 0x40

    .line 137
    new-array v4, v1, [B

    .line 138
    .local v4, "styleBuffer":[B
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v1, :cond_1

    .line 139
    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->fileContent:[B

    iget v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->offset:I

    add-int/2addr v6, v3

    aget-byte v5, v5, v6

    aput-byte v5, v4, v3

    .line 138
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 141
    :cond_1
    iget v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->offset:I

    add-int/2addr v5, v3

    iput v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->offset:I

    .line 142
    invoke-virtual {p0, v4}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->setStyle([B)V

    .line 144
    const/16 v1, 0x40

    .line 145
    new-array v0, v1, [B

    .line 146
    .local v0, "ScriptBuffer":[B
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v1, :cond_2

    .line 147
    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->fileContent:[B

    iget v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->offset:I

    add-int/2addr v6, v3

    aget-byte v5, v5, v6

    aput-byte v5, v0, v3

    .line 146
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 149
    :cond_2
    iget v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->offset:I

    add-int/2addr v5, v3

    iput v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->offset:I

    .line 150
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->setScript([B)V

    .line 151
    return-void
.end method

.method public setFullName([B)V
    .locals 0
    .param p1, "fullName"    # [B

    .prologue
    .line 60
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->FullName:[B

    .line 61
    return-void
.end method

.method public setOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 117
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->offset:I

    .line 118
    return-void
.end method

.method public setScript([B)V
    .locals 0
    .param p1, "script"    # [B

    .prologue
    .line 98
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->Script:[B

    .line 99
    return-void
.end method

.method public setStyle([B)V
    .locals 0
    .param p1, "style"    # [B

    .prologue
    .line 79
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->Style:[B

    .line 80
    return-void
.end method
