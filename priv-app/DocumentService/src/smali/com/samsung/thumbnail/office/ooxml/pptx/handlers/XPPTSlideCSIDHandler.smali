.class public Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSlideCSIDHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XPPTSlideCSIDHandler.java"


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "ID"    # I

    .prologue
    .line 8
    const-string/jumbo v0, "cSld"

    invoke-direct {p0, p1, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 9
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSlideCSIDHandler;->init(I)V

    .line 10
    return-void
.end method


# virtual methods
.method protected init(I)V
    .locals 4
    .param p1, "ID"    # I

    .prologue
    .line 13
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBackgrndHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBackgrndHandler;-><init>()V

    .line 14
    .local v0, "sldBckGrndHandler":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBackgrndHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSlideCSIDHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "bg"

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTShapeTreeHandler;

    invoke-direct {v1, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTShapeTreeHandler;-><init>(I)V

    .line 17
    .local v1, "spTreeHandler":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTShapeTreeHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSlideCSIDHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "spTree"

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    return-void
.end method
