.class Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$PointConsumer;
.super Ljava/lang/Object;
.source "XfrmHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler$IPointConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PointConsumer"
.end annotation


# instance fields
.field shape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 0
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$PointConsumer;->shape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 88
    return-void
.end method


# virtual methods
.method public setPoint(JJ)V
    .locals 1
    .param p1, "x"    # J
    .param p3, "y"    # J

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$PointConsumer;->shape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    if-nez v0, :cond_0

    .line 96
    :goto_0
    return-void

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$PointConsumer;->shape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setPoints(JJ)V

    goto :goto_0
.end method
