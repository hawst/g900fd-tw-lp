.class public Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;
.super Ljava/lang/Object;
.source "PaintProperties.java"


# instance fields
.field protected myColor:I

.field myPaintObject:Landroid/graphics/Paint;

.field protected myStrokeWidth:I

.field protected myStyle:Landroid/graphics/Paint$Style;

.field private myTextSize:I

.field myTypeFace:Landroid/graphics/Typeface;

.field private scaleX:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myPaintObject:Landroid/graphics/Paint;

    .line 10
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myStyle:Landroid/graphics/Paint$Style;

    .line 11
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myColor:I

    .line 12
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myStrokeWidth:I

    .line 13
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->scaleX:F

    .line 14
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myTypeFace:Landroid/graphics/Typeface;

    .line 15
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myTextSize:I

    return-void
.end method


# virtual methods
.method public getColor()I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myColor:I

    return v0
.end method

.method public getMyPaintBrush()Landroid/graphics/Paint;
    .locals 2

    .prologue
    .line 66
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myPaintObject:Landroid/graphics/Paint;

    .line 68
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myStyle:Landroid/graphics/Paint$Style;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myPaintObject:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myStyle:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myPaintObject:Landroid/graphics/Paint;

    iget v1, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 72
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myPaintObject:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 73
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myPaintObject:Landroid/graphics/Paint;

    iget v1, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myStrokeWidth:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 74
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myPaintObject:Landroid/graphics/Paint;

    iget v1, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->scaleX:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextScaleX(F)V

    .line 76
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myTextSize:I

    if-lez v0, :cond_1

    .line 77
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myPaintObject:Landroid/graphics/Paint;

    iget v1, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myTextSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 79
    :cond_1
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myPaintObject:Landroid/graphics/Paint;

    return-object v0
.end method

.method public getScaleX()F
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->scaleX:F

    return v0
.end method

.method public getStrokeWidth()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myStrokeWidth:I

    return v0
.end method

.method public getStyle()Landroid/graphics/Paint$Style;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myStyle:Landroid/graphics/Paint$Style;

    return-object v0
.end method

.method public getTextSize()F
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myTextSize:I

    int-to-float v0, v0

    return v0
.end method

.method public getTypeFace()Landroid/graphics/Typeface;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myTypeFace:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public setColor(I)V
    .locals 0
    .param p1, "myClor"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myColor:I

    .line 31
    return-void
.end method

.method public setScaleX(F)V
    .locals 0
    .param p1, "scalX"    # F

    .prologue
    .line 46
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->scaleX:F

    .line 47
    return-void
.end method

.method public setStrokeWidth(I)V
    .locals 0
    .param p1, "myStrkeWidth"    # I

    .prologue
    .line 38
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myStrokeWidth:I

    .line 39
    return-void
.end method

.method public setStyle(Landroid/graphics/Paint$Style;)V
    .locals 0
    .param p1, "myStle"    # Landroid/graphics/Paint$Style;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myStyle:Landroid/graphics/Paint$Style;

    .line 23
    return-void
.end method

.method public setTextSize(I)V
    .locals 0
    .param p1, "myTxtSize"    # I

    .prologue
    .line 62
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myTextSize:I

    .line 63
    return-void
.end method

.method public setTypeFace(Landroid/graphics/Typeface;)V
    .locals 0
    .param p1, "myTypFace"    # Landroid/graphics/Typeface;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->myTypeFace:Landroid/graphics/Typeface;

    .line 55
    return-void
.end method
