.class public Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;
.super Ljava/lang/Object;
.source "EMR_PolyPolyline16.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;


# instance fields
.field private RecordSize:I

.field private RecordType:I

.field private aPoints:[B

.field arrayOfPoint:[Landroid/graphics/Point;

.field private bottom:I

.field private count:I

.field private emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

.field fileContent:[B

.field private left:I

.field localPath:Landroid/graphics/Path;

.field private maxPolylineCount:I

.field private numberofPolylines:I

.field private offset:I

.field private polylineCountArray:[I

.field private polylinePointCount:[B

.field private right:I

.field private top:I

.field twoDArrayPoints:[[Landroid/graphics/Point;


# direct methods
.method public constructor <init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V
    .locals 1
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I
    .param p3, "emrGlobals"    # Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .prologue
    const/4 v0, 0x0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->localPath:Landroid/graphics/Path;

    .line 412
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->arrayOfPoint:[Landroid/graphics/Point;

    .line 415
    check-cast v0, [[Landroid/graphics/Point;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->twoDArrayPoints:[[Landroid/graphics/Point;

    .line 80
    iput-object p3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .line 81
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->fileContent:[B

    .line 82
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    .line 83
    return-void
.end method


# virtual methods
.method public getBottom()I
    .locals 1

    .prologue
    .line 224
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->bottom:I

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 148
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->count:I

    return v0
.end method

.method public getLeft()I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->left:I

    return v0
.end method

.method public getLocalPath()Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->localPath:Landroid/graphics/Path;

    return-object v0
.end method

.method public getNumberofPolylines()I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->numberofPolylines:I

    return v0
.end method

.method public getPolylinePointCount()[B
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->polylinePointCount:[B

    return-object v0
.end method

.method public getRecordSize()I
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->RecordSize:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->RecordType:I

    return v0
.end method

.method public getRight()I
    .locals 1

    .prologue
    .line 205
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->right:I

    return v0
.end method

.method public getTop()I
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->top:I

    return v0
.end method

.method public getaPoints()[B
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->aPoints:[B

    return-object v0
.end method

.method public prepare()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 423
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->getaPoints()[B

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->setPoints([B)V

    .line 424
    new-instance v7, Landroid/graphics/Path;

    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    .line 425
    .local v7, "localPath1":Landroid/graphics/Path;
    new-instance v8, Landroid/graphics/Path;

    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    iput-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->localPath:Landroid/graphics/Path;

    .line 427
    const/4 v6, 0x0

    .line 430
    .local v6, "l":I
    :goto_0
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->numberofPolylines:I

    if-lt v6, v8, :cond_0

    .line 461
    return-void

    .line 433
    :cond_0
    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->twoDArrayPoints:[[Landroid/graphics/Point;

    aget-object v8, v8, v6

    aget-object v8, v8, v10

    iget v8, v8, Landroid/graphics/Point;->x:I

    int-to-float v0, v8

    .line 434
    .local v0, "f1":F
    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->twoDArrayPoints:[[Landroid/graphics/Point;

    aget-object v8, v8, v6

    aget-object v8, v8, v10

    iget v8, v8, Landroid/graphics/Point;->y:I

    int-to-float v1, v8

    .line 435
    .local v1, "f2":F
    const/4 v2, 0x0

    .line 436
    .local v2, "f3":F
    const/4 v3, 0x0

    .line 438
    .local v3, "f4":F
    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->twoDArrayPoints:[[Landroid/graphics/Point;

    aget-object v8, v8, v6

    aget-object v8, v8, v10

    if-eqz v8, :cond_1

    .line 439
    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v8, v0, v8

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v9, v9, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v9, v1, v9

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 442
    :cond_1
    const/4 v4, 0x0

    .line 443
    .local v4, "i":I
    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->polylineCountArray:[I

    aget v5, v8, v6

    .line 446
    .local v5, "j":I
    :goto_1
    if-lt v4, v5, :cond_2

    .line 458
    add-int/lit8 v6, v6, 0x1

    .line 459
    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->localPath:Landroid/graphics/Path;

    invoke-virtual {v8, v7}, Landroid/graphics/Path;->addPath(Landroid/graphics/Path;)V

    goto :goto_0

    .line 449
    :cond_2
    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->twoDArrayPoints:[[Landroid/graphics/Point;

    aget-object v8, v8, v6

    aget-object v8, v8, v4

    if-eqz v8, :cond_3

    .line 450
    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->twoDArrayPoints:[[Landroid/graphics/Point;

    aget-object v8, v8, v6

    aget-object v8, v8, v4

    iget v8, v8, Landroid/graphics/Point;->x:I

    int-to-float v2, v8

    .line 451
    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->twoDArrayPoints:[[Landroid/graphics/Point;

    aget-object v8, v8, v6

    aget-object v8, v8, v4

    iget v8, v8, Landroid/graphics/Point;->y:I

    int-to-float v3, v8

    .line 453
    :cond_3
    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v8, v2, v8

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v9, v9, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v9, v3, v9

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 455
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method public readPolyPolyline16()V
    .locals 20

    .prologue
    .line 279
    const/4 v8, 0x0

    .line 281
    .local v8, "i":I
    const/16 v17, 0x4

    move/from16 v0, v17

    new-array v9, v0, [B

    .line 283
    .local v9, "intConvert":[B
    const/4 v8, 0x0

    :goto_0
    const/16 v17, 0x3

    move/from16 v0, v17

    if-gt v8, v0, :cond_0

    .line 284
    rsub-int/lit8 v17, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->fileContent:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    move/from16 v19, v0

    add-int v19, v19, v8

    aget-byte v18, v18, v19

    aput-byte v18, v9, v17

    .line 283
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 285
    :cond_0
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v9, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v4

    .line 286
    .local v4, "Type":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    move/from16 v17, v0

    add-int v17, v17, v8

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    .line 287
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->setRecordType(I)V

    .line 289
    const/4 v8, 0x0

    :goto_1
    const/16 v17, 0x3

    move/from16 v0, v17

    if-gt v8, v0, :cond_1

    .line 290
    rsub-int/lit8 v17, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->fileContent:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    move/from16 v19, v0

    add-int v19, v19, v8

    aget-byte v18, v18, v19

    aput-byte v18, v9, v17

    .line 289
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 291
    :cond_1
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v9, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v3

    .line 292
    .local v3, "Size":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    move/from16 v17, v0

    add-int v17, v17, v8

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    .line 293
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->setRecordSize(I)V

    .line 295
    const/4 v8, 0x0

    :goto_2
    const/16 v17, 0x3

    move/from16 v0, v17

    if-gt v8, v0, :cond_2

    .line 296
    rsub-int/lit8 v17, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->fileContent:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    move/from16 v19, v0

    add-int v19, v19, v8

    aget-byte v18, v18, v19

    aput-byte v18, v9, v17

    .line 295
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 297
    :cond_2
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v9, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v11

    .line 298
    .local v11, "lef":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    move/from16 v17, v0

    add-int v17, v17, v8

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    .line 299
    int-to-float v0, v11

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->setLeft(I)V

    .line 301
    const/4 v8, 0x0

    :goto_3
    const/16 v17, 0x3

    move/from16 v0, v17

    if-gt v8, v0, :cond_3

    .line 302
    rsub-int/lit8 v17, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->fileContent:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    move/from16 v19, v0

    add-int v19, v19, v8

    aget-byte v18, v18, v19

    aput-byte v18, v9, v17

    .line 301
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 303
    :cond_3
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v9, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v16

    .line 304
    .local v16, "top":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    move/from16 v17, v0

    add-int v17, v17, v8

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    .line 305
    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->setTop(I)V

    .line 307
    const/4 v8, 0x0

    :goto_4
    const/16 v17, 0x3

    move/from16 v0, v17

    if-gt v8, v0, :cond_4

    .line 308
    rsub-int/lit8 v17, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->fileContent:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    move/from16 v19, v0

    add-int v19, v19, v8

    aget-byte v18, v18, v19

    aput-byte v18, v9, v17

    .line 307
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    .line 309
    :cond_4
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v9, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v15

    .line 310
    .local v15, "right":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    move/from16 v17, v0

    add-int v17, v17, v8

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    .line 311
    int-to-float v0, v15

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->setRight(I)V

    .line 313
    const/4 v8, 0x0

    :goto_5
    const/16 v17, 0x3

    move/from16 v0, v17

    if-gt v8, v0, :cond_5

    .line 314
    rsub-int/lit8 v17, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->fileContent:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    move/from16 v19, v0

    add-int v19, v19, v8

    aget-byte v18, v18, v19

    aput-byte v18, v9, v17

    .line 313
    add-int/lit8 v8, v8, 0x1

    goto :goto_5

    .line 315
    :cond_5
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v9, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v5

    .line 316
    .local v5, "bot":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    move/from16 v17, v0

    add-int v17, v17, v8

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    .line 317
    int-to-float v0, v5

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->setBottom(I)V

    .line 319
    const/4 v8, 0x0

    :goto_6
    const/16 v17, 0x3

    move/from16 v0, v17

    if-gt v8, v0, :cond_6

    .line 320
    rsub-int/lit8 v17, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->fileContent:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    move/from16 v19, v0

    add-int v19, v19, v8

    aget-byte v18, v18, v19

    aput-byte v18, v9, v17

    .line 319
    add-int/lit8 v8, v8, 0x1

    goto :goto_6

    .line 321
    :cond_6
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v9, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v12

    .line 322
    .local v12, "numbrofPolylines":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    move/from16 v17, v0

    add-int v17, v17, v8

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    .line 323
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->setNumberofPolylines(I)V

    .line 325
    const/4 v8, 0x0

    :goto_7
    const/16 v17, 0x3

    move/from16 v0, v17

    if-gt v8, v0, :cond_7

    .line 326
    rsub-int/lit8 v17, v8, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->fileContent:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    move/from16 v19, v0

    add-int v19, v19, v8

    aget-byte v18, v18, v19

    aput-byte v18, v9, v17

    .line 325
    add-int/lit8 v8, v8, 0x1

    goto :goto_7

    .line 327
    :cond_7
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v9, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v7

    .line 328
    .local v7, "cnt":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    move/from16 v17, v0

    add-int v17, v17, v8

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    .line 329
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->setCount(I)V

    .line 332
    shl-int/lit8 v17, v12, 0x2

    move/from16 v0, v17

    new-array v2, v0, [B

    .line 333
    .local v2, "PolylinePointCount":[B
    new-array v0, v12, [I

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->polylineCountArray:[I

    .line 334
    const/4 v8, 0x0

    :goto_8
    if-ge v8, v12, :cond_a

    .line 335
    const/4 v10, 0x0

    .local v10, "j":I
    :goto_9
    const/16 v17, 0x3

    move/from16 v0, v17

    if-gt v10, v0, :cond_8

    .line 336
    rsub-int/lit8 v17, v10, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->fileContent:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    move/from16 v19, v0

    add-int v19, v19, v10

    aget-byte v18, v18, v19

    aput-byte v18, v9, v17

    .line 335
    add-int/lit8 v10, v10, 0x1

    goto :goto_9

    .line 337
    :cond_8
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v9, v0}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v13

    .line 338
    .local v13, "pcount":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    move/from16 v17, v0

    add-int v17, v17, v10

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    .line 339
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->polylineCountArray:[I

    move-object/from16 v17, v0

    aput v13, v17, v8

    .line 340
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->maxPolylineCount:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->polylineCountArray:[I

    move-object/from16 v18, v0

    aget v18, v18, v8

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_9

    .line 341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->polylineCountArray:[I

    move-object/from16 v17, v0

    aget v17, v17, v8

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->maxPolylineCount:I

    .line 334
    :cond_9
    add-int/lit8 v8, v8, 0x1

    goto :goto_8

    .line 344
    .end local v10    # "j":I
    .end local v13    # "pcount":I
    :cond_a
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->setPolylinePointCount([B)V

    .line 346
    add-int/lit8 v17, v3, -0x20

    shl-int/lit8 v18, v12, 0x2

    sub-int v6, v17, v18

    .line 347
    .local v6, "buffsize":I
    new-array v14, v6, [B

    .line 348
    .local v14, "pointsBuffer":[B
    const/4 v8, 0x0

    :goto_a
    if-ge v8, v6, :cond_b

    .line 349
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->fileContent:[B

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    move/from16 v18, v0

    add-int v18, v18, v8

    aget-byte v17, v17, v18

    aput-byte v17, v14, v8

    .line 348
    add-int/lit8 v8, v8, 0x1

    goto :goto_a

    .line 351
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    move/from16 v17, v0

    add-int v17, v17, v8

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->offset:I

    .line 352
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->setaPoints([B)V

    .line 353
    return-void
.end method

.method public render(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x2

    .line 363
    const/4 v1, 0x0

    .line 364
    .local v1, "i":I
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;-><init>()V

    .line 365
    .local v0, "PaintPolyline16PropertiesObject":Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;
    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 368
    iget-object v2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v2, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 369
    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v2, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 370
    iget-object v2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v3, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandlePen:I

    iget-object v2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v2, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->gethandleNumber()I

    move-result v2

    if-ne v3, v2, :cond_4

    .line 375
    :cond_0
    iget-object v2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v2, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 376
    iget-object v2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v2, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getPenStyle()I

    move-result v2

    if-ne v4, v2, :cond_5

    .line 378
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v2

    new-instance v3, Landroid/graphics/DashPathEffect;

    new-array v4, v4, [F

    fill-array-data v4, :array_0

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 388
    :goto_1
    iget-object v2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v2, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getColor()I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/emf/Util/toColorAndroid;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 391
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->getLocalPath()Landroid/graphics/Path;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 396
    :cond_1
    iget-object v2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v2, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eq v1, v2, :cond_2

    iget-object v2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v2, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_3

    .line 398
    :cond_2
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v2, v3, :cond_6

    .line 406
    :cond_3
    :goto_2
    return-void

    .line 369
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 383
    :cond_5
    iget-object v2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v2, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getPenStyle()I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/emf/Util/toStyleAndroid;->getStyle(I)Landroid/graphics/Paint$Style;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    goto :goto_1

    .line 401
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->getLocalPath()Landroid/graphics/Path;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_2

    .line 378
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x40400000    # 3.0f
    .end array-data
.end method

.method public setBottom(I)V
    .locals 0
    .param p1, "b"    # I

    .prologue
    .line 234
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->bottom:I

    .line 235
    return-void
.end method

.method public setCount(I)V
    .locals 0
    .param p1, "cnt"    # I

    .prologue
    .line 158
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->count:I

    .line 159
    return-void
.end method

.method public setLeft(I)V
    .locals 0
    .param p1, "l"    # I

    .prologue
    .line 177
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->left:I

    .line 178
    return-void
.end method

.method public setNumberofPolylines(I)V
    .locals 0
    .param p1, "numbrofPolylines"    # I

    .prologue
    .line 139
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->numberofPolylines:I

    .line 140
    return-void
.end method

.method public setPoints([B)V
    .locals 13
    .param p1, "points"    # [B

    .prologue
    .line 479
    const/4 v2, 0x0

    .line 480
    .local v2, "i":I
    const/4 v4, 0x0

    .line 481
    .local v4, "j":I
    const/4 v5, 0x0

    .line 482
    .local v5, "k":I
    const/4 v8, 0x0

    .line 484
    .local v8, "offset":I
    array-length v11, p1

    shr-int/lit8 v11, v11, 0x2

    new-array v11, v11, [Landroid/graphics/Point;

    iput-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->arrayOfPoint:[Landroid/graphics/Point;

    .line 486
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->numberofPolylines:I

    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->maxPolylineCount:I

    filled-new-array {v11, v12}, [I

    move-result-object v11

    const-class v12, Landroid/graphics/Point;

    invoke-static {v12, v11}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [[Landroid/graphics/Point;

    iput-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->twoDArrayPoints:[[Landroid/graphics/Point;

    .line 488
    const/4 v11, 0x2

    new-array v3, v11, [B

    .line 490
    .local v3, "intConvert":[B
    :goto_0
    array-length v11, p1

    if-ge v8, v11, :cond_4

    .line 491
    const/4 v2, 0x0

    :goto_1
    const/4 v11, 0x1

    if-gt v2, v11, :cond_0

    .line 492
    rsub-int/lit8 v11, v2, 0x1

    add-int v12, v8, v2

    aget-byte v12, p1, v12

    aput-byte v12, v3, v11

    .line 491
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 493
    :cond_0
    const/4 v11, 0x0

    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getSmallInt([BI)I

    move-result v0

    .line 494
    .local v0, "X":I
    add-int/2addr v8, v2

    .line 496
    const v11, 0x8000

    and-int/2addr v11, v0

    const v12, 0x8000

    if-ne v11, v12, :cond_1

    .line 497
    xor-int/lit8 v11, v0, -0x1

    const v12, 0xffff

    and-int/2addr v11, v12

    add-int/lit8 v11, v11, 0x1

    mul-int/lit8 v0, v11, -0x1

    .line 500
    :cond_1
    const/4 v4, 0x0

    :goto_2
    const/4 v11, 0x1

    if-gt v4, v11, :cond_2

    .line 501
    rsub-int/lit8 v11, v4, 0x1

    add-int v12, v8, v4

    aget-byte v12, p1, v12

    aput-byte v12, v3, v11

    .line 500
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 502
    :cond_2
    const/4 v11, 0x0

    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getSmallInt([BI)I

    move-result v1

    .line 504
    .local v1, "Y":I
    const v11, 0x8000

    and-int/2addr v11, v1

    const v12, 0x8000

    if-ne v11, v12, :cond_3

    .line 505
    xor-int/lit8 v11, v1, -0x1

    const v12, 0xffff

    and-int/2addr v11, v12

    add-int/lit8 v11, v11, 0x1

    mul-int/lit8 v1, v11, -0x1

    .line 508
    :cond_3
    add-int/2addr v8, v4

    .line 509
    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->arrayOfPoint:[Landroid/graphics/Point;

    new-instance v12, Landroid/graphics/Point;

    invoke-direct {v12, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    aput-object v12, v11, v5

    .line 511
    add-int/lit8 v5, v5, 0x1

    .line 512
    goto :goto_0

    .line 514
    .end local v0    # "X":I
    .end local v1    # "Y":I
    :cond_4
    const/4 v9, 0x0

    .line 515
    .local v9, "r":I
    const/4 v6, 0x0

    .local v6, "m":I
    :goto_3
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->numberofPolylines:I

    if-ge v6, v11, :cond_6

    .line 516
    const/4 v7, 0x0

    .local v7, "n":I
    :goto_4
    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->polylineCountArray:[I

    aget v11, v11, v6

    if-ge v7, v11, :cond_5

    .line 517
    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->twoDArrayPoints:[[Landroid/graphics/Point;

    aget-object v11, v11, v6

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->arrayOfPoint:[Landroid/graphics/Point;

    add-int/lit8 v10, v9, 0x1

    .end local v9    # "r":I
    .local v10, "r":I
    aget-object v12, v12, v9

    aput-object v12, v11, v7

    .line 516
    add-int/lit8 v7, v7, 0x1

    move v9, v10

    .end local v10    # "r":I
    .restart local v9    # "r":I
    goto :goto_4

    .line 515
    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 519
    .end local v7    # "n":I
    :cond_6
    return-void
.end method

.method public setPolylinePointCount([B)V
    .locals 0
    .param p1, "polylinePontCount"    # [B

    .prologue
    .line 263
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->polylinePointCount:[B

    .line 264
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "RecrdSize"    # I

    .prologue
    .line 120
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->RecordSize:I

    .line 121
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "RecrdType"    # I

    .prologue
    .line 101
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->RecordType:I

    .line 102
    return-void
.end method

.method public setRight(I)V
    .locals 0
    .param p1, "r"    # I

    .prologue
    .line 215
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->right:I

    .line 216
    return-void
.end method

.method public setTop(I)V
    .locals 0
    .param p1, "t"    # I

    .prologue
    .line 196
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->top:I

    .line 197
    return-void
.end method

.method public setaPoints([B)V
    .locals 0
    .param p1, "pt"    # [B

    .prologue
    .line 244
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolyPolyline16;->aPoints:[B

    .line 245
    return-void
.end method
