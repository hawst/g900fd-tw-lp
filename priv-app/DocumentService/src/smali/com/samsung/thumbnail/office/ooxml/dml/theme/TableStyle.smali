.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStyle;
.super Ljava/lang/Object;
.source "TableStyle.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    return-void
.end method


# virtual methods
.method public parseTableStyles(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "inputStream"    # Ljava/io/InputStream;

    .prologue
    .line 22
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStylesParser;

    invoke-direct {v0, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStylesParser;-><init>(Ljava/lang/Object;)V

    .line 23
    .local v0, "tableStylesParser":Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStylesParser;
    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStylesParser;->parse(Ljava/io/InputStream;)V

    .line 24
    return-void
.end method
