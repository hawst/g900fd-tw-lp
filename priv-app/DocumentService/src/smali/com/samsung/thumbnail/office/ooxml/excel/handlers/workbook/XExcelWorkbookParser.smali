.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelWorkbookParser;
.super Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;
.source "XExcelWorkbookParser.java"


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;)V
    .locals 0
    .param p1, "workbookobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;)V

    .line 19
    return-void
.end method


# virtual methods
.method public createRootElementHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelWorkbookHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelWorkbookHandler;-><init>()V

    return-object v0
.end method

.method public getMainNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0xc8

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelWorkbookParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v0

    return-object v0
.end method
