.class public Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;
.super Ljava/lang/Object;
.source "EMR_SetStretchBltMode.java"


# instance fields
.field private Size:I

.field private StretchMode:I

.field private Type:I

.field fileContent:[B

.field private offset:I


# direct methods
.method public constructor <init>([BI)V
    .locals 0
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->fileContent:[B

    .line 20
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->offset:I

    .line 21
    return-void
.end method


# virtual methods
.method public getRecordSize()I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->Size:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->Type:I

    return v0
.end method

.method public getStretchMode()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->StretchMode:I

    return v0
.end method

.method public readSetStretchBltMode()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x0

    .line 48
    const/4 v3, 0x0

    .line 50
    .local v3, "i":I
    const/4 v5, 0x4

    new-array v4, v5, [B

    .line 52
    .local v4, "intConvert":[B
    const/4 v3, 0x0

    :goto_0
    if-gt v3, v9, :cond_0

    .line 53
    rsub-int/lit8 v5, v3, 0x3

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->fileContent:[B

    iget v7, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->offset:I

    add-int/2addr v7, v3

    aget-byte v6, v6, v7

    aput-byte v6, v4, v5

    .line 52
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 54
    :cond_0
    invoke-static {v4, v8}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v2

    .line 55
    .local v2, "Type":I
    iget v5, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->offset:I

    add-int/2addr v5, v3

    iput v5, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->offset:I

    .line 56
    invoke-virtual {p0, v2}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->setRecordType(I)V

    .line 58
    const/4 v3, 0x0

    :goto_1
    if-gt v3, v9, :cond_1

    .line 59
    rsub-int/lit8 v5, v3, 0x3

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->fileContent:[B

    iget v7, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->offset:I

    add-int/2addr v7, v3

    aget-byte v6, v6, v7

    aput-byte v6, v4, v5

    .line 58
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 60
    :cond_1
    invoke-static {v4, v8}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 61
    .local v0, "Size":I
    iget v5, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->offset:I

    add-int/2addr v5, v3

    iput v5, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->offset:I

    .line 62
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->setRecordSize(I)V

    .line 64
    const/4 v3, 0x0

    :goto_2
    if-gt v3, v9, :cond_2

    .line 65
    rsub-int/lit8 v5, v3, 0x3

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->fileContent:[B

    iget v7, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->offset:I

    add-int/2addr v7, v3

    aget-byte v6, v6, v7

    aput-byte v6, v4, v5

    .line 64
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 66
    :cond_2
    invoke-static {v4, v8}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 67
    .local v1, "StretchMode":I
    iget v5, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->offset:I

    add-int/2addr v5, v3

    iput v5, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->offset:I

    .line 68
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->setStretchMode(I)V

    .line 70
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "RecrdSize"    # I

    .prologue
    .line 36
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->Size:I

    .line 37
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "RecrdType"    # I

    .prologue
    .line 28
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->Type:I

    .line 29
    return-void
.end method

.method public setStretchMode(I)V
    .locals 0
    .param p1, "StrtchMode"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetStretchBltMode;->StretchMode:I

    .line 45
    return-void
.end method
