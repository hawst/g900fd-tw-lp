.class public Lcom/samsung/thumbnail/office/ooxml/OOXMLMainNSHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "OOXMLMainNSHandler.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "elementName"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 15
    return-void
.end method


# virtual methods
.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 20
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getMainNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v2

    invoke-static {p2, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)Ljava/lang/String;

    move-result-object v1

    .line 23
    .local v1, "elementName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLMainNSHandler;->handlerMap:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    .line 26
    .local v0, "eleHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    if-eqz v0, :cond_0

    .line 27
    invoke-virtual {p1, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->pushHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 28
    invoke-virtual {v0, p1, v1, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 32
    :goto_0
    return-void

    .line 30
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->startUnknownElement()V

    goto :goto_0
.end method
