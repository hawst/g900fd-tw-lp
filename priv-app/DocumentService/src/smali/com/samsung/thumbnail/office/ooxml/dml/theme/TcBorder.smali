.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;
.super Ljava/lang/Object;
.source "TcBorder.java"


# instance fields
.field brdrLineProps:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;->brdrLineProps:Ljava/util/HashMap;

    .line 11
    return-void
.end method


# virtual methods
.method public addBrdrLineProps(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V
    .locals 1
    .param p1, "element"    # Ljava/lang/String;
    .param p2, "lineProps"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;->brdrLineProps:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    return-void
.end method

.method public getBrdrLineProps()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;->brdrLineProps:Ljava/util/HashMap;

    return-object v0
.end method
