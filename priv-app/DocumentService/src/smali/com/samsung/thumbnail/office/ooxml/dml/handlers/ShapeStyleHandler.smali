.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapeStyleHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "ShapeStyleHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FontRefHandler$IFontPropsConsumer;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ILineChoiceConsumer;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapeStyleHandler$ShapeStyleConsumer;
    }
.end annotation


# instance fields
.field private shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;


# direct methods
.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 8
    .param p1, "ID"    # I
    .param p2, "shInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 31
    const-string/jumbo v7, "style"

    invoke-direct {p0, p1, v7}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 33
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapeStyleHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 35
    const/4 v7, 0x3

    new-array v6, v7, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 38
    .local v6, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler;

    invoke-direct {v4, p0, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ILineChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 39
    .local v4, "lineRefHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v7, "lnRef"

    invoke-direct {v5, v7, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 41
    .local v5, "lineRefSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v7, 0x0

    aput-object v5, v6, v7

    .line 43
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler;

    invoke-direct {v0, p0, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 44
    .local v0, "fillRefHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v7, "fillRef"

    invoke-direct {v1, v7, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 46
    .local v1, "fillRefSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v7, 0x1

    aput-object v1, v6, v7

    .line 48
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FontRefHandler;

    invoke-direct {v2, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FontRefHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FontRefHandler$IFontPropsConsumer;)V

    .line 49
    .local v2, "fontRefHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FontRefHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v7, "fontRef"

    invoke-direct {v3, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 51
    .local v3, "fontRefSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v7, 0x2

    aput-object v3, v6, v7

    .line 53
    iput-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapeStyleHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 54
    return-void
.end method


# virtual methods
.method public consumeColor(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p1, "clrScheme"    # Ljava/lang/String;
    .param p2, "val"    # Ljava/lang/String;
    .param p3, "fillProperties"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    .param p4, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapeStyleHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setFontRefColor(Ljava/lang/String;)V

    .line 134
    return-void
.end method

.method public consumeFill(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 2
    .param p1, "fillProperties"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapeStyleHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getFillRefColor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setFillRefColor(Ljava/lang/String;)V

    .line 109
    return-void
.end method

.method public consumeLine(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V
    .locals 2
    .param p1, "lineProps"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapeStyleHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getColor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setLineRefColor(Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapeStyleHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    move-result-object v0

    if-nez v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapeStyleHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setLineProps(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V

    .line 119
    :cond_0
    return-void
.end method

.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 60
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    if-eqz v1, :cond_0

    .line 62
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getFillRefid()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 63
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getFillRefid()Ljava/lang/String;

    move-result-object v0

    .line 65
    .local v0, "fillRefId":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapeStyleHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setFillRefId(Ljava/lang/String;)V

    .line 69
    .end local v0    # "fillRefId":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public handleFontRef(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 127
    return-void
.end method
