.class public Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;
.super Ljava/lang/Object;
.source "EMR_SetTextAlign.java"


# instance fields
.field private RecordSize:I

.field private RecordType:I

.field private TextAlignmentMode:I

.field fileContent:[B

.field private offset:I


# direct methods
.method public constructor <init>([BI)V
    .locals 0
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->fileContent:[B

    .line 16
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->offset:I

    .line 17
    return-void
.end method


# virtual methods
.method public getRecordSize()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->RecordSize:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->RecordType:I

    return v0
.end method

.method public getTextAlignmentMode()I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->TextAlignmentMode:I

    return v0
.end method

.method public readEmfSetTextAlign()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x0

    .line 44
    const/4 v3, 0x0

    .line 46
    .local v3, "i":I
    const/4 v5, 0x4

    new-array v4, v5, [B

    .line 48
    .local v4, "intConvert":[B
    const/4 v3, 0x0

    :goto_0
    if-gt v3, v9, :cond_0

    .line 49
    rsub-int/lit8 v5, v3, 0x3

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->fileContent:[B

    iget v7, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->offset:I

    add-int/2addr v7, v3

    aget-byte v6, v6, v7

    aput-byte v6, v4, v5

    .line 48
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 50
    :cond_0
    invoke-static {v4, v8}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v2

    .line 51
    .local v2, "Type":I
    iget v5, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->offset:I

    add-int/2addr v5, v3

    iput v5, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->offset:I

    .line 52
    invoke-virtual {p0, v2}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->setRecordType(I)V

    .line 54
    const/4 v3, 0x0

    :goto_1
    if-gt v3, v9, :cond_1

    .line 55
    rsub-int/lit8 v5, v3, 0x3

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->fileContent:[B

    iget v7, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->offset:I

    add-int/2addr v7, v3

    aget-byte v6, v6, v7

    aput-byte v6, v4, v5

    .line 54
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 56
    :cond_1
    invoke-static {v4, v8}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 57
    .local v0, "Size":I
    iget v5, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->offset:I

    add-int/2addr v5, v3

    iput v5, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->offset:I

    .line 58
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->setRecordSize(I)V

    .line 60
    const/4 v3, 0x0

    :goto_2
    if-gt v3, v9, :cond_2

    .line 61
    rsub-int/lit8 v5, v3, 0x3

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->fileContent:[B

    iget v7, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->offset:I

    add-int/2addr v7, v3

    aget-byte v6, v6, v7

    aput-byte v6, v4, v5

    .line 60
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 62
    :cond_2
    invoke-static {v4, v8}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 63
    .local v1, "TextAlgnmentMode":I
    iget v5, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->offset:I

    add-int/2addr v5, v3

    iput v5, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->offset:I

    .line 64
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->setTextAlignmentMode(I)V

    .line 65
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "RecrdSize"    # I

    .prologue
    .line 32
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->RecordSize:I

    .line 33
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "RecrdType"    # I

    .prologue
    .line 24
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->RecordType:I

    .line 25
    return-void
.end method

.method public setTextAlignmentMode(I)V
    .locals 0
    .param p1, "TextAlgnmentMode"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetTextAlign;->TextAlignmentMode:I

    .line 41
    return-void
.end method
