.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStyleHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLStrictSequenceHandler;
.source "TableStyleHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler$IPartTableStyleConsumer;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableBackgroundHandler$ITableBackgroundConsumer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStyleHandler$ITableStyleConsumer;
    }
.end annotation


# instance fields
.field private styleID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStyleHandler$ITableStyleConsumer;)V
    .locals 2
    .param p1, "tblStyleConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStyleHandler$ITableStyleConsumer;

    .prologue
    .line 27
    const/16 v0, 0x1f

    const-string/jumbo v1, "tblStyle"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStrictSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 30
    return-void
.end method

.method private init()V
    .locals 25

    .prologue
    .line 33
    const/16 v24, 0xa

    move/from16 v0, v24

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    move-object/from16 v19, v0

    .line 38
    .local v19, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v23, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;

    const-string/jumbo v24, "wholeTbl"

    invoke-direct/range {v23 .. v25}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler$IPartTableStyleConsumer;)V

    .line 40
    .local v23, "whlTblHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;
    new-instance v22, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v24, "wholeTbl"

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 42
    .local v22, "whlSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v24, 0x0

    aput-object v22, v19, v24

    .line 44
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;

    const-string/jumbo v24, "band1H"

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v3, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler$IPartTableStyleConsumer;)V

    .line 46
    .local v3, "band1HHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v24, "band1H"

    move-object/from16 v0, v24

    invoke-direct {v4, v0, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 48
    .local v4, "band1HSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v24, 0x1

    aput-object v4, v19, v24

    .line 50
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;

    const-string/jumbo v24, "band2H"

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v7, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler$IPartTableStyleConsumer;)V

    .line 52
    .local v7, "band2HHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v24, "band2H"

    move-object/from16 v0, v24

    invoke-direct {v8, v0, v7}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 54
    .local v8, "band2HSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v24, 0x2

    aput-object v8, v19, v24

    .line 56
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;

    const-string/jumbo v24, "band1V"

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v5, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler$IPartTableStyleConsumer;)V

    .line 58
    .local v5, "band1VHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v24, "band1V"

    move-object/from16 v0, v24

    invoke-direct {v6, v0, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 60
    .local v6, "band1VSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v24, 0x3

    aput-object v6, v19, v24

    .line 62
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;

    const-string/jumbo v24, "band2V"

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v9, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler$IPartTableStyleConsumer;)V

    .line 64
    .local v9, "band2VHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v24, "band2V"

    move-object/from16 v0, v24

    invoke-direct {v10, v0, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 66
    .local v10, "band2VSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v24, 0x4

    aput-object v10, v19, v24

    .line 68
    new-instance v15, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;

    const-string/jumbo v24, "lastCol"

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v15, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler$IPartTableStyleConsumer;)V

    .line 70
    .local v15, "lstColHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;
    new-instance v16, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v24, "lastCol"

    move-object/from16 v0, v16

    move-object/from16 v1, v24

    invoke-direct {v0, v1, v15}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 72
    .local v16, "lstColSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v24, 0x5

    aput-object v16, v19, v24

    .line 74
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;

    const-string/jumbo v24, "firstCol"

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v11, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler$IPartTableStyleConsumer;)V

    .line 76
    .local v11, "fstColHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v24, "firstCol"

    move-object/from16 v0, v24

    invoke-direct {v12, v0, v11}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 78
    .local v12, "fstColSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v24, 0x6

    aput-object v12, v19, v24

    .line 80
    new-instance v17, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;

    const-string/jumbo v24, "lastRow"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    move-object/from16 v2, p0

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler$IPartTableStyleConsumer;)V

    .line 82
    .local v17, "lstRowHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v24, "lastRow"

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 84
    .local v18, "lstRwSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v24, 0x7

    aput-object v18, v19, v24

    .line 86
    new-instance v13, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;

    const-string/jumbo v24, "firstRow"

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v13, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler$IPartTableStyleConsumer;)V

    .line 88
    .local v13, "fstRowHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;
    new-instance v14, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v24, "firstRow"

    move-object/from16 v0, v24

    invoke-direct {v14, v0, v13}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 90
    .local v14, "fstRwSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v24, 0x8

    aput-object v14, v19, v24

    .line 92
    new-instance v20, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableBackgroundHandler;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableBackgroundHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableBackgroundHandler$ITableBackgroundConsumer;)V

    .line 94
    .local v20, "tblBackgroundHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableBackgroundHandler;
    new-instance v21, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v24, "tblBg"

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 96
    .local v21, "tblBgHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v24, 0x9

    aput-object v21, v19, v24

    .line 98
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 99
    return-void
.end method


# virtual methods
.method public handlePartStyle(Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 2
    .param p1, "tblPartStyle"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;
    .param p2, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 120
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStyleHandler;->styleID:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->addTblFormatList(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;)V

    .line 123
    return-void
.end method

.method public handleTableBackground(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "tblBgColor"    # Ljava/lang/String;

    .prologue
    .line 128
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStyleHandler;->styleID:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->addTblBgColor(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 104
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStrictSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 106
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p3}, Lorg/xml/sax/Attributes;->getLength()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 107
    invoke-interface {p3, v0}, Lorg/xml/sax/Attributes;->getQName(I)Ljava/lang/String;

    move-result-object v1

    .line 108
    .local v1, "qVal":Ljava/lang/String;
    const-string/jumbo v2, "styleId"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 109
    invoke-interface {p3, v0}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStyleHandler;->styleID:Ljava/lang/String;

    .line 106
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 113
    .end local v1    # "qVal":Ljava/lang/String;
    :cond_1
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStyleHandler;->init()V

    .line 114
    return-void
.end method
