.class public Lcom/samsung/thumbnail/office/ooxml/contenttype/CTOverride;
.super Ljava/lang/Object;
.source "CTOverride.java"


# instance fields
.field private ContentType:Ljava/lang/String;

.field private PartName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTOverride;->ContentType:Ljava/lang/String;

    return-object v0
.end method

.method public getPartName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTOverride;->PartName:Ljava/lang/String;

    return-object v0
.end method

.method public setContentType(Ljava/lang/String;)V
    .locals 0
    .param p1, "ContentType"    # Ljava/lang/String;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTOverride;->ContentType:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public setPartName(Ljava/lang/String;)V
    .locals 0
    .param p1, "PartName"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTOverride;->PartName:Ljava/lang/String;

    .line 30
    return-void
.end method
