.class public Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelDMExtHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DiagramDataModelDMExtHandler.java"


# instance fields
.field dataModel:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;


# direct methods
.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;)V
    .locals 1
    .param p1, "nsID"    # I
    .param p2, "dataModel"    # Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

    .prologue
    .line 20
    const-string/jumbo v0, "dataModelExt"

    invoke-direct {p0, p1, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 21
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelDMExtHandler;->dataModel:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

    .line 22
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 45
    return-void
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 39
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 40
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 27
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 29
    const-string/jumbo v1, "relId"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelDMExtHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 31
    .local v0, "attr":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelDMExtHandler;->dataModel:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

    if-eqz v1, :cond_0

    .line 32
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelDMExtHandler;->dataModel:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;->setDiagramRelId(Ljava/lang/String;)V

    .line 34
    :cond_0
    return-void
.end method
