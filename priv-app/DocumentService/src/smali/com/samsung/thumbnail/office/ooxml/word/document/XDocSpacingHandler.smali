.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSpacingHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLValueHandler;
.source "XDocSpacingHandler.java"


# instance fields
.field private propery:Lcom/samsung/thumbnail/office/ooxml/word/properties/IXDocPropery;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/properties/IXDocPropery;)V
    .locals 1
    .param p1, "propery"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/IXDocPropery;

    .prologue
    .line 18
    const-string/jumbo v0, "spacing"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLValueHandler;-><init>(Ljava/lang/String;)V

    .line 19
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSpacingHandler;->propery:Lcom/samsung/thumbnail/office/ooxml/word/properties/IXDocPropery;

    .line 20
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 7
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 25
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    invoke-direct {v4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;-><init>()V

    .line 26
    .local v4, "spaceProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;
    const/4 v1, -0x1

    .local v1, "before":I
    const/4 v0, -0x1

    .local v0, "after":I
    const/4 v2, -0x1

    .line 27
    .local v2, "line":I
    const/4 v3, 0x0

    .line 29
    .local v3, "lineRule":Ljava/lang/String;
    const-string/jumbo v6, "before"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSpacingHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v5

    .line 31
    .local v5, "value":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 32
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 35
    :cond_0
    const-string/jumbo v6, "after"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSpacingHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v5

    .line 36
    if-eqz v5, :cond_1

    .line 37
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 40
    :cond_1
    const-string/jumbo v6, "line"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSpacingHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v5

    .line 41
    if-eqz v5, :cond_2

    .line 42
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 45
    :cond_2
    const-string/jumbo v6, "lineRule"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSpacingHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v5

    .line 46
    if-eqz v5, :cond_3

    .line 47
    move-object v3, v5

    .line 49
    :cond_3
    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->setSpacingProp(IIILjava/lang/String;)V

    .line 51
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSpacingHandler;->propery:Lcom/samsung/thumbnail/office/ooxml/word/properties/IXDocPropery;

    invoke-interface {v6, v4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/IXDocPropery;->consumePropery(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty;)V

    .line 52
    return-void
.end method
