.class public Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;
.super Lorg/apache/poi/POIXMLDocumentPart;
.source "XWPFDiagram.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "XWPFDiagram"


# instance fields
.field private relArray:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;",
            ">;"
        }
    .end annotation
.end field

.field private shapeCount:I

.field private sheet:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

.field private xslfShapeAL:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;",
            ">;"
        }
    .end annotation
.end field

.field private xwpfShapeAL:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lorg/apache/poi/POIXMLDocumentPart;-><init>()V

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->xwpfShapeAL:Ljava/util/ArrayList;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->xslfShapeAL:Ljava/util/ArrayList;

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->shapeCount:I

    .line 37
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V
    .locals 1
    .param p1, "part"    # Lorg/apache/poi/openxml4j/opc/PackagePart;
    .param p2, "rel"    # Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/POIXMLDocumentPart;-><init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->xwpfShapeAL:Ljava/util/ArrayList;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->xslfShapeAL:Ljava/util/ArrayList;

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->shapeCount:I

    .line 41
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getRelArray()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->relArray:Ljava/util/List;

    .line 42
    return-void
.end method


# virtual methods
.method public addDiagramShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;)V
    .locals 1
    .param p1, "xslfShape"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->xslfShapeAL:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    return-void
.end method

.method public addDiagramShape(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V
    .locals 1
    .param p1, "xwpfShape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->xwpfShapeAL:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    return-void
.end method

.method public getDiagramShapeAL()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->xwpfShapeAL:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getRelList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->relArray:Ljava/util/List;

    return-object v0
.end method

.method public getShapeCount()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->shapeCount:I

    return v0
.end method

.method public getSheet()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->sheet:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    return-object v0
.end method

.method public getXSLFShapeAL()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->xslfShapeAL:Ljava/util/ArrayList;

    return-object v0
.end method

.method public parseDiagram(Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V
    .locals 7
    .param p1, "dataModel"    # Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;
    .param p2, "theme"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    .param p3, "docType"    # Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    .prologue
    .line 70
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/diagram/XDocDiagramParser;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/diagram/XDocDiagramParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 72
    .local v0, "diagramParser":Lcom/samsung/thumbnail/office/ooxml/diagram/XDocDiagramParser;
    const/4 v2, 0x0

    .line 75
    .local v2, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 76
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getRelArray()Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->relArray:Ljava/util/List;

    .line 77
    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/diagram/XDocDiagramParser;->parse(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    if-eqz v2, :cond_0

    .line 84
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 85
    :catch_0
    move-exception v1

    .line 86
    .local v1, "e":Ljava/io/IOException;
    const-string/jumbo v3, "XWPFDiagram"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "IOException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 79
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 80
    .restart local v1    # "e":Ljava/io/IOException;
    :try_start_2
    const-string/jumbo v3, "XWPFDiagram"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 82
    if-eqz v2, :cond_0

    .line 84
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 85
    :catch_2
    move-exception v1

    .line 86
    const-string/jumbo v3, "XWPFDiagram"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "IOException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 82
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_1

    .line 84
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 87
    :cond_1
    :goto_1
    throw v3

    .line 85
    :catch_3
    move-exception v1

    .line 86
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v4, "XWPFDiagram"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setShapeCount(I)V
    .locals 0
    .param p1, "shapeCount"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->shapeCount:I

    .line 62
    return-void
.end method

.method public setSheet(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;)V
    .locals 0
    .param p1, "sheet"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->sheet:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    .line 102
    return-void
.end method
