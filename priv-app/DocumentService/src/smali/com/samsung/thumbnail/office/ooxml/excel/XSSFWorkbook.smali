.class public Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;
.super Lorg/apache/poi/POIXMLDocument;
.source "XSSFWorkbook.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/util/IProgressUpdate;


# static fields
.field public static final DEFAULT_CHARACTER_WIDTH:F = 7.0017f

.field private static final TAG:Ljava/lang/String; = "XSSFWorkbook"

.field public static USE_CUSTOMVIEW:Z

.field private static final formatCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/poi/ss/format/CellFormat;",
            ">;"
        }
    .end annotation
.end field

.field public static uRIPREFIX:Ljava/lang/String;


# instance fields
.field private _missingCellPolicy:Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

.field public context:Landroid/content/Context;

.field excelVariables:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

.field private file:Ljava/io/File;

.field public formatPercent:Ljava/lang/String;

.field private isThumbnailPreview:Z

.field private mViewConsumer:Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;

.field private pictures:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;",
            ">;"
        }
    .end annotation
.end field

.field private sharedstring:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

.field private stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

.field private themeobj:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

.field private worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->USE_CUSTOMVIEW:Z

    .line 94
    const-string/jumbo v0, ""

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->uRIPREFIX:Ljava/lang/String;

    .line 586
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->formatCache:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 228
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->newPackage()Lorg/apache/poi/openxml4j/opc/OPCPackage;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/poi/POIXMLDocument;-><init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;)V

    .line 97
    const-string/jumbo v0, "0%"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->formatPercent:Ljava/lang/String;

    .line 130
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->mViewConsumer:Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;

    .line 133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->isThumbnailPreview:Z

    .line 207
    sget-object v0, Lorg/apache/poi/ss/usermodel/Row;->RETURN_NULL_AND_BLANK:Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->_missingCellPolicy:Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    .line 229
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->onWorkbookCreate()V

    .line 230
    return-void
.end method

.method public constructor <init>(Ljava/io/FileInputStream;Ljava/io/File;Landroid/content/Context;Landroid/os/Handler;Lcom/samsung/thumbnail/customview/word/CustomView;Z)V
    .locals 6
    .param p1, "fis"    # Ljava/io/FileInputStream;
    .param p2, "file"    # Ljava/io/File;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "mHandler"    # Landroid/os/Handler;
    .param p5, "customView"    # Lcom/samsung/thumbnail/customview/word/CustomView;
    .param p6, "isThumbnail"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 247
    invoke-static {p1}, Lorg/apache/poi/util/PackageHelper;->open(Ljava/io/InputStream;)Lorg/apache/poi/openxml4j/opc/OPCPackage;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/poi/POIXMLDocument;-><init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;)V

    .line 97
    const-string/jumbo v0, "0%"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->formatPercent:Ljava/lang/String;

    .line 130
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->mViewConsumer:Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;

    .line 133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->isThumbnailPreview:Z

    .line 207
    sget-object v0, Lorg/apache/poi/ss/usermodel/Row;->RETURN_NULL_AND_BLANK:Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->_missingCellPolicy:Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    .line 248
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->context:Landroid/content/Context;

    .line 249
    iput-boolean p6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->isThumbnailPreview:Z

    .line 250
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->file:Ljava/io/File;

    .line 252
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->excelVariables:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    .line 253
    new-instance v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;

    iget-boolean v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->isThumbnailPreview:Z

    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->excelVariables:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-object v1, p0

    move-object v2, p5

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;-><init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;Lcom/samsung/thumbnail/customview/word/CustomView;Landroid/content/Context;ZLcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;)V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->mViewConsumer:Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;

    .line 255
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Landroid/content/Context;Landroid/os/Handler;)V
    .locals 1
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "mHandler"    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 275
    invoke-static {p1}, Lorg/apache/poi/util/PackageHelper;->open(Ljava/io/InputStream;)Lorg/apache/poi/openxml4j/opc/OPCPackage;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/poi/POIXMLDocument;-><init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;)V

    .line 97
    const-string/jumbo v0, "0%"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->formatPercent:Ljava/lang/String;

    .line 130
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->mViewConsumer:Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;

    .line 133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->isThumbnailPreview:Z

    .line 207
    sget-object v0, Lorg/apache/poi/ss/usermodel/Row;->RETURN_NULL_AND_BLANK:Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->_missingCellPolicy:Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    .line 276
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->context:Landroid/content/Context;

    .line 277
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFactory;->getInstance()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFactory;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->load(Lorg/apache/poi/POIXMLFactory;)V

    .line 278
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 287
    invoke-static {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->openPackage(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/OPCPackage;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;-><init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;)V

    .line 288
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;)V
    .locals 1
    .param p1, "pkg"    # Lorg/apache/poi/openxml4j/opc/OPCPackage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 240
    invoke-direct {p0, p1}, Lorg/apache/poi/POIXMLDocument;-><init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;)V

    .line 97
    const-string/jumbo v0, "0%"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->formatPercent:Ljava/lang/String;

    .line 130
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->mViewConsumer:Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;

    .line 133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->isThumbnailPreview:Z

    .line 207
    sget-object v0, Lorg/apache/poi/ss/usermodel/Row;->RETURN_NULL_AND_BLANK:Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->_missingCellPolicy:Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    .line 241
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFactory;->getInstance()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFactory;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->load(Lorg/apache/poi/POIXMLFactory;)V

    .line 242
    return-void
.end method

.method static alpha()[Ljava/lang/String;
    .locals 7

    .prologue
    .line 733
    const/16 v5, 0x2bf

    new-array v4, v5, [Ljava/lang/String;

    .line 734
    .local v4, "result":[Ljava/lang/String;
    const/4 v5, 0x0

    const-string/jumbo v6, ""

    aput-object v6, v4, v5

    .line 735
    const/4 v3, 0x0

    .local v3, "j":I
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_0
    const/16 v5, 0x2bd

    if-gt v3, v5, :cond_1

    .line 736
    rem-int/lit8 v5, v3, 0x1a

    add-int/lit8 v5, v5, 0x41

    int-to-char v0, v5

    .line 737
    .local v0, "a":C
    div-int/lit8 v5, v3, 0x1a

    add-int/lit8 v5, v5, 0x41

    add-int/lit8 v5, v5, -0x1

    int-to-char v1, v5

    .line 739
    .local v1, "b":C
    div-int/lit8 v5, v3, 0x1a

    const/4 v6, 0x1

    if-lt v5, v6, :cond_0

    .line 740
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    .line 735
    :goto_1
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 742
    :cond_0
    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    goto :goto_1

    .line 745
    .end local v0    # "a":C
    .end local v1    # "b":C
    :cond_1
    return-object v4
.end method

.method private createCanvasForm()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 296
    const/4 v5, 0x0

    .line 297
    .local v5, "is":Ljava/io/InputStream;
    const/4 v6, 0x0

    .line 298
    .local v6, "mainDoc":Ljava/io/InputStream;
    const/4 v9, 0x0

    .line 301
    .local v9, "sharedStringPart":Lorg/apache/poi/openxml4j/opc/PackagePart;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v11

    invoke-virtual {v11}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    .line 302
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;

    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->context:Landroid/content/Context;

    invoke-direct {v11, v6, v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;-><init>(Ljava/io/InputStream;Landroid/content/Context;)V

    .line 305
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v11

    invoke-virtual {v11}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getEntryName()Ljava/lang/String;

    move-result-object v3

    .line 306
    .local v3, "entryName":Ljava/lang/String;
    const-string/jumbo v11, "xl/workbook.xml"

    invoke-virtual {v3, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 307
    new-instance v11, Ljava/lang/RuntimeException;

    const-string/jumbo v12, "Not a valid Excel file"

    invoke-direct {v11, v12}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 396
    .end local v3    # "entryName":Ljava/lang/String;
    :catchall_0
    move-exception v11

    if-eqz v5, :cond_0

    .line 398
    :try_start_1
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 404
    :cond_0
    :goto_0
    if-eqz v6, :cond_1

    .line 406
    :try_start_2
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 409
    :cond_1
    :goto_1
    throw v11

    .line 310
    .restart local v3    # "entryName":Ljava/lang/String;
    :cond_2
    :try_start_3
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getRelations()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/poi/POIXMLDocumentPart;

    .line 311
    .local v7, "p":Lorg/apache/poi/POIXMLDocumentPart;
    instance-of v11, v7, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    if-eqz v11, :cond_4

    .line 312
    move-object v0, v7

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    move-object v10, v0

    .line 313
    .local v10, "sheet":Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;
    iget-object v11, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    if-nez v11, :cond_4

    .line 314
    invoke-virtual {v7}, Lorg/apache/poi/POIXMLDocumentPart;->getPackageRelationship()Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v11

    invoke-virtual {v11}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getId()Ljava/lang/String;

    move-result-object v8

    .line 315
    .local v8, "relId":Ljava/lang/String;
    invoke-virtual {v7}, Lorg/apache/poi/POIXMLDocumentPart;->getPackageRelationship()Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v11

    invoke-virtual {v11}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getId()Ljava/lang/String;

    move-result-object v1

    .line 317
    .local v1, "actualId":Ljava/lang/String;
    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 320
    iget-object v11, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->mViewConsumer:Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;

    invoke-virtual {v11}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getCanvas()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v11

    invoke-virtual {v11}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setShowOnlyFirstSlide(Z)V

    .line 323
    iput-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    .line 324
    iget-object v11, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->file:Ljava/io/File;

    invoke-virtual {v11, v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->setFileName(Ljava/io/File;)V

    .line 325
    iget-object v11, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->context:Landroid/content/Context;

    invoke-virtual {v11, v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->setContext(Landroid/content/Context;)V

    .line 326
    iget-object v11, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->excelVariables:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    invoke-virtual {v11, v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->setExcelVariable(Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;)V

    .line 327
    iget-object v11, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->mViewConsumer:Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getCanvas()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->setElementCreator(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 329
    iget-object v11, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->mViewConsumer:Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getFolderName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->setFolderName(Ljava/lang/String;)V

    .line 331
    iget-object v11, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->onDocumentRead()V

    .line 335
    .end local v1    # "actualId":Ljava/lang/String;
    .end local v8    # "relId":Ljava/lang/String;
    .end local v10    # "sheet":Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;
    :cond_4
    invoke-virtual {v7}, Lorg/apache/poi/POIXMLDocumentPart;->getPackageRelationship()Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v11

    invoke-virtual {v11}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getRelationshipType()Ljava/lang/String;

    move-result-object v11

    const-string/jumbo v12, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/sharedStrings"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 338
    iget-object v11, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    if-eqz v11, :cond_7

    .line 339
    invoke-virtual {v7}, Lorg/apache/poi/POIXMLDocumentPart;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->processShareStringFile(Lorg/apache/poi/openxml4j/opc/PackagePart;)V

    .line 352
    :cond_5
    :goto_3
    instance-of v11, v7, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    if-eqz v11, :cond_6

    .line 353
    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .end local v7    # "p":Lorg/apache/poi/POIXMLDocumentPart;
    iput-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->themeobj:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .line 354
    iget-object v11, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->themeobj:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->parseTheme()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 358
    :cond_6
    if-eqz v5, :cond_3

    .line 360
    :try_start_4
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 361
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 341
    .restart local v7    # "p":Lorg/apache/poi/POIXMLDocumentPart;
    :cond_7
    :try_start_5
    invoke-virtual {v7}, Lorg/apache/poi/POIXMLDocumentPart;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v9

    goto :goto_3

    .line 343
    :cond_8
    invoke-virtual {v7}, Lorg/apache/poi/POIXMLDocumentPart;->getPackageRelationship()Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v11

    invoke-virtual {v11}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getRelationshipType()Ljava/lang/String;

    move-result-object v11

    const-string/jumbo v12, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 347
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->initializevariable()V

    .line 348
    invoke-virtual {v7}, Lorg/apache/poi/POIXMLDocumentPart;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v11

    invoke-virtual {v11}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 349
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->context:Landroid/content/Context;

    iget-object v13, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->excelVariables:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    invoke-direct {v11, v5, v12, v13}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;-><init>(Ljava/io/InputStream;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;)V

    iput-object v11, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    goto :goto_3

    .line 362
    .end local v7    # "p":Lorg/apache/poi/POIXMLDocumentPart;
    :catch_0
    move-exception v2

    .line 363
    .local v2, "e":Ljava/io/IOException;
    const-string/jumbo v11, "XSSFWorkbook"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "Exception: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 372
    .end local v2    # "e":Ljava/io/IOException;
    :cond_9
    if-eqz v9, :cond_a

    .line 373
    invoke-direct {p0, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->processShareStringFile(Lorg/apache/poi/openxml4j/opc/PackagePart;)V

    .line 378
    :cond_a
    iget-object v11, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->mViewConsumer:Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;

    invoke-virtual {v11}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getCanvas()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v11

    invoke-virtual {v11}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setShowOnlyFirstSlide(Z)V

    .line 386
    iget-object v11, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    if-eqz v11, :cond_b

    .line 387
    iget-object v11, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->mViewConsumer:Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;

    iget-object v12, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    iget-object v13, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->context:Landroid/content/Context;

    invoke-virtual {v11, v12, v13}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->processSheet(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;Landroid/content/Context;)V

    .line 392
    :cond_b
    iget-object v11, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->mViewConsumer:Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;

    invoke-virtual {v11}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getCustomView()Lcom/samsung/thumbnail/customview/word/CustomView;

    move-result-object v11

    if-eqz v11, :cond_c

    .line 393
    iget-object v11, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->mViewConsumer:Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;

    invoke-virtual {v11}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getCustomView()Lcom/samsung/thumbnail/customview/word/CustomView;

    move-result-object v11

    invoke-virtual {v11}, Lcom/samsung/thumbnail/customview/word/CustomView;->Load()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 396
    :cond_c
    if-eqz v5, :cond_d

    .line 398
    :try_start_6
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    .line 404
    :cond_d
    :goto_4
    if-eqz v6, :cond_e

    .line 406
    :try_start_7
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    .line 412
    :cond_e
    :goto_5
    return-void

    .line 399
    :catch_1
    move-exception v2

    .line 400
    .restart local v2    # "e":Ljava/io/IOException;
    const-string/jumbo v11, "XSSFWorkbook"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "Exception: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 407
    .end local v2    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v2

    .line 408
    .restart local v2    # "e":Ljava/io/IOException;
    const-string/jumbo v11, "XSSFWorkbook"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "Exception: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 399
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "entryName":Ljava/lang/String;
    .end local v4    # "i$":Ljava/util/Iterator;
    :catch_3
    move-exception v2

    .line 400
    .restart local v2    # "e":Ljava/io/IOException;
    const-string/jumbo v12, "XSSFWorkbook"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Exception: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 407
    .end local v2    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v2

    .line 408
    .restart local v2    # "e":Ljava/io/IOException;
    const-string/jumbo v12, "XSSFWorkbook"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Exception: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public static getInputStream(Ljava/lang/String;Ljava/io/File;)Ljava/io/InputStream;
    .locals 10
    .param p0, "entryName"    # Ljava/lang/String;
    .param p1, "fileName"    # Ljava/io/File;

    .prologue
    .line 1948
    const/4 v1, 0x0

    .line 1950
    .local v1, "inputStream":Ljava/io/InputStream;
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move-object v6, v1

    .line 1972
    :goto_0
    return-object v6

    .line 1954
    :cond_1
    const/4 v5, 0x0

    .line 1956
    .local v5, "zipInput":Ljava/util/zip/ZipInputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1958
    .end local v1    # "inputStream":Ljava/io/InputStream;
    .local v2, "inputStream":Ljava/io/InputStream;
    :try_start_1
    new-instance v6, Ljava/util/zip/ZipInputStream;

    invoke-direct {v6, v2}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1961
    .end local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    .local v6, "zipInput":Ljava/util/zip/ZipInputStream;
    :cond_2
    :try_start_2
    invoke-virtual {v6}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v4

    .local v4, "zipEntry":Ljava/util/zip/ZipEntry;
    if-eqz v4, :cond_3

    .line 1962
    invoke-virtual {v4}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1963
    .local v3, "name":Ljava/lang/String;
    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-result v7

    if-eqz v7, :cond_2

    move-object v1, v2

    .line 1964
    .end local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    goto :goto_0

    .end local v1    # "inputStream":Ljava/io/InputStream;
    .end local v3    # "name":Ljava/lang/String;
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    :cond_3
    move-object v5, v6

    .end local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    move-object v1, v2

    .end local v2    # "inputStream":Ljava/io/InputStream;
    .end local v4    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    :goto_1
    move-object v6, v5

    .line 1972
    goto :goto_0

    .line 1967
    :catch_0
    move-exception v0

    .line 1968
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_2
    const-string/jumbo v7, "XSSFWorkbook"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Exception: FNFE"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1969
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 1970
    .local v0, "e":Ljava/io/IOException;
    :goto_3
    const-string/jumbo v7, "XSSFWorkbook"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "IOException: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1969
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "inputStream":Ljava/io/InputStream;
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    :catch_2
    move-exception v0

    move-object v1, v2

    .end local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    goto :goto_3

    .end local v1    # "inputStream":Ljava/io/InputStream;
    .end local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    :catch_3
    move-exception v0

    move-object v5, v6

    .end local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    move-object v1, v2

    .end local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    goto :goto_3

    .line 1967
    .end local v1    # "inputStream":Ljava/io/InputStream;
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    goto :goto_2

    .end local v1    # "inputStream":Ljava/io/InputStream;
    .end local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    :catch_5
    move-exception v0

    move-object v5, v6

    .end local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    move-object v1, v2

    .end local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    goto :goto_2
.end method

.method protected static newPackage()Lorg/apache/poi/openxml4j/opc/OPCPackage;
    .locals 5

    .prologue
    .line 919
    :try_start_0
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {v3}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->create(Ljava/io/OutputStream;)Lorg/apache/poi/openxml4j/opc/OPCPackage;

    move-result-object v2

    .line 921
    .local v2, "pkg":Lorg/apache/poi/openxml4j/opc/OPCPackage;
    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->WORKBOOK:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->getDefaultFileName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/apache/poi/openxml4j/opc/PackagingURIHelper;->createPartName(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackagePartName;

    move-result-object v0

    .line 924
    .local v0, "corePartName":Lorg/apache/poi/openxml4j/opc/PackagePartName;
    sget-object v3, Lorg/apache/poi/openxml4j/opc/TargetMode;->INTERNAL:Lorg/apache/poi/openxml4j/opc/TargetMode;

    const-string/jumbo v4, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"

    invoke-virtual {v2, v0, v3, v4}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->addRelationship(Lorg/apache/poi/openxml4j/opc/PackagePartName;Lorg/apache/poi/openxml4j/opc/TargetMode;Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    .line 927
    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->WORKBOOK:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->getContentType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->createPart(Lorg/apache/poi/openxml4j/opc/PackagePartName;Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackagePart;

    .line 929
    invoke-virtual {v2}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->getPackageProperties()Lorg/apache/poi/openxml4j/opc/PackageProperties;

    move-result-object v3

    const-string/jumbo v4, "Apache POI"

    invoke-interface {v3, v4}, Lorg/apache/poi/openxml4j/opc/PackageProperties;->setCreatorProperty(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 931
    return-object v2

    .line 932
    .end local v0    # "corePartName":Lorg/apache/poi/openxml4j/opc/PackagePartName;
    :catch_0
    move-exception v1

    .line 933
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Lorg/apache/poi/POIXMLException;

    invoke-direct {v3, v1}, Lorg/apache/poi/POIXMLException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method

.method private onWorkbookCreate()V
    .locals 0

    .prologue
    .line 912
    return-void
.end method

.method private processShareStringFile(Lorg/apache/poi/openxml4j/opc/PackagePart;)V
    .locals 4
    .param p1, "part"    # Lorg/apache/poi/openxml4j/opc/PackagePart;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 415
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->initializesharedstringvar()V

    .line 416
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->excelVariables:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;-><init>(Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;)V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->sharedstring:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    .line 418
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->sharedstring:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->file:Ljava/io/File;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->setFileName(Ljava/io/File;)V

    .line 419
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->sharedstring:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->onSharedStringRead(Lorg/apache/poi/openxml4j/opc/PackagePart;)V

    .line 420
    return-void
.end method


# virtual methods
.method public addPicture(Ljava/io/InputStream;I)I
    .locals 6
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "format"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 999
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getAllPictures()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v0, v3, 0x1

    .line 1000
    .local v0, "imageNumber":I
    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;->RELATIONS:[Lorg/apache/poi/POIXMLRelation;

    aget-object v3, v3, p2

    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFactory;->getInstance()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFactory;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {p0, v3, v4, v0, v5}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->createRelationship(Lorg/apache/poi/POIXMLRelation;Lorg/apache/poi/POIXMLFactory;IZ)Lorg/apache/poi/POIXMLDocumentPart;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;

    .line 1003
    .local v1, "img":Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    .line 1004
    .local v2, "out":Ljava/io/OutputStream;
    invoke-static {p1, v2}, Lorg/apache/poi/util/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 1005
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    .line 1006
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->pictures:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1007
    add-int/lit8 v3, v0, -0x1

    return v3
.end method

.method public addPicture([BI)I
    .locals 7
    .param p1, "pictureData"    # [B
    .param p2, "format"    # I

    .prologue
    .line 965
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getAllPictures()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v1, v4, 0x1

    .line 966
    .local v1, "imageNumber":I
    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;->RELATIONS:[Lorg/apache/poi/POIXMLRelation;

    aget-object v4, v4, p2

    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFactory;->getInstance()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFactory;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {p0, v4, v5, v1, v6}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->createRelationship(Lorg/apache/poi/POIXMLRelation;Lorg/apache/poi/POIXMLFactory;IZ)Lorg/apache/poi/POIXMLDocumentPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;

    .line 970
    .local v2, "img":Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;
    :try_start_0
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    .line 971
    .local v3, "out":Ljava/io/OutputStream;
    invoke-virtual {v3, p1}, Ljava/io/OutputStream;->write([B)V

    .line 972
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 976
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->pictures:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 977
    add-int/lit8 v4, v1, -0x1

    return v4

    .line 973
    .end local v3    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v0

    .line 974
    .local v0, "e":Ljava/io/IOException;
    new-instance v4, Lorg/apache/poi/POIXMLException;

    invoke-direct {v4, v0}, Lorg/apache/poi/POIXMLException;-><init>(Ljava/lang/Throwable;)V

    throw v4
.end method

.method public cellBorderStyle(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "cellborder"    # Ljava/lang/String;

    .prologue
    .line 830
    const/4 v0, 0x0

    .line 831
    .local v0, "str":Ljava/lang/String;
    const-string/jumbo v1, "thin"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 832
    const-string/jumbo v0, "groove "

    .line 833
    :cond_0
    const-string/jumbo v1, "thick"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "medium"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    .line 835
    :cond_1
    const-string/jumbo v0, "solid "

    .line 836
    :cond_2
    const-string/jumbo v1, "mediumDashed"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_3

    const-string/jumbo v1, "slantDashDot"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_3

    const-string/jumbo v1, "dashDot"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_4

    .line 839
    :cond_3
    const-string/jumbo v0, "dashed "

    .line 840
    :cond_4
    const-string/jumbo v1, "mediumDashDot"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_5

    const-string/jumbo v1, "dashed"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_5

    const-string/jumbo v1, "mediumDashDotDot"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_5

    const-string/jumbo v1, "dotted"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_6

    .line 850
    :cond_5
    const-string/jumbo v0, "dotted "

    .line 851
    :cond_6
    const-string/jumbo v1, "double"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_7

    .line 852
    const-string/jumbo v0, "double "

    .line 854
    :cond_7
    return-object v0
.end method

.method public clearObject()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 444
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->sharedstring:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    if-eqz v0, :cond_0

    .line 445
    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->sharedstring:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    .line 446
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    if-eqz v0, :cond_1

    .line 447
    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    .line 448
    :cond_1
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    if-eqz v0, :cond_2

    .line 449
    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    .line 450
    :cond_2
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    if-eqz v0, :cond_3

    .line 451
    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    .line 452
    :cond_3
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->themeobj:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    if-eqz v0, :cond_4

    .line 453
    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->themeobj:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .line 454
    :cond_4
    return-void
.end method

.method public formatindexReturnVal(Ljava/lang/String;Ljava/lang/String;D)Ljava/lang/String;
    .locals 19
    .param p1, "formatStr"    # Ljava/lang/String;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "d"    # D

    .prologue
    .line 592
    if-eqz p1, :cond_a

    .line 593
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->formatPercent:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v14

    if-nez v14, :cond_0

    .line 597
    const/4 v13, 0x0

    .line 599
    .local v13, "val":I
    invoke-static/range {p2 .. p2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v14

    const-wide/high16 v16, 0x4059000000000000L    # 100.0

    mul-double v14, v14, v16

    double-to-int v13, v14

    .line 600
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "%"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    move-object/from16 v12, p2

    .line 669
    .end local v13    # "val":I
    .end local p2    # "text":Ljava/lang/String;
    .local v12, "text":Ljava/lang/String;
    :goto_0
    return-object v12

    .line 603
    .end local v12    # "text":Ljava/lang/String;
    .restart local p2    # "text":Ljava/lang/String;
    :cond_0
    const-string/jumbo v14, "$"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_1

    const-string/jumbo v14, "0"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_1

    const-string/jumbo v14, "#"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 606
    :cond_1
    sget-object v14, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->formatCache:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v14, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/poi/ss/format/CellFormat;

    .line 607
    .local v6, "fmt":Lorg/apache/poi/ss/format/CellFormat;
    if-nez v6, :cond_5

    .line 613
    new-instance v6, Lorg/apache/poi/ss/format/CellFormat;

    .end local v6    # "fmt":Lorg/apache/poi/ss/format/CellFormat;
    move-object/from16 v0, p1

    invoke-direct {v6, v0}, Lorg/apache/poi/ss/format/CellFormat;-><init>(Ljava/lang/String;)V

    .line 616
    .restart local v6    # "fmt":Lorg/apache/poi/ss/format/CellFormat;
    const-string/jumbo v14, "[$"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_4

    const-string/jumbo v14, "-"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 617
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;

    invoke-direct {v7}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;-><init>()V

    .line 618
    .local v7, "fmtInd":Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;
    new-instance v6, Lorg/apache/poi/ss/format/CellFormat;

    .end local v6    # "fmt":Lorg/apache/poi/ss/format/CellFormat;
    const-string/jumbo v14, "44"

    invoke-virtual {v7, v14}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->getFormat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v6, v14}, Lorg/apache/poi/ss/format/CellFormat;-><init>(Ljava/lang/String;)V

    .line 619
    .restart local v6    # "fmt":Lorg/apache/poi/ss/format/CellFormat;
    const-string/jumbo v14, "-"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 621
    .local v2, "checkIndex":I
    const-string/jumbo v3, "$"

    .line 623
    .local v3, "currencySymbol":Ljava/lang/String;
    add-int/lit8 v14, v2, 0x1

    add-int/lit8 v15, v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/samsung/thumbnail/util/Utils;->isValidInteger(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 625
    add-int/lit8 v14, v2, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 635
    :cond_2
    :goto_1
    invoke-static/range {p3 .. p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v14

    invoke-virtual {v6, v14}, Lorg/apache/poi/ss/format/CellFormat;->apply(Ljava/lang/Object;)Lorg/apache/poi/ss/format/CellFormatResult;

    move-result-object v9

    .line 636
    .local v9, "result":Lorg/apache/poi/ss/format/CellFormatResult;
    iget-object v0, v9, Lorg/apache/poi/ss/format/CellFormatResult;->text:Ljava/lang/String;

    move-object/from16 p2, v0

    .line 638
    const-string/jumbo v14, "$"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    move-object/from16 v12, p2

    .line 639
    .end local p2    # "text":Ljava/lang/String;
    .restart local v12    # "text":Ljava/lang/String;
    goto/16 :goto_0

    .line 627
    .end local v9    # "result":Lorg/apache/poi/ss/format/CellFormatResult;
    .end local v12    # "text":Ljava/lang/String;
    .restart local p2    # "text":Ljava/lang/String;
    :cond_3
    add-int/lit8 v14, v2, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/samsung/thumbnail/util/Utils;->isValidInteger(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 629
    add-int/lit8 v14, v2, 0x1

    add-int/lit8 v15, v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 641
    .end local v2    # "checkIndex":I
    .end local v3    # "currencySymbol":Ljava/lang/String;
    .end local v7    # "fmtInd":Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;
    :cond_4
    invoke-static/range {p3 .. p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v14

    invoke-virtual {v6, v14}, Lorg/apache/poi/ss/format/CellFormat;->apply(Ljava/lang/Object;)Lorg/apache/poi/ss/format/CellFormatResult;

    move-result-object v10

    .line 642
    .local v10, "result1":Lorg/apache/poi/ss/format/CellFormatResult;
    iget-object v0, v10, Lorg/apache/poi/ss/format/CellFormatResult;->text:Ljava/lang/String;

    move-object/from16 p2, v0

    .line 648
    .end local v10    # "result1":Lorg/apache/poi/ss/format/CellFormatResult;
    :cond_5
    const-string/jumbo v14, "?"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_6

    const-string/jumbo v14, "/"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 649
    :cond_6
    const-string/jumbo v14, "?"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 650
    const/16 v14, 0x3f

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    .line 651
    .local v8, "index":I
    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v14, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    .line 652
    .local v11, "temp":Ljava/lang/String;
    if-lez v8, :cond_7

    if-eqz v11, :cond_7

    .line 653
    move-object/from16 p2, v11

    .end local v8    # "index":I
    .end local v11    # "temp":Ljava/lang/String;
    :cond_7
    move-object/from16 v12, p2

    .line 657
    .end local p2    # "text":Ljava/lang/String;
    .restart local v12    # "text":Ljava/lang/String;
    goto/16 :goto_0

    .line 658
    .end local v6    # "fmt":Lorg/apache/poi/ss/format/CellFormat;
    .end local v12    # "text":Ljava/lang/String;
    .restart local p2    # "text":Ljava/lang/String;
    :cond_8
    const-string/jumbo v14, "General"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_9

    const-string/jumbo v14, "@"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 659
    :cond_9
    sget-object v6, Lorg/apache/poi/ss/format/CellFormat;->GENERAL_FORMAT:Lorg/apache/poi/ss/format/CellFormat;

    .line 660
    .restart local v6    # "fmt":Lorg/apache/poi/ss/format/CellFormat;
    invoke-static/range {p3 .. p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v14

    invoke-virtual {v6, v14}, Lorg/apache/poi/ss/format/CellFormat;->apply(Ljava/lang/Object;)Lorg/apache/poi/ss/format/CellFormatResult;

    move-result-object v9

    .line 661
    .restart local v9    # "result":Lorg/apache/poi/ss/format/CellFormatResult;
    iget-object v0, v9, Lorg/apache/poi/ss/format/CellFormatResult;->text:Ljava/lang/String;

    move-object/from16 p2, v0

    .end local v6    # "fmt":Lorg/apache/poi/ss/format/CellFormat;
    .end local v9    # "result":Lorg/apache/poi/ss/format/CellFormatResult;
    :cond_a
    move-object/from16 v12, p2

    .line 669
    .end local p2    # "text":Ljava/lang/String;
    .restart local v12    # "text":Ljava/lang/String;
    goto/16 :goto_0

    .line 664
    .end local v12    # "text":Ljava/lang/String;
    .restart local p2    # "text":Ljava/lang/String;
    :cond_b
    const-wide/high16 v14, 0x4059000000000000L    # 100.0

    mul-double v14, v14, p3

    invoke-static {v14, v15}, Ljava/lang/Math;->round(D)J

    move-result-wide v14

    long-to-double v14, v14

    const-wide/high16 v16, 0x4059000000000000L    # 100.0

    div-double v4, v14, v16

    .line 665
    .local v4, "finalValue":D
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    move-object/from16 v12, p2

    .line 666
    .end local p2    # "text":Ljava/lang/String;
    .restart local v12    # "text":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method public getActualFile()Ljava/io/File;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->file:Ljava/io/File;

    return-object v0
.end method

.method public getAllEmbedds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/openxml4j/opc/PackagePart;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/OpenXML4JException;
        }
    .end annotation

    .prologue
    .line 1893
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 1910
    .local v0, "embedds":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/openxml4j/opc/PackagePart;>;"
    return-object v0
.end method

.method public getAllPictures()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1222
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->pictures:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1225
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->pictures:Ljava/util/List;

    .line 1234
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->pictures:Ljava/util/List;

    return-object v0
.end method

.method public getBorderColor(ILjava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "cellborderid"    # I
    .param p2, "border"    # Ljava/lang/String;

    .prologue
    .line 752
    const/4 v0, 0x0

    .line 753
    .local v0, "bordercolor":Ljava/lang/String;
    const-string/jumbo v1, "left"

    invoke-virtual {p2, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 754
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellLeftBorderRGBColor()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 755
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellLeftBorderRGBColor()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bordercolor":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 768
    .restart local v0    # "bordercolor":Ljava/lang/String;
    :cond_0
    :goto_0
    const-string/jumbo v1, "rgt"

    invoke-virtual {p2, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 770
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellRightBorderRGBColor()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 771
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellRightBorderRGBColor()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bordercolor":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 785
    .restart local v0    # "bordercolor":Ljava/lang/String;
    :cond_1
    :goto_1
    const-string/jumbo v1, "top"

    invoke-virtual {p2, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    .line 786
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellTopBorderRGBColor()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 787
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellTopBorderRGBColor()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bordercolor":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 800
    .restart local v0    # "bordercolor":Ljava/lang/String;
    :cond_2
    :goto_2
    const-string/jumbo v1, "bottom"

    invoke-virtual {p2, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_3

    .line 801
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellBottomBorderRGBColor()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 803
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellBottomBorderRGBColor()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bordercolor":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 816
    .restart local v0    # "bordercolor":Ljava/lang/String;
    :cond_3
    :goto_3
    if-eqz v0, :cond_5

    .line 817
    const-string/jumbo v1, "000000"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_4

    const-string/jumbo v1, "4F81BD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_4

    const-string/jumbo v1, "EEECE1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_5

    .line 820
    :cond_4
    const-string/jumbo v0, "black"

    .line 822
    :cond_5
    return-object v0

    .line 758
    :cond_6
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellLeftBorderThemeColor()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bordercolor":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 760
    .restart local v0    # "bordercolor":Ljava/lang/String;
    if-eqz v0, :cond_7

    .line 761
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getColorSchemRGBval(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 764
    :cond_7
    const-string/jumbo v0, "black"

    goto/16 :goto_0

    .line 775
    :cond_8
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellRightBorderThemeColor()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bordercolor":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 777
    .restart local v0    # "bordercolor":Ljava/lang/String;
    if-eqz v0, :cond_9

    .line 778
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getColorSchemRGBval(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 781
    :cond_9
    const-string/jumbo v0, "black"

    goto/16 :goto_1

    .line 790
    :cond_a
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellTopBorderThemeColor()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bordercolor":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 792
    .restart local v0    # "bordercolor":Ljava/lang/String;
    if-eqz v0, :cond_b

    .line 793
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getColorSchemRGBval(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 796
    :cond_b
    const-string/jumbo v0, "black"

    goto/16 :goto_2

    .line 806
    :cond_c
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellBottomBorderThemeColor()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bordercolor":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 808
    .restart local v0    # "bordercolor":Ljava/lang/String;
    if-eqz v0, :cond_d

    .line 809
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getColorSchemRGBval(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 812
    :cond_d
    const-string/jumbo v0, "black"

    goto/16 :goto_3
.end method

.method public getCanvas()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->mViewConsumer:Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getCanvas()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v0

    return-object v0
.end method

.method public getCellborderid(I)I
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 460
    const/4 v0, 0x0

    .line 461
    .local v0, "borderid":I
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetStyleid()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, p1, :cond_2

    .line 463
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetStyleid()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 464
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetStyleid()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 466
    :cond_0
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellXFBorderId()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 467
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellXFBorderId()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 475
    :goto_0
    return v0

    .line 470
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 473
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getColorSchemRGBval(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 437
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->themeobj:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getExcelColorSchema()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getFolderPath()Ljava/io/File;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->mViewConsumer:Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getFolderPath()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public getMissingCellPolicy()Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;
    .locals 1

    .prologue
    .line 1501
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->_missingCellPolicy:Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    return-object v0
.end method

.method public getSIID(I)I
    .locals 6
    .param p1, "vValue"    # I

    .prologue
    .line 702
    const/4 v1, -0x1

    .line 703
    .local v1, "siID":I
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetVTextid()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 706
    .local v3, "textid":Ljava/lang/String;
    :try_start_0
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 707
    .local v2, "temp":I
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->sharedstring:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getExSharedStringSIValue()Ljava/util/HashMap;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    if-eqz v4, :cond_0

    .line 708
    move v1, v2

    .line 714
    .end local v2    # "temp":I
    :cond_0
    :goto_0
    return v1

    .line 710
    :catch_0
    move-exception v0

    .line 711
    .local v0, "e":Ljava/lang/NumberFormatException;
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getSIStringDetails(I)Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;
    .locals 3
    .param p1, "i"    # I

    .prologue
    .line 718
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->sharedstring:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getExSharedStringSIValue()Ljava/util/HashMap;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 720
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->sharedstring:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getExSharedStringSIValue()Ljava/util/HashMap;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    .line 725
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSharedStringObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;
    .locals 1

    .prologue
    .line 1943
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->sharedstring:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    return-object v0
.end method

.method public getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;
    .locals 1

    .prologue
    .line 1935
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    return-object v0
.end method

.method public getTextFromSheettextid(I)Ljava/lang/String;
    .locals 3
    .param p1, "i"    # I

    .prologue
    .line 505
    const-string/jumbo v0, ""

    .line 507
    .local v0, "text":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetTextDataType()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetTextDataType()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string/jumbo v2, "string"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 511
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetVTextid()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "text":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 512
    .restart local v0    # "text":Ljava/lang/String;
    invoke-virtual {p0, v0, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->isCellDateFormat(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 516
    :goto_0
    return-object v0

    .line 514
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->tryParseInt(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTextStyleid(I)I
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 482
    const/4 v0, 0x0

    .line 483
    .local v0, "textstyleid":I
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetStyleid()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, p1, :cond_2

    .line 485
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetStyleid()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 486
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetStyleid()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 488
    :cond_0
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellXFFontid()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 489
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellXFFontid()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 497
    :goto_0
    return v0

    .line 492
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 495
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getThemeObj()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    .locals 1

    .prologue
    .line 1939
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->themeobj:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    return-object v0
.end method

.method public initializesharedstringvar()V
    .locals 2

    .prologue
    .line 859
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->excelVariables:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    const/4 v1, 0x0

    iput v1, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->hashIndex:I

    .line 860
    return-void
.end method

.method public initializevariable()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 866
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->excelVariables:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    iput v1, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    .line 867
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->excelVariables:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    iput v1, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tblbordercount:I

    .line 869
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->excelVariables:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    iput v1, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->cellnum:I

    .line 870
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->excelVariables:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    iput v1, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->numalignment:I

    .line 871
    return-void
.end method

.method public isCellDateFormat(Ljava/lang/String;I)Ljava/lang/String;
    .locals 18
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "i"    # I

    .prologue
    .line 525
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;

    invoke-direct {v10}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;-><init>()V

    .line 526
    .local v10, "formatind":Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;
    const/4 v12, 0x0

    .line 527
    .local v12, "styleid":I
    const-string/jumbo v9, ""

    .line 528
    .local v9, "formatStr":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-le v15, v0, :cond_4

    .line 529
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetStyleid()Ljava/util/ArrayList;

    move-result-object v15

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v15

    move/from16 v0, p2

    if-le v15, v0, :cond_4

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetStyleid()Ljava/util/ArrayList;

    move-result-object v15

    move/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    if-eqz v15, :cond_4

    .line 531
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetStyleid()Ljava/util/ArrayList;

    move-result-object v15

    move/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 533
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellXFNumFmtId()Ljava/util/ArrayList;

    move-result-object v15

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v15

    if-le v15, v12, :cond_4

    .line 534
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellXFNumFmtId()Ljava/util/ArrayList;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 536
    .local v11, "numfmtid":Ljava/lang/String;
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 537
    .local v8, "formatIndex":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getApplynumberformatval()Ljava/util/ArrayList;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 539
    .local v2, "applynumfmtid":Ljava/lang/String;
    if-nez v2, :cond_2

    .line 542
    invoke-virtual {v10, v11}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->getFormat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 550
    :cond_0
    :goto_0
    if-lez v8, :cond_4

    .line 551
    invoke-static/range {p1 .. p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 552
    .local v4, "d":D
    const/4 v3, 0x0

    .line 553
    .local v3, "bDate":Z
    invoke-static {v4, v5}, Lorg/apache/poi/ss/usermodel/DateUtil;->isValidExcelDate(D)Z

    move-result v15

    if-eqz v15, :cond_4

    if-eqz v9, :cond_4

    .line 555
    invoke-static {v8, v9}, Lorg/apache/poi/ss/usermodel/DateUtil;->isADateFormat(ILjava/lang/String;)Z

    move-result v3

    .line 557
    const/4 v15, 0x1

    if-ne v3, v15, :cond_3

    .line 559
    invoke-virtual {v10, v4, v5, v8, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->getFormat(DILjava/lang/String;)Ljava/text/Format;

    move-result-object v7

    .line 561
    .local v7, "dateFormat":Ljava/text/Format;
    instance-of v15, v7, Lorg/apache/poi/ss/usermodel/ExcelStyleDateFormatter;

    if-eqz v15, :cond_1

    move-object v15, v7

    .line 562
    check-cast v15, Lorg/apache/poi/ss/usermodel/ExcelStyleDateFormatter;

    invoke-virtual {v15, v4, v5}, Lorg/apache/poi/ss/usermodel/ExcelStyleDateFormatter;->setDateToBeFormatted(D)V

    .line 566
    :cond_1
    const/4 v15, 0x0

    invoke-static {v4, v5, v15}, Lorg/apache/poi/ss/usermodel/DateUtil;->getJavaDate(DZ)Ljava/util/Date;

    move-result-object v6

    .line 567
    .local v6, "d1":Ljava/util/Date;
    invoke-virtual {v10, v6, v7}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->performDateFormatting(Ljava/util/Date;Ljava/text/Format;)Ljava/lang/String;

    move-result-object v14

    .line 569
    .local v14, "value":Ljava/lang/String;
    move-object/from16 p1, v14

    move-object/from16 v13, p1

    .line 583
    .end local v2    # "applynumfmtid":Ljava/lang/String;
    .end local v3    # "bDate":Z
    .end local v4    # "d":D
    .end local v6    # "d1":Ljava/util/Date;
    .end local v7    # "dateFormat":Ljava/text/Format;
    .end local v8    # "formatIndex":I
    .end local v11    # "numfmtid":Ljava/lang/String;
    .end local v14    # "value":Ljava/lang/String;
    .end local p1    # "text":Ljava/lang/String;
    .local v13, "text":Ljava/lang/String;
    :goto_1
    return-object v13

    .line 544
    .end local v13    # "text":Ljava/lang/String;
    .restart local v2    # "applynumfmtid":Ljava/lang/String;
    .restart local v8    # "formatIndex":I
    .restart local v11    # "numfmtid":Ljava/lang/String;
    .restart local p1    # "text":Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->stylesobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getstylenumfmtcode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 546
    if-nez v9, :cond_0

    .line 547
    invoke-virtual {v10, v11}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->getFormat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    goto :goto_0

    .line 572
    .restart local v3    # "bDate":Z
    .restart local v4    # "d":D
    :cond_3
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v9, v1, v4, v5}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->formatindexReturnVal(Ljava/lang/String;Ljava/lang/String;D)Ljava/lang/String;

    move-result-object p1

    move-object/from16 v13, p1

    .line 575
    .end local p1    # "text":Ljava/lang/String;
    .restart local v13    # "text":Ljava/lang/String;
    goto :goto_1

    .end local v2    # "applynumfmtid":Ljava/lang/String;
    .end local v3    # "bDate":Z
    .end local v4    # "d":D
    .end local v8    # "formatIndex":I
    .end local v11    # "numfmtid":Ljava/lang/String;
    .end local v13    # "text":Ljava/lang/String;
    .restart local p1    # "text":Ljava/lang/String;
    :cond_4
    move-object/from16 v13, p1

    .line 583
    .end local p1    # "text":Ljava/lang/String;
    .restart local v13    # "text":Ljava/lang/String;
    goto :goto_1
.end method

.method public isHidden()Z
    .locals 2

    .prologue
    .line 1914
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Not implemented yet"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isMacroEnabled()Z
    .locals 2

    .prologue
    .line 1420
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getContentType()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->MACROS_WORKBOOK:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->getContentType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public load()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 258
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFactory;->getInstance()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFactory;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->load(Lorg/apache/poi/POIXMLFactory;)V

    .line 259
    return-void
.end method

.method public onDocLoadFinished()V
    .locals 0

    .prologue
    .line 1931
    return-void
.end method

.method protected onDocumentRead()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 292
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->createCanvasForm()V

    .line 293
    return-void
.end method

.method public refreshWebView(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 1925
    return-void
.end method

.method public setHidden(Z)V
    .locals 2
    .param p1, "hiddenFlag"    # Z

    .prologue
    .line 1918
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Not implemented yet"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setMissingCellPolicy(Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;)V
    .locals 0
    .param p1, "missingCellPolicy"    # Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    .prologue
    .line 1510
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->_missingCellPolicy:Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    .line 1511
    return-void
.end method

.method public tryParseInt(I)Ljava/lang/String;
    .locals 7
    .param p1, "i"    # I

    .prologue
    .line 678
    const-string/jumbo v3, ""

    .line 679
    .local v3, "text":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->worksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetVTextid()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 683
    .local v4, "textid":Ljava/lang/String;
    :try_start_0
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 684
    .local v2, "temp":I
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->sharedstring:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getExSharedStringSIValue()Ljava/util/HashMap;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 686
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->sharedstring:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getExSharedStringSIValue()Ljava/util/HashMap;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    .line 688
    .local v1, "siVal":Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getText()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    move-object v4, v3

    .line 696
    .end local v1    # "siVal":Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;
    .end local v2    # "temp":I
    .end local v4    # "textid":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v4

    .line 694
    .restart local v4    # "textid":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 696
    .local v0, "e":Ljava/lang/NumberFormatException;
    goto :goto_0
.end method
