.class public Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;
.super Ljava/lang/Object;
.source "EMR_StrokePath.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;


# instance fields
.field private RecordSize:I

.field private RecordType:I

.field protected bounds:Landroid/graphics/RectF;

.field private emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

.field fileContent:[B

.field private leftTopX:I

.field private leftTopY:I

.field localPath:Landroid/graphics/Path;

.field private offset:I

.field private rightBottomX:I

.field private rightBottomY:I


# direct methods
.method public constructor <init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V
    .locals 1
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I
    .param p3, "emrGlobals"    # Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->bounds:Landroid/graphics/RectF;

    .line 229
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->localPath:Landroid/graphics/Path;

    .line 60
    iput-object p3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .line 61
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->fileContent:[B

    .line 62
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->offset:I

    .line 63
    return-void
.end method


# virtual methods
.method public getPath()Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->localPath:Landroid/graphics/Path;

    return-object v0
.end method

.method public getRecordSize()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->RecordSize:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->RecordType:I

    return v0
.end method

.method public getleftTopX()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->leftTopX:I

    return v0
.end method

.method public getleftTopY()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->leftTopY:I

    return v0
.end method

.method public getrightBottomX()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->rightBottomX:I

    return v0
.end method

.method public getrightBottomY()I
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->rightBottomY:I

    return v0
.end method

.method public prepare()V
    .locals 7

    .prologue
    .line 264
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->getleftTopX()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->getleftTopY()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float/2addr v3, v4

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->getrightBottomX()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float/2addr v4, v5

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->getrightBottomY()I

    move-result v5

    int-to-float v5, v5

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v6, v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float/2addr v5, v6

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->setBounds(FFFF)V

    .line 267
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    iput-object v2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->localPath:Landroid/graphics/Path;

    .line 268
    iget-object v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->bounds:Landroid/graphics/RectF;

    .line 269
    .local v1, "localRectF":Landroid/graphics/RectF;
    sget-object v0, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    .line 270
    .local v0, "localDirection":Landroid/graphics/Path$Direction;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->localPath:Landroid/graphics/Path;

    invoke-virtual {v2, v1, v0}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 271
    return-void
.end method

.method public readEmfStrokePath()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x0

    .line 183
    const/4 v2, 0x0

    .line 185
    .local v2, "i":I
    const/4 v8, 0x4

    new-array v3, v8, [B

    .line 187
    .local v3, "intConvert":[B
    const/4 v2, 0x0

    :goto_0
    if-gt v2, v12, :cond_0

    .line 188
    rsub-int/lit8 v8, v2, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->offset:I

    add-int/2addr v10, v2

    aget-byte v9, v9, v10

    aput-byte v9, v3, v8

    .line 187
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 189
    :cond_0
    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 190
    .local v1, "Type":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->offset:I

    add-int/2addr v8, v2

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->offset:I

    .line 191
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->setRecordType(I)V

    .line 193
    const/4 v2, 0x0

    :goto_1
    if-gt v2, v12, :cond_1

    .line 194
    rsub-int/lit8 v8, v2, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->offset:I

    add-int/2addr v10, v2

    aget-byte v9, v9, v10

    aput-byte v9, v3, v8

    .line 193
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 195
    :cond_1
    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 196
    .local v0, "Size":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->offset:I

    add-int/2addr v8, v2

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->offset:I

    .line 197
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->setRecordSize(I)V

    .line 199
    const/4 v2, 0x0

    :goto_2
    if-gt v2, v12, :cond_2

    .line 200
    rsub-int/lit8 v8, v2, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->offset:I

    add-int/2addr v10, v2

    aget-byte v9, v9, v10

    aput-byte v9, v3, v8

    .line 199
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 201
    :cond_2
    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v4

    .line 202
    .local v4, "leftTopX":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->offset:I

    add-int/2addr v8, v2

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->offset:I

    .line 203
    invoke-virtual {p0, v4}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->setleftTopX(I)V

    .line 205
    const/4 v2, 0x0

    :goto_3
    if-gt v2, v12, :cond_3

    .line 206
    rsub-int/lit8 v8, v2, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->offset:I

    add-int/2addr v10, v2

    aget-byte v9, v9, v10

    aput-byte v9, v3, v8

    .line 205
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 207
    :cond_3
    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v5

    .line 208
    .local v5, "leftTopY":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->offset:I

    add-int/2addr v8, v2

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->offset:I

    .line 209
    invoke-virtual {p0, v5}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->setleftTopY(I)V

    .line 211
    const/4 v2, 0x0

    :goto_4
    if-gt v2, v12, :cond_4

    .line 212
    rsub-int/lit8 v8, v2, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->offset:I

    add-int/2addr v10, v2

    aget-byte v9, v9, v10

    aput-byte v9, v3, v8

    .line 211
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 213
    :cond_4
    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v6

    .line 214
    .local v6, "rightBottomX":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->offset:I

    add-int/2addr v8, v2

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->offset:I

    .line 215
    invoke-virtual {p0, v6}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->setrightBottomX(I)V

    .line 217
    const/4 v2, 0x0

    :goto_5
    if-gt v2, v12, :cond_5

    .line 218
    rsub-int/lit8 v8, v2, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->offset:I

    add-int/2addr v10, v2

    aget-byte v9, v9, v10

    aput-byte v9, v3, v8

    .line 217
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 219
    :cond_5
    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v7

    .line 220
    .local v7, "rightBottomY":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->offset:I

    add-int/2addr v8, v2

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->offset:I

    .line 221
    invoke-virtual {p0, v7}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->setrightBottomY(I)V

    .line 223
    return-void
.end method

.method public render(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 282
    const/4 v2, 0x0

    .line 283
    .local v2, "i":I
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;-><init>()V

    .line 284
    .local v0, "PaintPenPropertiesObject":Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;
    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 286
    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 288
    .local v1, "emrSize":I
    if-lez v1, :cond_1

    .line 289
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 290
    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v5, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandlePen:I

    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->gethandleNumber()I

    move-result v4

    if-ne v5, v4, :cond_4

    .line 295
    :cond_0
    if-ge v2, v1, :cond_1

    .line 296
    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getColor()I

    move-result v4

    invoke-static {v4}, Lcom/samsung/thumbnail/office/emf/Util/toColorAndroid;->getColor(I)I

    move-result v3

    .line 299
    .local v3, "myColor":I
    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 300
    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    if-eqz v4, :cond_1

    .line 301
    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 303
    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-boolean v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->endPath:Z

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 304
    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    const/4 v5, 0x0

    iput-object v5, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    .line 309
    .end local v3    # "myColor":I
    :cond_1
    if-eq v2, v1, :cond_2

    if-nez v1, :cond_3

    .line 311
    :cond_2
    const/16 v4, 0x8

    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v4, v5, :cond_5

    .line 330
    :cond_3
    :goto_1
    return-void

    .line 289
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 315
    :cond_5
    const/4 v4, 0x7

    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v4, v5, :cond_6

    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    if-eqz v4, :cond_6

    .line 318
    const/high16 v4, -0x1000000

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 319
    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 325
    :cond_6
    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    if-eqz v4, :cond_3

    .line 326
    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_1
.end method

.method public setBounds(FFFF)V
    .locals 1
    .param p1, "lx"    # F
    .param p2, "ly"    # F
    .param p3, "rx"    # F
    .param p4, "ry"    # F

    .prologue
    .line 244
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->bounds:Landroid/graphics/RectF;

    .line 245
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->bounds:Landroid/graphics/RectF;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 246
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "RecrdSize"    # I

    .prologue
    .line 100
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->RecordSize:I

    .line 101
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "RecrdType"    # I

    .prologue
    .line 81
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->RecordType:I

    .line 82
    return-void
.end method

.method public setleftTopX(I)V
    .locals 0
    .param p1, "lftTopX"    # I

    .prologue
    .line 119
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->leftTopX:I

    .line 120
    return-void
.end method

.method public setleftTopY(I)V
    .locals 0
    .param p1, "lftTopY"    # I

    .prologue
    .line 138
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->leftTopY:I

    .line 139
    return-void
.end method

.method public setrightBottomX(I)V
    .locals 0
    .param p1, "rghtBottomX"    # I

    .prologue
    .line 157
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->rightBottomX:I

    .line 158
    return-void
.end method

.method public setrightBottomY(I)V
    .locals 0
    .param p1, "rghtBottomY"    # I

    .prologue
    .line 176
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_StrokePath;->rightBottomY:I

    .line 177
    return-void
.end method
