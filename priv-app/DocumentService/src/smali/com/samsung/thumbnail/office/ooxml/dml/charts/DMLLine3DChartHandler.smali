.class public Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLLine3DChartHandler;
.super Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;
.source "DMLLine3DChartHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 14
    const/16 v0, 0x23

    const-string/jumbo v1, "line3DChart"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;-><init>(ILjava/lang/String;)V

    .line 15
    return-void
.end method


# virtual methods
.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 36
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 37
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    if-eqz v0, :cond_0

    .line 41
    :goto_0
    return-void

    .line 40
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    const-string/jumbo v1, "Line"

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->addExChartName(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    const/4 v1, 0x2

    .line 20
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 21
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    if-eqz v0, :cond_0

    .line 23
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->setChartType(I)V

    .line 27
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    if-eqz v0, :cond_1

    .line 29
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->setChartType(I)V

    .line 32
    :cond_1
    return-void
.end method
