.class public Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelSheetDrawingParser;
.super Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;
.source "XExcelSheetDrawingParser.java"


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)V
    .locals 0
    .param p1, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)V

    .line 17
    return-void
.end method


# virtual methods
.method public createRootElementHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingSheetHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingSheetHandler;-><init>()V

    return-object v0
.end method

.method public getMainNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .locals 1

    .prologue
    .line 26
    const/16 v0, 0xc9

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelSheetDrawingParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v0

    return-object v0
.end method
