.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedTagNoChildrenHandler;
.source "TextHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextHandler$ITextConsumer;
    }
.end annotation


# instance fields
.field private consumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextHandler$ITextConsumer;

.field protected text:Ljava/lang/StringBuffer;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextHandler$ITextConsumer;)V
    .locals 2
    .param p1, "consumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextHandler$ITextConsumer;

    .prologue
    .line 21
    const/16 v0, 0x1f

    const-string/jumbo v1, "t"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedTagNoChildrenHandler;-><init>(ILjava/lang/String;)V

    .line 22
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextHandler$ITextConsumer;

    .line 23
    return-void
.end method


# virtual methods
.method public charactersHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;[CII)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "content"    # [C
    .param p3, "start"    # I
    .param p4, "length"    # I

    .prologue
    .line 34
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2, p3, p4}, Ljava/lang/String;-><init>([CII)V

    .line 35
    .local v0, "txt":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextHandler;->text:Ljava/lang/StringBuffer;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 36
    return-void
.end method

.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedTagNoChildrenHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextHandler$ITextConsumer;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextHandler;->text:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextHandler$ITextConsumer;->setText(Ljava/lang/String;)V

    .line 42
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 28
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextHandler;->text:Ljava/lang/StringBuffer;

    .line 29
    return-void
.end method
