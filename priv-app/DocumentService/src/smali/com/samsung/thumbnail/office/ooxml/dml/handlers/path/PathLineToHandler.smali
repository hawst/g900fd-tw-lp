.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathLineToHandler;
.super Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathSegmentsPointsHandler;
.source "PathLineToHandler.java"


# instance fields
.field private pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V
    .locals 1
    .param p1, "pathDesc"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    .prologue
    .line 10
    const-string/jumbo v0, "lnTo"

    invoke-direct {p0, v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathSegmentsPointsHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V

    .line 11
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathLineToHandler;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    .line 12
    return-void
.end method


# virtual methods
.method public point([Ljava/lang/String;)V
    .locals 2
    .param p1, "valuesArray"    # [Ljava/lang/String;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathLineToHandler;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->addSegmentstoPath(I[Ljava/lang/String;)V

    .line 17
    return-void
.end method
