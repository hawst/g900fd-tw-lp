.class public Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelTextBodyHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XExcelTextBodyHandler.java"


# instance fields
.field private textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;)V
    .locals 1
    .param p1, "autoShape"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;

    .prologue
    .line 20
    const-string/jumbo v0, "txBody"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 21
    check-cast p1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    .end local p1    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelTextBodyHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    .line 22
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelTextBodyHandler;->init()V

    .line 23
    return-void
.end method


# virtual methods
.method protected init()V
    .locals 6

    .prologue
    .line 26
    const/4 v5, 0x2

    new-array v4, v5, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 28
    .local v4, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextBodyPropertyHandler;

    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelTextBodyHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    invoke-direct {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextBodyPropertyHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;)V

    .line 30
    .local v0, "bodyPrHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextBodyPropertyHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v5, "bodyPr"

    invoke-direct {v1, v5, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 33
    .local v1, "bodyPrSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v5, 0x0

    aput-object v1, v4, v5

    .line 35
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ParagraphHandler;

    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelTextBodyHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    invoke-direct {v2, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ParagraphHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;)V

    .line 36
    .local v2, "paraHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ParagraphHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v5, "p"

    invoke-direct {v3, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 38
    .local v3, "paraSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v5, 0x1

    aput-object v3, v4, v5

    .line 47
    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelTextBodyHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 48
    return-void
.end method
