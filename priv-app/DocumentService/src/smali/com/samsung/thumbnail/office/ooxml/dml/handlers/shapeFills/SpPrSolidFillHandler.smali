.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;
.super Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;
.source "SpPrSolidFillHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler$ColorConsumer;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V
    .locals 6
    .param p1, "fillConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;
    .param p2, "shInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "theme"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .prologue
    .line 14
    const/16 v1, 0x1f

    const-string/jumbo v2, "solidFill"

    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler$ColorConsumer;

    invoke-direct {v3, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler$ColorConsumer;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;)V

    move-object v0, p0

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 16
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 21
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;->colorConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler$ColorConsumer;

    .line 22
    .local v0, "clrConsumer":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler$ColorConsumer;
    iget-object v1, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler$ColorConsumer;->fillConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;

    iget-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler$ColorConsumer;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-interface {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;->consumeFill(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V

    .line 23
    return-void
.end method
