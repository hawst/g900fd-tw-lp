.class public final enum Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;
.super Ljava/lang/Enum;
.source "XWPFShapeStyle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EConnectorType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

.field public static final enum CURVED:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

.field public static final enum ELBOW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

.field public static final enum NONE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

.field public static final enum STRAIGHT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 99
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    const-string/jumbo v1, "NONE"

    const-string/jumbo v2, "none"

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    .line 102
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    const-string/jumbo v1, "STRAIGHT"

    const-string/jumbo v2, "straight"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;->STRAIGHT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    .line 105
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    const-string/jumbo v1, "ELBOW"

    const-string/jumbo v2, "elbow"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;->ELBOW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    .line 108
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    const-string/jumbo v1, "CURVED"

    const-string/jumbo v2, "curved"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;->CURVED:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    .line 95
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;->STRAIGHT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;->ELBOW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;->CURVED:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 111
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 112
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;->value:Ljava/lang/String;

    .line 113
    return-void
.end method

.method public static convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 120
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;->values()[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 121
    .local v2, "inst":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 125
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;
    :goto_1
    return-object v2

    .line 120
    .restart local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 125
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 95
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    return-object v0
.end method


# virtual methods
.method public xmlValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;->value:Ljava/lang/String;

    return-object v0
.end method
