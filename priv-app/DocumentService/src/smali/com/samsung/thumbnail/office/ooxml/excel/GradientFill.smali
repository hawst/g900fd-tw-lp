.class public Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;
.super Ljava/lang/Object;
.source "GradientFill.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/excel/IFill;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;,
        Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;
    }
.end annotation


# instance fields
.field private bottom:D

.field private degree:D

.field private left:D

.field private right:D

.field private stopLst:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;",
            ">;"
        }
    .end annotation
.end field

.field private top:D

.field private type:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;->LINEAR:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->type:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

    .line 106
    return-void
.end method


# virtual methods
.method public addStop(Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;)V
    .locals 1
    .param p1, "stop"    # Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->stopLst:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->stopLst:Ljava/util/ArrayList;

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->stopLst:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    return-void
.end method

.method public getBottom()D
    .locals 2

    .prologue
    .line 71
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->bottom:D

    return-wide v0
.end method

.method public getDegree()D
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->degree:D

    return-wide v0
.end method

.method public getLeft()D
    .locals 2

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->left:D

    return-wide v0
.end method

.method public getRight()D
    .locals 2

    .prologue
    .line 63
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->right:D

    return-wide v0
.end method

.method public getStopLst()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->stopLst:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTop()D
    .locals 2

    .prologue
    .line 67
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->top:D

    return-wide v0
.end method

.method public getType()Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->type:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

    return-object v0
.end method

.method public setBottom(D)V
    .locals 1
    .param p1, "bottom"    # D

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->bottom:D

    .line 44
    return-void
.end method

.method public setDegree(D)V
    .locals 1
    .param p1, "degree"    # D

    .prologue
    .line 27
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->degree:D

    .line 28
    return-void
.end method

.method public setGradientType(Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;)V
    .locals 0
    .param p1, "type"    # Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->type:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

    .line 48
    return-void
.end method

.method public setLeft(D)V
    .locals 1
    .param p1, "left"    # D

    .prologue
    .line 31
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->left:D

    .line 32
    return-void
.end method

.method public setRight(D)V
    .locals 1
    .param p1, "right"    # D

    .prologue
    .line 35
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->right:D

    .line 36
    return-void
.end method

.method public setTop(D)V
    .locals 1
    .param p1, "top"    # D

    .prologue
    .line 39
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->top:D

    .line 40
    return-void
.end method
