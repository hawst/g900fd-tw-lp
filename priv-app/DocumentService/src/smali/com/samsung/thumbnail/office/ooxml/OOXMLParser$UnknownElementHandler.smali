.class Lcom/samsung/thumbnail/office/ooxml/OOXMLParser$UnknownElementHandler;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "OOXMLParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UnknownElementHandler"
.end annotation


# instance fields
.field level:I

.field parser:Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

.field final synthetic this$0:Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p2, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 438
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser$UnknownElementHandler;->this$0:Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 436
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser$UnknownElementHandler;->level:I

    .line 439
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser$UnknownElementHandler;->parser:Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .line 440
    return-void
.end method


# virtual methods
.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "namespaceURI"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 451
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser$UnknownElementHandler;->level:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser$UnknownElementHandler;->level:I

    .line 452
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser$UnknownElementHandler;->level:I

    if-eqz v0, :cond_0

    .line 457
    :goto_0
    return-void

    .line 455
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser$UnknownElementHandler;->this$0:Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    # getter for: Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->xmlReader:Lorg/xml/sax/XMLReader;
    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->access$000(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Lorg/xml/sax/XMLReader;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser$UnknownElementHandler;->parser:Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 456
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser$UnknownElementHandler;->parser:Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->removeNameSpace()V

    goto :goto_0
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "namespaceURI"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "atts"    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 445
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser$UnknownElementHandler;->level:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser$UnknownElementHandler;->level:I

    .line 446
    return-void
.end method
