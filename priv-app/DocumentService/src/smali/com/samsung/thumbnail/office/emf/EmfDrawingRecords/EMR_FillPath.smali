.class public Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;
.super Ljava/lang/Object;
.source "EMR_FillPath.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;


# instance fields
.field private RecordSize:I

.field private RecordType:I

.field protected bounds:Landroid/graphics/RectF;

.field private emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

.field fileContent:[B

.field private leftTopX:I

.field private leftTopY:I

.field localPath:Landroid/graphics/Path;

.field private offset:I

.field private rightBottomX:I

.field private rightBottomY:I


# direct methods
.method public constructor <init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V
    .locals 1
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I
    .param p3, "emrGlobals"    # Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .prologue
    const/4 v0, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->bounds:Landroid/graphics/RectF;

    .line 228
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->localPath:Landroid/graphics/Path;

    .line 59
    iput-object p3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .line 60
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->fileContent:[B

    .line 61
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->offset:I

    .line 62
    return-void
.end method


# virtual methods
.method public getPath()Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->localPath:Landroid/graphics/Path;

    return-object v0
.end method

.method public getRecordSize()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->RecordSize:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->RecordType:I

    return v0
.end method

.method public getleftTopX()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->leftTopX:I

    return v0
.end method

.method public getleftTopY()I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->leftTopY:I

    return v0
.end method

.method public getrightBottomX()I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->rightBottomX:I

    return v0
.end method

.method public getrightBottomY()I
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->rightBottomY:I

    return v0
.end method

.method public prepare()V
    .locals 7

    .prologue
    .line 263
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->getleftTopX()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->getleftTopY()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float/2addr v3, v4

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->getrightBottomX()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float/2addr v4, v5

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->getrightBottomY()I

    move-result v5

    int-to-float v5, v5

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v6, v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float/2addr v5, v6

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->setBounds(FFFF)V

    .line 266
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    iput-object v2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->localPath:Landroid/graphics/Path;

    .line 267
    iget-object v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->bounds:Landroid/graphics/RectF;

    .line 268
    .local v1, "localRectF":Landroid/graphics/RectF;
    sget-object v0, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    .line 269
    .local v0, "localDirection":Landroid/graphics/Path$Direction;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->localPath:Landroid/graphics/Path;

    invoke-virtual {v2, v1, v0}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 270
    return-void
.end method

.method public readEmfFillPath()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x0

    .line 182
    const/4 v2, 0x0

    .line 184
    .local v2, "i":I
    const/4 v8, 0x4

    new-array v3, v8, [B

    .line 186
    .local v3, "intConvert":[B
    const/4 v2, 0x0

    :goto_0
    if-gt v2, v12, :cond_0

    .line 187
    rsub-int/lit8 v8, v2, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->offset:I

    add-int/2addr v10, v2

    aget-byte v9, v9, v10

    aput-byte v9, v3, v8

    .line 186
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 188
    :cond_0
    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 189
    .local v1, "Type":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->offset:I

    add-int/2addr v8, v2

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->offset:I

    .line 190
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->setRecordType(I)V

    .line 192
    const/4 v2, 0x0

    :goto_1
    if-gt v2, v12, :cond_1

    .line 193
    rsub-int/lit8 v8, v2, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->offset:I

    add-int/2addr v10, v2

    aget-byte v9, v9, v10

    aput-byte v9, v3, v8

    .line 192
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 194
    :cond_1
    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 195
    .local v0, "Size":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->offset:I

    add-int/2addr v8, v2

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->offset:I

    .line 196
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->setRecordSize(I)V

    .line 198
    const/4 v2, 0x0

    :goto_2
    if-gt v2, v12, :cond_2

    .line 199
    rsub-int/lit8 v8, v2, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->offset:I

    add-int/2addr v10, v2

    aget-byte v9, v9, v10

    aput-byte v9, v3, v8

    .line 198
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 200
    :cond_2
    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v4

    .line 201
    .local v4, "leftTopX":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->offset:I

    add-int/2addr v8, v2

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->offset:I

    .line 202
    int-to-float v8, v4

    invoke-static {v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v8

    float-to-int v8, v8

    invoke-virtual {p0, v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->setleftTopX(I)V

    .line 204
    const/4 v2, 0x0

    :goto_3
    if-gt v2, v12, :cond_3

    .line 205
    rsub-int/lit8 v8, v2, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->offset:I

    add-int/2addr v10, v2

    aget-byte v9, v9, v10

    aput-byte v9, v3, v8

    .line 204
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 206
    :cond_3
    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v5

    .line 207
    .local v5, "leftTopY":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->offset:I

    add-int/2addr v8, v2

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->offset:I

    .line 208
    int-to-float v8, v5

    invoke-static {v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v8

    float-to-int v8, v8

    invoke-virtual {p0, v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->setleftTopY(I)V

    .line 210
    const/4 v2, 0x0

    :goto_4
    if-gt v2, v12, :cond_4

    .line 211
    rsub-int/lit8 v8, v2, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->offset:I

    add-int/2addr v10, v2

    aget-byte v9, v9, v10

    aput-byte v9, v3, v8

    .line 210
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 212
    :cond_4
    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v6

    .line 213
    .local v6, "rightBottomX":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->offset:I

    add-int/2addr v8, v2

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->offset:I

    .line 214
    int-to-float v8, v6

    invoke-static {v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v8

    float-to-int v8, v8

    invoke-virtual {p0, v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->setrightBottomX(I)V

    .line 216
    const/4 v2, 0x0

    :goto_5
    if-gt v2, v12, :cond_5

    .line 217
    rsub-int/lit8 v8, v2, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->offset:I

    add-int/2addr v10, v2

    aget-byte v9, v9, v10

    aput-byte v9, v3, v8

    .line 216
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 218
    :cond_5
    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v7

    .line 219
    .local v7, "rightBottomY":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->offset:I

    add-int/2addr v8, v2

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->offset:I

    .line 220
    int-to-float v8, v7

    invoke-static {v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v8

    float-to-int v8, v8

    invoke-virtual {p0, v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->setrightBottomY(I)V

    .line 222
    return-void
.end method

.method public render(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 281
    const/4 v2, 0x0

    .line 282
    .local v2, "i":I
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;-><init>()V

    .line 284
    .local v0, "PaintBrushPropertiesObject":Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 286
    .local v1, "emrSize":I
    if-lez v1, :cond_1

    .line 287
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 288
    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v5, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandleBrush:I

    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->gethandleNumber()I

    move-result v4

    if-ne v5, v4, :cond_4

    .line 293
    :cond_0
    if-ge v2, v1, :cond_1

    .line 294
    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->getColor()I

    move-result v4

    invoke-static {v4}, Lcom/samsung/thumbnail/office/emf/Util/toColorAndroid;->getColor(I)I

    move-result v3

    .line 297
    .local v3, "myColor":I
    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 298
    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    if-eqz v4, :cond_1

    .line 299
    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 301
    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-boolean v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->endPath:Z

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 302
    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    const/4 v5, 0x0

    iput-object v5, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    .line 307
    .end local v3    # "myColor":I
    :cond_1
    if-eq v2, v1, :cond_2

    if-nez v1, :cond_3

    .line 309
    :cond_2
    const/4 v4, 0x5

    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v4, v5, :cond_5

    .line 321
    :cond_3
    :goto_1
    return-void

    .line 287
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 313
    :cond_5
    const/4 v4, 0x4

    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v4, v5, :cond_3

    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    if-eqz v4, :cond_3

    .line 316
    const/high16 v4, -0x1000000

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 317
    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_1
.end method

.method public setBounds(FFFF)V
    .locals 1
    .param p1, "lx"    # F
    .param p2, "ly"    # F
    .param p3, "rx"    # F
    .param p4, "ry"    # F

    .prologue
    .line 243
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->bounds:Landroid/graphics/RectF;

    .line 244
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->bounds:Landroid/graphics/RectF;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 245
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "RecrdSize"    # I

    .prologue
    .line 99
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->RecordSize:I

    .line 100
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "RecrdType"    # I

    .prologue
    .line 80
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->RecordType:I

    .line 81
    return-void
.end method

.method public setleftTopX(I)V
    .locals 0
    .param p1, "lftTopX"    # I

    .prologue
    .line 118
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->leftTopX:I

    .line 119
    return-void
.end method

.method public setleftTopY(I)V
    .locals 0
    .param p1, "lftTopY"    # I

    .prologue
    .line 137
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->leftTopY:I

    .line 138
    return-void
.end method

.method public setrightBottomX(I)V
    .locals 0
    .param p1, "rghtBottomX"    # I

    .prologue
    .line 156
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->rightBottomX:I

    .line 157
    return-void
.end method

.method public setrightBottomY(I)V
    .locals 0
    .param p1, "rghtBottomY"    # I

    .prologue
    .line 175
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_FillPath;->rightBottomY:I

    .line 176
    return-void
.end method
