.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocObjectHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XDocObjectHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    const-string/jumbo v0, "object"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 16
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 34
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 35
    return-void
.end method

.method protected init()V
    .locals 4

    .prologue
    .line 20
    const/4 v3, 0x1

    new-array v0, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 22
    .local v0, "seqDescriptors":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeHandler;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeHandler;-><init>()V

    .line 23
    .local v2, "shapeHandler":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;

    const-string/jumbo v3, "shape"

    invoke-direct {v1, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 25
    .local v1, "shapeDes":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/VMLSeqDescriptor;
    const/4 v3, 0x0

    aput-object v1, v0, v3

    .line 27
    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocObjectHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 28
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 41
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 42
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocObjectHandler;->init()V

    .line 43
    return-void
.end method
