.class public Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLOrderHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DMLOrderHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    const/16 v0, 0x23

    const-string/jumbo v1, "order"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 24
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 29
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 30
    const-string/jumbo v1, "val"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLOrderHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 32
    .local v0, "val":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 33
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getCurSeries()Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->setOrder(I)V

    .line 43
    :goto_0
    return-void

    .line 37
    :cond_0
    if-eqz v0, :cond_1

    .line 38
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getCurSeries()Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->setOrder(I)V

    .line 42
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->addOrderVal(Ljava/lang/String;)V

    goto :goto_0
.end method
