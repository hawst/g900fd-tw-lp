.class public Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;
.super Ljava/lang/Object;
.source "EMR_Polygon.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;


# instance fields
.field private Brush:I

.field private Count:I

.field private Pen:I

.field private RecordSize:I

.field private RecordType:I

.field private aPoints:[B

.field arrayOfPoint:[Landroid/graphics/Point;

.field private emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

.field fileContent:[B

.field private leftTopX:I

.field private leftTopY:I

.field private localPath:Landroid/graphics/Path;

.field private offset:I

.field private rightBottomX:I

.field private rightBottomY:I


# direct methods
.method public constructor <init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V
    .locals 1
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I
    .param p3, "emrGlobals"    # Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .prologue
    const/4 v0, 0x0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->localPath:Landroid/graphics/Path;

    .line 66
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->arrayOfPoint:[Landroid/graphics/Point;

    .line 79
    iput-object p3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .line 80
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->fileContent:[B

    .line 81
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    .line 82
    return-void
.end method


# virtual methods
.method public getBrush()I
    .locals 1

    .prologue
    .line 242
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->Brush:I

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 204
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->Count:I

    return v0
.end method

.method public getLeftTopX()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->leftTopX:I

    return v0
.end method

.method public getLeftTopY()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->leftTopY:I

    return v0
.end method

.method public getPath()Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->localPath:Landroid/graphics/Path;

    return-object v0
.end method

.method public getPen()I
    .locals 1

    .prologue
    .line 261
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->Pen:I

    return v0
.end method

.method public getRecordSize()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->RecordSize:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->RecordType:I

    return v0
.end method

.method public getRightBottomX()I
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->rightBottomX:I

    return v0
.end method

.method public getRightBottomY()I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->rightBottomY:I

    return v0
.end method

.method public getaPoints()[B
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->aPoints:[B

    return-object v0
.end method

.method public prepare()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 352
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->getaPoints()[B

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->setPoints([B)V

    .line 353
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->arrayOfPoint:[Landroid/graphics/Point;

    array-length v6, v6

    const/4 v7, 0x1

    if-gt v6, v7, :cond_0

    .line 378
    :goto_0
    return-void

    .line 356
    :cond_0
    new-instance v6, Landroid/graphics/Path;

    invoke-direct {v6}, Landroid/graphics/Path;-><init>()V

    iput-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->localPath:Landroid/graphics/Path;

    .line 358
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v6, v6, v8

    iget v6, v6, Landroid/graphics/Point;->x:I

    int-to-float v0, v6

    .line 359
    .local v0, "f1":F
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v6, v6, v8

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v1, v6

    .line 360
    .local v1, "f2":F
    const/4 v2, 0x0

    .line 361
    .local v2, "f3":F
    const/4 v3, 0x0

    .line 363
    .local v3, "f4":F
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->localPath:Landroid/graphics/Path;

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v7, v0, v7

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v8, v1, v8

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 364
    const/4 v4, 0x1

    .line 367
    .local v4, "i":I
    :goto_1
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->arrayOfPoint:[Landroid/graphics/Point;

    array-length v5, v6

    .line 368
    .local v5, "j":I
    if-lt v4, v5, :cond_1

    .line 376
    invoke-virtual {p0, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->setCurrentXY(FF)V

    .line 377
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->localPath:Landroid/graphics/Path;

    invoke-virtual {v6}, Landroid/graphics/Path;->close()V

    goto :goto_0

    .line 370
    :cond_1
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v6, v6, v4

    iget v6, v6, Landroid/graphics/Point;->x:I

    int-to-float v2, v6

    .line 371
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v6, v6, v4

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v3, v6

    .line 373
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->localPath:Landroid/graphics/Path;

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v7, v2, v7

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v8, v3, v8

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 374
    add-int/lit8 v4, v4, 0x1

    .line 375
    goto :goto_1
.end method

.method public readEmfPolygon()V
    .locals 14

    .prologue
    .line 278
    const/4 v4, 0x0

    .line 280
    .local v4, "i":I
    const/4 v11, 0x4

    new-array v5, v11, [B

    .line 282
    .local v5, "intConvert":[B
    const/4 v4, 0x0

    :goto_0
    const/4 v11, 0x3

    if-gt v4, v11, :cond_0

    .line 283
    rsub-int/lit8 v11, v4, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    add-int/2addr v13, v4

    aget-byte v12, v12, v13

    aput-byte v12, v5, v11

    .line 282
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 284
    :cond_0
    const/4 v11, 0x0

    invoke-static {v5, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v2

    .line 285
    .local v2, "Type":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    add-int/2addr v11, v4

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    .line 286
    invoke-virtual {p0, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->setRecordType(I)V

    .line 288
    const/4 v4, 0x0

    :goto_1
    const/4 v11, 0x3

    if-gt v4, v11, :cond_1

    .line 289
    rsub-int/lit8 v11, v4, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    add-int/2addr v13, v4

    aget-byte v12, v12, v13

    aput-byte v12, v5, v11

    .line 288
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 290
    :cond_1
    const/4 v11, 0x0

    invoke-static {v5, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 291
    .local v1, "Size":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    add-int/2addr v11, v4

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    .line 292
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->setRecordSize(I)V

    .line 294
    const/4 v4, 0x0

    :goto_2
    const/4 v11, 0x3

    if-gt v4, v11, :cond_2

    .line 295
    rsub-int/lit8 v11, v4, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    add-int/2addr v13, v4

    aget-byte v12, v12, v13

    aput-byte v12, v5, v11

    .line 294
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 296
    :cond_2
    const/4 v11, 0x0

    invoke-static {v5, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v6

    .line 297
    .local v6, "leftTopX":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    add-int/2addr v11, v4

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    .line 298
    int-to-float v11, v6

    invoke-static {v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->setLeftTopX(I)V

    .line 300
    const/4 v4, 0x0

    :goto_3
    const/4 v11, 0x3

    if-gt v4, v11, :cond_3

    .line 301
    rsub-int/lit8 v11, v4, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    add-int/2addr v13, v4

    aget-byte v12, v12, v13

    aput-byte v12, v5, v11

    .line 300
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 302
    :cond_3
    const/4 v11, 0x0

    invoke-static {v5, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v7

    .line 303
    .local v7, "leftTopY":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    add-int/2addr v11, v4

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    .line 304
    int-to-float v11, v7

    invoke-static {v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->setLeftTopY(I)V

    .line 306
    const/4 v4, 0x0

    :goto_4
    const/4 v11, 0x3

    if-gt v4, v11, :cond_4

    .line 307
    rsub-int/lit8 v11, v4, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    add-int/2addr v13, v4

    aget-byte v12, v12, v13

    aput-byte v12, v5, v11

    .line 306
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 308
    :cond_4
    const/4 v11, 0x0

    invoke-static {v5, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v9

    .line 309
    .local v9, "rightBottomX":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    add-int/2addr v11, v4

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    .line 310
    int-to-float v11, v9

    invoke-static {v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->setRightBottomX(I)V

    .line 312
    const/4 v4, 0x0

    :goto_5
    const/4 v11, 0x3

    if-gt v4, v11, :cond_5

    .line 313
    rsub-int/lit8 v11, v4, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    add-int/2addr v13, v4

    aget-byte v12, v12, v13

    aput-byte v12, v5, v11

    .line 312
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 314
    :cond_5
    const/4 v11, 0x0

    invoke-static {v5, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v10

    .line 315
    .local v10, "rightBottomY":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    add-int/2addr v11, v4

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    .line 316
    int-to-float v11, v10

    invoke-static {v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->setRightBottomY(I)V

    .line 318
    const/4 v4, 0x0

    :goto_6
    const/4 v11, 0x3

    if-gt v4, v11, :cond_6

    .line 319
    rsub-int/lit8 v11, v4, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    add-int/2addr v13, v4

    aget-byte v12, v12, v13

    aput-byte v12, v5, v11

    .line 318
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 320
    :cond_6
    const/4 v11, 0x0

    invoke-static {v5, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 321
    .local v0, "Count":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    add-int/2addr v11, v4

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    .line 322
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->setCount(I)V

    .line 324
    add-int/lit8 v3, v1, -0x1c

    .line 325
    .local v3, "buffsize":I
    new-array v8, v3, [B

    .line 326
    .local v8, "pointsBuffer":[B
    const/4 v4, 0x0

    :goto_7
    if-ge v4, v3, :cond_7

    .line 327
    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->fileContent:[B

    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    add-int/2addr v12, v4

    aget-byte v11, v11, v12

    aput-byte v11, v8, v4

    .line 326
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 329
    :cond_7
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    add-int/2addr v11, v4

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->offset:I

    .line 330
    invoke-virtual {p0, v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->setaPoints([B)V

    .line 331
    return-void
.end method

.method public render(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 397
    const/4 v3, 0x0

    .line 398
    .local v3, "i":I
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;-><init>()V

    .line 399
    .local v0, "PaintPolygonPropertiesBrushObject":Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;
    new-instance v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;-><init>()V

    .line 400
    .local v1, "PaintPolygonPropertiesPenObject":Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;
    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v5}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 402
    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 404
    .local v2, "emrSize":I
    if-lez v2, :cond_1

    .line 405
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    .line 406
    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v6, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandleBrush:I

    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->gethandleNumber()I

    move-result v5

    if-ne v6, v5, :cond_8

    .line 411
    :cond_0
    if-ge v3, v2, :cond_1

    .line 412
    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->getColor()I

    move-result v5

    invoke-static {v5}, Lcom/samsung/thumbnail/office/emf/Util/toColorAndroid;->getColor(I)I

    move-result v4

    .line 415
    .local v4, "myColor":I
    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 416
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->getPath()Landroid/graphics/Path;

    move-result-object v5

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 421
    .end local v4    # "myColor":I
    :cond_1
    if-eq v3, v2, :cond_2

    if-nez v2, :cond_3

    .line 422
    :cond_2
    const/4 v5, 0x5

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v6, v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v5, v6, :cond_9

    .line 433
    :cond_3
    :goto_1
    if-lez v2, :cond_5

    .line 434
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v2, :cond_4

    .line 435
    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v6, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandlePen:I

    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->gethandleNumber()I

    move-result v5

    if-ne v6, v5, :cond_a

    .line 439
    :cond_4
    if-ge v3, v2, :cond_5

    .line 440
    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getColor()I

    move-result v5

    invoke-static {v5}, Lcom/samsung/thumbnail/office/emf/Util/toColorAndroid;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 443
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->getPath()Landroid/graphics/Path;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 448
    :cond_5
    if-eq v3, v2, :cond_6

    if-nez v2, :cond_7

    .line 449
    :cond_6
    const/16 v5, 0x8

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v6, v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v5, v6, :cond_b

    .line 457
    :cond_7
    :goto_3
    return-void

    .line 405
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 424
    :cond_9
    const/4 v5, 0x4

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v6, v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v5, v6, :cond_3

    .line 426
    const/high16 v5, -0x1000000

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 427
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->getPath()Landroid/graphics/Path;

    move-result-object v5

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 434
    :cond_a
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 452
    :cond_b
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->getPath()Landroid/graphics/Path;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_3
.end method

.method public setBrush(I)V
    .locals 0
    .param p1, "Brsh"    # I

    .prologue
    .line 252
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->Brush:I

    .line 253
    return-void
.end method

.method public setCount(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 214
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->Count:I

    .line 215
    return-void
.end method

.method public setCurrentXY(FF)V
    .locals 2
    .param p1, "currX"    # F
    .param p2, "currY"    # F

    .prologue
    .line 342
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v1, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v1, p1, v1

    iput v1, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->currentX:F

    .line 343
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v1, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v1, p2, v1

    iput v1, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->currentY:F

    .line 344
    return-void
.end method

.method public setLeftTopX(I)V
    .locals 0
    .param p1, "leftTopX"    # I

    .prologue
    .line 138
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->leftTopX:I

    .line 139
    return-void
.end method

.method public setLeftTopY(I)V
    .locals 0
    .param p1, "leftTopY"    # I

    .prologue
    .line 157
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->leftTopY:I

    .line 158
    return-void
.end method

.method public setPen(I)V
    .locals 0
    .param p1, "Pn"    # I

    .prologue
    .line 271
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->Pen:I

    .line 272
    return-void
.end method

.method public setPoints([B)V
    .locals 12
    .param p1, "points"    # [B

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x0

    const/high16 v9, -0x80000000

    .line 466
    const/4 v2, 0x0

    .line 467
    .local v2, "i":I
    const/4 v4, 0x0

    .line 468
    .local v4, "j":I
    const/4 v5, 0x0

    .line 469
    .local v5, "k":I
    const/4 v6, 0x0

    .line 471
    .local v6, "offset":I
    array-length v7, p1

    shr-int/lit8 v7, v7, 0x3

    new-array v7, v7, [Landroid/graphics/Point;

    iput-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->arrayOfPoint:[Landroid/graphics/Point;

    .line 473
    const/4 v7, 0x4

    new-array v3, v7, [B

    .line 475
    .local v3, "intConvert":[B
    :goto_0
    array-length v7, p1

    if-ge v6, v7, :cond_4

    .line 476
    const/4 v2, 0x0

    :goto_1
    if-gt v2, v11, :cond_0

    .line 477
    rsub-int/lit8 v7, v2, 0x3

    add-int v8, v6, v2

    aget-byte v8, p1, v8

    aput-byte v8, v3, v7

    .line 476
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 478
    :cond_0
    invoke-static {v3, v10}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 479
    .local v0, "X":I
    add-int/2addr v6, v2

    .line 481
    and-int v7, v0, v9

    if-ne v7, v9, :cond_1

    .line 482
    xor-int/lit8 v7, v0, -0x1

    and-int/lit8 v7, v7, -0x1

    add-int/lit8 v7, v7, 0x1

    mul-int/lit8 v0, v7, -0x1

    .line 485
    :cond_1
    const/4 v4, 0x0

    :goto_2
    if-gt v4, v11, :cond_2

    .line 486
    rsub-int/lit8 v7, v4, 0x3

    add-int v8, v6, v4

    aget-byte v8, p1, v8

    aput-byte v8, v3, v7

    .line 485
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 487
    :cond_2
    invoke-static {v3, v10}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getSmallInt([BI)I

    move-result v1

    .line 489
    .local v1, "Y":I
    and-int v7, v1, v9

    if-ne v7, v9, :cond_3

    .line 490
    xor-int/lit8 v7, v1, -0x1

    and-int/lit8 v7, v7, -0x1

    add-int/lit8 v7, v7, 0x1

    mul-int/lit8 v1, v7, -0x1

    .line 493
    :cond_3
    add-int/2addr v6, v4

    .line 494
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->arrayOfPoint:[Landroid/graphics/Point;

    new-instance v8, Landroid/graphics/Point;

    invoke-direct {v8, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    aput-object v8, v7, v5

    .line 496
    add-int/lit8 v5, v5, 0x1

    .line 497
    goto :goto_0

    .line 499
    .end local v0    # "X":I
    .end local v1    # "Y":I
    :cond_4
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 119
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->RecordSize:I

    .line 120
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 100
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->RecordType:I

    .line 101
    return-void
.end method

.method public setRightBottomX(I)V
    .locals 0
    .param p1, "rightBottomX"    # I

    .prologue
    .line 176
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->rightBottomX:I

    .line 177
    return-void
.end method

.method public setRightBottomY(I)V
    .locals 0
    .param p1, "rightBottomY"    # I

    .prologue
    .line 195
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->rightBottomY:I

    .line 196
    return-void
.end method

.method public setaPoints([B)V
    .locals 0
    .param p1, "aPoints"    # [B

    .prologue
    .line 233
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polygon;->aPoints:[B

    .line 234
    return-void
.end method
