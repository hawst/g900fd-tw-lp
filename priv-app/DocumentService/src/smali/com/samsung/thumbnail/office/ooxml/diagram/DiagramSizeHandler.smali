.class public Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSizeHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "DiagramSizeHandler.java"


# instance fields
.field private xfrmDgm:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IXfrmHandlerDgm;

.field private xfrmTxBody:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler$IXfrmTxBodyHandler;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IXfrmHandlerDgm;)V
    .locals 2
    .param p1, "xfrmDgm"    # Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IXfrmHandlerDgm;

    .prologue
    .line 21
    const/16 v0, 0x1f

    const-string/jumbo v1, "ext"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 22
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSizeHandler;->xfrmDgm:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IXfrmHandlerDgm;

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSizeHandler;->xfrmTxBody:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler$IXfrmTxBodyHandler;

    .line 24
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler$IXfrmTxBodyHandler;)V
    .locals 2
    .param p1, "xfrmTxBody"    # Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler$IXfrmTxBodyHandler;

    .prologue
    .line 27
    const/16 v0, 0x1f

    const-string/jumbo v1, "ext"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 28
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSizeHandler;->xfrmTxBody:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler$IXfrmTxBodyHandler;

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSizeHandler;->xfrmDgm:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IXfrmHandlerDgm;

    .line 30
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 8
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 35
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 37
    const-string/jumbo v0, "cx"

    invoke-virtual {p0, p3, v0, p1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSizeHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v6

    .line 38
    .local v6, "cxVal":Ljava/lang/String;
    const-string/jumbo v0, "cy"

    invoke-virtual {p0, p3, v0, p1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSizeHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v7

    .line 40
    .local v7, "cyVal":Ljava/lang/String;
    const-wide/16 v2, 0x0

    .line 41
    .local v2, "cx":J
    const-wide/16 v4, 0x0

    .line 42
    .local v4, "cy":J
    if-eqz v6, :cond_0

    .line 43
    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 45
    :cond_0
    if-eqz v7, :cond_1

    .line 46
    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 52
    :cond_1
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSizeHandler;->xfrmDgm:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IXfrmHandlerDgm;

    if-eqz v0, :cond_3

    .line 53
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSizeHandler;->xfrmDgm:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IXfrmHandlerDgm;

    move-object v1, p1

    invoke-interface/range {v0 .. v5}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IXfrmHandlerDgm;->setSize(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;JJ)V

    .line 57
    :cond_2
    :goto_0
    return-void

    .line 54
    :cond_3
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSizeHandler;->xfrmTxBody:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler$IXfrmTxBodyHandler;

    if-eqz v0, :cond_2

    .line 55
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSizeHandler;->xfrmTxBody:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler$IXfrmTxBodyHandler;

    move-object v1, p1

    invoke-interface/range {v0 .. v5}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler$IXfrmTxBodyHandler;->setTxXfrmSize(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;JJ)V

    goto :goto_0
.end method
