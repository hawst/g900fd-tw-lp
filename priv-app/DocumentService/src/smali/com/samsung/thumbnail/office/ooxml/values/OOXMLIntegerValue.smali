.class public Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;
.super Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;
.source "OOXMLIntegerValue.java"


# instance fields
.field private intValue:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;-><init>()V

    .line 13
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;->parseString(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;->intValue:I

    .line 14
    return-void
.end method

.method private parseString(Ljava/lang/String;)I
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 22
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 26
    :goto_0
    return v1

    .line 23
    :catch_0
    move-exception v0

    .line 24
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Number Format Exception :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 26
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;->intValue:I

    return v0
.end method
