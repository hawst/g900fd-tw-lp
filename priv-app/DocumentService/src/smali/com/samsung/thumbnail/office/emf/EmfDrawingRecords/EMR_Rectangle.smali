.class public Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;
.super Ljava/lang/Object;
.source "EMR_Rectangle.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;


# instance fields
.field private Size:I

.field private Type:I

.field protected bounds:Landroid/graphics/RectF;

.field private emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

.field fileContent:[B

.field private leftTopX:I

.field private leftTopY:I

.field localPath:Landroid/graphics/Path;

.field private offset:I

.field private rightBottomX:I

.field private rightBottomY:I


# direct methods
.method public constructor <init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V
    .locals 1
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I
    .param p3, "emrGlobals"    # Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .prologue
    const/4 v0, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->bounds:Landroid/graphics/RectF;

    .line 233
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->localPath:Landroid/graphics/Path;

    .line 64
    iput-object p3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .line 65
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->fileContent:[B

    .line 66
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->offset:I

    .line 67
    return-void
.end method


# virtual methods
.method public getPath()Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->localPath:Landroid/graphics/Path;

    return-object v0
.end method

.method public getRecordSize()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->Size:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->Type:I

    return v0
.end method

.method public getleftTopX()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->leftTopX:I

    return v0
.end method

.method public getleftTopY()I
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->leftTopY:I

    return v0
.end method

.method public getrightBottomX()I
    .locals 1

    .prologue
    .line 151
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->rightBottomX:I

    return v0
.end method

.method public getrightBottomY()I
    .locals 1

    .prologue
    .line 170
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->rightBottomY:I

    return v0
.end method

.method public prepare()V
    .locals 7

    .prologue
    .line 268
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->getleftTopX()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->getleftTopY()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float/2addr v3, v4

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->getrightBottomX()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float/2addr v4, v5

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->getrightBottomY()I

    move-result v5

    int-to-float v5, v5

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v6, v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float/2addr v5, v6

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->setBounds(FFFF)V

    .line 271
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    iput-object v2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->localPath:Landroid/graphics/Path;

    .line 272
    iget-object v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->bounds:Landroid/graphics/RectF;

    .line 273
    .local v1, "localRectF":Landroid/graphics/RectF;
    sget-object v0, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    .line 274
    .local v0, "localDirection":Landroid/graphics/Path$Direction;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->localPath:Landroid/graphics/Path;

    invoke-virtual {v2, v1, v0}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 275
    return-void
.end method

.method public readEmfrectangle()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x0

    .line 187
    const/4 v2, 0x0

    .line 189
    .local v2, "i":I
    const/4 v8, 0x4

    new-array v3, v8, [B

    .line 191
    .local v3, "intConvert":[B
    const/4 v2, 0x0

    :goto_0
    if-gt v2, v12, :cond_0

    .line 192
    rsub-int/lit8 v8, v2, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->offset:I

    add-int/2addr v10, v2

    aget-byte v9, v9, v10

    aput-byte v9, v3, v8

    .line 191
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 193
    :cond_0
    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 194
    .local v1, "Type":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->offset:I

    add-int/2addr v8, v2

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->offset:I

    .line 195
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->setRecordType(I)V

    .line 197
    const/4 v2, 0x0

    :goto_1
    if-gt v2, v12, :cond_1

    .line 198
    rsub-int/lit8 v8, v2, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->offset:I

    add-int/2addr v10, v2

    aget-byte v9, v9, v10

    aput-byte v9, v3, v8

    .line 197
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 199
    :cond_1
    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 200
    .local v0, "Size":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->offset:I

    add-int/2addr v8, v2

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->offset:I

    .line 201
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->setRecordSize(I)V

    .line 203
    const/4 v2, 0x0

    :goto_2
    if-gt v2, v12, :cond_2

    .line 204
    rsub-int/lit8 v8, v2, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->offset:I

    add-int/2addr v10, v2

    aget-byte v9, v9, v10

    aput-byte v9, v3, v8

    .line 203
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 205
    :cond_2
    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v4

    .line 206
    .local v4, "leftTopX":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->offset:I

    add-int/2addr v8, v2

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->offset:I

    .line 207
    invoke-virtual {p0, v4}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->setleftTopX(I)V

    .line 209
    const/4 v2, 0x0

    :goto_3
    if-gt v2, v12, :cond_3

    .line 210
    rsub-int/lit8 v8, v2, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->offset:I

    add-int/2addr v10, v2

    aget-byte v9, v9, v10

    aput-byte v9, v3, v8

    .line 209
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 211
    :cond_3
    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v5

    .line 212
    .local v5, "leftTopY":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->offset:I

    add-int/2addr v8, v2

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->offset:I

    .line 213
    invoke-virtual {p0, v5}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->setleftTopY(I)V

    .line 215
    const/4 v2, 0x0

    :goto_4
    if-gt v2, v12, :cond_4

    .line 216
    rsub-int/lit8 v8, v2, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->offset:I

    add-int/2addr v10, v2

    aget-byte v9, v9, v10

    aput-byte v9, v3, v8

    .line 215
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 217
    :cond_4
    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v6

    .line 218
    .local v6, "rightBottomX":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->offset:I

    add-int/2addr v8, v2

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->offset:I

    .line 219
    invoke-virtual {p0, v6}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->setrightBottomX(I)V

    .line 221
    const/4 v2, 0x0

    :goto_5
    if-gt v2, v12, :cond_5

    .line 222
    rsub-int/lit8 v8, v2, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->offset:I

    add-int/2addr v10, v2

    aget-byte v9, v9, v10

    aput-byte v9, v3, v8

    .line 221
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 223
    :cond_5
    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v7

    .line 224
    .local v7, "rightBottomY":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->offset:I

    add-int/2addr v8, v2

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->offset:I

    .line 225
    invoke-virtual {p0, v7}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->setrightBottomY(I)V

    .line 227
    return-void
.end method

.method public render(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 285
    const/4 v3, 0x0

    .line 286
    .local v3, "i":I
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;-><init>()V

    .line 287
    .local v0, "PaintRectangleBrushPropertiesObject":Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;
    new-instance v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;-><init>()V

    .line 288
    .local v1, "PaintRectanglePenPropertiesObject":Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;
    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v5}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 290
    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 292
    .local v2, "emrSize":I
    if-lez v2, :cond_1

    .line 293
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    .line 294
    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v6, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandleBrush:I

    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->gethandleNumber()I

    move-result v5

    if-ne v6, v5, :cond_8

    .line 299
    :cond_0
    if-ge v3, v2, :cond_1

    .line 300
    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->getColor()I

    move-result v5

    invoke-static {v5}, Lcom/samsung/thumbnail/office/emf/Util/toColorAndroid;->getColor(I)I

    move-result v4

    .line 303
    .local v4, "myColor":I
    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 304
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->getPath()Landroid/graphics/Path;

    move-result-object v5

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 309
    .end local v4    # "myColor":I
    :cond_1
    if-eq v3, v2, :cond_2

    if-nez v2, :cond_3

    .line 311
    :cond_2
    const/4 v5, 0x5

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v6, v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v5, v6, :cond_9

    .line 322
    :cond_3
    :goto_1
    if-lez v2, :cond_5

    .line 323
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v2, :cond_4

    .line 324
    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v6, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandlePen:I

    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->gethandleNumber()I

    move-result v5

    if-ne v6, v5, :cond_a

    .line 328
    :cond_4
    if-ge v3, v2, :cond_5

    .line 329
    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getPenStyle()I

    move-result v5

    invoke-static {v5}, Lcom/samsung/thumbnail/office/emf/Util/toStyleAndroid;->getStyle(I)Landroid/graphics/Paint$Style;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 332
    iget-object v5, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getColor()I

    move-result v5

    invoke-static {v5}, Lcom/samsung/thumbnail/office/emf/Util/toColorAndroid;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 335
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->getPath()Landroid/graphics/Path;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 340
    :cond_5
    if-eq v3, v2, :cond_6

    if-nez v2, :cond_7

    .line 342
    :cond_6
    const/16 v5, 0x8

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v6, v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v5, v6, :cond_b

    .line 349
    :cond_7
    :goto_3
    return-void

    .line 293
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 313
    :cond_9
    const/4 v5, 0x4

    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v6, v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v5, v6, :cond_3

    .line 315
    const/high16 v5, -0x1000000

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 316
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->getPath()Landroid/graphics/Path;

    move-result-object v5

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 323
    :cond_a
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 345
    :cond_b
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->getPath()Landroid/graphics/Path;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_3
.end method

.method public setBounds(FFFF)V
    .locals 1
    .param p1, "lx"    # F
    .param p2, "ly"    # F
    .param p3, "rx"    # F
    .param p4, "ry"    # F

    .prologue
    .line 248
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->bounds:Landroid/graphics/RectF;

    .line 249
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->bounds:Landroid/graphics/RectF;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 250
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "RecrdSize"    # I

    .prologue
    .line 104
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->Size:I

    .line 105
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "RecrdType"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->Type:I

    .line 86
    return-void
.end method

.method public setleftTopX(I)V
    .locals 0
    .param p1, "lftTopX"    # I

    .prologue
    .line 123
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->leftTopX:I

    .line 124
    return-void
.end method

.method public setleftTopY(I)V
    .locals 0
    .param p1, "lftTopY"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->leftTopY:I

    .line 143
    return-void
.end method

.method public setrightBottomX(I)V
    .locals 0
    .param p1, "rghtBottomX"    # I

    .prologue
    .line 161
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->rightBottomX:I

    .line 162
    return-void
.end method

.method public setrightBottomY(I)V
    .locals 0
    .param p1, "rghtBottomY"    # I

    .prologue
    .line 180
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Rectangle;->rightBottomY:I

    .line 181
    return-void
.end method
