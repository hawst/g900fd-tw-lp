.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelStyleRFontHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelStyleRFontHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    const-string/jumbo v0, "rFont"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 22
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 27
    const-string/jumbo v2, "val"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelStyleRFontHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 28
    .local v0, "val":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v1

    .line 30
    .local v1, "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v2

    iget v3, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    invoke-virtual {v2, v0, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->addExSharedStringStyleFontname(Ljava/lang/String;I)V

    .line 33
    return-void
.end method
