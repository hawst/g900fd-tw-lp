.class public Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;
.super Lorg/apache/poi/POIXMLDocumentPart;
.source "XWPFChart.java"


# instance fields
.field private chartType:I

.field private curSeries:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

.field private grouping:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

.field private height:F

.field private parsed:Z

.field private scatterStyle:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

.field private seriesLst:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;",
            ">;"
        }
    .end annotation
.end field

.field private shapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

.field private styles:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;

.field private width:F


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Lorg/apache/poi/POIXMLDocumentPart;-><init>()V

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->chartType:I

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->parsed:Z

    .line 79
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->createChart()V

    .line 80
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V
    .locals 1
    .param p1, "part"    # Lorg/apache/poi/openxml4j/opc/PackagePart;
    .param p2, "rel"    # Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/POIXMLDocumentPart;-><init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->chartType:I

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->parsed:Z

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->seriesLst:Ljava/util/ArrayList;

    .line 108
    return-void
.end method

.method private createChart()V
    .locals 0

    .prologue
    .line 239
    return-void
.end method


# virtual methods
.method public addSeries(Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;)V
    .locals 1
    .param p1, "series"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->curSeries:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    .line 116
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->seriesLst:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    return-void
.end method

.method public getChartType()I
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->chartType:I

    return v0
.end method

.method public getCurSeries()Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->curSeries:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    return-object v0
.end method

.method public getGrouping()Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->grouping:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

    return-object v0
.end method

.method public getHeight()F
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->height:F

    return v0
.end method

.method public getScatterStyle()Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->scatterStyle:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    return-object v0
.end method

.method public getSeries()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->seriesLst:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getShapeProps()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->shapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    return-object v0
.end method

.method public getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->styles:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;

    return-object v0
.end method

.method public getWidth()F
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->width:F

    return v0
.end method

.method public parseChart()V
    .locals 7

    .prologue
    .line 160
    iget-boolean v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->parsed:Z

    if-eqz v3, :cond_1

    .line 183
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XDocChartParser;

    invoke-direct {v0, p0}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XDocChartParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)V

    .line 165
    .local v0, "chartParser":Lcom/samsung/thumbnail/office/ooxml/word/chart/XDocChartParser;
    const/4 v2, 0x0

    .line 168
    .local v2, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 170
    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XDocChartParser;->parse(Ljava/io/InputStream;)V

    .line 171
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->parsed:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    if-eqz v2, :cond_0

    .line 177
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 178
    :catch_0
    move-exception v1

    .line 179
    .local v1, "e":Ljava/io/IOException;
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 172
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 173
    .restart local v1    # "e":Ljava/io/IOException;
    :try_start_2
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 175
    if-eqz v2, :cond_0

    .line 177
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 178
    :catch_2
    move-exception v1

    .line 179
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 175
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_2

    .line 177
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 180
    :cond_2
    :goto_1
    throw v3

    .line 178
    :catch_3
    move-exception v1

    .line 179
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v4, "DocumentService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setChartType(I)V
    .locals 0
    .param p1, "chartType"    # I

    .prologue
    .line 190
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->chartType:I

    .line 191
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "mContex"    # Landroid/content/Context;

    .prologue
    .line 112
    return-void
.end method

.method public setGrouping(Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;)V
    .locals 0
    .param p1, "grouping"    # Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->grouping:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EChartGrouping;

    .line 145
    return-void
.end method

.method public setHeight(F)V
    .locals 0
    .param p1, "height"    # F

    .prologue
    .line 136
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->height:F

    .line 137
    return-void
.end method

.method public setScatterStyle(Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;)V
    .locals 0
    .param p1, "scatterStyle"    # Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->scatterStyle:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    .line 153
    return-void
.end method

.method public setShapeProps(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 198
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->shapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 199
    return-void
.end method

.method public setWidth(F)V
    .locals 0
    .param p1, "width"    # F

    .prologue
    .line 128
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->width:F

    .line 129
    return-void
.end method
