.class public final enum Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;
.super Ljava/lang/Enum;
.source "XWPFShapeStyle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EShapeType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum ACCENTBORDERCALLOUT1:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum ACCENTBORDERCALLOUT2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum ACCENTBORDERCALLOUT3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum ACCENTCALLOUT1:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum ACCENTCALLOUT2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum ACCENTCALLOUT3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum ARC:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum ARCCONNECTOR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum BENTCONNECTOR3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum BEVEL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum BLOCKARC:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum BORDERCALLOUT1:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum BORDERCALLOUT2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum BORDERCALLOUT3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum BORDER_CALLOUT1:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum BORDER_CALLOUT2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum BORDER_CALLOUT3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum BRACEPAIR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum BRACKETPAIR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum CALLOUT1:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum CALLOUT2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum CALLOUT3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum CAN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum CHEVRON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum CUBE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum CURVE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum CURVEDCONNECTOR3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum CURVEDDOWNARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum CURVEDLEFTARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum CURVEDRIGHTARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum CURVEDUPARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum DIAMOND:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum DOUBLEWAVE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum DOWNARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum DOWNARROWCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum ELLIPSE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum ELLIPSERIBBON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum ELLIPSERIBBON2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTALTERNATEPROCESS:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTCOLLATE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTCONNECTOR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTDECISION:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTDELAY:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTDISPLAY:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTDOCUMENT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTEXTRACT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTINPUTOUTPUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTINTERNALSTORAGE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTMAGNETICDISK:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTMAGNETICDRUM:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTMAGNETICTAPE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTMANUALINPUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTMANUALOPERATION:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTMERGE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTMULTIDOCUMENT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTOFFPAGECONNECTOR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTONLINESTORAGE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTOR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTPREDEFINEDPROCESS:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTPREPARATION:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTPROCESS:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTPUNCHEDCARD:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTPUNCHEDTAPE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTSORT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTSUMMINGJUNCTION:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOWCHARTTERMINATOR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FLOW_CHART_SHAPES:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum FOLDEDCORNER:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum HEXAGON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum HOMEPLATE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum HORIZONTALSCROLL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum IMAGE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum IRREGULARSEAL1:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum IRREGULARSEAL2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum IRREGULAR_SEAL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum LEFTARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum LEFTARROWCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum LEFTBRACE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum LEFTBRACKET:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum LEFTRIGHTARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum LEFTRIGHTARROWCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum LIGHTNINGBOLT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum LINE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum LINECONNECTOR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum MOON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum NOTCHEDRIGHTARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum OCTAGON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum OVAL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum PARALLELOGRAM:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum PENTAGON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum PLAQUE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum PLUS:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum POLYLINE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum RECT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum RIBBON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum RIBBON2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum RIBBON_SHAPES:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum RIGHTARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum RIGHTARROWCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum RIGHTBRACE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum RIGHTBRACKET:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum RIGHTTRIANGLE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum ROUNDRECT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum SCROLL_SHAPES:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum SHAPE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum SHAPETYPE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum SMILEYFACE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum STAR16:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum STAR24:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum STAR32:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum STAR4:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum STAR8:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum STARSHAPES:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum SUN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum TEXTONPATH:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum TRAPEZIUM:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum TRAPEZOID:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum TRIANGLE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum UNKNOWN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum UPARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum UPARROWCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum UPDOWNARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum VERTICALSCROLL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum WAVE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum WAVE_SHAPES:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum WEDGERECTCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum WEDGEROUNDRECTCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field public static final enum WEDGE_CALLOUTS:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 15
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "RECT"

    invoke-direct {v0, v1, v3}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RECT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "OVAL"

    invoke-direct {v0, v1, v4}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->OVAL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "ELLIPSE"

    invoke-direct {v0, v1, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ELLIPSE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "POLYLINE"

    invoke-direct {v0, v1, v6}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->POLYLINE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "ROUNDRECT"

    invoke-direct {v0, v1, v7}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ROUNDRECT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "LINE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LINE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "CURVE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CURVE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "ARC"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ARC:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "BLOCKARC"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BLOCKARC:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "IMAGE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->IMAGE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "SHAPE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->SHAPE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    .line 19
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "SHAPETYPE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->SHAPETYPE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "UNKNOWN"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->UNKNOWN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "LINECONNECTOR"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LINECONNECTOR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "ARCCONNECTOR"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ARCCONNECTOR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    .line 27
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "RIGHTTRIANGLE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RIGHTTRIANGLE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "DIAMOND"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->DIAMOND:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "PARALLELOGRAM"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->PARALLELOGRAM:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "TRAPEZIUM"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->TRAPEZIUM:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "TRAPEZOID"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->TRAPEZOID:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "OCTAGON"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->OCTAGON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "PENTAGON"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->PENTAGON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "PLUS"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->PLUS:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "CAN"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CAN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "HEXAGON"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->HEXAGON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "PLAQUE"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->PLAQUE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "CHEVRON"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CHEVRON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "HOMEPLATE"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->HOMEPLATE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "LIGHTNINGBOLT"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LIGHTNINGBOLT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "MOON"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->MOON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "CUBE"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CUBE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FOLDEDCORNER"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FOLDEDCORNER:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "BEVEL"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BEVEL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "SUN"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->SUN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "SMILEYFACE"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->SMILEYFACE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "LEFTBRACE"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LEFTBRACE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "RIGHTBRACE"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RIGHTBRACE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "BRACEPAIR"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BRACEPAIR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "BRACKETPAIR"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BRACKETPAIR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "LEFTBRACKET"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LEFTBRACKET:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "RIGHTBRACKET"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RIGHTBRACKET:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "RIGHTARROWCALLOUT"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RIGHTARROWCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "LEFTARROWCALLOUT"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LEFTARROWCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "UPARROWCALLOUT"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->UPARROWCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "DOWNARROWCALLOUT"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->DOWNARROWCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "LEFTRIGHTARROWCALLOUT"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LEFTRIGHTARROWCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "CURVEDRIGHTARROW"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CURVEDRIGHTARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "CURVEDLEFTARROW"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CURVEDLEFTARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "CURVEDUPARROW"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CURVEDUPARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "CURVEDDOWNARROW"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CURVEDDOWNARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "DOWNARROW"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->DOWNARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "UPARROW"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->UPARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "RIGHTARROW"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RIGHTARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "LEFTARROW"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LEFTARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "LEFTRIGHTARROW"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LEFTRIGHTARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "UPDOWNARROW"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->UPDOWNARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "NOTCHEDRIGHTARROW"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->NOTCHEDRIGHTARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    .line 29
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "TRIANGLE"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->TRIANGLE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    .line 31
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOW_CHART_SHAPES"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOW_CHART_SHAPES:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "IRREGULAR_SEAL"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->IRREGULAR_SEAL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "STARSHAPES"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->STARSHAPES:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "RIBBON_SHAPES"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RIBBON_SHAPES:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "SCROLL_SHAPES"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->SCROLL_SHAPES:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "WAVE_SHAPES"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->WAVE_SHAPES:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "WEDGE_CALLOUTS"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->WEDGE_CALLOUTS:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "BORDER_CALLOUT1"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BORDER_CALLOUT1:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "BORDER_CALLOUT2"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BORDER_CALLOUT2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "BORDER_CALLOUT3"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BORDER_CALLOUT3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    .line 34
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTPROCESS"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTPROCESS:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTALTERNATEPROCESS"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTALTERNATEPROCESS:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTDECISION"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTDECISION:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTINPUTOUTPUT"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTINPUTOUTPUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTPREDEFINEDPROCESS"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTPREDEFINEDPROCESS:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTINTERNALSTORAGE"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTINTERNALSTORAGE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTDOCUMENT"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTDOCUMENT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTMULTIDOCUMENT"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTMULTIDOCUMENT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTTERMINATOR"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTTERMINATOR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTPREPARATION"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTPREPARATION:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTMANUALINPUT"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTMANUALINPUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTMANUALOPERATION"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTMANUALOPERATION:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTCONNECTOR"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTCONNECTOR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTOFFPAGECONNECTOR"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTOFFPAGECONNECTOR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTPUNCHEDCARD"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTPUNCHEDCARD:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTPUNCHEDTAPE"

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTPUNCHEDTAPE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTSUMMINGJUNCTION"

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTSUMMINGJUNCTION:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTOR"

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTOR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTCOLLATE"

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTCOLLATE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTSORT"

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTSORT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTEXTRACT"

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTEXTRACT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTMERGE"

    const/16 v2, 0x59

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTMERGE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTONLINESTORAGE"

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTONLINESTORAGE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTDELAY"

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTDELAY:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTMAGNETICTAPE"

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTMAGNETICTAPE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTMAGNETICDISK"

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTMAGNETICDISK:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTMAGNETICDRUM"

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTMAGNETICDRUM:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "FLOWCHARTDISPLAY"

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTDISPLAY:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    .line 37
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "IRREGULARSEAL1"

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->IRREGULARSEAL1:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "IRREGULARSEAL2"

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->IRREGULARSEAL2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "STAR4"

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->STAR4:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "STAR8"

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->STAR8:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "STAR16"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->STAR16:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "STAR24"

    const/16 v2, 0x65

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->STAR24:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "STAR32"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->STAR32:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "RIBBON2"

    const/16 v2, 0x67

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RIBBON2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "RIBBON"

    const/16 v2, 0x68

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RIBBON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "ELLIPSERIBBON2"

    const/16 v2, 0x69

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ELLIPSERIBBON2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "ELLIPSERIBBON"

    const/16 v2, 0x6a

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ELLIPSERIBBON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "VERTICALSCROLL"

    const/16 v2, 0x6b

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->VERTICALSCROLL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "HORIZONTALSCROLL"

    const/16 v2, 0x6c

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->HORIZONTALSCROLL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "WAVE"

    const/16 v2, 0x6d

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->WAVE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "DOUBLEWAVE"

    const/16 v2, 0x6e

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->DOUBLEWAVE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    .line 40
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "WEDGERECTCALLOUT"

    const/16 v2, 0x6f

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->WEDGERECTCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "WEDGEROUNDRECTCALLOUT"

    const/16 v2, 0x70

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->WEDGEROUNDRECTCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "BORDERCALLOUT1"

    const/16 v2, 0x71

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BORDERCALLOUT1:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "BORDERCALLOUT2"

    const/16 v2, 0x72

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BORDERCALLOUT2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "BORDERCALLOUT3"

    const/16 v2, 0x73

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BORDERCALLOUT3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "ACCENTCALLOUT1"

    const/16 v2, 0x74

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ACCENTCALLOUT1:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "ACCENTCALLOUT2"

    const/16 v2, 0x75

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ACCENTCALLOUT2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "ACCENTCALLOUT3"

    const/16 v2, 0x76

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ACCENTCALLOUT3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "CALLOUT1"

    const/16 v2, 0x77

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CALLOUT1:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "CALLOUT2"

    const/16 v2, 0x78

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CALLOUT2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "CALLOUT3"

    const/16 v2, 0x79

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CALLOUT3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "ACCENTBORDERCALLOUT1"

    const/16 v2, 0x7a

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ACCENTBORDERCALLOUT1:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "ACCENTBORDERCALLOUT2"

    const/16 v2, 0x7b

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ACCENTBORDERCALLOUT2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "ACCENTBORDERCALLOUT3"

    const/16 v2, 0x7c

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ACCENTBORDERCALLOUT3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    .line 42
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "BENTCONNECTOR3"

    const/16 v2, 0x7d

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BENTCONNECTOR3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "CURVEDCONNECTOR3"

    const/16 v2, 0x7e

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CURVEDCONNECTOR3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    const-string/jumbo v1, "TEXTONPATH"

    const/16 v2, 0x7f

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->TEXTONPATH:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    .line 14
    const/16 v0, 0x80

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RECT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->OVAL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ELLIPSE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->POLYLINE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ROUNDRECT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LINE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CURVE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ARC:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BLOCKARC:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->IMAGE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->SHAPE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->SHAPETYPE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->UNKNOWN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LINECONNECTOR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ARCCONNECTOR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RIGHTTRIANGLE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->DIAMOND:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->PARALLELOGRAM:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->TRAPEZIUM:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->TRAPEZOID:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->OCTAGON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->PENTAGON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->PLUS:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CAN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->HEXAGON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->PLAQUE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CHEVRON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->HOMEPLATE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LIGHTNINGBOLT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->MOON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CUBE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FOLDEDCORNER:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BEVEL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->SUN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->SMILEYFACE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LEFTBRACE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RIGHTBRACE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BRACEPAIR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BRACKETPAIR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LEFTBRACKET:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RIGHTBRACKET:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RIGHTARROWCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LEFTARROWCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->UPARROWCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->DOWNARROWCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LEFTRIGHTARROWCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CURVEDRIGHTARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CURVEDLEFTARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CURVEDUPARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CURVEDDOWNARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->DOWNARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->UPARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RIGHTARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LEFTARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LEFTRIGHTARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->UPDOWNARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->NOTCHEDRIGHTARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->TRIANGLE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOW_CHART_SHAPES:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->IRREGULAR_SEAL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->STARSHAPES:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RIBBON_SHAPES:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->SCROLL_SHAPES:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->WAVE_SHAPES:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->WEDGE_CALLOUTS:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BORDER_CALLOUT1:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BORDER_CALLOUT2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BORDER_CALLOUT3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTPROCESS:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTALTERNATEPROCESS:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTDECISION:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTINPUTOUTPUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTPREDEFINEDPROCESS:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTINTERNALSTORAGE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTDOCUMENT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTMULTIDOCUMENT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTTERMINATOR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTPREPARATION:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTMANUALINPUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTMANUALOPERATION:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTCONNECTOR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTOFFPAGECONNECTOR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTPUNCHEDCARD:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTPUNCHEDTAPE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTSUMMINGJUNCTION:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTOR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTCOLLATE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTSORT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTEXTRACT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTMERGE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTONLINESTORAGE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTDELAY:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTMAGNETICTAPE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTMAGNETICDISK:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTMAGNETICDRUM:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTDISPLAY:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x60

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->IRREGULARSEAL1:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x61

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->IRREGULARSEAL2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x62

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->STAR4:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x63

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->STAR8:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->STAR16:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x65

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->STAR24:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x66

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->STAR32:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x67

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RIBBON2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x68

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RIBBON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x69

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ELLIPSERIBBON2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ELLIPSERIBBON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->VERTICALSCROLL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->HORIZONTALSCROLL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->WAVE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->DOUBLEWAVE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->WEDGERECTCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x70

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->WEDGEROUNDRECTCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x71

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BORDERCALLOUT1:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x72

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BORDERCALLOUT2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x73

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BORDERCALLOUT3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x74

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ACCENTCALLOUT1:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x75

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ACCENTCALLOUT2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x76

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ACCENTCALLOUT3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x77

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CALLOUT1:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x78

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CALLOUT2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x79

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CALLOUT3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ACCENTBORDERCALLOUT1:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ACCENTBORDERCALLOUT2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ACCENTBORDERCALLOUT3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BENTCONNECTOR3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CURVEDCONNECTOR3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->TEXTONPATH:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static parseStringPrefix(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;
    .locals 6
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->values()[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 50
    .local v3, "t":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 54
    .end local v3    # "t":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;
    :goto_1
    return-object v3

    .line 49
    .restart local v3    # "t":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 54
    .end local v3    # "t":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;
    :cond_1
    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->UNKNOWN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 14
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    return-object v0
.end method


# virtual methods
.method public getType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)Ljava/lang/String;
    .locals 2
    .param p1, "type"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    .prologue
    .line 45
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->name()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
