.class public Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;
.super Ljava/lang/Object;
.source "EMR_Polyline.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;


# instance fields
.field private Size:I

.field private Type:I

.field private aPoints:[B

.field arrayOfPoint:[Landroid/graphics/Point;

.field private bottom:I

.field private count:I

.field private emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

.field fileContent:[B

.field private left:I

.field localPath:Landroid/graphics/Path;

.field private offset:I

.field private right:I

.field private top:I


# direct methods
.method public constructor <init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V
    .locals 1
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I
    .param p3, "emrGlobals"    # Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .prologue
    const/4 v0, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 341
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->localPath:Landroid/graphics/Path;

    .line 344
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->arrayOfPoint:[Landroid/graphics/Point;

    .line 69
    iput-object p3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .line 70
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->fileContent:[B

    .line 71
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    .line 72
    return-void
.end method


# virtual methods
.method public getBottom()I
    .locals 1

    .prologue
    .line 194
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->bottom:I

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->count:I

    return v0
.end method

.method public getLeft()I
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->left:I

    return v0
.end method

.method public getLocalPath()Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->localPath:Landroid/graphics/Path;

    return-object v0
.end method

.method public getRecordSize()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->Size:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->Type:I

    return v0
.end method

.method public getRight()I
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->right:I

    return v0
.end method

.method public getTop()I
    .locals 1

    .prologue
    .line 156
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->top:I

    return v0
.end method

.method public getaPoints()[B
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->aPoints:[B

    return-object v0
.end method

.method public prepare()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 352
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->getaPoints()[B

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->setPoints([B)V

    .line 353
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->arrayOfPoint:[Landroid/graphics/Point;

    array-length v6, v6

    const/4 v7, 0x1

    if-gt v6, v7, :cond_1

    .line 372
    :cond_0
    return-void

    .line 356
    :cond_1
    new-instance v6, Landroid/graphics/Path;

    invoke-direct {v6}, Landroid/graphics/Path;-><init>()V

    iput-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->localPath:Landroid/graphics/Path;

    .line 357
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v6, v6, v8

    iget v6, v6, Landroid/graphics/Point;->x:I

    int-to-float v0, v6

    .line 358
    .local v0, "f1":F
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v6, v6, v8

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v1, v6

    .line 360
    .local v1, "f2":F
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->localPath:Landroid/graphics/Path;

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v7, v0, v7

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v8, v1, v8

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 362
    const/4 v4, 0x1

    .line 364
    .local v4, "i":I
    :goto_0
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->arrayOfPoint:[Landroid/graphics/Point;

    array-length v5, v6

    .line 365
    .local v5, "j":I
    if-ge v4, v5, :cond_0

    .line 367
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v6, v6, v4

    iget v6, v6, Landroid/graphics/Point;->x:I

    int-to-float v2, v6

    .line 368
    .local v2, "f3":F
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v6, v6, v4

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v3, v6

    .line 369
    .local v3, "f4":F
    iget-object v6, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->localPath:Landroid/graphics/Path;

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v7, v2, v7

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v8, v3, v8

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 370
    add-int/lit8 v4, v4, 0x1

    .line 371
    goto :goto_0
.end method

.method public readpolyline()V
    .locals 14

    .prologue
    .line 230
    const/4 v5, 0x0

    .line 232
    .local v5, "i":I
    const/4 v11, 0x4

    new-array v6, v11, [B

    .line 234
    .local v6, "intConvert":[B
    const/4 v5, 0x0

    :goto_0
    const/4 v11, 0x3

    if-gt v5, v11, :cond_0

    .line 235
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 234
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 236
    :cond_0
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 237
    .local v1, "Type":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    .line 238
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->setRecordType(I)V

    .line 240
    const/4 v5, 0x0

    :goto_1
    const/4 v11, 0x3

    if-gt v5, v11, :cond_1

    .line 241
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 240
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 242
    :cond_1
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 243
    .local v0, "Size":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    .line 244
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->setRecordSize(I)V

    .line 246
    const/4 v5, 0x0

    :goto_2
    const/4 v11, 0x3

    if-gt v5, v11, :cond_2

    .line 247
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 246
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 248
    :cond_2
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v7

    .line 249
    .local v7, "lef":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    .line 250
    invoke-virtual {p0, v7}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->setLeft(I)V

    .line 252
    const/4 v5, 0x0

    :goto_3
    const/4 v11, 0x3

    if-gt v5, v11, :cond_3

    .line 253
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 252
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 254
    :cond_3
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v10

    .line 255
    .local v10, "top":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    .line 256
    invoke-virtual {p0, v10}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->setTop(I)V

    .line 258
    const/4 v5, 0x0

    :goto_4
    const/4 v11, 0x3

    if-gt v5, v11, :cond_4

    .line 259
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 258
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 260
    :cond_4
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v9

    .line 261
    .local v9, "right":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    .line 262
    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->setRight(I)V

    .line 264
    const/4 v5, 0x0

    :goto_5
    const/4 v11, 0x3

    if-gt v5, v11, :cond_5

    .line 265
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 264
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 266
    :cond_5
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v2

    .line 267
    .local v2, "bot":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    .line 268
    invoke-virtual {p0, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->setBottom(I)V

    .line 271
    const/4 v5, 0x0

    :goto_6
    const/4 v11, 0x3

    if-gt v5, v11, :cond_6

    .line 272
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 271
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    .line 273
    :cond_6
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v4

    .line 274
    .local v4, "count":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    .line 275
    invoke-virtual {p0, v4}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->setCount(I)V

    .line 277
    add-int/lit8 v3, v0, -0x1c

    .line 278
    .local v3, "buffsize":I
    new-array v8, v3, [B

    .line 279
    .local v8, "pointsBuffer":[B
    const/4 v5, 0x0

    :goto_7
    if-ge v5, v3, :cond_7

    .line 280
    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->fileContent:[B

    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    add-int/2addr v12, v5

    aget-byte v11, v11, v12

    aput-byte v11, v8, v5

    .line 279
    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    .line 282
    :cond_7
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->offset:I

    .line 283
    invoke-virtual {p0, v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->setaPoints([B)V

    .line 284
    return-void
.end method

.method public render(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v5, 0x2

    .line 294
    const/4 v2, 0x0

    .line 295
    .local v2, "i":I
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;-><init>()V

    .line 296
    .local v0, "PaintPolylinePropertiesObject":Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 298
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 300
    .local v1, "emrSize":I
    if-lez v1, :cond_1

    .line 301
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 302
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandlePen:I

    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->gethandleNumber()I

    move-result v3

    if-ne v4, v3, :cond_4

    .line 307
    :cond_0
    if-ge v2, v1, :cond_1

    .line 308
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getPenStyle()I

    move-result v3

    if-ne v5, v3, :cond_5

    .line 310
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v3

    new-instance v4, Landroid/graphics/DashPathEffect;

    new-array v5, v5, [F

    fill-array-data v5, :array_0

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 320
    :goto_1
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getColor()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/thumbnail/office/emf/Util/toColorAndroid;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 323
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->getLocalPath()Landroid/graphics/Path;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 328
    :cond_1
    if-eq v2, v1, :cond_2

    if-nez v1, :cond_3

    .line 330
    :cond_2
    const/16 v3, 0x8

    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v3, v4, :cond_6

    .line 338
    :cond_3
    :goto_2
    return-void

    .line 301
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 315
    :cond_5
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getPenStyle()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/thumbnail/office/emf/Util/toStyleAndroid;->getStyle(I)Landroid/graphics/Paint$Style;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    goto :goto_1

    .line 333
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->getLocalPath()Landroid/graphics/Path;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_2

    .line 310
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x40400000    # 3.0f
    .end array-data
.end method

.method public setBottom(I)V
    .locals 0
    .param p1, "b"    # I

    .prologue
    .line 204
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->bottom:I

    .line 205
    return-void
.end method

.method public setCount(I)V
    .locals 0
    .param p1, "cnt"    # I

    .prologue
    .line 128
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->count:I

    .line 129
    return-void
.end method

.method public setLeft(I)V
    .locals 0
    .param p1, "l"    # I

    .prologue
    .line 147
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->left:I

    .line 148
    return-void
.end method

.method public setPoints([B)V
    .locals 12
    .param p1, "points"    # [B

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x0

    const/high16 v9, -0x80000000

    .line 390
    const/4 v2, 0x0

    .line 391
    .local v2, "i":I
    const/4 v4, 0x0

    .line 392
    .local v4, "j":I
    const/4 v5, 0x0

    .line 393
    .local v5, "k":I
    const/4 v6, 0x0

    .line 395
    .local v6, "offset":I
    array-length v7, p1

    shr-int/lit8 v7, v7, 0x3

    new-array v7, v7, [Landroid/graphics/Point;

    iput-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->arrayOfPoint:[Landroid/graphics/Point;

    .line 397
    const/4 v7, 0x4

    new-array v3, v7, [B

    .line 399
    .local v3, "intConvert":[B
    :goto_0
    array-length v7, p1

    if-ge v6, v7, :cond_4

    .line 400
    const/4 v2, 0x0

    :goto_1
    if-gt v2, v11, :cond_0

    .line 401
    rsub-int/lit8 v7, v2, 0x3

    add-int v8, v6, v2

    aget-byte v8, p1, v8

    aput-byte v8, v3, v7

    .line 400
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 402
    :cond_0
    invoke-static {v3, v10}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 403
    .local v0, "X":I
    add-int/2addr v6, v2

    .line 405
    and-int v7, v0, v9

    if-ne v7, v9, :cond_1

    .line 406
    xor-int/lit8 v7, v0, -0x1

    and-int/lit8 v7, v7, -0x1

    add-int/lit8 v7, v7, 0x1

    mul-int/lit8 v0, v7, -0x1

    .line 409
    :cond_1
    const/4 v4, 0x0

    :goto_2
    if-gt v4, v11, :cond_2

    .line 410
    rsub-int/lit8 v7, v4, 0x3

    add-int v8, v6, v4

    aget-byte v8, p1, v8

    aput-byte v8, v3, v7

    .line 409
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 411
    :cond_2
    invoke-static {v3, v10}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 413
    .local v1, "Y":I
    and-int v7, v1, v9

    if-ne v7, v9, :cond_3

    .line 414
    xor-int/lit8 v7, v1, -0x1

    and-int/lit8 v7, v7, -0x1

    add-int/lit8 v7, v7, 0x1

    mul-int/lit8 v1, v7, -0x1

    .line 417
    :cond_3
    add-int/2addr v6, v4

    .line 418
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->arrayOfPoint:[Landroid/graphics/Point;

    new-instance v8, Landroid/graphics/Point;

    invoke-direct {v8, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    aput-object v8, v7, v5

    .line 420
    add-int/lit8 v5, v5, 0x1

    .line 421
    goto :goto_0

    .line 423
    .end local v0    # "X":I
    .end local v1    # "Y":I
    :cond_4
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "RecrdSize"    # I

    .prologue
    .line 109
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->Size:I

    .line 110
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "RecrdType"    # I

    .prologue
    .line 90
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->Type:I

    .line 91
    return-void
.end method

.method public setRight(I)V
    .locals 0
    .param p1, "r"    # I

    .prologue
    .line 185
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->right:I

    .line 186
    return-void
.end method

.method public setTop(I)V
    .locals 0
    .param p1, "t"    # I

    .prologue
    .line 166
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->top:I

    .line 167
    return-void
.end method

.method public setaPoints([B)V
    .locals 0
    .param p1, "pt"    # [B

    .prologue
    .line 214
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_Polyline;->aPoints:[B

    .line 215
    return-void
.end method
