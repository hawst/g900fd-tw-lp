.class Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$SizeConsumer;
.super Ljava/lang/Object;
.source "XfrmHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Size2DHandler$ISizeConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SizeConsumer"
.end annotation


# instance fields
.field shape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 0
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$SizeConsumer;->shape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 104
    return-void
.end method


# virtual methods
.method public setSize(JJ)V
    .locals 1
    .param p1, "w"    # J
    .param p3, "h"    # J

    .prologue
    .line 107
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$SizeConsumer;->shape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    if-nez v0, :cond_0

    .line 111
    :goto_0
    return-void

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$SizeConsumer;->shape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setSize(JJ)V

    goto :goto_0
.end method
