.class public Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorksheets;
.super Ljava/lang/Object;
.source "XSSFExcelWorksheets.java"


# static fields
.field private static final MERGE_CELLS_STREAM_SIZE:I = 0xf

.field private static final TAG:Ljava/lang/String; = "XSSFExcelWorksheets"


# instance fields
.field private fileName:Ljava/io/File;

.field private ss:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;Landroid/content/Context;Ljava/io/File;)V
    .locals 0
    .param p1, "ss"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;
    .param p2, "mContext"    # Landroid/content/Context;
    .param p3, "fileName"    # Ljava/io/File;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorksheets;->ss:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    .line 75
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorksheets;->fileName:Ljava/io/File;

    .line 76
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;Landroid/content/Context;ZLjava/io/File;)V
    .locals 0
    .param p1, "ss"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;
    .param p2, "mContext"    # Landroid/content/Context;
    .param p3, "isCanvasView"    # Z
    .param p4, "fileName"    # Ljava/io/File;

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorksheets;->ss:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    .line 83
    iput-object p4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorksheets;->fileName:Ljava/io/File;

    .line 84
    return-void
.end method

.method private getMergedCellsStream(Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 19
    .param p1, "inputStream"    # Ljava/io/InputStream;

    .prologue
    .line 160
    const/4 v6, 0x0

    .line 161
    .local v6, "is":Ljava/io/ByteArrayInputStream;
    const-wide/16 v14, 0x0

    .line 163
    .local v14, "totalSize":J
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/io/InputStream;->available()I

    move-result v16

    move/from16 v0, v16

    int-to-long v14, v0

    .line 165
    const-wide/16 v16, 0x3c00

    sub-long v12, v14, v16

    .line 167
    .local v12, "sizeToReduce":J
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 168
    .local v2, "baos":Ljava/io/ByteArrayOutputStream;
    const/4 v10, 0x0

    .line 170
    .local v10, "read":I
    const/16 v16, 0x400

    move/from16 v0, v16

    new-array v3, v0, [B

    .line 171
    .local v3, "buffer":[B
    const/4 v11, 0x0

    .line 172
    .local v11, "totalRead":I
    :cond_0
    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/io/InputStream;->read([B)I

    move-result v10

    const/16 v16, -0x1

    move/from16 v0, v16

    if-eq v10, v0, :cond_2

    .line 173
    add-int/2addr v11, v10

    .line 174
    int-to-long v0, v11

    move-wide/from16 v16, v0

    cmp-long v16, v16, v12

    if-lez v16, :cond_0

    .line 175
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v2, v3, v0, v10}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 188
    .end local v2    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v3    # "buffer":[B
    .end local v10    # "read":I
    .end local v11    # "totalRead":I
    .end local v12    # "sizeToReduce":J
    :catch_0
    move-exception v4

    .line 189
    .local v4, "e":Ljava/lang/Exception;
    const-string/jumbo v16, "XSSFExcelWorksheets"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Exception: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    return-object v6

    .line 178
    .restart local v2    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v3    # "buffer":[B
    .restart local v10    # "read":I
    .restart local v11    # "totalRead":I
    .restart local v12    # "sizeToReduce":J
    :cond_2
    :try_start_1
    new-instance v9, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v9, v0}, Ljava/lang/String;-><init>([B)V

    .line 180
    .local v9, "mergeCells":Ljava/lang/String;
    const-string/jumbo v16, "</sheetData>"

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v16

    if-eqz v16, :cond_1

    const-string/jumbo v16, "<mergeCells"

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v16

    if-eqz v16, :cond_1

    .line 182
    const-string/jumbo v16, "<mergeCells"

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 185
    .local v8, "lastPart":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    .line 186
    .local v5, "finalArray":[B
    new-instance v7, Ljava/io/ByteArrayInputStream;

    invoke-direct {v7, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .end local v6    # "is":Ljava/io/ByteArrayInputStream;
    .local v7, "is":Ljava/io/ByteArrayInputStream;
    move-object v6, v7

    .end local v7    # "is":Ljava/io/ByteArrayInputStream;
    .restart local v6    # "is":Ljava/io/ByteArrayInputStream;
    goto :goto_1
.end method

.method private parseExcel(Ljava/io/InputStream;Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;)V
    .locals 5
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "var"    # Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    .prologue
    const/4 v3, 0x0

    .line 90
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsParser;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorksheets;->ss:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-direct {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;)V

    .line 91
    .local v1, "excelParser":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsParser;
    const-string/jumbo v2, ""

    iput-object v2, p2, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->rowindex:Ljava/lang/String;

    .line 92
    iput v3, p2, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->count:I

    .line 93
    iput v3, p2, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countc:I

    .line 94
    iput v3, p2, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countv:I

    .line 95
    iput v3, p2, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->mergecount:I

    .line 97
    :try_start_0
    invoke-virtual {v1, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsParser;->parse(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :goto_0
    return-void

    .line 98
    :catch_0
    move-exception v0

    .line 99
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "XSSFExcelWorksheets"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private parseMergedCellsProperty(Ljava/io/InputStream;Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;)V
    .locals 5
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "var"    # Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    .prologue
    .line 104
    if-nez p1, :cond_0

    .line 115
    :goto_0
    return-void

    .line 108
    :cond_0
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsParser;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorksheets;->ss:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-direct {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;)V

    .line 109
    .local v1, "excelParser":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsParser;
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsParser;->createRootElementHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsParser;->pushHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 111
    :try_start_0
    invoke-virtual {v1, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsParser;->parse(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 112
    :catch_0
    move-exception v0

    .line 113
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "XSSFExcelWorksheets"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected onWorksheetRead(Lorg/apache/poi/openxml4j/opc/PackagePart;Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;)V
    .locals 6
    .param p1, "packagePart"    # Lorg/apache/poi/openxml4j/opc/PackagePart;
    .param p2, "var"    # Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    .prologue
    .line 118
    const/4 v1, 0x0

    .line 120
    .local v1, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p1}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 122
    if-eqz v1, :cond_2

    .line 123
    invoke-direct {p0, v1, p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorksheets;->parseExcel(Ljava/io/InputStream;Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;)V

    .line 124
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 125
    invoke-virtual {p1}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 126
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorksheets;->ss:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->isMergeCellsParsed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 127
    invoke-direct {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorksheets;->getMergedCellsStream(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorksheets;->parseMergedCellsProperty(Ljava/io/InputStream;Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 145
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 151
    :cond_1
    :goto_1
    return-void

    .line 130
    :cond_2
    :try_start_2
    invoke-virtual {p1}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getEntryName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorksheets;->fileName:Ljava/io/File;

    invoke-static {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getInputStream(Ljava/lang/String;Ljava/io/File;)Ljava/io/InputStream;

    move-result-object v1

    .line 131
    if-eqz v1, :cond_0

    .line 132
    invoke-direct {p0, v1, p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorksheets;->parseExcel(Ljava/io/InputStream;Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;)V

    .line 133
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 134
    invoke-virtual {p1}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getEntryName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorksheets;->fileName:Ljava/io/File;

    invoke-static {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getInputStream(Ljava/lang/String;Ljava/io/File;)Ljava/io/InputStream;

    move-result-object v1

    .line 135
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorksheets;->ss:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->isMergeCellsParsed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 136
    invoke-direct {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorksheets;->getMergedCellsStream(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorksheets;->parseMergedCellsProperty(Ljava/io/InputStream;Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 140
    :catch_0
    move-exception v0

    .line 141
    .local v0, "e":Ljava/io/IOException;
    :try_start_3
    const-string/jumbo v2, "XSSFExcelWorksheets"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "IOException: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 143
    if-eqz v1, :cond_1

    .line 145
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 146
    :catch_1
    move-exception v0

    .line 147
    const-string/jumbo v2, "XSSFExcelWorksheets"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 146
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 147
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v2, "XSSFExcelWorksheets"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 143
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    if-eqz v1, :cond_3

    .line 145
    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 148
    :cond_3
    :goto_2
    throw v2

    .line 146
    :catch_3
    move-exception v0

    .line 147
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v3, "XSSFExcelWorksheets"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
