.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyHandler;
.super Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;
.source "XDocBodyHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 15
    const-string/jumbo v0, "body"

    const/16 v1, 0x64

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;-><init>(Ljava/lang/String;I)V

    .line 16
    return-void
.end method


# virtual methods
.method protected init()V
    .locals 3

    .prologue
    .line 19
    invoke-super {p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->init()V

    .line 20
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;-><init>(I)V

    .line 22
    .local v0, "sectPrHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "sectPr"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 28
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 29
    return-void
.end method
