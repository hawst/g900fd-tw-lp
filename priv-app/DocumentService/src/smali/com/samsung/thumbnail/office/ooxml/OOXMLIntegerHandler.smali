.class public Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLValueHandler;
.source "OOXMLIntegerHandler.java"


# instance fields
.field private valConcumer:Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V
    .locals 0
    .param p1, "elementName"    # Ljava/lang/String;
    .param p2, "valConcumer"    # Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLValueHandler;-><init>(Ljava/lang/String;)V

    .line 17
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;->valConcumer:Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;

    .line 18
    return-void
.end method


# virtual methods
.method public handleValue(Ljava/lang/String;)V
    .locals 2
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 23
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;->valConcumer:Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;

    if-nez v1, :cond_0

    .line 28
    :goto_0
    return-void

    .line 26
    :cond_0
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;

    invoke-direct {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;-><init>(Ljava/lang/String;)V

    .line 27
    .local v0, "intVal":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;->valConcumer:Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;

    invoke-interface {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;->consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V

    goto :goto_0
.end method
