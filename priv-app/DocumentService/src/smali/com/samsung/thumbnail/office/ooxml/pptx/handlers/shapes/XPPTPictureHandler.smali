.class public Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTPictureHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XPPTPictureHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;


# instance fields
.field ID:I

.field private picShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

.field private shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "ID"    # I

    .prologue
    .line 30
    const-string/jumbo v0, "pic"

    invoke-direct {p0, p1, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 31
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTPictureHandler;->ID:I

    .line 32
    return-void
.end method


# virtual methods
.method public init(I)V
    .locals 9
    .param p1, "ID"    # I

    .prologue
    const/16 v8, 0x33

    .line 35
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-direct {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;-><init>()V

    iput-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTPictureHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 36
    const/4 v7, 0x3

    new-array v4, v7, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 38
    .local v4, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapeBlipFillHandler;

    invoke-direct {v0, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapeBlipFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;)V

    .line 39
    .local v0, "blipBillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapeBlipFillHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;

    const-string/jumbo v7, "blipFill"

    invoke-direct {v3, v8, v7, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 42
    .local v3, "picSeq":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;
    const/4 v7, 0x0

    aput-object v3, v4, v7

    .line 44
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;

    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTPictureHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-direct {v5, p1, v7}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 46
    .local v5, "shPropHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;

    const-string/jumbo v7, "spPr"

    invoke-direct {v6, v8, v7, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 49
    .local v6, "shPropSeq":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;
    const/4 v7, 0x1

    aput-object v6, v4, v7

    .line 51
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualPicturePropHandler;

    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTPictureHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-direct {v1, v8, v7}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualPicturePropHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 53
    .local v1, "nVisPictPropHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualPicturePropHandler;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;

    const-string/jumbo v7, "nvPicPr"

    invoke-direct {v2, v8, v7, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 56
    .local v2, "nvPicPropSeq":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;
    const/4 v7, 0x2

    aput-object v2, v4, v7

    .line 58
    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTPictureHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 59
    return-void
.end method

.method public setBlipEmbed(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "blipEmbed"    # Ljava/lang/String;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTPictureHandler;->picShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTPictureHandler;->picShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->setBlipEmbed(Ljava/lang/String;)V

    .line 76
    :cond_0
    return-void
.end method

.method public setTile(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "flip"    # Ljava/lang/String;

    .prologue
    .line 80
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 64
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 65
    iget v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTPictureHandler;->ID:I

    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTPictureHandler;->init(I)V

    .line 66
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    .line 67
    .local v0, "slide":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTPictureHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-direct {v1, v2, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;)V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTPictureHandler;->picShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

    .line 68
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTPictureHandler;->picShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->addShapes(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;)V

    .line 69
    return-void
.end method
