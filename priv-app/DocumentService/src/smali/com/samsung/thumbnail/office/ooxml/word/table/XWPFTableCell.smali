.class public Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;
.super Ljava/lang/Object;
.source "XWPFTableCell.java"


# instance fields
.field private cellProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

.field private docParaList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/hslf/DocParagraph;",
            ">;"
        }
    .end annotation
.end field

.field private paraList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;->paraList:Ljava/util/List;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;->docParaList:Ljava/util/ArrayList;

    .line 38
    return-void
.end method


# virtual methods
.method public addPara(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V
    .locals 1
    .param p1, "docPara"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;->docParaList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    return-void
.end method

.method public addPara(Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;)V
    .locals 1
    .param p1, "para"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;->paraList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    return-void
.end method

.method public getCellProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;->cellProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    return-object v0
.end method

.method public getDocParaList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/hslf/DocParagraph;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;->docParaList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getParaList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;->paraList:Ljava/util/List;

    return-object v0
.end method

.method public setCellProp(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;)V
    .locals 0
    .param p1, "cellProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;->cellProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    .line 58
    return-void
.end method
