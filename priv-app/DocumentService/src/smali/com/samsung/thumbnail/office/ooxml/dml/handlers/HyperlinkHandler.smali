.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/HyperlinkHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "HyperlinkHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/HyperlinkHandler$IHlinkCharPropConsumer;
    }
.end annotation


# instance fields
.field private hLinkConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/HyperlinkHandler$IHlinkCharPropConsumer;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/HyperlinkHandler$IHlinkCharPropConsumer;)V
    .locals 2
    .param p1, "hLinkConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/HyperlinkHandler$IHlinkCharPropConsumer;

    .prologue
    .line 20
    const/16 v0, 0x1f

    const-string/jumbo v1, "hlinkClick"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 21
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/HyperlinkHandler;->hLinkConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/HyperlinkHandler$IHlinkCharPropConsumer;

    .line 22
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 27
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 29
    const/16 v2, 0xa

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->getPrefix()Ljava/lang/String;

    move-result-object v1

    .line 31
    .local v1, "prefix":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 33
    .local v0, "attrName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/HyperlinkHandler;->hLinkConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/HyperlinkHandler$IHlinkCharPropConsumer;

    invoke-interface {p3, v0}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/HyperlinkHandler$IHlinkCharPropConsumer;->setLinkId(Ljava/lang/String;)V

    .line 34
    return-void
.end method
