.class public Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
.super Ljava/lang/Object;
.source "ExcelVariables.java"


# instance fields
.field public cellnum:I

.field public columcount:I

.field public count:I

.field public countc:I

.field public countv:I

.field public customCols:Z

.field public fontid:I

.field public hashIndex:I

.field public maxCol:I

.field public mergecount:I

.field public numalignment:I

.field public rowindex:Ljava/lang/String;

.field public sharedstyleIndex:I

.field public span:Ljava/lang/String;

.field public tableWidth:F

.field public tblbordercount:I

.field public tempString:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->hashIndex:I

    .line 22
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tempString:Ljava/lang/String;

    .line 33
    iput v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    .line 34
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tableWidth:F

    .line 37
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->maxCol:I

    .line 38
    iput-boolean v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->customCols:Z

    return-void
.end method
