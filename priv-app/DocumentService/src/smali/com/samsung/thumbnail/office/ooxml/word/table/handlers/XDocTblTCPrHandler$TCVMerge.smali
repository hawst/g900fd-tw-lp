.class public Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCVMerge;
.super Ljava/lang/Object;
.source "XDocTblTCPrHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TCVMerge"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCVMerge;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V
    .locals 3
    .param p1, "value"    # Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;

    .prologue
    .line 138
    instance-of v2, p1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 139
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    .line 140
    .local v0, "strVal":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    const/4 v1, 0x0

    .line 141
    .local v1, "vMer":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 142
    const-string/jumbo v2, "continue"

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    move-result-object v1

    .line 146
    :goto_0
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCVMerge;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;

    # getter for: Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;->tblCellProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;
    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;->access$000(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;)Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->setVMerge(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;)V

    .line 148
    .end local v0    # "strVal":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    .end local v1    # "vMer":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;
    :cond_0
    return-void

    .line 144
    .restart local v0    # "strVal":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    .restart local v1    # "vMer":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    move-result-object v1

    goto :goto_0
.end method
