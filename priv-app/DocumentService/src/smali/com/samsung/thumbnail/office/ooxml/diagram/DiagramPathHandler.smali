.class public Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPathHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DiagramPathHandler.java"


# instance fields
.field private pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V
    .locals 9
    .param p1, "pathDesc"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    .prologue
    .line 25
    const/16 v7, 0x1f

    const-string/jumbo v8, "path"

    invoke-direct {p0, v7, v8}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 26
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPathHandler;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    .line 28
    const/4 v7, 0x3

    new-array v6, v7, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 30
    .local v6, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathArcToHandler;

    invoke-direct {v1, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathArcToHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V

    .line 31
    .local v1, "arcToHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathArcToHandler;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v7, "arcTo"

    invoke-direct {v0, v7, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 33
    .local v0, "arcSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v7, 0x0

    aput-object v0, v6, v7

    .line 35
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathLineToHandler;

    invoke-direct {v3, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathLineToHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V

    .line 36
    .local v3, "lineToHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathLineToHandler;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v7, "lnTo"

    invoke-direct {v2, v7, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 38
    .local v2, "lineSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v7, 0x1

    aput-object v2, v6, v7

    .line 40
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathMoveToHandler;

    invoke-direct {v5, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathMoveToHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V

    .line 41
    .local v5, "moveToHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathMoveToHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v7, "moveTo"

    invoke-direct {v4, v7, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 43
    .local v4, "moveSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v7, 0x2

    aput-object v4, v6, v7

    .line 50
    iput-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPathHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 51
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 7
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 56
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 58
    const/4 v0, 0x0

    .line 59
    .local v0, "fill":Z
    const/4 v3, 0x0

    .line 60
    .local v3, "stroke":Z
    const/16 v4, 0x5460

    .line 61
    .local v4, "w":I
    const/16 v1, 0x5460

    .line 62
    .local v1, "h":I
    const-string/jumbo v6, "w"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPathHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v5

    .line 63
    .local v5, "width":Ljava/lang/String;
    const-string/jumbo v6, "h"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPathHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    .line 64
    .local v2, "height":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 65
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 66
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPathHandler;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    invoke-virtual {v6, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->setWidth(I)V

    .line 68
    :cond_0
    if-eqz v2, :cond_1

    .line 69
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 70
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPathHandler;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    invoke-virtual {v6, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->setHeight(I)V

    .line 72
    :cond_1
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPathHandler;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    invoke-virtual {v6, v4, v1, v0, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->createNewPath(IIZZ)V

    .line 74
    return-void
.end method
