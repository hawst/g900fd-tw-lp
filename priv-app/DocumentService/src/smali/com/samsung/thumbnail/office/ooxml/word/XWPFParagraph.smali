.class public Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;
.super Ljava/lang/Object;
.source "XWPFParagraph.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/IBodyElement;


# instance fields
.field private fldSmplInstr:Ljava/lang/String;

.field private hasText:Z

.field private isFooter:Z

.field private isHeader:Z

.field private paraCharProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

.field private paraProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

.field private runLst:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->runLst:Ljava/util/ArrayList;

    .line 39
    return-void
.end method


# virtual methods
.method public addRun(Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;)V
    .locals 1
    .param p1, "run"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->runLst:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 59
    return-void
.end method

.method public clearRunList()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->runLst:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 67
    return-void
.end method

.method public getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->paraCharProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    return-object v0
.end method

.method public getElementType()Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->PARAGRAPH:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    return-object v0
.end method

.method public getFldSmplInstr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->fldSmplInstr:Ljava/lang/String;

    return-object v0
.end method

.method public getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->paraProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    return-object v0
.end method

.method public getRunList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->runLst:Ljava/util/ArrayList;

    return-object v0
.end method

.method public hasText()Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->hasText:Z

    return v0
.end method

.method public isFooter()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->isFooter:Z

    return v0
.end method

.method public isHeader()Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->isHeader:Z

    return v0
.end method

.method public setCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V
    .locals 0
    .param p1, "charProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->paraCharProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .line 43
    return-void
.end method

.method public setFldSmplInstr(Ljava/lang/String;)V
    .locals 0
    .param p1, "fldSmplInstr"    # Ljava/lang/String;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->fldSmplInstr:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public setHasText(Z)V
    .locals 0
    .param p1, "hasText"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->hasText:Z

    .line 71
    return-void
.end method

.method public setIsFooter(Z)V
    .locals 0
    .param p1, "isFooter"    # Z

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->isFooter:Z

    .line 31
    return-void
.end method

.method public setIsHeader(Z)V
    .locals 0
    .param p1, "isHeader"    # Z

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->isHeader:Z

    .line 21
    return-void
.end method

.method public setParaProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V
    .locals 0
    .param p1, "paraProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->paraProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    .line 51
    return-void
.end method
