.class public Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;
.super Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;
.source "WedgeCallouts.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mDrawOnCanvas:Z

.field private mLeftMarg:I

.field private mTopMarg:I

.field public newBitmapHeight:I

.field public newBitmapWidth:I

.field private rectF:Landroid/graphics/RectF;

.field private startA:F

.field public subFactor_Height:I

.field public subFactor_Width:I

.field private sweepA:F


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 3
    .param p1, "folderPath"    # Ljava/io/File;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;-><init>()V

    .line 30
    const-string/jumbo v0, "Callouts"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->TAG:Ljava/lang/String;

    .line 31
    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    .line 32
    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    .line 33
    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->newBitmapHeight:I

    .line 34
    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->newBitmapWidth:I

    .line 35
    iput-boolean v1, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mDrawOnCanvas:Z

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    .line 40
    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    .line 41
    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    .line 44
    iput-object p1, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->folderPath:Ljava/io/File;

    .line 45
    return-void
.end method

.method private initArc(FFFFFFFF)V
    .locals 25
    .param p1, "left"    # F
    .param p2, "top"    # F
    .param p3, "right"    # F
    .param p4, "bottom"    # F
    .param p5, "xStart"    # F
    .param p6, "yStart"    # F
    .param p7, "xEnd"    # F
    .param p8, "yEnd"    # F

    .prologue
    .line 685
    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->width:F

    const v20, 0x46a8c000    # 21600.0f

    div-float v6, v7, v20

    .line 686
    .local v6, "mulFactor_Width":F
    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->height:F

    const v20, 0x46a8c000    # 21600.0f

    div-float v5, v7, v20

    .line 688
    .local v5, "mulFactor_Height":F
    new-instance v7, Landroid/graphics/RectF;

    mul-float v20, p1, v6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    sub-float v20, v20, v21

    mul-float v21, p2, v5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v22, p3, v6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    sub-float v22, v22, v23

    mul-float v23, p4, v5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    sub-float v23, v23, v24

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-direct {v7, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    .line 691
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->centerX()F

    move-result v7

    float-to-double v12, v7

    .line 692
    .local v12, "xc":D
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->centerY()F

    move-result v7

    float-to-double v0, v7

    move-wide/from16 v18, v0

    .line 693
    .local v18, "yc":D
    mul-float v7, p5, v6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    sub-float v7, v7, v20

    float-to-double v8, v7

    .line 694
    .local v8, "x1":D
    mul-float v7, p6, v5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    sub-float v7, v7, v20

    float-to-double v14, v7

    .line 695
    .local v14, "y1":D
    sub-double v20, v14, v18

    sub-double v22, v8, v12

    invoke-static/range {v20 .. v23}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v7, v0

    move-object/from16 v0, p0

    iput v7, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    .line 696
    mul-float v7, p7, v6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    sub-float v7, v7, v20

    float-to-double v10, v7

    .line 697
    .local v10, "x2":D
    mul-float v7, p8, v5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    sub-float v7, v7, v20

    float-to-double v0, v7

    move-wide/from16 v16, v0

    .line 698
    .local v16, "y2":D
    sub-double v20, v16, v18

    sub-double v22, v10, v12

    invoke-static/range {v20 .. v23}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v4, v0

    .line 699
    .local v4, "endA":F
    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    sub-float v7, v4, v7

    move-object/from16 v0, p0

    iput v7, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    .line 700
    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    const/16 v20, 0x0

    cmpl-float v7, v7, v20

    if-lez v7, :cond_0

    .line 701
    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    const/high16 v20, 0x43b40000    # 360.0f

    sub-float v7, v7, v20

    move-object/from16 v0, p0

    iput v7, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    .line 703
    :cond_0
    return-void
.end method


# virtual methods
.method protected draw(I)V
    .locals 64
    .param p1, "shapeCount"    # I

    .prologue
    .line 166
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->shapeName:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 681
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->bitmapHight:I

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->newBitmapHeight:I

    .line 170
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->bitmapWidth:I

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->newBitmapWidth:I

    .line 172
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->width:F

    const v4, 0x46a8c000    # 21600.0f

    div-float v57, v3, v4

    .line 173
    .local v57, "mulFactor_Width":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->height:F

    const v4, 0x46a8c000    # 21600.0f

    div-float v56, v3, v4

    .line 183
    .local v56, "mulFactor_Height":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    if-nez v3, :cond_2

    .line 184
    const/16 v3, 0x546

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    .line 185
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    if-nez v3, :cond_3

    .line 186
    const/16 v3, 0x6540

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    .line 188
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    const/4 v4, 0x0

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v54

    .line 189
    .local v54, "minX":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    const/16 v4, 0x5460

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v52

    .line 190
    .local v52, "maxX":I
    move/from16 v0, v54

    int-to-float v3, v0

    mul-float v3, v3, v57

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    .line 191
    move/from16 v0, v52

    int-to-float v3, v0

    mul-float v3, v3, v57

    move/from16 v0, v54

    int-to-float v4, v0

    mul-float v4, v4, v57

    sub-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->newBitmapWidth:I

    .line 193
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    const/4 v4, 0x0

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v55

    .line 194
    .local v55, "minY":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    const/16 v4, 0x5460

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v53

    .line 195
    .local v53, "maxY":I
    move/from16 v0, v55

    int-to-float v3, v0

    mul-float v3, v3, v56

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    .line 196
    move/from16 v0, v53

    int-to-float v3, v0

    mul-float v3, v3, v56

    move/from16 v0, v55

    int-to-float v4, v0

    mul-float v4, v4, v56

    sub-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->newBitmapHeight:I

    .line 198
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->newBitmapWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->newBitmapHeight:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 203
    .local v2, "_bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "WedgeRectCallout"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 204
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    rsub-int v3, v3, 0x2a30

    int-to-float v12, v3

    .line 205
    .local v12, "at0":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    rsub-int v3, v3, 0x2a30

    int-to-float v13, v3

    .line 206
    .local v13, "at1":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    sub-int/2addr v3, v4

    int-to-float v0, v3

    move/from16 v24, v0

    .line 207
    .local v24, "at2":F
    add-float v35, v12, v13

    .line 208
    .local v35, "at3":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    rsub-int v3, v3, 0x5460

    int-to-float v0, v3

    move/from16 v44, v0

    .line 209
    .local v44, "at4":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    rsub-int v3, v3, 0x5460

    int-to-float v0, v3

    move/from16 v45, v0

    .line 210
    .local v45, "at5":F
    const/4 v3, 0x0

    cmpl-float v3, v12, v3

    if-lez v3, :cond_16

    const/high16 v46, 0x45610000    # 3600.0f

    .line 211
    .local v46, "at6":F
    :goto_1
    const/4 v3, 0x0

    cmpl-float v3, v12, v3

    if-lez v3, :cond_17

    const v47, 0x460ca000    # 9000.0f

    .line 212
    .local v47, "at7":F
    :goto_2
    const/4 v3, 0x0

    cmpl-float v3, v13, v3

    if-lez v3, :cond_18

    const/high16 v48, 0x45610000    # 3600.0f

    .line 213
    .local v48, "at8":F
    :goto_3
    const/4 v3, 0x0

    cmpl-float v3, v13, v3

    if-lez v3, :cond_19

    const v49, 0x460ca000    # 9000.0f

    .line 214
    .local v49, "at9":F
    :goto_4
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_1a

    const/4 v14, 0x0

    .line 215
    .local v14, "at10":F
    :goto_5
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_1b

    move v15, v14

    .line 216
    .local v15, "at11":F
    :goto_6
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    if-lez v3, :cond_1c

    const/16 v16, 0x0

    .line 217
    .local v16, "at12":F
    :goto_7
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_1d

    move/from16 v17, v46

    .line 218
    .local v17, "at13":F
    :goto_8
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_1e

    move/from16 v18, v46

    .line 219
    .local v18, "at14":F
    :goto_9
    const/4 v3, 0x0

    cmpl-float v3, v45, v3

    if-lez v3, :cond_1f

    move/from16 v19, v46

    .line 220
    .local v19, "at15":F
    :goto_a
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_20

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v0, v3

    move/from16 v20, v0

    .line 221
    .local v20, "at16":F
    :goto_b
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_21

    const v21, 0x46a8c000    # 21600.0f

    .line 222
    .local v21, "at17":F
    :goto_c
    const/4 v3, 0x0

    cmpl-float v3, v44, v3

    if-lez v3, :cond_22

    const v22, 0x46a8c000    # 21600.0f

    .line 223
    .local v22, "at18":F
    :goto_d
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_23

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v0, v3

    move/from16 v23, v0

    .line 224
    .local v23, "at19":F
    :goto_e
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_24

    move/from16 v25, v23

    .line 225
    .local v25, "at20":F
    :goto_f
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    if-lez v3, :cond_25

    move/from16 v26, v46

    .line 226
    .local v26, "at21":F
    :goto_10
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_26

    move/from16 v27, v48

    .line 227
    .local v27, "at22":F
    :goto_11
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_27

    move/from16 v28, v27

    .line 228
    .local v28, "at23":F
    :goto_12
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    if-lez v3, :cond_28

    move/from16 v29, v48

    .line 229
    .local v29, "at24":F
    :goto_13
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_29

    const v30, 0x46a8c000    # 21600.0f

    .line 230
    .local v30, "at25":F
    :goto_14
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_2a

    const v31, 0x46a8c000    # 21600.0f

    .line 231
    .local v31, "at26":F
    :goto_15
    const/4 v3, 0x0

    cmpl-float v3, v45, v3

    if-lez v3, :cond_2b

    const v32, 0x46a8c000    # 21600.0f

    .line 232
    .local v32, "at27":F
    :goto_16
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_2c

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v0, v3

    move/from16 v33, v0

    .line 233
    .local v33, "at28":F
    :goto_17
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_2d

    move/from16 v34, v48

    .line 234
    .local v34, "at29":F
    :goto_18
    const/4 v3, 0x0

    cmpl-float v3, v44, v3

    if-lez v3, :cond_2e

    move/from16 v36, v48

    .line 235
    .local v36, "at30":F
    :goto_19
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_2f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v0, v3

    move/from16 v37, v0

    .line 236
    .local v37, "at31":F
    :goto_1a
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_30

    move/from16 v38, v37

    .line 237
    .local v38, "at32":F
    :goto_1b
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    if-lez v3, :cond_31

    const/16 v39, 0x0

    .line 239
    .local v39, "at33":F
    :goto_1c
    new-instance v59, Landroid/graphics/Path;

    invoke-direct/range {v59 .. v59}, Landroid/graphics/Path;-><init>()V

    .line 244
    .local v59, "path":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    rsub-int/lit8 v3, v3, 0x0

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    rsub-int/lit8 v4, v4, 0x0

    int-to-float v4, v4

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 245
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    rsub-int/lit8 v3, v3, 0x0

    int-to-float v3, v3

    mul-float v4, v48, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 247
    mul-float v3, v16, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v29, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 249
    const/4 v3, 0x0

    mul-float v3, v3, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v49, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 251
    const/4 v3, 0x0

    mul-float v3, v3, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 253
    mul-float v3, v46, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 255
    mul-float v3, v19, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v32, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 257
    mul-float v3, v47, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 259
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 261
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v49, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 263
    mul-float v3, v22, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v36, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 265
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v48, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 267
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 269
    mul-float v3, v47, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 271
    mul-float v3, v26, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v39, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 273
    mul-float v3, v46, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 275
    invoke-virtual/range {v59 .. v59}, Landroid/graphics/Path;->close()V

    .line 277
    new-instance v50, Landroid/graphics/Canvas;

    move-object/from16 v0, v50

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 279
    .local v50, "canvas":Landroid/graphics/Canvas;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_4

    .line 280
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, v50

    move-object/from16 v1, v59

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 281
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_5

    .line 282
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v50

    move-object/from16 v1, v59

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 286
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 287
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 288
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 289
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    .line 293
    .end local v12    # "at0":F
    .end local v13    # "at1":F
    .end local v14    # "at10":F
    .end local v15    # "at11":F
    .end local v16    # "at12":F
    .end local v17    # "at13":F
    .end local v18    # "at14":F
    .end local v19    # "at15":F
    .end local v20    # "at16":F
    .end local v21    # "at17":F
    .end local v22    # "at18":F
    .end local v23    # "at19":F
    .end local v24    # "at2":F
    .end local v25    # "at20":F
    .end local v26    # "at21":F
    .end local v27    # "at22":F
    .end local v28    # "at23":F
    .end local v29    # "at24":F
    .end local v30    # "at25":F
    .end local v31    # "at26":F
    .end local v32    # "at27":F
    .end local v33    # "at28":F
    .end local v34    # "at29":F
    .end local v35    # "at3":F
    .end local v36    # "at30":F
    .end local v37    # "at31":F
    .end local v38    # "at32":F
    .end local v39    # "at33":F
    .end local v44    # "at4":F
    .end local v45    # "at5":F
    .end local v46    # "at6":F
    .end local v47    # "at7":F
    .end local v48    # "at8":F
    .end local v49    # "at9":F
    .end local v50    # "canvas":Landroid/graphics/Canvas;
    .end local v59    # "path":Landroid/graphics/Path;
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "WedgeRRectCallout"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "wedgeRoundRectCallout"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 295
    :cond_7
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    rsub-int v3, v3, 0x2a30

    int-to-float v12, v3

    .line 296
    .restart local v12    # "at0":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    rsub-int v3, v3, 0x2a30

    int-to-float v13, v3

    .line 297
    .restart local v13    # "at1":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    sub-int/2addr v3, v4

    int-to-float v0, v3

    move/from16 v24, v0

    .line 298
    .restart local v24    # "at2":F
    add-float v35, v12, v13

    .line 299
    .restart local v35    # "at3":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    rsub-int v3, v3, 0x5460

    int-to-float v0, v3

    move/from16 v44, v0

    .line 300
    .restart local v44    # "at4":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    rsub-int v3, v3, 0x5460

    int-to-float v0, v3

    move/from16 v45, v0

    .line 301
    .restart local v45    # "at5":F
    const/4 v3, 0x0

    cmpl-float v3, v12, v3

    if-lez v3, :cond_32

    const/high16 v46, 0x45610000    # 3600.0f

    .line 302
    .restart local v46    # "at6":F
    :goto_1d
    const/4 v3, 0x0

    cmpl-float v3, v12, v3

    if-lez v3, :cond_33

    const v47, 0x460ca000    # 9000.0f

    .line 303
    .restart local v47    # "at7":F
    :goto_1e
    const/4 v3, 0x0

    cmpl-float v3, v13, v3

    if-lez v3, :cond_34

    const/high16 v48, 0x45610000    # 3600.0f

    .line 304
    .restart local v48    # "at8":F
    :goto_1f
    const/4 v3, 0x0

    cmpl-float v3, v13, v3

    if-lez v3, :cond_35

    const v49, 0x460ca000    # 9000.0f

    .line 305
    .restart local v49    # "at9":F
    :goto_20
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_36

    const/4 v14, 0x0

    .line 306
    .restart local v14    # "at10":F
    :goto_21
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_37

    move v15, v14

    .line 307
    .restart local v15    # "at11":F
    :goto_22
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    if-lez v3, :cond_38

    const/16 v16, 0x0

    .line 308
    .restart local v16    # "at12":F
    :goto_23
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_39

    move/from16 v17, v46

    .line 309
    .restart local v17    # "at13":F
    :goto_24
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_3a

    move/from16 v18, v46

    .line 310
    .restart local v18    # "at14":F
    :goto_25
    const/4 v3, 0x0

    cmpl-float v3, v45, v3

    if-lez v3, :cond_3b

    move/from16 v19, v46

    .line 311
    .restart local v19    # "at15":F
    :goto_26
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_3c

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v0, v3

    move/from16 v20, v0

    .line 312
    .restart local v20    # "at16":F
    :goto_27
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_3d

    const v21, 0x46a8c000    # 21600.0f

    .line 313
    .restart local v21    # "at17":F
    :goto_28
    const/4 v3, 0x0

    cmpl-float v3, v44, v3

    if-lez v3, :cond_3e

    const v22, 0x46a8c000    # 21600.0f

    .line 314
    .restart local v22    # "at18":F
    :goto_29
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_3f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v0, v3

    move/from16 v23, v0

    .line 315
    .restart local v23    # "at19":F
    :goto_2a
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_40

    move/from16 v25, v23

    .line 316
    .restart local v25    # "at20":F
    :goto_2b
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    if-lez v3, :cond_41

    move/from16 v26, v46

    .line 317
    .restart local v26    # "at21":F
    :goto_2c
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_42

    move/from16 v27, v48

    .line 318
    .restart local v27    # "at22":F
    :goto_2d
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_43

    move/from16 v28, v27

    .line 319
    .restart local v28    # "at23":F
    :goto_2e
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    if-lez v3, :cond_44

    move/from16 v29, v48

    .line 320
    .restart local v29    # "at24":F
    :goto_2f
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_45

    const v30, 0x46a8c000    # 21600.0f

    .line 321
    .restart local v30    # "at25":F
    :goto_30
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_46

    const v31, 0x46a8c000    # 21600.0f

    .line 322
    .restart local v31    # "at26":F
    :goto_31
    const/4 v3, 0x0

    cmpl-float v3, v45, v3

    if-lez v3, :cond_47

    const v32, 0x46a8c000    # 21600.0f

    .line 323
    .restart local v32    # "at27":F
    :goto_32
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_48

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v0, v3

    move/from16 v33, v0

    .line 324
    .restart local v33    # "at28":F
    :goto_33
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_49

    move/from16 v34, v48

    .line 325
    .restart local v34    # "at29":F
    :goto_34
    const/4 v3, 0x0

    cmpl-float v3, v44, v3

    if-lez v3, :cond_4a

    move/from16 v36, v48

    .line 326
    .restart local v36    # "at30":F
    :goto_35
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_4b

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v0, v3

    move/from16 v37, v0

    .line 327
    .restart local v37    # "at31":F
    :goto_36
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_4c

    move/from16 v38, v37

    .line 328
    .restart local v38    # "at32":F
    :goto_37
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    if-lez v3, :cond_4d

    const/16 v39, 0x0

    .line 330
    .restart local v39    # "at33":F
    :goto_38
    new-instance v59, Landroid/graphics/Path;

    invoke-direct/range {v59 .. v59}, Landroid/graphics/Path;-><init>()V

    .line 331
    .restart local v59    # "path":Landroid/graphics/Path;
    const/high16 v3, 0x45610000    # 3600.0f

    mul-float v3, v3, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 333
    const/4 v3, 0x0

    mul-float v3, v3, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    mul-float v5, v5, v57

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    const/high16 v6, 0x45610000    # 3600.0f

    mul-float v6, v6, v56

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 337
    const/4 v3, 0x0

    mul-float v3, v3, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v48, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 339
    mul-float v3, v16, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v29, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 341
    const/4 v3, 0x0

    mul-float v3, v3, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v49, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 343
    const/4 v3, 0x0

    mul-float v3, v3, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x468ca000    # 18000.0f

    mul-float v4, v4, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 345
    const/4 v3, 0x0

    mul-float v3, v3, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    const/high16 v5, 0x45610000    # 3600.0f

    mul-float v5, v5, v57

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    mul-float v6, v6, v56

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 349
    mul-float v3, v46, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 351
    mul-float v3, v19, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v32, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 353
    mul-float v3, v47, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 355
    const v3, 0x468ca000    # 18000.0f

    mul-float v3, v3, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 357
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    mul-float v5, v5, v57

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    const v6, 0x468ca000    # 18000.0f

    mul-float v6, v6, v56

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 361
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v49, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 363
    mul-float v3, v22, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v36, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 365
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v48, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 367
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/high16 v4, 0x45610000    # 3600.0f

    mul-float v4, v4, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 369
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    const v5, 0x468ca000    # 18000.0f

    mul-float v5, v5, v57

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    const/4 v6, 0x0

    mul-float v6, v6, v56

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 373
    mul-float v3, v47, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 375
    mul-float v3, v26, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v39, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 377
    mul-float v3, v46, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 379
    invoke-virtual/range {v59 .. v59}, Landroid/graphics/Path;->close()V

    .line 381
    new-instance v50, Landroid/graphics/Canvas;

    move-object/from16 v0, v50

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 383
    .restart local v50    # "canvas":Landroid/graphics/Canvas;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_8

    .line 384
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, v50

    move-object/from16 v1, v59

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 385
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_9

    .line 386
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v50

    move-object/from16 v1, v59

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 390
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->textArea:Landroid/graphics/RectF;

    const v4, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 391
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->textArea:Landroid/graphics/RectF;

    const v4, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 392
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->textArea:Landroid/graphics/RectF;

    const v4, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 393
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->textArea:Landroid/graphics/RectF;

    const v4, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    .line 455
    .end local v12    # "at0":F
    .end local v13    # "at1":F
    .end local v14    # "at10":F
    .end local v15    # "at11":F
    .end local v16    # "at12":F
    .end local v17    # "at13":F
    .end local v18    # "at14":F
    .end local v19    # "at15":F
    .end local v20    # "at16":F
    .end local v21    # "at17":F
    .end local v22    # "at18":F
    .end local v23    # "at19":F
    .end local v24    # "at2":F
    .end local v25    # "at20":F
    .end local v26    # "at21":F
    .end local v27    # "at22":F
    .end local v28    # "at23":F
    .end local v29    # "at24":F
    .end local v30    # "at25":F
    .end local v31    # "at26":F
    .end local v32    # "at27":F
    .end local v33    # "at28":F
    .end local v34    # "at29":F
    .end local v35    # "at3":F
    .end local v36    # "at30":F
    .end local v37    # "at31":F
    .end local v38    # "at32":F
    .end local v39    # "at33":F
    .end local v44    # "at4":F
    .end local v45    # "at5":F
    .end local v46    # "at6":F
    .end local v47    # "at7":F
    .end local v48    # "at8":F
    .end local v49    # "at9":F
    .end local v50    # "canvas":Landroid/graphics/Canvas;
    .end local v59    # "path":Landroid/graphics/Path;
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "CloudCallout"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 456
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->newBitmapWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->newBitmapHeight:I

    add-int/lit8 v4, v4, 0x4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 458
    .end local v2    # "_bitmap":Landroid/graphics/Bitmap;
    .local v11, "_bitmap":Landroid/graphics/Bitmap;
    new-instance v50, Landroid/graphics/Canvas;

    move-object/from16 v0, v50

    invoke-direct {v0, v11}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 460
    .restart local v50    # "canvas":Landroid/graphics/Canvas;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    add-int/lit16 v3, v3, -0x2a30

    int-to-float v12, v3

    .line 461
    .restart local v12    # "at0":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    add-int/lit16 v3, v3, -0x2a30

    int-to-float v13, v3

    .line 462
    .restart local v13    # "at1":F
    const-wide v4, 0x40c5180000000000L    # 10800.0

    float-to-double v8, v13

    float-to-double v0, v12

    move-wide/from16 v62, v0

    move-wide/from16 v0, v62

    invoke-static {v8, v9, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    mul-double/2addr v4, v8

    double-to-float v0, v4

    move/from16 v24, v0

    .line 466
    .restart local v24    # "at2":F
    const-wide v4, 0x40c5180000000000L    # 10800.0

    float-to-double v8, v13

    float-to-double v0, v12

    move-wide/from16 v62, v0

    move-wide/from16 v0, v62

    invoke-static {v8, v9, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    mul-double/2addr v4, v8

    double-to-float v0, v4

    move/from16 v35, v0

    .line 470
    .restart local v35    # "at3":F
    const v3, 0x4628c000    # 10800.0f

    add-float v44, v24, v3

    .line 471
    .restart local v44    # "at4":F
    const v3, 0x4628c000    # 10800.0f

    add-float v45, v35, v3

    .line 472
    .restart local v45    # "at5":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v3, v3

    sub-float v46, v44, v3

    .line 473
    .restart local v46    # "at6":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v3, v3

    sub-float v47, v45, v3

    .line 474
    .restart local v47    # "at7":F
    mul-float v3, v46, v46

    mul-float v4, v47, v47

    add-float/2addr v3, v4

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v48, v0

    .line 476
    .restart local v48    # "at8":F
    const v49, 0x45ce4000    # 6600.0f

    .line 477
    .restart local v49    # "at9":F
    sub-float v14, v48, v49

    .line 478
    .restart local v14    # "at10":F
    const/high16 v3, 0x40400000    # 3.0f

    div-float v15, v14, v3

    .line 479
    .restart local v15    # "at11":F
    const/high16 v16, 0x44e10000    # 1800.0f

    .line 480
    .restart local v16    # "at12":F
    add-float v17, v15, v16

    .line 481
    .restart local v17    # "at13":F
    mul-float v3, v17, v46

    div-float v18, v3, v48

    .line 482
    .restart local v18    # "at14":F
    mul-float v3, v17, v47

    div-float v19, v3, v48

    .line 483
    .restart local v19    # "at15":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v3, v3

    add-float v20, v18, v3

    .line 484
    .restart local v20    # "at16":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v3, v3

    add-float v21, v19, v3

    .line 485
    .restart local v21    # "at17":F
    const/high16 v22, 0x45960000    # 4800.0f

    .line 486
    .restart local v22    # "at18":F
    const/high16 v3, 0x40000000    # 2.0f

    mul-float v23, v15, v3

    .line 487
    .restart local v23    # "at19":F
    add-float v25, v22, v23

    .line 488
    .restart local v25    # "at20":F
    mul-float v3, v25, v46

    div-float v26, v3, v48

    .line 489
    .restart local v26    # "at21":F
    mul-float v3, v25, v47

    div-float v27, v3, v48

    .line 490
    .restart local v27    # "at22":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v3, v3

    add-float v28, v26, v3

    .line 491
    .restart local v28    # "at23":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v3, v3

    add-float v29, v27, v3

    .line 492
    .restart local v29    # "at24":F
    const/high16 v30, 0x44960000    # 1200.0f

    .line 493
    .restart local v30    # "at25":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    add-int/lit16 v3, v3, 0x258

    int-to-float v0, v3

    move/from16 v31, v0

    .line 494
    .restart local v31    # "at26":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    add-int/lit16 v3, v3, -0x258

    int-to-float v0, v3

    move/from16 v32, v0

    .line 495
    .restart local v32    # "at27":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    add-int/lit16 v3, v3, 0x258

    int-to-float v0, v3

    move/from16 v33, v0

    .line 496
    .restart local v33    # "at28":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    add-int/lit16 v3, v3, -0x258

    int-to-float v0, v3

    move/from16 v34, v0

    .line 497
    .restart local v34    # "at29":F
    add-float v36, v20, v30

    .line 498
    .restart local v36    # "at30":F
    sub-float v37, v20, v30

    .line 499
    .restart local v37    # "at31":F
    add-float v38, v21, v30

    .line 500
    .restart local v38    # "at32":F
    sub-float v39, v21, v30

    .line 501
    .restart local v39    # "at33":F
    add-float v40, v28, v16

    .line 502
    .local v40, "at34":F
    sub-float v41, v28, v16

    .line 503
    .local v41, "at35":F
    add-float v42, v29, v16

    .line 504
    .local v42, "at36":F
    sub-float v43, v29, v16

    .line 507
    .local v43, "at37":F
    new-instance v59, Landroid/graphics/Path;

    invoke-direct/range {v59 .. v59}, Landroid/graphics/Path;-><init>()V

    .line 508
    .restart local v59    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    const v4, 0x45dfe800    # 7165.0f

    const v5, 0x4587c800    # 4345.0f

    const v6, 0x464cd800    # 13110.0f

    const v7, 0x44f3c000    # 1950.0f

    const v8, 0x45e08800    # 7185.0f

    const/high16 v9, 0x44870000    # 1080.0f

    const v10, 0x46464800    # 12690.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 509
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 510
    const v3, 0x43ed8000    # 475.0f

    const v4, 0x46375000    # 11732.0f

    const v5, 0x45971800    # 4835.0f

    const v6, 0x4689e400    # 17650.0f

    const/high16 v7, 0x44870000    # 1080.0f

    const v8, 0x46464800    # 12690.0f

    const v9, 0x4535e000    # 2910.0f

    const v10, 0x4689d000    # 17640.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 511
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 512
    const v3, 0x45153000    # 2387.0f

    const v4, 0x46187400    # 9757.0f

    const v5, 0x461dec00    # 10107.0f

    const v6, 0x469e9800    # 20300.0f

    const v7, 0x4535e000    # 2910.0f

    const v8, 0x4689d000    # 17640.0f

    const v9, 0x4600ac00    # 8235.0f

    const v10, 0x4698b200    # 19545.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 513
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 514
    const v3, 0x443f8000    # 766.0f

    const v4, 0x46417800    # 12382.0f

    const v5, 0x46613000    # 14412.0f

    const v6, 0x46a8ba00    # 21597.0f

    const v7, 0x4600ac00    # 8235.0f

    const v8, 0x4698b200    # 19545.0f

    const v9, 0x465f2000    # 14280.0f

    const v10, 0x468f3400    # 18330.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 515
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 516
    const v3, 0x4649b800    # 12910.0f

    const v4, 0x462d2000    # 11080.0f

    const v5, 0x46920e00    # 18695.0f

    const v6, 0x46940600    # 18947.0f

    const v7, 0x465f2000    # 14280.0f

    const v8, 0x468f3400    # 18330.0f

    const v9, 0x46920400    # 18690.0f

    const v10, 0x466b1400    # 15045.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 517
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 518
    const v3, 0x46679800    # 14822.0f

    const v4, 0x45b73000    # 5862.0f

    const v5, 0x46a8ba00    # 21597.0f

    const v6, 0x466ba800    # 15082.0f

    const v7, 0x46920400    # 18690.0f

    const v8, 0x466b1400    # 15045.0f

    const v9, 0x4502f000    # 2095.0f

    const v10, 0x45ef8800    # 7665.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 519
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    const/high16 v6, 0x43160000    # 150.0f

    add-float/2addr v5, v6

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 520
    const v3, 0x46767000    # 15772.0f

    const/high16 v4, 0x45220000    # 2592.0f

    const v5, 0x46a4e200    # 21105.0f

    const v6, 0x461a2400    # 9865.0f

    const v7, 0x46a33e00    # 20895.0f

    const v8, 0x45ef8800    # 7665.0f

    const v9, 0x46958800    # 19140.0f

    const v10, 0x4529b000    # 2715.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 521
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 522
    const v3, 0x465fe800    # 14330.0f

    const/4 v4, 0x0

    const v5, 0x4695e600    # 19187.0f

    const v6, 0x45ce1800    # 6595.0f

    const v7, 0x46958800    # 19140.0f

    const v8, 0x4529b000    # 2715.0f

    const v9, 0x4668f800    # 14910.0f

    const v10, 0x44924000    # 1170.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 523
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 524
    const v3, 0x462bc000    # 10992.0f

    const/4 v4, 0x0

    const v5, 0x466ff400    # 15357.0f

    const v6, 0x45b9c800    # 5945.0f

    const v7, 0x4668f800    # 14910.0f

    const v8, 0x44924000    # 1170.0f

    const v9, 0x462fc800    # 11250.0f

    const v10, 0x44d02000    # 1665.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 525
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 526
    const v3, 0x45d12000    # 6692.0f

    const v4, 0x44228000    # 650.0f

    const v5, 0x463be400    # 12025.0f

    const v6, 0x45f76800    # 7917.0f

    const v7, 0x462fc800    # 11250.0f

    const v8, 0x44d02000    # 1665.0f

    const v9, 0x45dae800    # 7005.0f

    const v10, 0x45214000    # 2580.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 527
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 528
    const/high16 v3, 0x44ef0000    # 1912.0f

    const v4, 0x44f68000    # 1972.0f

    const v5, 0x46076400    # 8665.0f

    const v6, 0x462e6800    # 11162.0f

    const v7, 0x45dae800    # 7005.0f

    const v8, 0x45214000    # 2580.0f

    const v9, 0x44f3c000    # 1950.0f

    const v10, 0x45e08800    # 7185.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 529
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 530
    invoke-virtual/range {v59 .. v59}, Landroid/graphics/Path;->close()V

    .line 531
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_b

    .line 532
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, v50

    move-object/from16 v1, v59

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 533
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_c

    .line 534
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v50

    move-object/from16 v1, v59

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 536
    :cond_c
    new-instance v59, Landroid/graphics/Path;

    .end local v59    # "path":Landroid/graphics/Path;
    invoke-direct/range {v59 .. v59}, Landroid/graphics/Path;-><init>()V

    .line 537
    .restart local v59    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    const v4, 0x45dfe800    # 7165.0f

    const v5, 0x4587c800    # 4345.0f

    const v6, 0x464cd800    # 13110.0f

    const/high16 v7, 0x44870000    # 1080.0f

    const v8, 0x46464800    # 12690.0f

    const v9, 0x45124000    # 2340.0f

    const v10, 0x464c6000    # 13080.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 538
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 539
    const v3, 0x43ed8000    # 475.0f

    const v4, 0x46375000    # 11732.0f

    const v5, 0x45971800    # 4835.0f

    const v6, 0x4689e400    # 17650.0f

    const v7, 0x4535e000    # 2910.0f

    const v8, 0x4689d000    # 17640.0f

    const v9, 0x45589000    # 3465.0f

    const v10, 0x46884a00    # 17445.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 540
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 541
    const v3, 0x45ef6000    # 7660.0f

    const v4, 0x46417800    # 12382.0f

    const v5, 0x46613000    # 14412.0f

    const v6, 0x46a8ba00    # 21597.0f

    const v7, 0x45f70800    # 7905.0f

    const v8, 0x4691e600    # 18675.0f

    const v9, 0x4600ac00    # 8235.0f

    const v10, 0x4698b200    # 19545.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 542
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 543
    const v3, 0x45ef6000    # 7660.0f

    const v4, 0x46417800    # 12382.0f

    const v5, 0x46613000    # 14412.0f

    const v6, 0x46a8ba00    # 21597.0f

    const v7, 0x465f2000    # 14280.0f

    const v8, 0x468f3400    # 18330.0f

    const/high16 v9, 0x46610000    # 14400.0f

    const v10, 0x4687b400    # 17370.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 544
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 545
    const v3, 0x4649b800    # 12910.0f

    const v4, 0x462d2000    # 11080.0f

    const v5, 0x46920e00    # 18695.0f

    const v6, 0x46940600    # 18947.0f

    const v7, 0x46920400    # 18690.0f

    const v8, 0x466b1400    # 15045.0f

    const v9, 0x46855c00    # 17070.0f

    const v10, 0x46334c00    # 11475.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 546
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 547
    const v3, 0x46767000    # 15772.0f

    const/high16 v4, 0x45220000    # 2592.0f

    const v5, 0x45043000    # 2115.0f

    const v6, 0x461a2400    # 9865.0f

    const v7, 0x469d9e00    # 20175.0f

    const v8, 0x460cdc00    # 9015.0f

    const v9, 0x46a33e00    # 20895.0f

    const v10, 0x45ef8800    # 7665.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 548
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 549
    const v3, 0x465fe800    # 14330.0f

    const/4 v4, 0x0

    const v5, 0x4695e600    # 19187.0f

    const v6, 0x45ce1800    # 6595.0f

    const/high16 v7, 0x46960000    # 19200.0f

    const v8, 0x45511000    # 3345.0f

    const v9, 0x46958800    # 19140.0f

    const v10, 0x4529b000    # 2715.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 550
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 551
    const v3, 0x465fe800    # 14330.0f

    const/4 v4, 0x0

    const v5, 0x4695e600    # 19187.0f

    const v6, 0x45ce1800    # 6595.0f

    const v7, 0x4668f800    # 14910.0f

    const v8, 0x44924000    # 1170.0f

    const v9, 0x46635800    # 14550.0f

    const v10, 0x44f78000    # 1980.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 552
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 553
    const v3, 0x462bc000    # 10992.0f

    const/4 v4, 0x0

    const v5, 0x466ff400    # 15357.0f

    const v6, 0x45b9c800    # 5945.0f

    const v7, 0x462fc800    # 11250.0f

    const v8, 0x44d02000    # 1665.0f

    const v9, 0x462c8000    # 11040.0f

    const v10, 0x45124000    # 2340.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 554
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 555
    const/high16 v3, 0x44ef0000    # 1912.0f

    const v4, 0x44f68000    # 1972.0f

    const v5, 0x46076400    # 8665.0f

    const v6, 0x462e6800    # 11162.0f

    const v7, 0x45ef1000    # 7650.0f

    const v8, 0x454c6000    # 3270.0f

    const v9, 0x45dae800    # 7005.0f

    const v10, 0x45214000    # 2580.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 556
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 557
    const/high16 v3, 0x44ef0000    # 1912.0f

    const v4, 0x44f68000    # 1972.0f

    const v5, 0x46076400    # 8665.0f

    const v6, 0x462e6800    # 11162.0f

    const v7, 0x44f3c000    # 1950.0f

    const v8, 0x45e08800    # 7185.0f

    const v9, 0x45016000    # 2070.0f

    const v10, 0x45f69000    # 7890.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 558
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 559
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_d

    .line 560
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v50

    move-object/from16 v1, v59

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 563
    :cond_d
    new-instance v59, Landroid/graphics/Path;

    .end local v59    # "path":Landroid/graphics/Path;
    invoke-direct/range {v59 .. v59}, Landroid/graphics/Path;-><init>()V

    .line 564
    .restart local v59    # "path":Landroid/graphics/Path;
    mul-float v3, v28, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v43, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 566
    mul-float v3, v41, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v43, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    mul-float v5, v41, v57

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    mul-float v6, v29, v56

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 570
    mul-float v3, v41, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v42, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    mul-float v5, v28, v57

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    mul-float v6, v42, v56

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 574
    mul-float v3, v40, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v42, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    mul-float v5, v40, v57

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    mul-float v6, v29, v56

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 578
    mul-float v3, v40, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v43, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    mul-float v5, v28, v57

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    mul-float v6, v43, v56

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 582
    invoke-virtual/range {v59 .. v59}, Landroid/graphics/Path;->close()V

    .line 583
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_e

    .line 584
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, v50

    move-object/from16 v1, v59

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 585
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_f

    .line 586
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v50

    move-object/from16 v1, v59

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 589
    :cond_f
    new-instance v59, Landroid/graphics/Path;

    .end local v59    # "path":Landroid/graphics/Path;
    invoke-direct/range {v59 .. v59}, Landroid/graphics/Path;-><init>()V

    .line 590
    .restart local v59    # "path":Landroid/graphics/Path;
    mul-float v3, v20, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v39, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 592
    mul-float v3, v37, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v39, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    mul-float v5, v37, v57

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    mul-float v6, v21, v56

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 596
    mul-float v3, v37, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v38, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    mul-float v5, v20, v57

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    mul-float v6, v38, v56

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 600
    mul-float v3, v36, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v38, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    mul-float v5, v36, v57

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    mul-float v6, v21, v56

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 604
    mul-float v3, v36, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v39, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    mul-float v5, v20, v57

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    mul-float v6, v39, v56

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 608
    invoke-virtual/range {v59 .. v59}, Landroid/graphics/Path;->close()V

    .line 609
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_10

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_10

    .line 610
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, v50

    move-object/from16 v1, v59

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 611
    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_11

    .line 612
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v50

    move-object/from16 v1, v59

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 615
    :cond_11
    new-instance v59, Landroid/graphics/Path;

    .end local v59    # "path":Landroid/graphics/Path;
    invoke-direct/range {v59 .. v59}, Landroid/graphics/Path;-><init>()V

    .line 616
    .restart local v59    # "path":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v3, v3

    mul-float v3, v3, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v34, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 618
    mul-float v3, v32, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v34, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    mul-float v5, v32, v57

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v6, v6

    mul-float v6, v6, v56

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 622
    mul-float v3, v32, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v33, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v5, v5

    mul-float v5, v5, v57

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    mul-float v6, v33, v56

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 626
    mul-float v3, v31, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v33, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    mul-float v5, v31, v57

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v6, v6

    mul-float v6, v6, v56

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 630
    mul-float v3, v31, v57

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v34, v56

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v5, v5

    mul-float v5, v5, v57

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    mul-float v6, v34, v56

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 634
    invoke-virtual/range {v59 .. v59}, Landroid/graphics/Path;->close()V

    .line 635
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_12

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_12

    .line 636
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, v50

    move-object/from16 v1, v59

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 637
    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_13

    .line 638
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v50

    move-object/from16 v1, v59

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 642
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->textArea:Landroid/graphics/RectF;

    const v4, 0x457d2000    # 4050.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 643
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->textArea:Landroid/graphics/RectF;

    const v4, 0x457d2000    # 4050.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 644
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->textArea:Landroid/graphics/RectF;

    const v4, 0x46680800    # 14850.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 645
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->textArea:Landroid/graphics/RectF;

    const v4, 0x46680800    # 14850.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    move-object v2, v11

    .line 649
    .end local v11    # "_bitmap":Landroid/graphics/Bitmap;
    .end local v12    # "at0":F
    .end local v13    # "at1":F
    .end local v14    # "at10":F
    .end local v15    # "at11":F
    .end local v16    # "at12":F
    .end local v17    # "at13":F
    .end local v18    # "at14":F
    .end local v19    # "at15":F
    .end local v20    # "at16":F
    .end local v21    # "at17":F
    .end local v22    # "at18":F
    .end local v23    # "at19":F
    .end local v24    # "at2":F
    .end local v25    # "at20":F
    .end local v26    # "at21":F
    .end local v27    # "at22":F
    .end local v28    # "at23":F
    .end local v29    # "at24":F
    .end local v30    # "at25":F
    .end local v31    # "at26":F
    .end local v32    # "at27":F
    .end local v33    # "at28":F
    .end local v34    # "at29":F
    .end local v35    # "at3":F
    .end local v36    # "at30":F
    .end local v37    # "at31":F
    .end local v38    # "at32":F
    .end local v39    # "at33":F
    .end local v40    # "at34":F
    .end local v41    # "at35":F
    .end local v42    # "at36":F
    .end local v43    # "at37":F
    .end local v44    # "at4":F
    .end local v45    # "at5":F
    .end local v46    # "at6":F
    .end local v47    # "at7":F
    .end local v48    # "at8":F
    .end local v49    # "at9":F
    .end local v50    # "canvas":Landroid/graphics/Canvas;
    .end local v59    # "path":Landroid/graphics/Path;
    .restart local v2    # "_bitmap":Landroid/graphics/Bitmap;
    :cond_14
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mDrawOnCanvas:Z

    if-nez v3, :cond_0

    .line 650
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 652
    .local v7, "mtx":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->flipHorizontal:Z

    if-eqz v3, :cond_4e

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->flipVertical:Z

    if-nez v3, :cond_4e

    .line 653
    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 662
    :cond_15
    :goto_39
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rotation:I

    int-to-float v3, v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    invoke-virtual {v7, v3, v4, v5}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 665
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v60

    .line 669
    .local v60, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v58, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->folderPath:Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "pic_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->shapeName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ".png"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v58

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 675
    .local v58, "out":Ljava/io/FileOutputStream;
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    move-object/from16 v0, v60

    move-object/from16 v1, v58

    invoke-virtual {v0, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 676
    invoke-virtual/range {v58 .. v58}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 677
    .end local v58    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v51

    .line 678
    .local v51, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception while writing pictures to out file"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v51

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 210
    .end local v7    # "mtx":Landroid/graphics/Matrix;
    .end local v51    # "e":Ljava/lang/Exception;
    .end local v60    # "rotatedBMP":Landroid/graphics/Bitmap;
    .restart local v12    # "at0":F
    .restart local v13    # "at1":F
    .restart local v24    # "at2":F
    .restart local v35    # "at3":F
    .restart local v44    # "at4":F
    .restart local v45    # "at5":F
    :cond_16
    const v46, 0x4644e000    # 12600.0f

    goto/16 :goto_1

    .line 211
    .restart local v46    # "at6":F
    :cond_17
    const v47, 0x468ca000    # 18000.0f

    goto/16 :goto_2

    .line 212
    .restart local v47    # "at7":F
    :cond_18
    const v48, 0x4644e000    # 12600.0f

    goto/16 :goto_3

    .line 213
    .restart local v48    # "at8":F
    :cond_19
    const v49, 0x468ca000    # 18000.0f

    goto/16 :goto_4

    .line 214
    .restart local v49    # "at9":F
    :cond_1a
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v14, v3

    goto/16 :goto_5

    .line 215
    .restart local v14    # "at10":F
    :cond_1b
    const/4 v15, 0x0

    goto/16 :goto_6

    .restart local v15    # "at11":F
    :cond_1c
    move/from16 v16, v15

    .line 216
    goto/16 :goto_7

    .line 217
    .restart local v16    # "at12":F
    :cond_1d
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v0, v3

    move/from16 v17, v0

    goto/16 :goto_8

    .restart local v17    # "at13":F
    :cond_1e
    move/from16 v18, v17

    .line 218
    goto/16 :goto_9

    .restart local v18    # "at14":F
    :cond_1f
    move/from16 v19, v18

    .line 219
    goto/16 :goto_a

    .line 220
    .restart local v19    # "at15":F
    :cond_20
    const v20, 0x46a8c000    # 21600.0f

    goto/16 :goto_b

    .restart local v20    # "at16":F
    :cond_21
    move/from16 v21, v20

    .line 221
    goto/16 :goto_c

    .restart local v21    # "at17":F
    :cond_22
    move/from16 v22, v21

    .line 222
    goto/16 :goto_d

    .restart local v22    # "at18":F
    :cond_23
    move/from16 v23, v46

    .line 223
    goto/16 :goto_e

    .restart local v23    # "at19":F
    :cond_24
    move/from16 v25, v46

    .line 224
    goto/16 :goto_f

    .restart local v25    # "at20":F
    :cond_25
    move/from16 v26, v25

    .line 225
    goto/16 :goto_10

    .line 226
    .restart local v26    # "at21":F
    :cond_26
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v0, v3

    move/from16 v27, v0

    goto/16 :goto_11

    .restart local v27    # "at22":F
    :cond_27
    move/from16 v28, v48

    .line 227
    goto/16 :goto_12

    .restart local v28    # "at23":F
    :cond_28
    move/from16 v29, v28

    .line 228
    goto/16 :goto_13

    .line 229
    .restart local v29    # "at24":F
    :cond_29
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v0, v3

    move/from16 v30, v0

    goto/16 :goto_14

    .restart local v30    # "at25":F
    :cond_2a
    move/from16 v31, v30

    .line 230
    goto/16 :goto_15

    .restart local v31    # "at26":F
    :cond_2b
    move/from16 v32, v31

    .line 231
    goto/16 :goto_16

    .restart local v32    # "at27":F
    :cond_2c
    move/from16 v33, v48

    .line 232
    goto/16 :goto_17

    .restart local v33    # "at28":F
    :cond_2d
    move/from16 v34, v33

    .line 233
    goto/16 :goto_18

    .restart local v34    # "at29":F
    :cond_2e
    move/from16 v36, v34

    .line 234
    goto/16 :goto_19

    .line 235
    .restart local v36    # "at30":F
    :cond_2f
    const/16 v37, 0x0

    goto/16 :goto_1a

    .line 236
    .restart local v37    # "at31":F
    :cond_30
    const/16 v38, 0x0

    goto/16 :goto_1b

    .restart local v38    # "at32":F
    :cond_31
    move/from16 v39, v38

    .line 237
    goto/16 :goto_1c

    .line 301
    .end local v14    # "at10":F
    .end local v15    # "at11":F
    .end local v16    # "at12":F
    .end local v17    # "at13":F
    .end local v18    # "at14":F
    .end local v19    # "at15":F
    .end local v20    # "at16":F
    .end local v21    # "at17":F
    .end local v22    # "at18":F
    .end local v23    # "at19":F
    .end local v25    # "at20":F
    .end local v26    # "at21":F
    .end local v27    # "at22":F
    .end local v28    # "at23":F
    .end local v29    # "at24":F
    .end local v30    # "at25":F
    .end local v31    # "at26":F
    .end local v32    # "at27":F
    .end local v33    # "at28":F
    .end local v34    # "at29":F
    .end local v36    # "at30":F
    .end local v37    # "at31":F
    .end local v38    # "at32":F
    .end local v46    # "at6":F
    .end local v47    # "at7":F
    .end local v48    # "at8":F
    .end local v49    # "at9":F
    :cond_32
    const v46, 0x4644e000    # 12600.0f

    goto/16 :goto_1d

    .line 302
    .restart local v46    # "at6":F
    :cond_33
    const v47, 0x468ca000    # 18000.0f

    goto/16 :goto_1e

    .line 303
    .restart local v47    # "at7":F
    :cond_34
    const v48, 0x4644e000    # 12600.0f

    goto/16 :goto_1f

    .line 304
    .restart local v48    # "at8":F
    :cond_35
    const v49, 0x468ca000    # 18000.0f

    goto/16 :goto_20

    .line 305
    .restart local v49    # "at9":F
    :cond_36
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v14, v3

    goto/16 :goto_21

    .line 306
    .restart local v14    # "at10":F
    :cond_37
    const/4 v15, 0x0

    goto/16 :goto_22

    .restart local v15    # "at11":F
    :cond_38
    move/from16 v16, v15

    .line 307
    goto/16 :goto_23

    .line 308
    .restart local v16    # "at12":F
    :cond_39
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v0, v3

    move/from16 v17, v0

    goto/16 :goto_24

    .restart local v17    # "at13":F
    :cond_3a
    move/from16 v18, v17

    .line 309
    goto/16 :goto_25

    .restart local v18    # "at14":F
    :cond_3b
    move/from16 v19, v18

    .line 310
    goto/16 :goto_26

    .line 311
    .restart local v19    # "at15":F
    :cond_3c
    const v20, 0x46a8c000    # 21600.0f

    goto/16 :goto_27

    .restart local v20    # "at16":F
    :cond_3d
    move/from16 v21, v20

    .line 312
    goto/16 :goto_28

    .restart local v21    # "at17":F
    :cond_3e
    move/from16 v22, v21

    .line 313
    goto/16 :goto_29

    .restart local v22    # "at18":F
    :cond_3f
    move/from16 v23, v46

    .line 314
    goto/16 :goto_2a

    .restart local v23    # "at19":F
    :cond_40
    move/from16 v25, v46

    .line 315
    goto/16 :goto_2b

    .restart local v25    # "at20":F
    :cond_41
    move/from16 v26, v25

    .line 316
    goto/16 :goto_2c

    .line 317
    .restart local v26    # "at21":F
    :cond_42
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v0, v3

    move/from16 v27, v0

    goto/16 :goto_2d

    .restart local v27    # "at22":F
    :cond_43
    move/from16 v28, v48

    .line 318
    goto/16 :goto_2e

    .restart local v28    # "at23":F
    :cond_44
    move/from16 v29, v28

    .line 319
    goto/16 :goto_2f

    .line 320
    .restart local v29    # "at24":F
    :cond_45
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v0, v3

    move/from16 v30, v0

    goto/16 :goto_30

    .restart local v30    # "at25":F
    :cond_46
    move/from16 v31, v30

    .line 321
    goto/16 :goto_31

    .restart local v31    # "at26":F
    :cond_47
    move/from16 v32, v31

    .line 322
    goto/16 :goto_32

    .restart local v32    # "at27":F
    :cond_48
    move/from16 v33, v48

    .line 323
    goto/16 :goto_33

    .restart local v33    # "at28":F
    :cond_49
    move/from16 v34, v33

    .line 324
    goto/16 :goto_34

    .restart local v34    # "at29":F
    :cond_4a
    move/from16 v36, v34

    .line 325
    goto/16 :goto_35

    .line 326
    .restart local v36    # "at30":F
    :cond_4b
    const/16 v37, 0x0

    goto/16 :goto_36

    .line 327
    .restart local v37    # "at31":F
    :cond_4c
    const/16 v38, 0x0

    goto/16 :goto_37

    .restart local v38    # "at32":F
    :cond_4d
    move/from16 v39, v38

    .line 328
    goto/16 :goto_38

    .line 655
    .end local v12    # "at0":F
    .end local v13    # "at1":F
    .end local v14    # "at10":F
    .end local v15    # "at11":F
    .end local v16    # "at12":F
    .end local v17    # "at13":F
    .end local v18    # "at14":F
    .end local v19    # "at15":F
    .end local v20    # "at16":F
    .end local v21    # "at17":F
    .end local v22    # "at18":F
    .end local v23    # "at19":F
    .end local v24    # "at2":F
    .end local v25    # "at20":F
    .end local v26    # "at21":F
    .end local v27    # "at22":F
    .end local v28    # "at23":F
    .end local v29    # "at24":F
    .end local v30    # "at25":F
    .end local v31    # "at26":F
    .end local v32    # "at27":F
    .end local v33    # "at28":F
    .end local v34    # "at29":F
    .end local v35    # "at3":F
    .end local v36    # "at30":F
    .end local v37    # "at31":F
    .end local v38    # "at32":F
    .end local v44    # "at4":F
    .end local v45    # "at5":F
    .end local v46    # "at6":F
    .end local v47    # "at7":F
    .end local v48    # "at8":F
    .end local v49    # "at9":F
    .restart local v7    # "mtx":Landroid/graphics/Matrix;
    :cond_4e
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->flipVertical:Z

    if-eqz v3, :cond_4f

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->flipHorizontal:Z

    if-nez v3, :cond_4f

    .line 656
    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, -0x40800000    # -1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    goto/16 :goto_39

    .line 658
    :cond_4f
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->flipHorizontal:Z

    if-eqz v3, :cond_15

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->flipVertical:Z

    if-eqz v3, :cond_15

    .line 659
    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v4, -0x40800000    # -1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    goto/16 :goto_39
.end method

.method public draw(Landroid/graphics/Canvas;I)V
    .locals 62
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeCount"    # I

    .prologue
    .line 708
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->shapeName:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 1224
    :cond_0
    :goto_0
    return-void

    .line 711
    :cond_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->bitmapHight:I

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->newBitmapHeight:I

    .line 712
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->bitmapWidth:I

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->newBitmapWidth:I

    .line 714
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->width:F

    const v4, 0x46a8c000    # 21600.0f

    div-float v56, v3, v4

    .line 715
    .local v56, "mulFactor_Width":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->height:F

    const v4, 0x46a8c000    # 21600.0f

    div-float v55, v3, v4

    .line 725
    .local v55, "mulFactor_Height":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    if-nez v3, :cond_2

    .line 726
    const/16 v3, 0x546

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    .line 727
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    if-nez v3, :cond_3

    .line 728
    const/16 v3, 0x6540

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    .line 730
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    const/4 v4, 0x0

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v53

    .line 731
    .local v53, "minX":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    const/16 v4, 0x5460

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v51

    .line 732
    .local v51, "maxX":I
    move/from16 v0, v53

    int-to-float v3, v0

    mul-float v3, v3, v56

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    .line 733
    move/from16 v0, v51

    int-to-float v3, v0

    mul-float v3, v3, v56

    move/from16 v0, v53

    int-to-float v4, v0

    mul-float v4, v4, v56

    sub-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->newBitmapWidth:I

    .line 735
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    const/4 v4, 0x0

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v54

    .line 736
    .local v54, "minY":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    const/16 v4, 0x5460

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v52

    .line 737
    .local v52, "maxY":I
    move/from16 v0, v54

    int-to-float v3, v0

    mul-float v3, v3, v55

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    .line 738
    move/from16 v0, v52

    int-to-float v3, v0

    mul-float v3, v3, v55

    move/from16 v0, v54

    int-to-float v4, v0

    mul-float v4, v4, v55

    sub-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->newBitmapHeight:I

    .line 740
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->newBitmapWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->newBitmapHeight:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 745
    .local v2, "_bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "WedgeRectCallout"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 746
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    rsub-int v3, v3, 0x2a30

    int-to-float v12, v3

    .line 747
    .local v12, "at0":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    rsub-int v3, v3, 0x2a30

    int-to-float v13, v3

    .line 748
    .local v13, "at1":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    sub-int/2addr v3, v4

    int-to-float v0, v3

    move/from16 v24, v0

    .line 749
    .local v24, "at2":F
    add-float v35, v12, v13

    .line 750
    .local v35, "at3":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    rsub-int v3, v3, 0x5460

    int-to-float v0, v3

    move/from16 v44, v0

    .line 751
    .local v44, "at4":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    rsub-int v3, v3, 0x5460

    int-to-float v0, v3

    move/from16 v45, v0

    .line 752
    .local v45, "at5":F
    const/4 v3, 0x0

    cmpl-float v3, v12, v3

    if-lez v3, :cond_16

    const/high16 v46, 0x45610000    # 3600.0f

    .line 753
    .local v46, "at6":F
    :goto_1
    const/4 v3, 0x0

    cmpl-float v3, v12, v3

    if-lez v3, :cond_17

    const v47, 0x460ca000    # 9000.0f

    .line 754
    .local v47, "at7":F
    :goto_2
    const/4 v3, 0x0

    cmpl-float v3, v13, v3

    if-lez v3, :cond_18

    const/high16 v48, 0x45610000    # 3600.0f

    .line 755
    .local v48, "at8":F
    :goto_3
    const/4 v3, 0x0

    cmpl-float v3, v13, v3

    if-lez v3, :cond_19

    const v49, 0x460ca000    # 9000.0f

    .line 756
    .local v49, "at9":F
    :goto_4
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_1a

    const/4 v14, 0x0

    .line 757
    .local v14, "at10":F
    :goto_5
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_1b

    move v15, v14

    .line 758
    .local v15, "at11":F
    :goto_6
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    if-lez v3, :cond_1c

    const/16 v16, 0x0

    .line 759
    .local v16, "at12":F
    :goto_7
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_1d

    move/from16 v17, v46

    .line 760
    .local v17, "at13":F
    :goto_8
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_1e

    move/from16 v18, v46

    .line 761
    .local v18, "at14":F
    :goto_9
    const/4 v3, 0x0

    cmpl-float v3, v45, v3

    if-lez v3, :cond_1f

    move/from16 v19, v46

    .line 762
    .local v19, "at15":F
    :goto_a
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_20

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v0, v3

    move/from16 v20, v0

    .line 763
    .local v20, "at16":F
    :goto_b
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_21

    const v21, 0x46a8c000    # 21600.0f

    .line 764
    .local v21, "at17":F
    :goto_c
    const/4 v3, 0x0

    cmpl-float v3, v44, v3

    if-lez v3, :cond_22

    const v22, 0x46a8c000    # 21600.0f

    .line 765
    .local v22, "at18":F
    :goto_d
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_23

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v0, v3

    move/from16 v23, v0

    .line 766
    .local v23, "at19":F
    :goto_e
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_24

    move/from16 v25, v23

    .line 767
    .local v25, "at20":F
    :goto_f
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    if-lez v3, :cond_25

    move/from16 v26, v46

    .line 768
    .local v26, "at21":F
    :goto_10
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_26

    move/from16 v27, v48

    .line 769
    .local v27, "at22":F
    :goto_11
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_27

    move/from16 v28, v27

    .line 770
    .local v28, "at23":F
    :goto_12
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    if-lez v3, :cond_28

    move/from16 v29, v48

    .line 771
    .local v29, "at24":F
    :goto_13
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_29

    const v30, 0x46a8c000    # 21600.0f

    .line 772
    .local v30, "at25":F
    :goto_14
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_2a

    const v31, 0x46a8c000    # 21600.0f

    .line 773
    .local v31, "at26":F
    :goto_15
    const/4 v3, 0x0

    cmpl-float v3, v45, v3

    if-lez v3, :cond_2b

    const v32, 0x46a8c000    # 21600.0f

    .line 774
    .local v32, "at27":F
    :goto_16
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_2c

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v0, v3

    move/from16 v33, v0

    .line 775
    .local v33, "at28":F
    :goto_17
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_2d

    move/from16 v34, v48

    .line 776
    .local v34, "at29":F
    :goto_18
    const/4 v3, 0x0

    cmpl-float v3, v44, v3

    if-lez v3, :cond_2e

    move/from16 v36, v48

    .line 777
    .local v36, "at30":F
    :goto_19
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_2f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v0, v3

    move/from16 v37, v0

    .line 778
    .local v37, "at31":F
    :goto_1a
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_30

    move/from16 v38, v37

    .line 779
    .local v38, "at32":F
    :goto_1b
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    if-lez v3, :cond_31

    const/16 v39, 0x0

    .line 781
    .local v39, "at33":F
    :goto_1c
    new-instance v58, Landroid/graphics/Path;

    invoke-direct/range {v58 .. v58}, Landroid/graphics/Path;-><init>()V

    .line 786
    .local v58, "path":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    rsub-int/lit8 v3, v3, 0x0

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    rsub-int/lit8 v4, v4, 0x0

    int-to-float v4, v4

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 787
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    rsub-int/lit8 v3, v3, 0x0

    int-to-float v3, v3

    mul-float v4, v48, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 789
    mul-float v3, v16, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v29, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 791
    const/4 v3, 0x0

    mul-float v3, v3, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v49, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 793
    const/4 v3, 0x0

    mul-float v3, v3, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 795
    mul-float v3, v46, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 797
    mul-float v3, v19, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v32, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 799
    mul-float v3, v47, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 801
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 803
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v49, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 805
    mul-float v3, v22, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v36, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 807
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v48, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 809
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 811
    mul-float v3, v47, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 813
    mul-float v3, v26, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v39, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 815
    mul-float v3, v46, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 817
    invoke-virtual/range {v58 .. v58}, Landroid/graphics/Path;->close()V

    .line 821
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_4

    .line 822
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 823
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_5

    .line 824
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 828
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 829
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 830
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 831
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    .line 835
    .end local v12    # "at0":F
    .end local v13    # "at1":F
    .end local v14    # "at10":F
    .end local v15    # "at11":F
    .end local v16    # "at12":F
    .end local v17    # "at13":F
    .end local v18    # "at14":F
    .end local v19    # "at15":F
    .end local v20    # "at16":F
    .end local v21    # "at17":F
    .end local v22    # "at18":F
    .end local v23    # "at19":F
    .end local v24    # "at2":F
    .end local v25    # "at20":F
    .end local v26    # "at21":F
    .end local v27    # "at22":F
    .end local v28    # "at23":F
    .end local v29    # "at24":F
    .end local v30    # "at25":F
    .end local v31    # "at26":F
    .end local v32    # "at27":F
    .end local v33    # "at28":F
    .end local v34    # "at29":F
    .end local v35    # "at3":F
    .end local v36    # "at30":F
    .end local v37    # "at31":F
    .end local v38    # "at32":F
    .end local v39    # "at33":F
    .end local v44    # "at4":F
    .end local v45    # "at5":F
    .end local v46    # "at6":F
    .end local v47    # "at7":F
    .end local v48    # "at8":F
    .end local v49    # "at9":F
    .end local v58    # "path":Landroid/graphics/Path;
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "WedgeRRectCallout"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "wedgeRoundRectCallout"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 837
    :cond_7
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    rsub-int v3, v3, 0x2a30

    int-to-float v12, v3

    .line 838
    .restart local v12    # "at0":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    rsub-int v3, v3, 0x2a30

    int-to-float v13, v3

    .line 839
    .restart local v13    # "at1":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    sub-int/2addr v3, v4

    int-to-float v0, v3

    move/from16 v24, v0

    .line 840
    .restart local v24    # "at2":F
    add-float v35, v12, v13

    .line 841
    .restart local v35    # "at3":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    rsub-int v3, v3, 0x5460

    int-to-float v0, v3

    move/from16 v44, v0

    .line 842
    .restart local v44    # "at4":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    rsub-int v3, v3, 0x5460

    int-to-float v0, v3

    move/from16 v45, v0

    .line 843
    .restart local v45    # "at5":F
    const/4 v3, 0x0

    cmpl-float v3, v12, v3

    if-lez v3, :cond_32

    const/high16 v46, 0x45610000    # 3600.0f

    .line 844
    .restart local v46    # "at6":F
    :goto_1d
    const/4 v3, 0x0

    cmpl-float v3, v12, v3

    if-lez v3, :cond_33

    const v47, 0x460ca000    # 9000.0f

    .line 845
    .restart local v47    # "at7":F
    :goto_1e
    const/4 v3, 0x0

    cmpl-float v3, v13, v3

    if-lez v3, :cond_34

    const/high16 v48, 0x45610000    # 3600.0f

    .line 846
    .restart local v48    # "at8":F
    :goto_1f
    const/4 v3, 0x0

    cmpl-float v3, v13, v3

    if-lez v3, :cond_35

    const v49, 0x460ca000    # 9000.0f

    .line 847
    .restart local v49    # "at9":F
    :goto_20
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_36

    const/4 v14, 0x0

    .line 848
    .restart local v14    # "at10":F
    :goto_21
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_37

    move v15, v14

    .line 849
    .restart local v15    # "at11":F
    :goto_22
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    if-lez v3, :cond_38

    const/16 v16, 0x0

    .line 850
    .restart local v16    # "at12":F
    :goto_23
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_39

    move/from16 v17, v46

    .line 851
    .restart local v17    # "at13":F
    :goto_24
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_3a

    move/from16 v18, v46

    .line 852
    .restart local v18    # "at14":F
    :goto_25
    const/4 v3, 0x0

    cmpl-float v3, v45, v3

    if-lez v3, :cond_3b

    move/from16 v19, v46

    .line 853
    .restart local v19    # "at15":F
    :goto_26
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_3c

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v0, v3

    move/from16 v20, v0

    .line 854
    .restart local v20    # "at16":F
    :goto_27
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_3d

    const v21, 0x46a8c000    # 21600.0f

    .line 855
    .restart local v21    # "at17":F
    :goto_28
    const/4 v3, 0x0

    cmpl-float v3, v44, v3

    if-lez v3, :cond_3e

    const v22, 0x46a8c000    # 21600.0f

    .line 856
    .restart local v22    # "at18":F
    :goto_29
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_3f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v0, v3

    move/from16 v23, v0

    .line 857
    .restart local v23    # "at19":F
    :goto_2a
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_40

    move/from16 v25, v23

    .line 858
    .restart local v25    # "at20":F
    :goto_2b
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    if-lez v3, :cond_41

    move/from16 v26, v46

    .line 859
    .restart local v26    # "at21":F
    :goto_2c
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_42

    move/from16 v27, v48

    .line 860
    .restart local v27    # "at22":F
    :goto_2d
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_43

    move/from16 v28, v27

    .line 861
    .restart local v28    # "at23":F
    :goto_2e
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    if-lez v3, :cond_44

    move/from16 v29, v48

    .line 862
    .restart local v29    # "at24":F
    :goto_2f
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_45

    const v30, 0x46a8c000    # 21600.0f

    .line 863
    .restart local v30    # "at25":F
    :goto_30
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_46

    const v31, 0x46a8c000    # 21600.0f

    .line 864
    .restart local v31    # "at26":F
    :goto_31
    const/4 v3, 0x0

    cmpl-float v3, v45, v3

    if-lez v3, :cond_47

    const v32, 0x46a8c000    # 21600.0f

    .line 865
    .restart local v32    # "at27":F
    :goto_32
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_48

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v0, v3

    move/from16 v33, v0

    .line 866
    .restart local v33    # "at28":F
    :goto_33
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_49

    move/from16 v34, v48

    .line 867
    .restart local v34    # "at29":F
    :goto_34
    const/4 v3, 0x0

    cmpl-float v3, v44, v3

    if-lez v3, :cond_4a

    move/from16 v36, v48

    .line 868
    .restart local v36    # "at30":F
    :goto_35
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-lez v3, :cond_4b

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v0, v3

    move/from16 v37, v0

    .line 869
    .restart local v37    # "at31":F
    :goto_36
    const/4 v3, 0x0

    cmpl-float v3, v35, v3

    if-lez v3, :cond_4c

    move/from16 v38, v37

    .line 870
    .restart local v38    # "at32":F
    :goto_37
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    if-lez v3, :cond_4d

    const/16 v39, 0x0

    .line 872
    .restart local v39    # "at33":F
    :goto_38
    new-instance v58, Landroid/graphics/Path;

    invoke-direct/range {v58 .. v58}, Landroid/graphics/Path;-><init>()V

    .line 873
    .restart local v58    # "path":Landroid/graphics/Path;
    const/high16 v3, 0x45610000    # 3600.0f

    mul-float v3, v3, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 875
    const/4 v3, 0x0

    mul-float v3, v3, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    mul-float v5, v5, v56

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    const/high16 v6, 0x45610000    # 3600.0f

    mul-float v6, v6, v55

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 879
    const/4 v3, 0x0

    mul-float v3, v3, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v48, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 881
    mul-float v3, v16, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v29, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 883
    const/4 v3, 0x0

    mul-float v3, v3, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v49, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 885
    const/4 v3, 0x0

    mul-float v3, v3, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x468ca000    # 18000.0f

    mul-float v4, v4, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 887
    const/4 v3, 0x0

    mul-float v3, v3, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    const/high16 v5, 0x45610000    # 3600.0f

    mul-float v5, v5, v56

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    mul-float v6, v6, v55

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 891
    mul-float v3, v46, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 893
    mul-float v3, v19, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v32, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 895
    mul-float v3, v47, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 897
    const v3, 0x468ca000    # 18000.0f

    mul-float v3, v3, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 899
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    mul-float v5, v5, v56

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    const v6, 0x468ca000    # 18000.0f

    mul-float v6, v6, v55

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 903
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v49, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 905
    mul-float v3, v22, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v36, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 907
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v48, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 909
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/high16 v4, 0x45610000    # 3600.0f

    mul-float v4, v4, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 911
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    const v5, 0x468ca000    # 18000.0f

    mul-float v5, v5, v56

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    const/4 v6, 0x0

    mul-float v6, v6, v55

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 915
    mul-float v3, v47, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 917
    mul-float v3, v26, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v39, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 919
    mul-float v3, v46, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 921
    invoke-virtual/range {v58 .. v58}, Landroid/graphics/Path;->close()V

    .line 925
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_8

    .line 926
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 927
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_9

    .line 928
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 932
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->textArea:Landroid/graphics/RectF;

    const v4, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 933
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->textArea:Landroid/graphics/RectF;

    const v4, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 934
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->textArea:Landroid/graphics/RectF;

    const v4, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 935
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->textArea:Landroid/graphics/RectF;

    const v4, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    .line 997
    .end local v12    # "at0":F
    .end local v13    # "at1":F
    .end local v14    # "at10":F
    .end local v15    # "at11":F
    .end local v16    # "at12":F
    .end local v17    # "at13":F
    .end local v18    # "at14":F
    .end local v19    # "at15":F
    .end local v20    # "at16":F
    .end local v21    # "at17":F
    .end local v22    # "at18":F
    .end local v23    # "at19":F
    .end local v24    # "at2":F
    .end local v25    # "at20":F
    .end local v26    # "at21":F
    .end local v27    # "at22":F
    .end local v28    # "at23":F
    .end local v29    # "at24":F
    .end local v30    # "at25":F
    .end local v31    # "at26":F
    .end local v32    # "at27":F
    .end local v33    # "at28":F
    .end local v34    # "at29":F
    .end local v35    # "at3":F
    .end local v36    # "at30":F
    .end local v37    # "at31":F
    .end local v38    # "at32":F
    .end local v39    # "at33":F
    .end local v44    # "at4":F
    .end local v45    # "at5":F
    .end local v46    # "at6":F
    .end local v47    # "at7":F
    .end local v48    # "at8":F
    .end local v49    # "at9":F
    .end local v58    # "path":Landroid/graphics/Path;
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "CloudCallout"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 998
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->newBitmapWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->newBitmapHeight:I

    add-int/lit8 v4, v4, 0x4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 1002
    .end local v2    # "_bitmap":Landroid/graphics/Bitmap;
    .local v11, "_bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    add-int/lit16 v3, v3, -0x2a30

    int-to-float v12, v3

    .line 1003
    .restart local v12    # "at0":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    add-int/lit16 v3, v3, -0x2a30

    int-to-float v13, v3

    .line 1004
    .restart local v13    # "at1":F
    const-wide v4, 0x40c5180000000000L    # 10800.0

    float-to-double v8, v13

    float-to-double v0, v12

    move-wide/from16 v60, v0

    move-wide/from16 v0, v60

    invoke-static {v8, v9, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    mul-double/2addr v4, v8

    double-to-float v0, v4

    move/from16 v24, v0

    .line 1008
    .restart local v24    # "at2":F
    const-wide v4, 0x40c5180000000000L    # 10800.0

    float-to-double v8, v13

    float-to-double v0, v12

    move-wide/from16 v60, v0

    move-wide/from16 v0, v60

    invoke-static {v8, v9, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    mul-double/2addr v4, v8

    double-to-float v0, v4

    move/from16 v35, v0

    .line 1012
    .restart local v35    # "at3":F
    const v3, 0x4628c000    # 10800.0f

    add-float v44, v24, v3

    .line 1013
    .restart local v44    # "at4":F
    const v3, 0x4628c000    # 10800.0f

    add-float v45, v35, v3

    .line 1014
    .restart local v45    # "at5":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v3, v3

    sub-float v46, v44, v3

    .line 1015
    .restart local v46    # "at6":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v3, v3

    sub-float v47, v45, v3

    .line 1016
    .restart local v47    # "at7":F
    mul-float v3, v46, v46

    mul-float v4, v47, v47

    add-float/2addr v3, v4

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v48, v0

    .line 1018
    .restart local v48    # "at8":F
    const v49, 0x45ce4000    # 6600.0f

    .line 1019
    .restart local v49    # "at9":F
    sub-float v14, v48, v49

    .line 1020
    .restart local v14    # "at10":F
    const/high16 v3, 0x40400000    # 3.0f

    div-float v15, v14, v3

    .line 1021
    .restart local v15    # "at11":F
    const/high16 v16, 0x44e10000    # 1800.0f

    .line 1022
    .restart local v16    # "at12":F
    add-float v17, v15, v16

    .line 1023
    .restart local v17    # "at13":F
    mul-float v3, v17, v46

    div-float v18, v3, v48

    .line 1024
    .restart local v18    # "at14":F
    mul-float v3, v17, v47

    div-float v19, v3, v48

    .line 1025
    .restart local v19    # "at15":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v3, v3

    add-float v20, v18, v3

    .line 1026
    .restart local v20    # "at16":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v3, v3

    add-float v21, v19, v3

    .line 1027
    .restart local v21    # "at17":F
    const/high16 v22, 0x45960000    # 4800.0f

    .line 1028
    .restart local v22    # "at18":F
    const/high16 v3, 0x40000000    # 2.0f

    mul-float v23, v15, v3

    .line 1029
    .restart local v23    # "at19":F
    add-float v25, v22, v23

    .line 1030
    .restart local v25    # "at20":F
    mul-float v3, v25, v46

    div-float v26, v3, v48

    .line 1031
    .restart local v26    # "at21":F
    mul-float v3, v25, v47

    div-float v27, v3, v48

    .line 1032
    .restart local v27    # "at22":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v3, v3

    add-float v28, v26, v3

    .line 1033
    .restart local v28    # "at23":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v3, v3

    add-float v29, v27, v3

    .line 1034
    .restart local v29    # "at24":F
    const/high16 v30, 0x44960000    # 1200.0f

    .line 1035
    .restart local v30    # "at25":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    add-int/lit16 v3, v3, 0x258

    int-to-float v0, v3

    move/from16 v31, v0

    .line 1036
    .restart local v31    # "at26":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    add-int/lit16 v3, v3, -0x258

    int-to-float v0, v3

    move/from16 v32, v0

    .line 1037
    .restart local v32    # "at27":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    add-int/lit16 v3, v3, 0x258

    int-to-float v0, v3

    move/from16 v33, v0

    .line 1038
    .restart local v33    # "at28":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    add-int/lit16 v3, v3, -0x258

    int-to-float v0, v3

    move/from16 v34, v0

    .line 1039
    .restart local v34    # "at29":F
    add-float v36, v20, v30

    .line 1040
    .restart local v36    # "at30":F
    sub-float v37, v20, v30

    .line 1041
    .restart local v37    # "at31":F
    add-float v38, v21, v30

    .line 1042
    .restart local v38    # "at32":F
    sub-float v39, v21, v30

    .line 1043
    .restart local v39    # "at33":F
    add-float v40, v28, v16

    .line 1044
    .local v40, "at34":F
    sub-float v41, v28, v16

    .line 1045
    .local v41, "at35":F
    add-float v42, v29, v16

    .line 1046
    .local v42, "at36":F
    sub-float v43, v29, v16

    .line 1049
    .local v43, "at37":F
    new-instance v58, Landroid/graphics/Path;

    invoke-direct/range {v58 .. v58}, Landroid/graphics/Path;-><init>()V

    .line 1050
    .restart local v58    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    const v4, 0x45dfe800    # 7165.0f

    const v5, 0x4587c800    # 4345.0f

    const v6, 0x464cd800    # 13110.0f

    const v7, 0x44f3c000    # 1950.0f

    const v8, 0x45e08800    # 7185.0f

    const/high16 v9, 0x44870000    # 1080.0f

    const v10, 0x46464800    # 12690.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 1051
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 1052
    const v3, 0x43ed8000    # 475.0f

    const v4, 0x46375000    # 11732.0f

    const v5, 0x45971800    # 4835.0f

    const v6, 0x4689e400    # 17650.0f

    const/high16 v7, 0x44870000    # 1080.0f

    const v8, 0x46464800    # 12690.0f

    const v9, 0x4535e000    # 2910.0f

    const v10, 0x4689d000    # 17640.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 1053
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1054
    const v3, 0x45153000    # 2387.0f

    const v4, 0x46187400    # 9757.0f

    const v5, 0x461dec00    # 10107.0f

    const v6, 0x469e9800    # 20300.0f

    const v7, 0x4535e000    # 2910.0f

    const v8, 0x4689d000    # 17640.0f

    const v9, 0x4600ac00    # 8235.0f

    const v10, 0x4698b200    # 19545.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 1055
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1056
    const v3, 0x443f8000    # 766.0f

    const v4, 0x46417800    # 12382.0f

    const v5, 0x46613000    # 14412.0f

    const v6, 0x46a8ba00    # 21597.0f

    const v7, 0x4600ac00    # 8235.0f

    const v8, 0x4698b200    # 19545.0f

    const v9, 0x465f2000    # 14280.0f

    const v10, 0x468f3400    # 18330.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 1057
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1058
    const v3, 0x4649b800    # 12910.0f

    const v4, 0x462d2000    # 11080.0f

    const v5, 0x46920e00    # 18695.0f

    const v6, 0x46940600    # 18947.0f

    const v7, 0x465f2000    # 14280.0f

    const v8, 0x468f3400    # 18330.0f

    const v9, 0x46920400    # 18690.0f

    const v10, 0x466b1400    # 15045.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 1059
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1060
    const v3, 0x46679800    # 14822.0f

    const v4, 0x45b73000    # 5862.0f

    const v5, 0x46a8ba00    # 21597.0f

    const v6, 0x466ba800    # 15082.0f

    const v7, 0x46920400    # 18690.0f

    const v8, 0x466b1400    # 15045.0f

    const v9, 0x4502f000    # 2095.0f

    const v10, 0x45ef8800    # 7665.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 1061
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    const/high16 v6, 0x43160000    # 150.0f

    add-float/2addr v5, v6

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1062
    const v3, 0x46767000    # 15772.0f

    const/high16 v4, 0x45220000    # 2592.0f

    const v5, 0x46a4e200    # 21105.0f

    const v6, 0x461a2400    # 9865.0f

    const v7, 0x46a33e00    # 20895.0f

    const v8, 0x45ef8800    # 7665.0f

    const v9, 0x46958800    # 19140.0f

    const v10, 0x4529b000    # 2715.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 1063
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1064
    const v3, 0x465fe800    # 14330.0f

    const/4 v4, 0x0

    const v5, 0x4695e600    # 19187.0f

    const v6, 0x45ce1800    # 6595.0f

    const v7, 0x46958800    # 19140.0f

    const v8, 0x4529b000    # 2715.0f

    const v9, 0x4668f800    # 14910.0f

    const v10, 0x44924000    # 1170.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 1065
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1066
    const v3, 0x462bc000    # 10992.0f

    const/4 v4, 0x0

    const v5, 0x466ff400    # 15357.0f

    const v6, 0x45b9c800    # 5945.0f

    const v7, 0x4668f800    # 14910.0f

    const v8, 0x44924000    # 1170.0f

    const v9, 0x462fc800    # 11250.0f

    const v10, 0x44d02000    # 1665.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 1067
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1068
    const v3, 0x45d12000    # 6692.0f

    const v4, 0x44228000    # 650.0f

    const v5, 0x463be400    # 12025.0f

    const v6, 0x45f76800    # 7917.0f

    const v7, 0x462fc800    # 11250.0f

    const v8, 0x44d02000    # 1665.0f

    const v9, 0x45dae800    # 7005.0f

    const v10, 0x45214000    # 2580.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 1069
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1070
    const/high16 v3, 0x44ef0000    # 1912.0f

    const v4, 0x44f68000    # 1972.0f

    const v5, 0x46076400    # 8665.0f

    const v6, 0x462e6800    # 11162.0f

    const v7, 0x45dae800    # 7005.0f

    const v8, 0x45214000    # 2580.0f

    const v9, 0x44f3c000    # 1950.0f

    const v10, 0x45e08800    # 7185.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 1071
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1072
    invoke-virtual/range {v58 .. v58}, Landroid/graphics/Path;->close()V

    .line 1073
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_b

    .line 1074
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1075
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_c

    .line 1076
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1078
    :cond_c
    new-instance v58, Landroid/graphics/Path;

    .end local v58    # "path":Landroid/graphics/Path;
    invoke-direct/range {v58 .. v58}, Landroid/graphics/Path;-><init>()V

    .line 1079
    .restart local v58    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    const v4, 0x45dfe800    # 7165.0f

    const v5, 0x4587c800    # 4345.0f

    const v6, 0x464cd800    # 13110.0f

    const/high16 v7, 0x44870000    # 1080.0f

    const v8, 0x46464800    # 12690.0f

    const v9, 0x45124000    # 2340.0f

    const v10, 0x464c6000    # 13080.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 1080
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 1081
    const v3, 0x43ed8000    # 475.0f

    const v4, 0x46375000    # 11732.0f

    const v5, 0x45971800    # 4835.0f

    const v6, 0x4689e400    # 17650.0f

    const v7, 0x4535e000    # 2910.0f

    const v8, 0x4689d000    # 17640.0f

    const v9, 0x45589000    # 3465.0f

    const v10, 0x46884a00    # 17445.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 1082
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 1083
    const v3, 0x45ef6000    # 7660.0f

    const v4, 0x46417800    # 12382.0f

    const v5, 0x46613000    # 14412.0f

    const v6, 0x46a8ba00    # 21597.0f

    const v7, 0x45f70800    # 7905.0f

    const v8, 0x4691e600    # 18675.0f

    const v9, 0x4600ac00    # 8235.0f

    const v10, 0x4698b200    # 19545.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 1084
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 1085
    const v3, 0x45ef6000    # 7660.0f

    const v4, 0x46417800    # 12382.0f

    const v5, 0x46613000    # 14412.0f

    const v6, 0x46a8ba00    # 21597.0f

    const v7, 0x465f2000    # 14280.0f

    const v8, 0x468f3400    # 18330.0f

    const/high16 v9, 0x46610000    # 14400.0f

    const v10, 0x4687b400    # 17370.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 1086
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 1087
    const v3, 0x4649b800    # 12910.0f

    const v4, 0x462d2000    # 11080.0f

    const v5, 0x46920e00    # 18695.0f

    const v6, 0x46940600    # 18947.0f

    const v7, 0x46920400    # 18690.0f

    const v8, 0x466b1400    # 15045.0f

    const v9, 0x46855c00    # 17070.0f

    const v10, 0x46334c00    # 11475.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 1088
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 1089
    const v3, 0x46767000    # 15772.0f

    const/high16 v4, 0x45220000    # 2592.0f

    const v5, 0x45043000    # 2115.0f

    const v6, 0x461a2400    # 9865.0f

    const v7, 0x469d9e00    # 20175.0f

    const v8, 0x460cdc00    # 9015.0f

    const v9, 0x46a33e00    # 20895.0f

    const v10, 0x45ef8800    # 7665.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 1090
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 1091
    const v3, 0x465fe800    # 14330.0f

    const/4 v4, 0x0

    const v5, 0x4695e600    # 19187.0f

    const v6, 0x45ce1800    # 6595.0f

    const/high16 v7, 0x46960000    # 19200.0f

    const v8, 0x45511000    # 3345.0f

    const v9, 0x46958800    # 19140.0f

    const v10, 0x4529b000    # 2715.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 1092
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 1093
    const v3, 0x465fe800    # 14330.0f

    const/4 v4, 0x0

    const v5, 0x4695e600    # 19187.0f

    const v6, 0x45ce1800    # 6595.0f

    const v7, 0x4668f800    # 14910.0f

    const v8, 0x44924000    # 1170.0f

    const v9, 0x46635800    # 14550.0f

    const v10, 0x44f78000    # 1980.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 1094
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 1095
    const v3, 0x462bc000    # 10992.0f

    const/4 v4, 0x0

    const v5, 0x466ff400    # 15357.0f

    const v6, 0x45b9c800    # 5945.0f

    const v7, 0x462fc800    # 11250.0f

    const v8, 0x44d02000    # 1665.0f

    const v9, 0x462c8000    # 11040.0f

    const v10, 0x45124000    # 2340.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 1096
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 1097
    const/high16 v3, 0x44ef0000    # 1912.0f

    const v4, 0x44f68000    # 1972.0f

    const v5, 0x46076400    # 8665.0f

    const v6, 0x462e6800    # 11162.0f

    const v7, 0x45ef1000    # 7650.0f

    const v8, 0x454c6000    # 3270.0f

    const v9, 0x45dae800    # 7005.0f

    const v10, 0x45214000    # 2580.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 1098
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 1099
    const/high16 v3, 0x44ef0000    # 1912.0f

    const v4, 0x44f68000    # 1972.0f

    const v5, 0x46076400    # 8665.0f

    const v6, 0x462e6800    # 11162.0f

    const v7, 0x44f3c000    # 1950.0f

    const v8, 0x45e08800    # 7185.0f

    const v9, 0x45016000    # 2070.0f

    const v10, 0x45f69000    # 7890.0f

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->initArc(FFFFFFFF)V

    .line 1100
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->startA:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->sweepA:F

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 1101
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_d

    .line 1102
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1105
    :cond_d
    new-instance v58, Landroid/graphics/Path;

    .end local v58    # "path":Landroid/graphics/Path;
    invoke-direct/range {v58 .. v58}, Landroid/graphics/Path;-><init>()V

    .line 1106
    .restart local v58    # "path":Landroid/graphics/Path;
    mul-float v3, v28, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v43, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1108
    mul-float v3, v41, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v43, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    mul-float v5, v41, v56

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    mul-float v6, v29, v55

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 1112
    mul-float v3, v41, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v42, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    mul-float v5, v28, v56

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    mul-float v6, v42, v55

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 1116
    mul-float v3, v40, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v42, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    mul-float v5, v40, v56

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    mul-float v6, v29, v55

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 1120
    mul-float v3, v40, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v43, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    mul-float v5, v28, v56

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    mul-float v6, v43, v55

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 1124
    invoke-virtual/range {v58 .. v58}, Landroid/graphics/Path;->close()V

    .line 1125
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_e

    .line 1126
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1127
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_f

    .line 1128
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1131
    :cond_f
    new-instance v58, Landroid/graphics/Path;

    .end local v58    # "path":Landroid/graphics/Path;
    invoke-direct/range {v58 .. v58}, Landroid/graphics/Path;-><init>()V

    .line 1132
    .restart local v58    # "path":Landroid/graphics/Path;
    mul-float v3, v20, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v39, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1134
    mul-float v3, v37, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v39, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    mul-float v5, v37, v56

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    mul-float v6, v21, v55

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 1138
    mul-float v3, v37, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v38, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    mul-float v5, v20, v56

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    mul-float v6, v38, v55

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 1142
    mul-float v3, v36, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v38, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    mul-float v5, v36, v56

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    mul-float v6, v21, v55

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 1146
    mul-float v3, v36, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v39, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    mul-float v5, v20, v56

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    mul-float v6, v39, v55

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 1150
    invoke-virtual/range {v58 .. v58}, Landroid/graphics/Path;->close()V

    .line 1151
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_10

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_10

    .line 1152
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1153
    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_11

    .line 1154
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1157
    :cond_11
    new-instance v58, Landroid/graphics/Path;

    .end local v58    # "path":Landroid/graphics/Path;
    invoke-direct/range {v58 .. v58}, Landroid/graphics/Path;-><init>()V

    .line 1158
    .restart local v58    # "path":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v3, v3

    mul-float v3, v3, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v34, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1160
    mul-float v3, v32, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v34, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    mul-float v5, v32, v56

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v6, v6

    mul-float v6, v6, v55

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 1164
    mul-float v3, v32, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v33, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v5, v5

    mul-float v5, v5, v56

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    mul-float v6, v33, v55

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 1168
    mul-float v3, v31, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v33, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    mul-float v5, v31, v56

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v6, v6

    mul-float v6, v6, v55

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 1172
    mul-float v3, v31, v56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float v4, v34, v55

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v5, v5

    mul-float v5, v5, v56

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Width:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    mul-float v6, v34, v55

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->subFactor_Height:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    move-object/from16 v0, v58

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 1176
    invoke-virtual/range {v58 .. v58}, Landroid/graphics/Path;->close()V

    .line 1177
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_12

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_12

    .line 1178
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1179
    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_13

    .line 1180
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1184
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->textArea:Landroid/graphics/RectF;

    const v4, 0x457d2000    # 4050.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 1185
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->textArea:Landroid/graphics/RectF;

    const v4, 0x457d2000    # 4050.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 1186
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->textArea:Landroid/graphics/RectF;

    const v4, 0x46680800    # 14850.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 1187
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->textArea:Landroid/graphics/RectF;

    const v4, 0x46680800    # 14850.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    move-object v2, v11

    .line 1191
    .end local v11    # "_bitmap":Landroid/graphics/Bitmap;
    .end local v12    # "at0":F
    .end local v13    # "at1":F
    .end local v14    # "at10":F
    .end local v15    # "at11":F
    .end local v16    # "at12":F
    .end local v17    # "at13":F
    .end local v18    # "at14":F
    .end local v19    # "at15":F
    .end local v20    # "at16":F
    .end local v21    # "at17":F
    .end local v22    # "at18":F
    .end local v23    # "at19":F
    .end local v24    # "at2":F
    .end local v25    # "at20":F
    .end local v26    # "at21":F
    .end local v27    # "at22":F
    .end local v28    # "at23":F
    .end local v29    # "at24":F
    .end local v30    # "at25":F
    .end local v31    # "at26":F
    .end local v32    # "at27":F
    .end local v33    # "at28":F
    .end local v34    # "at29":F
    .end local v35    # "at3":F
    .end local v36    # "at30":F
    .end local v37    # "at31":F
    .end local v38    # "at32":F
    .end local v39    # "at33":F
    .end local v40    # "at34":F
    .end local v41    # "at35":F
    .end local v42    # "at36":F
    .end local v43    # "at37":F
    .end local v44    # "at4":F
    .end local v45    # "at5":F
    .end local v46    # "at6":F
    .end local v47    # "at7":F
    .end local v48    # "at8":F
    .end local v49    # "at9":F
    .end local v58    # "path":Landroid/graphics/Path;
    .restart local v2    # "_bitmap":Landroid/graphics/Bitmap;
    :cond_14
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mDrawOnCanvas:Z

    if-nez v3, :cond_0

    .line 1192
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 1194
    .local v7, "mtx":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->flipHorizontal:Z

    if-eqz v3, :cond_4e

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->flipVertical:Z

    if-nez v3, :cond_4e

    .line 1195
    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 1204
    :cond_15
    :goto_39
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->rotation:I

    int-to-float v3, v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    invoke-virtual {v7, v3, v4, v5}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 1207
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v59

    .line 1211
    .local v59, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v57, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->folderPath:Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "pic_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->shapeName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ".png"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v57

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 1217
    .local v57, "out":Ljava/io/FileOutputStream;
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    move-object/from16 v0, v59

    move-object/from16 v1, v57

    invoke-virtual {v0, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1218
    invoke-virtual/range {v57 .. v57}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1219
    .end local v57    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v50

    .line 1220
    .local v50, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception while writing pictures to out file"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v50

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 752
    .end local v7    # "mtx":Landroid/graphics/Matrix;
    .end local v50    # "e":Ljava/lang/Exception;
    .end local v59    # "rotatedBMP":Landroid/graphics/Bitmap;
    .restart local v12    # "at0":F
    .restart local v13    # "at1":F
    .restart local v24    # "at2":F
    .restart local v35    # "at3":F
    .restart local v44    # "at4":F
    .restart local v45    # "at5":F
    :cond_16
    const v46, 0x4644e000    # 12600.0f

    goto/16 :goto_1

    .line 753
    .restart local v46    # "at6":F
    :cond_17
    const v47, 0x468ca000    # 18000.0f

    goto/16 :goto_2

    .line 754
    .restart local v47    # "at7":F
    :cond_18
    const v48, 0x4644e000    # 12600.0f

    goto/16 :goto_3

    .line 755
    .restart local v48    # "at8":F
    :cond_19
    const v49, 0x468ca000    # 18000.0f

    goto/16 :goto_4

    .line 756
    .restart local v49    # "at9":F
    :cond_1a
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v14, v3

    goto/16 :goto_5

    .line 757
    .restart local v14    # "at10":F
    :cond_1b
    const/4 v15, 0x0

    goto/16 :goto_6

    .restart local v15    # "at11":F
    :cond_1c
    move/from16 v16, v15

    .line 758
    goto/16 :goto_7

    .line 759
    .restart local v16    # "at12":F
    :cond_1d
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v0, v3

    move/from16 v17, v0

    goto/16 :goto_8

    .restart local v17    # "at13":F
    :cond_1e
    move/from16 v18, v17

    .line 760
    goto/16 :goto_9

    .restart local v18    # "at14":F
    :cond_1f
    move/from16 v19, v18

    .line 761
    goto/16 :goto_a

    .line 762
    .restart local v19    # "at15":F
    :cond_20
    const v20, 0x46a8c000    # 21600.0f

    goto/16 :goto_b

    .restart local v20    # "at16":F
    :cond_21
    move/from16 v21, v20

    .line 763
    goto/16 :goto_c

    .restart local v21    # "at17":F
    :cond_22
    move/from16 v22, v21

    .line 764
    goto/16 :goto_d

    .restart local v22    # "at18":F
    :cond_23
    move/from16 v23, v46

    .line 765
    goto/16 :goto_e

    .restart local v23    # "at19":F
    :cond_24
    move/from16 v25, v46

    .line 766
    goto/16 :goto_f

    .restart local v25    # "at20":F
    :cond_25
    move/from16 v26, v25

    .line 767
    goto/16 :goto_10

    .line 768
    .restart local v26    # "at21":F
    :cond_26
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v0, v3

    move/from16 v27, v0

    goto/16 :goto_11

    .restart local v27    # "at22":F
    :cond_27
    move/from16 v28, v48

    .line 769
    goto/16 :goto_12

    .restart local v28    # "at23":F
    :cond_28
    move/from16 v29, v28

    .line 770
    goto/16 :goto_13

    .line 771
    .restart local v29    # "at24":F
    :cond_29
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v0, v3

    move/from16 v30, v0

    goto/16 :goto_14

    .restart local v30    # "at25":F
    :cond_2a
    move/from16 v31, v30

    .line 772
    goto/16 :goto_15

    .restart local v31    # "at26":F
    :cond_2b
    move/from16 v32, v31

    .line 773
    goto/16 :goto_16

    .restart local v32    # "at27":F
    :cond_2c
    move/from16 v33, v48

    .line 774
    goto/16 :goto_17

    .restart local v33    # "at28":F
    :cond_2d
    move/from16 v34, v33

    .line 775
    goto/16 :goto_18

    .restart local v34    # "at29":F
    :cond_2e
    move/from16 v36, v34

    .line 776
    goto/16 :goto_19

    .line 777
    .restart local v36    # "at30":F
    :cond_2f
    const/16 v37, 0x0

    goto/16 :goto_1a

    .line 778
    .restart local v37    # "at31":F
    :cond_30
    const/16 v38, 0x0

    goto/16 :goto_1b

    .restart local v38    # "at32":F
    :cond_31
    move/from16 v39, v38

    .line 779
    goto/16 :goto_1c

    .line 843
    .end local v14    # "at10":F
    .end local v15    # "at11":F
    .end local v16    # "at12":F
    .end local v17    # "at13":F
    .end local v18    # "at14":F
    .end local v19    # "at15":F
    .end local v20    # "at16":F
    .end local v21    # "at17":F
    .end local v22    # "at18":F
    .end local v23    # "at19":F
    .end local v25    # "at20":F
    .end local v26    # "at21":F
    .end local v27    # "at22":F
    .end local v28    # "at23":F
    .end local v29    # "at24":F
    .end local v30    # "at25":F
    .end local v31    # "at26":F
    .end local v32    # "at27":F
    .end local v33    # "at28":F
    .end local v34    # "at29":F
    .end local v36    # "at30":F
    .end local v37    # "at31":F
    .end local v38    # "at32":F
    .end local v46    # "at6":F
    .end local v47    # "at7":F
    .end local v48    # "at8":F
    .end local v49    # "at9":F
    :cond_32
    const v46, 0x4644e000    # 12600.0f

    goto/16 :goto_1d

    .line 844
    .restart local v46    # "at6":F
    :cond_33
    const v47, 0x468ca000    # 18000.0f

    goto/16 :goto_1e

    .line 845
    .restart local v47    # "at7":F
    :cond_34
    const v48, 0x4644e000    # 12600.0f

    goto/16 :goto_1f

    .line 846
    .restart local v48    # "at8":F
    :cond_35
    const v49, 0x468ca000    # 18000.0f

    goto/16 :goto_20

    .line 847
    .restart local v49    # "at9":F
    :cond_36
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v14, v3

    goto/16 :goto_21

    .line 848
    .restart local v14    # "at10":F
    :cond_37
    const/4 v15, 0x0

    goto/16 :goto_22

    .restart local v15    # "at11":F
    :cond_38
    move/from16 v16, v15

    .line 849
    goto/16 :goto_23

    .line 850
    .restart local v16    # "at12":F
    :cond_39
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval0:I

    int-to-float v0, v3

    move/from16 v17, v0

    goto/16 :goto_24

    .restart local v17    # "at13":F
    :cond_3a
    move/from16 v18, v17

    .line 851
    goto/16 :goto_25

    .restart local v18    # "at14":F
    :cond_3b
    move/from16 v19, v18

    .line 852
    goto/16 :goto_26

    .line 853
    .restart local v19    # "at15":F
    :cond_3c
    const v20, 0x46a8c000    # 21600.0f

    goto/16 :goto_27

    .restart local v20    # "at16":F
    :cond_3d
    move/from16 v21, v20

    .line 854
    goto/16 :goto_28

    .restart local v21    # "at17":F
    :cond_3e
    move/from16 v22, v21

    .line 855
    goto/16 :goto_29

    .restart local v22    # "at18":F
    :cond_3f
    move/from16 v23, v46

    .line 856
    goto/16 :goto_2a

    .restart local v23    # "at19":F
    :cond_40
    move/from16 v25, v46

    .line 857
    goto/16 :goto_2b

    .restart local v25    # "at20":F
    :cond_41
    move/from16 v26, v25

    .line 858
    goto/16 :goto_2c

    .line 859
    .restart local v26    # "at21":F
    :cond_42
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v0, v3

    move/from16 v27, v0

    goto/16 :goto_2d

    .restart local v27    # "at22":F
    :cond_43
    move/from16 v28, v48

    .line 860
    goto/16 :goto_2e

    .restart local v28    # "at23":F
    :cond_44
    move/from16 v29, v28

    .line 861
    goto/16 :goto_2f

    .line 862
    .restart local v29    # "at24":F
    :cond_45
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->adjval1:I

    int-to-float v0, v3

    move/from16 v30, v0

    goto/16 :goto_30

    .restart local v30    # "at25":F
    :cond_46
    move/from16 v31, v30

    .line 863
    goto/16 :goto_31

    .restart local v31    # "at26":F
    :cond_47
    move/from16 v32, v31

    .line 864
    goto/16 :goto_32

    .restart local v32    # "at27":F
    :cond_48
    move/from16 v33, v48

    .line 865
    goto/16 :goto_33

    .restart local v33    # "at28":F
    :cond_49
    move/from16 v34, v33

    .line 866
    goto/16 :goto_34

    .restart local v34    # "at29":F
    :cond_4a
    move/from16 v36, v34

    .line 867
    goto/16 :goto_35

    .line 868
    .restart local v36    # "at30":F
    :cond_4b
    const/16 v37, 0x0

    goto/16 :goto_36

    .line 869
    .restart local v37    # "at31":F
    :cond_4c
    const/16 v38, 0x0

    goto/16 :goto_37

    .restart local v38    # "at32":F
    :cond_4d
    move/from16 v39, v38

    .line 870
    goto/16 :goto_38

    .line 1197
    .end local v12    # "at0":F
    .end local v13    # "at1":F
    .end local v14    # "at10":F
    .end local v15    # "at11":F
    .end local v16    # "at12":F
    .end local v17    # "at13":F
    .end local v18    # "at14":F
    .end local v19    # "at15":F
    .end local v20    # "at16":F
    .end local v21    # "at17":F
    .end local v22    # "at18":F
    .end local v23    # "at19":F
    .end local v24    # "at2":F
    .end local v25    # "at20":F
    .end local v26    # "at21":F
    .end local v27    # "at22":F
    .end local v28    # "at23":F
    .end local v29    # "at24":F
    .end local v30    # "at25":F
    .end local v31    # "at26":F
    .end local v32    # "at27":F
    .end local v33    # "at28":F
    .end local v34    # "at29":F
    .end local v35    # "at3":F
    .end local v36    # "at30":F
    .end local v37    # "at31":F
    .end local v38    # "at32":F
    .end local v44    # "at4":F
    .end local v45    # "at5":F
    .end local v46    # "at6":F
    .end local v47    # "at7":F
    .end local v48    # "at8":F
    .end local v49    # "at9":F
    .restart local v7    # "mtx":Landroid/graphics/Matrix;
    :cond_4e
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->flipVertical:Z

    if-eqz v3, :cond_4f

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->flipHorizontal:Z

    if-nez v3, :cond_4f

    .line 1198
    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, -0x40800000    # -1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    goto/16 :goto_39

    .line 1200
    :cond_4f
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->flipHorizontal:Z

    if-eqz v3, :cond_15

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->flipVertical:Z

    if-eqz v3, :cond_15

    .line 1201
    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v4, -0x40800000    # -1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    goto/16 :goto_39
.end method

.method public drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F

    .prologue
    .line 121
    invoke-super {p0, p3, p4, p7, p8}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IFF)V

    .line 122
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mDrawOnCanvas:Z

    .line 123
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mLeftMarg:I

    .line 124
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mTopMarg:I

    .line 125
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 126
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->draw(Landroid/graphics/Canvas;I)V

    .line 127
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 128
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mDrawOnCanvas:Z

    .line 129
    return-void
.end method

.method public drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p2, "shapeCount"    # I

    .prologue
    .line 112
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V

    .line 114
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->draw(I)V

    .line 115
    return-void
.end method

.method public drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 98
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;I)V

    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mDrawOnCanvas:Z

    .line 100
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mLeftMarg:I

    .line 101
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mTopMarg:I

    .line 102
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->width:F

    .line 103
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->height:F

    .line 104
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 105
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->draw(Landroid/graphics/Canvas;I)V

    .line 106
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 107
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mDrawOnCanvas:Z

    .line 108
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V
    .locals 0
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 49
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 51
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->draw(I)V

    .line 52
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V
    .locals 2
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I

    .prologue
    .line 56
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mDrawOnCanvas:Z

    .line 58
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mLeftMarg:I

    .line 59
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mTopMarg:I

    .line 60
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 61
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->draw(Landroid/graphics/Canvas;I)V

    .line 62
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mDrawOnCanvas:Z

    .line 64
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 142
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 143
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mDrawOnCanvas:Z

    .line 144
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mLeftMarg:I

    .line 145
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mTopMarg:I

    .line 146
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 147
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->draw(Landroid/graphics/Canvas;I)V

    .line 148
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 149
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mDrawOnCanvas:Z

    .line 150
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 154
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V

    .line 155
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mDrawOnCanvas:Z

    .line 156
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mLeftMarg:I

    .line 157
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mTopMarg:I

    .line 158
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 159
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->draw(Landroid/graphics/Canvas;I)V

    .line 160
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 161
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mDrawOnCanvas:Z

    .line 162
    return-void
.end method

.method public drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .prologue
    .line 136
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 137
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->draw(I)V

    .line 138
    return-void
.end method

.method public drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 69
    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p7

    move v4, p8

    move-object v5, p9

    invoke-super/range {v0 .. v5}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSSExcelShape(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mDrawOnCanvas:Z

    .line 72
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mLeftMarg:I

    .line 73
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mTopMarg:I

    .line 74
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 75
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->draw(Landroid/graphics/Canvas;I)V

    .line 76
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 77
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mDrawOnCanvas:Z

    .line 78
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 82
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mDrawOnCanvas:Z

    .line 84
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mLeftMarg:I

    .line 85
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mTopMarg:I

    .line 86
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->width:F

    .line 87
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->height:F

    .line 88
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 89
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->draw(Landroid/graphics/Canvas;I)V

    .line 90
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 91
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->mDrawOnCanvas:Z

    .line 92
    return-void
.end method
