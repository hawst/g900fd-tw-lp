.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLChoiceHandler;
.source "TblStylesLineHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ILineChoiceConsumer;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler$ISpprLineConsumer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler$IBorderConsumer;
    }
.end annotation


# instance fields
.field private brdrConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler$IBorderConsumer;

.field private lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

.field private lineRefHandler:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler;

.field private lineToHandler:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler$IBorderConsumer;)V
    .locals 5
    .param p1, "elementName"    # Ljava/lang/String;
    .param p2, "brdrConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler$IBorderConsumer;

    .prologue
    const/16 v4, 0x1f

    .line 32
    invoke-direct {p0, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLChoiceHandler;-><init>(ILjava/lang/String;)V

    .line 33
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;->brdrConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler$IBorderConsumer;

    .line 35
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;

    const/4 v2, 0x0

    const-string/jumbo v3, "ln"

    invoke-direct {v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;->lineToHandler:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;

    .line 36
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;->lineToHandler:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;

    invoke-virtual {v1, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->setConsumer(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler$ISpprLineConsumer;)V

    .line 37
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "ln"

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;->lineToHandler:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    const/4 v0, 0x0

    .line 40
    .local v0, "shapeInfo":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler;

    invoke-direct {v1, p0, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ILineChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;->lineRefHandler:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler;

    .line 41
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "lnRef"

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;->lineRefHandler:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;->choiceHandlerMap:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;->handlerMap:Ljava/util/HashMap;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    return-void
.end method


# virtual methods
.method public consumeLine(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V
    .locals 0
    .param p1, "lineProps"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .line 109
    return-void
.end method

.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 5
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLChoiceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 61
    const-string/jumbo v4, "left"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 62
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;-><init>()V

    .line 63
    .local v1, "linePropsLeft":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .line 64
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;->brdrConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler$IBorderConsumer;

    invoke-interface {v4, p2, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler$IBorderConsumer;->borderCosumer(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V

    .line 65
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;

    invoke-virtual {v4, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->addLinePropsList(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V

    .line 67
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;

    invoke-virtual {v4, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->consumeLine(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V

    .line 99
    .end local v1    # "linePropsLeft":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    const-string/jumbo v4, "top"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 70
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    invoke-direct {v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;-><init>()V

    .line 71
    .local v3, "linePropsTop":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .line 72
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;->brdrConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler$IBorderConsumer;

    invoke-interface {v4, p2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler$IBorderConsumer;->borderCosumer(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V

    .line 73
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;

    invoke-virtual {v4, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->addLinePropsList(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V

    .line 75
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;

    invoke-virtual {v4, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->consumeLine(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V

    goto :goto_0

    .line 77
    .end local v3    # "linePropsTop":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    :cond_2
    const-string/jumbo v4, "right"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 78
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;-><init>()V

    .line 79
    .local v2, "linePropsRight":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .line 80
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;->brdrConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler$IBorderConsumer;

    invoke-interface {v4, p2, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler$IBorderConsumer;->borderCosumer(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V

    .line 81
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;

    invoke-virtual {v4, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->addLinePropsList(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V

    .line 83
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;

    invoke-virtual {v4, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->consumeLine(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V

    goto :goto_0

    .line 85
    .end local v2    # "linePropsRight":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    :cond_3
    const-string/jumbo v4, "bottom"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 86
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;-><init>()V

    .line 87
    .local v0, "linePropsBottom":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .line 88
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;->brdrConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler$IBorderConsumer;

    invoke-interface {v4, p2, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler$IBorderConsumer;->borderCosumer(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V

    .line 89
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;

    invoke-virtual {v4, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->addLinePropsList(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V

    .line 91
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;

    invoke-virtual {v4, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->consumeLine(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V

    goto :goto_0
.end method

.method public setConsumer(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler$ISpprLineConsumer;)V
    .locals 1
    .param p1, "styLineConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler$ISpprLineConsumer;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;->lineToHandler:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler;->setConsumer(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/SpprLineHandler$ISpprLineConsumer;)V

    .line 49
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 54
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLChoiceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 56
    return-void
.end method

.method public tblStylesLineConsumer(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "lnProps"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .prologue
    .line 103
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .line 104
    return-void
.end method
