.class public Lcom/samsung/thumbnail/office/ooxml/diagram/OOXMLArrayHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "OOXMLArrayHandler.java"


# instance fields
.field private eleHandler:Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;


# direct methods
.method public constructor <init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V
    .locals 0
    .param p1, "nsID"    # I
    .param p2, "elementName"    # Ljava/lang/String;
    .param p3, "eleHandler"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 26
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/OOXMLArrayHandler;->eleHandler:Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    .line 27
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V
    .locals 0
    .param p1, "elementName"    # Ljava/lang/String;
    .param p2, "eleHandler"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 20
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/OOXMLArrayHandler;->eleHandler:Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    .line 21
    return-void
.end method


# virtual methods
.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/OOXMLArrayHandler;->eleHandler:Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    invoke-virtual {v0, p2, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->checkElement(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/OOXMLArrayHandler;->eleHandler:Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 39
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/OOXMLArrayHandler;->eleHandler:Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    invoke-virtual {p1, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->pushHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 41
    :cond_0
    return-void
.end method
