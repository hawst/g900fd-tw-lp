.class public Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;
.super Ljava/lang/Object;
.source "XSSFFormatIndex.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex$ConstantStringFormat;
    }
.end annotation


# instance fields
.field private _builtinFormats:[Ljava/lang/String;

.field private final amPmPattern:Ljava/util/regex/Pattern;

.field private final colorPattern:Ljava/util/regex/Pattern;

.field private dateSymbols:Ljava/text/DateFormatSymbols;

.field private final daysAsText:Ljava/util/regex/Pattern;

.field private decimalSymbols:Ljava/text/DecimalFormatSymbols;

.field private emulateCsv:Z

.field private formats:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/text/Format;",
            ">;"
        }
    .end annotation
.end field

.field private formatstring:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private generalDecimalNumFormat:Ljava/text/Format;

.field private generalWholeNumFormat:Ljava/text/Format;

.field private final numPattern:Ljava/util/regex/Pattern;

.field private final specialPatternGroup:Ljava/util/regex/Pattern;

.field private str:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->formatstring:Ljava/util/Map;

    .line 33
    invoke-static {}, Lorg/apache/poi/ss/usermodel/BuiltinFormats;->getAll()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->_builtinFormats:[Ljava/lang/String;

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->emulateCsv:Z

    .line 35
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->str:Ljava/lang/String;

    .line 43
    const-string/jumbo v0, "(\\[\\$[^-\\]]*-[0-9A-Z]+\\])"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->specialPatternGroup:Ljava/util/regex/Pattern;

    .line 45
    const-string/jumbo v0, "[0#]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->numPattern:Ljava/util/regex/Pattern;

    .line 46
    const-string/jumbo v0, "((A|P)[M/P]*)"

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->amPmPattern:Ljava/util/regex/Pattern;

    .line 48
    const-string/jumbo v0, "([d]{3,})"

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->daysAsText:Ljava/util/regex/Pattern;

    .line 186
    const-string/jumbo v0, "(\\[BLACK\\])|(\\[BLUE\\])|(\\[CYAN\\])|(\\[GREEN\\])|(\\[MAGENTA\\])|(\\[RED\\])|(\\[WHITE\\])|(\\[YELLOW\\])|(\\[COLOR\\s*\\d\\])|(\\[COLOR\\s*[0-5]\\d\\])"

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->colorPattern:Ljava/util/regex/Pattern;

    .line 335
    return-void
.end method

.method private cleanFormatForNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "formatStr"    # Ljava/lang/String;

    .prologue
    const/16 v8, 0x3f

    const/16 v7, 0x2a

    const/16 v6, 0x20

    const/16 v4, 0x5f

    const/16 v5, 0x5c

    .line 261
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 263
    .local v2, "sb":Ljava/lang/StringBuffer;
    iget-boolean v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->emulateCsv:Z

    if-eqz v3, :cond_5

    .line 268
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-ge v1, v3, :cond_a

    .line 269
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v0

    .line 270
    .local v0, "c":C
    if-eq v0, v4, :cond_0

    if-eq v0, v7, :cond_0

    if-ne v0, v8, :cond_1

    .line 271
    :cond_0
    if-lez v1, :cond_2

    add-int/lit8 v3, v1, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v3

    if-ne v3, v5, :cond_2

    .line 268
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 275
    :cond_2
    if-ne v0, v8, :cond_3

    .line 276
    invoke-virtual {v2, v1, v6}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    goto :goto_1

    .line 277
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_1

    .line 281
    if-ne v0, v4, :cond_4

    .line 282
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v2, v3, v6}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 287
    :goto_2
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 284
    :cond_4
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 296
    .end local v0    # "c":C
    .end local v1    # "i":I
    :cond_5
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-ge v1, v3, :cond_a

    .line 297
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v0

    .line 298
    .restart local v0    # "c":C
    if-eq v0, v4, :cond_6

    if-ne v0, v7, :cond_7

    .line 299
    :cond_6
    if-lez v1, :cond_8

    add-int/lit8 v3, v1, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v3

    if-ne v3, v5, :cond_8

    .line 296
    :cond_7
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 303
    :cond_8
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_9

    .line 307
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 310
    :cond_9
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    goto :goto_4

    .line 317
    .end local v0    # "c":C
    :cond_a
    const/4 v1, 0x0

    :goto_5
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-ge v1, v3, :cond_e

    .line 318
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v0

    .line 320
    .restart local v0    # "c":C
    if-eq v0, v5, :cond_b

    const/16 v3, 0x22

    if-ne v0, v3, :cond_d

    .line 321
    :cond_b
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 322
    add-int/lit8 v1, v1, -0x1

    .line 317
    :cond_c
    :goto_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 325
    :cond_d
    const/16 v3, 0x2b

    if-ne v0, v3, :cond_c

    if-lez v1, :cond_c

    add-int/lit8 v3, v1, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v3

    const/16 v4, 0x45

    if-ne v3, v4, :cond_c

    .line 326
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 327
    add-int/lit8 v1, v1, -0x1

    goto :goto_6

    .line 331
    .end local v0    # "c":C
    :cond_e
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private createDateFormat(Ljava/lang/String;D)Ljava/text/Format;
    .locals 22
    .param p1, "pFormatStr"    # Ljava/lang/String;
    .param p2, "cellValue"    # D

    .prologue
    .line 403
    move-object/from16 v7, p1

    .line 404
    .local v7, "formatStr":Ljava/lang/String;
    const-string/jumbo v18, "\\\\-"

    const-string/jumbo v19, "-"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 405
    const-string/jumbo v18, "\\\\,"

    const-string/jumbo v19, ","

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 406
    const-string/jumbo v18, "\\\\ "

    const-string/jumbo v19, " "

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 407
    const-string/jumbo v18, "\\\\/"

    const-string/jumbo v19, "/"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 408
    const-string/jumbo v18, ";@"

    const-string/jumbo v19, ""

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 409
    const-string/jumbo v18, "\"/\""

    const-string/jumbo v19, "/"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 413
    const/4 v8, 0x0

    .line 414
    .local v8, "hasAmPm":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->amPmPattern:Ljava/util/regex/Pattern;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 415
    .local v3, "amPmMatcher":Ljava/util/regex/Matcher;
    :goto_0
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v18

    if-eqz v18, :cond_0

    .line 416
    const-string/jumbo v18, "@"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 417
    const/4 v8, 0x1

    .line 418
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->amPmPattern:Ljava/util/regex/Pattern;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    goto :goto_0

    .line 420
    :cond_0
    const-string/jumbo v18, "@"

    const-string/jumbo v19, "a"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 422
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->daysAsText:Ljava/util/regex/Pattern;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    .line 423
    .local v6, "dateMatcher":Ljava/util/regex/Matcher;
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->find()Z

    move-result v18

    if-eqz v18, :cond_1

    .line 424
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v15

    .line 425
    .local v15, "match":Ljava/lang/String;
    sget-object v18, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v18

    const-string/jumbo v19, "D"

    const-string/jumbo v20, "E"

    invoke-virtual/range {v18 .. v20}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 438
    .end local v15    # "match":Ljava/lang/String;
    :cond_1
    new-instance v17, Ljava/lang/StringBuffer;

    const/16 v18, 0x100

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 440
    .local v17, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {v7}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    .line 441
    .local v5, "chars":[C
    const/4 v14, 0x1

    .line 442
    .local v14, "mIsMonth":Z
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 443
    .local v16, "ms":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v12, 0x0

    .line 444
    .local v12, "isElapsed":Z
    const/4 v13, 0x0

    .local v13, "j":I
    :goto_1
    array-length v0, v5

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v13, v0, :cond_1a

    .line 445
    aget-char v4, v5, v13

    .line 446
    .local v4, "c":C
    const/16 v18, 0x5b

    move/from16 v0, v18

    if-ne v4, v0, :cond_2

    if-nez v12, :cond_2

    .line 447
    const/4 v12, 0x1

    .line 448
    const/4 v14, 0x0

    .line 449
    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 444
    :goto_2
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 450
    :cond_2
    const/16 v18, 0x5d

    move/from16 v0, v18

    if-ne v4, v0, :cond_3

    if-eqz v12, :cond_3

    .line 451
    const/4 v12, 0x0

    .line 452
    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 453
    :cond_3
    if-eqz v12, :cond_a

    .line 454
    const/16 v18, 0x68

    move/from16 v0, v18

    if-eq v4, v0, :cond_4

    const/16 v18, 0x48

    move/from16 v0, v18

    if-ne v4, v0, :cond_5

    .line 455
    :cond_4
    const/16 v18, 0x48

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 456
    :cond_5
    const/16 v18, 0x6d

    move/from16 v0, v18

    if-eq v4, v0, :cond_6

    const/16 v18, 0x4d

    move/from16 v0, v18

    if-ne v4, v0, :cond_7

    .line 457
    :cond_6
    const/16 v18, 0x6d

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 458
    :cond_7
    const/16 v18, 0x73

    move/from16 v0, v18

    if-eq v4, v0, :cond_8

    const/16 v18, 0x53

    move/from16 v0, v18

    if-ne v4, v0, :cond_9

    .line 459
    :cond_8
    const/16 v18, 0x73

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 461
    :cond_9
    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 463
    :cond_a
    const/16 v18, 0x68

    move/from16 v0, v18

    if-eq v4, v0, :cond_b

    const/16 v18, 0x48

    move/from16 v0, v18

    if-ne v4, v0, :cond_d

    .line 464
    :cond_b
    const/4 v14, 0x0

    .line 465
    if-eqz v8, :cond_c

    .line 466
    const/16 v18, 0x68

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 468
    :cond_c
    const/16 v18, 0x48

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 470
    :cond_d
    const/16 v18, 0x6d

    move/from16 v0, v18

    if-eq v4, v0, :cond_e

    const/16 v18, 0x4d

    move/from16 v0, v18

    if-ne v4, v0, :cond_10

    .line 471
    :cond_e
    if-eqz v14, :cond_f

    .line 472
    const/16 v18, 0x4d

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 473
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuffer;->length()I

    move-result v18

    add-int/lit8 v18, v18, -0x1

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 475
    :cond_f
    const/16 v18, 0x6d

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 477
    :cond_10
    const/16 v18, 0x73

    move/from16 v0, v18

    if-eq v4, v0, :cond_11

    const/16 v18, 0x53

    move/from16 v0, v18

    if-ne v4, v0, :cond_14

    .line 478
    :cond_11
    const/16 v18, 0x73

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 480
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_3
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v9, v0, :cond_13

    .line 481
    move-object/from16 v0, v16

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Integer;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 482
    .local v11, "index":I
    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v18

    const/16 v19, 0x4d

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_12

    .line 483
    add-int/lit8 v18, v11, 0x1

    const-string/jumbo v19, "m"

    move-object/from16 v0, v17

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v11, v1, v2}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 480
    :cond_12
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 486
    .end local v11    # "index":I
    :cond_13
    const/4 v14, 0x1

    .line 487
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->clear()V

    goto/16 :goto_2

    .line 488
    .end local v9    # "i":I
    :cond_14
    invoke-static {v4}, Ljava/lang/Character;->isLetter(C)Z

    move-result v18

    if-eqz v18, :cond_19

    .line 489
    const/4 v14, 0x1

    .line 490
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->clear()V

    .line 491
    const/16 v18, 0x79

    move/from16 v0, v18

    if-eq v4, v0, :cond_15

    const/16 v18, 0x59

    move/from16 v0, v18

    if-ne v4, v0, :cond_16

    .line 492
    :cond_15
    const/16 v18, 0x79

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 493
    :cond_16
    const/16 v18, 0x64

    move/from16 v0, v18

    if-eq v4, v0, :cond_17

    const/16 v18, 0x44

    move/from16 v0, v18

    if-ne v4, v0, :cond_18

    .line 494
    :cond_17
    const/16 v18, 0x64

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 496
    :cond_18
    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 499
    :cond_19
    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 502
    .end local v4    # "c":C
    :cond_1a
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    .line 505
    :try_start_0
    new-instance v18, Lorg/apache/poi/ss/usermodel/ExcelStyleDateFormatter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->dateSymbols:Ljava/text/DateFormatSymbols;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v0, v7, v1}, Lorg/apache/poi/ss/usermodel/ExcelStyleDateFormatter;-><init>(Ljava/lang/String;Ljava/text/DateFormatSymbols;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 510
    :goto_4
    return-object v18

    .line 506
    :catch_0
    move-exception v10

    .line 510
    .local v10, "iae":Ljava/lang/IllegalArgumentException;
    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->getDefaultFormat(D)Ljava/text/Format;

    move-result-object v18

    goto :goto_4
.end method

.method private createFormat(DILjava/lang/String;)Ljava/text/Format;
    .locals 11
    .param p1, "cellValue"    # D
    .param p3, "formatIndex"    # I
    .param p4, "sFormat"    # Ljava/lang/String;

    .prologue
    .line 195
    move-object v3, p4

    .line 199
    .local v3, "formatStr":Ljava/lang/String;
    if-eqz v3, :cond_3

    .line 200
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->colorPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v9, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 201
    .local v2, "colourM":Ljava/util/regex/Matcher;
    :goto_0
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 202
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v1

    .line 205
    .local v1, "colour":Ljava/lang/String;
    invoke-virtual {v3, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 206
    .local v0, "at":I
    const/4 v9, -0x1

    if-ne v0, v9, :cond_2

    .line 219
    .end local v0    # "at":I
    .end local v1    # "colour":Ljava/lang/String;
    :cond_0
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->specialPatternGroup:Ljava/util/regex/Pattern;

    invoke-virtual {v9, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 221
    .local v4, "m":Ljava/util/regex/Matcher;
    new-instance v7, Ljava/lang/StringBuffer;

    const/16 v9, 0x100

    invoke-direct {v7, v9}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 224
    .local v7, "sb":Ljava/lang/StringBuffer;
    :goto_1
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 225
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v5

    .line 226
    .local v5, "match":Ljava/lang/String;
    const/16 v9, 0x24

    invoke-virtual {v5, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    const/16 v10, 0x2d

    invoke-virtual {v5, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v10

    invoke-virtual {v5, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 228
    .local v8, "symbol":Ljava/lang/String;
    const/16 v9, 0x24

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    const/4 v10, -0x1

    if-le v9, v10, :cond_1

    .line 229
    const/4 v9, 0x0

    const/16 v10, 0x24

    invoke-virtual {v8, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v10

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 230
    const/16 v9, 0x5c

    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 231
    const/16 v9, 0x24

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 233
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    .line 234
    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 236
    :cond_1
    invoke-virtual {v4, v8}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 237
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->specialPatternGroup:Ljava/util/regex/Pattern;

    invoke-virtual {v9, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 238
    goto :goto_1

    .line 208
    .end local v4    # "m":Ljava/util/regex/Matcher;
    .end local v5    # "match":Ljava/lang/String;
    .end local v7    # "sb":Ljava/lang/StringBuffer;
    .end local v8    # "symbol":Ljava/lang/String;
    .restart local v0    # "at":I
    .restart local v1    # "colour":Ljava/lang/String;
    :cond_2
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v10, 0x0

    invoke-virtual {v3, v10, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v10, v0

    invoke-virtual {v3, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 210
    .local v6, "nFormatStr":Ljava/lang/String;
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 214
    move-object v3, v6

    .line 215
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->colorPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v9, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 216
    goto/16 :goto_0

    .line 241
    .end local v0    # "at":I
    .end local v1    # "colour":Ljava/lang/String;
    .end local v2    # "colourM":Ljava/util/regex/Matcher;
    .end local v6    # "nFormatStr":Ljava/lang/String;
    :cond_3
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_5

    .line 242
    :cond_4
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->getDefaultFormat(D)Ljava/text/Format;

    move-result-object v9

    .line 257
    :goto_2
    return-object v9

    .line 245
    :cond_5
    invoke-static {p3, v3}, Lorg/apache/poi/ss/usermodel/DateUtil;->isADateFormat(ILjava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-static {p1, p2}, Lorg/apache/poi/ss/usermodel/DateUtil;->isValidExcelDate(D)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 247
    invoke-direct {p0, v3, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->createDateFormat(Ljava/lang/String;D)Ljava/text/Format;

    move-result-object v9

    goto :goto_2

    .line 249
    :cond_6
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->numPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v9, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/regex/Matcher;->find()Z

    move-result v9

    if-eqz v9, :cond_7

    .line 250
    invoke-direct {p0, v3, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->createNumberFormat(Ljava/lang/String;D)Ljava/text/Format;

    move-result-object v9

    goto :goto_2

    .line 253
    :cond_7
    iget-boolean v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->emulateCsv:Z

    if-eqz v9, :cond_8

    .line 254
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex$ConstantStringFormat;

    invoke-direct {p0, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->cleanFormatForNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex$ConstantStringFormat;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 257
    :cond_8
    const/4 v9, 0x0

    goto :goto_2
.end method

.method static createIntegerOnlyFormat(Ljava/lang/String;)Ljava/text/DecimalFormat;
    .locals 2
    .param p0, "fmt"    # Ljava/lang/String;

    .prologue
    .line 357
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-direct {v0, p0}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 358
    .local v0, "result":Ljava/text/DecimalFormat;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->setParseIntegerOnly(Z)V

    .line 359
    return-object v0
.end method

.method private createNumberFormat(Ljava/lang/String;D)Ljava/text/Format;
    .locals 4
    .param p1, "formatStr"    # Ljava/lang/String;
    .param p2, "cellValue"    # D

    .prologue
    .line 363
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->cleanFormatForNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 366
    .local v1, "format":Ljava/lang/String;
    :try_start_0
    new-instance v0, Ljava/text/DecimalFormat;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->decimalSymbols:Ljava/text/DecimalFormatSymbols;

    invoke-direct {v0, v1, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 367
    .local v0, "df":Ljava/text/DecimalFormat;
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->setExcelStyleRoundingMode(Ljava/text/DecimalFormat;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 373
    .end local v0    # "df":Ljava/text/DecimalFormat;
    :goto_0
    return-object v0

    .line 369
    :catch_0
    move-exception v2

    .line 373
    .local v2, "iae":Ljava/lang/IllegalArgumentException;
    invoke-direct {p0, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->getDefaultFormat(D)Ljava/text/Format;

    move-result-object v0

    goto :goto_0
.end method

.method private getDefaultFormat(D)Ljava/text/Format;
    .locals 1
    .param p1, "cellValue"    # D

    .prologue
    .line 523
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->isWholeNumber(D)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 524
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->generalWholeNumFormat:Ljava/text/Format;

    .line 526
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->generalDecimalNumFormat:Ljava/text/Format;

    goto :goto_0
.end method

.method private isWholeNumber(D)Z
    .locals 3
    .param p1, "d"    # D

    .prologue
    .line 537
    invoke-static {p1, p2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public fillformatindex()V
    .locals 3

    .prologue
    .line 62
    const-string/jumbo v0, "\"$\"#,##0_);\\(\"$\"#,##0\\)"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->str:Ljava/lang/String;

    .line 63
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->formatstring:Ljava/util/Map;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->str:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    const-string/jumbo v0, "\"$\"#,##0_);[Red]\\(\"$\"#,##0\\)"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->str:Ljava/lang/String;

    .line 66
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->formatstring:Ljava/util/Map;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->str:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    const-string/jumbo v0, "\"$\"#,##0.00_);\\(\"$\"#,##0.00\\)"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->str:Ljava/lang/String;

    .line 69
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->formatstring:Ljava/util/Map;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->str:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    const-string/jumbo v0, "\"$\"#,##0.00_);[Red]\\(\"$\"#,##0.00\\)"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->str:Ljava/lang/String;

    .line 72
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->formatstring:Ljava/util/Map;

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->str:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    const-string/jumbo v0, "_(\"$\"* #,##0_);_(\"$\"* \\(#,##0\\);_(\"$\"* \"-\"_);_(@_)"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->str:Ljava/lang/String;

    .line 75
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->formatstring:Ljava/util/Map;

    const/16 v1, 0x2a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->str:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    const-string/jumbo v0, "_(* #,##0_);_(* \\(#,##0\\);_(* \"-\"_);_(@_)"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->str:Ljava/lang/String;

    .line 78
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->formatstring:Ljava/util/Map;

    const/16 v1, 0x29

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->str:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    const-string/jumbo v0, "_(\"$\"* #,##0.00_);_(\"$\"* \\(#,##0.00\\);_(\"$\"* \"-\"??_);_(@_)"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->str:Ljava/lang/String;

    .line 81
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->formatstring:Ljava/util/Map;

    const/16 v1, 0x2c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->str:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    const-string/jumbo v0, "_(* #,##0.00_);_(* \\(#,##0.00\\);_(* \"-\"??_);_(@_)"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->str:Ljava/lang/String;

    .line 84
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->formatstring:Ljava/util/Map;

    const/16 v1, 0x2b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->str:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    const-string/jumbo v0, "[$-F400]h:mm:ss\\ AM/PM"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->str:Ljava/lang/String;

    .line 87
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->formatstring:Ljava/util/Map;

    const/16 v1, 0xa4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->str:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    const-string/jumbo v0, "[$-409]h:mm:ss\\ AM/PM"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->str:Ljava/lang/String;

    .line 90
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->formatstring:Ljava/util/Map;

    const/16 v1, 0xa5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->str:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    return-void
.end method

.method public getFormat(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "index"    # Ljava/lang/String;

    .prologue
    .line 109
    invoke-static {p1}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    move-result v1

    .line 110
    .local v1, "ind":S
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->fillformatindex()V

    .line 111
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->getformatindex(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 113
    .local v0, "fmt":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->_builtinFormats:[Ljava/lang/String;

    array-length v2, v2

    if-le v2, v1, :cond_0

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->_builtinFormats:[Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aget-object v2, v2, v3

    if-eqz v2, :cond_0

    .line 115
    if-eqz v0, :cond_1

    .line 122
    .end local v0    # "fmt":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 118
    .restart local v0    # "fmt":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->_builtinFormats:[Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aget-object v0, v2, v3

    goto :goto_0
.end method

.method public getFormat(DILjava/lang/String;)Ljava/text/Format;
    .locals 11
    .param p1, "cellValue"    # D
    .param p3, "formatIndex"    # I
    .param p4, "formatStrIn"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    const/16 v8, 0x3b

    const-wide/16 v6, 0x0

    .line 138
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->initializevariables()V

    .line 139
    move-object v2, p4

    .line 142
    .local v2, "formatStr":Ljava/lang/String;
    invoke-virtual {v2, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 143
    .local v0, "firstAt":I
    invoke-virtual {v2, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 145
    .local v3, "lastAt":I
    const/4 v5, -0x1

    if-eq v0, v5, :cond_0

    if-eq v0, v3, :cond_0

    .line 146
    add-int/lit8 v5, v0, 0x1

    invoke-virtual {v2, v8, v5}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    .line 147
    .local v4, "secondAt":I
    if-ne v4, v3, :cond_5

    .line 148
    cmpl-double v5, p1, v6

    if-nez v5, :cond_4

    .line 149
    add-int/lit8 v5, v3, 0x1

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 164
    .end local v4    # "secondAt":I
    :cond_0
    :goto_0
    cmpl-double v5, p1, v6

    if-nez v5, :cond_1

    const-string/jumbo v5, "#"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string/jumbo v5, "0"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 166
    const-string/jumbo v5, "#"

    const-string/jumbo v6, ""

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 169
    :cond_1
    const-string/jumbo v5, "General"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string/jumbo v5, "@"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 170
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->isWholeNumber(D)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 171
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->generalWholeNumFormat:Ljava/text/Format;

    .line 178
    :cond_3
    :goto_1
    return-object v1

    .line 151
    .restart local v4    # "secondAt":I
    :cond_4
    invoke-virtual {v2, v9, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 154
    :cond_5
    cmpl-double v5, p1, v6

    if-nez v5, :cond_6

    .line 155
    add-int/lit8 v5, v4, 0x1

    invoke-virtual {v2, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 157
    :cond_6
    invoke-virtual {v2, v9, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 173
    .end local v4    # "secondAt":I
    :cond_7
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->generalDecimalNumFormat:Ljava/text/Format;

    goto :goto_1

    .line 175
    :cond_8
    invoke-direct {p0, p1, p2, p3, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->createFormat(DILjava/lang/String;)Ljava/text/Format;

    move-result-object v1

    .line 176
    .local v1, "format":Ljava/text/Format;
    if-eqz v1, :cond_3

    .line 177
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->formats:Ljava/util/Map;

    invoke-interface {v5, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public getformatindex(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 95
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->formatstring:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 96
    .local v0, "formatstr":Ljava/lang/String;
    return-object v0
.end method

.method public initializevariables()V
    .locals 4

    .prologue
    .line 127
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 128
    .local v0, "locale":Ljava/util/Locale;
    new-instance v1, Ljava/text/DateFormatSymbols;

    invoke-direct {v1, v0}, Ljava/text/DateFormatSymbols;-><init>(Ljava/util/Locale;)V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->dateSymbols:Ljava/text/DateFormatSymbols;

    .line 129
    new-instance v1, Ljava/text/DecimalFormatSymbols;

    invoke-direct {v1, v0}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->decimalSymbols:Ljava/text/DecimalFormatSymbols;

    .line 130
    new-instance v1, Ljava/text/DecimalFormat;

    const-string/jumbo v2, "#"

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->decimalSymbols:Ljava/text/DecimalFormatSymbols;

    invoke-direct {v1, v2, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->generalWholeNumFormat:Ljava/text/Format;

    .line 131
    new-instance v1, Ljava/text/DecimalFormat;

    const-string/jumbo v2, "#.##########"

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->decimalSymbols:Ljava/text/DecimalFormatSymbols;

    invoke-direct {v1, v2, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->generalDecimalNumFormat:Ljava/text/Format;

    .line 133
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->formats:Ljava/util/Map;

    .line 134
    return-void
.end method

.method public performDateFormatting(Ljava/util/Date;Ljava/text/Format;)Ljava/lang/String;
    .locals 1
    .param p1, "d"    # Ljava/util/Date;
    .param p2, "dateFormat"    # Ljava/text/Format;

    .prologue
    .line 55
    if-eqz p2, :cond_0

    .line 56
    invoke-virtual {p2, p1}, Ljava/text/Format;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 58
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public setExcelStyleRoundingMode(Ljava/text/DecimalFormat;)V
    .locals 1
    .param p1, "format"    # Ljava/text/DecimalFormat;

    .prologue
    .line 378
    sget-object v0, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {p0, p1, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->setExcelStyleRoundingMode(Ljava/text/DecimalFormat;Ljava/math/RoundingMode;)V

    .line 379
    return-void
.end method

.method public setExcelStyleRoundingMode(Ljava/text/DecimalFormat;Ljava/math/RoundingMode;)V
    .locals 10
    .param p1, "format"    # Ljava/text/DecimalFormat;
    .param p2, "roundingMode"    # Ljava/math/RoundingMode;

    .prologue
    .line 384
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string/jumbo v6, "setRoundingMode"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Ljava/math/RoundingMode;

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 386
    .local v4, "srm":Ljava/lang/reflect/Method;
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    invoke-virtual {v4, p1, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_3

    .line 400
    .end local v4    # "srm":Ljava/lang/reflect/Method;
    :goto_0
    return-void

    .line 387
    :catch_0
    move-exception v0

    .line 388
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string/jumbo v5, "DocumentService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 390
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v1

    .line 392
    .local v1, "iae":Ljava/lang/IllegalAccessException;
    new-instance v5, Ljava/lang/RuntimeException;

    const-string/jumbo v6, "Unable to set rounding mode"

    invoke-direct {v5, v6, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 393
    .end local v1    # "iae":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v2

    .line 395
    .local v2, "ite":Ljava/lang/reflect/InvocationTargetException;
    new-instance v5, Ljava/lang/RuntimeException;

    const-string/jumbo v6, "Unable to set rounding mode"

    invoke-direct {v5, v6, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 396
    .end local v2    # "ite":Ljava/lang/reflect/InvocationTargetException;
    :catch_3
    move-exception v3

    .line 397
    .local v3, "se":Ljava/lang/SecurityException;
    const-string/jumbo v5, "DocumentService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
