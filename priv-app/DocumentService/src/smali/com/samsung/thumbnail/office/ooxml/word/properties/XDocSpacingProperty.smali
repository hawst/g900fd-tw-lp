.class public Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;
.super Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty;
.source "XDocSpacingProperty.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty$LineRule;
    }
.end annotation


# instance fields
.field protected afterIndent:I

.field protected beforeIndent:I

.field protected lineRule:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty$LineRule;

.field protected lineValue:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 6
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty;-><init>()V

    .line 7
    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->afterIndent:I

    .line 8
    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->beforeIndent:I

    .line 9
    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->lineValue:I

    .line 46
    return-void
.end method


# virtual methods
.method public getAfter()I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->afterIndent:I

    return v0
.end method

.method public getBefore()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->beforeIndent:I

    return v0
.end method

.method public getLine()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->lineValue:I

    return v0
.end method

.method public getLineRule()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty$LineRule;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->lineRule:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty$LineRule;

    return-object v0
.end method

.method public merge(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;)V
    .locals 2
    .param p1, "prop"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    .prologue
    const/4 v1, -0x1

    .line 36
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->afterIndent:I

    if-ne v0, v1, :cond_0

    .line 37
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getAfter()I

    move-result v0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->afterIndent:I

    .line 38
    :cond_0
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->beforeIndent:I

    if-ne v0, v1, :cond_1

    .line 39
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getBefore()I

    move-result v0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->beforeIndent:I

    .line 40
    :cond_1
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->lineValue:I

    if-ne v0, v1, :cond_2

    .line 41
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getLine()I

    move-result v0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->lineValue:I

    .line 42
    :cond_2
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->lineRule:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty$LineRule;

    if-nez v0, :cond_3

    .line 43
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getLineRule()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty$LineRule;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->lineRule:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty$LineRule;

    .line 44
    :cond_3
    return-void
.end method

.method public setSpacingProp(IIILjava/lang/String;)V
    .locals 1
    .param p1, "after"    # I
    .param p2, "before"    # I
    .param p3, "line"    # I
    .param p4, "lineRule"    # Ljava/lang/String;

    .prologue
    .line 13
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->afterIndent:I

    .line 14
    iput p2, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->beforeIndent:I

    .line 15
    iput p3, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->lineValue:I

    .line 16
    invoke-static {p4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty$LineRule;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty$LineRule;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->lineRule:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty$LineRule;

    .line 17
    return-void
.end method
