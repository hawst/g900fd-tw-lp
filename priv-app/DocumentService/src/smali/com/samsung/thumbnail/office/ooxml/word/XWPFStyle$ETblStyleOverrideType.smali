.class public final enum Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;
.super Ljava/lang/Enum;
.source "XWPFStyle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ETblStyleOverrideType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

.field public static final enum BAND1_HORZ:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

.field public static final enum BAND1_VERT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

.field public static final enum BAND2_HORZ:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

.field public static final enum BAND2_VERT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

.field public static final enum FIRST_COL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

.field public static final enum FIRST_ROW:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

.field public static final enum LAST_COL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

.field public static final enum LAST_ROW:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

.field public static final enum NE_CELL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

.field public static final enum NW_CELL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

.field public static final enum SE_CELL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

.field public static final enum SW_CELL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

.field public static final enum WHOLE_TABLE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 256
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    const-string/jumbo v1, "WHOLE_TABLE"

    const-string/jumbo v2, "wholeTable"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->WHOLE_TABLE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    .line 260
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    const-string/jumbo v1, "BAND1_VERT"

    const-string/jumbo v2, "band1Vert"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->BAND1_VERT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    .line 263
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    const-string/jumbo v1, "BAND2_VERT"

    const-string/jumbo v2, "band2Vert"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->BAND2_VERT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    .line 266
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    const-string/jumbo v1, "BAND1_HORZ"

    const-string/jumbo v2, "band1Horz"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->BAND1_HORZ:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    .line 269
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    const-string/jumbo v1, "BAND2_HORZ"

    const-string/jumbo v2, "band2Horz"

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->BAND2_HORZ:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    .line 273
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    const-string/jumbo v1, "LAST_COL"

    const/4 v2, 0x5

    const-string/jumbo v3, "lastCol"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->LAST_COL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    .line 276
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    const-string/jumbo v1, "FIRST_COL"

    const/4 v2, 0x6

    const-string/jumbo v3, "firstCol"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->FIRST_COL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    .line 280
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    const-string/jumbo v1, "LAST_ROW"

    const/4 v2, 0x7

    const-string/jumbo v3, "lastRow"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->LAST_ROW:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    .line 284
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    const-string/jumbo v1, "FIRST_ROW"

    const/16 v2, 0x8

    const-string/jumbo v3, "firstRow"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->FIRST_ROW:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    .line 288
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    const-string/jumbo v1, "NE_CELL"

    const/16 v2, 0x9

    const-string/jumbo v3, "neCell"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->NE_CELL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    .line 291
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    const-string/jumbo v1, "NW_CELL"

    const/16 v2, 0xa

    const-string/jumbo v3, "nwCell"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->NW_CELL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    .line 294
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    const-string/jumbo v1, "SE_CELL"

    const/16 v2, 0xb

    const-string/jumbo v3, "seCell"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->SE_CELL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    .line 297
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    const-string/jumbo v1, "SW_CELL"

    const/16 v2, 0xc

    const-string/jumbo v3, "swCell"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->SW_CELL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    .line 252
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->WHOLE_TABLE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->BAND1_VERT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->BAND2_VERT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->BAND1_HORZ:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->BAND2_HORZ:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->LAST_COL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->FIRST_COL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->LAST_ROW:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->FIRST_ROW:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->NE_CELL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->NW_CELL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->SE_CELL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->SW_CELL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 300
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 301
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->value:Ljava/lang/String;

    .line 302
    return-void
.end method

.method public static convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 309
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->values()[Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 310
    .local v2, "inst":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 314
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;
    :goto_1
    return-object v2

    .line 309
    .restart local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 314
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 252
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;
    .locals 1

    .prologue
    .line 252
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    return-object v0
.end method


# virtual methods
.method public xmlValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->value:Ljava/lang/String;

    return-object v0
.end method
