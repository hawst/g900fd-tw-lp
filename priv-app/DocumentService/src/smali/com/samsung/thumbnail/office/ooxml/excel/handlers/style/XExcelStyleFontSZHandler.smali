.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontSZHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelStyleFontSZHandler.java"


# instance fields
.field sz:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    const-string/jumbo v0, "sz"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 25
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 30
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 31
    const-string/jumbo v1, "val"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontSZHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontSZHandler;->sz:Ljava/lang/String;

    .line 34
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v0

    .line 36
    .local v0, "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontSZHandler;->sz:Ljava/lang/String;

    iget v3, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    invoke-virtual {v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleTextSize(Ljava/lang/String;I)V

    .line 38
    return-void
.end method
