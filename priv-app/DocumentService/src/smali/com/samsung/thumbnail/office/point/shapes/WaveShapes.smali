.class public Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;
.super Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;
.source "WaveShapes.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mDrawOnCanvas:Z

.field private mLeftMarg:I

.field private mTopMarg:I


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "folderPath"    # Ljava/io/File;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;-><init>()V

    .line 28
    const-string/jumbo v0, "WaveShapes"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->TAG:Ljava/lang/String;

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mDrawOnCanvas:Z

    .line 34
    iput-object p1, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->folderPath:Ljava/io/File;

    .line 35
    return-void
.end method


# virtual methods
.method protected draw(Landroid/graphics/Canvas;I)V
    .locals 59
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeCount"    # I

    .prologue
    .line 156
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->width:F

    const v4, 0x46a8c000    # 21600.0f

    div-float v56, v3, v4

    .line 157
    .local v56, "mulFactor_Width":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->height:F

    const v4, 0x46a8c000    # 21600.0f

    div-float v55, v3, v4

    .line 159
    .local v55, "mulFactor_Height":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "Wave"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 169
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval0:I

    if-nez v3, :cond_0

    .line 170
    const/16 v3, 0xaf9

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval0:I

    .line 171
    :cond_0
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval1:I

    if-nez v3, :cond_1

    .line 172
    const/16 v3, 0x2a30

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval1:I

    .line 174
    :cond_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval0:I

    int-to-float v10, v3

    .line 175
    .local v10, "at0":F
    const/high16 v3, 0x42240000    # 41.0f

    mul-float/2addr v3, v10

    const/high16 v4, 0x41100000    # 9.0f

    div-float v11, v3, v4

    .line 176
    .local v11, "at1":F
    const/high16 v3, 0x41b80000    # 23.0f

    mul-float/2addr v3, v10

    const/high16 v4, 0x41100000    # 9.0f

    div-float v22, v3, v4

    .line 177
    .local v22, "at2":F
    move/from16 v0, v22

    neg-float v0, v0

    move/from16 v33, v0

    .line 178
    .local v33, "at3":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval0:I

    rsub-int v3, v3, 0x5460

    int-to-float v0, v3

    move/from16 v44, v0

    .line 179
    .local v44, "at4":F
    const v3, 0x46a8c000    # 21600.0f

    sub-float v49, v3, v11

    .line 180
    .local v49, "at5":F
    const v3, 0x46a8c000    # 21600.0f

    sub-float v50, v3, v33

    .line 181
    .local v50, "at6":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval1:I

    add-int/lit16 v3, v3, -0x2a30

    int-to-float v0, v3

    move/from16 v51, v0

    .line 182
    .local v51, "at7":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval1:I

    rsub-int v3, v3, 0x5460

    int-to-float v0, v3

    move/from16 v52, v0

    .line 183
    .local v52, "at8":F
    const/high16 v3, 0x40000000    # 2.0f

    mul-float v3, v3, v52

    const/high16 v4, 0x40400000    # 3.0f

    div-float v53, v3, v4

    .line 184
    .local v53, "at9":F
    const/high16 v3, 0x40800000    # 4.0f

    mul-float v3, v3, v52

    const/high16 v4, 0x40400000    # 3.0f

    div-float v12, v3, v4

    .line 185
    .local v12, "at10":F
    const/high16 v3, 0x40000000    # 2.0f

    mul-float v3, v3, v52

    const/high16 v4, 0x3f800000    # 1.0f

    div-float v13, v3, v4

    .line 186
    .local v13, "at11":F
    const v3, 0x46a8c000    # 21600.0f

    sub-float v14, v3, v53

    .line 187
    .local v14, "at12":F
    const v3, 0x46a8c000    # 21600.0f

    sub-float v15, v3, v12

    .line 188
    .local v15, "at13":F
    const v3, 0x46a8c000    # 21600.0f

    sub-float v16, v3, v13

    .line 189
    .local v16, "at14":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval1:I

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    const/high16 v4, 0x40400000    # 3.0f

    div-float v17, v3, v4

    .line 190
    .local v17, "at15":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval1:I

    mul-int/lit8 v3, v3, 0x4

    int-to-float v3, v3

    const/high16 v4, 0x40400000    # 3.0f

    div-float v18, v3, v4

    .line 191
    .local v18, "at16":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval1:I

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    const/high16 v4, 0x3f800000    # 1.0f

    div-float v19, v3, v4

    .line 192
    .local v19, "at17":F
    const v3, 0x46a8c000    # 21600.0f

    sub-float v20, v3, v17

    .line 193
    .local v20, "at18":F
    const v3, 0x46a8c000    # 21600.0f

    sub-float v21, v3, v18

    .line 194
    .local v21, "at19":F
    const v3, 0x46a8c000    # 21600.0f

    sub-float v23, v3, v19

    .line 195
    .local v23, "at20":F
    const/4 v3, 0x0

    cmpl-float v3, v51, v3

    if-lez v3, :cond_6

    move/from16 v24, v16

    .line 196
    .local v24, "at21":F
    :goto_0
    const/4 v3, 0x0

    cmpl-float v3, v51, v3

    if-lez v3, :cond_7

    move/from16 v25, v15

    .line 197
    .local v25, "at22":F
    :goto_1
    const/4 v3, 0x0

    cmpl-float v3, v51, v3

    if-lez v3, :cond_8

    move/from16 v26, v14

    .line 198
    .local v26, "at23":F
    :goto_2
    const/4 v3, 0x0

    cmpl-float v3, v51, v3

    if-lez v3, :cond_9

    const v27, 0x46a8c000    # 21600.0f

    .line 199
    .local v27, "at24":F
    :goto_3
    const/4 v3, 0x0

    cmpl-float v3, v51, v3

    if-lez v3, :cond_a

    const/16 v28, 0x0

    .line 200
    .local v28, "at25":F
    :goto_4
    const/4 v3, 0x0

    cmpl-float v3, v51, v3

    if-lez v3, :cond_b

    move/from16 v29, v53

    .line 201
    .local v29, "at26":F
    :goto_5
    const/4 v3, 0x0

    cmpl-float v3, v51, v3

    if-lez v3, :cond_c

    move/from16 v30, v12

    .line 202
    .local v30, "at27":F
    :goto_6
    const/4 v3, 0x0

    cmpl-float v3, v51, v3

    if-lez v3, :cond_d

    move/from16 v31, v13

    .line 204
    .local v31, "at28":F
    :goto_7
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 206
    .local v2, "path":Landroid/graphics/Path;
    mul-float v3, v31, v56

    mul-float v4, v10, v55

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 207
    mul-float v3, v30, v56

    mul-float v4, v11, v55

    mul-float v5, v29, v56

    mul-float v6, v33, v55

    mul-float v7, v28, v56

    mul-float v8, v10, v55

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 210
    mul-float v3, v24, v56

    mul-float v4, v44, v55

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 211
    mul-float v3, v25, v56

    mul-float v4, v49, v55

    mul-float v5, v26, v56

    mul-float v6, v50, v55

    mul-float v7, v27, v56

    mul-float v8, v44, v55

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 214
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 216
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_2

    .line 217
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 218
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_3

    .line 219
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 223
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 224
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x45d2f000    # 6750.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 225
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 226
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x465d7c00    # 14175.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    .line 322
    .end local v2    # "path":Landroid/graphics/Path;
    .end local v10    # "at0":F
    .end local v11    # "at1":F
    .end local v12    # "at10":F
    .end local v13    # "at11":F
    .end local v14    # "at12":F
    .end local v15    # "at13":F
    .end local v16    # "at14":F
    .end local v17    # "at15":F
    .end local v18    # "at16":F
    .end local v19    # "at17":F
    .end local v20    # "at18":F
    .end local v21    # "at19":F
    .end local v22    # "at2":F
    .end local v23    # "at20":F
    .end local v24    # "at21":F
    .end local v25    # "at22":F
    .end local v26    # "at23":F
    .end local v27    # "at24":F
    .end local v28    # "at25":F
    .end local v29    # "at26":F
    .end local v30    # "at27":F
    .end local v31    # "at28":F
    .end local v33    # "at3":F
    .end local v44    # "at4":F
    .end local v49    # "at5":F
    .end local v50    # "at6":F
    .end local v51    # "at7":F
    .end local v52    # "at8":F
    .end local v53    # "at9":F
    :cond_4
    :goto_8
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mDrawOnCanvas:Z

    if-nez v3, :cond_5

    .line 323
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 326
    .local v8, "mtx":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->rotation:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->width:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->height:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    invoke-virtual {v8, v3, v4, v5}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 328
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->bitmap:Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->bitmapWidth:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->bitmapHight:I

    const/4 v9, 0x1

    invoke-static/range {v3 .. v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v58

    .line 333
    .local v58, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v57, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->folderPath:Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "pic_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->shapeName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ".png"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v57

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 339
    .local v57, "out":Ljava/io/FileOutputStream;
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    move-object/from16 v0, v58

    move-object/from16 v1, v57

    invoke-virtual {v0, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 340
    invoke-virtual/range {v57 .. v57}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 346
    .end local v8    # "mtx":Landroid/graphics/Matrix;
    .end local v57    # "out":Ljava/io/FileOutputStream;
    .end local v58    # "rotatedBMP":Landroid/graphics/Bitmap;
    :cond_5
    :goto_9
    return-void

    .line 195
    .restart local v10    # "at0":F
    .restart local v11    # "at1":F
    .restart local v12    # "at10":F
    .restart local v13    # "at11":F
    .restart local v14    # "at12":F
    .restart local v15    # "at13":F
    .restart local v16    # "at14":F
    .restart local v17    # "at15":F
    .restart local v18    # "at16":F
    .restart local v19    # "at17":F
    .restart local v20    # "at18":F
    .restart local v21    # "at19":F
    .restart local v22    # "at2":F
    .restart local v23    # "at20":F
    .restart local v33    # "at3":F
    .restart local v44    # "at4":F
    .restart local v49    # "at5":F
    .restart local v50    # "at6":F
    .restart local v51    # "at7":F
    .restart local v52    # "at8":F
    .restart local v53    # "at9":F
    :cond_6
    const/16 v24, 0x0

    goto/16 :goto_0

    .restart local v24    # "at21":F
    :cond_7
    move/from16 v25, v17

    .line 196
    goto/16 :goto_1

    .restart local v25    # "at22":F
    :cond_8
    move/from16 v26, v18

    .line 197
    goto/16 :goto_2

    .restart local v26    # "at23":F
    :cond_9
    move/from16 v27, v19

    .line 198
    goto/16 :goto_3

    .restart local v27    # "at24":F
    :cond_a
    move/from16 v28, v23

    .line 199
    goto/16 :goto_4

    .restart local v28    # "at25":F
    :cond_b
    move/from16 v29, v21

    .line 200
    goto/16 :goto_5

    .restart local v29    # "at26":F
    :cond_c
    move/from16 v30, v20

    .line 201
    goto/16 :goto_6

    .line 202
    .restart local v30    # "at27":F
    :cond_d
    const v31, 0x46a8c000    # 21600.0f

    goto/16 :goto_7

    .line 229
    .end local v10    # "at0":F
    .end local v11    # "at1":F
    .end local v12    # "at10":F
    .end local v13    # "at11":F
    .end local v14    # "at12":F
    .end local v15    # "at13":F
    .end local v16    # "at14":F
    .end local v17    # "at15":F
    .end local v18    # "at16":F
    .end local v19    # "at17":F
    .end local v20    # "at18":F
    .end local v21    # "at19":F
    .end local v22    # "at2":F
    .end local v23    # "at20":F
    .end local v24    # "at21":F
    .end local v25    # "at22":F
    .end local v26    # "at23":F
    .end local v27    # "at24":F
    .end local v28    # "at25":F
    .end local v29    # "at26":F
    .end local v30    # "at27":F
    .end local v33    # "at3":F
    .end local v44    # "at4":F
    .end local v49    # "at5":F
    .end local v50    # "at6":F
    .end local v51    # "at7":F
    .end local v52    # "at8":F
    .end local v53    # "at9":F
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "DoubleWave"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 239
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval0:I

    if-nez v3, :cond_f

    .line 240
    const/16 v3, 0x57c

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval0:I

    .line 241
    :cond_f
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval1:I

    if-nez v3, :cond_10

    .line 242
    const/16 v3, 0x2a30

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval1:I

    .line 244
    :cond_10
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval0:I

    int-to-float v10, v3

    .line 245
    .restart local v10    # "at0":F
    const/high16 v3, 0x42240000    # 41.0f

    mul-float/2addr v3, v10

    const/high16 v4, 0x41100000    # 9.0f

    div-float v11, v3, v4

    .line 246
    .restart local v11    # "at1":F
    const/high16 v3, 0x41b80000    # 23.0f

    mul-float/2addr v3, v10

    const/high16 v4, 0x41100000    # 9.0f

    div-float v22, v3, v4

    .line 247
    .restart local v22    # "at2":F
    move/from16 v0, v22

    neg-float v0, v0

    move/from16 v33, v0

    .line 248
    .restart local v33    # "at3":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval0:I

    rsub-int v3, v3, 0x5460

    int-to-float v0, v3

    move/from16 v44, v0

    .line 249
    .restart local v44    # "at4":F
    const v3, 0x46a8c000    # 21600.0f

    sub-float v49, v3, v11

    .line 250
    .restart local v49    # "at5":F
    const v3, 0x46a8c000    # 21600.0f

    sub-float v50, v3, v33

    .line 251
    .restart local v50    # "at6":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval1:I

    add-int/lit16 v3, v3, -0x2a30

    int-to-float v0, v3

    move/from16 v51, v0

    .line 252
    .restart local v51    # "at7":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval1:I

    rsub-int v3, v3, 0x5460

    int-to-float v0, v3

    move/from16 v52, v0

    .line 253
    .restart local v52    # "at8":F
    const/high16 v3, 0x40400000    # 3.0f

    div-float v53, v52, v3

    .line 254
    .restart local v53    # "at9":F
    const/high16 v3, 0x40000000    # 2.0f

    mul-float v3, v3, v52

    const/high16 v4, 0x40400000    # 3.0f

    div-float v12, v3, v4

    .line 255
    .restart local v12    # "at10":F
    const/high16 v3, 0x40800000    # 4.0f

    mul-float v3, v3, v52

    const/high16 v4, 0x40400000    # 3.0f

    div-float v13, v3, v4

    .line 256
    .restart local v13    # "at11":F
    const/high16 v3, 0x40a00000    # 5.0f

    mul-float v3, v3, v52

    const/high16 v4, 0x40400000    # 3.0f

    div-float v14, v3, v4

    .line 257
    .restart local v14    # "at12":F
    const/high16 v3, 0x40000000    # 2.0f

    mul-float v15, v52, v3

    .line 258
    .restart local v15    # "at13":F
    const v3, 0x46a8c000    # 21600.0f

    sub-float v16, v3, v53

    .line 259
    .restart local v16    # "at14":F
    const v3, 0x46a8c000    # 21600.0f

    sub-float v17, v3, v12

    .line 260
    .restart local v17    # "at15":F
    const v3, 0x46a8c000    # 21600.0f

    sub-float v18, v3, v52

    .line 261
    .restart local v18    # "at16":F
    const v3, 0x46a8c000    # 21600.0f

    sub-float v19, v3, v13

    .line 262
    .restart local v19    # "at17":F
    const v3, 0x46a8c000    # 21600.0f

    sub-float v20, v3, v14

    .line 263
    .restart local v20    # "at18":F
    const v3, 0x46a8c000    # 21600.0f

    sub-float v21, v3, v15

    .line 264
    .restart local v21    # "at19":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval1:I

    mul-int/lit8 v3, v3, 0x1

    int-to-float v3, v3

    const/high16 v4, 0x40400000    # 3.0f

    div-float v23, v3, v4

    .line 265
    .restart local v23    # "at20":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval1:I

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    const/high16 v4, 0x40400000    # 3.0f

    div-float v24, v3, v4

    .line 266
    .restart local v24    # "at21":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval1:I

    mul-int/lit8 v3, v3, 0x4

    int-to-float v3, v3

    const/high16 v4, 0x40400000    # 3.0f

    div-float v25, v3, v4

    .line 267
    .restart local v25    # "at22":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval1:I

    mul-int/lit8 v3, v3, 0x5

    int-to-float v3, v3

    const/high16 v4, 0x40400000    # 3.0f

    div-float v26, v3, v4

    .line 268
    .restart local v26    # "at23":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval1:I

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    const/high16 v4, 0x3f800000    # 1.0f

    div-float v27, v3, v4

    .line 269
    .restart local v27    # "at24":F
    const v3, 0x46a8c000    # 21600.0f

    sub-float v28, v3, v23

    .line 270
    .restart local v28    # "at25":F
    const v3, 0x46a8c000    # 21600.0f

    sub-float v29, v3, v24

    .line 271
    .restart local v29    # "at26":F
    const v3, 0x46a8c000    # 21600.0f

    sub-float v30, v3, v25

    .line 272
    .restart local v30    # "at27":F
    const v3, 0x46a8c000    # 21600.0f

    sub-float v31, v3, v26

    .line 273
    .restart local v31    # "at28":F
    const v3, 0x46a8c000    # 21600.0f

    sub-float v32, v3, v27

    .line 274
    .local v32, "at29":F
    const/4 v3, 0x0

    cmpl-float v3, v51, v3

    if-lez v3, :cond_13

    move/from16 v34, v21

    .line 275
    .local v34, "at30":F
    :goto_a
    const/4 v3, 0x0

    cmpl-float v3, v51, v3

    if-lez v3, :cond_14

    move/from16 v35, v20

    .line 276
    .local v35, "at31":F
    :goto_b
    const/4 v3, 0x0

    cmpl-float v3, v51, v3

    if-lez v3, :cond_15

    move/from16 v36, v19

    .line 277
    .local v36, "at32":F
    :goto_c
    const/4 v3, 0x0

    cmpl-float v3, v51, v3

    if-lez v3, :cond_16

    move/from16 v37, v18

    .line 278
    .local v37, "at33":F
    :goto_d
    const/4 v3, 0x0

    cmpl-float v3, v51, v3

    if-lez v3, :cond_17

    move/from16 v38, v17

    .line 279
    .local v38, "at34":F
    :goto_e
    const/4 v3, 0x0

    cmpl-float v3, v51, v3

    if-lez v3, :cond_18

    move/from16 v39, v16

    .line 280
    .local v39, "at35":F
    :goto_f
    const/4 v3, 0x0

    cmpl-float v3, v51, v3

    if-lez v3, :cond_19

    const v40, 0x46a8c000    # 21600.0f

    .line 281
    .local v40, "at36":F
    :goto_10
    const/4 v3, 0x0

    cmpl-float v3, v51, v3

    if-lez v3, :cond_1a

    const/16 v41, 0x0

    .line 282
    .local v41, "at37":F
    :goto_11
    const/4 v3, 0x0

    cmpl-float v3, v51, v3

    if-lez v3, :cond_1b

    move/from16 v42, v53

    .line 283
    .local v42, "at38":F
    :goto_12
    const/4 v3, 0x0

    cmpl-float v3, v51, v3

    if-lez v3, :cond_1c

    move/from16 v43, v12

    .line 284
    .local v43, "at39":F
    :goto_13
    move/from16 v45, v52

    .line 285
    .local v45, "at40":F
    const/4 v3, 0x0

    cmpl-float v3, v51, v3

    if-lez v3, :cond_1d

    move/from16 v46, v13

    .line 286
    .local v46, "at41":F
    :goto_14
    const/4 v3, 0x0

    cmpl-float v3, v51, v3

    if-lez v3, :cond_1e

    move/from16 v47, v14

    .line 287
    .local v47, "at42":F
    :goto_15
    const/4 v3, 0x0

    cmpl-float v3, v51, v3

    if-lez v3, :cond_1f

    move/from16 v48, v15

    .line 289
    .local v48, "at43":F
    :goto_16
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 292
    .restart local v2    # "path":Landroid/graphics/Path;
    mul-float v3, v48, v56

    mul-float v4, v10, v55

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 293
    mul-float v3, v47, v56

    mul-float v4, v11, v55

    mul-float v5, v46, v56

    mul-float v6, v33, v55

    mul-float v7, v45, v56

    mul-float v8, v10, v55

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 296
    mul-float v3, v43, v56

    mul-float v4, v11, v55

    mul-float v5, v42, v56

    mul-float v6, v33, v55

    mul-float v7, v41, v56

    mul-float v8, v10, v55

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 299
    mul-float v3, v34, v56

    mul-float v4, v44, v55

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 300
    mul-float v3, v35, v56

    mul-float v4, v49, v55

    mul-float v5, v36, v56

    mul-float v6, v50, v55

    mul-float v7, v37, v56

    mul-float v8, v44, v55

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 303
    mul-float v3, v38, v56

    mul-float v4, v49, v55

    mul-float v5, v39, v56

    mul-float v6, v50, v55

    mul-float v7, v40, v56

    mul-float v8, v44, v55

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 306
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 308
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_11

    .line 309
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 310
    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_12

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_12

    .line 311
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 315
    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 316
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x4552f000    # 3375.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 317
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 318
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x468e6200    # 18225.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_8

    .line 274
    .end local v2    # "path":Landroid/graphics/Path;
    .end local v34    # "at30":F
    .end local v35    # "at31":F
    .end local v36    # "at32":F
    .end local v37    # "at33":F
    .end local v38    # "at34":F
    .end local v39    # "at35":F
    .end local v40    # "at36":F
    .end local v41    # "at37":F
    .end local v42    # "at38":F
    .end local v43    # "at39":F
    .end local v45    # "at40":F
    .end local v46    # "at41":F
    .end local v47    # "at42":F
    .end local v48    # "at43":F
    :cond_13
    const/16 v34, 0x0

    goto/16 :goto_a

    .restart local v34    # "at30":F
    :cond_14
    move/from16 v35, v23

    .line 275
    goto/16 :goto_b

    .restart local v35    # "at31":F
    :cond_15
    move/from16 v36, v24

    .line 276
    goto/16 :goto_c

    .line 277
    .restart local v36    # "at32":F
    :cond_16
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->adjval1:I

    int-to-float v0, v3

    move/from16 v37, v0

    goto/16 :goto_d

    .restart local v37    # "at33":F
    :cond_17
    move/from16 v38, v25

    .line 278
    goto/16 :goto_e

    .restart local v38    # "at34":F
    :cond_18
    move/from16 v39, v26

    .line 279
    goto/16 :goto_f

    .restart local v39    # "at35":F
    :cond_19
    move/from16 v40, v27

    .line 280
    goto/16 :goto_10

    .restart local v40    # "at36":F
    :cond_1a
    move/from16 v41, v32

    .line 281
    goto/16 :goto_11

    .restart local v41    # "at37":F
    :cond_1b
    move/from16 v42, v31

    .line 282
    goto/16 :goto_12

    .restart local v42    # "at38":F
    :cond_1c
    move/from16 v43, v30

    .line 283
    goto/16 :goto_13

    .restart local v43    # "at39":F
    .restart local v45    # "at40":F
    :cond_1d
    move/from16 v46, v29

    .line 285
    goto/16 :goto_14

    .restart local v46    # "at41":F
    :cond_1e
    move/from16 v47, v28

    .line 286
    goto/16 :goto_15

    .line 287
    .restart local v47    # "at42":F
    :cond_1f
    const v48, 0x46a8c000    # 21600.0f

    goto/16 :goto_16

    .line 341
    .end local v10    # "at0":F
    .end local v11    # "at1":F
    .end local v12    # "at10":F
    .end local v13    # "at11":F
    .end local v14    # "at12":F
    .end local v15    # "at13":F
    .end local v16    # "at14":F
    .end local v17    # "at15":F
    .end local v18    # "at16":F
    .end local v19    # "at17":F
    .end local v20    # "at18":F
    .end local v21    # "at19":F
    .end local v22    # "at2":F
    .end local v23    # "at20":F
    .end local v24    # "at21":F
    .end local v25    # "at22":F
    .end local v26    # "at23":F
    .end local v27    # "at24":F
    .end local v28    # "at25":F
    .end local v29    # "at26":F
    .end local v30    # "at27":F
    .end local v31    # "at28":F
    .end local v32    # "at29":F
    .end local v33    # "at3":F
    .end local v34    # "at30":F
    .end local v35    # "at31":F
    .end local v36    # "at32":F
    .end local v37    # "at33":F
    .end local v38    # "at34":F
    .end local v39    # "at35":F
    .end local v40    # "at36":F
    .end local v41    # "at37":F
    .end local v42    # "at38":F
    .end local v43    # "at39":F
    .end local v44    # "at4":F
    .end local v45    # "at40":F
    .end local v46    # "at41":F
    .end local v47    # "at42":F
    .end local v49    # "at5":F
    .end local v50    # "at6":F
    .end local v51    # "at7":F
    .end local v52    # "at8":F
    .end local v53    # "at9":F
    .restart local v8    # "mtx":Landroid/graphics/Matrix;
    .restart local v58    # "rotatedBMP":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v54

    .line 342
    .local v54, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception while writing pictures to out file"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v54

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_9
.end method

.method public drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F

    .prologue
    .line 110
    invoke-super {p0, p3, p4, p7, p8}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IFF)V

    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mDrawOnCanvas:Z

    .line 112
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mLeftMarg:I

    .line 113
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mTopMarg:I

    .line 114
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 115
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 116
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 117
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mDrawOnCanvas:Z

    .line 118
    return-void
.end method

.method public drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p2, "shapeCount"    # I

    .prologue
    .line 101
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V

    .line 103
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->draw(I)V

    .line 104
    return-void
.end method

.method public drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 87
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;I)V

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mDrawOnCanvas:Z

    .line 89
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mLeftMarg:I

    .line 90
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mTopMarg:I

    .line 91
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->width:F

    .line 92
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->height:F

    .line 93
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 94
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 95
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mDrawOnCanvas:Z

    .line 97
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V
    .locals 0
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 39
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 40
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->draw(I)V

    .line 41
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V
    .locals 2
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I

    .prologue
    .line 45
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mDrawOnCanvas:Z

    .line 47
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mLeftMarg:I

    .line 48
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mTopMarg:I

    .line 49
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 50
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 51
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mDrawOnCanvas:Z

    .line 53
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 131
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 132
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mDrawOnCanvas:Z

    .line 133
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mLeftMarg:I

    .line 134
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mTopMarg:I

    .line 135
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 136
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 137
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 138
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mDrawOnCanvas:Z

    .line 139
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 143
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V

    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mDrawOnCanvas:Z

    .line 145
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mLeftMarg:I

    .line 146
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mTopMarg:I

    .line 147
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 148
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 149
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 150
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mDrawOnCanvas:Z

    .line 151
    return-void
.end method

.method public drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .prologue
    .line 125
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 126
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->draw(I)V

    .line 127
    return-void
.end method

.method public drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 58
    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p7

    move v4, p8

    move-object v5, p9

    invoke-super/range {v0 .. v5}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSSExcelShape(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mDrawOnCanvas:Z

    .line 61
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mLeftMarg:I

    .line 62
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mTopMarg:I

    .line 63
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 64
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 65
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mDrawOnCanvas:Z

    .line 67
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 71
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mDrawOnCanvas:Z

    .line 73
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mLeftMarg:I

    .line 74
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mTopMarg:I

    .line 75
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->width:F

    .line 76
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->height:F

    .line 77
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 78
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 79
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->mDrawOnCanvas:Z

    .line 81
    return-void
.end method
