.class public Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/GroupShapeHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "GroupShapeHandler.java"


# instance fields
.field private ID:I

.field private autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

.field private shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "ID"    # I

    .prologue
    .line 22
    const-string/jumbo v0, "grpSp"

    invoke-direct {p0, p1, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 23
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/GroupShapeHandler;->ID:I

    .line 24
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 62
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 63
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    .line 65
    .local v0, "slide":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->setGroupshape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;)V

    .line 66
    return-void
.end method

.method protected init(I)V
    .locals 7
    .param p1, "ID"    # I

    .prologue
    .line 27
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;

    const-string/jumbo v5, "sp"

    invoke-direct {v4, p1, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;-><init>(ILjava/lang/String;)V

    .line 29
    .local v4, "shapeHandler":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/GroupShapeHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v6, "sp"

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTPictureHandler;

    invoke-direct {v3, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTPictureHandler;-><init>(I)V

    .line 32
    .local v3, "picHandler":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTPictureHandler;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/GroupShapeHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v6, "pic"

    invoke-virtual {v5, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicFrameHandler;

    invoke-direct {v1, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicFrameHandler;-><init>(I)V

    .line 35
    .local v1, "graphicFrame":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicFrameHandler;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/GroupShapeHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v6, "graphicFrame"

    invoke-virtual {v5, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;

    const-string/jumbo v5, "cxnSp"

    invoke-direct {v0, p1, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;-><init>(ILjava/lang/String;)V

    .line 39
    .local v0, "cxnShapeHandler":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTShapeHandler;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/GroupShapeHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v6, "cxnSp"

    invoke-virtual {v5, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/GroupShapePropHandler;

    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/GroupShapeHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-direct {v2, p1, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/GroupShapePropHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 43
    .local v2, "grpShapePropHandler":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/GroupShapePropHandler;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/GroupShapeHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v6, "grpSpPr"

    invoke-virtual {v5, v6, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 49
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 50
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    .line 51
    .local v0, "slide":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/GroupShapeHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 52
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/GroupShapeHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-direct {v1, v2, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;)V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/GroupShapeHandler;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    .line 53
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/GroupShapeHandler;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->setIsGroupshape()V

    .line 55
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/GroupShapeHandler;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->addShapes(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;)V

    .line 56
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/GroupShapeHandler;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->setGroupshape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;)V

    .line 57
    iget v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/GroupShapeHandler;->ID:I

    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/GroupShapeHandler;->init(I)V

    .line 58
    return-void
.end method
