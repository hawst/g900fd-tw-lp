.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;
.source "SpPrNoFillHandler.java"


# instance fields
.field private fillProperty:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

.field private isLine:Z

.field private parent:Ljava/lang/String;

.field private shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 3
    .param p1, "shInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    const/4 v2, 0x0

    .line 38
    const/16 v0, 0x1f

    const-string/jumbo v1, "noFill"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;-><init>(ILjava/lang/String;)V

    .line 17
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;->parent:Ljava/lang/String;

    .line 18
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;->fillProperty:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .line 39
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 40
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;Ljava/lang/String;)V
    .locals 3
    .param p1, "shInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "property"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    .param p3, "inParent"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 25
    const/16 v0, 0x1f

    const-string/jumbo v1, "noFill"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;-><init>(ILjava/lang/String;)V

    .line 17
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;->parent:Ljava/lang/String;

    .line 18
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;->fillProperty:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .line 26
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 27
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;->parent:Ljava/lang/String;

    .line 28
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;->fillProperty:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .line 29
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Z)V
    .locals 3
    .param p1, "shInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "isLine"    # Z

    .prologue
    const/4 v2, 0x0

    .line 32
    const/16 v0, 0x1f

    const-string/jumbo v1, "noFill"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;-><init>(ILjava/lang/String;)V

    .line 17
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;->parent:Ljava/lang/String;

    .line 18
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;->fillProperty:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .line 33
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 34
    iput-boolean p2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;->isLine:Z

    .line 35
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    const/4 v2, 0x1

    .line 45
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 48
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;->parent:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;->parent:Ljava/lang/String;

    const-string/jumbo v1, "ExcelspPr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;->fillProperty:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;->fillProperty:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    const/16 v1, 0xff

    iput v1, v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->fillType:I

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;->isLine:Z

    if-eqz v0, :cond_2

    .line 68
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    iput-boolean v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isLineNoFill:Z

    .line 75
    :cond_1
    :goto_0
    return-void

    .line 71
    :cond_2
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;->isLine:Z

    if-nez v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;->shInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    iput-boolean v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isShapeNoFill:Z

    goto :goto_0
.end method
