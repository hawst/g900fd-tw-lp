.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgSzHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;
.source "XDocPgSzHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgSzHandler$IPgSize;
    }
.end annotation


# instance fields
.field private pgSize:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgSzHandler$IPgSize;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgSzHandler$IPgSize;)V
    .locals 1
    .param p1, "pgSize"    # Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgSzHandler$IPgSize;

    .prologue
    .line 24
    const-string/jumbo v0, "pgSz"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;-><init>(Ljava/lang/String;)V

    .line 25
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgSzHandler;->pgSize:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgSzHandler$IPgSize;

    .line 26
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 31
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 33
    const-string/jumbo v1, "w"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgSzHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "val":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 35
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgSzHandler;->pgSize:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgSzHandler$IPgSize;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v2

    invoke-interface {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgSzHandler$IPgSize;->setPgWidth(F)V

    .line 38
    :cond_0
    const-string/jumbo v1, "h"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgSzHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 39
    if-eqz v0, :cond_1

    .line 40
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgSzHandler;->pgSize:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgSzHandler$IPgSize;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v2

    invoke-interface {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgSzHandler$IPgSize;->setPgHeight(F)V

    .line 43
    :cond_1
    const-string/jumbo v1, "orient"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgSzHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 44
    if-nez v0, :cond_3

    .line 51
    :cond_2
    :goto_0
    return-void

    .line 48
    :cond_3
    const-string/jumbo v1, "landscape"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 49
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgSzHandler;->pgSize:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgSzHandler$IPgSize;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgSzHandler$IPgSize;->setLandscape(Z)V

    goto :goto_0
.end method
