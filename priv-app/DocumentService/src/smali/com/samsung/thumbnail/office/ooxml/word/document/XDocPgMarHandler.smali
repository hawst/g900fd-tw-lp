.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;
.source "XDocPgMarHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler$IPgMargin;
    }
.end annotation


# instance fields
.field private pgMar:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler$IPgMargin;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler$IPgMargin;)V
    .locals 1
    .param p1, "pgMar"    # Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler$IPgMargin;

    .prologue
    .line 21
    const-string/jumbo v0, "pgMar"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;-><init>(Ljava/lang/String;)V

    .line 22
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler;->pgMar:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler$IPgMargin;

    .line 23
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 28
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 30
    const-string/jumbo v1, "top"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 31
    .local v0, "val":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 32
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler;->pgMar:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler$IPgMargin;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v2

    float-to-int v2, v2

    invoke-interface {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler$IPgMargin;->setTop(I)V

    .line 35
    :cond_0
    const-string/jumbo v1, "bottom"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 36
    if-eqz v0, :cond_1

    .line 37
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler;->pgMar:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler$IPgMargin;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v2

    float-to-int v2, v2

    invoke-interface {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler$IPgMargin;->setBotom(I)V

    .line 40
    :cond_1
    const-string/jumbo v1, "left"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 41
    if-eqz v0, :cond_2

    .line 42
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler;->pgMar:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler$IPgMargin;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v2

    invoke-interface {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler$IPgMargin;->setLeft(F)V

    .line 45
    :cond_2
    const-string/jumbo v1, "right"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 46
    if-eqz v0, :cond_3

    .line 47
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler;->pgMar:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler$IPgMargin;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v2

    invoke-interface {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler$IPgMargin;->setRight(F)V

    .line 50
    :cond_3
    const-string/jumbo v1, "header"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 51
    if-eqz v0, :cond_4

    .line 52
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler;->pgMar:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler$IPgMargin;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v2

    invoke-interface {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler$IPgMargin;->setHeader(F)V

    .line 55
    :cond_4
    const-string/jumbo v1, "footer"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 56
    if-eqz v0, :cond_5

    .line 57
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler;->pgMar:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler$IPgMargin;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v2

    invoke-interface {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPgMarHandler$IPgMargin;->setFooter(F)V

    .line 59
    :cond_5
    return-void
.end method
