.class public Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;
.super Ljava/lang/Object;
.source "XDocShadingProperty.java"


# instance fields
.field private color:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

.field private fill:Ljava/lang/String;

.field private pattern:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->color:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    return-object v0
.end method

.method public getFill()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->fill:Ljava/lang/String;

    return-object v0
.end method

.method public getPattern()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->pattern:Ljava/lang/String;

    return-object v0
.end method

.method public setColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V
    .locals 0
    .param p1, "color"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->color:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .line 23
    return-void
.end method

.method public setFill(Ljava/lang/String;)V
    .locals 0
    .param p1, "fill"    # Ljava/lang/String;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->fill:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public setPattern(Ljava/lang/String;)V
    .locals 0
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->pattern:Ljava/lang/String;

    .line 19
    return-void
.end method
