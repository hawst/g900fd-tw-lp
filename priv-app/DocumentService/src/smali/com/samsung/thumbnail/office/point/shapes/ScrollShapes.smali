.class public Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;
.super Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;
.source "ScrollShapes.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mDrawOnCanvas:Z

.field private mLeftMarg:I

.field private mTopMarg:I


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "folderPath"    # Ljava/io/File;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;-><init>()V

    .line 28
    const-string/jumbo v0, "ScrollShapes"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->TAG:Ljava/lang/String;

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mDrawOnCanvas:Z

    .line 34
    iput-object p1, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->folderPath:Ljava/io/File;

    .line 35
    return-void
.end method


# virtual methods
.method protected draw(Landroid/graphics/Canvas;I)V
    .locals 21
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeCount"    # I

    .prologue
    .line 156
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    const v3, 0x46a8c000    # 21600.0f

    div-float v17, v2, v3

    .line 157
    .local v17, "mulFactor_Width":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    const v3, 0x46a8c000    # 21600.0f

    div-float v16, v2, v3

    .line 162
    .local v16, "mulFactor_Height":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->adjval0:I

    if-nez v2, :cond_0

    .line 163
    const/16 v2, 0xa8c

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->adjval0:I

    .line 165
    :cond_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->adjval0:I

    int-to-float v9, v2

    .line 166
    .local v9, "at1":F
    const/high16 v2, 0x3f800000    # 1.0f

    mul-float/2addr v2, v9

    const/high16 v3, 0x40000000    # 2.0f

    div-float v10, v2, v3

    .line 167
    .local v10, "at2":F
    const/high16 v2, 0x40400000    # 3.0f

    mul-float/2addr v2, v9

    const/high16 v3, 0x40800000    # 4.0f

    div-float v11, v2, v3

    .line 168
    .local v11, "at3":F
    const/high16 v2, 0x40a00000    # 5.0f

    mul-float/2addr v2, v9

    const/high16 v3, 0x40800000    # 4.0f

    div-float v12, v2, v3

    .line 169
    .local v12, "at4":F
    const/high16 v2, 0x40400000    # 3.0f

    mul-float/2addr v2, v9

    const/high16 v3, 0x40000000    # 2.0f

    div-float v13, v2, v3

    .line 170
    .local v13, "at5":F
    const/high16 v2, 0x40000000    # 2.0f

    mul-float v14, v9, v2

    .line 172
    .local v14, "at6":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "HorizontalScroll"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 173
    new-instance v19, Landroid/graphics/Path;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Path;-><init>()V

    .line 176
    .local v19, "path":Landroid/graphics/Path;
    const/4 v2, 0x0

    mul-float v3, v13, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 177
    const/4 v2, 0x0

    mul-float v3, v9, v16

    mul-float v4, v10, v17

    mul-float v5, v9, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 179
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->adjval0:I

    int-to-float v3, v3

    mul-float v3, v3, v17

    sub-float/2addr v2, v3

    mul-float v3, v9, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 181
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->adjval0:I

    int-to-float v3, v3

    mul-float v3, v3, v17

    sub-float/2addr v2, v3

    mul-float v3, v10, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 183
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->adjval0:I

    int-to-float v3, v3

    mul-float v3, v3, v17

    sub-float/2addr v2, v3

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    mul-float v5, v10, v17

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 185
    const v2, 0x46a8c000    # 21600.0f

    mul-float v2, v2, v17

    const/4 v3, 0x0

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v17

    mul-float v5, v10, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 187
    const v2, 0x46a8c000    # 21600.0f

    mul-float v2, v2, v17

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    mul-float v4, v13, v16

    sub-float/2addr v3, v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 189
    const v2, 0x46a8c000    # 21600.0f

    mul-float v2, v2, v17

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    mul-float v4, v9, v16

    sub-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    mul-float v5, v10, v17

    sub-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    mul-float v6, v9, v16

    sub-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 192
    mul-float v2, v9, v17

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    mul-float v4, v9, v16

    sub-float/2addr v3, v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 193
    mul-float v2, v9, v17

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    mul-float v4, v10, v16

    sub-float/2addr v3, v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 194
    mul-float v2, v9, v17

    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v16

    mul-float v4, v10, v17

    const v5, 0x46a8c000    # 21600.0f

    mul-float v5, v5, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 196
    const/4 v2, 0x0

    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v16

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    mul-float v6, v10, v17

    sub-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 198
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Path;->close()V

    .line 199
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v2, :cond_1

    .line 200
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 201
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_2

    .line 202
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 205
    :cond_2
    const/4 v2, 0x0

    mul-float v3, v13, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 206
    const/4 v2, 0x0

    mul-float v3, v14, v16

    mul-float v4, v10, v17

    mul-float v5, v14, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 208
    mul-float v2, v9, v17

    mul-float v3, v14, v16

    mul-float v4, v9, v17

    mul-float v5, v13, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 210
    mul-float v2, v9, v17

    mul-float v3, v12, v16

    mul-float v4, v11, v17

    mul-float v5, v12, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 212
    mul-float v2, v10, v17

    mul-float v3, v12, v16

    mul-float v4, v10, v17

    mul-float v5, v13, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 214
    mul-float v2, v10, v17

    mul-float v3, v14, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 217
    mul-float v2, v9, v17

    mul-float v3, v13, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 218
    mul-float v2, v9, v17

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    mul-float v4, v9, v16

    sub-float/2addr v3, v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 221
    const v2, 0x46a8c000    # 21600.0f

    mul-float v2, v2, v17

    mul-float v3, v10, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 222
    const v2, 0x46a8c000    # 21600.0f

    mul-float v2, v2, v17

    mul-float v3, v9, v16

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    mul-float v5, v10, v17

    sub-float/2addr v4, v5

    mul-float v5, v9, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 224
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->adjval0:I

    int-to-float v3, v3

    mul-float v3, v3, v17

    sub-float/2addr v2, v3

    mul-float v3, v9, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 228
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->adjval0:I

    int-to-float v3, v3

    mul-float v3, v3, v17

    sub-float/2addr v2, v3

    mul-float v3, v10, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 230
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->adjval0:I

    int-to-float v3, v3

    mul-float v3, v3, v17

    sub-float/2addr v2, v3

    mul-float v3, v11, v16

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    mul-float v5, v11, v17

    sub-float/2addr v4, v5

    mul-float v5, v11, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 233
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    mul-float v3, v10, v17

    sub-float/2addr v2, v3

    mul-float v3, v11, v16

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    mul-float v5, v10, v17

    sub-float/2addr v4, v5

    mul-float v5, v10, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 235
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    mul-float v3, v10, v17

    sub-float/2addr v2, v3

    mul-float v3, v9, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 237
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_3

    .line 238
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 242
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x454fd000    # 3325.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 243
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x454fd000    # 3325.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 244
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x4693a800    # 18900.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 245
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x468ec600    # 18275.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    .line 325
    .end local v19    # "path":Landroid/graphics/Path;
    :cond_4
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mDrawOnCanvas:Z

    if-nez v2, :cond_5

    .line 326
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 329
    .local v7, "mtx":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->rotation:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-virtual {v7, v2, v3, v4}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 331
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->bitmap:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->bitmapWidth:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->bitmapHight:I

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v20

    .line 336
    .local v20, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v18, Ljava/io/FileOutputStream;

    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->folderPath:Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "pic_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->shapeName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ".png"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 342
    .local v18, "out":Ljava/io/FileOutputStream;
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 343
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 348
    .end local v7    # "mtx":Landroid/graphics/Matrix;
    .end local v18    # "out":Ljava/io/FileOutputStream;
    .end local v20    # "rotatedBMP":Landroid/graphics/Bitmap;
    :cond_5
    :goto_1
    return-void

    .line 248
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "VerticalScroll"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 250
    new-instance v19, Landroid/graphics/Path;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Path;-><init>()V

    .line 253
    .restart local v19    # "path":Landroid/graphics/Path;
    mul-float v2, v13, v17

    const/4 v3, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 254
    mul-float v2, v9, v17

    const/4 v3, 0x0

    mul-float v4, v9, v17

    mul-float v5, v10, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 256
    mul-float v2, v9, v17

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->adjval0:I

    int-to-float v4, v4

    mul-float v4, v4, v16

    sub-float/2addr v3, v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 258
    mul-float v2, v10, v17

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->adjval0:I

    int-to-float v4, v4

    mul-float v4, v4, v16

    sub-float/2addr v3, v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 260
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->adjval0:I

    int-to-float v4, v4

    mul-float v4, v4, v16

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    mul-float v6, v10, v16

    sub-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 262
    const/4 v2, 0x0

    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v16

    mul-float v4, v10, v17

    const v5, 0x46a8c000    # 21600.0f

    mul-float v5, v5, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 264
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    mul-float v3, v13, v17

    sub-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 265
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    mul-float v3, v9, v17

    sub-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v16

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    mul-float v5, v9, v17

    sub-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    mul-float v6, v10, v16

    sub-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 268
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    mul-float v3, v9, v17

    sub-float/2addr v2, v3

    mul-float v3, v9, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 269
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    mul-float v3, v10, v17

    sub-float/2addr v2, v3

    mul-float v3, v9, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 270
    const v2, 0x46a8c000    # 21600.0f

    mul-float v2, v2, v17

    mul-float v3, v9, v16

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v17

    mul-float v5, v10, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 272
    const v2, 0x46a8c000    # 21600.0f

    mul-float v2, v2, v17

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    mul-float v5, v10, v17

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 274
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Path;->close()V

    .line 275
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v2, :cond_7

    .line 276
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 277
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_8

    .line 278
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 281
    :cond_8
    mul-float v2, v13, v17

    const/4 v3, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 282
    mul-float v2, v14, v17

    const/4 v3, 0x0

    mul-float v4, v14, v17

    mul-float v5, v10, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 284
    mul-float v2, v14, v17

    mul-float v3, v9, v16

    mul-float v4, v13, v17

    mul-float v5, v9, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 286
    mul-float v2, v12, v17

    mul-float v3, v9, v16

    mul-float v4, v12, v17

    mul-float v5, v11, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 288
    mul-float v2, v12, v17

    mul-float v3, v10, v16

    mul-float v4, v13, v17

    mul-float v5, v10, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 290
    mul-float v2, v14, v17

    mul-float v3, v10, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 293
    mul-float v2, v13, v17

    mul-float v3, v9, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 294
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    mul-float v3, v9, v17

    sub-float/2addr v2, v3

    mul-float v3, v9, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 297
    mul-float v2, v10, v17

    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v16

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 298
    mul-float v2, v9, v17

    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v16

    mul-float v4, v9, v17

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    mul-float v6, v10, v16

    sub-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 300
    mul-float v2, v9, v17

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->adjval0:I

    int-to-float v4, v4

    mul-float v4, v4, v16

    sub-float/2addr v3, v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 304
    mul-float v2, v10, v17

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->adjval0:I

    int-to-float v4, v4

    mul-float v4, v4, v16

    sub-float/2addr v3, v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 306
    mul-float v2, v11, v17

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->adjval0:I

    int-to-float v4, v4

    mul-float v4, v4, v16

    sub-float/2addr v3, v4

    mul-float v4, v11, v17

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    mul-float v6, v11, v16

    sub-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 309
    mul-float v2, v11, v17

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    mul-float v4, v10, v16

    sub-float/2addr v3, v4

    mul-float v4, v10, v17

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    mul-float v6, v10, v16

    sub-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 311
    mul-float v2, v9, v17

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    mul-float v4, v10, v16

    sub-float/2addr v3, v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 313
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_9

    .line 314
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 318
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x454fd000    # 3325.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 319
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x454fd000    # 3325.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 320
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x468ec600    # 18275.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 321
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x468ec600    # 18275.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 344
    .end local v19    # "path":Landroid/graphics/Path;
    .restart local v7    # "mtx":Landroid/graphics/Matrix;
    .restart local v20    # "rotatedBMP":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v15

    .line 345
    .local v15, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception while writing pictures to out file"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F

    .prologue
    .line 110
    invoke-super {p0, p3, p4, p7, p8}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IFF)V

    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mDrawOnCanvas:Z

    .line 112
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mLeftMarg:I

    .line 113
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mTopMarg:I

    .line 114
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 115
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 116
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 117
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mDrawOnCanvas:Z

    .line 118
    return-void
.end method

.method public drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p2, "shapeCount"    # I

    .prologue
    .line 101
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V

    .line 103
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->draw(I)V

    .line 104
    return-void
.end method

.method public drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 87
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;I)V

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mDrawOnCanvas:Z

    .line 89
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mLeftMarg:I

    .line 90
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mTopMarg:I

    .line 91
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    .line 92
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    .line 93
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 94
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 95
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mDrawOnCanvas:Z

    .line 97
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V
    .locals 0
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 39
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 40
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->draw(I)V

    .line 41
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V
    .locals 2
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I

    .prologue
    .line 45
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mDrawOnCanvas:Z

    .line 47
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mLeftMarg:I

    .line 48
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mTopMarg:I

    .line 49
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 50
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 51
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mDrawOnCanvas:Z

    .line 53
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 131
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 132
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mDrawOnCanvas:Z

    .line 133
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mLeftMarg:I

    .line 134
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mTopMarg:I

    .line 135
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 136
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 137
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 138
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mDrawOnCanvas:Z

    .line 139
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 143
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V

    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mDrawOnCanvas:Z

    .line 145
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mLeftMarg:I

    .line 146
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mTopMarg:I

    .line 147
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 148
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 149
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 150
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mDrawOnCanvas:Z

    .line 151
    return-void
.end method

.method public drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .prologue
    .line 125
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 126
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->draw(I)V

    .line 127
    return-void
.end method

.method public drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 58
    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p7

    move v4, p8

    move-object v5, p9

    invoke-super/range {v0 .. v5}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSSExcelShape(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mDrawOnCanvas:Z

    .line 61
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mLeftMarg:I

    .line 62
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mTopMarg:I

    .line 63
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 64
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 65
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mDrawOnCanvas:Z

    .line 67
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 71
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mDrawOnCanvas:Z

    .line 73
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mLeftMarg:I

    .line 74
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mTopMarg:I

    .line 75
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->width:F

    .line 76
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->height:F

    .line 77
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 78
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 79
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->mDrawOnCanvas:Z

    .line 81
    return-void
.end method
