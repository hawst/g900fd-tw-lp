.class public final Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
.super Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;
.source "XSLFSlide.java"


# instance fields
.field private chart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

.field private chartCatNum:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private chartCatStr:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field chartMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;",
            ">;"
        }
    .end annotation
.end field

.field private chartName:Ljava/lang/String;

.field private chartTxNum:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private chartTxStr:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private chartXVal:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private chartYVal:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public chartcnt:I

.field private chartnumCount:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected charts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;",
            ">;"
        }
    .end annotation
.end field

.field private chartstrCount:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected dataModels:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;",
            ">;"
        }
    .end annotation
.end field

.field protected diagrams:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;",
            ">;"
        }
    .end annotation
.end field

.field private fillRefId:Ljava/lang/String;

.field private graphicFrame:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;

.field private hyperlinks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlink;",
            ">;"
        }
    .end annotation
.end field

.field private isChart:Z

.field private slideLayout:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartcnt:I

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->hyperlinks:Ljava/util/ArrayList;

    .line 57
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartMap:Ljava/util/HashMap;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartCatStr:Ljava/util/ArrayList;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartCatNum:Ljava/util/ArrayList;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartTxStr:Ljava/util/ArrayList;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartTxNum:Ljava/util/ArrayList;

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartXVal:Ljava/util/ArrayList;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartYVal:Ljava/util/ArrayList;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartstrCount:Ljava/util/ArrayList;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartnumCount:Ljava/util/ArrayList;

    .line 73
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->dataModels:Ljava/util/HashMap;

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->diagrams:Ljava/util/HashMap;

    .line 81
    return-void
.end method

.method constructor <init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V
    .locals 1
    .param p1, "part"    # Lorg/apache/poi/openxml4j/opc/PackagePart;
    .param p2, "rel"    # Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 97
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;-><init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartcnt:I

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->hyperlinks:Ljava/util/ArrayList;

    .line 57
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartMap:Ljava/util/HashMap;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartCatStr:Ljava/util/ArrayList;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartCatNum:Ljava/util/ArrayList;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartTxStr:Ljava/util/ArrayList;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartTxNum:Ljava/util/ArrayList;

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartXVal:Ljava/util/ArrayList;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartYVal:Ljava/util/ArrayList;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartstrCount:Ljava/util/ArrayList;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartnumCount:Ljava/util/ArrayList;

    .line 73
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->dataModels:Ljava/util/HashMap;

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->diagrams:Ljava/util/HashMap;

    .line 98
    return-void
.end method

.method private initHyperlinks()V
    .locals 7

    .prologue
    .line 223
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v3

    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->HYPERLINK:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFRelation;->getRelation()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getRelationshipsByType(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 226
    .local v2, "relIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/openxml4j/opc/PackageRelationship;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 227
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    .line 228
    .local v1, "rel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->hyperlinks:Ljava/util/ArrayList;

    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlink;

    invoke-virtual {v1}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    move-result-object v6

    invoke-virtual {v6}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlink;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 231
    .end local v1    # "rel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .end local v2    # "relIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/openxml4j/opc/PackageRelationship;>;"
    :catch_0
    move-exception v0

    .line 232
    .local v0, "e":Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
    new-instance v3, Lorg/apache/poi/POIXMLException;

    invoke-direct {v3, v0}, Lorg/apache/poi/POIXMLException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 234
    .end local v0    # "e":Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
    .restart local v2    # "relIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/openxml4j/opc/PackageRelationship;>;"
    :cond_0
    return-void
.end method


# virtual methods
.method public addChartCatnum(Ljava/lang/String;)V
    .locals 1
    .param p1, "eximgprstname"    # Ljava/lang/String;

    .prologue
    .line 287
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartCatNum:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 288
    return-void
.end method

.method public addChartCatstr(Ljava/lang/String;)V
    .locals 1
    .param p1, "eximgprstname"    # Ljava/lang/String;

    .prologue
    .line 307
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartCatStr:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 308
    return-void
.end method

.method public addChartName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 263
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartName:Ljava/lang/String;

    .line 264
    return-void
.end method

.method public addChartNumCount(Ljava/lang/String;)V
    .locals 1
    .param p1, "cnt"    # Ljava/lang/String;

    .prologue
    .line 271
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartnumCount:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 272
    return-void
.end method

.method public addChartTxnum(Ljava/lang/String;)V
    .locals 1
    .param p1, "eximgprstname"    # Ljava/lang/String;

    .prologue
    .line 295
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartTxNum:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 296
    return-void
.end method

.method public addChartTxstr(Ljava/lang/String;)V
    .locals 1
    .param p1, "eximgprstname"    # Ljava/lang/String;

    .prologue
    .line 311
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartTxStr:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 312
    return-void
.end method

.method public addChartXVal(Ljava/lang/String;)V
    .locals 1
    .param p1, "eximgprstname"    # Ljava/lang/String;

    .prologue
    .line 319
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartXVal:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 320
    return-void
.end method

.method public addChartYVal(Ljava/lang/String;)V
    .locals 1
    .param p1, "eximgprstname"    # Ljava/lang/String;

    .prologue
    .line 327
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartYVal:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 328
    return-void
.end method

.method public addCharts(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)V
    .locals 1
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .prologue
    .line 255
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->charts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 256
    return-void
.end method

.method public addChartstrCount(Ljava/lang/String;)V
    .locals 1
    .param p1, "cnt"    # Ljava/lang/String;

    .prologue
    .line 279
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartstrCount:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 280
    return-void
.end method

.method protected buildCharts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;",
            ">;"
        }
    .end annotation

    .prologue
    .line 331
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 332
    .local v0, "shapes":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;>;"
    return-object v0
.end method

.method public getChartCatnum()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 283
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartCatNum:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getChartCatstr()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 299
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartCatStr:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getChartContents(Ljava/lang/String;)Lcom/samsung/thumbnail/customview/hslf/ChartContents;
    .locals 3
    .param p1, "relId"    # Ljava/lang/String;

    .prologue
    .line 198
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 199
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->parseRelations()V

    .line 201
    :cond_0
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .line 202
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    if-nez v2, :cond_1

    .line 203
    const/4 v1, 0x0

    .line 209
    :goto_0
    return-object v1

    .line 205
    :cond_1
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->parseChart()V

    .line 206
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;-><init>()V

    .line 207
    .local v0, "chartBuilder":Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->drawChartForCanvas(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    move-result-object v1

    .line 209
    .local v1, "chartContents":Lcom/samsung/thumbnail/customview/hslf/ChartContents;
    goto :goto_0
.end method

.method public getChartData(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;
    .locals 2
    .param p1, "relId"    # Ljava/lang/String;

    .prologue
    .line 167
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 168
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->parseRelations()V

    .line 170
    :cond_0
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .line 171
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    if-nez v1, :cond_1

    .line 172
    const/4 v1, 0x0

    .line 193
    :goto_0
    return-object v1

    .line 174
    :cond_1
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->parseChart()V

    .line 176
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getExChartName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->addChartName(Ljava/lang/String;)V

    .line 177
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getExChartNumval()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->addChartNumCount(Ljava/lang/String;)V

    .line 178
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getExChartStrnum()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->addChartstrCount(Ljava/lang/String;)V

    .line 180
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getChartCatNumVal()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 181
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getChartCatNumVal()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->addChartCatnum(Ljava/lang/String;)V

    .line 180
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 182
    :cond_2
    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getChartCatStrVal()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 183
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getChartCatStrVal()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->addChartCatstr(Ljava/lang/String;)V

    .line 182
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 184
    :cond_3
    const/4 v0, 0x0

    :goto_3
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getChartTxNumVal()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 185
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getChartTxNumVal()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->addChartTxnum(Ljava/lang/String;)V

    .line 184
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 186
    :cond_4
    const/4 v0, 0x0

    :goto_4
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getChartTxStrVal()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 187
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getChartTxStrVal()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->addChartTxstr(Ljava/lang/String;)V

    .line 186
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 188
    :cond_5
    const/4 v0, 0x0

    :goto_5
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getXVal()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_6

    .line 189
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getXVal()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->addChartXVal(Ljava/lang/String;)V

    .line 188
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 190
    :cond_6
    const/4 v0, 0x0

    :goto_6
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getYVal()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_7

    .line 191
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getYVal()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->addChartYVal(Ljava/lang/String;)V

    .line 190
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 193
    :cond_7
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    goto/16 :goto_0
.end method

.method public getChartList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;",
            ">;"
        }
    .end annotation

    .prologue
    .line 336
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->charts:Ljava/util/List;

    if-nez v0, :cond_0

    .line 337
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->buildCharts()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->charts:Ljava/util/List;

    .line 339
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->charts:Ljava/util/List;

    return-object v0
.end method

.method public getChartName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartName:Ljava/lang/String;

    return-object v0
.end method

.method public getChartNumCount()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 267
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartnumCount:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getChartTxnum()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 291
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartTxNum:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getChartTxstr()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 303
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartTxStr:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getChartXVal()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 315
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartXVal:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getChartYVal()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 323
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartYVal:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCharts()[Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;
    .locals 2

    .prologue
    .line 343
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getChartList()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->charts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    return-object v0
.end method

.method public getChartstrCount()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 275
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartstrCount:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getDiagramDataModelId(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;
    .locals 1
    .param p1, "dataModelId"    # Ljava/lang/String;

    .prologue
    .line 151
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->dataModels:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

    return-object v0
.end method

.method public getDiagramRelId(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;
    .locals 1
    .param p1, "diagramRelId"    # Ljava/lang/String;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->diagrams:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    return-object v0
.end method

.method public getFillRefid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->fillRefId:Ljava/lang/String;

    return-object v0
.end method

.method public getGraphicData()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->graphicFrame:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;

    return-object v0
.end method

.method public getHyperlinkByID(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlink;
    .locals 3
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 237
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->hyperlinks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 238
    .local v0, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlink;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 239
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlink;

    .line 240
    .local v1, "link":Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlink;
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlink;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 243
    .end local v1    # "link":Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlink;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getHyperlinks()[Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlink;
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->hyperlinks:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->hyperlinks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlink;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlink;

    return-object v0
.end method

.method public getIsChart()Z
    .locals 1

    .prologue
    .line 217
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->isChart:Z

    return v0
.end method

.method public getMasterSheet()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .locals 1

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v0

    return-object v0
.end method

.method protected getRootElementName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    const-string/jumbo v0, "sld"

    return-object v0
.end method

.method public getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->slideLayout:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    if-nez v0, :cond_0

    .line 160
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->parseRelations()V

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->slideLayout:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    return-object v0
.end method

.method public parseRelations()V
    .locals 7

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getRelations()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/POIXMLDocumentPart;

    .line 132
    .local v3, "p":Lorg/apache/poi/POIXMLDocumentPart;
    instance-of v5, v3, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    if-eqz v5, :cond_1

    .line 133
    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    .end local v3    # "p":Lorg/apache/poi/POIXMLDocumentPart;
    iput-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->slideLayout:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    goto :goto_0

    .line 134
    .restart local v3    # "p":Lorg/apache/poi/POIXMLDocumentPart;
    :cond_1
    instance-of v5, v3, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    if-eqz v5, :cond_2

    .line 135
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->setIsChart(Z)V

    move-object v4, v3

    .line 136
    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .line 138
    .local v4, "xssfChart":Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lorg/apache/poi/POIXMLDocumentPart;->getPackageRelationship()Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    iget v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartcnt:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->chartcnt:I

    goto :goto_0

    .line 140
    .end local v4    # "xssfChart":Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;
    :cond_2
    instance-of v5, v3, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

    if-eqz v5, :cond_3

    move-object v0, v3

    .line 141
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

    .line 142
    .local v0, "dataModel":Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->dataModels:Ljava/util/HashMap;

    invoke-virtual {v3}, Lorg/apache/poi/POIXMLDocumentPart;->getPackageRelationship()Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 143
    .end local v0    # "dataModel":Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;
    :cond_3
    instance-of v5, v3, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    if-eqz v5, :cond_0

    move-object v1, v3

    .line 144
    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    .line 145
    .local v1, "diagram":Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->diagrams:Ljava/util/HashMap;

    invoke-virtual {v3}, Lorg/apache/poi/POIXMLDocumentPart;->getPackageRelationship()Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 148
    .end local v1    # "diagram":Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;
    .end local v3    # "p":Lorg/apache/poi/POIXMLDocumentPart;
    :cond_4
    return-void
.end method

.method public parseSlide()V
    .locals 7

    .prologue
    .line 101
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/pptx/XPPTSlideParser;

    invoke-direct {v2, p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XPPTSlideParser;-><init>(Ljava/lang/Object;)V

    .line 102
    .local v2, "slideParser":Lcom/samsung/thumbnail/office/ooxml/pptx/XPPTSlideParser;
    const/4 v1, 0x0

    .line 105
    .local v1, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->initHyperlinks()V

    .line 106
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 107
    invoke-virtual {v2, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XPPTSlideParser;->parse(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    if-eqz v1, :cond_0

    .line 113
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 114
    :catch_0
    move-exception v0

    .line 115
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 108
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 109
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_2
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 111
    if-eqz v1, :cond_0

    .line 113
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 114
    :catch_2
    move-exception v0

    .line 115
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 111
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 113
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 116
    :cond_1
    :goto_1
    throw v3

    .line 114
    :catch_3
    move-exception v0

    .line 115
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v4, "DocumentService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setFillRefId(Ljava/lang/String;)V
    .locals 0
    .param p1, "fillRefId"    # Ljava/lang/String;

    .prologue
    .line 347
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->fillRefId:Ljava/lang/String;

    .line 348
    return-void
.end method

.method public setIsChart(Z)V
    .locals 0
    .param p1, "isChart"    # Z

    .prologue
    .line 213
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->isChart:Z

    .line 214
    return-void
.end method
