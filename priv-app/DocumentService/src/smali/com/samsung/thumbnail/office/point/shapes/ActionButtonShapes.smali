.class public Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;
.super Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;
.source "ActionButtonShapes.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mDrawOnCanvas:Z

.field private mLeftMarg:I

.field private mTopMarg:I


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "folderPath"    # Ljava/io/File;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;-><init>()V

    .line 33
    const-string/jumbo v0, "ActionButtonShapes"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->TAG:Ljava/lang/String;

    .line 39
    iput-object p1, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->folderPath:Ljava/io/File;

    .line 40
    return-void
.end method

.method private unifyWidthHeight()V
    .locals 3

    .prologue
    .line 46
    const/4 v0, 0x0

    .line 48
    .local v0, "dimension":I
    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->bitmapHight:I

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->bitmapWidth:I

    if-le v1, v2, :cond_0

    .line 49
    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->bitmapHight:I

    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->bitmapWidth:I

    .line 50
    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->height:F

    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->width:F

    .line 51
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->bitmapHight:I

    .line 58
    :goto_0
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->bitmap:Landroid/graphics/Bitmap;

    .line 60
    return-void

    .line 53
    :cond_0
    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->bitmapWidth:I

    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->bitmapHight:I

    .line 54
    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->width:F

    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->height:F

    .line 55
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->bitmapWidth:I

    goto :goto_0
.end method


# virtual methods
.method protected draw(Landroid/graphics/Canvas;I)V
    .locals 46
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeCount"    # I

    .prologue
    .line 154
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->width:F

    const v4, 0x46a8c000    # 21600.0f

    div-float v42, v3, v4

    .line 155
    .local v42, "mulFactor_Width":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->height:F

    const v4, 0x46a8c000    # 21600.0f

    div-float v41, v3, v4

    .line 157
    .local v41, "mulFactor_Height":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "ActionButtonHome"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 159
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->adjval0:I

    if-nez v3, :cond_0

    .line 160
    const/16 v3, 0x546

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->adjval0:I

    .line 165
    :cond_0
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->adjval0:I

    int-to-float v10, v3

    .line 166
    .local v10, "adj0":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->width:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->adjval0:I

    int-to-float v4, v4

    mul-float v4, v4, v42

    sub-float v11, v3, v4

    .line 167
    .local v11, "adj1":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->height:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->adjval0:I

    int-to-float v4, v4

    mul-float v4, v4, v41

    sub-float v12, v3, v4

    .line 169
    .local v12, "adj2":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->width:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float v33, v3, v4

    .line 170
    .local v33, "at3":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->height:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float v38, v3, v4

    .line 176
    .local v38, "at4":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->adjval0:I

    int-to-float v3, v3

    mul-float v3, v3, v41

    add-float v3, v3, v38

    const v4, 0x45fd2000    # 8100.0f

    mul-float v4, v4, v41

    sub-float v39, v3, v4

    .line 178
    .local v39, "at9":F
    const v3, 0x45fd2000    # 8100.0f

    mul-float v3, v3, v41

    add-float/2addr v3, v12

    sub-float v13, v3, v38

    .line 179
    .local v13, "at10":F
    mul-float v3, v10, v42

    add-float v3, v3, v33

    const v4, 0x45fd2000    # 8100.0f

    mul-float v4, v4, v42

    sub-float v14, v3, v4

    .line 183
    .local v14, "at11":F
    const v3, 0x45fd2000    # 8100.0f

    mul-float v3, v3, v42

    add-float/2addr v3, v11

    sub-float v15, v3, v33

    .line 184
    .local v15, "at12":F
    sub-float v16, v13, v39

    .line 185
    .local v16, "at13":F
    const/high16 v3, 0x41800000    # 16.0f

    div-float v17, v16, v3

    .line 186
    .local v17, "at14":F
    const/high16 v3, 0x41000000    # 8.0f

    div-float v18, v16, v3

    .line 187
    .local v18, "at15":F
    const/high16 v3, 0x40400000    # 3.0f

    mul-float v3, v3, v16

    const/high16 v4, 0x41800000    # 16.0f

    div-float v19, v3, v4

    .line 188
    .local v19, "at16":F
    const/high16 v3, 0x40a00000    # 5.0f

    mul-float v3, v3, v16

    const/high16 v4, 0x41800000    # 16.0f

    div-float v20, v3, v4

    .line 189
    .local v20, "at17":F
    const/high16 v3, 0x40e00000    # 7.0f

    mul-float v3, v3, v16

    const/high16 v4, 0x41800000    # 16.0f

    div-float v21, v3, v4

    .line 190
    .local v21, "at18":F
    const/high16 v3, 0x41100000    # 9.0f

    mul-float v3, v3, v16

    const/high16 v4, 0x41800000    # 16.0f

    div-float v22, v3, v4

    .line 191
    .local v22, "at19":F
    const/high16 v3, 0x41300000    # 11.0f

    mul-float v3, v3, v16

    const/high16 v4, 0x41800000    # 16.0f

    div-float v23, v3, v4

    .line 192
    .local v23, "at20":F
    const/high16 v3, 0x40400000    # 3.0f

    mul-float v3, v3, v16

    const/high16 v4, 0x40800000    # 4.0f

    div-float v24, v3, v4

    .line 193
    .local v24, "at21":F
    const/high16 v3, 0x41500000    # 13.0f

    mul-float v3, v3, v16

    const/high16 v4, 0x41800000    # 16.0f

    div-float v25, v3, v4

    .line 194
    .local v25, "at22":F
    const/high16 v3, 0x40e00000    # 7.0f

    mul-float v3, v3, v16

    const/high16 v4, 0x41000000    # 8.0f

    div-float v26, v3, v4

    .line 195
    .local v26, "at23":F
    add-float v27, v39, v17

    .line 196
    .local v27, "at24":F
    add-float v28, v39, v19

    .line 197
    .local v28, "at25":F
    add-float v29, v39, v20

    .line 198
    .local v29, "at26":F
    add-float v30, v39, v24

    .line 199
    .local v30, "at27":F
    add-float v31, v14, v18

    .line 200
    .local v31, "at28":F
    add-float v32, v14, v21

    .line 201
    .local v32, "at29":F
    add-float v34, v14, v22

    .line 202
    .local v34, "at30":F
    add-float v35, v14, v23

    .line 203
    .local v35, "at31":F
    add-float v36, v14, v25

    .line 204
    .local v36, "at32":F
    add-float v37, v14, v26

    .line 224
    .local v37, "at33":F
    new-instance v44, Landroid/graphics/Path;

    invoke-direct/range {v44 .. v44}, Landroid/graphics/Path;-><init>()V

    .line 227
    .local v44, "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, v44

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 228
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->height:F

    move-object/from16 v0, v44

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 230
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->width:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->height:F

    move-object/from16 v0, v44

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 232
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->width:F

    const/4 v4, 0x0

    move-object/from16 v0, v44

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 234
    invoke-virtual/range {v44 .. v44}, Landroid/graphics/Path;->close()V

    .line 236
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_1

    .line 237
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 238
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_2

    .line 239
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 241
    :cond_2
    new-instance v44, Landroid/graphics/Path;

    .end local v44    # "path":Landroid/graphics/Path;
    invoke-direct/range {v44 .. v44}, Landroid/graphics/Path;-><init>()V

    .line 243
    .restart local v44    # "path":Landroid/graphics/Path;
    mul-float v3, v10, v42

    mul-float v4, v10, v41

    move-object/from16 v0, v44

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 245
    mul-float v3, v10, v42

    move-object/from16 v0, v44

    invoke-virtual {v0, v3, v12}, Landroid/graphics/Path;->lineTo(FF)V

    .line 246
    move-object/from16 v0, v44

    invoke-virtual {v0, v11, v12}, Landroid/graphics/Path;->lineTo(FF)V

    .line 247
    mul-float v3, v10, v41

    move-object/from16 v0, v44

    invoke-virtual {v0, v11, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 248
    invoke-virtual/range {v44 .. v44}, Landroid/graphics/Path;->close()V

    .line 250
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_3

    .line 251
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 252
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_4

    .line 253
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 256
    :cond_4
    new-instance v44, Landroid/graphics/Path;

    .end local v44    # "path":Landroid/graphics/Path;
    invoke-direct/range {v44 .. v44}, Landroid/graphics/Path;-><init>()V

    .line 257
    .restart local v44    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, v44

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 258
    mul-float v3, v10, v42

    mul-float v4, v10, v41

    move-object/from16 v0, v44

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 259
    invoke-virtual/range {v44 .. v44}, Landroid/graphics/Path;->close()V

    .line 261
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_5

    .line 262
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 263
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_6

    .line 264
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 266
    :cond_6
    new-instance v44, Landroid/graphics/Path;

    .end local v44    # "path":Landroid/graphics/Path;
    invoke-direct/range {v44 .. v44}, Landroid/graphics/Path;-><init>()V

    .line 269
    .restart local v44    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->height:F

    move-object/from16 v0, v44

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 270
    mul-float v3, v10, v42

    move-object/from16 v0, v44

    invoke-virtual {v0, v3, v12}, Landroid/graphics/Path;->lineTo(FF)V

    .line 271
    invoke-virtual/range {v44 .. v44}, Landroid/graphics/Path;->close()V

    .line 273
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_7

    .line 274
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 275
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_8

    .line 276
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 278
    :cond_8
    new-instance v44, Landroid/graphics/Path;

    .end local v44    # "path":Landroid/graphics/Path;
    invoke-direct/range {v44 .. v44}, Landroid/graphics/Path;-><init>()V

    .line 280
    .restart local v44    # "path":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->width:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->height:F

    move-object/from16 v0, v44

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 283
    move-object/from16 v0, v44

    invoke-virtual {v0, v11, v12}, Landroid/graphics/Path;->lineTo(FF)V

    .line 284
    invoke-virtual/range {v44 .. v44}, Landroid/graphics/Path;->close()V

    .line 286
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_9

    .line 287
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 288
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_a

    .line 289
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 291
    :cond_a
    new-instance v44, Landroid/graphics/Path;

    .end local v44    # "path":Landroid/graphics/Path;
    invoke-direct/range {v44 .. v44}, Landroid/graphics/Path;-><init>()V

    .line 293
    .restart local v44    # "path":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->width:F

    const/4 v4, 0x0

    move-object/from16 v0, v44

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 295
    mul-float v3, v10, v41

    move-object/from16 v0, v44

    invoke-virtual {v0, v11, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 297
    invoke-virtual/range {v44 .. v44}, Landroid/graphics/Path;->close()V

    .line 299
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_b

    .line 300
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 301
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_c

    .line 302
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 304
    :cond_c
    new-instance v44, Landroid/graphics/Path;

    .end local v44    # "path":Landroid/graphics/Path;
    invoke-direct/range {v44 .. v44}, Landroid/graphics/Path;-><init>()V

    .line 307
    .restart local v44    # "path":Landroid/graphics/Path;
    move-object/from16 v0, v44

    move/from16 v1, v33

    move/from16 v2, v39

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 308
    move-object/from16 v0, v44

    move/from16 v1, v38

    invoke-virtual {v0, v14, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 309
    move-object/from16 v0, v44

    move/from16 v1, v31

    move/from16 v2, v38

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 310
    move-object/from16 v0, v44

    move/from16 v1, v31

    invoke-virtual {v0, v1, v13}, Landroid/graphics/Path;->lineTo(FF)V

    .line 311
    move-object/from16 v0, v44

    move/from16 v1, v37

    invoke-virtual {v0, v1, v13}, Landroid/graphics/Path;->lineTo(FF)V

    .line 312
    move-object/from16 v0, v44

    move/from16 v1, v37

    move/from16 v2, v38

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 314
    move-object/from16 v0, v44

    move/from16 v1, v38

    invoke-virtual {v0, v15, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 315
    move-object/from16 v0, v44

    move/from16 v1, v36

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 316
    move-object/from16 v0, v44

    move/from16 v1, v36

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 317
    move-object/from16 v0, v44

    move/from16 v1, v35

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 318
    move-object/from16 v0, v44

    move/from16 v1, v35

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 319
    invoke-virtual/range {v44 .. v44}, Landroid/graphics/Path;->close()V

    .line 321
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_d

    .line 322
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 323
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_e

    .line 324
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 327
    :cond_e
    new-instance v44, Landroid/graphics/Path;

    .end local v44    # "path":Landroid/graphics/Path;
    invoke-direct/range {v44 .. v44}, Landroid/graphics/Path;-><init>()V

    .line 328
    .restart local v44    # "path":Landroid/graphics/Path;
    move-object/from16 v0, v44

    move/from16 v1, v35

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 329
    move-object/from16 v0, v44

    move/from16 v1, v36

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 330
    invoke-virtual/range {v44 .. v44}, Landroid/graphics/Path;->close()V

    .line 332
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_f

    .line 333
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 334
    :cond_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_10

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_10

    .line 335
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 337
    :cond_10
    new-instance v44, Landroid/graphics/Path;

    .end local v44    # "path":Landroid/graphics/Path;
    invoke-direct/range {v44 .. v44}, Landroid/graphics/Path;-><init>()V

    .line 339
    .restart local v44    # "path":Landroid/graphics/Path;
    move-object/from16 v0, v44

    move/from16 v1, v31

    move/from16 v2, v38

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 340
    move-object/from16 v0, v44

    move/from16 v1, v37

    move/from16 v2, v38

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 341
    invoke-virtual/range {v44 .. v44}, Landroid/graphics/Path;->close()V

    .line 342
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_11

    .line 343
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 344
    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_12

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_12

    .line 345
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 347
    :cond_12
    new-instance v44, Landroid/graphics/Path;

    .end local v44    # "path":Landroid/graphics/Path;
    invoke-direct/range {v44 .. v44}, Landroid/graphics/Path;-><init>()V

    .line 349
    .restart local v44    # "path":Landroid/graphics/Path;
    move-object/from16 v0, v44

    move/from16 v1, v32

    invoke-virtual {v0, v1, v13}, Landroid/graphics/Path;->moveTo(FF)V

    .line 350
    move-object/from16 v0, v44

    move/from16 v1, v32

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 351
    move-object/from16 v0, v44

    move/from16 v1, v34

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 352
    move-object/from16 v0, v44

    move/from16 v1, v34

    invoke-virtual {v0, v1, v13}, Landroid/graphics/Path;->lineTo(FF)V

    .line 353
    invoke-virtual/range {v44 .. v44}, Landroid/graphics/Path;->close()V

    .line 355
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_13

    .line 356
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 357
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_14

    .line 358
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 361
    :cond_14
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 362
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 363
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 364
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    .line 369
    .end local v10    # "adj0":F
    .end local v11    # "adj1":F
    .end local v12    # "adj2":F
    .end local v13    # "at10":F
    .end local v14    # "at11":F
    .end local v15    # "at12":F
    .end local v16    # "at13":F
    .end local v17    # "at14":F
    .end local v18    # "at15":F
    .end local v19    # "at16":F
    .end local v20    # "at17":F
    .end local v21    # "at18":F
    .end local v22    # "at19":F
    .end local v23    # "at20":F
    .end local v24    # "at21":F
    .end local v25    # "at22":F
    .end local v26    # "at23":F
    .end local v27    # "at24":F
    .end local v28    # "at25":F
    .end local v29    # "at26":F
    .end local v30    # "at27":F
    .end local v31    # "at28":F
    .end local v32    # "at29":F
    .end local v33    # "at3":F
    .end local v34    # "at30":F
    .end local v35    # "at31":F
    .end local v36    # "at32":F
    .end local v37    # "at33":F
    .end local v38    # "at4":F
    .end local v39    # "at9":F
    .end local v44    # "path":Landroid/graphics/Path;
    :cond_15
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mDrawOnCanvas:Z

    if-nez v3, :cond_16

    .line 370
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 373
    .local v8, "mtx":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->rotation:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->width:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->height:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    invoke-virtual {v8, v3, v4, v5}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 375
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->bitmap:Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->bitmapWidth:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->bitmapHight:I

    const/4 v9, 0x1

    invoke-static/range {v3 .. v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v45

    .line 380
    .local v45, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v43, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->folderPath:Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "pic_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->shapeName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ".png"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v43

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 383
    .local v43, "out":Ljava/io/FileOutputStream;
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    move-object/from16 v0, v45

    move-object/from16 v1, v43

    invoke-virtual {v0, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 384
    invoke-virtual/range {v43 .. v43}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 390
    .end local v8    # "mtx":Landroid/graphics/Matrix;
    .end local v43    # "out":Ljava/io/FileOutputStream;
    .end local v45    # "rotatedBMP":Landroid/graphics/Bitmap;
    :cond_16
    :goto_0
    return-void

    .line 385
    .restart local v8    # "mtx":Landroid/graphics/Matrix;
    .restart local v45    # "rotatedBMP":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v40

    .line 386
    .local v40, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception while writing pictures to out file"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v40

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p2, "shapeCount"    # I

    .prologue
    .line 108
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V

    .line 109
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->unifyWidthHeight()V

    .line 110
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->draw(I)V

    .line 111
    return-void
.end method

.method public drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;I)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .param p2, "shapeCount"    # I

    .prologue
    .line 147
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;I)V

    .line 148
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->unifyWidthHeight()V

    .line 149
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->draw(I)V

    .line 150
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V
    .locals 0
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 66
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 67
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->unifyWidthHeight()V

    .line 68
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->draw(I)V

    .line 69
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V
    .locals 2
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I

    .prologue
    .line 73
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 74
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->unifyWidthHeight()V

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mDrawOnCanvas:Z

    .line 76
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mLeftMarg:I

    .line 77
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mTopMarg:I

    .line 78
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 79
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 80
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 81
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mDrawOnCanvas:Z

    .line 82
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 123
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 124
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mDrawOnCanvas:Z

    .line 125
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mLeftMarg:I

    .line 126
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mTopMarg:I

    .line 127
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 128
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 129
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 130
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mDrawOnCanvas:Z

    .line 131
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 135
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V

    .line 136
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mDrawOnCanvas:Z

    .line 137
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mLeftMarg:I

    .line 138
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mTopMarg:I

    .line 139
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 140
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 141
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 142
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mDrawOnCanvas:Z

    .line 143
    return-void
.end method

.method public drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .prologue
    .line 116
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 117
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->unifyWidthHeight()V

    .line 118
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->draw(I)V

    .line 119
    return-void
.end method

.method public drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 87
    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p7

    move/from16 v4, p8

    move-object/from16 v5, p9

    invoke-super/range {v0 .. v5}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSSExcelShape(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 88
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mLeftMarg:I

    .line 89
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mTopMarg:I

    .line 90
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mDrawOnCanvas:Z

    .line 92
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iput-wide v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->lineWidth:D

    .line 93
    invoke-virtual/range {p9 .. p9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    move-result-object v6

    .line 94
    .local v6, "lineProp":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    if-eqz v6, :cond_0

    .line 95
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getLineWidth()Ljava/lang/String;

    move-result-object v7

    .line 96
    .local v7, "lnWidth":Ljava/lang/String;
    if-eqz v7, :cond_0

    .line 97
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints(I)I

    move-result v0

    int-to-double v0, v0

    iput-wide v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->lineWidth:D

    .line 100
    .end local v7    # "lnWidth":Ljava/lang/String;
    :cond_0
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 101
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 102
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 103
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->mDrawOnCanvas:Z

    .line 104
    return-void
.end method
