.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "BlipHandler.java"


# instance fields
.field private blip:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;)V
    .locals 2
    .param p1, "blip"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;

    .prologue
    .line 20
    const/16 v0, 0x1f

    const-string/jumbo v1, "blip"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 21
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipHandler;->blip:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;

    .line 22
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipHandler;->blip:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;

    if-eqz v0, :cond_0

    .line 46
    const-string/jumbo v0, "r:embed"

    invoke-interface {p3, v0}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 48
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipHandler;->blip:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;

    const-string/jumbo v1, "r:embed"

    invoke-interface {p3, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;->setBlipEmbed(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 60
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    const-string/jumbo v0, "a:embed"

    invoke-interface {p3, v0}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipHandler;->blip:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;

    const-string/jumbo v1, "a:embed"

    invoke-interface {p3, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;->setBlipEmbed(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    goto :goto_0
.end method
