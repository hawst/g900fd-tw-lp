.class public Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
.super Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;
.source "XSLFAutoShape.java"


# instance fields
.field private isGroup:Z

.field private shapeLst:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;)V
    .locals 0
    .param p1, "shapeProperties"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "sheet"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;)V

    .line 42
    return-void
.end method


# virtual methods
.method public addShapes(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;)V
    .locals 1
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->shapeLst:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->shapeLst:Ljava/util/ArrayList;

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->shapeLst:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    return-void
.end method

.method public getShapes()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->shapeLst:Ljava/util/ArrayList;

    return-object v0
.end method

.method public isGroup()Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->isGroup:Z

    return v0
.end method

.method public processGroupShapes()V
    .locals 38

    .prologue
    .line 64
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->shapeLst:Ljava/util/ArrayList;

    move-object/from16 v35, v0

    if-nez v35, :cond_1

    .line 130
    :cond_0
    return-void

    .line 68
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->shapeLst:Ljava/util/ArrayList;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Ljava/util/ArrayList;->size()I

    move-result v26

    .line 69
    .local v26, "size":I
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    move/from16 v0, v26

    if-ge v12, v0, :cond_0

    .line 70
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v9

    .line 71
    .local v9, "groupShapeProp":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->shapeLst:Ljava/util/ArrayList;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;

    invoke-virtual/range {v35 .. v35}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v13

    .line 73
    .local v13, "shapeProp":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getActualShapeHeight()J

    move-result-wide v18

    .line 74
    .local v18, "shapeHt":J
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getActualShapeWidth()J

    move-result-wide v20

    .line 76
    .local v20, "shapeWt":J
    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getActualShapeHeight()J

    move-result-wide v10

    .line 77
    .local v10, "ht":J
    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getActualShapeWidth()J

    move-result-wide v28

    .line 79
    .local v28, "wd":J
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getChildShW()J

    move-result-wide v36

    sub-long v16, v36, v28

    .line 80
    .local v16, "modifiedWd":J
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getChildShH()J

    move-result-wide v36

    sub-long v14, v36, v10

    .line 82
    .local v14, "modifiedHt":J
    add-long v16, v16, v28

    .line 83
    add-long/2addr v14, v10

    .line 85
    const-wide/16 v36, 0x0

    cmp-long v35, v14, v36

    if-gtz v35, :cond_2

    .line 86
    const-wide/16 v14, 0x1

    .line 87
    :cond_2
    const-wide/16 v36, 0x0

    cmp-long v35, v16, v36

    if-gtz v35, :cond_3

    .line 88
    const-wide/16 v16, 0x1

    .line 90
    :cond_3
    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v35, v0

    move-wide/from16 v0, v16

    long-to-float v0, v0

    move/from16 v36, v0

    div-float v27, v35, v36

    .line 91
    .local v27, "xOffset":F
    move-wide/from16 v0, v18

    long-to-float v0, v0

    move/from16 v35, v0

    long-to-float v0, v14

    move/from16 v36, v0

    div-float v34, v35, v36

    .line 93
    .local v34, "yOffset":F
    const/16 v35, 0x0

    cmpg-float v35, v27, v35

    if-gtz v35, :cond_4

    .line 94
    const/high16 v27, 0x3f800000    # 1.0f

    .line 96
    :cond_4
    const/16 v35, 0x0

    cmpg-float v35, v34, v35

    if-gtz v35, :cond_5

    .line 97
    const/high16 v34, 0x3f800000    # 1.0f

    .line 99
    :cond_5
    move-wide/from16 v0, v28

    long-to-float v0, v0

    move/from16 v35, v0

    mul-float v35, v35, v27

    move/from16 v0, v35

    float-to-long v0, v0

    move-wide/from16 v28, v0

    .line 100
    long-to-float v0, v10

    move/from16 v35, v0

    mul-float v35, v35, v34

    move/from16 v0, v35

    float-to-long v10, v0

    .line 102
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeXvalue()J

    move-result-wide v36

    move-wide/from16 v0, v36

    long-to-double v0, v0

    move-wide/from16 v36, v0

    invoke-static/range {v36 .. v37}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->toEMU(D)I

    move-result v35

    move/from16 v0, v35

    int-to-long v0, v0

    move-wide/from16 v22, v0

    .line 103
    .local v22, "shapeX":J
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeYvalue()J

    move-result-wide v36

    move-wide/from16 v0, v36

    long-to-double v0, v0

    move-wide/from16 v36, v0

    invoke-static/range {v36 .. v37}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->toEMU(D)I

    move-result v35

    move/from16 v0, v35

    int-to-long v0, v0

    move-wide/from16 v24, v0

    .line 105
    .local v24, "shapeY":J
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getChildX()J

    move-result-wide v4

    .line 106
    .local v4, "chX":J
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getChildY()J

    move-result-wide v6

    .line 108
    .local v6, "chY":J
    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeXvalue()J

    move-result-wide v36

    move-wide/from16 v0, v36

    long-to-double v0, v0

    move-wide/from16 v36, v0

    invoke-static/range {v36 .. v37}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->toEMU(D)I

    move-result v35

    move/from16 v0, v35

    int-to-long v0, v0

    move-wide/from16 v30, v0

    .line 109
    .local v30, "x":J
    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeYvalue()J

    move-result-wide v36

    move-wide/from16 v0, v36

    long-to-double v0, v0

    move-wide/from16 v36, v0

    invoke-static/range {v36 .. v37}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->toEMU(D)I

    move-result v35

    move/from16 v0, v35

    int-to-long v0, v0

    move-wide/from16 v32, v0

    .line 111
    .local v32, "y":J
    sub-long v36, v30, v4

    add-long v30, v36, v22

    .line 112
    sub-long v36, v32, v6

    add-long v32, v36, v24

    .line 114
    move-wide/from16 v0, v28

    invoke-virtual {v13, v0, v1, v10, v11}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setSize(JJ)V

    .line 115
    move-wide/from16 v0, v30

    move-wide/from16 v2, v32

    invoke-virtual {v13, v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setPoints(JJ)V

    .line 119
    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v8

    .line 120
    .local v8, "fillProp":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    if-nez v8, :cond_7

    .line 121
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->consumeFill(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V

    .line 126
    :goto_1
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getRotation()I

    move-result v35

    if-eqz v35, :cond_6

    .line 127
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getRotation()I

    move-result v35

    move/from16 v0, v35

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setRotation(I)V

    .line 69
    :cond_6
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_0

    .line 123
    :cond_7
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v8, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->merge(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V

    goto :goto_1
.end method

.method public setIsGroupshape()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->isGroup:Z

    .line 46
    return-void
.end method
