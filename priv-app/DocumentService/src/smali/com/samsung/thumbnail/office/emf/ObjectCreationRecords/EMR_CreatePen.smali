.class public Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;
.super Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;
.source "EMR_CreatePen.java"


# instance fields
.field private Color:I

.field private PenStyle:I

.field private Size:I

.field private Type:I

.field private Width:I

.field fileContent:[B

.field private ihPen:I

.field private offset:I


# direct methods
.method public constructor <init>([BI)V
    .locals 0
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->fileContent:[B

    .line 19
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->offset:I

    .line 20
    return-void
.end method


# virtual methods
.method public getColor()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->Color:I

    return v0
.end method

.method public getIhPen()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->ihPen:I

    return v0
.end method

.method public getPenStyle()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->PenStyle:I

    return v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->Size:I

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->Type:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->Width:I

    return v0
.end method

.method public readEmfCreatePen()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x0

    .line 71
    const/4 v4, 0x0

    .line 73
    .local v4, "i":I
    const/4 v8, 0x4

    new-array v6, v8, [B

    .line 75
    .local v6, "intConvert":[B
    const/4 v4, 0x0

    :goto_0
    if-gt v4, v12, :cond_0

    .line 76
    rsub-int/lit8 v8, v4, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->offset:I

    add-int/2addr v10, v4

    aget-byte v9, v9, v10

    aput-byte v9, v6, v8

    .line 75
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 77
    :cond_0
    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v3

    .line 78
    .local v3, "Type":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->offset:I

    add-int/2addr v8, v4

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->offset:I

    .line 79
    invoke-virtual {p0, v3}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->setType(I)V

    .line 80
    invoke-virtual {p0, v3}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->setObjectType(I)V

    .line 82
    const/4 v4, 0x0

    :goto_1
    if-gt v4, v12, :cond_1

    .line 83
    rsub-int/lit8 v8, v4, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->offset:I

    add-int/2addr v10, v4

    aget-byte v9, v9, v10

    aput-byte v9, v6, v8

    .line 82
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 84
    :cond_1
    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v2

    .line 85
    .local v2, "Size":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->offset:I

    add-int/2addr v8, v4

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->offset:I

    .line 86
    invoke-virtual {p0, v2}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->setSize(I)V

    .line 88
    const/4 v4, 0x0

    :goto_2
    if-gt v4, v12, :cond_2

    .line 89
    rsub-int/lit8 v8, v4, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->offset:I

    add-int/2addr v10, v4

    aget-byte v9, v9, v10

    aput-byte v9, v6, v8

    .line 88
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 90
    :cond_2
    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v5

    .line 91
    .local v5, "ihPen":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->offset:I

    add-int/2addr v8, v4

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->offset:I

    .line 92
    invoke-virtual {p0, v5}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->setIhPen(I)V

    .line 93
    invoke-virtual {p0, v5}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->sethandleNumber(I)V

    .line 95
    const/4 v4, 0x0

    :goto_3
    if-gt v4, v12, :cond_3

    .line 96
    rsub-int/lit8 v8, v4, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->offset:I

    add-int/2addr v10, v4

    aget-byte v9, v9, v10

    aput-byte v9, v6, v8

    .line 95
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 97
    :cond_3
    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 98
    .local v1, "PenStyle":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->offset:I

    add-int/2addr v8, v4

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->offset:I

    .line 99
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->setPenStyle(I)V

    .line 101
    const/4 v4, 0x0

    :goto_4
    if-gt v4, v12, :cond_4

    .line 102
    rsub-int/lit8 v8, v4, 0x3

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->offset:I

    add-int/2addr v10, v4

    aget-byte v9, v9, v10

    aput-byte v9, v6, v8

    .line 101
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 103
    :cond_4
    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v7

    .line 104
    .local v7, "width":I
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->offset:I

    add-int/2addr v8, v4

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->offset:I

    .line 105
    int-to-float v8, v7

    invoke-static {v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v8

    float-to-int v8, v8

    invoke-virtual {p0, v8}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->setWidth(I)V

    .line 107
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->offset:I

    add-int/lit8 v8, v8, 0x4

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->offset:I

    .line 109
    const/4 v4, 0x0

    :goto_5
    const/4 v8, 0x2

    if-gt v4, v8, :cond_5

    .line 110
    add-int/lit8 v8, v4, 0x1

    iget-object v9, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->fileContent:[B

    iget v10, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->offset:I

    add-int/2addr v10, v4

    aget-byte v9, v9, v10

    aput-byte v9, v6, v8

    .line 109
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 111
    :cond_5
    aput-byte v11, v6, v11

    .line 112
    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 113
    .local v0, "Color":I
    add-int/lit8 v4, v4, 0x1

    .line 114
    iget v8, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->offset:I

    add-int/2addr v8, v4

    iput v8, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->offset:I

    .line 115
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->setColor(I)V

    .line 116
    return-void
.end method

.method public setColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->Color:I

    .line 68
    return-void
.end method

.method public setIhPen(I)V
    .locals 0
    .param p1, "ihPen"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->ihPen:I

    .line 44
    return-void
.end method

.method public setPenStyle(I)V
    .locals 0
    .param p1, "penStyle"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->PenStyle:I

    .line 52
    return-void
.end method

.method public setSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 35
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->Size:I

    .line 36
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->Type:I

    .line 28
    return-void
.end method

.method public setWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 59
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->Width:I

    .line 60
    return-void
.end method
