.class public Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFUtilities;
.super Ljava/lang/Object;
.source "WMFUtilities.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Utils"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decodeString(Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFFont;[B)Ljava/lang/String;
    .locals 4
    .param p0, "wmfFont"    # Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFFont;
    .param p1, "bstr"    # [B

    .prologue
    .line 13
    if-eqz p0, :cond_0

    .line 14
    :try_start_0
    iget v1, p0, Lcom/samsung/thumbnail/office/wmf/WmfGlobals/WMFFont;->charset:I
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    sparse-switch v1, :sswitch_data_0

    .line 54
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1}, Ljava/lang/String;-><init>([B)V

    :goto_1
    return-object v1

    .line 16
    :sswitch_0
    :try_start_1
    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "ISO-8859-1"

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 50
    :catch_0
    move-exception v0

    .line 51
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    const-string/jumbo v1, "Utils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "ERROR: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 18
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    :sswitch_1
    :try_start_2
    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "US-ASCII"

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    goto :goto_1

    .line 20
    :sswitch_2
    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "Shift_JIS"

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    goto :goto_1

    .line 22
    :sswitch_3
    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "cp949"

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    goto :goto_1

    .line 24
    :sswitch_4
    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "x-Johab"

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    goto :goto_1

    .line 26
    :sswitch_5
    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "GB2312"

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    goto :goto_1

    .line 28
    :sswitch_6
    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "Big5"

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    goto :goto_1

    .line 30
    :sswitch_7
    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "windows-1253"

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    goto :goto_1

    .line 32
    :sswitch_8
    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "cp1254"

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    goto :goto_1

    .line 34
    :sswitch_9
    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "cp1258"

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    goto :goto_1

    .line 36
    :sswitch_a
    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "windows-1255"

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    goto/16 :goto_1

    .line 38
    :sswitch_b
    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "windows-1256"

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    goto/16 :goto_1

    .line 40
    :sswitch_c
    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "windows-1251"

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    goto/16 :goto_1

    .line 42
    :sswitch_d
    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "cp874"

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    goto/16 :goto_1

    .line 44
    :sswitch_e
    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "cp1250"

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    goto/16 :goto_1

    .line 46
    :sswitch_f
    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "cp437"

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    .line 14
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x80 -> :sswitch_2
        0x81 -> :sswitch_3
        0x82 -> :sswitch_4
        0x86 -> :sswitch_5
        0x88 -> :sswitch_6
        0xa1 -> :sswitch_7
        0xa2 -> :sswitch_8
        0xa3 -> :sswitch_9
        0xb1 -> :sswitch_a
        0xb2 -> :sswitch_b
        0xcc -> :sswitch_c
        0xde -> :sswitch_d
        0xee -> :sswitch_e
        0xff -> :sswitch_f
    .end sparse-switch
.end method

.method public static getHorizontalAlignment(I)I
    .locals 3
    .param p0, "align"    # I

    .prologue
    const/4 v1, 0x6

    const/4 v2, 0x2

    .line 58
    move v0, p0

    .line 59
    .local v0, "v":I
    rem-int/lit8 v0, v0, 0x18

    .line 60
    rem-int/lit8 v0, v0, 0x8

    .line 61
    if-lt v0, v1, :cond_0

    .line 65
    :goto_0
    return v1

    .line 63
    :cond_0
    if-lt v0, v2, :cond_1

    move v1, v2

    .line 64
    goto :goto_0

    .line 65
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getVerticalAlignment(I)I
    .locals 2
    .param p0, "align"    # I

    .prologue
    .line 69
    move v0, p0

    .line 70
    .local v0, "v":I
    div-int/lit8 v1, v0, 0x18

    if-eqz v1, :cond_0

    .line 71
    const/16 v1, 0x18

    .line 75
    :goto_0
    return v1

    .line 72
    :cond_0
    rem-int/lit8 v0, v0, 0x18

    .line 73
    div-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_1

    .line 74
    const/16 v1, 0x8

    goto :goto_0

    .line 75
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
