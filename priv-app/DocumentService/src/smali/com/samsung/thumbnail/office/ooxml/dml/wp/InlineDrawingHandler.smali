.class public Lcom/samsung/thumbnail/office/ooxml/dml/wp/InlineDrawingHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "InlineDrawingHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDocPrHandler$IDocPrObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/wp/EffectExtentHandler$IEffectExtentObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/wp/ExtentHandler$IExtentObserver;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 25
    const/16 v0, 0x1e

    const-string/jumbo v1, "inline"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 26
    return-void
.end method

.method private init()V
    .locals 11

    .prologue
    const/16 v10, 0x1f

    .line 29
    const/4 v9, 0x4

    new-array v8, v9, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 31
    .local v8, "seqDescriptors":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/wp/ExtentHandler;

    invoke-direct {v4, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/ExtentHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/wp/ExtentHandler$IExtentObserver;)V

    .line 32
    .local v4, "extentHandler":Lcom/samsung/thumbnail/office/ooxml/dml/wp/ExtentHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/wp/WPDMLSeqDescriptor;

    const-string/jumbo v9, "extent"

    invoke-direct {v5, v9, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/WPDMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 34
    .local v5, "extentSeq":Lcom/samsung/thumbnail/office/ooxml/dml/wp/WPDMLSeqDescriptor;
    const/4 v9, 0x0

    aput-object v5, v8, v9

    .line 36
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/wp/EffectExtentHandler;

    invoke-direct {v2, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/EffectExtentHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/wp/EffectExtentHandler$IEffectExtentObserver;)V

    .line 37
    .local v2, "effectExtent":Lcom/samsung/thumbnail/office/ooxml/dml/wp/EffectExtentHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/wp/WPDMLSeqDescriptor;

    const-string/jumbo v9, "effectExtent"

    invoke-direct {v3, v9, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/WPDMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 39
    .local v3, "effectExtentSeq":Lcom/samsung/thumbnail/office/ooxml/dml/wp/WPDMLSeqDescriptor;
    const/4 v9, 0x1

    aput-object v3, v8, v9

    .line 41
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GraphicHandler;

    invoke-direct {v7}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GraphicHandler;-><init>()V

    .line 42
    .local v7, "graphicHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GraphicHandler;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    const-string/jumbo v9, "graphic"

    invoke-direct {v6, v10, v9, v7}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 45
    .local v6, "graphic":Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    const/4 v9, 0x2

    aput-object v6, v8, v9

    .line 47
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDocPrHandler;

    invoke-direct {v1, p0}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDocPrHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDocPrHandler$IDocPrObserver;)V

    .line 48
    .local v1, "docPrHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDocPrHandler;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    const-string/jumbo v9, "graphic"

    invoke-direct {v0, v10, v9, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 51
    .local v0, "docPr":Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    const/4 v9, 0x3

    aput-object v0, v8, v9

    .line 53
    iput-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/InlineDrawingHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 54
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 68
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 69
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;

    invoke-interface {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;->endPicture()V

    .line 70
    return-void
.end method

.method public setDocPr(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 84
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->setDocPr(ILjava/lang/String;)V

    .line 85
    return-void
.end method

.method public setEffectExtent(IIIILcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p1, "l"    # I
    .param p2, "r"    # I
    .param p3, "b"    # I
    .param p4, "t"    # I
    .param p5, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 79
    invoke-virtual {p5}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->setEffectExtent(IIII)V

    .line 80
    return-void
.end method

.method public setExtent(IILcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p1, "cx"    # I
    .param p2, "cy"    # I
    .param p3, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 74
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->setPictureExtent(II)V

    .line 75
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 59
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 60
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/InlineDrawingHandler;->init()V

    .line 61
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;

    invoke-interface {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;->startPicture()V

    .line 62
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;

    const-string/jumbo v1, "inline"

    invoke-interface {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;->setPictureType(Ljava/lang/String;)V

    .line 64
    return-void
.end method
