.class public Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;
.super Ljava/lang/Object;
.source "GdiObject.java"


# instance fields
.field id:I

.field obj:Ljava/lang/Object;

.field type:I

.field used:Z


# direct methods
.method constructor <init>(IZ)V
    .locals 1
    .param p1, "_id"    # I
    .param p2, "_used"    # Z

    .prologue
    const/4 v0, 0x0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->type:I

    .line 10
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->id:I

    .line 11
    iput-boolean p2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->used:Z

    .line 12
    iput v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->type:I

    .line 13
    return-void
.end method


# virtual methods
.method public Setup(ILjava/lang/Object;)V
    .locals 1
    .param p1, "_type"    # I
    .param p2, "_obj"    # Ljava/lang/Object;

    .prologue
    .line 21
    iput-object p2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->obj:Ljava/lang/Object;

    .line 22
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->type:I

    .line 23
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->used:Z

    .line 24
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16
    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->used:Z

    .line 17
    iput v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->type:I

    .line 18
    return-void
.end method

.method public getID()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->id:I

    return v0
.end method

.method public getObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->obj:Ljava/lang/Object;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->type:I

    return v0
.end method

.method public isUsed()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->used:Z

    return v0
.end method
