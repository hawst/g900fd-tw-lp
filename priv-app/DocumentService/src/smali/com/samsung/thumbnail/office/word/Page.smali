.class public Lcom/samsung/thumbnail/office/word/Page;
.super Lcom/samsung/thumbnail/customview/CanvasElementPart;
.source "Page.java"


# instance fields
.field private bottomBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

.field private elementList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/CanvasElementPart;",
            ">;"
        }
    .end annotation
.end field

.field private isCoverPage:Z

.field private leftBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

.field private mBackgroundColor:I

.field private mBottomMargin:F

.field private mFooterStartPoint:F

.field private mHeaderStartPoint:F

.field private mLeftMargin:F

.field private mRightMargin:F

.field private mTopMargin:F

.field private pgBorDisplay:Ljava/lang/String;

.field private pgBorOffset:Ljava/lang/String;

.field private rightBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

.field private topBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/CanvasElementPart;-><init>()V

    .line 20
    const-string/jumbo v0, "#ffffff"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/thumbnail/office/word/Page;->mBackgroundColor:I

    .line 34
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/word/Page;->init()V

    .line 35
    return-void
.end method

.method private init()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/thumbnail/office/word/Page;->mBackgroundColor:I

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/word/Page;->elementList:Ljava/util/ArrayList;

    .line 41
    return-void
.end method


# virtual methods
.method public addElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V
    .locals 1
    .param p1, "docPara"    # Lcom/samsung/thumbnail/customview/CanvasElementPart;

    .prologue
    .line 163
    iget-object v0, p0, Lcom/samsung/thumbnail/office/word/Page;->elementList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    return-void
.end method

.method public getBackgroundColor()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/samsung/thumbnail/office/word/Page;->mBackgroundColor:I

    return v0
.end method

.method public getBottomBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/thumbnail/office/word/Page;->bottomBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    return-object v0
.end method

.method public getBottomMargin()F
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/samsung/thumbnail/office/word/Page;->mBottomMargin:F

    return v0
.end method

.method public getElementList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/CanvasElementPart;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178
    iget-object v0, p0, Lcom/samsung/thumbnail/office/word/Page;->elementList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getFooterStartPoint()F
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/samsung/thumbnail/office/word/Page;->mFooterStartPoint:F

    return v0
.end method

.method public getHeaderStartPoint()F
    .locals 1

    .prologue
    .line 145
    iget v0, p0, Lcom/samsung/thumbnail/office/word/Page;->mHeaderStartPoint:F

    return v0
.end method

.method public getLeftBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/thumbnail/office/word/Page;->leftBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    return-object v0
.end method

.method public getLeftMargin()F
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcom/samsung/thumbnail/office/word/Page;->mLeftMargin:F

    return v0
.end method

.method public getPgBorDisplay()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/thumbnail/office/word/Page;->pgBorDisplay:Ljava/lang/String;

    return-object v0
.end method

.method public getPgBorOffset()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/thumbnail/office/word/Page;->pgBorOffset:Ljava/lang/String;

    return-object v0
.end method

.method public getRightBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/thumbnail/office/word/Page;->rightBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    return-object v0
.end method

.method public getRightMargin()F
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lcom/samsung/thumbnail/office/word/Page;->mRightMargin:F

    return v0
.end method

.method public getTopBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/thumbnail/office/word/Page;->topBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    return-object v0
.end method

.method public getTopMargin()F
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/samsung/thumbnail/office/word/Page;->mTopMargin:F

    return v0
.end method

.method public isCoverPage()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/word/Page;->isCoverPage:Z

    return v0
.end method

.method public setBackgroundColor(I)V
    .locals 0
    .param p1, "backgroundColor"    # I

    .prologue
    .line 100
    iput p1, p0, Lcom/samsung/thumbnail/office/word/Page;->mBackgroundColor:I

    .line 101
    return-void
.end method

.method public setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V
    .locals 0
    .param p1, "bottomBorder"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/samsung/thumbnail/office/word/Page;->bottomBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 89
    return-void
.end method

.method public setBottomMargin(F)V
    .locals 0
    .param p1, "bottomMargin"    # F

    .prologue
    .line 122
    iput p1, p0, Lcom/samsung/thumbnail/office/word/Page;->mBottomMargin:F

    .line 123
    return-void
.end method

.method public setFooterStartPoint(F)V
    .locals 0
    .param p1, "footerStartPoint"    # F

    .prologue
    .line 158
    iput p1, p0, Lcom/samsung/thumbnail/office/word/Page;->mFooterStartPoint:F

    .line 159
    return-void
.end method

.method public setHeaderStartPoint(F)V
    .locals 0
    .param p1, "headerStartPoint"    # F

    .prologue
    .line 149
    iput p1, p0, Lcom/samsung/thumbnail/office/word/Page;->mHeaderStartPoint:F

    .line 150
    return-void
.end method

.method public setIsCoverPage(Z)V
    .locals 0
    .param p1, "isCoverPage"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/word/Page;->isCoverPage:Z

    .line 93
    return-void
.end method

.method public setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V
    .locals 0
    .param p1, "leftBorder"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/samsung/thumbnail/office/word/Page;->leftBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 77
    return-void
.end method

.method public setLeftMargin(F)V
    .locals 0
    .param p1, "leftMargin"    # F

    .prologue
    .line 131
    iput p1, p0, Lcom/samsung/thumbnail/office/word/Page;->mLeftMargin:F

    .line 132
    return-void
.end method

.method public setPgBorDisplay(Ljava/lang/String;)V
    .locals 0
    .param p1, "pgBorDisplay"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/samsung/thumbnail/office/word/Page;->pgBorDisplay:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public setPgBorOffset(Ljava/lang/String;)V
    .locals 0
    .param p1, "pgBorOffset"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/samsung/thumbnail/office/word/Page;->pgBorOffset:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V
    .locals 0
    .param p1, "rightBorder"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/samsung/thumbnail/office/word/Page;->rightBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 85
    return-void
.end method

.method public setRightMargin(F)V
    .locals 0
    .param p1, "rightMargin"    # F

    .prologue
    .line 140
    iput p1, p0, Lcom/samsung/thumbnail/office/word/Page;->mRightMargin:F

    .line 141
    return-void
.end method

.method public setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V
    .locals 0
    .param p1, "topBorder"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/samsung/thumbnail/office/word/Page;->topBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 81
    return-void
.end method

.method public setTopMargin(F)V
    .locals 0
    .param p1, "topMargin"    # F

    .prologue
    .line 113
    iput p1, p0, Lcom/samsung/thumbnail/office/word/Page;->mTopMargin:F

    .line 114
    return-void
.end method
