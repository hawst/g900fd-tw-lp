.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrPattFillHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "SpPrPattFillHandler.java"


# instance fields
.field private mConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;)V
    .locals 3
    .param p1, "consumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;

    .prologue
    .line 17
    const/16 v1, 0x1f

    const-string/jumbo v2, "pattFill"

    invoke-direct {p0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 19
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrPattFillHandler;->mConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;

    .line 20
    const/4 v1, 0x0

    new-array v0, v1, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 21
    .local v0, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrPattFillHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 22
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 27
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 29
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;-><init>()V

    .line 31
    .local v0, "fillProps":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrPattFillHandler;->mConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;

    if-eqz v1, :cond_0

    .line 32
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrPattFillHandler;->mConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;

    invoke-interface {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;->consumeFill(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V

    .line 35
    :cond_0
    return-void
.end method
