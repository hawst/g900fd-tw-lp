.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentSecPrParser;
.super Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;
.source "XDocumentSecPrParser.java"


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;)V
    .locals 0
    .param p1, "consumer"    # Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;)V

    .line 17
    return-void
.end method


# virtual methods
.method public createRootElementHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrHandler;-><init>(I)V

    return-object v0
.end method

.method public getMainNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .locals 1

    .prologue
    .line 27
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocumentSecPrParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v0

    return-object v0
.end method
