.class public Lcom/samsung/thumbnail/office/ooxml/word/chart/XDocChartParser;
.super Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;
.source "XDocChartParser.java"


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;)V
    .locals 0
    .param p1, "chart"    # Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;-><init>(Ljava/lang/Object;)V

    .line 16
    return-void
.end method


# virtual methods
.method public createRootElementHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartHandler;-><init>()V

    .line 21
    .local v0, "chartHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartHandler;
    return-object v0
.end method

.method public getMainNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .locals 1

    .prologue
    .line 26
    const/16 v0, 0x23

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XDocChartParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v0

    return-object v0
.end method
