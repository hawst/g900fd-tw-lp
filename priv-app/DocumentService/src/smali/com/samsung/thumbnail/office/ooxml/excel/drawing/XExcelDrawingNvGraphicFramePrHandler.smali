.class public Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingNvGraphicFramePrHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelDrawingNvGraphicFramePrHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 17
    const/16 v1, 0xc9

    const-string/jumbo v2, "nvGraphicFramePr"

    invoke-direct {p0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 20
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingCNvPrHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingCNvPrHandler;-><init>()V

    .line 21
    .local v0, "cnvpichandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingCNvPrHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingNvGraphicFramePrHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "cNvPr"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    return-void
.end method


# virtual methods
.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 38
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 40
    return-void
.end method
