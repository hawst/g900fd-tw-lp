.class public Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;
.super Ljava/lang/Object;
.source "XWPFSectPr.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$1;,
        Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;,
        Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$FooterRef;,
        Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;
    }
.end annotation


# static fields
.field public static final BODY_SEC:I = 0x14

.field public static final PARA_SEC:I = 0x15


# instance fields
.field private bottom:I

.field private bottomBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

.field private canEleCrtr:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

.field private colCount:I

.field private colSpace:I

.field private footer:F

.field private footerRef:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$FooterRef;",
            ">;"
        }
    .end annotation
.end field

.field private header:F

.field private headerRef:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;",
            ">;"
        }
    .end annotation
.end field

.field private isFooter:Z

.field private isHeader:Z

.field private isLandscape:Z

.field private left:F

.field private leftBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

.field private pgBorDisplay:Ljava/lang/String;

.field private pgBorOffset:Ljava/lang/String;

.field private pgHeight:F

.field private pgWidth:F

.field private right:F

.field private rightBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

.field private secType:I

.field private titlePagePresence:Z

.field private top:I

.field private topBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

.field private type:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->headerRef:Ljava/util/ArrayList;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->footerRef:Ljava/util/ArrayList;

    .line 49
    return-void
.end method


# virtual methods
.method public addFooterRef(Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;Ljava/lang/String;)V
    .locals 2
    .param p1, "type"    # Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;
    .param p2, "rID"    # Ljava/lang/String;

    .prologue
    .line 181
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->isFooter:Z

    .line 182
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->footerRef:Ljava/util/ArrayList;

    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$FooterRef;

    invoke-direct {v1, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$FooterRef;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    return-void
.end method

.method public addHeaderRef(Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;Ljava/lang/String;)V
    .locals 2
    .param p1, "type"    # Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;
    .param p2, "rID"    # Ljava/lang/String;

    .prologue
    .line 176
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->isHeader:Z

    .line 177
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->headerRef:Ljava/util/ArrayList;

    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;

    invoke-direct {v1, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 178
    return-void
.end method

.method public getBottom()F
    .locals 1

    .prologue
    .line 218
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->bottom:I

    int-to-float v0, v0

    return v0
.end method

.method public getBottomBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->bottomBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    return-object v0
.end method

.method public getCanvasElementsCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->canEleCrtr:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    return-object v0
.end method

.method public getColsCount()I
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->colCount:I

    return v0
.end method

.method public getColsSpace()I
    .locals 1

    .prologue
    .line 230
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->colSpace:I

    return v0
.end method

.method public getFooter()F
    .locals 1

    .prologue
    .line 202
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->footer:F

    return v0
.end method

.method public getFooterRef()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$FooterRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->footerRef:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getHeader()F
    .locals 1

    .prologue
    .line 206
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->header:F

    return v0
.end method

.method public getHeaderRef()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$HeaderRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 190
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->headerRef:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getLeft()F
    .locals 1

    .prologue
    .line 214
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->left:F

    return v0
.end method

.method public getLeftBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->leftBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    return-object v0
.end method

.method public getPgBorDisplay()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->pgBorDisplay:Ljava/lang/String;

    return-object v0
.end method

.method public getPgBorOffset()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->pgBorOffset:Ljava/lang/String;

    return-object v0
.end method

.method public getPgHeight()F
    .locals 1

    .prologue
    .line 242
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->pgHeight:F

    return v0
.end method

.method public getPgWidth()F
    .locals 1

    .prologue
    .line 238
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->pgWidth:F

    return v0
.end method

.method public getRight()F
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->right:F

    return v0
.end method

.method public getRightBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->rightBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    return-object v0
.end method

.method public getSecType()I
    .locals 1

    .prologue
    .line 226
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->secType:I

    return v0
.end method

.method public getTop()F
    .locals 1

    .prologue
    .line 222
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->top:I

    int-to-float v0, v0

    return v0
.end method

.method public getTopBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->topBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    return-object v0
.end method

.method public getType()Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->type:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    return-object v0
.end method

.method public isFooter()Z
    .locals 1

    .prologue
    .line 198
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->isFooter:Z

    return v0
.end method

.method public isHeader()Z
    .locals 1

    .prologue
    .line 194
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->isHeader:Z

    return v0
.end method

.method public isLandscape()Z
    .locals 1

    .prologue
    .line 246
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->isLandscape:Z

    return v0
.end method

.method public isTitlePagePresence()Z
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->titlePagePresence:Z

    return v0
.end method

.method public setBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V
    .locals 2
    .param p1, "borderType"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    .param p2, "border"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .prologue
    .line 78
    if-eqz p1, :cond_0

    .line 80
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$XDocBorder$EBorderType:[I

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 82
    :pswitch_0
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->leftBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    goto :goto_0

    .line 85
    :pswitch_1
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->topBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    goto :goto_0

    .line 88
    :pswitch_2
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->rightBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    goto :goto_0

    .line 91
    :pswitch_3
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->bottomBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    goto :goto_0

    .line 80
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setBottom(I)V
    .locals 0
    .param p1, "bottom"    # I

    .prologue
    .line 156
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->bottom:I

    .line 157
    return-void
.end method

.method public setCanvasElementCreater(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 1
    .param p1, "canEleCrtr"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->canEleCrtr:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    if-nez v0, :cond_0

    .line 53
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->canEleCrtr:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .line 54
    :cond_0
    return-void
.end method

.method public setColsCount(I)V
    .locals 0
    .param p1, "colCount"    # I

    .prologue
    .line 140
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->colCount:I

    .line 141
    return-void
.end method

.method public setColsSpace(I)V
    .locals 0
    .param p1, "colSpace"    # I

    .prologue
    .line 144
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->colSpace:I

    .line 145
    return-void
.end method

.method public setFooter(F)V
    .locals 0
    .param p1, "footer"    # F

    .prologue
    .line 172
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->footer:F

    .line 173
    return-void
.end method

.method public setHeader(F)V
    .locals 0
    .param p1, "header"    # F

    .prologue
    .line 168
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->header:F

    .line 169
    return-void
.end method

.method public setLandscape(Z)V
    .locals 0
    .param p1, "isLandscape"    # Z

    .prologue
    .line 128
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->isLandscape:Z

    .line 129
    return-void
.end method

.method public setLeft(F)V
    .locals 0
    .param p1, "left"    # F

    .prologue
    .line 160
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->left:F

    .line 161
    return-void
.end method

.method public setPgBorDisplay(Ljava/lang/String;)V
    .locals 0
    .param p1, "pgBorDisplay"    # Ljava/lang/String;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->pgBorDisplay:Ljava/lang/String;

    .line 109
    return-void
.end method

.method public setPgBorOffset(Ljava/lang/String;)V
    .locals 0
    .param p1, "pgBorOffset"    # Ljava/lang/String;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->pgBorOffset:Ljava/lang/String;

    .line 101
    return-void
.end method

.method public setPgHeight(F)V
    .locals 0
    .param p1, "pgHeight"    # F

    .prologue
    .line 132
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->pgHeight:F

    .line 133
    return-void
.end method

.method public setPgWidth(F)V
    .locals 0
    .param p1, "pgWidth"    # F

    .prologue
    .line 136
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->pgWidth:F

    .line 137
    return-void
.end method

.method public setRight(F)V
    .locals 0
    .param p1, "right"    # F

    .prologue
    .line 164
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->right:F

    .line 165
    return-void
.end method

.method public setSecType(I)V
    .locals 0
    .param p1, "secType"    # I

    .prologue
    .line 148
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->secType:I

    .line 149
    return-void
.end method

.method public setTitlePagePresence(Z)V
    .locals 0
    .param p1, "titlePagePresence"    # Z

    .prologue
    .line 120
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->titlePagePresence:Z

    .line 121
    return-void
.end method

.method public setTop(I)V
    .locals 0
    .param p1, "top"    # I

    .prologue
    .line 152
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->top:I

    .line 153
    return-void
.end method

.method public setType(Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;)V
    .locals 0
    .param p1, "type"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    .prologue
    .line 124
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->type:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr$ESectPrType;

    .line 125
    return-void
.end method
