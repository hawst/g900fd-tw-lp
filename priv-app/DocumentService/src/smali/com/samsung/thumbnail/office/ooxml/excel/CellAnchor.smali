.class public Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;
.super Ljava/lang/Object;
.source "CellAnchor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;
    }
.end annotation


# instance fields
.field private blipId:Ljava/lang/String;

.field private fromCol:I

.field private fromColOff:J

.field private fromRow:I

.field private fromRowOff:J

.field private id:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private shapeParaLst:Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;

.field private shapeProperty:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

.field private type:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    return-void
.end method


# virtual methods
.method public getBlipId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->blipId:Ljava/lang/String;

    return-object v0
.end method

.method public getFromCol()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->fromCol:I

    return v0
.end method

.method public getFromColOff()J
    .locals 2

    .prologue
    .line 92
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->fromColOff:J

    return-wide v0
.end method

.method public getFromRow()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->fromRow:I

    return v0
.end method

.method public getFromRowOff()J
    .locals 2

    .prologue
    .line 100
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->fromRowOff:J

    return-wide v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPara()Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->shapeParaLst:Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;

    return-object v0
.end method

.method public getShapeProperty()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->shapeProperty:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    return-object v0
.end method

.method public getType()Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->type:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    return-object v0
.end method

.method public setBlipId(Ljava/lang/String;)V
    .locals 0
    .param p1, "blipId"    # Ljava/lang/String;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->blipId:Ljava/lang/String;

    .line 105
    return-void
.end method

.method public setFromCol(I)V
    .locals 0
    .param p1, "fromCol"    # I

    .prologue
    .line 72
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->fromCol:I

    .line 73
    return-void
.end method

.method public setFromColOff(J)V
    .locals 1
    .param p1, "fromColOff"    # J

    .prologue
    .line 88
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->fromColOff:J

    .line 89
    return-void
.end method

.method public setFromRow(I)V
    .locals 0
    .param p1, "fromRow"    # I

    .prologue
    .line 80
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->fromRow:I

    .line 81
    return-void
.end method

.method public setFromRowOff(J)V
    .locals 1
    .param p1, "fromRowOff"    # J

    .prologue
    .line 96
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->fromRowOff:J

    .line 97
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->id:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->name:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public setPara(Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;)V
    .locals 0
    .param p1, "shapeParaLst"    # Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->shapeParaLst:Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;

    .line 49
    return-void
.end method

.method public setShapePropery(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 0
    .param p1, "shapeProperty"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->shapeProperty:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 41
    return-void
.end method

.method public setType(Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;)V
    .locals 0
    .param p1, "type"    # Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->type:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    .line 33
    return-void
.end method
