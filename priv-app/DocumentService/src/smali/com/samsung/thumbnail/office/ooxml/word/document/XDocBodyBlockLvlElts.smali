.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLMainNSHandler;
.source "XDocBodyBlockLvlElts.java"


# instance fields
.field private type:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1, "elementName"    # Ljava/lang/String;
    .param p2, "type"    # I

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLMainNSHandler;-><init>(Ljava/lang/String;)V

    .line 20
    iput p2, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->type:I

    .line 21
    return-void
.end method


# virtual methods
.method protected init()V
    .locals 5

    .prologue
    .line 24
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocParaHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocParaHandler;-><init>()V

    .line 25
    .local v0, "paraHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocParaHandler;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v4, "p"

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableHandler;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableHandler;-><init>()V

    .line 27
    .local v2, "tableHandler":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableHandler;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v4, "tbl"

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;

    iget v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->type:I

    invoke-direct {v1, v3}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;-><init>(I)V

    .line 29
    .local v1, "sdtHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v4, "sdt"

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    return-void
.end method

.method protected initSdtCell()V
    .locals 4

    .prologue
    .line 42
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCHandler;-><init>()V

    .line 43
    .local v1, "tcHandler":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "tc"

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;

    iget v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->type:I

    invoke-direct {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;-><init>(I)V

    .line 45
    .local v0, "sdtHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "sdt"

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    return-void
.end method

.method protected initSdtRow()V
    .locals 4

    .prologue
    .line 49
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRHandler;-><init>()V

    .line 50
    .local v1, "tblRowHanlder":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "tr"

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;

    iget v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->type:I

    invoke-direct {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;-><init>(I)V

    .line 52
    .local v0, "sdtHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "sdt"

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    return-void
.end method

.method protected initSdtRun()V
    .locals 5

    .prologue
    .line 33
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRunHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRunHandler;-><init>()V

    .line 34
    .local v1, "runHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRunHandler;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v4, "r"

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocHyperlinkHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocHyperlinkHandler;-><init>()V

    .line 36
    .local v0, "hyperlinkHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocHyperlinkHandler;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v4, "hyperlink"

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;

    iget v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->type:I

    invoke-direct {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;-><init>(I)V

    .line 38
    .local v2, "sdtHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v4, "sdt"

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 58
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLMainNSHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 59
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->type:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_1

    .line 60
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->init()V

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->type:I

    const/16 v1, 0x65

    if-ne v0, v1, :cond_2

    .line 62
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->initSdtRun()V

    goto :goto_0

    .line 63
    :cond_2
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->type:I

    const/16 v1, 0x66

    if-ne v0, v1, :cond_3

    .line 64
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->initSdtCell()V

    goto :goto_0

    .line 65
    :cond_3
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->type:I

    const/16 v1, 0x67

    if-ne v0, v1, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->initSdtRow()V

    goto :goto_0
.end method
