.class public Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartBarChartHandler;
.super Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;
.source "DMLChartBarChartHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 20
    const/16 v0, 0x23

    const-string/jumbo v1, "barChart"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;-><init>(ILjava/lang/String;)V

    .line 21
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    .line 24
    const/4 v3, 0x1

    new-array v0, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 26
    .local v0, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLBarChartDirHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLBarChartDirHandler;-><init>()V

    .line 27
    .local v1, "serHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLBarChartDirHandler;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v3, "barDir"

    invoke-direct {v2, v3, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 29
    .local v2, "serSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/4 v3, 0x0

    aput-object v2, v0, v3

    .line 31
    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartBarChartHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 32
    return-void
.end method


# virtual methods
.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 44
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 50
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 37
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 38
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartBarChartHandler;->init()V

    .line 39
    return-void
.end method
