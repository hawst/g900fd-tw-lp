.class public Lcom/samsung/thumbnail/office/ooxml/dml/wp/PosOffsetHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "PosOffsetHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/wp/PosOffsetHandler$IOffsetPosObserver;
    }
.end annotation


# instance fields
.field private offsetPosOberserver:Lcom/samsung/thumbnail/office/ooxml/dml/wp/PosOffsetHandler$IOffsetPosObserver;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/wp/PosOffsetHandler$IOffsetPosObserver;)V
    .locals 2
    .param p1, "offsetPosObserver"    # Lcom/samsung/thumbnail/office/ooxml/dml/wp/PosOffsetHandler$IOffsetPosObserver;

    .prologue
    .line 21
    const/16 v0, 0x1e

    const-string/jumbo v1, "posOffset"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 22
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/PosOffsetHandler;->offsetPosOberserver:Lcom/samsung/thumbnail/office/ooxml/dml/wp/PosOffsetHandler$IOffsetPosObserver;

    .line 23
    return-void
.end method


# virtual methods
.method public charactersHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;[CII)V
    .locals 6
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "content"    # [C
    .param p3, "start"    # I
    .param p4, "end"    # I

    .prologue
    .line 28
    const/4 v1, 0x0

    .line 29
    .local v1, "offsetValue":I
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p2, p3, p4}, Ljava/lang/String;-><init>([CII)V

    .line 32
    .local v2, "str":Ljava/lang/String;
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 36
    :goto_0
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/PosOffsetHandler;->offsetPosOberserver:Lcom/samsung/thumbnail/office/ooxml/dml/wp/PosOffsetHandler$IOffsetPosObserver;

    int-to-long v4, v1

    invoke-interface {v3, v4, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/PosOffsetHandler$IOffsetPosObserver;->setOffsetVal(J)V

    .line 37
    return-void

    .line 33
    :catch_0
    move-exception v0

    .line 34
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
