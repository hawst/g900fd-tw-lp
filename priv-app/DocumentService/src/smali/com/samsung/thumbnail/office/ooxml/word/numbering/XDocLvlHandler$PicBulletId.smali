.class Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$PicBulletId;
.super Ljava/lang/Object;
.source "XDocLvlHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PicBulletId"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;


# direct methods
.method constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$PicBulletId;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V
    .locals 3
    .param p1, "value"    # Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;

    .prologue
    .line 191
    instance-of v1, p1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 192
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;

    .line 193
    .local v0, "picBulletId":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler$PicBulletId;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;

    # getter for: Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;->numLvl:Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;
    invoke-static {v1}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;->access$000(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlHandler;)Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;->getValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->setPicBulletId(I)V

    .line 195
    .end local v0    # "picBulletId":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;
    :cond_0
    return-void
.end method
