.class public Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;
.super Ljava/lang/Object;
.source "XDocBordersProperty.java"


# instance fields
.field private borders:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;",
            "Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->borders:Ljava/util/HashMap;

    .line 22
    return-void
.end method

.method private setHTMLBorderStyles(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "style"    # Ljava/lang/StringBuilder;
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    return-void
.end method


# virtual methods
.method public createBorderStyle(Ljava/lang/StringBuilder;)V
    .locals 14
    .param p1, "style"    # Ljava/lang/StringBuilder;

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x4

    .line 29
    new-array v2, v10, [Ljava/lang/String;

    const-string/jumbo v8, "top"

    aput-object v8, v2, v9

    const-string/jumbo v8, "right"

    aput-object v8, v2, v11

    const-string/jumbo v8, "bottom"

    aput-object v8, v2, v12

    const-string/jumbo v8, "left"

    aput-object v8, v2, v13

    .line 30
    .local v2, "borderTypes":[Ljava/lang/String;
    new-array v3, v10, [Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    sget-object v8, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->TOP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v8, v3, v9

    sget-object v8, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->RIGHT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v8, v3, v11

    sget-object v8, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->BOTTOM:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v8, v3, v12

    sget-object v8, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->LEFT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v8, v3, v13

    .line 33
    .local v3, "eBorders":[Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v8, 0x100

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 34
    .local v5, "padding":Ljava/lang/StringBuilder;
    const-string/jumbo v8, "padding:"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v10, :cond_5

    .line 37
    iget-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->borders:Ljava/util/HashMap;

    aget-object v9, v3, v4

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 38
    .local v0, "border":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    const/4 v1, 0x0

    .line 39
    .local v1, "borderType":Ljava/lang/String;
    if-eqz v0, :cond_4

    .line 40
    const-string/jumbo v7, ""

    .line 41
    .local v7, "value":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 42
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;)Ljava/lang/String;

    move-result-object v1

    .line 43
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 45
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 46
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->isAuto()Z

    move-result v8

    if-nez v8, :cond_1

    .line 47
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "#"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 50
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSize()I

    move-result v8

    if-ltz v8, :cond_3

    .line 51
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSize()I

    move-result v6

    .line 52
    .local v6, "size":I
    if-eqz v1, :cond_2

    .line 53
    const-string/jumbo v8, "double"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 54
    add-int/lit8 v6, v6, 0xe

    .line 57
    :cond_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v6}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->eightsOfPoint(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 59
    .end local v6    # "size":I
    :cond_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "border-"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget-object v9, v2, v4

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, p1, v8, v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->setHTMLBorderStyles(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSpace()I

    move-result v8

    if-lez v8, :cond_4

    .line 63
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSpace()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "pt "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .end local v7    # "value":Ljava/lang/String;
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 67
    .end local v0    # "border":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .end local v1    # "borderType":Ljava/lang/String;
    :cond_5
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ";"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    return-void
.end method

.method public createTableBorderStyle(Ljava/lang/StringBuilder;Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;)V
    .locals 11
    .param p1, "style"    # Ljava/lang/StringBuilder;
    .param p2, "tableBorders"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    .prologue
    .line 72
    const/4 v9, 0x4

    new-array v2, v9, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string/jumbo v10, "top"

    aput-object v10, v2, v9

    const/4 v9, 0x1

    const-string/jumbo v10, "right"

    aput-object v10, v2, v9

    const/4 v9, 0x2

    const-string/jumbo v10, "bottom"

    aput-object v10, v2, v9

    const/4 v9, 0x3

    const-string/jumbo v10, "left"

    aput-object v10, v2, v9

    .line 73
    .local v2, "borderTypes":[Ljava/lang/String;
    const/4 v9, 0x4

    new-array v3, v9, [Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    const/4 v9, 0x0

    sget-object v10, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->TOP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v10, v3, v9

    const/4 v9, 0x1

    sget-object v10, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->RIGHT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v10, v3, v9

    const/4 v9, 0x2

    sget-object v10, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->BOTTOM:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v10, v3, v9

    const/4 v9, 0x3

    sget-object v10, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->LEFT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v10, v3, v9

    .line 76
    .local v3, "eBorders":[Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v9, 0x100

    invoke-direct {v5, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 77
    .local v5, "padding":Ljava/lang/StringBuilder;
    const-string/jumbo v9, "padding:"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    const/4 v9, 0x4

    if-ge v4, v9, :cond_b

    .line 80
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->borders:Ljava/util/HashMap;

    aget-object v10, v3, v4

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 81
    .local v0, "border":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    const/4 v7, 0x0

    .line 82
    .local v7, "tblBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    if-eqz p2, :cond_0

    iget-object v9, p2, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->borders:Ljava/util/HashMap;

    if-eqz v9, :cond_0

    .line 83
    iget-object v9, p2, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->borders:Ljava/util/HashMap;

    aget-object v10, v3, v4

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "tblBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 84
    .restart local v7    # "tblBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :cond_0
    const/4 v1, 0x0

    .line 85
    .local v1, "borderType":Ljava/lang/String;
    if-eqz v0, :cond_6

    .line 86
    const-string/jumbo v8, ""

    .line 87
    .local v8, "value":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 88
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v9

    invoke-virtual {v0, v9}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;)Ljava/lang/String;

    move-result-object v1

    .line 89
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 91
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 92
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->isAuto()Z

    move-result v9

    if-nez v9, :cond_2

    .line 93
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "#"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 96
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSize()I

    move-result v9

    if-ltz v9, :cond_4

    .line 97
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSize()I

    move-result v6

    .line 98
    .local v6, "size":I
    if-eqz v1, :cond_3

    .line 99
    const-string/jumbo v9, "double"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 100
    add-int/lit8 v6, v6, 0xe

    .line 103
    :cond_3
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v6}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->eightsOfPoint(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 105
    .end local v6    # "size":I
    :cond_4
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "border-"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    aget-object v10, v2, v4

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ":"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, p1, v9, v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->setHTMLBorderStyles(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSpace()I

    move-result v9

    if-lez v9, :cond_5

    .line 109
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSpace()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "pt "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    .end local v8    # "value":Ljava/lang/String;
    :cond_5
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 111
    :cond_6
    if-eqz v7, :cond_5

    .line 113
    const-string/jumbo v8, ""

    .line 114
    .restart local v8    # "value":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v9

    if-eqz v9, :cond_7

    .line 115
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;)Ljava/lang/String;

    move-result-object v1

    .line 116
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 118
    :cond_7
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v9

    if-eqz v9, :cond_8

    .line 119
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->isAuto()Z

    move-result v9

    if-nez v9, :cond_8

    .line 120
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "#"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 123
    :cond_8
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSize()I

    move-result v9

    if-ltz v9, :cond_a

    .line 124
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSize()I

    move-result v6

    .line 125
    .restart local v6    # "size":I
    if-eqz v1, :cond_9

    .line 126
    const-string/jumbo v9, "double"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 127
    add-int/lit8 v6, v6, 0xe

    .line 130
    :cond_9
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v6}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->eightsOfPoint(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 132
    .end local v6    # "size":I
    :cond_a
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "border-"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    aget-object v10, v2, v4

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ":"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, p1, v9, v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->setHTMLBorderStyles(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSpace()I

    move-result v9

    if-lez v9, :cond_5

    .line 136
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSpace()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "pt "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 143
    .end local v0    # "border":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .end local v1    # "borderType":Ljava/lang/String;
    .end local v7    # "tblBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .end local v8    # "value":Ljava/lang/String;
    :cond_b
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ";"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    return-void
.end method

.method public getBorders()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;",
            "Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->borders:Ljava/util/HashMap;

    return-object v0
.end method

.method public setBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V
    .locals 1
    .param p1, "borderType"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    .param p2, "border"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->borders:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    return-void
.end method
