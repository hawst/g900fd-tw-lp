.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrColsHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;
.source "XDocSectPrColsHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrColsHandler$ICols;
    }
.end annotation


# instance fields
.field private cols:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrColsHandler$ICols;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrColsHandler$ICols;)V
    .locals 1
    .param p1, "cols"    # Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrColsHandler$ICols;

    .prologue
    .line 20
    const-string/jumbo v0, "cols"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;-><init>(Ljava/lang/String;)V

    .line 21
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrColsHandler;->cols:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrColsHandler$ICols;

    .line 22
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 27
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 29
    const-string/jumbo v1, "num"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrColsHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 30
    .local v0, "val":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 31
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrColsHandler;->cols:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrColsHandler$ICols;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrColsHandler$ICols;->setColumnCount(I)V

    .line 34
    :cond_0
    const-string/jumbo v1, "space"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrColsHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 35
    if-eqz v0, :cond_1

    .line 36
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrColsHandler;->cols:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrColsHandler$ICols;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSectPrColsHandler$ICols;->setColumnSpace(I)V

    .line 38
    :cond_1
    return-void
.end method
