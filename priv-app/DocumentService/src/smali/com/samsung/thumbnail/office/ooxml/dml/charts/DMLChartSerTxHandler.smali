.class public Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerTxHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DMLChartSerTxHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;


# instance fields
.field private seriesVal:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    .line 29
    const/16 v5, 0x23

    const-string/jumbo v6, "tx"

    invoke-direct {p0, v5, v6}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 31
    const/4 v5, 0x2

    new-array v2, v5, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 33
    .local v2, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrRefHandler;

    invoke-direct {v3, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrRefHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;)V

    .line 34
    .local v3, "strrefHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrRefHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v5, "strRef"

    invoke-direct {v4, v5, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 36
    .local v4, "strrefSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/4 v5, 0x0

    aput-object v4, v2, v5

    .line 39
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler;

    invoke-direct {v0, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;)V

    .line 40
    .local v0, "ptvHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v5, "v"

    invoke-direct {v1, v5, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 42
    .local v1, "ptvSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/4 v5, 0x1

    aput-object v1, v2, v5

    .line 43
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerTxHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 44
    return-void
.end method


# virtual methods
.method public chartNumVal(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "val"    # Ljava/lang/String;

    .prologue
    .line 82
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->addChartTxNumVal(Ljava/lang/String;)V

    .line 83
    return-void
.end method

.method public chartStrVal(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "str"    # Ljava/lang/String;

    .prologue
    .line 87
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->addChartTxStrVal(Ljava/lang/String;)V

    .line 88
    return-void
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 71
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 73
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    if-eqz v0, :cond_0

    .line 78
    :goto_0
    return-void

    .line 77
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    const-string/jumbo v1, "String"

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->setExChartVAl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setFormatCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "formatCode"    # Ljava/lang/String;

    .prologue
    .line 106
    return-void
.end method

.method public setPtVal(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "strVal"    # Ljava/lang/String;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerTxHandler;->seriesVal:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->addPtVal(Ljava/lang/String;)V

    .line 94
    return-void
.end method

.method public setRefType(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;I)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "refType"    # I

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerTxHandler;->seriesVal:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->setRefType(I)V

    .line 99
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 49
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 52
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    if-eqz v0, :cond_0

    .line 53
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerTxHandler;->seriesVal:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    .line 54
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getCurSeries()Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    move-result-object v0

    const-string/jumbo v1, "tx"

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerTxHandler;->seriesVal:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->putSeriesVal(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;)V

    .line 59
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    if-eqz v0, :cond_1

    .line 60
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerTxHandler;->seriesVal:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    .line 61
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getCurSeries()Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    move-result-object v0

    const-string/jumbo v1, "tx"

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerTxHandler;->seriesVal:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->putSeriesVal(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;)V

    .line 64
    :cond_1
    return-void
.end method
