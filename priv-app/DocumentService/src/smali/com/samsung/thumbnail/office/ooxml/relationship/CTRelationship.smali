.class public Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;
.super Ljava/lang/Object;
.source "CTRelationship.java"


# instance fields
.field private Id:Ljava/lang/String;

.field private Target:Ljava/lang/String;

.field private TargetMode:Ljava/lang/String;

.field private Type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;->Id:Ljava/lang/String;

    return-object v0
.end method

.method public getTargeMode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;->TargetMode:Ljava/lang/String;

    return-object v0
.end method

.method public getTarget()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;->Target:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;->Type:Ljava/lang/String;

    return-object v0
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "Id"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;->Id:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public setTarget(Ljava/lang/String;)V
    .locals 0
    .param p1, "Target"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;->Target:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public setTargetMode(Ljava/lang/String;)V
    .locals 0
    .param p1, "TargetMode"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;->TargetMode:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "Type"    # Ljava/lang/String;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;->Type:Ljava/lang/String;

    .line 27
    return-void
.end method
