.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeElementsHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "ThemeElementsHandler.java"


# instance fields
.field private theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    const-string/jumbo v0, "themeElements"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 22
    return-void
.end method


# virtual methods
.method protected init()V
    .locals 8

    .prologue
    .line 25
    const/4 v7, 0x3

    new-array v6, v7, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 27
    .local v6, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ColorSchemeHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ColorSchemeHandler;-><init>()V

    .line 28
    .local v0, "clrSchemeHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/ColorSchemeHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v7, "clrScheme"

    invoke-direct {v1, v7, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 30
    .local v1, "clrSchemeSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v7, 0x0

    aput-object v1, v6, v7

    .line 32
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FontSchemeHandler;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FontSchemeHandler;-><init>()V

    .line 33
    .local v2, "fontSchemeHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FontSchemeHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v7, "fontScheme"

    invoke-direct {v3, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 35
    .local v3, "fontSchemeSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v7, 0x1

    aput-object v3, v6, v7

    .line 37
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FormatSchemeHandler;

    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeElementsHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-direct {v4, v7}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FormatSchemeHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 38
    .local v4, "formatSchemeHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FormatSchemeHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v7, "fmtScheme"

    invoke-direct {v5, v7, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 40
    .local v5, "formatSchemeSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v7, 0x2

    aput-object v5, v6, v7

    .line 42
    iput-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeElementsHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 43
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 48
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 49
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    if-eqz v0, :cond_0

    .line 50
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeElementsHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .line 52
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeElementsHandler;->init()V

    .line 53
    return-void
.end method
