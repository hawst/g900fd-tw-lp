.class public Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTTextBodyHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XPPTTextBodyHandler.java"


# instance fields
.field private textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;


# direct methods
.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;)V
    .locals 1
    .param p1, "ID"    # I
    .param p2, "autoShape"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;

    .prologue
    .line 21
    const-string/jumbo v0, "txBody"

    invoke-direct {p0, p1, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 22
    check-cast p2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    .end local p2    # "autoShape":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTTextBodyHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    .line 23
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTTextBodyHandler;->init()V

    .line 24
    return-void
.end method


# virtual methods
.method protected init()V
    .locals 9

    .prologue
    .line 27
    const/4 v7, 0x3

    new-array v6, v7, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 29
    .local v6, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ParagraphHandler;

    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTTextBodyHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    invoke-direct {v4, v7}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ParagraphHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;)V

    .line 30
    .local v4, "paraHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ParagraphHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v7, "p"

    invoke-direct {v5, v7, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 32
    .local v5, "paraSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v7, 0x0

    aput-object v5, v6, v7

    .line 34
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;

    const-string/jumbo v7, "lstStyle"

    iget-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTTextBodyHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    invoke-direct {v2, v7, v8}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;)V

    .line 36
    .local v2, "lstStyleHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v7, "lstStyle"

    invoke-direct {v3, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 38
    .local v3, "lstStyleSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v7, 0x1

    aput-object v3, v6, v7

    .line 40
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextBodyPropertyHandler;

    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTTextBodyHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    invoke-direct {v0, v7}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextBodyPropertyHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;)V

    .line 42
    .local v0, "bodyPrHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextBodyPropertyHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v7, "bodyPr"

    invoke-direct {v1, v7, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 44
    .local v1, "bodyPrSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v7, 0x2

    aput-object v1, v6, v7

    .line 46
    iput-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTTextBodyHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 47
    return-void
.end method
