.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableRowHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "TableRowHandler.java"


# instance fields
.field private table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

.field private tableRow:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;)V
    .locals 2
    .param p1, "table"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    .prologue
    .line 25
    const/16 v0, 0x1f

    const-string/jumbo v1, "tr"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 26
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableRowHandler;->table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    .line 27
    return-void
.end method

.method private init()V
    .locals 3

    .prologue
    .line 30
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableCellHandler;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableRowHandler;->tableRow:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableCellHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;)V

    .line 31
    .local v0, "tcHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableCellHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableRowHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "tc"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 37
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 38
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableRowHandler;->table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    invoke-direct {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;)V

    iput-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableRowHandler;->tableRow:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;

    .line 39
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableRowHandler;->table:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableRowHandler;->tableRow:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTable;->addRow(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;)V

    .line 40
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableRowHandler;->init()V

    .line 42
    const-string/jumbo v3, "h"

    invoke-virtual {p0, p3, v3, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableRowHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    .line 43
    .local v2, "heightVal":Ljava/lang/String;
    const-wide/16 v0, 0x0

    .line 44
    .local v0, "height":J
    if-eqz v2, :cond_0

    .line 46
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 47
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableRowHandler;->tableRow:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;

    invoke-virtual {v3, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;->setHeight(J)V

    .line 49
    :cond_0
    return-void
.end method
