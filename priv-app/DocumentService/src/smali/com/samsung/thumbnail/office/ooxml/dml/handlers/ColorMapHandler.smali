.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorMapHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "ColorMapHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/pptx/OverrideClrMapHandler$IClrMapObserver;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "mapEle"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorMapHandler;->init()V

    .line 26
    return-void
.end method

.method private init()V
    .locals 6

    .prologue
    .line 29
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/pptx/OverrideClrMapHandler;

    invoke-direct {v1, p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/OverrideClrMapHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/OverrideClrMapHandler$IClrMapObserver;)V

    .line 30
    .local v1, "overrideClrMapHand":Lcom/samsung/thumbnail/office/ooxml/pptx/OverrideClrMapHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorMapHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "overrideClrMapping"

    invoke-virtual {v4, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    const/4 v4, 0x1

    new-array v3, v4, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 34
    .local v3, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/pptx/OverrideClrMapHandler;

    invoke-direct {v2, p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/OverrideClrMapHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/OverrideClrMapHandler$IClrMapObserver;)V

    .line 35
    .local v2, "ovrideClrMapHandler":Lcom/samsung/thumbnail/office/ooxml/pptx/OverrideClrMapHandler;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v4, "overrideClrMapping"

    invoke-direct {v0, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 37
    .local v0, "overClrMapSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v4, 0x0

    aput-object v0, v3, v4

    .line 39
    iput-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorMapHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 40
    return-void
.end method


# virtual methods
.method public setClrMap(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/util/HashMap;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p2, "colorMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->setColorMap(Ljava/util/Map;)V

    .line 68
    :cond_0
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 45
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 46
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 48
    .local v0, "colorMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p3}, Lorg/xml/sax/Attributes;->getLength()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 49
    invoke-interface {p3, v1}, Lorg/xml/sax/Attributes;->getQName(I)Ljava/lang/String;

    move-result-object v2

    .line 50
    .local v2, "name":Ljava/lang/String;
    invoke-interface {p3, v1}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object v3

    .line 51
    .local v3, "value":Ljava/lang/String;
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 52
    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 56
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "value":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    if-eqz v4, :cond_2

    .line 57
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->setColorMap(Ljava/util/Map;)V

    .line 60
    :cond_2
    return-void
.end method
