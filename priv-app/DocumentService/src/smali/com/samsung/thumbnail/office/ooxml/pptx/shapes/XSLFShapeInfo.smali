.class public Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
.super Ljava/lang/Object;
.source "XSLFShapeInfo.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ILineChoiceConsumer;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;


# instance fields
.field private adjVal:I

.field private anchor:Ljava/lang/String;

.field private cellColor:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

.field protected childShH:J

.field protected childShW:J

.field protected childShX:J

.field protected childShY:J

.field private diagonalBrdr:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;",
            ">;"
        }
    .end annotation
.end field

.field private fillAlpha:J

.field private fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

.field private fillRefColor:Ljava/lang/String;

.field private fillRefId:Ljava/lang/String;

.field private flipH:Z

.field private flipV:Z

.field private fontRef:Ljava/lang/String;

.field private formulaNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private formulas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private gradFillColor:Ljava/lang/String;

.field private gradFillPathName:Ljava/lang/String;

.field private gridSpan:I

.field private hMerge:Z

.field private isDiagonalBorder:Z

.field private isDiagramShape:Z

.field public isLineNoFill:Z

.field public isShapeNoFill:Z

.field private isTextBox:Z

.field private isTransformSet:Z

.field private lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

.field private linePropsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;",
            ">;"
        }
    .end annotation
.end field

.field private lineRefColor:Ljava/lang/String;

.field private lumMod:Ljava/lang/String;

.field private marginBottom:Ljava/lang/String;

.field private marginLeft:Ljava/lang/String;

.field private marginRight:Ljava/lang/String;

.field private marginTop:Ljava/lang/String;

.field protected orgShH:J

.field protected orgShW:J

.field private pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

.field private placeHolder:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

.field private prstValue:Ljava/lang/String;

.field private refId:Ljava/lang/String;

.field private rot:I

.field private rowSpan:I

.field private sdRatio:F

.field protected shH:J

.field protected shW:J

.field protected shX:J

.field protected shY:J

.field private shapeId:I

.field private shapeName:Ljava/lang/String;

.field private shapeType:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field private textRotation:I

.field private vMerge:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-boolean v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isTextBox:Z

    .line 41
    iput-boolean v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isTransformSet:Z

    .line 48
    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->rowSpan:I

    .line 49
    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->gridSpan:I

    .line 50
    iput-boolean v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->hMerge:Z

    .line 51
    iput-boolean v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->vMerge:Z

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->linePropsList:Ljava/util/ArrayList;

    .line 66
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->diagonalBrdr:Ljava/util/HashMap;

    .line 67
    iput-boolean v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isDiagonalBorder:Z

    .line 68
    iput-boolean v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isDiagramShape:Z

    .line 69
    iput v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->adjVal:I

    .line 70
    iput v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->textRotation:I

    .line 71
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->fillAlpha:J

    .line 73
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->sdRatio:F

    .line 75
    const-string/jumbo v0, "line"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->gradFillPathName:Ljava/lang/String;

    .line 101
    iput-boolean v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isLineNoFill:Z

    .line 102
    iput-boolean v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isShapeNoFill:Z

    return-void
.end method


# virtual methods
.method public addDiagonalBrdr(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V
    .locals 1
    .param p1, "element"    # Ljava/lang/String;
    .param p2, "lineProps"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .prologue
    .line 605
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->diagonalBrdr:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 606
    return-void
.end method

.method public addLinePropsList(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V
    .locals 1
    .param p1, "lineProps"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .prologue
    .line 228
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->linePropsList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 229
    return-void
.end method

.method public addRowSpan()V
    .locals 1

    .prologue
    .line 444
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->rowSpan:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->rowSpan:I

    .line 445
    return-void
.end method

.method public consumeFill(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 0
    .param p1, "fillProperties"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 615
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .line 616
    return-void
.end method

.method public consumeLine(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V
    .locals 0
    .param p1, "lineProps"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .prologue
    .line 610
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .line 611
    return-void
.end method

.method public getActualShapeHeight()J
    .locals 2

    .prologue
    .line 512
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->orgShH:J

    return-wide v0
.end method

.method public getActualShapeWidth()J
    .locals 2

    .prologue
    .line 516
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->orgShW:J

    return-wide v0
.end method

.method public getAdjVal()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 367
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjValNameArray()Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_a

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjValNameArray()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_a

    .line 369
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjValNameArray()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string/jumbo v4, "adj"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjValNameArray()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string/jumbo v4, "adj1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 372
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjValArray()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v5, 0x4

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjValArray()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v5, v4}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v1, v3

    check-cast v1, Ljava/lang/String;

    .line 374
    .local v1, "fmlaValue":Ljava/lang/String;
    const-string/jumbo v3, "-"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 375
    const/4 v3, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 380
    :cond_1
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 381
    .local v2, "val":I
    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->shapeAdjustValue(I)I

    move-result v0

    .line 383
    .local v0, "convrtValue":I
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "rightArrow"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "leftArrow"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "upArrow"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "notchedRightArrow"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "leftRightArrow"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "RightArrowCallout"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "LeftArrowCallout"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "UpArrowCallout"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "DownArrowCallout"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "LeftRightArrowCallout"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 393
    :cond_2
    rsub-int v3, v0, 0x5460

    div-int/lit8 v0, v3, 0x2

    .line 395
    :cond_3
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "downArrow"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "upDownArrow"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 397
    :cond_4
    add-int/lit16 v0, v0, 0x3e8

    .line 399
    :cond_5
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "chevron"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 400
    div-int/lit8 v0, v0, 0x2

    .line 402
    :cond_6
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "SmileyFace"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 403
    rsub-int v3, v0, 0x5460

    div-int/lit8 v3, v3, 0x2

    add-int/lit16 v0, v3, 0x1464

    .line 405
    :cond_7
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "Ribbon2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 406
    rsub-int v0, v0, 0x5460

    .line 408
    :cond_8
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "Star4"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "Star8"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "Star16"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "Star24"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "Star32"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 413
    :cond_9
    rsub-int v0, v0, 0x2a30

    .line 428
    .end local v0    # "convrtValue":I
    .end local v1    # "fmlaValue":Ljava/lang/String;
    .end local v2    # "val":I
    :cond_a
    return v0
.end method

.method public getAdjVal(I)I
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjValNameArray()Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_13

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjValNameArray()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_13

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjValNameArray()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v3, p1, :cond_13

    .line 266
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjValNameArray()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "adj"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, p1, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 268
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjValArray()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v5, 0x4

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjValArray()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v5, v4}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v1, v3

    check-cast v1, Ljava/lang/String;

    .line 270
    .local v1, "fmlaValue":Ljava/lang/String;
    const-string/jumbo v3, "-"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 271
    const/4 v3, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 276
    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 277
    .local v2, "val":I
    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->shapeAdjustValue(I)I

    move-result v0

    .line 285
    .local v0, "convrtValue":I
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "leftArrow"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "upArrow"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "leftRightArrow"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 288
    :cond_1
    div-int/lit8 v0, v0, 0x2

    .line 290
    :cond_2
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "downArrow"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 291
    rsub-int v3, v0, 0x5460

    add-int/lit16 v0, v3, 0x5dc

    .line 293
    :cond_3
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "upDownArrow"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "Ribbon2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "Ribbon"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "wave"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "doubleWave"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 298
    :cond_4
    rsub-int v3, v0, 0x5460

    div-int/lit8 v0, v3, 0x2

    .line 300
    :cond_5
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "rightArrow"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 301
    rsub-int v0, v0, 0x5460

    .line 303
    :cond_6
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "RightArrowCallout"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 312
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjValNameArray()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string/jumbo v4, "adj2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 313
    rsub-int v0, v0, 0x2a30

    .line 315
    :cond_7
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjValNameArray()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string/jumbo v4, "adj3"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 316
    rsub-int v3, v0, 0x5460

    add-int/lit16 v0, v3, 0x708

    .line 319
    :cond_8
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "LeftArrowCallout"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "UpArrowCallout"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 321
    :cond_9
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjValNameArray()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string/jumbo v4, "adj2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 322
    rsub-int v0, v0, 0x2a30

    .line 324
    :cond_a
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjValNameArray()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string/jumbo v4, "adj3"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 325
    add-int/lit16 v0, v0, -0x708

    .line 327
    :cond_b
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjValNameArray()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string/jumbo v4, "adj4"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 328
    rsub-int v0, v0, 0x5460

    .line 331
    :cond_c
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "DownArrowCallout"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 332
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjValNameArray()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string/jumbo v4, "adj2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 333
    rsub-int v0, v0, 0x2a30

    .line 335
    :cond_d
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjValNameArray()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string/jumbo v4, "adj3"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 336
    rsub-int v0, v0, 0x5460

    .line 339
    :cond_e
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "LeftRightArrowCallout"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 340
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjValNameArray()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string/jumbo v4, "adj2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 341
    rsub-int v0, v0, 0x2a30

    .line 343
    :cond_f
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjValNameArray()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string/jumbo v4, "adj3"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 344
    div-int/lit8 v0, v0, 0x2

    .line 346
    :cond_10
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjValNameArray()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string/jumbo v4, "adj4"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 347
    rsub-int v3, v0, 0x5460

    div-int/lit8 v0, v3, 0x2

    .line 350
    :cond_11
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    const-string/jumbo v4, "CurvedRightArrow"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 356
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getAdjValNameArray()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string/jumbo v4, "adj3"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 357
    rsub-int v0, v0, 0x5460

    .line 363
    .end local v0    # "convrtValue":I
    .end local v1    # "fmlaValue":Ljava/lang/String;
    .end local v2    # "val":I
    :cond_12
    :goto_0
    return v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAdjValArray()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->formulas:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getAdjValNameArray()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 250
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->formulaNames:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getAdjValue()I
    .locals 1

    .prologue
    .line 631
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->adjVal:I

    return v0
.end method

.method public getAnchor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->anchor:Ljava/lang/String;

    return-object v0
.end method

.method public getCellColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .locals 1

    .prologue
    .line 556
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->cellColor:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    return-object v0
.end method

.method public getChildShH()J
    .locals 2

    .prologue
    .line 176
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->childShH:J

    return-wide v0
.end method

.method public getChildShW()J
    .locals 2

    .prologue
    .line 172
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->childShW:J

    return-wide v0
.end method

.method public getChildX()J
    .locals 2

    .prologue
    .line 159
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->childShX:J

    return-wide v0
.end method

.method public getChildY()J
    .locals 2

    .prologue
    .line 163
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->childShY:J

    return-wide v0
.end method

.method public getDiagLineProps()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;",
            ">;"
        }
    .end annotation

    .prologue
    .line 601
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->diagonalBrdr:Ljava/util/HashMap;

    return-object v0
.end method

.method public getFillAlpha()J
    .locals 2

    .prologue
    .line 655
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->fillAlpha:J

    return-wide v0
.end method

.method public getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    .locals 1

    .prologue
    .line 589
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    return-object v0
.end method

.method public getFillRefColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 564
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->fillRefColor:Ljava/lang/String;

    return-object v0
.end method

.method public getFillRefId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 552
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->fillRefId:Ljava/lang/String;

    return-object v0
.end method

.method public getFontRef()Ljava/lang/String;
    .locals 1

    .prologue
    .line 572
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->fontRef:Ljava/lang/String;

    return-object v0
.end method

.method public getGradFillColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 647
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->gradFillColor:Ljava/lang/String;

    return-object v0
.end method

.method public getGradFillPathName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->gradFillPathName:Ljava/lang/String;

    return-object v0
.end method

.method public getGridSpan()I
    .locals 1

    .prologue
    .line 440
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->gridSpan:I

    return v0
.end method

.method public getHMerge()Z
    .locals 1

    .prologue
    .line 436
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->hMerge:Z

    return v0
.end method

.method public getIsDiagonalBorder()Z
    .locals 1

    .prologue
    .line 597
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isDiagonalBorder:Z

    return v0
.end method

.method public getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    .locals 1

    .prologue
    .line 585
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    return-object v0
.end method

.method public getLinePropsList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;",
            ">;"
        }
    .end annotation

    .prologue
    .line 232
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->linePropsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getLineRefColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 581
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->lineRefColor:Ljava/lang/String;

    return-object v0
.end method

.method public getLumMod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 544
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->lumMod:Ljava/lang/String;

    return-object v0
.end method

.method public getMarginBottom()Ljava/lang/String;
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->marginBottom:Ljava/lang/String;

    return-object v0
.end method

.method public getMarginLeft()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->marginLeft:Ljava/lang/String;

    return-object v0
.end method

.method public getMarginRight()Ljava/lang/String;
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->marginRight:Ljava/lang/String;

    return-object v0
.end method

.method public getMarginTop()Ljava/lang/String;
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->marginTop:Ljava/lang/String;

    return-object v0
.end method

.method public getPathDescriptor()Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    return-object v0
.end method

.method public getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->placeHolder:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    return-object v0
.end method

.method public getPrstName()Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->shapeType:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    return-object v0
.end method

.method public getPrstShapeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    return-object v0
.end method

.method public getRefId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 659
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->refId:Ljava/lang/String;

    return-object v0
.end method

.method public getRotation()I
    .locals 1

    .prologue
    .line 476
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->rot:I

    return v0
.end method

.method public getRowSpan()I
    .locals 1

    .prologue
    .line 258
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->rowSpan:I

    return v0
.end method

.method public getSDRatio()F
    .locals 1

    .prologue
    .line 679
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->sdRatio:F

    return v0
.end method

.method public getShapeHeight()J
    .locals 2

    .prologue
    .line 504
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->shH:J

    return-wide v0
.end method

.method public getShapeId()I
    .locals 1

    .prologue
    .line 500
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->shapeId:I

    return v0
.end method

.method public getShapeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 496
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->shapeName:Ljava/lang/String;

    return-object v0
.end method

.method public getShapeWidth()J
    .locals 2

    .prologue
    .line 508
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->shW:J

    return-wide v0
.end method

.method public getShapeXvalue()J
    .locals 2

    .prologue
    .line 528
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->shX:J

    return-wide v0
.end method

.method public getShapeYvalue()J
    .locals 2

    .prologue
    .line 532
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->shY:J

    return-wide v0
.end method

.method public getTextRotation()I
    .locals 1

    .prologue
    .line 639
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->textRotation:I

    return v0
.end method

.method public getVMerge()Z
    .locals 1

    .prologue
    .line 432
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->vMerge:Z

    return v0
.end method

.method public isDiagramShape()Z
    .locals 1

    .prologue
    .line 619
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isDiagramShape:Z

    return v0
.end method

.method public isFlipH()Z
    .locals 1

    .prologue
    .line 488
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->flipH:Z

    return v0
.end method

.method public isFlipV()Z
    .locals 1

    .prologue
    .line 492
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->flipV:Z

    return v0
.end method

.method public isTextBox()Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isTextBox:Z

    return v0
.end method

.method public isTransform()Z
    .locals 1

    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isTransformSet:Z

    return v0
.end method

.method public setAdjValue(I)V
    .locals 0
    .param p1, "adjVal"    # I

    .prologue
    .line 627
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->adjVal:I

    .line 628
    return-void
.end method

.method public setAdjustValues(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 237
    .local p1, "formulaNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p2, "formulas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->formulaNames:Ljava/util/ArrayList;

    .line 238
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->formulas:Ljava/util/ArrayList;

    .line 239
    return-void
.end method

.method public setAnchor(Ljava/lang/String;)V
    .locals 0
    .param p1, "anchor"    # Ljava/lang/String;

    .prologue
    .line 200
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->anchor:Ljava/lang/String;

    .line 201
    return-void
.end method

.method public setCellColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V
    .locals 0
    .param p1, "cellColor"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .prologue
    .line 536
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->cellColor:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .line 537
    return-void
.end method

.method public setChildPoints(JJ)V
    .locals 1
    .param p1, "x"    # J
    .param p3, "y"    # J

    .prologue
    .line 154
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->childShX:J

    .line 155
    iput-wide p3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->childShY:J

    .line 156
    return-void
.end method

.method public setChildSize(JJ)V
    .locals 1
    .param p1, "w"    # J
    .param p3, "h"    # J

    .prologue
    .line 167
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->childShW:J

    .line 168
    iput-wide p3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->childShH:J

    .line 169
    return-void
.end method

.method public setDiagonalBorder(Z)V
    .locals 0
    .param p1, "diagonalBrdr"    # Z

    .prologue
    .line 593
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isDiagonalBorder:Z

    .line 594
    return-void
.end method

.method public setDiagramShape(Z)V
    .locals 0
    .param p1, "isDiagramShape"    # Z

    .prologue
    .line 623
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isDiagramShape:Z

    .line 624
    return-void
.end method

.method public setFillAlpha(J)V
    .locals 1
    .param p1, "alpha"    # J

    .prologue
    .line 651
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->fillAlpha:J

    .line 652
    return-void
.end method

.method public setFillProps(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 0
    .param p1, "fillProps"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 667
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .line 668
    return-void
.end method

.method public setFillRefColor(Ljava/lang/String;)V
    .locals 0
    .param p1, "fillRefColor"    # Ljava/lang/String;

    .prologue
    .line 560
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->fillRefColor:Ljava/lang/String;

    .line 561
    return-void
.end method

.method public setFillRefId(Ljava/lang/String;)V
    .locals 0
    .param p1, "fillRefId"    # Ljava/lang/String;

    .prologue
    .line 548
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->fillRefId:Ljava/lang/String;

    .line 549
    return-void
.end method

.method public setFlipH(Z)V
    .locals 0
    .param p1, "flipH"    # Z

    .prologue
    .line 480
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->flipH:Z

    .line 481
    return-void
.end method

.method public setFlipV(Z)V
    .locals 0
    .param p1, "flipV"    # Z

    .prologue
    .line 484
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->flipV:Z

    .line 485
    return-void
.end method

.method public setFontRefColor(Ljava/lang/String;)V
    .locals 0
    .param p1, "fontRef"    # Ljava/lang/String;

    .prologue
    .line 568
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->fontRef:Ljava/lang/String;

    .line 569
    return-void
.end method

.method public setGradFillColor(Ljava/lang/String;)V
    .locals 0
    .param p1, "gradFillColor"    # Ljava/lang/String;

    .prologue
    .line 643
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->gradFillColor:Ljava/lang/String;

    .line 644
    return-void
.end method

.method public setGradFillPathName(Ljava/lang/String;)V
    .locals 0
    .param p1, "gradFillPathName"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->gradFillPathName:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public setGridSpan(I)V
    .locals 0
    .param p1, "gridSpan"    # I

    .prologue
    .line 212
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->gridSpan:I

    .line 213
    return-void
.end method

.method public setHMerge(Z)V
    .locals 0
    .param p1, "hMerge"    # Z

    .prologue
    .line 216
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->hMerge:Z

    .line 217
    return-void
.end method

.method public setLineProps(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V
    .locals 0
    .param p1, "lineProps"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .prologue
    .line 671
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .line 672
    return-void
.end method

.method public setLineRefColor(Ljava/lang/String;)V
    .locals 0
    .param p1, "lineRefColor"    # Ljava/lang/String;

    .prologue
    .line 577
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->lineRefColor:Ljava/lang/String;

    .line 578
    return-void
.end method

.method public setLumMod(Ljava/lang/String;)V
    .locals 0
    .param p1, "lumMod"    # Ljava/lang/String;

    .prologue
    .line 540
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->lumMod:Ljava/lang/String;

    .line 541
    return-void
.end method

.method public setMarginBottom(Ljava/lang/String;)V
    .locals 0
    .param p1, "marginBottom"    # Ljava/lang/String;

    .prologue
    .line 196
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->marginBottom:Ljava/lang/String;

    .line 197
    return-void
.end method

.method public setMarginLeft(Ljava/lang/String;)V
    .locals 0
    .param p1, "marginLeft"    # Ljava/lang/String;

    .prologue
    .line 184
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->marginLeft:Ljava/lang/String;

    .line 185
    return-void
.end method

.method public setMarginRight(Ljava/lang/String;)V
    .locals 0
    .param p1, "marginRight"    # Ljava/lang/String;

    .prologue
    .line 188
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->marginRight:Ljava/lang/String;

    .line 189
    return-void
.end method

.method public setMarginTop(Ljava/lang/String;)V
    .locals 0
    .param p1, "marginTop"    # Ljava/lang/String;

    .prologue
    .line 192
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->marginTop:Ljava/lang/String;

    .line 193
    return-void
.end method

.method public setPathDescriptor(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V
    .locals 0
    .param p1, "pathDesc"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    .prologue
    .line 242
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    .line 243
    return-void
.end method

.method public setPlaceHolder(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;)V
    .locals 0
    .param p1, "placeHolder"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->placeHolder:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    .line 108
    return-void
.end method

.method public setPoints(JJ)V
    .locals 3
    .param p1, "x"    # J
    .param p3, "y"    # J

    .prologue
    .line 139
    invoke-static {p1, p2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->toPoints(J)D

    move-result-wide v0

    double-to-long v0, v0

    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->shX:J

    .line 140
    invoke-static {p3, p4}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->toPoints(J)D

    move-result-wide v0

    double-to-long v0, v0

    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->shY:J

    .line 141
    return-void
.end method

.method public setPrstName(Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;)V
    .locals 0
    .param p1, "shapeType"    # Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .prologue
    .line 208
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->shapeType:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 209
    return-void
.end method

.method public setPrstShapeName(Ljava/lang/String;)V
    .locals 0
    .param p1, "prstValue"    # Ljava/lang/String;

    .prologue
    .line 204
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->prstValue:Ljava/lang/String;

    .line 205
    return-void
.end method

.method public setRefId(Ljava/lang/String;)V
    .locals 0
    .param p1, "refId"    # Ljava/lang/String;

    .prologue
    .line 663
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->refId:Ljava/lang/String;

    .line 664
    return-void
.end method

.method public setRotation(I)V
    .locals 0
    .param p1, "rot"    # I

    .prologue
    .line 180
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->rot:I

    .line 181
    return-void
.end method

.method public setRowSpan(I)V
    .locals 0
    .param p1, "rowSpan"    # I

    .prologue
    .line 224
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->rowSpan:I

    .line 225
    return-void
.end method

.method public setSDRatio(F)V
    .locals 0
    .param p1, "sdRatio"    # F

    .prologue
    .line 675
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->sdRatio:F

    .line 676
    return-void
.end method

.method public setShapeId(I)V
    .locals 0
    .param p1, "shapeId"    # I

    .prologue
    .line 115
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->shapeId:I

    .line 116
    return-void
.end method

.method public setShapeName(Ljava/lang/String;)V
    .locals 0
    .param p1, "shapeName"    # Ljava/lang/String;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->shapeName:Ljava/lang/String;

    .line 120
    return-void
.end method

.method public setShapeXvalue(J)V
    .locals 1
    .param p1, "shX"    # J

    .prologue
    .line 520
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->shX:J

    .line 521
    return-void
.end method

.method public setShapeYvalue(J)V
    .locals 1
    .param p1, "shY"    # J

    .prologue
    .line 524
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->shY:J

    .line 525
    return-void
.end method

.method public setSize(JJ)V
    .locals 5
    .param p1, "w"    # J
    .param p3, "h"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 144
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->orgShH:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->orgShW:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 146
    iput-wide p3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->orgShH:J

    .line 147
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->orgShW:J

    .line 149
    :cond_0
    invoke-static {p1, p2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->toPoints(J)D

    move-result-wide v0

    double-to-long v0, v0

    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->shW:J

    .line 150
    invoke-static {p3, p4}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->toPoints(J)D

    move-result-wide v0

    double-to-long v0, v0

    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->shH:J

    .line 151
    return-void
.end method

.method public setTextBox(Z)V
    .locals 0
    .param p1, "txBox"    # Z

    .prologue
    .line 123
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isTextBox:Z

    .line 124
    return-void
.end method

.method public setTextRotation(I)V
    .locals 0
    .param p1, "textRotation"    # I

    .prologue
    .line 635
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->textRotation:I

    .line 636
    return-void
.end method

.method public setTransform()V
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isTransformSet:Z

    .line 132
    return-void
.end method

.method public setVMerge(Z)V
    .locals 0
    .param p1, "vMerge"    # Z

    .prologue
    .line 220
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->vMerge:Z

    .line 221
    return-void
.end method
