.class public Lcom/samsung/thumbnail/office/point/shapes/StarShapes;
.super Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;
.source "StarShapes.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mDrawOnCanvas:Z

.field private mLeftMarg:I

.field private mTopMarg:I


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "folderPath"    # Ljava/io/File;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;-><init>()V

    .line 24
    const-string/jumbo v0, "StarShapes"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->TAG:Ljava/lang/String;

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mDrawOnCanvas:Z

    .line 30
    iput-object p1, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->folderPath:Ljava/io/File;

    .line 31
    return-void
.end method


# virtual methods
.method protected draw(Landroid/graphics/Canvas;I)V
    .locals 40
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeCount"    # I

    .prologue
    .line 152
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->width:F

    const v3, 0x46a8c000    # 21600.0f

    div-float v36, v2, v3

    .line 153
    .local v36, "mulFactor_Width":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->height:F

    const v3, 0x46a8c000    # 21600.0f

    div-float v35, v2, v3

    .line 155
    .local v35, "mulFactor_Height":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "Star4"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "Seal4"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 163
    :cond_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->adjval0:I

    if-nez v2, :cond_1

    .line 164
    const/16 v2, 0x1fa4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->adjval0:I

    .line 167
    :cond_1
    new-instance v38, Landroid/graphics/Path;

    invoke-direct/range {v38 .. v38}, Landroid/graphics/Path;-><init>()V

    .line 168
    .local v38, "path":Landroid/graphics/Path;
    const v2, 0x46a8c000    # 21600.0f

    mul-float v2, v2, v36

    const v3, 0x4628c000    # 10800.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 169
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->adjval0:I

    rsub-int v2, v2, 0x2a30

    mul-int/lit16 v2, v2, 0x5a82

    const v3, 0x8000

    div-int/2addr v2, v3

    add-int/lit16 v2, v2, 0x2a30

    int-to-float v2, v2

    mul-float v2, v2, v36

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->adjval0:I

    rsub-int v3, v3, 0x2a30

    mul-int/lit16 v3, v3, 0x5a82

    const v4, 0x8000

    div-int/2addr v3, v4

    rsub-int v3, v3, 0x2a30

    int-to-float v3, v3

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 173
    const v2, 0x4628c000    # 10800.0f

    mul-float v2, v2, v36

    const/4 v3, 0x0

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 174
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->adjval0:I

    rsub-int v2, v2, 0x2a30

    mul-int/lit16 v2, v2, 0x5a82

    const v3, 0x8000

    div-int/2addr v2, v3

    rsub-int v2, v2, 0x2a30

    int-to-float v2, v2

    mul-float v2, v2, v36

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->adjval0:I

    rsub-int v3, v3, 0x2a30

    mul-int/lit16 v3, v3, 0x5a82

    const v4, 0x8000

    div-int/2addr v3, v4

    rsub-int v3, v3, 0x2a30

    int-to-float v3, v3

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 178
    const/4 v2, 0x0

    const v3, 0x4628c000    # 10800.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 179
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->adjval0:I

    rsub-int v2, v2, 0x2a30

    mul-int/lit16 v2, v2, 0x5a82

    const v3, 0x8000

    div-int/2addr v2, v3

    rsub-int v2, v2, 0x2a30

    int-to-float v2, v2

    mul-float v2, v2, v36

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->adjval0:I

    rsub-int v3, v3, 0x2a30

    mul-int/lit16 v3, v3, 0x5a82

    const v4, 0x8000

    div-int/2addr v3, v4

    add-int/lit16 v3, v3, 0x2a30

    int-to-float v3, v3

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 183
    const v2, 0x4628c000    # 10800.0f

    mul-float v2, v2, v36

    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 184
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->adjval0:I

    rsub-int v2, v2, 0x2a30

    mul-int/lit16 v2, v2, 0x5a82

    const v3, 0x8000

    div-int/2addr v2, v3

    add-int/lit16 v2, v2, 0x2a30

    int-to-float v2, v2

    mul-float v2, v2, v36

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->adjval0:I

    rsub-int v3, v3, 0x2a30

    mul-int/lit16 v3, v3, 0x5a82

    const v4, 0x8000

    div-int/2addr v3, v4

    add-int/lit16 v3, v3, 0x2a30

    int-to-float v3, v3

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 188
    invoke-virtual/range {v38 .. v38}, Landroid/graphics/Path;->close()V

    .line 190
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v2, :cond_2

    .line 191
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 192
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_3

    .line 193
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 197
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x461e3400    # 10125.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 198
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x461e3400    # 10125.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 199
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x46334c00    # 11475.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 200
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x46334c00    # 11475.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    .line 559
    .end local v38    # "path":Landroid/graphics/Path;
    :cond_4
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mDrawOnCanvas:Z

    if-nez v2, :cond_5

    .line 560
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 563
    .local v7, "mtx":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->rotation:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->width:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->height:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-virtual {v7, v2, v3, v4}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 565
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->bitmap:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->bitmapWidth:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->bitmapHight:I

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v39

    .line 570
    .local v39, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v37, Ljava/io/FileOutputStream;

    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->folderPath:Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "pic_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->shapeName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ".png"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v37

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 576
    .local v37, "out":Ljava/io/FileOutputStream;
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    move-object/from16 v0, v39

    move-object/from16 v1, v37

    invoke-virtual {v0, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 577
    invoke-virtual/range {v37 .. v37}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 582
    .end local v7    # "mtx":Landroid/graphics/Matrix;
    .end local v37    # "out":Ljava/io/FileOutputStream;
    .end local v39    # "rotatedBMP":Landroid/graphics/Bitmap;
    :cond_5
    :goto_1
    return-void

    .line 203
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "StarShapes"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "Star"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "Star5"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 207
    :cond_7
    new-instance v38, Landroid/graphics/Path;

    invoke-direct/range {v38 .. v38}, Landroid/graphics/Path;-><init>()V

    .line 208
    .restart local v38    # "path":Landroid/graphics/Path;
    const v2, 0x4628c000    # 10800.0f

    mul-float v2, v2, v36

    const/4 v3, 0x0

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 209
    const v2, 0x46016000    # 8280.0f

    mul-float v2, v2, v36

    const v3, 0x46010c00    # 8259.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 210
    const/4 v2, 0x0

    mul-float v2, v2, v36

    const v3, 0x46010c00    # 8259.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 211
    const/high16 v2, 0x45d20000    # 6720.0f

    mul-float v2, v2, v36

    const v3, 0x45a0d000    # 5146.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 212
    const v2, 0x45834000    # 4200.0f

    mul-float v2, v2, v36

    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 213
    const v2, 0x45ce4000    # 6600.0f

    mul-float v2, v2, v36

    const v3, -0x3a632800    # -5019.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 214
    const v2, 0x4687f000    # 17400.0f

    mul-float v2, v2, v36

    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 215
    const v2, 0x46688000    # 14880.0f

    mul-float v2, v2, v36

    const v3, 0x46517400    # 13405.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 216
    const v2, 0x46a8c000    # 21600.0f

    mul-float v2, v2, v36

    const v3, 0x46010c00    # 8259.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 217
    const v2, -0x39fea000    # -8280.0f

    mul-float v2, v2, v36

    const/4 v3, 0x0

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 218
    invoke-virtual/range {v38 .. v38}, Landroid/graphics/Path;->close()V

    .line 220
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v2, :cond_8

    .line 221
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 222
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_9

    .line 223
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 227
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x45e67800    # 7375.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 228
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x4613a800    # 9450.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 229
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x465e4400    # 14225.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 230
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x46680800    # 14850.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 233
    .end local v38    # "path":Landroid/graphics/Path;
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "Star8"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "Seal8"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 239
    :cond_b
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->adjval0:I

    if-nez v2, :cond_c

    .line 240
    const/16 v2, 0x9ea

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->adjval0:I

    .line 242
    :cond_c
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->adjval0:I

    rsub-int v2, v2, 0x2a30

    mul-int/lit16 v2, v2, 0x7642

    int-to-float v2, v2

    const/high16 v3, 0x47000000    # 32768.0f

    div-float v10, v2, v3

    .line 243
    .local v10, "at1":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->adjval0:I

    rsub-int v2, v2, 0x2a30

    mul-int/lit16 v2, v2, 0x30fc

    int-to-float v2, v2

    const/high16 v3, 0x47000000    # 32768.0f

    div-float v21, v2, v3

    .line 244
    .local v21, "at2":F
    const v2, 0x4628c000    # 10800.0f

    add-float v27, v10, v2

    .line 245
    .local v27, "at3":F
    const v2, 0x4628c000    # 10800.0f

    add-float v28, v21, v2

    .line 246
    .local v28, "at4":F
    const v2, 0x4628c000    # 10800.0f

    sub-float v29, v2, v10

    .line 247
    .local v29, "at5":F
    const v2, 0x4628c000    # 10800.0f

    sub-float v30, v2, v21

    .line 249
    .local v30, "at6":F
    new-instance v38, Landroid/graphics/Path;

    invoke-direct/range {v38 .. v38}, Landroid/graphics/Path;-><init>()V

    .line 250
    .restart local v38    # "path":Landroid/graphics/Path;
    const v2, 0x46a8c000    # 21600.0f

    mul-float v2, v2, v36

    const v3, 0x4628c000    # 10800.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 251
    mul-float v2, v27, v36

    mul-float v3, v30, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 252
    const v2, 0x46900800    # 18436.0f

    mul-float v2, v2, v36

    const v3, 0x4545b000    # 3163.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 253
    mul-float v2, v28, v36

    mul-float v3, v29, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 254
    const v2, 0x4628c000    # 10800.0f

    mul-float v2, v2, v36

    const/4 v3, 0x0

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 255
    mul-float v2, v30, v36

    mul-float v3, v29, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 256
    const v2, 0x4545b000    # 3163.0f

    mul-float v2, v2, v36

    const v3, 0x4545b000    # 3163.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 257
    mul-float v2, v29, v36

    mul-float v3, v30, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 258
    const/4 v2, 0x0

    mul-float v2, v2, v36

    const v3, 0x4628c000    # 10800.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 259
    mul-float v2, v29, v36

    mul-float v3, v28, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 260
    const v2, 0x4545b000    # 3163.0f

    mul-float v2, v2, v36

    const v3, 0x46900800    # 18436.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 261
    mul-float v2, v30, v36

    mul-float v3, v27, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 262
    const v2, 0x4628c000    # 10800.0f

    mul-float v2, v2, v36

    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 263
    mul-float v2, v28, v36

    mul-float v3, v27, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 264
    const v2, 0x46900800    # 18436.0f

    mul-float v2, v2, v36

    const v3, 0x46900800    # 18436.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 265
    mul-float v2, v27, v36

    mul-float v3, v28, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 266
    invoke-virtual/range {v38 .. v38}, Landroid/graphics/Path;->close()V

    .line 268
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v2, :cond_d

    .line 269
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 270
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_e

    .line 271
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 275
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x4593a800    # 4725.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 276
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x4593a800    # 4725.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 277
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x4683d600    # 16875.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 278
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x4683d600    # 16875.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 281
    .end local v10    # "at1":F
    .end local v21    # "at2":F
    .end local v27    # "at3":F
    .end local v28    # "at4":F
    .end local v29    # "at5":F
    .end local v30    # "at6":F
    .end local v38    # "path":Landroid/graphics/Path;
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "Star16"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "Seal16"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 287
    :cond_10
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->adjval0:I

    if-nez v2, :cond_11

    .line 288
    const/16 v2, 0x9ea

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->adjval0:I

    .line 290
    :cond_11
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->adjval0:I

    rsub-int v2, v2, 0x2a30

    int-to-float v9, v2

    .line 291
    .local v9, "at0":F
    const v2, 0x46fb1400    # 32138.0f

    mul-float/2addr v2, v9

    const/high16 v3, 0x47000000    # 32768.0f

    div-float v10, v2, v3

    .line 292
    .restart local v10    # "at1":F
    const v2, 0x45c7c800    # 6393.0f

    mul-float/2addr v2, v9

    const/high16 v3, 0x47000000    # 32768.0f

    div-float v21, v2, v3

    .line 293
    .restart local v21    # "at2":F
    const v2, 0x46d4dc00    # 27246.0f

    mul-float/2addr v2, v9

    const/high16 v3, 0x47000000    # 32768.0f

    div-float v27, v2, v3

    .line 294
    .restart local v27    # "at3":F
    const v2, 0x468e3a00    # 18205.0f

    mul-float/2addr v2, v9

    const/high16 v3, 0x47000000    # 32768.0f

    div-float v28, v2, v3

    .line 295
    .restart local v28    # "at4":F
    const v2, 0x4628c000    # 10800.0f

    add-float v29, v10, v2

    .line 296
    .restart local v29    # "at5":F
    const v2, 0x4628c000    # 10800.0f

    add-float v30, v21, v2

    .line 297
    .restart local v30    # "at6":F
    const v2, 0x4628c000    # 10800.0f

    add-float v31, v27, v2

    .line 298
    .local v31, "at7":F
    const v2, 0x4628c000    # 10800.0f

    add-float v32, v28, v2

    .line 299
    .local v32, "at8":F
    const v2, 0x4628c000    # 10800.0f

    sub-float v33, v2, v10

    .line 300
    .local v33, "at9":F
    const v2, 0x4628c000    # 10800.0f

    sub-float v11, v2, v21

    .line 301
    .local v11, "at10":F
    const v2, 0x4628c000    # 10800.0f

    sub-float v12, v2, v27

    .line 302
    .local v12, "at11":F
    const v2, 0x4628c000    # 10800.0f

    sub-float v13, v2, v28

    .line 304
    .local v13, "at12":F
    new-instance v38, Landroid/graphics/Path;

    invoke-direct/range {v38 .. v38}, Landroid/graphics/Path;-><init>()V

    .line 305
    .restart local v38    # "path":Landroid/graphics/Path;
    const v2, 0x46a8c000    # 21600.0f

    mul-float v2, v2, v36

    const v3, 0x4628c000    # 10800.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 306
    mul-float v2, v29, v36

    mul-float v3, v11, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 307
    const v2, 0x46a25200    # 20777.0f

    mul-float v2, v2, v36

    const v3, 0x45d05800    # 6667.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 308
    mul-float v2, v31, v36

    mul-float v3, v13, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 309
    const v2, 0x46900800    # 18436.0f

    mul-float v2, v2, v36

    const v3, 0x4545b000    # 3163.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 310
    mul-float v2, v32, v36

    mul-float v3, v12, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 311
    const v2, 0x46618000    # 14432.0f

    mul-float v2, v2, v36

    const v3, 0x444d8000    # 822.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 312
    mul-float v2, v30, v36

    mul-float v3, v33, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 313
    const v2, 0x4628c000    # 10800.0f

    mul-float v2, v2, v36

    const/4 v3, 0x0

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 314
    mul-float v2, v11, v36

    mul-float v3, v33, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 315
    const v2, 0x45d05800    # 6667.0f

    mul-float v2, v2, v36

    const v3, 0x444d8000    # 822.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 316
    mul-float v2, v13, v36

    mul-float v3, v12, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 317
    const v2, 0x4545b000    # 3163.0f

    mul-float v2, v2, v36

    const v3, 0x4545b000    # 3163.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 318
    mul-float v2, v12, v36

    mul-float v3, v13, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 319
    const v2, 0x444d8000    # 822.0f

    mul-float v2, v2, v36

    const v3, 0x45d05800    # 6667.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 320
    mul-float v2, v33, v36

    mul-float v3, v11, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 321
    const/4 v2, 0x0

    mul-float v2, v2, v36

    const v3, 0x4628c000    # 10800.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 322
    mul-float v2, v33, v36

    mul-float v3, v30, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 323
    const v2, 0x444d8000    # 822.0f

    mul-float v2, v2, v36

    const v3, 0x46695000    # 14932.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 324
    mul-float v2, v12, v36

    mul-float v3, v32, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 325
    const v2, 0x4545b000    # 3163.0f

    mul-float v2, v2, v36

    const v3, 0x46900800    # 18436.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 326
    mul-float v2, v13, v36

    mul-float v3, v31, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 327
    const v2, 0x45d05800    # 6667.0f

    mul-float v2, v2, v36

    const v3, 0x46a25200    # 20777.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 328
    mul-float v2, v11, v36

    mul-float v3, v29, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 329
    const v2, 0x4628c000    # 10800.0f

    mul-float v2, v2, v36

    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 330
    mul-float v2, v30, v36

    mul-float v3, v29, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 331
    const v2, 0x46695000    # 14932.0f

    mul-float v2, v2, v36

    const v3, 0x46a25200    # 20777.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 332
    mul-float v2, v32, v36

    mul-float v3, v31, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 333
    const v2, 0x46900800    # 18436.0f

    mul-float v2, v2, v36

    const v3, 0x46900800    # 18436.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 334
    mul-float v2, v31, v36

    mul-float v3, v32, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 335
    const v2, 0x46a25200    # 20777.0f

    mul-float v2, v2, v36

    const v3, 0x46695000    # 14932.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 336
    mul-float v2, v29, v36

    mul-float v3, v30, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 337
    invoke-virtual/range {v38 .. v38}, Landroid/graphics/Path;->close()V

    .line 339
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v2, :cond_12

    .line 340
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 341
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_13

    .line 342
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 346
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x45bdd800    # 6075.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 347
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x45bdd800    # 6075.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 348
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x46729400    # 15525.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 349
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x46729400    # 15525.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 352
    .end local v9    # "at0":F
    .end local v10    # "at1":F
    .end local v11    # "at10":F
    .end local v12    # "at11":F
    .end local v13    # "at12":F
    .end local v21    # "at2":F
    .end local v27    # "at3":F
    .end local v28    # "at4":F
    .end local v29    # "at5":F
    .end local v30    # "at6":F
    .end local v31    # "at7":F
    .end local v32    # "at8":F
    .end local v33    # "at9":F
    .end local v38    # "path":Landroid/graphics/Path;
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "Star24"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "Seal24"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 358
    :cond_15
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->adjval0:I

    if-nez v2, :cond_16

    .line 359
    const/16 v2, 0xa8c

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->adjval0:I

    .line 361
    :cond_16
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->adjval0:I

    rsub-int v2, v2, 0x2a30

    int-to-float v9, v2

    .line 362
    .restart local v9    # "at0":F
    const v2, 0x46fdd000    # 32488.0f

    mul-float/2addr v2, v9

    const/high16 v3, 0x47000000    # 32768.0f

    div-float v10, v2, v3

    .line 363
    .restart local v10    # "at1":F
    const v2, 0x4585a800    # 4277.0f

    mul-float/2addr v2, v9

    const/high16 v3, 0x47000000    # 32768.0f

    div-float v21, v2, v3

    .line 364
    .restart local v21    # "at2":F
    const v2, 0x46ec8400    # 30274.0f

    mul-float/2addr v2, v9

    const/high16 v3, 0x47000000    # 32768.0f

    div-float v27, v2, v3

    .line 365
    .restart local v27    # "at3":F
    const v2, 0x4643f000    # 12540.0f

    mul-float/2addr v2, v9

    const/high16 v3, 0x47000000    # 32768.0f

    div-float v28, v2, v3

    .line 366
    .restart local v28    # "at4":F
    const v2, 0x46cb1a00    # 25997.0f

    mul-float/2addr v2, v9

    const/high16 v3, 0x47000000    # 32768.0f

    div-float v29, v2, v3

    .line 367
    .restart local v29    # "at5":F
    const v2, 0x469bd800    # 19948.0f

    mul-float/2addr v2, v9

    const/high16 v3, 0x47000000    # 32768.0f

    div-float v30, v2, v3

    .line 368
    .restart local v30    # "at6":F
    const v2, 0x4628c000    # 10800.0f

    add-float v31, v10, v2

    .line 369
    .restart local v31    # "at7":F
    const v2, 0x4628c000    # 10800.0f

    add-float v32, v21, v2

    .line 370
    .restart local v32    # "at8":F
    const v2, 0x4628c000    # 10800.0f

    add-float v33, v27, v2

    .line 371
    .restart local v33    # "at9":F
    const v2, 0x4628c000    # 10800.0f

    add-float v11, v28, v2

    .line 372
    .restart local v11    # "at10":F
    const v2, 0x4628c000    # 10800.0f

    add-float v12, v29, v2

    .line 373
    .restart local v12    # "at11":F
    const v2, 0x4628c000    # 10800.0f

    add-float v13, v30, v2

    .line 374
    .restart local v13    # "at12":F
    const v2, 0x4628c000    # 10800.0f

    sub-float v14, v2, v10

    .line 375
    .local v14, "at13":F
    const v2, 0x4628c000    # 10800.0f

    sub-float v15, v2, v21

    .line 376
    .local v15, "at14":F
    const v2, 0x4628c000    # 10800.0f

    sub-float v16, v2, v27

    .line 377
    .local v16, "at15":F
    const v2, 0x4628c000    # 10800.0f

    sub-float v17, v2, v28

    .line 378
    .local v17, "at16":F
    const v2, 0x4628c000    # 10800.0f

    sub-float v18, v2, v29

    .line 379
    .local v18, "at17":F
    const v2, 0x4628c000    # 10800.0f

    sub-float v19, v2, v30

    .line 381
    .local v19, "at18":F
    new-instance v38, Landroid/graphics/Path;

    invoke-direct/range {v38 .. v38}, Landroid/graphics/Path;-><init>()V

    .line 382
    .restart local v38    # "path":Landroid/graphics/Path;
    const v2, 0x46a8c000    # 21600.0f

    mul-float v2, v2, v36

    const v3, 0x4628c000    # 10800.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 383
    mul-float v2, v31, v36

    mul-float v3, v15, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 384
    const v2, 0x46a5e000    # 21232.0f

    mul-float v2, v2, v36

    const v3, 0x45fa2800    # 8005.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 385
    mul-float v2, v33, v36

    mul-float v3, v17, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 386
    const v2, 0x469d7200    # 20153.0f

    mul-float v2, v2, v36

    const v3, 0x45a8c000    # 5400.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 387
    mul-float v2, v12, v36

    mul-float v3, v19, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 388
    const v2, 0x46900a00    # 18437.0f

    mul-float v2, v2, v36

    const v3, 0x4545b000    # 3163.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 389
    mul-float v2, v13, v36

    mul-float v3, v18, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 390
    const v2, 0x467d2000    # 16200.0f

    mul-float v2, v2, v36

    const v3, 0x44b4e000    # 1447.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 391
    mul-float v2, v11, v36

    mul-float v3, v16, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 392
    const v2, 0x46546c00    # 13595.0f

    mul-float v2, v2, v36

    const/high16 v3, 0x43b80000    # 368.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 393
    mul-float v2, v32, v36

    mul-float v3, v14, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 394
    const v2, 0x4628c000    # 10800.0f

    mul-float v2, v2, v36

    const/4 v3, 0x0

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 395
    mul-float v2, v15, v36

    mul-float v3, v14, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 396
    const v2, 0x45fa2800    # 8005.0f

    mul-float v2, v2, v36

    const/high16 v3, 0x43b80000    # 368.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 397
    mul-float v2, v17, v36

    mul-float v3, v16, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 398
    const v2, 0x45a8c000    # 5400.0f

    mul-float v2, v2, v36

    const v3, 0x44b4e000    # 1447.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 399
    mul-float v2, v19, v36

    mul-float v3, v18, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 400
    const v2, 0x4545b000    # 3163.0f

    mul-float v2, v2, v36

    const v3, 0x4545b000    # 3163.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 401
    mul-float v2, v18, v36

    mul-float v3, v19, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 402
    const v2, 0x44b4e000    # 1447.0f

    mul-float v2, v2, v36

    const v3, 0x45a8c000    # 5400.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 403
    mul-float v2, v16, v36

    mul-float v3, v17, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 404
    const/high16 v2, 0x43b80000    # 368.0f

    mul-float v2, v2, v36

    const v3, 0x45fa2800    # 8005.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 405
    mul-float v2, v14, v36

    mul-float v3, v15, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 406
    const/4 v2, 0x0

    mul-float v2, v2, v36

    const v3, 0x4628c000    # 10800.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 407
    mul-float v2, v14, v36

    mul-float v3, v32, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 408
    const/high16 v2, 0x43b80000    # 368.0f

    mul-float v2, v2, v36

    const v3, 0x46546c00    # 13595.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 409
    mul-float v2, v16, v36

    mul-float v3, v11, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 410
    const v2, 0x44b4e000    # 1447.0f

    mul-float v2, v2, v36

    const v3, 0x467d2000    # 16200.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 411
    mul-float v2, v18, v36

    mul-float v3, v13, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 412
    const v2, 0x4545b000    # 3163.0f

    mul-float v2, v2, v36

    const v3, 0x46900a00    # 18437.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 413
    mul-float v2, v19, v36

    mul-float v3, v12, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 414
    const v2, 0x45a8c000    # 5400.0f

    mul-float v2, v2, v36

    const v3, 0x469d7200    # 20153.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 415
    mul-float v2, v17, v36

    mul-float v3, v33, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 416
    const v2, 0x45fa2800    # 8005.0f

    mul-float v2, v2, v36

    const v3, 0x46a5e000    # 21232.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 417
    mul-float v2, v15, v36

    mul-float v3, v31, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 418
    const v2, 0x4628c000    # 10800.0f

    mul-float v2, v2, v36

    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 419
    mul-float v2, v32, v36

    mul-float v3, v31, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 420
    const v2, 0x46546c00    # 13595.0f

    mul-float v2, v2, v36

    const v3, 0x46a5e000    # 21232.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 421
    mul-float v2, v11, v36

    mul-float v3, v33, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 422
    const v2, 0x467d2000    # 16200.0f

    mul-float v2, v2, v36

    const v3, 0x469d7200    # 20153.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 423
    mul-float v2, v13, v36

    mul-float v3, v12, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 424
    const v2, 0x46900a00    # 18437.0f

    mul-float v2, v2, v36

    const v3, 0x46900a00    # 18437.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 425
    mul-float v2, v12, v36

    mul-float v3, v13, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 426
    const v2, 0x469d7200    # 20153.0f

    mul-float v2, v2, v36

    const v3, 0x467d2000    # 16200.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 427
    mul-float v2, v33, v36

    mul-float v3, v11, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 428
    const v2, 0x46a5e000    # 21232.0f

    mul-float v2, v2, v36

    const v3, 0x46546c00    # 13595.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 429
    mul-float v2, v31, v36

    mul-float v3, v32, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 430
    invoke-virtual/range {v38 .. v38}, Landroid/graphics/Path;->close()V

    .line 432
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v2, :cond_17

    .line 433
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 434
    :cond_17
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_18

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_18

    .line 435
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 439
    :cond_18
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x45bdd800    # 6075.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 440
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x45bdd800    # 6075.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 441
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x46729400    # 15525.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 442
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x46729400    # 15525.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 445
    .end local v9    # "at0":F
    .end local v10    # "at1":F
    .end local v11    # "at10":F
    .end local v12    # "at11":F
    .end local v13    # "at12":F
    .end local v14    # "at13":F
    .end local v15    # "at14":F
    .end local v16    # "at15":F
    .end local v17    # "at16":F
    .end local v18    # "at17":F
    .end local v19    # "at18":F
    .end local v21    # "at2":F
    .end local v27    # "at3":F
    .end local v28    # "at4":F
    .end local v29    # "at5":F
    .end local v30    # "at6":F
    .end local v31    # "at7":F
    .end local v32    # "at8":F
    .end local v33    # "at9":F
    .end local v38    # "path":Landroid/graphics/Path;
    :cond_19
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "Star32"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "Seal32"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 450
    :cond_1a
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->adjval0:I

    if-nez v2, :cond_1b

    .line 451
    const/16 v2, 0xa8c

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->adjval0:I

    .line 453
    :cond_1b
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->adjval0:I

    rsub-int v2, v2, 0x2a30

    int-to-float v9, v2

    .line 454
    .restart local v9    # "at0":F
    const v2, 0x46fec400    # 32610.0f

    mul-float/2addr v2, v9

    const/high16 v3, 0x47000000    # 32768.0f

    div-float v10, v2, v3

    .line 455
    .restart local v10    # "at1":F
    const v2, 0x4548c000    # 3212.0f

    mul-float/2addr v2, v9

    const/high16 v3, 0x47000000    # 32768.0f

    div-float v21, v2, v3

    .line 456
    .restart local v21    # "at2":F
    const v2, 0x46f4fa00    # 31357.0f

    mul-float/2addr v2, v9

    const/high16 v3, 0x47000000    # 32768.0f

    div-float v27, v2, v3

    .line 457
    .restart local v27    # "at3":F
    const v2, 0x4614a000    # 9512.0f

    mul-float/2addr v2, v9

    const/high16 v3, 0x47000000    # 32768.0f

    div-float v28, v2, v3

    .line 458
    .restart local v28    # "at4":F
    const v2, 0x46e1c600    # 28899.0f

    mul-float/2addr v2, v9

    const/high16 v3, 0x47000000    # 32768.0f

    div-float v29, v2, v3

    .line 459
    .restart local v29    # "at5":F
    const v2, 0x46715c00    # 15447.0f

    mul-float/2addr v2, v9

    const/high16 v3, 0x47000000    # 32768.0f

    div-float v30, v2, v3

    .line 460
    .restart local v30    # "at6":F
    const v2, 0x46c5e400    # 25330.0f

    mul-float/2addr v2, v9

    const/high16 v3, 0x47000000    # 32768.0f

    div-float v31, v2, v3

    .line 461
    .restart local v31    # "at7":F
    const v2, 0x46a26800    # 20788.0f

    mul-float/2addr v2, v9

    const/high16 v3, 0x47000000    # 32768.0f

    div-float v32, v2, v3

    .line 462
    .restart local v32    # "at8":F
    const v2, 0x4628c000    # 10800.0f

    add-float v33, v10, v2

    .line 463
    .restart local v33    # "at9":F
    const v2, 0x4628c000    # 10800.0f

    add-float v11, v21, v2

    .line 464
    .restart local v11    # "at10":F
    const v2, 0x4628c000    # 10800.0f

    add-float v12, v27, v2

    .line 465
    .restart local v12    # "at11":F
    const v2, 0x4628c000    # 10800.0f

    add-float v13, v28, v2

    .line 466
    .restart local v13    # "at12":F
    const v2, 0x4628c000    # 10800.0f

    add-float v14, v29, v2

    .line 467
    .restart local v14    # "at13":F
    const v2, 0x4628c000    # 10800.0f

    add-float v15, v30, v2

    .line 468
    .restart local v15    # "at14":F
    const v2, 0x4628c000    # 10800.0f

    add-float v16, v31, v2

    .line 469
    .restart local v16    # "at15":F
    const v2, 0x4628c000    # 10800.0f

    add-float v17, v32, v2

    .line 470
    .restart local v17    # "at16":F
    const v2, 0x4628c000    # 10800.0f

    sub-float v18, v2, v10

    .line 471
    .restart local v18    # "at17":F
    const v2, 0x4628c000    # 10800.0f

    sub-float v19, v2, v21

    .line 472
    .restart local v19    # "at18":F
    const v2, 0x4628c000    # 10800.0f

    sub-float v20, v2, v27

    .line 473
    .local v20, "at19":F
    const v2, 0x4628c000    # 10800.0f

    sub-float v22, v2, v28

    .line 474
    .local v22, "at20":F
    const v2, 0x4628c000    # 10800.0f

    sub-float v23, v2, v29

    .line 475
    .local v23, "at21":F
    const v2, 0x4628c000    # 10800.0f

    sub-float v24, v2, v30

    .line 476
    .local v24, "at22":F
    const v2, 0x4628c000    # 10800.0f

    sub-float v25, v2, v31

    .line 477
    .local v25, "at23":F
    const v2, 0x4628c000    # 10800.0f

    sub-float v26, v2, v32

    .line 479
    .local v26, "at24":F
    new-instance v38, Landroid/graphics/Path;

    invoke-direct/range {v38 .. v38}, Landroid/graphics/Path;-><init>()V

    .line 480
    .restart local v38    # "path":Landroid/graphics/Path;
    const v2, 0x46a8c000    # 21600.0f

    mul-float v2, v2, v36

    const v3, 0x4628c000    # 10800.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 481
    mul-float v2, v33, v36

    mul-float v3, v19, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 482
    const v2, 0x46a72000    # 21392.0f

    mul-float v2, v2, v36

    const v3, 0x4607d400    # 8693.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 483
    mul-float v2, v12, v36

    mul-float v3, v22, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 484
    const v2, 0x46a25200    # 20777.0f

    mul-float v2, v2, v36

    const v3, 0x45d05800    # 6667.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 485
    mul-float v2, v14, v36

    mul-float v3, v24, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 486
    const v2, 0x469a8800    # 19780.0f

    mul-float v2, v2, v36

    const/high16 v3, 0x45960000    # 4800.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 487
    mul-float v2, v16, v36

    mul-float v3, v26, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 488
    const v2, 0x46900800    # 18436.0f

    mul-float v2, v2, v36

    const v3, 0x4545b000    # 3163.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 489
    mul-float v2, v17, v36

    mul-float v3, v25, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 490
    const v2, 0x46834000    # 16800.0f

    mul-float v2, v2, v36

    const v3, 0x44e38000    # 1820.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 491
    mul-float v2, v15, v36

    mul-float v3, v23, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 492
    const v2, 0x46695000    # 14932.0f

    mul-float v2, v2, v36

    const v3, 0x444d8000    # 822.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 493
    mul-float v2, v13, v36

    mul-float v3, v20, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 494
    const v2, 0x4649ac00    # 12907.0f

    mul-float v2, v2, v36

    const/high16 v3, 0x43500000    # 208.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 495
    mul-float v2, v11, v36

    mul-float v3, v18, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 496
    const v2, 0x4628c000    # 10800.0f

    mul-float v2, v2, v36

    const/4 v3, 0x0

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 497
    mul-float v2, v19, v36

    mul-float v3, v18, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 498
    const v2, 0x4607d400    # 8693.0f

    mul-float v2, v2, v36

    const/high16 v3, 0x43500000    # 208.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 499
    mul-float v2, v22, v36

    mul-float v3, v20, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 500
    const v2, 0x45d05800    # 6667.0f

    mul-float v2, v2, v36

    const v3, 0x444d8000    # 822.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 501
    mul-float v2, v24, v36

    mul-float v3, v23, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 502
    const/high16 v2, 0x45960000    # 4800.0f

    mul-float v2, v2, v36

    const v3, 0x44e38000    # 1820.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 503
    mul-float v2, v26, v36

    mul-float v3, v25, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 504
    const v2, 0x4545b000    # 3163.0f

    mul-float v2, v2, v36

    const v3, 0x4545b000    # 3163.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 505
    mul-float v2, v25, v36

    mul-float v3, v26, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 506
    const v2, 0x44e38000    # 1820.0f

    mul-float v2, v2, v36

    const/high16 v3, 0x45960000    # 4800.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 507
    mul-float v2, v23, v36

    mul-float v3, v24, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 508
    const v2, 0x444d8000    # 822.0f

    mul-float v2, v2, v36

    const v3, 0x45d05800    # 6667.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 509
    mul-float v2, v20, v36

    mul-float v3, v22, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 510
    const/high16 v2, 0x43500000    # 208.0f

    mul-float v2, v2, v36

    const v3, 0x4607d400    # 8693.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 511
    mul-float v2, v18, v36

    mul-float v3, v19, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 512
    const/4 v2, 0x0

    mul-float v2, v2, v36

    const v3, 0x4628c000    # 10800.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 513
    mul-float v2, v18, v36

    mul-float v3, v11, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 514
    const/high16 v2, 0x43500000    # 208.0f

    mul-float v2, v2, v36

    const v3, 0x4649ac00    # 12907.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 515
    mul-float v2, v20, v36

    mul-float v3, v13, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 516
    const v2, 0x444d8000    # 822.0f

    mul-float v2, v2, v36

    const v3, 0x46695000    # 14932.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 517
    mul-float v2, v23, v36

    mul-float v3, v15, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 518
    const v2, 0x44e38000    # 1820.0f

    mul-float v2, v2, v36

    const v3, 0x46834000    # 16800.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 519
    mul-float v2, v25, v36

    mul-float v3, v17, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 520
    const v2, 0x4545b000    # 3163.0f

    mul-float v2, v2, v36

    const v3, 0x46900800    # 18436.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 521
    mul-float v2, v26, v36

    mul-float v3, v16, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 522
    const/high16 v2, 0x45960000    # 4800.0f

    mul-float v2, v2, v36

    const v3, 0x469a8800    # 19780.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 523
    mul-float v2, v24, v36

    mul-float v3, v14, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 524
    const v2, 0x45d05800    # 6667.0f

    mul-float v2, v2, v36

    const v3, 0x469cda00    # 20077.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 525
    mul-float v2, v22, v36

    mul-float v3, v12, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 526
    const v2, 0x4607d400    # 8693.0f

    mul-float v2, v2, v36

    const v3, 0x46a72000    # 21392.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 527
    mul-float v2, v19, v36

    mul-float v3, v33, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 528
    const v2, 0x4628c000    # 10800.0f

    mul-float v2, v2, v36

    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 529
    mul-float v2, v11, v36

    mul-float v3, v33, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 530
    const v2, 0x4649ac00    # 12907.0f

    mul-float v2, v2, v36

    const v3, 0x46a72000    # 21392.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 531
    mul-float v2, v13, v36

    mul-float v3, v12, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 532
    const v2, 0x46695000    # 14932.0f

    mul-float v2, v2, v36

    const v3, 0x46a25200    # 20777.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 533
    mul-float v2, v15, v36

    mul-float v3, v14, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 534
    const v2, 0x46834000    # 16800.0f

    mul-float v2, v2, v36

    const v3, 0x469a8800    # 19780.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 535
    mul-float v2, v17, v36

    mul-float v3, v16, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 536
    const v2, 0x46900800    # 18436.0f

    mul-float v2, v2, v36

    const v3, 0x46900800    # 18436.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 537
    mul-float v2, v16, v36

    mul-float v3, v17, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 538
    const v2, 0x469a8800    # 19780.0f

    mul-float v2, v2, v36

    const v3, 0x46834000    # 16800.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 539
    mul-float v2, v14, v36

    mul-float v3, v15, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 540
    const v2, 0x46a25200    # 20777.0f

    mul-float v2, v2, v36

    const v3, 0x46695000    # 14932.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 541
    mul-float v2, v12, v36

    mul-float v3, v13, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 542
    const v2, 0x46a72000    # 21392.0f

    mul-float v2, v2, v36

    const v3, 0x4649ac00    # 12907.0f

    mul-float v3, v3, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 543
    mul-float v2, v33, v36

    mul-float v3, v11, v35

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 544
    invoke-virtual/range {v38 .. v38}, Landroid/graphics/Path;->close()V

    .line 546
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_1c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v2, :cond_1c

    .line 547
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 548
    :cond_1c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_1d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_1d

    .line 549
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 553
    :cond_1d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x45bdd800    # 6075.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 554
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x45bdd800    # 6075.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 555
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x46729400    # 15525.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 556
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x46729400    # 15525.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 578
    .end local v9    # "at0":F
    .end local v10    # "at1":F
    .end local v11    # "at10":F
    .end local v12    # "at11":F
    .end local v13    # "at12":F
    .end local v14    # "at13":F
    .end local v15    # "at14":F
    .end local v16    # "at15":F
    .end local v17    # "at16":F
    .end local v18    # "at17":F
    .end local v19    # "at18":F
    .end local v20    # "at19":F
    .end local v21    # "at2":F
    .end local v22    # "at20":F
    .end local v23    # "at21":F
    .end local v24    # "at22":F
    .end local v25    # "at23":F
    .end local v26    # "at24":F
    .end local v27    # "at3":F
    .end local v28    # "at4":F
    .end local v29    # "at5":F
    .end local v30    # "at6":F
    .end local v31    # "at7":F
    .end local v32    # "at8":F
    .end local v33    # "at9":F
    .end local v38    # "path":Landroid/graphics/Path;
    .restart local v7    # "mtx":Landroid/graphics/Matrix;
    .restart local v39    # "rotatedBMP":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v34

    .line 579
    .local v34, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception while writing pictures to out file"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v34

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F

    .prologue
    .line 106
    invoke-super {p0, p3, p4, p7, p8}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IFF)V

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mDrawOnCanvas:Z

    .line 108
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mLeftMarg:I

    .line 109
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mTopMarg:I

    .line 110
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 111
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 112
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 113
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mDrawOnCanvas:Z

    .line 114
    return-void
.end method

.method public drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p2, "shapeCount"    # I

    .prologue
    .line 97
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V

    .line 99
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->draw(I)V

    .line 100
    return-void
.end method

.method public drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 83
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;I)V

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mDrawOnCanvas:Z

    .line 85
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mLeftMarg:I

    .line 86
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mTopMarg:I

    .line 87
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->width:F

    .line 88
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->height:F

    .line 89
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 90
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 91
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 92
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mDrawOnCanvas:Z

    .line 93
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V
    .locals 0
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 35
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 36
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->draw(I)V

    .line 37
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V
    .locals 2
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I

    .prologue
    .line 41
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mDrawOnCanvas:Z

    .line 43
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mLeftMarg:I

    .line 44
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mTopMarg:I

    .line 45
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 46
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 47
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mDrawOnCanvas:Z

    .line 49
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 127
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 128
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mDrawOnCanvas:Z

    .line 129
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mLeftMarg:I

    .line 130
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mTopMarg:I

    .line 131
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 132
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 133
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 134
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mDrawOnCanvas:Z

    .line 135
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 139
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V

    .line 140
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mDrawOnCanvas:Z

    .line 141
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mLeftMarg:I

    .line 142
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mTopMarg:I

    .line 143
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 144
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 145
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 146
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mDrawOnCanvas:Z

    .line 147
    return-void
.end method

.method public drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .prologue
    .line 121
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 122
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->draw(I)V

    .line 123
    return-void
.end method

.method public drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 54
    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p7

    move v4, p8

    move-object v5, p9

    invoke-super/range {v0 .. v5}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSSExcelShape(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mDrawOnCanvas:Z

    .line 57
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mLeftMarg:I

    .line 58
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mTopMarg:I

    .line 59
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 60
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 61
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mDrawOnCanvas:Z

    .line 63
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 67
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mDrawOnCanvas:Z

    .line 69
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mLeftMarg:I

    .line 70
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mTopMarg:I

    .line 71
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->width:F

    .line 72
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->height:F

    .line 73
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 74
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 75
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->mDrawOnCanvas:Z

    .line 77
    return-void
.end method
