.class public Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLRadarChartHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DMLRadarChartHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 22
    const/16 v0, 0x23

    const-string/jumbo v1, "radarChart"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 23
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    .line 26
    const/4 v3, 0x1

    new-array v0, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 28
    .local v0, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerHandler;-><init>()V

    .line 29
    .local v1, "serHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerHandler;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v3, "ser"

    invoke-direct {v2, v3, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 31
    .local v2, "serSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/4 v3, 0x0

    aput-object v2, v0, v3

    .line 33
    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLRadarChartHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 34
    return-void
.end method


# virtual methods
.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 54
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 55
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    if-eqz v0, :cond_0

    .line 59
    :goto_0
    return-void

    .line 58
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    const-string/jumbo v1, "Radar"

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->addExChartName(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    const/4 v1, 0x5

    .line 39
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 40
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLRadarChartHandler;->init()V

    .line 41
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    if-eqz v0, :cond_0

    .line 42
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->setChartType(I)V

    .line 46
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    if-eqz v0, :cond_1

    .line 47
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->setChartType(I)V

    .line 50
    :cond_1
    return-void
.end method
