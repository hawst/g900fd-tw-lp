.class public abstract Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;
.super Lorg/apache/poi/POIXMLDocumentPart;
.source "XWPFHeaderFooter.java"


# instance fields
.field private bodyElements:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/IBodyElement;",
            ">;"
        }
    .end annotation
.end field

.field private document:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

.field protected picturesData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0}, Lorg/apache/poi/POIXMLDocumentPart;-><init>()V

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;->picturesData:Ljava/util/HashMap;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;->bodyElements:Ljava/util/ArrayList;

    .line 53
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;->readHdrFtr()V

    .line 54
    return-void
.end method

.method constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;)V
    .locals 2
    .param p1, "doc"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/apache/poi/POIXMLDocumentPart;-><init>()V

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;->picturesData:Ljava/util/HashMap;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;->bodyElements:Ljava/util/ArrayList;

    .line 44
    if-nez p1, :cond_0

    .line 45
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 48
    :cond_0
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;->document:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    .line 49
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;->readHdrFtr()V

    .line 50
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/POIXMLDocumentPart;Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V
    .locals 2
    .param p1, "parent"    # Lorg/apache/poi/POIXMLDocumentPart;
    .param p2, "part"    # Lorg/apache/poi/openxml4j/opc/PackagePart;
    .param p3, "rel"    # Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/poi/POIXMLDocumentPart;-><init>(Lorg/apache/poi/POIXMLDocumentPart;Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;->picturesData:Ljava/util/HashMap;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;->bodyElements:Ljava/util/ArrayList;

    .line 63
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;->getParent()Lorg/apache/poi/POIXMLDocumentPart;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;->document:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    .line 65
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;->document:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    if-nez v0, :cond_0

    .line 66
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 68
    :cond_0
    return-void
.end method


# virtual methods
.method public getAllPictureData()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;->picturesData:Ljava/util/HashMap;

    return-object v0
.end method

.method public getBodyElements()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/IBodyElement;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;->bodyElements:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getOwner()Lorg/apache/poi/POIXMLDocumentPart;
    .locals 0

    .prologue
    .line 208
    return-object p0
.end method

.method public getPart()Lorg/apache/poi/POIXMLDocumentPart;
    .locals 0

    .prologue
    .line 277
    return-object p0
.end method

.method public getPictureDataByID(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;
    .locals 2
    .param p1, "blipID"    # Ljava/lang/String;

    .prologue
    .line 200
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;->getRelationById(Ljava/lang/String;)Lorg/apache/poi/POIXMLDocumentPart;

    move-result-object v0

    .line 201
    .local v0, "relatedPart":Lorg/apache/poi/POIXMLDocumentPart;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;

    if-eqz v1, :cond_0

    .line 202
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;

    .line 204
    .end local v0    # "relatedPart":Lorg/apache/poi/POIXMLDocumentPart;
    :goto_0
    return-object v0

    .restart local v0    # "relatedPart":Lorg/apache/poi/POIXMLDocumentPart;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getXWPFDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;->document:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;->document:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    .line 266
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;->getParent()Lorg/apache/poi/POIXMLDocumentPart;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    goto :goto_0
.end method

.method protected onDocumentRead()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;->getRelations()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/POIXMLDocumentPart;

    .line 73
    .local v1, "poixmlDocumentPart":Lorg/apache/poi/POIXMLDocumentPart;
    instance-of v3, v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;

    if-eqz v3, :cond_0

    move-object v2, v1

    .line 74
    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;

    .line 75
    .local v2, "xwpfPicData":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;->picturesData:Ljava/util/HashMap;

    invoke-virtual {v1}, Lorg/apache/poi/POIXMLDocumentPart;->getPackageRelationship()Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 79
    .end local v1    # "poixmlDocumentPart":Lorg/apache/poi/POIXMLDocumentPart;
    .end local v2    # "xwpfPicData":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;
    :cond_1
    return-void
.end method

.method public readHdrFtr()V
    .locals 0

    .prologue
    .line 240
    return-void
.end method

.method public setBodyElements(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/IBodyElement;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 86
    .local p1, "bodyElements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/ooxml/word/IBodyElement;>;"
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;->bodyElements:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 87
    return-void
.end method

.method public setXWPFDocument(Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;)V
    .locals 0
    .param p1, "doc"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    .prologue
    .line 259
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;->document:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    .line 260
    return-void
.end method
