.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetdataRowCVHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedTagNoChildrenHandler;
.source "XExcelSheetdataRowCVHandler.java"


# instance fields
.field public textindex:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    const-string/jumbo v0, "v"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedTagNoChildrenHandler;-><init>(Ljava/lang/String;)V

    .line 19
    return-void
.end method


# virtual methods
.method public charactersHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;[CII)V
    .locals 4
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "content"    # [C
    .param p3, "start"    # I
    .param p4, "end"    # I

    .prologue
    .line 24
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p2, p3, p4}, Ljava/lang/String;-><init>([CII)V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetdataRowCVHandler;->textindex:Ljava/lang/String;

    .line 25
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v0

    .line 27
    .local v0, "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    iget v1, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countv:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countv:I

    .line 31
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetdataRowCVHandler;->textindex:Ljava/lang/String;

    iget v3, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countv:I

    invoke-virtual {v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->addExWorkSheetVTextid(Ljava/lang/String;I)V

    .line 34
    iget v1, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countv:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countv:I

    .line 35
    return-void
.end method
