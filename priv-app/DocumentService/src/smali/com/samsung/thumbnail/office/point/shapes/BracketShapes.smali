.class public Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;
.super Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;
.source "BracketShapes.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mDrawOnCanvas:Z

.field private mLeftMarg:I

.field private mTopMarg:I


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "folderPath"    # Ljava/io/File;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;-><init>()V

    .line 29
    const-string/jumbo v0, "BracketShapes"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->TAG:Ljava/lang/String;

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mDrawOnCanvas:Z

    .line 35
    iput-object p1, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->folderPath:Ljava/io/File;

    .line 36
    return-void
.end method


# virtual methods
.method protected draw(Landroid/graphics/Canvas;I)V
    .locals 17
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeCount"    # I

    .prologue
    .line 160
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v2, "BracketPair"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 165
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->adjval0:I

    if-nez v1, :cond_0

    .line 166
    const/16 v1, 0xe10

    move-object/from16 v0, p0

    iput v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->adjval0:I

    .line 168
    :cond_0
    const/4 v8, 0x0

    .line 170
    .local v8, "adj_val":F
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_5

    .line 171
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->adjval0:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float v8, v1, v2

    .line 176
    :goto_0
    new-instance v12, Landroid/graphics/RectF;

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v3, v8

    sub-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v3, v8

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    invoke-direct {v12, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 178
    .local v12, "rectF2":Landroid/graphics/RectF;
    new-instance v13, Landroid/graphics/RectF;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v3, v8

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v4, v8

    invoke-direct {v13, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 179
    .local v13, "rectF3":Landroid/graphics/RectF;
    new-instance v14, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, v8

    sub-float/2addr v1, v2

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v4, v8

    invoke-direct {v14, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 180
    .local v14, "rectF4":Landroid/graphics/RectF;
    new-instance v15, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, v8

    sub-float/2addr v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v3, v8

    sub-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    invoke-direct {v15, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 183
    .local v15, "rectF5":Landroid/graphics/RectF;
    new-instance v11, Landroid/graphics/Path;

    invoke-direct {v11}, Landroid/graphics/Path;-><init>()V

    .line 185
    .local v11, "path":Landroid/graphics/Path;
    const/high16 v1, 0x42b40000    # 90.0f

    const/high16 v2, 0x42b40000    # 90.0f

    invoke-virtual {v11, v12, v1, v2}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 186
    const/4 v1, 0x0

    invoke-virtual {v11, v1, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 187
    const/high16 v1, 0x43340000    # 180.0f

    const/high16 v2, 0x42b40000    # 90.0f

    invoke-virtual {v11, v13, v1, v2}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 189
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v1, :cond_1

    .line 190
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 192
    :cond_1
    new-instance v11, Landroid/graphics/Path;

    .end local v11    # "path":Landroid/graphics/Path;
    invoke-direct {v11}, Landroid/graphics/Path;-><init>()V

    .line 193
    .restart local v11    # "path":Landroid/graphics/Path;
    const/high16 v1, 0x43870000    # 270.0f

    const/high16 v2, 0x42b40000    # 90.0f

    invoke-virtual {v11, v14, v1, v2}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 194
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    sub-float/2addr v2, v8

    invoke-virtual {v11, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 195
    const/4 v1, 0x0

    const/high16 v2, 0x42b40000    # 90.0f

    invoke-virtual {v11, v15, v1, v2}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 197
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v1, :cond_2

    .line 198
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 202
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->textArea:Landroid/graphics/RectF;

    const v2, 0x4552f000    # 3375.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->left:F

    .line 203
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->textArea:Landroid/graphics/RectF;

    const v2, 0x4552f000    # 3375.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->top:F

    .line 204
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->textArea:Landroid/graphics/RectF;

    const v2, 0x468e6200    # 18225.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->right:F

    .line 205
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->textArea:Landroid/graphics/RectF;

    const v2, 0x468e6200    # 18225.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    .line 273
    .end local v8    # "adj_val":F
    .end local v11    # "path":Landroid/graphics/Path;
    .end local v12    # "rectF2":Landroid/graphics/RectF;
    .end local v13    # "rectF3":Landroid/graphics/RectF;
    .end local v14    # "rectF4":Landroid/graphics/RectF;
    .end local v15    # "rectF5":Landroid/graphics/RectF;
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mDrawOnCanvas:Z

    if-nez v1, :cond_4

    .line 274
    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    .line 277
    .local v6, "mtx":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->rotation:I

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    invoke-virtual {v6, v1, v2, v3}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 279
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->bitmap:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->bitmapWidth:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->bitmapHight:I

    const/4 v7, 0x1

    invoke-static/range {v1 .. v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 284
    .local v16, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v10, Ljava/io/FileOutputStream;

    new-instance v1, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->folderPath:Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "pic_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->shapeName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ".png"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v10, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 290
    .local v10, "out":Ljava/io/FileOutputStream;
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x64

    move-object/from16 v0, v16

    invoke-virtual {v0, v1, v2, v10}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 291
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 296
    .end local v6    # "mtx":Landroid/graphics/Matrix;
    .end local v10    # "out":Ljava/io/FileOutputStream;
    .end local v16    # "rotatedBMP":Landroid/graphics/Bitmap;
    :cond_4
    :goto_2
    return-void

    .line 173
    .restart local v8    # "adj_val":F
    :cond_5
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->adjval0:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float v8, v1, v2

    goto/16 :goto_0

    .line 209
    .end local v8    # "adj_val":F
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v2, "LeftBracket"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 211
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->adjval0:I

    if-nez v1, :cond_7

    .line 212
    const/16 v1, 0x708

    move-object/from16 v0, p0

    iput v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->adjval0:I

    .line 214
    :cond_7
    const/4 v8, 0x0

    .line 216
    .restart local v8    # "adj_val":F
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_9

    .line 217
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->adjval0:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float v8, v1, v2

    .line 222
    :goto_3
    new-instance v11, Landroid/graphics/Path;

    invoke-direct {v11}, Landroid/graphics/Path;-><init>()V

    .line 224
    .restart local v11    # "path":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    const/4 v2, 0x0

    invoke-virtual {v11, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 225
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v11, v1, v2, v3, v8}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 226
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    sub-float/2addr v2, v8

    invoke-virtual {v11, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 227
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    invoke-virtual {v11, v1, v2, v3, v4}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 229
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v1, :cond_8

    .line 230
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 233
    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->textArea:Landroid/graphics/RectF;

    const v2, 0x45fd2000    # 8100.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->left:F

    .line 234
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->textArea:Landroid/graphics/RectF;

    const v2, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->top:F

    .line 235
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->textArea:Landroid/graphics/RectF;

    const v2, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->right:F

    .line 236
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->textArea:Landroid/graphics/RectF;

    const v2, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 219
    .end local v11    # "path":Landroid/graphics/Path;
    :cond_9
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->adjval0:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float v8, v1, v2

    goto/16 :goto_3

    .line 239
    .end local v8    # "adj_val":F
    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v2, "RightBracket"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 241
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->adjval0:I

    if-nez v1, :cond_b

    .line 242
    const/16 v1, 0x708

    move-object/from16 v0, p0

    iput v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->adjval0:I

    .line 244
    :cond_b
    const/4 v8, 0x0

    .line 246
    .restart local v8    # "adj_val":F
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_d

    .line 247
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->adjval0:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float v8, v1, v2

    .line 252
    :goto_4
    new-instance v11, Landroid/graphics/Path;

    invoke-direct {v11}, Landroid/graphics/Path;-><init>()V

    .line 254
    .restart local v11    # "path":Landroid/graphics/Path;
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v11, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 255
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    invoke-virtual {v11, v1, v2, v3, v8}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 256
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    sub-float/2addr v2, v8

    invoke-virtual {v11, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 257
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    invoke-virtual {v11, v1, v2, v3, v4}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 259
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v1, :cond_c

    .line 260
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 264
    :cond_c
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->textArea:Landroid/graphics/RectF;

    const v2, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->left:F

    .line 265
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->textArea:Landroid/graphics/RectF;

    const v2, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->top:F

    .line 266
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->textArea:Landroid/graphics/RectF;

    const v2, 0x4652f000    # 13500.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->right:F

    .line 267
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->textArea:Landroid/graphics/RectF;

    const v2, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 249
    .end local v11    # "path":Landroid/graphics/Path;
    :cond_d
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->adjval0:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float v8, v1, v2

    goto/16 :goto_4

    .line 292
    .end local v8    # "adj_val":F
    .restart local v6    # "mtx":Landroid/graphics/Matrix;
    .restart local v16    # "rotatedBMP":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v9

    .line 293
    .local v9, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Exception while writing pictures to out file"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method public drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F

    .prologue
    .line 81
    invoke-super {p0, p3, p4, p7, p8}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IFF)V

    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mDrawOnCanvas:Z

    .line 83
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mLeftMarg:I

    .line 84
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mTopMarg:I

    .line 85
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 86
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 87
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mDrawOnCanvas:Z

    .line 89
    return-void
.end method

.method public drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p2, "shapeCount"    # I

    .prologue
    .line 72
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V

    .line 74
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->draw(I)V

    .line 75
    return-void
.end method

.method public drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 146
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;I)V

    .line 147
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mDrawOnCanvas:Z

    .line 148
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mLeftMarg:I

    .line 149
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mTopMarg:I

    .line 150
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    .line 151
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    .line 152
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 153
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 154
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 155
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mDrawOnCanvas:Z

    .line 156
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V
    .locals 0
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 40
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 41
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->draw(I)V

    .line 42
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V
    .locals 2
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I

    .prologue
    .line 46
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mDrawOnCanvas:Z

    .line 48
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mLeftMarg:I

    .line 49
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mTopMarg:I

    .line 50
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 51
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 52
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mDrawOnCanvas:Z

    .line 54
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 101
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 102
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mDrawOnCanvas:Z

    .line 103
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mLeftMarg:I

    .line 104
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mTopMarg:I

    .line 105
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 106
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 107
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mDrawOnCanvas:Z

    .line 109
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 113
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V

    .line 114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mDrawOnCanvas:Z

    .line 115
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mLeftMarg:I

    .line 116
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mTopMarg:I

    .line 117
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 118
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 119
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 120
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mDrawOnCanvas:Z

    .line 121
    return-void
.end method

.method public drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .prologue
    .line 95
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 96
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->draw(I)V

    .line 97
    return-void
.end method

.method public drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 59
    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p7

    move v4, p8

    move-object v5, p9

    invoke-super/range {v0 .. v5}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSSExcelShape(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mDrawOnCanvas:Z

    .line 62
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mLeftMarg:I

    .line 63
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mTopMarg:I

    .line 64
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 65
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 66
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mDrawOnCanvas:Z

    .line 68
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;I)V
    .locals 1
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 124
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 125
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->draw(I)V

    .line 126
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 130
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 131
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mDrawOnCanvas:Z

    .line 132
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mLeftMarg:I

    .line 133
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mTopMarg:I

    .line 134
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->width:F

    .line 135
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->height:F

    .line 136
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 137
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 138
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 139
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->mDrawOnCanvas:Z

    .line 140
    return-void
.end method
