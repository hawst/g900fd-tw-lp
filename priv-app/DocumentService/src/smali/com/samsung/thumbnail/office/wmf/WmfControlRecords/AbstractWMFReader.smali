.class public Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;
.super Ljava/lang/Object;
.source "AbstractWMFReader.java"


# instance fields
.field protected isotropic:Z

.field public lastObjectIdx:I

.field public mOffset:I

.field protected numObjects:I

.field protected objectVector:Ljava/util/List;

.field protected vpH:I

.field protected vpW:I

.field protected vpX:I

.field protected vpY:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;->isotropic:Z

    return-void
.end method


# virtual methods
.method public addObject(ILjava/lang/Object;)I
    .locals 4
    .param p1, "type"    # I
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 38
    const/4 v2, 0x0

    .line 40
    .local v2, "startIdx":I
    move v1, v2

    .local v1, "i":I
    :goto_0
    iget v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;->numObjects:I

    if-ge v1, v3, :cond_0

    .line 41
    iget-object v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;->objectVector:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;

    .line 42
    .local v0, "gdi":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;
    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->used:Z

    if-nez v3, :cond_1

    .line 43
    invoke-virtual {v0, p1, p2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->Setup(ILjava/lang/Object;)V

    .line 44
    iput v1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;->lastObjectIdx:I

    .line 49
    .end local v0    # "gdi":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;
    :cond_0
    iget v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;->lastObjectIdx:I

    return v3

    .line 40
    .restart local v0    # "gdi":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public addObjectAt(ILjava/lang/Object;I)I
    .locals 3
    .param p1, "type"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "idx"    # I

    .prologue
    .line 21
    if-eqz p3, :cond_0

    iget v2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;->numObjects:I

    if-le p3, v2, :cond_2

    .line 22
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;->addObject(ILjava/lang/Object;)I

    .line 23
    iget p3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;->lastObjectIdx:I

    .line 34
    .end local p3    # "idx":I
    :cond_1
    :goto_0
    return p3

    .line 25
    .restart local p3    # "idx":I
    :cond_2
    iput p3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;->lastObjectIdx:I

    .line 26
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;->numObjects:I

    if-ge v1, v2, :cond_1

    .line 27
    iget-object v2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;->objectVector:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;

    .line 28
    .local v0, "gdi":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;
    if-ne v1, p3, :cond_3

    .line 29
    invoke-virtual {v0, p1, p2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;->Setup(ILjava/lang/Object;)V

    goto :goto_0

    .line 26
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public getNumObjects()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;->numObjects:I

    return v0
.end method

.method public getObject(I)Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;->objectVector:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/GdiObject;

    return-object v0
.end method

.method public getVpHFactor()F
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    return v0
.end method

.method public getVpWFactor()F
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    return v0
.end method

.method public readByte([B)B
    .locals 3
    .param p1, "is"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 77
    const/4 v1, 0x1

    new-array v0, v1, [B

    .line 79
    .local v0, "intConvert":[B
    iget v1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;->mOffset:I

    aget-byte v1, p1, v1

    aput-byte v1, v0, v2

    .line 80
    iget v1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;->mOffset:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;->mOffset:I

    .line 81
    aget-byte v1, v0, v2

    return v1
.end method

.method public readInt([B)I
    .locals 5
    .param p1, "is"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    const/4 v1, 0x0

    .line 67
    .local v1, "i":I
    const/4 v3, 0x4

    new-array v2, v3, [B

    .line 69
    .local v2, "intConvert":[B
    const/4 v1, 0x0

    :goto_0
    const/4 v3, 0x3

    if-gt v1, v3, :cond_0

    .line 70
    rsub-int/lit8 v3, v1, 0x3

    iget v4, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;->mOffset:I

    add-int/2addr v4, v1

    aget-byte v4, p1, v4

    aput-byte v4, v2, v3

    .line 69
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 71
    :cond_0
    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 72
    .local v0, "Type":I
    iget v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;->mOffset:I

    add-int/2addr v3, v1

    iput v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;->mOffset:I

    .line 73
    return v0
.end method

.method public readShort([B)S
    .locals 5
    .param p1, "is"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    const/4 v3, 0x2

    new-array v2, v3, [B

    .line 55
    .local v2, "shortConvert":[B
    const/4 v0, 0x0

    .line 57
    .local v0, "i":I
    const/4 v0, 0x0

    :goto_0
    const/4 v3, 0x1

    if-gt v0, v3, :cond_0

    .line 58
    rsub-int/lit8 v3, v0, 0x1

    iget v4, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;->mOffset:I

    add-int/2addr v4, v0

    aget-byte v4, p1, v4

    aput-byte v4, v2, v3

    .line 57
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 59
    :cond_0
    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getSmallInt([BI)I

    move-result v3

    int-to-short v1, v3

    .line 60
    .local v1, "reserved":S
    iget v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;->mOffset:I

    add-int/2addr v3, v0

    iput v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;->mOffset:I

    .line 61
    return v1
.end method
