.class public final enum Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;
.super Ljava/lang/Enum;
.source "XDocTblCellProperties.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VAlign"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

.field public static final enum BOTH:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

.field public static final enum BOTTOM:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

.field public static final enum CENTER:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

.field public static final enum TOP:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 121
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    const-string/jumbo v1, "TOP"

    const-string/jumbo v2, "top"

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;->TOP:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    const-string/jumbo v1, "CENTER"

    const-string/jumbo v2, "center"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;->CENTER:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    const-string/jumbo v1, "BOTH"

    const-string/jumbo v2, "both"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;->BOTH:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    const-string/jumbo v1, "BOTTOM"

    const-string/jumbo v2, "bottom"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;->BOTTOM:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    .line 120
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;->TOP:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;->CENTER:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;->BOTH:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;->BOTTOM:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 125
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 126
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;->value:Ljava/lang/String;

    .line 127
    return-void
.end method

.method public static convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 134
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;->values()[Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 135
    .local v2, "inst":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 139
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;
    :goto_1
    return-object v2

    .line 134
    .restart local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 139
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 120
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;
    .locals 1

    .prologue
    .line 120
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    return-object v0
.end method


# virtual methods
.method public xmlValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;->value:Ljava/lang/String;

    return-object v0
.end method
