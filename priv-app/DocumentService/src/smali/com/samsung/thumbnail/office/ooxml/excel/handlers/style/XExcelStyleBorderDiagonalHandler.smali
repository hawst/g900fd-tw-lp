.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderDiagonalHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelStyleBorderDiagonalHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 19
    const-string/jumbo v1, "diagonal"

    invoke-direct {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 20
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;-><init>()V

    .line 21
    .local v0, "colorHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderDiagonalHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "color"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 35
    return-void
.end method
