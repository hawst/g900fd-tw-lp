.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FontRefHandler;
.super Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;
.source "FontRefHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FontRefHandler$IFontPropsConsumer;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FontRefHandler$IFontPropsConsumer;)V
    .locals 6
    .param p1, "consumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FontRefHandler$IFontPropsConsumer;

    .prologue
    const/4 v4, 0x0

    .line 14
    const/16 v1, 0x1f

    const-string/jumbo v2, "fontRef"

    move-object v0, p0

    move-object v3, p1

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 16
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 21
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 23
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    if-eqz v4, :cond_1

    .line 25
    const-string/jumbo v4, "idx"

    invoke-virtual {p0, p3, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FontRefHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 26
    .local v1, "idxVal":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 27
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FontRefHandler;->colorConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FontRefHandler$IFontPropsConsumer;

    .line 28
    .local v0, "fConsumer":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FontRefHandler$IFontPropsConsumer;
    const-string/jumbo v4, "major"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 29
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getMajorFont()Ljava/lang/String;

    move-result-object v2

    .line 30
    .local v2, "majorIdx":Ljava/lang/String;
    invoke-interface {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FontRefHandler$IFontPropsConsumer;->handleFontRef(Ljava/lang/String;)V

    .line 32
    .end local v2    # "majorIdx":Ljava/lang/String;
    :cond_0
    const-string/jumbo v4, "minor"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 33
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getMinorFont()Ljava/lang/String;

    move-result-object v3

    .line 34
    .local v3, "minorIdx":Ljava/lang/String;
    invoke-interface {v0, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FontRefHandler$IFontPropsConsumer;->handleFontRef(Ljava/lang/String;)V

    .line 40
    .end local v0    # "fConsumer":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FontRefHandler$IFontPropsConsumer;
    .end local v1    # "idxVal":Ljava/lang/String;
    .end local v3    # "minorIdx":Ljava/lang/String;
    :cond_1
    return-void
.end method
