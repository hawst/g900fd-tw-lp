.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathQuadBezToHandler;
.super Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathSegmentsPointsHandler;
.source "PathQuadBezToHandler.java"


# instance fields
.field private coordinates:[Ljava/lang/String;

.field private count:I

.field private pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V
    .locals 1
    .param p1, "pathDesc"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    .prologue
    .line 16
    const-string/jumbo v0, "quadBezTo"

    invoke-direct {p0, v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathSegmentsPointsHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V

    .line 17
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathQuadBezToHandler;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    .line 18
    return-void
.end method


# virtual methods
.method public point([Ljava/lang/String;)V
    .locals 5
    .param p1, "valuesArray"    # [Ljava/lang/String;

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 31
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathQuadBezToHandler;->count:I

    if-nez v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathQuadBezToHandler;->coordinates:[Ljava/lang/String;

    aget-object v1, p1, v2

    aput-object v1, v0, v2

    .line 33
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathQuadBezToHandler;->coordinates:[Ljava/lang/String;

    aget-object v1, p1, v3

    aput-object v1, v0, v3

    .line 34
    iput v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathQuadBezToHandler;->count:I

    .line 40
    :goto_0
    return-void

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathQuadBezToHandler;->coordinates:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v2, p1, v2

    aput-object v2, v0, v1

    .line 38
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathQuadBezToHandler;->coordinates:[Ljava/lang/String;

    aget-object v1, p1, v3

    aput-object v1, v0, v4

    .line 39
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathQuadBezToHandler;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathQuadBezToHandler;->coordinates:[Ljava/lang/String;

    invoke-virtual {v0, v4, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->addSegmentstoPath(I[Ljava/lang/String;)V

    goto :goto_0
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 23
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathSegmentsPointsHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 25
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathQuadBezToHandler;->coordinates:[Ljava/lang/String;

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathQuadBezToHandler;->count:I

    .line 27
    return-void
.end method
