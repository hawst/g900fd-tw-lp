.class public Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;
.super Ljava/lang/Object;
.source "XWPFAbstractNum.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum$EMultiLevelType;
    }
.end annotation


# instance fields
.field private abstractNumId:I

.field private multiLvlType:Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum$EMultiLevelType;

.field private numLvls:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;",
            ">;"
        }
    .end annotation
.end field

.field private numStyleLink:Ljava/lang/String;

.field private numbering:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;->numbering:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;->numLvls:Ljava/util/ArrayList;

    .line 42
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;)V
    .locals 1
    .param p1, "numbering"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;->numbering:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;->numLvls:Ljava/util/ArrayList;

    .line 47
    return-void
.end method


# virtual methods
.method public addNumLvl(Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;)V
    .locals 1
    .param p1, "numLvl"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;->numLvls:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    return-void
.end method

.method public getAbstractNumId()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;->abstractNumId:I

    return v0
.end method

.method public getMultiLvlType()Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum$EMultiLevelType;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;->multiLvlType:Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum$EMultiLevelType;

    return-object v0
.end method

.method public getNumLvl(I)Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 71
    :try_start_0
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;->numLvls:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    :goto_0
    return-object v1

    .line 72
    :catch_0
    move-exception v0

    .line 73
    .local v0, "exce":Ljava/lang/Exception;
    const-string/jumbo v1, "DocumentService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNumLvl()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;->numLvls:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getNumStyleLink()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;->numStyleLink:Ljava/lang/String;

    return-object v0
.end method

.method public getNumbering()Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;->numbering:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;

    return-object v0
.end method

.method public setAbstractNumId(I)V
    .locals 0
    .param p1, "abstractNumId"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;->abstractNumId:I

    .line 51
    return-void
.end method

.method public setMultiLvlType(Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum$EMultiLevelType;)V
    .locals 0
    .param p1, "multiLvlType"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum$EMultiLevelType;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;->multiLvlType:Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum$EMultiLevelType;

    .line 55
    return-void
.end method

.method public setNumStyleLink(Ljava/lang/String;)V
    .locals 0
    .param p1, "numStyleLink"    # Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;->numStyleLink:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public setNumbering(Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;)V
    .locals 0
    .param p1, "numbering"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;->numbering:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;

    .line 96
    return-void
.end method
