.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontNameHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelStyleFontNameHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    const-string/jumbo v0, "name"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 23
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 28
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 29
    const-string/jumbo v2, "val"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontNameHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 31
    .local v0, "name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v1

    .line 33
    .local v1, "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v3, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    invoke-virtual {v2, v0, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleTextFontname(Ljava/lang/String;I)V

    .line 36
    return-void
.end method
