.class public Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;
.super Ljava/lang/Object;
.source "EMR_SetViewPortOrgex.java"


# instance fields
.field private RecordSize:I

.field private RecordType:I

.field fileContent:[B

.field private offset:I

.field private xOrigin:I

.field private yOrigin:I


# direct methods
.method public constructor <init>([BI)V
    .locals 0
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->fileContent:[B

    .line 21
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->offset:I

    .line 22
    return-void
.end method


# virtual methods
.method public getRecordSize()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->RecordSize:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->RecordType:I

    return v0
.end method

.method public getXOrigin()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->xOrigin:I

    return v0
.end method

.method public getYOrigin()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->yOrigin:I

    return v0
.end method

.method public readEmfSetViewPortOrgex()V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x0

    .line 57
    const/4 v2, 0x0

    .line 59
    .local v2, "i":I
    const/4 v6, 0x4

    new-array v3, v6, [B

    .line 61
    .local v3, "intConvert":[B
    const/4 v2, 0x0

    :goto_0
    if-gt v2, v10, :cond_0

    .line 62
    rsub-int/lit8 v6, v2, 0x3

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->fileContent:[B

    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->offset:I

    add-int/2addr v8, v2

    aget-byte v7, v7, v8

    aput-byte v7, v3, v6

    .line 61
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 63
    :cond_0
    invoke-static {v3, v9}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 64
    .local v1, "Type":I
    iget v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->offset:I

    add-int/2addr v6, v2

    iput v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->offset:I

    .line 65
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->setRecordType(I)V

    .line 67
    const/4 v2, 0x0

    :goto_1
    if-gt v2, v10, :cond_1

    .line 68
    rsub-int/lit8 v6, v2, 0x3

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->fileContent:[B

    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->offset:I

    add-int/2addr v8, v2

    aget-byte v7, v7, v8

    aput-byte v7, v3, v6

    .line 67
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 69
    :cond_1
    invoke-static {v3, v9}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 70
    .local v0, "Size":I
    iget v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->offset:I

    add-int/2addr v6, v2

    iput v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->offset:I

    .line 71
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->setRecordSize(I)V

    .line 73
    const/4 v2, 0x0

    :goto_2
    if-gt v2, v10, :cond_2

    .line 74
    rsub-int/lit8 v6, v2, 0x3

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->fileContent:[B

    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->offset:I

    add-int/2addr v8, v2

    aget-byte v7, v7, v8

    aput-byte v7, v3, v6

    .line 73
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 75
    :cond_2
    invoke-static {v3, v9}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v4

    .line 76
    .local v4, "xOrign":I
    iget v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->offset:I

    add-int/2addr v6, v2

    iput v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->offset:I

    .line 77
    int-to-float v6, v4

    invoke-static {v6}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {p0, v6}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->setXOrigin(I)V

    .line 79
    const/4 v2, 0x0

    :goto_3
    if-gt v2, v10, :cond_3

    .line 80
    rsub-int/lit8 v6, v2, 0x3

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->fileContent:[B

    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->offset:I

    add-int/2addr v8, v2

    aget-byte v7, v7, v8

    aput-byte v7, v3, v6

    .line 79
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 81
    :cond_3
    invoke-static {v3, v9}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v5

    .line 82
    .local v5, "yOrign":I
    iget v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->offset:I

    add-int/2addr v6, v2

    iput v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->offset:I

    .line 83
    int-to-float v6, v5

    invoke-static {v6}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {p0, v6}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->setYOrigin(I)V

    .line 85
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "RecrdSize"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->RecordSize:I

    .line 34
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "RecrdType"    # I

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->RecordType:I

    .line 26
    return-void
.end method

.method public setXOrigin(I)V
    .locals 0
    .param p1, "xOrign"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->xOrigin:I

    .line 42
    return-void
.end method

.method public setYOrigin(I)V
    .locals 0
    .param p1, "yOrign"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_SetViewPortOrgex;->yOrigin:I

    .line 50
    return-void
.end method
