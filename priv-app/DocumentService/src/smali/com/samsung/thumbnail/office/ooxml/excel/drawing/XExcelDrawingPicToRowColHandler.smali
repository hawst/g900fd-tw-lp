.class public Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicToRowColHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelDrawingPicToRowColHandler.java"


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;)V
    .locals 6
    .param p1, "cellAnchor"    # Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    .prologue
    .line 14
    const/16 v4, 0xc9

    const-string/jumbo v5, "to"

    invoke-direct {p0, v4, v5}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 17
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicColHandler;

    const-string/jumbo v4, "to"

    invoke-direct {v1, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicColHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;)V

    .line 19
    .local v1, "tocolHandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicColHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicToRowColHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "col"

    invoke-virtual {v4, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicColOffHandler;

    const-string/jumbo v4, "to"

    invoke-direct {v2, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicColOffHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;)V

    .line 22
    .local v2, "tocolOffHandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicColOffHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicToRowColHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "colOff"

    invoke-virtual {v4, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowHandler;

    const-string/jumbo v4, "to"

    invoke-direct {v3, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;)V

    .line 26
    .local v3, "torowHandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicToRowColHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "row"

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowOffHandler;

    const-string/jumbo v4, "to"

    invoke-direct {v0, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowOffHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;)V

    .line 29
    .local v0, "fromrowOffHandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowOffHandler;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicToRowColHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v5, "rowOff"

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    return-void
.end method
