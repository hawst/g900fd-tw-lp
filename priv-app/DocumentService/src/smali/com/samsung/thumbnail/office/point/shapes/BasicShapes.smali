.class public Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;
.super Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;
.source "BasicShapes.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private flipValue:Ljava/lang/String;

.field private mDrawOnCanvas:Z

.field private mLeftMarg:I

.field private mTopMarg:I


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "folderPath"    # Ljava/io/File;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;-><init>()V

    .line 35
    const-string/jumbo v0, "BasicShapes"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->TAG:Ljava/lang/String;

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mDrawOnCanvas:Z

    .line 42
    iput-object p1, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->folderPath:Ljava/io/File;

    .line 43
    return-void
.end method

.method private drawTextbox(Landroid/graphics/Canvas;I)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeCount"    # I

    .prologue
    const v9, 0x441c4000    # 625.0f

    const/4 v6, 0x0

    const/16 v4, 0xf2

    const/4 v3, -0x1

    const v8, 0x46a8c000    # 21600.0f

    .line 772
    new-instance v2, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v2, v3}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    iput-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    .line 773
    new-instance v2, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v2, v3}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    iput-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    .line 775
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 776
    .local v1, "whitePaint":Landroid/graphics/Paint;
    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 778
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_0

    .line 779
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 780
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    const/16 v3, 0xff

    invoke-virtual {v2, v3, v4, v4, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 781
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    const v3, -0x333334

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 784
    :cond_0
    new-instance v0, Landroid/graphics/RectF;

    iget-wide v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v6

    iget-wide v4, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v6

    iget v4, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    iget-wide v6, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    iget v5, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    iget-wide v6, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 787
    .local v0, "rect":Landroid/graphics/RectF;
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 788
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v2, :cond_1

    .line 789
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 792
    :cond_1
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_2

    .line 793
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 797
    :cond_2
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v3, v9

    div-float/2addr v3, v8

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 798
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v3, v9

    div-float/2addr v3, v8

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 799
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x46a37a00    # 20925.0f

    iget v4, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v3, v4

    div-float/2addr v3, v8

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 800
    iget-object v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v3, 0x46a37a00    # 20925.0f

    iget v4, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v3, v4

    div-float/2addr v3, v8

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    .line 803
    return-void
.end method


# virtual methods
.method protected draw(Landroid/graphics/Canvas;I)V
    .locals 28
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeCount"    # I

    .prologue
    .line 194
    new-instance v22, Landroid/graphics/Paint;

    invoke-direct/range {v22 .. v22}, Landroid/graphics/Paint;-><init>()V

    .line 195
    .local v22, "whitePaint":Landroid/graphics/Paint;
    const/4 v4, -0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 197
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "RoundRectangle"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "roundRect"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "round1Rect"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "round2DiagRect"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 201
    :cond_0
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    if-nez v4, :cond_1

    .line 202
    const/16 v4, 0x1518

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    .line 204
    :cond_1
    const/4 v11, 0x0

    .line 205
    .local v11, "adj":I
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    cmpl-float v4, v4, v5

    if-ltz v4, :cond_7

    .line 206
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    float-to-int v11, v4

    .line 210
    :goto_0
    new-instance v20, Landroid/graphics/RectF;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    float-to-double v6, v6

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    move-wide/from16 v24, v0

    add-double v6, v6, v24

    double-to-float v6, v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    float-to-double v0, v7

    move-wide/from16 v24, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    move-wide/from16 v26, v0

    add-double v24, v24, v26

    move-wide/from16 v0, v24

    double-to-float v7, v0

    move-object/from16 v0, v20

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 217
    .local v20, "rect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getAlpha()I

    move-result v4

    const/16 v5, 0x4d

    if-ne v4, v5, :cond_2

    .line 218
    int-to-float v4, v11

    int-to-float v5, v11

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v4, v5, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 221
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_3

    .line 222
    int-to-float v4, v11

    int-to-float v5, v11

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 225
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_4

    .line 226
    int-to-float v4, v11

    int-to-float v5, v11

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 230
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 231
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 232
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 233
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    .line 745
    .end local v11    # "adj":I
    .end local v20    # "rect":Landroid/graphics/RectF;
    :cond_5
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mDrawOnCanvas:Z

    if-nez v4, :cond_6

    .line 747
    new-instance v9, Landroid/graphics/Matrix;

    invoke-direct {v9}, Landroid/graphics/Matrix;-><init>()V

    .line 750
    .local v9, "mtx":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->rotation:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    invoke-virtual {v9, v4, v5, v6}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 752
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->bitmap:Landroid/graphics/Bitmap;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->bitmapWidth:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->bitmapHight:I

    const/4 v10, 0x1

    invoke-static/range {v4 .. v10}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 757
    .local v21, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v17, Ljava/io/FileOutputStream;

    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->folderPath:Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "pic_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p2

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ".png"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-direct {v0, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 761
    .local v17, "out":Ljava/io/FileOutputStream;
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v4, v5, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 762
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 767
    .end local v9    # "mtx":Landroid/graphics/Matrix;
    .end local v17    # "out":Ljava/io/FileOutputStream;
    .end local v21    # "rotatedBMP":Landroid/graphics/Bitmap;
    :cond_6
    :goto_2
    return-void

    .line 208
    .restart local v11    # "adj":I
    :cond_7
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    float-to-int v11, v4

    goto/16 :goto_0

    .line 236
    .end local v11    # "adj":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "line"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 237
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->flipValue:Ljava/lang/String;

    if-nez v4, :cond_a

    .line 238
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_9

    .line 239
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 241
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_5

    .line 242
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 245
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_b

    .line 246
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 248
    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_5

    .line 249
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 252
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "rect"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "Rectangle"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "snip2DiagRect"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 255
    :cond_d
    new-instance v20, Landroid/graphics/RectF;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    double-to-float v7, v0

    add-float/2addr v6, v7

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    double-to-float v8, v0

    add-float/2addr v7, v8

    move-object/from16 v0, v20

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 260
    .restart local v20    # "rect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getAlpha()I

    move-result v4

    const/16 v5, 0x4d

    if-ne v4, v5, :cond_e

    .line 261
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 264
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_f

    .line 265
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 268
    :cond_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_10

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_10

    .line 270
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    const/16 v5, 0xff

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 271
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 275
    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x441c4000    # 625.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 276
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x441c4000    # 625.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 277
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 278
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 281
    .end local v20    # "rect":Landroid/graphics/RectF;
    :cond_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "Ellipse"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_12

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "Oval"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 284
    :cond_12
    const/16 v20, 0x0

    .line 288
    .restart local v20    # "rect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    const/high16 v5, 0x41200000    # 10.0f

    cmpl-float v4, v4, v5

    if-ltz v4, :cond_17

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    const/high16 v5, 0x41200000    # 10.0f

    cmpl-float v4, v4, v5

    if-ltz v4, :cond_17

    .line 289
    new-instance v20, Landroid/graphics/RectF;

    .end local v20    # "rect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v4, v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    float-to-double v6, v6

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    move-wide/from16 v24, v0

    const-wide/high16 v26, 0x4000000000000000L    # 2.0

    mul-double v24, v24, v26

    sub-double v6, v6, v24

    double-to-float v6, v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    float-to-double v0, v7

    move-wide/from16 v24, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    move-wide/from16 v26, v0

    sub-double v24, v24, v26

    move-wide/from16 v0, v24

    double-to-float v7, v0

    move-object/from16 v0, v20

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 304
    .restart local v20    # "rect":Landroid/graphics/RectF;
    :cond_13
    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_14

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getAlpha()I

    move-result v4

    const/16 v5, 0x4d

    if-ne v4, v5, :cond_14

    .line 305
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 308
    :cond_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_15

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_15

    .line 309
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 311
    :cond_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_16

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_16

    .line 312
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 315
    :cond_16
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x4545b000    # 3163.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 316
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x4545b000    # 3163.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 317
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46900a00    # 18437.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 318
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46900a00    # 18437.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 292
    :cond_17
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    const/high16 v5, 0x41200000    # 10.0f

    cmpg-float v4, v4, v5

    if-gtz v4, :cond_18

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    const/high16 v5, 0x41200000    # 10.0f

    cmpg-float v4, v4, v5

    if-gtz v4, :cond_18

    .line 293
    new-instance v20, Landroid/graphics/RectF;

    .end local v20    # "rect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v4, v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    const/high16 v6, 0x41200000    # 10.0f

    const/high16 v7, 0x41200000    # 10.0f

    move-object/from16 v0, v20

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .restart local v20    # "rect":Landroid/graphics/RectF;
    goto/16 :goto_3

    .line 295
    :cond_18
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    const/high16 v5, 0x41200000    # 10.0f

    cmpg-float v4, v4, v5

    if-gtz v4, :cond_19

    .line 296
    new-instance v20, Landroid/graphics/RectF;

    .end local v20    # "rect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v4, v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    const/high16 v6, 0x41200000    # 10.0f

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    float-to-double v0, v7

    move-wide/from16 v24, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    move-wide/from16 v26, v0

    sub-double v24, v24, v26

    move-wide/from16 v0, v24

    double-to-float v7, v0

    move-object/from16 v0, v20

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .restart local v20    # "rect":Landroid/graphics/RectF;
    goto/16 :goto_3

    .line 298
    :cond_19
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    const/high16 v5, 0x41200000    # 10.0f

    cmpg-float v4, v4, v5

    if-gtz v4, :cond_13

    .line 299
    new-instance v20, Landroid/graphics/RectF;

    .end local v20    # "rect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v4, v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    float-to-double v6, v6

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    move-wide/from16 v24, v0

    const-wide/high16 v26, 0x4000000000000000L    # 2.0

    mul-double v24, v24, v26

    sub-double v6, v6, v24

    double-to-float v6, v6

    const/high16 v7, 0x41200000    # 10.0f

    move-object/from16 v0, v20

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .restart local v20    # "rect":Landroid/graphics/RectF;
    goto/16 :goto_3

    .line 321
    .end local v20    # "rect":Landroid/graphics/RectF;
    :cond_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "Diamond"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1e

    .line 322
    new-instance v19, Landroid/graphics/Path;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Path;-><init>()V

    .line 323
    .local v19, "path":Landroid/graphics/Path;
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 324
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 325
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 327
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 331
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Path;->close()V

    .line 334
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_1b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getAlpha()I

    move-result v4

    const/16 v5, 0x4d

    if-ne v4, v5, :cond_1b

    .line 335
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 338
    :cond_1b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_1c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_1c

    .line 339
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 341
    :cond_1c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_1d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_1d

    .line 342
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 347
    :cond_1d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x45a8c000    # 5400.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 348
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x45a8c000    # 5400.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 349
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x467d2000    # 16200.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 350
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x467d2000    # 16200.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 353
    .end local v19    # "path":Landroid/graphics/Path;
    :cond_1e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "IsocelesTriangle"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "Triangle"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_24

    .line 359
    :cond_1f
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    if-nez v4, :cond_20

    .line 360
    const/16 v4, 0x2a30

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    .line 362
    :cond_20
    new-instance v19, Landroid/graphics/Path;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Path;-><init>()V

    .line 363
    .restart local v19    # "path":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 365
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 366
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 369
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Path;->close()V

    .line 372
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_21

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getAlpha()I

    move-result v4

    const/16 v5, 0x4d

    if-ne v4, v5, :cond_21

    .line 373
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 376
    :cond_21
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_22

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_22

    .line 377
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 379
    :cond_22
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_23

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_23

    .line 380
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 384
    :cond_23
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x45d2f000    # 6750.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 385
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const/high16 v5, 0x463e0000    # 12160.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 386
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46680800    # 14850.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 387
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 390
    .end local v19    # "path":Landroid/graphics/Path;
    :cond_24
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "RightTriangle"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_25

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "rtTriangle"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_29

    .line 392
    :cond_25
    new-instance v19, Landroid/graphics/Path;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Path;-><init>()V

    .line 393
    .restart local v19    # "path":Landroid/graphics/Path;
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 394
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 395
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 396
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Path;->close()V

    .line 399
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_26

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getAlpha()I

    move-result v4

    const/16 v5, 0x4d

    if-ne v4, v5, :cond_26

    .line 400
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 403
    :cond_26
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_27

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_27

    .line 404
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 406
    :cond_27
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_28

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_28

    .line 407
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 411
    :cond_28
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x4528c000    # 2700.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 412
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x4652f000    # 13500.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 413
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x4628c000    # 10800.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 414
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x4693a800    # 18900.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 417
    .end local v19    # "path":Landroid/graphics/Path;
    :cond_29
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "Parallelogram"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2e

    .line 422
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    if-nez v4, :cond_2a

    .line 423
    const/16 v4, 0x1518

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    .line 425
    :cond_2a
    new-instance v19, Landroid/graphics/Path;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Path;-><init>()V

    .line 426
    .restart local v19    # "path":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 428
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 429
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    sub-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 431
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 432
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Path;->close()V

    .line 435
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_2b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getAlpha()I

    move-result v4

    const/16 v5, 0x4d

    if-ne v4, v5, :cond_2b

    .line 436
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 439
    :cond_2b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_2c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_2c

    .line 440
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 442
    :cond_2c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_2d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_2d

    .line 443
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 447
    :cond_2d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x45a8c000    # 5400.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 448
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x45a8c000    # 5400.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 449
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x467d2000    # 16200.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 450
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x467d2000    # 16200.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 453
    .end local v19    # "path":Landroid/graphics/Path;
    :cond_2e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "Pentagon"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_32

    .line 454
    new-instance v19, Landroid/graphics/Path;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Path;-><init>()V

    .line 455
    .restart local v19    # "path":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 456
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    const v6, 0x46010c00    # 8259.0f

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 458
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    const v6, 0x45834000    # 4200.0f

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    sub-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 460
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    const v5, 0x45834000    # 4200.0f

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 462
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    const v6, 0x46010c00    # 8259.0f

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 464
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Path;->close()V

    .line 467
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_2f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getAlpha()I

    move-result v4

    const/16 v5, 0x4d

    if-ne v4, v5, :cond_2f

    .line 468
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 471
    :cond_2f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_30

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_30

    .line 472
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 474
    :cond_30
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_31

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_31

    .line 475
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 479
    :cond_31
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x45bdd800    # 6075.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 480
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x45bdd800    # 6075.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 481
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46729400    # 15525.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 482
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46891c00    # 17550.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 485
    .end local v19    # "path":Landroid/graphics/Path;
    :cond_32
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "Hexagon"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_37

    .line 490
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    if-nez v4, :cond_33

    .line 491
    const/16 v4, 0x1518

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    .line 493
    :cond_33
    new-instance v19, Landroid/graphics/Path;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Path;-><init>()V

    .line 494
    .restart local v19    # "path":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 496
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    sub-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 498
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 500
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    sub-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 502
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 504
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 505
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Path;->close()V

    .line 508
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_34

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getAlpha()I

    move-result v4

    const/16 v5, 0x4d

    if-ne v4, v5, :cond_34

    .line 509
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 512
    :cond_34
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_35

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_35

    .line 513
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 515
    :cond_35
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_36

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_36

    .line 516
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 521
    :cond_36
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x45a8c000    # 5400.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 522
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x45a8c000    # 5400.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 523
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x467d2000    # 16200.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 524
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x467d2000    # 16200.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 527
    .end local v19    # "path":Landroid/graphics/Path;
    :cond_37
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "Octagon"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3c

    .line 532
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    if-nez v4, :cond_38

    .line 533
    const/16 v4, 0x18b6

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    .line 535
    :cond_38
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float v13, v4, v5

    .line 536
    .local v13, "adj_width":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float v12, v4, v5

    .line 538
    .local v12, "adj_height":F
    new-instance v19, Landroid/graphics/Path;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Path;-><init>()V

    .line 539
    .restart local v19    # "path":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v4, v13

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 540
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    sub-float/2addr v4, v13

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 542
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v5, v12

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 544
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    sub-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 546
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    sub-float/2addr v4, v13

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 548
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v4, v13

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 550
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    sub-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 552
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v5, v12

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 553
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Path;->close()V

    .line 556
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_39

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getAlpha()I

    move-result v4

    const/16 v5, 0x4d

    if-ne v4, v5, :cond_39

    .line 557
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 560
    :cond_39
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_3a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_3a

    .line 561
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 563
    :cond_3a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_3b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_3b

    .line 564
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 569
    :cond_3b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x457d2000    # 4050.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 570
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x457d2000    # 4050.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 571
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46891c00    # 17550.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 572
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46891c00    # 17550.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 575
    .end local v12    # "adj_height":F
    .end local v13    # "adj_width":F
    .end local v19    # "path":Landroid/graphics/Path;
    :cond_3c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "Plus"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "mathPlus"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_42

    .line 581
    :cond_3d
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    if-nez v4, :cond_3e

    .line 582
    const/16 v4, 0x1518

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    .line 584
    :cond_3e
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float v13, v4, v5

    .line 585
    .restart local v13    # "adj_width":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float v12, v4, v5

    .line 587
    .restart local v12    # "adj_height":F
    new-instance v19, Landroid/graphics/Path;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Path;-><init>()V

    .line 588
    .restart local v19    # "path":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v4, v13

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 589
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    sub-float/2addr v4, v13

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 591
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    sub-float/2addr v4, v13

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v5, v12

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 593
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v5, v12

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 595
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    sub-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 597
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    sub-float/2addr v4, v13

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    sub-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 599
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    sub-float/2addr v4, v13

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 601
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v4, v13

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 603
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v4, v13

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    sub-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 605
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    sub-float/2addr v5, v12

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 607
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v5, v12

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 608
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v4, v13

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v5, v12

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 610
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Path;->close()V

    .line 613
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_3f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getAlpha()I

    move-result v4

    const/16 v5, 0x4d

    if-ne v4, v5, :cond_3f

    .line 614
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 617
    :cond_3f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_40

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_40

    .line 618
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 620
    :cond_40
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_41

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_41

    .line 621
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 625
    :cond_41
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x45a8c000    # 5400.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 626
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x45a8c000    # 5400.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 627
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x467d2000    # 16200.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 628
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x467d2000    # 16200.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 631
    .end local v12    # "adj_height":F
    .end local v13    # "adj_width":F
    .end local v19    # "path":Landroid/graphics/Path;
    :cond_42
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "Can"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_49

    .line 635
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    if-nez v4, :cond_43

    .line 636
    const/16 v4, 0x1518

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    .line 638
    :cond_43
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    const v5, 0x46a8c000    # 21600.0f

    div-float v16, v4, v5

    .line 639
    .local v16, "mulFactor_Width":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    const v5, 0x46a8c000    # 21600.0f

    div-float v15, v4, v5

    .line 648
    .local v15, "mulFactor_Height":F
    new-instance v19, Landroid/graphics/Path;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Path;-><init>()V

    .line 649
    .restart local v19    # "path":Landroid/graphics/Path;
    const v4, 0x4628c000    # 10800.0f

    mul-float v4, v4, v16

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 651
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    double-to-float v7, v0

    add-float/2addr v6, v7

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    int-to-float v7, v7

    mul-float/2addr v7, v15

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    double-to-float v8, v0

    add-float/2addr v7, v8

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 654
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    int-to-float v6, v6

    mul-float/2addr v6, v15

    sub-float/2addr v5, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 656
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    mul-float/2addr v5, v15

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x4628c000    # 10800.0f

    mul-float v6, v6, v16

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    double-to-float v7, v0

    add-float/2addr v6, v7

    const v7, 0x46a8c000    # 21600.0f

    mul-float/2addr v7, v15

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    double-to-float v8, v0

    add-float/2addr v7, v8

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 660
    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v16

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    mul-float/2addr v5, v15

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    mul-float v6, v6, v16

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    double-to-float v7, v0

    add-float/2addr v6, v7

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    int-to-float v8, v8

    mul-float/2addr v8, v15

    sub-float/2addr v7, v8

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    double-to-float v8, v0

    add-float/2addr v7, v8

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 664
    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v16

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    int-to-float v5, v5

    mul-float/2addr v5, v15

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 666
    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v16

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x4628c000    # 10800.0f

    mul-float v6, v6, v16

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    double-to-float v7, v0

    add-float/2addr v6, v7

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    double-to-float v8, v0

    add-float/2addr v7, v8

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 669
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Path;->close()V

    .line 672
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_44

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getAlpha()I

    move-result v4

    const/16 v5, 0x4d

    if-ne v4, v5, :cond_44

    .line 673
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 676
    :cond_44
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_45

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_45

    .line 677
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 679
    :cond_45
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_46

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_46

    .line 680
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 683
    :cond_46
    new-instance v18, Landroid/graphics/Paint;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/Paint;-><init>()V

    .line 685
    .local v18, "paint2":Landroid/graphics/Paint;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_47

    .line 686
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->set(Landroid/graphics/Paint;)V

    .line 687
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getAlpha()I

    move-result v4

    add-int/lit8 v4, v4, -0x1e

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 689
    :cond_47
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    int-to-float v5, v5

    mul-float/2addr v5, v15

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 691
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    int-to-float v5, v5

    mul-float/2addr v5, v15

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x4628c000    # 10800.0f

    mul-float v6, v6, v16

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    double-to-float v7, v0

    add-float/2addr v6, v7

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    int-to-float v7, v7

    mul-float/2addr v7, v15

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    double-to-float v8, v0

    add-float/2addr v7, v8

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 695
    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v16

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    int-to-float v5, v5

    mul-float/2addr v5, v15

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    mul-float v6, v6, v16

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    double-to-float v7, v0

    add-float/2addr v6, v7

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->adjval0:I

    int-to-float v7, v7

    mul-float/2addr v7, v15

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    double-to-float v8, v0

    add-float/2addr v7, v8

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 701
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 702
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_48

    .line 703
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 716
    :cond_48
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 717
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x45a8c000    # 5400.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 718
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 719
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x467d2000    # 16200.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 722
    .end local v15    # "mulFactor_Height":F
    .end local v16    # "mulFactor_Width":F
    .end local v18    # "paint2":Landroid/graphics/Paint;
    .end local v19    # "path":Landroid/graphics/Path;
    :cond_49
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "TextBox"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 723
    new-instance v20, Landroid/graphics/RectF;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    double-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    double-to-float v7, v0

    add-float/2addr v6, v7

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineWidth:D

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    double-to-float v8, v0

    add-float/2addr v7, v8

    move-object/from16 v0, v20

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 727
    .restart local v20    # "rect":Landroid/graphics/RectF;
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 728
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_4a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_4a

    .line 729
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 732
    :cond_4a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_4b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_4b

    .line 733
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 737
    :cond_4b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x441c4000    # 625.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 738
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x441c4000    # 625.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 739
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 740
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->textArea:Landroid/graphics/RectF;

    const v5, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 763
    .end local v20    # "rect":Landroid/graphics/RectF;
    .restart local v9    # "mtx":Landroid/graphics/Matrix;
    .restart local v21    # "rotatedBMP":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v14

    .line 764
    .local v14, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception while writing pictures to out file"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method public drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F

    .prologue
    .line 79
    invoke-super {p0, p3, p4, p7, p8}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IFF)V

    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mDrawOnCanvas:Z

    .line 81
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mLeftMarg:I

    .line 82
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mTopMarg:I

    .line 83
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 84
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 85
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 86
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mDrawOnCanvas:Z

    .line 87
    return-void
.end method

.method public drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p2, "shapeCount"    # I

    .prologue
    .line 59
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V

    .line 60
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->draw(I)V

    .line 61
    return-void
.end method

.method public drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;I)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .param p2, "shapeCount"    # I

    .prologue
    .line 53
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;I)V

    .line 54
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->draw(I)V

    .line 55
    return-void
.end method

.method public drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mDrawOnCanvas:Z

    .line 96
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mLeftMarg:I

    .line 97
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mTopMarg:I

    .line 98
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    .line 99
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    .line 100
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 102
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getEscherSpRecord()Lorg/apache/poi/ddf/EscherSpRecord;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getEscherOptRecord()Lorg/apache/poi/ddf/EscherOptRecord;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->isHorizontalLine()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 104
    :cond_1
    invoke-super {p0, p1, p2, p6, p7}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;IFF)V

    .line 105
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 110
    :goto_0
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 111
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mDrawOnCanvas:Z

    .line 112
    return-void

    .line 107
    :cond_2
    invoke-direct {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->drawTextbox(Landroid/graphics/Canvas;I)V

    goto :goto_0
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V
    .locals 0
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 47
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 48
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->draw(I)V

    .line 49
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V
    .locals 2
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I

    .prologue
    .line 65
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mDrawOnCanvas:Z

    .line 67
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mLeftMarg:I

    .line 68
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mTopMarg:I

    .line 69
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 70
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 71
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mDrawOnCanvas:Z

    .line 73
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 136
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 137
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mDrawOnCanvas:Z

    .line 138
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mLeftMarg:I

    .line 139
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mTopMarg:I

    .line 140
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 141
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 142
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 143
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mDrawOnCanvas:Z

    .line 144
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;ZII)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .param p5, "isFlip"    # Z
    .param p6, "leftMargin"    # I
    .param p7, "topMargin"    # I

    .prologue
    .line 148
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V

    .line 149
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mDrawOnCanvas:Z

    .line 150
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mLeftMarg:I

    .line 151
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mTopMarg:I

    .line 153
    if-eqz p5, :cond_0

    .line 154
    const-string/jumbo v0, "rotate"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->flipValue:Ljava/lang/String;

    .line 156
    :cond_0
    int-to-float v0, p6

    int-to-float v1, p7

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 157
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 158
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 159
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mDrawOnCanvas:Z

    .line 160
    return-void
.end method

.method public drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .prologue
    .line 131
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 132
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->draw(I)V

    .line 133
    return-void
.end method

.method public drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 117
    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p7

    move v4, p8

    move-object v5, p9

    invoke-super/range {v0 .. v5}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSSExcelShape(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mDrawOnCanvas:Z

    .line 120
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mLeftMarg:I

    .line 121
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mTopMarg:I

    .line 122
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 123
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 124
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 125
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mDrawOnCanvas:Z

    .line 126
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;I)V
    .locals 1
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 163
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 164
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->draw(I)V

    .line 165
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIIILjava/lang/String;Z)V
    .locals 3
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # I
    .param p7, "height"    # I
    .param p8, "flipValue"    # Ljava/lang/String;
    .param p9, "isHeaderOrFooter"    # Z

    .prologue
    const/16 v2, 0x7f

    .line 170
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 171
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mDrawOnCanvas:Z

    .line 172
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mLeftMarg:I

    .line 173
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mTopMarg:I

    .line 174
    int-to-float v0, p6

    iput v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->width:F

    .line 175
    int-to-float v0, p7

    iput v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->height:F

    .line 176
    iput-object p8, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->flipValue:Ljava/lang/String;

    .line 177
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 178
    if-eqz p9, :cond_1

    .line 179
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v0, :cond_1

    .line 183
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->paintLine:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 186
    :cond_1
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 187
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 188
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->mDrawOnCanvas:Z

    .line 189
    return-void
.end method
