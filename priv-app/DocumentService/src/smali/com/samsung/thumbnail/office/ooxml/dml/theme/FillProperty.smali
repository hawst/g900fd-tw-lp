.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
.super Ljava/lang/Object;
.source "FillProperty.java"


# static fields
.field public static final EMPTY_FILL:I = 0xff

.field public static final FILL_BACKGROUND:I = 0x9

.field public static final FILL_GRADIENT:I = 0xa

.field public static final FILL_PATTERN:I = 0x1

.field public static final FILL_PICTURE:I = 0x3

.field public static final FILL_SHADE:I = 0x4

.field public static final FILL_SHADE_CENTER:I = 0x5

.field public static final FILL_SHADE_SCALE:I = 0x7

.field public static final FILL_SHADE_SHAPE:I = 0x6

.field public static final FILL_SHADE_TITLE:I = 0x8

.field public static final FILL_SOLID:I = 0xb

.field public static final FILL_TEXTURE:I = 0x2


# instance fields
.field public alpha:Ljava/lang/String;

.field public alphaOff:Ljava/lang/String;

.field public bgColor:Ljava/lang/String;

.field public bottomFill:I

.field public colorLst:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;",
            ">;"
        }
    .end annotation
.end field

.field public fillType:I

.field public fillrefColor:Ljava/lang/String;

.field public fontRef:Ljava/lang/String;

.field public foreColor:Ljava/lang/String;

.field public gradColorLst:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;",
            ">;>;"
        }
    .end annotation
.end field

.field public gradientAngle:I

.field public gradientColors:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public gradientPositions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public isSchemeClr:Z

.field public leftFill:I

.field public lumMod:Ljava/lang/String;

.field public lumOff:Ljava/lang/String;

.field public rightFill:I

.field public rotateWithShape:Z

.field public satMod:Ljava/lang/String;

.field public satOff:Ljava/lang/String;

.field public shade:Ljava/lang/String;

.field public tint:Ljava/lang/String;

.field public topFill:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradientColors:Ljava/util/ArrayList;

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradientPositions:Ljava/util/ArrayList;

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->colorLst:Ljava/util/ArrayList;

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradColorLst:Ljava/util/ArrayList;

    .line 96
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 1
    .param p1, "fillProps"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradientColors:Ljava/util/ArrayList;

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradientPositions:Ljava/util/ArrayList;

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->colorLst:Ljava/util/ArrayList;

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradColorLst:Ljava/util/ArrayList;

    .line 99
    if-nez p1, :cond_0

    .line 103
    :goto_0
    return-void

    .line 102
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->assign(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V

    goto :goto_0
.end method


# virtual methods
.method public addGradColorLst()V
    .locals 2

    .prologue
    .line 206
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 207
    .local v0, "val":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;>;"
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->colorLst:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 208
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradColorLst:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 209
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->colorLst:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 210
    return-void
.end method

.method public addPositionVal(F)V
    .locals 2
    .param p1, "pos"    # F

    .prologue
    .line 202
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradientPositions:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 203
    return-void
.end method

.method public addXDocColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V
    .locals 1
    .param p1, "color"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .prologue
    .line 194
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->colorLst:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 195
    return-void
.end method

.method public assign(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 1
    .param p1, "fillProps"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 221
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->fillType:I

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->fillType:I

    .line 222
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->foreColor:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->foreColor:Ljava/lang/String;

    .line 223
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->bgColor:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->bgColor:Ljava/lang/String;

    .line 224
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->rotateWithShape:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->rotateWithShape:Z

    .line 225
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradientColors:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradientColors:Ljava/util/ArrayList;

    .line 226
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradientPositions:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradientPositions:Ljava/util/ArrayList;

    .line 227
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradientAngle:I

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradientAngle:I

    .line 228
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->topFill:I

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->topFill:I

    .line 229
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->bottomFill:I

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->bottomFill:I

    .line 230
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->leftFill:I

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->leftFill:I

    .line 231
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->rightFill:I

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->rightFill:I

    .line 232
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->colorLst:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->colorLst:Ljava/util/ArrayList;

    .line 233
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradColorLst:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradColorLst:Ljava/util/ArrayList;

    .line 235
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->fillrefColor:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->fillrefColor:Ljava/lang/String;

    .line 236
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->fontRef:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->fontRef:Ljava/lang/String;

    .line 237
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->lumMod:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->lumMod:Ljava/lang/String;

    .line 238
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->lumOff:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->lumOff:Ljava/lang/String;

    .line 239
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->satMod:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->satMod:Ljava/lang/String;

    .line 240
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->satOff:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->satOff:Ljava/lang/String;

    .line 241
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->alpha:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->alpha:Ljava/lang/String;

    .line 242
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->alphaOff:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->alphaOff:Ljava/lang/String;

    .line 243
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->shade:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->shade:Ljava/lang/String;

    .line 244
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->tint:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->tint:Ljava/lang/String;

    .line 245
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->isSchemeClr:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->isSchemeClr:Z

    .line 246
    return-void
.end method

.method public getAlpha()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->alpha:Ljava/lang/String;

    return-object v0
.end method

.method public getAlphaOff()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->alphaOff:Ljava/lang/String;

    return-object v0
.end method

.method public getFillRefColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->fillrefColor:Ljava/lang/String;

    return-object v0
.end method

.method public getFontRef()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->fontRef:Ljava/lang/String;

    return-object v0
.end method

.method public getForeColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->foreColor:Ljava/lang/String;

    return-object v0
.end method

.method public getGradColorLst()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 213
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradColorLst:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getLumMod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->lumMod:Ljava/lang/String;

    return-object v0
.end method

.method public getLumOff()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->lumOff:Ljava/lang/String;

    return-object v0
.end method

.method public getPosition()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 217
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradientPositions:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSatMod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->satMod:Ljava/lang/String;

    return-object v0
.end method

.method public getSatOff()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->satOff:Ljava/lang/String;

    return-object v0
.end method

.method public getShade()Ljava/lang/String;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->shade:Ljava/lang/String;

    return-object v0
.end method

.method public getTint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->tint:Ljava/lang/String;

    return-object v0
.end method

.method public getXDocColor()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 198
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->colorLst:Ljava/util/ArrayList;

    return-object v0
.end method

.method public merge(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 2
    .param p1, "fillProp"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 256
    if-nez p1, :cond_0

    .line 294
    :goto_0
    return-void

    .line 259
    :cond_0
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->fillType:I

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->fillType:I

    .line 260
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->foreColor:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 261
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->foreColor:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->foreColor:Ljava/lang/String;

    .line 263
    :cond_1
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->bgColor:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 264
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->bgColor:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->bgColor:Ljava/lang/String;

    .line 266
    :cond_2
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->rotateWithShape:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->rotateWithShape:Z

    .line 267
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradientColors:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    .line 268
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradientColors:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradientColors:Ljava/util/ArrayList;

    .line 270
    :cond_3
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradientPositions:Ljava/util/ArrayList;

    if-nez v0, :cond_4

    .line 271
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradientPositions:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradientPositions:Ljava/util/ArrayList;

    .line 273
    :cond_4
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->colorLst:Ljava/util/ArrayList;

    if-nez v0, :cond_5

    .line 274
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->colorLst:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->colorLst:Ljava/util/ArrayList;

    .line 276
    :cond_5
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradColorLst:Ljava/util/ArrayList;

    if-nez v0, :cond_6

    .line 277
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradColorLst:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradColorLst:Ljava/util/ArrayList;

    .line 279
    :cond_6
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->tint:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->tint:Ljava/lang/String;

    const-string/jumbo v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 280
    :cond_7
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->tint:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->tint:Ljava/lang/String;

    .line 282
    :cond_8
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->shade:Ljava/lang/String;

    if-nez v0, :cond_9

    .line 283
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->shade:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->shade:Ljava/lang/String;

    .line 285
    :cond_9
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->alpha:Ljava/lang/String;

    if-nez v0, :cond_a

    .line 286
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->alpha:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->alpha:Ljava/lang/String;

    .line 289
    :cond_a
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradientAngle:I

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->gradientAngle:I

    .line 290
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->topFill:I

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->topFill:I

    .line 291
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->bottomFill:I

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->bottomFill:I

    .line 292
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->leftFill:I

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->leftFill:I

    .line 293
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->rightFill:I

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->rightFill:I

    goto :goto_0
.end method

.method public setAlpha(Ljava/lang/String;)V
    .locals 0
    .param p1, "alpha"    # Ljava/lang/String;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->alpha:Ljava/lang/String;

    .line 163
    return-void
.end method

.method public setAlphaOff(Ljava/lang/String;)V
    .locals 0
    .param p1, "alphaOff"    # Ljava/lang/String;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->alphaOff:Ljava/lang/String;

    .line 171
    return-void
.end method

.method public setFillRefColor(Ljava/lang/String;)V
    .locals 0
    .param p1, "fillrefColor"    # Ljava/lang/String;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->fillrefColor:Ljava/lang/String;

    .line 115
    return-void
.end method

.method public setFontRefColor(Ljava/lang/String;)V
    .locals 0
    .param p1, "fontRef"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->fontRef:Ljava/lang/String;

    .line 123
    return-void
.end method

.method public setForeColor(Ljava/lang/String;)V
    .locals 0
    .param p1, "foreColor"    # Ljava/lang/String;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->foreColor:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public setLumMod(Ljava/lang/String;)V
    .locals 0
    .param p1, "lumMod"    # Ljava/lang/String;

    .prologue
    .line 130
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->lumMod:Ljava/lang/String;

    .line 131
    return-void
.end method

.method public setLumOff(Ljava/lang/String;)V
    .locals 0
    .param p1, "lumOff"    # Ljava/lang/String;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->lumOff:Ljava/lang/String;

    .line 139
    return-void
.end method

.method public setSatMod(Ljava/lang/String;)V
    .locals 0
    .param p1, "satMod"    # Ljava/lang/String;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->satMod:Ljava/lang/String;

    .line 147
    return-void
.end method

.method public setSatOff(Ljava/lang/String;)V
    .locals 0
    .param p1, "satOff"    # Ljava/lang/String;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->satOff:Ljava/lang/String;

    .line 155
    return-void
.end method

.method public setShade(Ljava/lang/String;)V
    .locals 0
    .param p1, "shade"    # Ljava/lang/String;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->shade:Ljava/lang/String;

    .line 179
    return-void
.end method

.method public setTint(Ljava/lang/String;)V
    .locals 0
    .param p1, "tint"    # Ljava/lang/String;

    .prologue
    .line 186
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->tint:Ljava/lang/String;

    .line 187
    return-void
.end method
