.class public final enum Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;
.super Ljava/lang/Enum;
.source "XWPFShapeStyle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EStrokeJoinStyle"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

.field public static final enum BEVEL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

.field public static final enum MITER:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

.field public static final enum ROUND:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 207
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

    const-string/jumbo v1, "ROUND"

    const-string/jumbo v2, "round"

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;->ROUND:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

    .line 210
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

    const-string/jumbo v1, "BEVEL"

    const-string/jumbo v2, "bevel"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;->BEVEL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

    .line 213
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

    const-string/jumbo v1, "MITER"

    const-string/jumbo v2, "miter"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;->MITER:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

    .line 203
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;->ROUND:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;->BEVEL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;->MITER:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 216
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 217
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;->value:Ljava/lang/String;

    .line 218
    return-void
.end method

.method public static convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 225
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;->values()[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 226
    .local v2, "inst":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 230
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;
    :goto_1
    return-object v2

    .line 225
    .restart local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 230
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 203
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;
    .locals 1

    .prologue
    .line 203
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

    return-object v0
.end method


# virtual methods
.method public xmlValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;->value:Ljava/lang/String;

    return-object v0
.end method
