.class public Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;
.super Ljava/lang/Object;
.source "XWPFNum.java"


# instance fields
.field private abstractNumId:I

.field private lvlOverrideLst:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;",
            ">;"
        }
    .end annotation
.end field

.field private numId:I

.field private numbering:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;->numbering:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;

    .line 35
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;)V
    .locals 0
    .param p1, "numbering"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;->numbering:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;

    .line 39
    return-void
.end method


# virtual methods
.method public addLvlOverride(Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;)V
    .locals 1
    .param p1, "lvlOverride"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;->lvlOverrideLst:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;->lvlOverrideLst:Ljava/util/ArrayList;

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;->lvlOverrideLst:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    return-void
.end method

.method public getAbstractNumId()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;->abstractNumId:I

    return v0
.end method

.method public getLvlOverride()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;->lvlOverrideLst:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getNumId()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;->numId:I

    return v0
.end method

.method public getNumbering()Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;->numbering:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;

    return-object v0
.end method

.method public setAbstractNumId(I)V
    .locals 0
    .param p1, "abstractNumId"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;->abstractNumId:I

    .line 47
    return-void
.end method

.method public setNumId(I)V
    .locals 0
    .param p1, "numId"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;->numId:I

    .line 43
    return-void
.end method

.method public setNumbering(Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;)V
    .locals 0
    .param p1, "numbering"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;->numbering:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;

    .line 74
    return-void
.end method
