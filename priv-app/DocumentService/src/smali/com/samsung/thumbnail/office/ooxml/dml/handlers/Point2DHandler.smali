.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "Point2DHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler$IPointConsumer;
    }
.end annotation


# instance fields
.field consumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler$IPointConsumer;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler$IPointConsumer;)V
    .locals 1
    .param p1, "eleName"    # Ljava/lang/String;
    .param p2, "pConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler$IPointConsumer;

    .prologue
    .line 19
    const/16 v0, 0x1f

    invoke-direct {p0, v0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 20
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler$IPointConsumer;

    .line 21
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 7
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 26
    const-string/jumbo v6, "x"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    .line 27
    .local v2, "xVal":Ljava/lang/String;
    const-string/jumbo v6, "y"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v3

    .line 34
    .local v3, "yVal":Ljava/lang/String;
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 35
    .local v0, "x":J
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 37
    .local v4, "y":J
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler$IPointConsumer;

    if-nez v6, :cond_0

    .line 42
    :goto_0
    return-void

    .line 41
    :cond_0
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler$IPointConsumer;

    invoke-interface {v6, v0, v1, v4, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler$IPointConsumer;->setPoint(JJ)V

    goto :goto_0
.end method
