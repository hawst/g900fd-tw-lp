.class public Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;
.super Ljava/lang/Object;
.source "XSLFBackground.java"


# instance fields
.field private bgClrRef:Ljava/lang/String;

.field private bgFillRefNumber:I

.field private fillProp:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

.field private picture:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->bgFillRefNumber:I

    .line 18
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->fillProp:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .line 19
    return-void
.end method


# virtual methods
.method public getBgFillRefNumber()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->bgFillRefNumber:I

    return v0
.end method

.method public getBgRefClr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->bgClrRef:Ljava/lang/String;

    return-object v0
.end method

.method public getFillProperties()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->fillProp:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    return-object v0
.end method

.method public getPicture()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->picture:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

    return-object v0
.end method

.method public getPictureData()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->picture:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->picture:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->getPictureData()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    move-result-object v0

    .line 51
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBgFillRefNumber(I)V
    .locals 0
    .param p1, "refNum"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->bgFillRefNumber:I

    .line 41
    return-void
.end method

.method public setBgRefClr(Ljava/lang/String;)V
    .locals 0
    .param p1, "bgRefClr"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->bgClrRef:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public setBlipFill(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;)V
    .locals 2
    .param p1, "picture"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->picture:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

    .line 23
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->isTile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->fillProp:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    const/4 v1, 0x2

    iput v1, v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->fillType:I

    .line 25
    :cond_0
    return-void
.end method

.method public setFillProperties(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 0
    .param p1, "fillProp"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->fillProp:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .line 57
    return-void
.end method

.method public setFillType(I)V
    .locals 1
    .param p1, "fillType"    # I

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->fillProp:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->fillProp:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    iput p1, v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->fillType:I

    .line 67
    :cond_0
    return-void
.end method
