.class public final enum Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;
.super Ljava/lang/Enum;
.source "XWPFTable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EWidthType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

.field public static final enum AUTO:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

.field public static final enum DXA:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

.field public static final enum NIL:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

.field public static final enum PCT:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 373
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    const-string/jumbo v1, "NIL"

    const-string/jumbo v2, "nil"

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;->NIL:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    .line 375
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    const-string/jumbo v1, "PCT"

    const-string/jumbo v2, "pct"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;->PCT:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    .line 377
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    const-string/jumbo v1, "DXA"

    const-string/jumbo v2, "dxa"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;->DXA:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    .line 379
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    const-string/jumbo v1, "AUTO"

    const-string/jumbo v2, "auto"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;->AUTO:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    .line 371
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;->NIL:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;->PCT:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;->DXA:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;->AUTO:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 383
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 384
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;->value:Ljava/lang/String;

    .line 385
    return-void
.end method

.method public static convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 392
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;->values()[Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 393
    .local v2, "inst":Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 397
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;
    :goto_1
    return-object v2

    .line 392
    .restart local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 397
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 371
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;
    .locals 1

    .prologue
    .line 371
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    return-object v0
.end method


# virtual methods
.method public xmlValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;->value:Ljava/lang/String;

    return-object v0
.end method
