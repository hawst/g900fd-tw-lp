.class Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrJustify;
.super Ljava/lang/Object;
.source "XDocPPrHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "XPPrJustify"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;


# direct methods
.method constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrJustify;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V
    .locals 3
    .param p1, "value"    # Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;

    .prologue
    .line 183
    instance-of v2, p1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    if-eqz v2, :cond_2

    move-object v0, p1

    .line 184
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    .line 185
    .local v0, "justify":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 186
    .local v1, "val":Ljava/lang/String;
    const-string/jumbo v2, "both"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "distribute"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 188
    :cond_0
    const-string/jumbo v1, "justify"

    .line 191
    :cond_1
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$XPPrJustify;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;

    iget-object v2, v2, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;->paraProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v2, v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->setJustify(Ljava/lang/String;)V

    .line 193
    .end local v0    # "justify":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    .end local v1    # "val":Ljava/lang/String;
    :cond_2
    return-void
.end method
