.class public Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;
.super Ljava/lang/Object;
.source "XDocTblCellProperties.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;,
        Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;
    }
.end annotation


# instance fields
.field private borderProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

.field private cnfStyle:Ljava/lang/String;

.field private gridSpan:I

.field private hMerge:Z

.field private rowSpan:I

.field private shade:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

.field private txtDirection:Ljava/lang/String;

.field private vAlign:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

.field private vMerge:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

.field private width:I

.field private widthType:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->gridSpan:I

    .line 18
    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->rowSpan:I

    .line 25
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;->DXA:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->widthType:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    .line 26
    const-string/jumbo v0, "normal"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->txtDirection:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method public addRowSpan()V
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->rowSpan:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->rowSpan:I

    .line 79
    return-void
.end method

.method public getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->borderProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    return-object v0
.end method

.method public getCNFStyle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->cnfStyle:Ljava/lang/String;

    return-object v0
.end method

.method public getGridSpan()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->gridSpan:I

    return v0
.end method

.method public getHMerge()Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->hMerge:Z

    return v0
.end method

.method public getRowSpan()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->rowSpan:I

    return v0
.end method

.method public getShade()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->shade:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    return-object v0
.end method

.method public getTextDirection()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->txtDirection:Ljava/lang/String;

    return-object v0
.end method

.method public getVAlign()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->vAlign:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    return-object v0
.end method

.method public getVMerge()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->vMerge:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->width:I

    return v0
.end method

.method public getWidthType()Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->widthType:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    return-object v0
.end method

.method public setBorders(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V
    .locals 1
    .param p1, "borderType"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    .param p2, "border"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->borderProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    if-nez v0, :cond_0

    .line 103
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->borderProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->borderProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->setBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    .line 106
    return-void
.end method

.method public setCNFStyle(Ljava/lang/String;)V
    .locals 0
    .param p1, "cnfStyle"    # Ljava/lang/String;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->cnfStyle:Ljava/lang/String;

    .line 114
    return-void
.end method

.method public setGridSpan(I)V
    .locals 0
    .param p1, "gridSpan"    # I

    .prologue
    .line 38
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->gridSpan:I

    .line 39
    return-void
.end method

.method public setHMerge(Z)V
    .locals 0
    .param p1, "hMerge"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->hMerge:Z

    .line 47
    return-void
.end method

.method public setShade(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V
    .locals 0
    .param p1, "shade"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->shade:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .line 63
    return-void
.end method

.method public setTextDirection(Ljava/lang/String;)V
    .locals 0
    .param p1, "txtDirection"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->txtDirection:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public setVAlign(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;)V
    .locals 0
    .param p1, "vAlign"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->vAlign:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    .line 59
    return-void
.end method

.method public setVMerge(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;)V
    .locals 0
    .param p1, "vMerge"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->vMerge:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    .line 43
    return-void
.end method

.method public setWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->width:I

    .line 51
    return-void
.end method

.method public setWidthType(Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;)V
    .locals 0
    .param p1, "widthType"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->widthType:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    .line 55
    return-void
.end method
