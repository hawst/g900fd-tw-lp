.class public Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLMarkerColorHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DMLMarkerColorHandler.java"


# direct methods
.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 4
    .param p1, "ID"    # I
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 17
    const-string/jumbo v3, "marker"

    invoke-direct {p0, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 19
    const/4 v3, 0x1

    new-array v0, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 21
    .local v0, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;

    invoke-direct {v1, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 23
    .local v1, "shapePropHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v3, "spPr"

    invoke-direct {v2, v3, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 25
    .local v2, "shapePropSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/4 v3, 0x0

    aput-object v2, v0, v3

    .line 26
    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLMarkerColorHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 27
    return-void
.end method
