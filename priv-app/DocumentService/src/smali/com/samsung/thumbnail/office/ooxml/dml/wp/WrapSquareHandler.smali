.class public Lcom/samsung/thumbnail/office/ooxml/dml/wp/WrapSquareHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "WrapSquareHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/wp/WrapSquareHandler$IWrapSquare;
    }
.end annotation


# instance fields
.field mWrapSqaure:Lcom/samsung/thumbnail/office/ooxml/dml/wp/WrapSquareHandler$IWrapSquare;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/wp/WrapSquareHandler$IWrapSquare;)V
    .locals 2
    .param p1, "wrapSqrEle"    # Lcom/samsung/thumbnail/office/ooxml/dml/wp/WrapSquareHandler$IWrapSquare;

    .prologue
    .line 16
    const/16 v0, 0x1e

    const-string/jumbo v1, "wrapSquare"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 17
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/WrapSquareHandler;->mWrapSqaure:Lcom/samsung/thumbnail/office/ooxml/dml/wp/WrapSquareHandler$IWrapSquare;

    .line 18
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 23
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    const-string/jumbo v1, "wrapText"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/WrapSquareHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;-><init>(Ljava/lang/String;)V

    .line 25
    .local v0, "wrapVal":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/WrapSquareHandler;->mWrapSqaure:Lcom/samsung/thumbnail/office/ooxml/dml/wp/WrapSquareHandler$IWrapSquare;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/WrapSquareHandler$IWrapSquare;->getWrapSquareText(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 26
    return-void
.end method
