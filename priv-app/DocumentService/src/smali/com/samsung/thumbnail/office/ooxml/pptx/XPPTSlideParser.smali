.class public Lcom/samsung/thumbnail/office/ooxml/pptx/XPPTSlideParser;
.super Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBaseParser;
.source "XPPTSlideParser.java"


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .param p1, "consumer"    # Ljava/lang/Object;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBaseParser;-><init>(Ljava/lang/Object;)V

    .line 17
    return-void
.end method


# virtual methods
.method public createRootElementHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    .locals 3

    .prologue
    .line 24
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSlideHandler;

    const/16 v1, 0x33

    const-string/jumbo v2, "sld"

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSlideHandler;-><init>(ILjava/lang/String;)V

    return-object v0
.end method

.method public getMainNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .locals 1

    .prologue
    .line 29
    const/16 v0, 0x33

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XPPTSlideParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v0

    return-object v0
.end method

.method public startParse()V
    .locals 0

    .prologue
    .line 20
    return-void
.end method
