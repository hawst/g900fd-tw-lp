.class public Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;
.super Ljava/lang/Object;
.source "XWPFTableRow.java"


# instance fields
.field protected curCellNo:I

.field private rowProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;

.field private tableCells:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;->tableCells:Ljava/util/List;

    .line 42
    return-void
.end method


# virtual methods
.method public addCellCount(I)V
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;->curCellNo:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;->curCellNo:I

    .line 63
    return-void
.end method

.method public addTableCell(Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;)V
    .locals 1
    .param p1, "tableCell"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;->tableCells:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    return-void
.end method

.method public getCellList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;->tableCells:Ljava/util/List;

    return-object v0
.end method

.method public getRowProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;->rowProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;

    return-object v0
.end method

.method public setTblRowProp(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;)V
    .locals 0
    .param p1, "rowProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;->rowProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;

    .line 50
    return-void
.end method
