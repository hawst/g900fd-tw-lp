.class public Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
.super Ljava/lang/Object;
.source "WordDocCustomParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/word/WordDocCustomParser$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "WordDocCustomParser"


# instance fields
.field doc:Lorg/apache/poi/hwpf/HWPFDocument;

.field fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

.field private folderName:Ljava/lang/String;

.field private folderPath:Ljava/io/File;

.field fs:Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

.field private headerOfcDrawingArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;",
            ">;"
        }
    .end annotation
.end field

.field private headerOfficeDrawings:Lorg/apache/poi/hwpf/usermodel/OfficeDrawings;

.field private headerShapeCount:I

.field private headerStories:Lorg/apache/poi/hwpf/usermodel/HeaderStories;

.field private headerStoriesRange:Lorg/apache/poi/hwpf/usermodel/Range;

.field private imageCount:I

.field private inList:Z

.field private mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

.field private mContext:Landroid/content/Context;

.field private mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

.field private mainOfcDrawingArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;",
            ">;"
        }
    .end annotation
.end field

.field private mainOfficeDrawings:Lorg/apache/poi/hwpf/usermodel/OfficeDrawings;

.field private mainShapeCount:I

.field private numberingState:Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;

.field private paraNumber:I

.field picTable:Lorg/apache/poi/hwpf/model/PicturesTable;

.field private plcfHdd:Lorg/apache/poi/hwpf/model/PlexOfCps;

.field range:Lorg/apache/poi/hwpf/usermodel/Range;

.field private slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

.field private totalCellWidth:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->totalCellWidth:F

    .line 111
    new-instance v1, Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;

    invoke-direct {v1}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->numberingState:Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;

    .line 119
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->paraNumber:I

    .line 124
    iput-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->fs:Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    .line 126
    iput-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    .line 128
    iput-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->range:Lorg/apache/poi/hwpf/usermodel/Range;

    .line 130
    iput-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->picTable:Lorg/apache/poi/hwpf/model/PicturesTable;

    .line 132
    iput-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    .line 165
    iput-object p1, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mContext:Landroid/content/Context;

    .line 166
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->folderName:Ljava/lang/String;

    .line 167
    iget-object v1, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mContext:Landroid/content/Context;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->folderName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->folderPath:Ljava/io/File;

    .line 169
    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-direct {v0}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;-><init>()V

    .line 170
    .local v0, "slideDimention":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    new-instance v1, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-direct {v1, v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;-><init>(Lcom/samsung/thumbnail/customview/hslf/SlideDimention;)V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .line 171
    new-instance v1, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-direct {v1, v2}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;-><init>(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    .line 172
    return-void
.end method

.method private calColspan(I[Ljava/lang/String;II)I
    .locals 4
    .param p1, "cellWidth"    # I
    .param p2, "width"    # [Ljava/lang/String;
    .param p3, "afterCheck"    # I
    .param p4, "numCells"    # I

    .prologue
    .line 2185
    const/4 v1, 0x0

    .line 2186
    .local v1, "colSpan":I
    const/4 v0, 0x0

    .line 2187
    .local v0, "add":I
    move v2, p3

    .local v2, "j":I
    :goto_0
    array-length v3, p2

    if-ge v2, v3, :cond_0

    .line 2188
    aget-object v3, p2, v2

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    add-int/2addr v0, v3

    .line 2189
    if-ne p1, v0, :cond_1

    .line 2195
    :cond_0
    return v1

    .line 2192
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 2187
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private calculateTableXValue(ID)F
    .locals 6
    .param p1, "tableJustification"    # I
    .param p2, "totalCellWidth"    # D

    .prologue
    .line 2129
    iget-object v4, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getSlideWidth()D

    move-result-wide v2

    .line 2130
    .local v2, "totalWidth":D
    const-wide/16 v0, 0x0

    .line 2132
    .local v0, "calculatedWidth":D
    const/4 v4, 0x1

    if-eq p1, v4, :cond_0

    const/4 v4, 0x5

    if-ne p1, v4, :cond_2

    .line 2133
    :cond_0
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double v4, v2, v4

    sub-double v0, v4, p2

    .line 2138
    :cond_1
    :goto_0
    double-to-float v4, v0

    return v4

    .line 2135
    :cond_2
    const/4 v4, 0x2

    if-eq p1, v4, :cond_3

    const/16 v4, 0x8

    if-ne p1, v4, :cond_1

    .line 2136
    :cond_3
    sub-double v0, v2, p2

    goto :goto_0
.end method

.method private convertIco24ToARGB(I)Ljava/lang/String;
    .locals 11
    .param p1, "abgrValue"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v10, 0x1

    .line 1338
    const/4 v7, -0x1

    if-ne p1, v7, :cond_1

    .line 1358
    :cond_0
    :goto_0
    return-object v6

    .line 1342
    :cond_1
    shr-int/lit8 v7, p1, 0x0

    and-int/lit16 v5, v7, 0xff

    .line 1344
    .local v5, "r":I
    shr-int/lit8 v7, p1, 0x8

    and-int/lit16 v3, v7, 0xff

    .line 1346
    .local v3, "g":I
    shr-int/lit8 v7, p1, 0x10

    and-int/lit16 v2, v7, 0xff

    .line 1348
    .local v2, "b":I
    ushr-int/lit8 v0, p1, 0x18

    .line 1350
    .local v0, "a":I
    if-eqz v0, :cond_0

    .line 1355
    shl-int/lit8 v6, v0, 0x18

    shl-int/lit8 v7, v5, 0x10

    or-int/2addr v6, v7

    shl-int/lit8 v7, v3, 0x8

    or-int/2addr v6, v7

    or-int v1, v6, v2

    .line 1356
    .local v1, "argb":I
    const-string/jumbo v6, "#%08X"

    new-array v7, v10, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1358
    .local v4, "hexColor":Ljava/lang/String;
    invoke-virtual {v4, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method

.method private convertIco24ToRGB(I)Ljava/lang/String;
    .locals 7
    .param p1, "abgrValue"    # I

    .prologue
    .line 1315
    const/4 v5, -0x1

    if-ne p1, v5, :cond_0

    .line 1316
    const/4 v5, 0x0

    .line 1328
    :goto_0
    return-object v5

    .line 1318
    :cond_0
    const v5, 0xffffff

    and-int v0, p1, v5

    .line 1319
    .local v0, "bgrValue":I
    and-int/lit16 v5, v0, 0xff

    shl-int/lit8 v5, v5, 0x10

    const v6, 0xff00

    and-int/2addr v6, v0

    or-int/2addr v5, v6

    const/high16 v6, 0xff0000

    and-int/2addr v6, v0

    shr-int/lit8 v6, v6, 0x10

    or-int v4, v5, v6

    .line 1322
    .local v4, "rgbValue":I
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "#"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1323
    .local v3, "result":Ljava/lang/StringBuilder;
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    .line 1324
    .local v1, "hex":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    .local v2, "i":I
    :goto_1
    const/4 v5, 0x6

    if-ge v2, v5, :cond_1

    .line 1325
    const/16 v5, 0x30

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1324
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1327
    :cond_1
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1328
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method private drawHorizontalLine(Lorg/apache/poi/hwpf/usermodel/Picture;II)Lcom/samsung/thumbnail/customview/word/ShapesController;
    .locals 27
    .param p1, "picture"    # Lorg/apache/poi/hwpf/usermodel/Picture;
    .param p2, "leftIndentation"    # I
    .param p3, "topIndentation"    # I

    .prologue
    .line 1582
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Picture;->getHorizontalScalingFactor()I

    move-result v2

    .line 1583
    .local v2, "aspectRatioX":I
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Picture;->getVerticalScalingFactor()I

    move-result v22

    .line 1588
    .local v22, "aspectRatioY":I
    if-lez v2, :cond_0

    .line 1589
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDxaGoal()I

    move-result v6

    mul-int/2addr v6, v2

    div-int/lit16 v6, v6, 0x3e8

    invoke-static {v6}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v25

    .line 1595
    .local v25, "width":F
    :goto_0
    if-lez v22, :cond_1

    .line 1596
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDyaGoal()I

    move-result v6

    mul-int v6, v6, v22

    div-int/lit16 v6, v6, 0x3e8

    invoke-static {v6}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v11

    .line 1602
    .local v11, "height":F
    :goto_1
    new-instance v23, Lorg/apache/poi/hwpf/model/FSPA;

    invoke-direct/range {v23 .. v23}, Lorg/apache/poi/hwpf/model/FSPA;-><init>()V

    .line 1603
    .local v23, "fspaObj":Lorg/apache/poi/hwpf/model/FSPA;
    const/4 v6, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Lorg/apache/poi/hwpf/model/FSPA;->setXaLeft(I)V

    .line 1604
    const/4 v6, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Lorg/apache/poi/hwpf/model/FSPA;->setXaRight(I)V

    .line 1605
    const/4 v6, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Lorg/apache/poi/hwpf/model/FSPA;->setYaTop(I)V

    .line 1606
    const/4 v6, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Lorg/apache/poi/hwpf/model/FSPA;->setYaBottom(I)V

    .line 1607
    const/16 v6, 0x4e2

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Lorg/apache/poi/hwpf/model/FSPA;->setSpid(I)V

    .line 1609
    new-instance v5, Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    invoke-direct {v5}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;-><init>()V

    .line 1610
    .local v5, "shapeInfo":Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->setFSPA(Lorg/apache/poi/hwpf/model/FSPA;)V

    .line 1611
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->setHorizontalLine(Z)V

    .line 1614
    const-string/jumbo v4, "BasicShapes"

    .line 1616
    .local v4, "shapeCategory":Ljava/lang/String;
    move/from16 v24, p2

    .line 1617
    .local v24, "leftMargin":I
    const/16 v26, 0x0

    .line 1624
    .local v26, "zOrder":I
    new-instance v3, Lcom/samsung/thumbnail/customview/word/ShapesController;

    const/4 v6, 0x1

    const/4 v7, 0x1

    move/from16 v0, v24

    int-to-float v8, v0

    move/from16 v0, p3

    int-to-float v9, v0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move/from16 v0, v25

    float-to-double v12, v0

    invoke-virtual {v10, v12, v13}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocWidth(D)D

    move-result-wide v12

    double-to-int v10, v12

    int-to-float v10, v10

    move/from16 v0, v26

    int-to-long v12, v0

    const-wide/16 v14, 0x0

    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->ABSOLUTE:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    sget-object v17, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->LEFT_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    sget-object v18, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->ABSOLUTE:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    sget-object v19, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->TOP_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->folderPath:Ljava/io/File;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v21, v0

    invoke-direct/range {v3 .. v21}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/word/WordShapeInfo;IIFFFFJJLcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;Ljava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1632
    .local v3, "spCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v6, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOC:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v3, v6}, Lcom/samsung/thumbnail/customview/word/ShapesController;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 1634
    return-object v3

    .line 1592
    .end local v3    # "spCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    .end local v4    # "shapeCategory":Ljava/lang/String;
    .end local v5    # "shapeInfo":Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .end local v11    # "height":F
    .end local v23    # "fspaObj":Lorg/apache/poi/hwpf/model/FSPA;
    .end local v24    # "leftMargin":I
    .end local v25    # "width":F
    .end local v26    # "zOrder":I
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDxaGoal()I

    move-result v6

    invoke-static {v6}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v25

    .restart local v25    # "width":F
    goto/16 :goto_0

    .line 1599
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDyaGoal()I

    move-result v6

    invoke-static {v6}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v11

    .restart local v11    # "height":F
    goto/16 :goto_1
.end method

.method private drawOfficeDrawing(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V
    .locals 22
    .param p1, "paraType"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;
    .param p2, "docPara"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .prologue
    .line 2278
    const/16 v16, 0x0

    .line 2279
    .local v16, "officeDrawing":Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;
    sget-object v2, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->NORMAL:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_2

    .line 2280
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mainOfcDrawingArray:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mainOfcDrawingArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mainShapeCount:I

    if-le v2, v3, :cond_0

    .line 2282
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mainOfcDrawingArray:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mainShapeCount:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "officeDrawing":Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;
    check-cast v16, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;

    .line 2283
    .restart local v16    # "officeDrawing":Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mainShapeCount:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mainShapeCount:I

    .line 2295
    :cond_0
    :goto_0
    if-nez v16, :cond_3

    .line 2350
    :cond_1
    :goto_1
    return-void

    .line 2287
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->headerOfcDrawingArray:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->headerOfcDrawingArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->headerShapeCount:I

    if-le v2, v3, :cond_0

    .line 2289
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->headerOfcDrawingArray:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->headerShapeCount:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "officeDrawing":Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;
    check-cast v16, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;

    .line 2291
    .restart local v16    # "officeDrawing":Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->headerShapeCount:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->headerShapeCount:I

    goto :goto_0

    .line 2298
    :cond_3
    invoke-interface/range {v16 .. v16}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;->getPictureData()[B

    move-result-object v2

    if-eqz v2, :cond_5

    .line 2300
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->imageCount:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->imageCount:I

    .line 2301
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->imageCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".bmp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2303
    .local v5, "imageName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-interface/range {v16 .. v16}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;->getRectangleLeft()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v3

    float-to-double v0, v3

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v2, v0, v1}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocWidth(D)D

    move-result-wide v2

    double-to-int v6, v2

    .line 2305
    .local v6, "left":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-interface/range {v16 .. v16}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;->getRectangleRight()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v3

    float-to-double v0, v3

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v2, v0, v1}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocWidth(D)D

    move-result-wide v2

    double-to-int v8, v2

    .line 2307
    .local v8, "right":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-interface/range {v16 .. v16}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;->getRectangleTop()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v3

    float-to-double v0, v3

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v2, v0, v1}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocHeight(D)D

    move-result-wide v2

    double-to-int v7, v2

    .line 2309
    .local v7, "top":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-interface/range {v16 .. v16}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;->getRectangleBottom()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v3

    float-to-double v0, v3

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v2, v0, v1}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocHeight(D)D

    move-result-wide v2

    double-to-int v9, v2

    .line 2313
    .local v9, "bottom":I
    const/16 v17, 0x0

    .line 2314
    .local v17, "rotation":I
    invoke-interface/range {v16 .. v16}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;->getOfficeArtSpContainerList()Ljava/util/List;

    move-result-object v12

    check-cast v12, Ljava/util/ArrayList;

    .line 2317
    .local v12, "escherContainerRecordList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/ddf/EscherRecord;>;"
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 2318
    .local v13, "escherOptRcdList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_4

    .line 2319
    const/4 v2, 0x0

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/apache/poi/ddf/EscherRecord;

    .local v14, "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    move-object v11, v14

    .line 2320
    check-cast v11, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 2321
    .local v11, "escherContainerRecord":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v2, -0xff5

    invoke-virtual {v11, v2, v13}, Lorg/apache/poi/ddf/EscherContainerRecord;->getRecordsById(SLjava/util/List;)V

    .line 2324
    const/4 v2, 0x0

    invoke-interface {v13, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherOptRecord;

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/samsung/thumbnail/util/Utils;->getIntEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;S)I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->rotationAngleToDegree_97Format(I)I

    move-result v17

    .line 2330
    .end local v11    # "escherContainerRecord":Lorg/apache/poi/ddf/EscherContainerRecord;
    .end local v14    # "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    :cond_4
    invoke-interface/range {v16 .. v16}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;->getPictureData()[B

    move-result-object v3

    invoke-interface/range {v16 .. v16}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;->getEscherBlipRecord()Lorg/apache/poi/ddf/EscherBlipRecord;

    move-result-object v4

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v9}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->readPicture([BLorg/apache/poi/ddf/EscherRecord;Ljava/lang/String;IIII)Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    move-result-object v10

    .line 2335
    .local v10, "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    if-eqz v10, :cond_1

    .line 2336
    sget-object v2, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOC:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v10, v2}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 2337
    move/from16 v0, v17

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setRotationAngle(I)V

    .line 2338
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 2342
    .end local v5    # "imageName":Ljava/lang/String;
    .end local v6    # "left":I
    .end local v7    # "top":I
    .end local v8    # "right":I
    .end local v9    # "bottom":I
    .end local v10    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .end local v12    # "escherContainerRecordList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/ddf/EscherRecord;>;"
    .end local v13    # "escherOptRcdList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    .end local v17    # "rotation":I
    :cond_5
    const/16 v19, 0x0

    .line 2343
    .local v19, "shapesControllerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/ShapesController;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->readShape(Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;)Ljava/util/ArrayList;

    move-result-object v19

    .line 2345
    if-eqz v19, :cond_1

    .line 2346
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .line 2347
    .local v18, "shape":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto :goto_2
.end method

.method private filterText(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 2263
    const-string/jumbo v0, "&#39;"

    const-string/jumbo v1, "\'"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2264
    const-string/jumbo v0, "&#34;"

    const-string/jumbo v1, "\""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2265
    const-string/jumbo v0, "&quot;"

    const-string/jumbo v1, "\""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2266
    const-string/jumbo v0, "&amp;"

    const-string/jumbo v1, "&"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2267
    const-string/jumbo v0, "&lt;"

    const-string/jumbo v1, "<"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2268
    const-string/jumbo v0, "&gt;"

    const-string/jumbo v1, ">"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2269
    const-string/jumbo v0, "&nbsp;"

    const-string/jumbo v1, " "

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2270
    const-string/jumbo v0, "&apos;"

    const-string/jumbo v1, "\'"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2272
    const-string/jumbo v0, "\u000b"

    const-string/jumbo v1, "\n"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2273
    return-object p1
.end method

.method private findRowForColspan(IILjava/util/ArrayList;I)[Ljava/lang/String;
    .locals 5
    .param p1, "curruntRow"    # I
    .param p2, "numOfRows"    # I
    .param p4, "cellNo"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<[",
            "Ljava/lang/String;",
            ">;I)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 2201
    .local p3, "cellWidthLst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Ljava/lang/String;>;"
    move v2, p1

    .line 2202
    .local v2, "org":I
    const/4 v3, 0x0

    .line 2203
    .local v3, "val":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 2206
    .local v0, "changeToOrg":Z
    :cond_0
    const/4 v1, 0x0

    .line 2207
    .local v1, "noCells":I
    add-int/lit8 v4, p2, -0x1

    if-lt p1, v4, :cond_1

    .line 2208
    move p1, v2

    .line 2209
    const/4 v0, 0x1

    .line 2212
    :cond_1
    if-eqz v0, :cond_2

    .line 2213
    add-int/lit8 p1, p1, -0x1

    .line 2214
    invoke-virtual {p3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    array-length v1, v4

    .line 2220
    :goto_0
    if-ne v1, p4, :cond_0

    .line 2221
    invoke-virtual {p3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "val":[Ljava/lang/String;
    check-cast v3, [Ljava/lang/String;

    .line 2225
    .restart local v3    # "val":[Ljava/lang/String;
    return-object v3

    .line 2216
    :cond_2
    add-int/lit8 p1, p1, 0x1

    .line 2217
    invoke-virtual {p3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    array-length v1, v4

    goto :goto_0
.end method

.method private getParaAlignment(I)Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;
    .locals 1
    .param p1, "paraJustification"    # I

    .prologue
    .line 2236
    packed-switch p1, :pswitch_data_0

    .line 2251
    :pswitch_0
    sget-object v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->LEFT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    :goto_0
    return-object v0

    .line 2239
    :pswitch_1
    sget-object v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->CENTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    goto :goto_0

    .line 2242
    :pswitch_2
    sget-object v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->RIGHT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    goto :goto_0

    .line 2246
    :pswitch_3
    sget-object v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->JUSTIFY:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    goto :goto_0

    .line 2236
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getRowAndCellAlignment(I)I
    .locals 1
    .param p1, "alignKey"    # I

    .prologue
    .line 2100
    const/4 v0, 0x0

    .line 2101
    .local v0, "alignment":I
    packed-switch p1, :pswitch_data_0

    .line 2119
    :pswitch_0
    const/4 v0, 0x0

    .line 2122
    :goto_0
    return v0

    .line 2104
    :pswitch_1
    const/4 v0, 0x1

    .line 2105
    goto :goto_0

    .line 2108
    :pswitch_2
    const/4 v0, 0x2

    .line 2109
    goto :goto_0

    .line 2113
    :pswitch_3
    const/4 v0, 0x3

    .line 2114
    goto :goto_0

    .line 2101
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getTableBorderColor(I)Lorg/apache/poi/java/awt/Color;
    .locals 1
    .param p1, "borderColor"    # I

    .prologue
    .line 1977
    packed-switch p1, :pswitch_data_0

    .line 2016
    sget-object v0, Lorg/apache/poi/java/awt/Color;->BLACK:Lorg/apache/poi/java/awt/Color;

    :goto_0
    return-object v0

    .line 1981
    :pswitch_0
    sget-object v0, Lorg/apache/poi/java/awt/Color;->BLACK:Lorg/apache/poi/java/awt/Color;

    goto :goto_0

    .line 1983
    :pswitch_1
    sget-object v0, Lorg/apache/poi/java/awt/Color;->BLACK:Lorg/apache/poi/java/awt/Color;

    goto :goto_0

    .line 1985
    :pswitch_2
    sget-object v0, Lorg/apache/poi/java/awt/Color;->BLUE:Lorg/apache/poi/java/awt/Color;

    goto :goto_0

    .line 1987
    :pswitch_3
    sget-object v0, Lorg/apache/poi/java/awt/Color;->CYAN:Lorg/apache/poi/java/awt/Color;

    goto :goto_0

    .line 1989
    :pswitch_4
    sget-object v0, Lorg/apache/poi/java/awt/Color;->GREEN:Lorg/apache/poi/java/awt/Color;

    goto :goto_0

    .line 1991
    :pswitch_5
    sget-object v0, Lorg/apache/poi/java/awt/Color;->MAGENTA:Lorg/apache/poi/java/awt/Color;

    goto :goto_0

    .line 1993
    :pswitch_6
    sget-object v0, Lorg/apache/poi/java/awt/Color;->RED:Lorg/apache/poi/java/awt/Color;

    goto :goto_0

    .line 1995
    :pswitch_7
    sget-object v0, Lorg/apache/poi/java/awt/Color;->YELLOW:Lorg/apache/poi/java/awt/Color;

    goto :goto_0

    .line 1997
    :pswitch_8
    sget-object v0, Lorg/apache/poi/java/awt/Color;->WHITE:Lorg/apache/poi/java/awt/Color;

    goto :goto_0

    .line 1999
    :pswitch_9
    sget-object v0, Lorg/apache/poi/java/awt/Color;->BLUE:Lorg/apache/poi/java/awt/Color;

    goto :goto_0

    .line 2001
    :pswitch_a
    sget-object v0, Lorg/apache/poi/java/awt/Color;->CYAN:Lorg/apache/poi/java/awt/Color;

    goto :goto_0

    .line 2003
    :pswitch_b
    sget-object v0, Lorg/apache/poi/java/awt/Color;->GREEN:Lorg/apache/poi/java/awt/Color;

    goto :goto_0

    .line 2005
    :pswitch_c
    sget-object v0, Lorg/apache/poi/java/awt/Color;->MAGENTA:Lorg/apache/poi/java/awt/Color;

    goto :goto_0

    .line 2007
    :pswitch_d
    sget-object v0, Lorg/apache/poi/java/awt/Color;->RED:Lorg/apache/poi/java/awt/Color;

    goto :goto_0

    .line 2009
    :pswitch_e
    sget-object v0, Lorg/apache/poi/java/awt/Color;->YELLOW:Lorg/apache/poi/java/awt/Color;

    goto :goto_0

    .line 2011
    :pswitch_f
    sget-object v0, Lorg/apache/poi/java/awt/Color;->DARK_GRAY:Lorg/apache/poi/java/awt/Color;

    goto :goto_0

    .line 2013
    :pswitch_10
    sget-object v0, Lorg/apache/poi/java/awt/Color;->LIGHT_GRAY:Lorg/apache/poi/java/awt/Color;

    goto :goto_0

    .line 1977
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method private getTableBorderStyle(I)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .locals 1
    .param p1, "borderType"    # I

    .prologue
    .line 2032
    packed-switch p1, :pswitch_data_0

    .line 2088
    :pswitch_0
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    :goto_0
    return-object v0

    .line 2036
    :pswitch_1
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2038
    :pswitch_2
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2040
    :pswitch_3
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2042
    :pswitch_4
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOUBLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2045
    :pswitch_5
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2047
    :pswitch_6
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOTTED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2049
    :pswitch_7
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DASHED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2051
    :pswitch_8
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOT_DASH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2053
    :pswitch_9
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOT_DOT_DASH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2055
    :pswitch_a
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIPLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2057
    :pswitch_b
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2059
    :pswitch_c
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2061
    :pswitch_d
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2063
    :pswitch_e
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2065
    :pswitch_f
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2067
    :pswitch_10
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2069
    :pswitch_11
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2071
    :pswitch_12
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2073
    :pswitch_13
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2075
    :pswitch_14
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->WAVE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2077
    :pswitch_15
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOUBLE_WAVE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2079
    :pswitch_16
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DASH_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2081
    :pswitch_17
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DASH_DOT_STROKED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2083
    :pswitch_18
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THREE_D_EMBOSS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2085
    :pswitch_19
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THREE_D_ENGRAVE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    goto :goto_0

    .line 2032
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
    .end packed-switch
.end method

.method private getXDocBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .locals 4
    .param p1, "borderCode"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .prologue
    .line 805
    const/4 v0, 0x0

    .line 808
    .local v0, "xDocBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getLineWidth()I

    move-result v2

    if-lez v2, :cond_0

    .line 809
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .end local v0    # "xDocBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;-><init>()V

    .line 810
    .restart local v0    # "xDocBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 811
    .local v1, "xDocColor":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getColor()S

    move-result v2

    invoke-static {v2}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->getColor(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ThumbnailUtils;->getHighLightColor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 814
    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->setBorderColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    .line 815
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getLineWidth()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->setBorderSize(I)V

    .line 816
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getSpace()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->setBorderSpace(I)V

    .line 817
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getBorderType()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->getTableBorderStyle(I)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->setBorderStyle(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;)V

    .line 821
    .end local v1    # "xDocColor":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    :cond_0
    return-object v0
.end method

.method private init(Ljava/io/FileInputStream;)V
    .locals 7
    .param p1, "fileInputStream"    # Ljava/io/FileInputStream;

    .prologue
    .line 238
    :try_start_0
    new-instance v2, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    invoke-direct {v2, p1}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/InputStream;)V

    iput-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->fs:Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    .line 239
    new-instance v2, Lorg/apache/poi/hwpf/HWPFDocument;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->fs:Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    invoke-direct {v2, v3}, Lorg/apache/poi/hwpf/HWPFDocument;-><init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V

    iput-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->doc:Lorg/apache/poi/hwpf/HWPFDocument;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 251
    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    if-nez v2, :cond_2

    .line 287
    :cond_0
    :goto_0
    return-void

    .line 240
    :catch_0
    move-exception v0

    .line 241
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 242
    .local v1, "msg":Ljava/lang/String;
    const-string/jumbo v2, "WordDocCustomParser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    if-eqz v1, :cond_1

    const-string/jumbo v2, "Invalid header signature"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 244
    new-instance v2, Lcom/samsung/index/GeneralEncryptedDocException;

    invoke-direct {v2, v1}, Lcom/samsung/index/GeneralEncryptedDocException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 247
    :cond_1
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 254
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "msg":Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    .line 257
    :try_start_1
    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/HWPFDocument;->getFileInformationBlock()Lorg/apache/poi/hwpf/model/FileInformationBlock;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;
    :try_end_1
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 263
    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/HWPFDocument;->getRange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->range:Lorg/apache/poi/hwpf/usermodel/Range;

    .line 265
    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/HWPFDocument;->getPicturesTable()Lorg/apache/poi/hwpf/model/PicturesTable;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->picTable:Lorg/apache/poi/hwpf/model/PicturesTable;

    .line 268
    :try_start_2
    new-instance v2, Lorg/apache/poi/hwpf/usermodel/HeaderStories;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-direct {v2, v3}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;-><init>(Lorg/apache/poi/hwpf/HWPFDocument;)V

    iput-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->headerStories:Lorg/apache/poi/hwpf/usermodel/HeaderStories;

    .line 269
    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/HWPFDocument;->getHeaderStoryRange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->headerStoriesRange:Lorg/apache/poi/hwpf/usermodel/Range;

    .line 271
    new-instance v2, Lorg/apache/poi/hwpf/model/PlexOfCps;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/HWPFDocument;->getTableStream()[B

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getPlcfHddOffset()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getPlcfHddSize()I

    move-result v5

    const/4 v6, 0x0

    invoke-direct {v2, v3, v4, v5, v6}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    iput-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->plcfHdd:Lorg/apache/poi/hwpf/model/PlexOfCps;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 277
    :goto_1
    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/HWPFDocument;->getOfficeDrawingsMain()Lorg/apache/poi/hwpf/usermodel/OfficeDrawings;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mainOfficeDrawings:Lorg/apache/poi/hwpf/usermodel/OfficeDrawings;

    .line 279
    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mainOfficeDrawings:Lorg/apache/poi/hwpf/usermodel/OfficeDrawings;

    if-eqz v2, :cond_3

    .line 280
    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mainOfficeDrawings:Lorg/apache/poi/hwpf/usermodel/OfficeDrawings;

    invoke-interface {v2}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawings;->getOfficeDrawingsArray()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mainOfcDrawingArray:Ljava/util/ArrayList;

    .line 282
    :cond_3
    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/HWPFDocument;->getOfficeDrawingsHeaders()Lorg/apache/poi/hwpf/usermodel/OfficeDrawings;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->headerOfficeDrawings:Lorg/apache/poi/hwpf/usermodel/OfficeDrawings;

    .line 284
    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->headerOfficeDrawings:Lorg/apache/poi/hwpf/usermodel/OfficeDrawings;

    if-eqz v2, :cond_0

    .line 285
    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->headerOfficeDrawings:Lorg/apache/poi/hwpf/usermodel/OfficeDrawings;

    invoke-interface {v2}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawings;->getOfficeDrawingsArray()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->headerOfcDrawingArray:Ljava/util/ArrayList;

    goto/16 :goto_0

    .line 258
    :catch_1
    move-exception v0

    .line 259
    .local v0, "e":Lorg/apache/poi/EncryptedDocumentException;
    const-string/jumbo v2, "WordDocCustomParser"

    const-string/jumbo v3, "This document is encrypted. "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 273
    .end local v0    # "e":Lorg/apache/poi/EncryptedDocumentException;
    :catch_2
    move-exception v0

    .line 274
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "WordDocCustomParser"

    const-string/jumbo v3, "No headers or footers in the document"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private readCharRun(Lorg/apache/poi/hwpf/usermodel/CharacterRun;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 22
    .param p1, "charRun"    # Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .param p2, "bulletPrefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/hwpf/usermodel/CharacterRun;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/word/Run;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1124
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 1126
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 1128
    .local v11, "runList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Run;>;"
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isSpecialCharacter()Z

    move-result v18

    if-nez v18, :cond_0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isSymbol()Z

    move-result v18

    if-nez v18, :cond_0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isVanished()Z

    move-result v18

    if-nez v18, :cond_0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isMarkedDeleted()Z

    move-result v18

    if-nez v18, :cond_0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isMarkedInserted()Z

    move-result v18

    if-nez v18, :cond_0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isFldVanished()Z

    move-result v18

    if-nez v18, :cond_0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isObj()Z

    move-result v18

    if-nez v18, :cond_0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isOle2()Z

    move-result v18

    if-eqz v18, :cond_2

    .line 1132
    :cond_0
    const/4 v11, 0x0

    .line 1305
    .end local v11    # "runList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Run;>;"
    :cond_1
    :goto_0
    return-object v11

    .line 1134
    .restart local v11    # "runList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Run;>;"
    :cond_2
    new-instance v10, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-direct {v10}, Lcom/samsung/thumbnail/customview/word/Run;-><init>()V

    .line 1136
    .local v10, "run":Lcom/samsung/thumbnail/customview/word/Run;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getFontName()Ljava/lang/String;

    move-result-object v5

    .line 1139
    .local v5, "fontName":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getFontSize()I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    const/high16 v19, 0x40000000    # 2.0f

    div-float v6, v18, v19

    .line 1141
    .local v6, "fontSize":F
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->text()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->stripFields(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    const-string/jumbo v19, "SEQ CHAPTER \\h \\r 1"

    const-string/jumbo v20, ""

    invoke-virtual/range {v18 .. v20}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v18

    const-string/jumbo v19, "EMBED Excel.Chart.8 \\s"

    const-string/jumbo v20, ""

    invoke-virtual/range {v18 .. v20}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v18

    const-string/jumbo v19, "\\* MERGEFORMATINET"

    const-string/jumbo v20, ""

    invoke-virtual/range {v18 .. v20}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v18

    const-string/jumbo v19, "PAGE   \\* MERGEFORMAT"

    const-string/jumbo v20, ""

    invoke-virtual/range {v18 .. v20}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v18

    const-string/jumbo v19, "SHAPE  \\* MERGEFORMAT"

    const-string/jumbo v20, ""

    invoke-virtual/range {v18 .. v20}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v16

    .line 1152
    .local v16, "text":Ljava/lang/String;
    const-string/jumbo v18, "INCLUDEPICTURE"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 1153
    const/4 v9, 0x0

    .line 1154
    .local v9, "removePicURl":Ljava/lang/String;
    const/16 v18, 0x22

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v18

    if-lez v18, :cond_3

    .line 1155
    const/16 v18, 0x22

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v18

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    .line 1156
    const-string/jumbo v18, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v9, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v16

    .line 1158
    :cond_3
    const-string/jumbo v18, "INCLUDEPICTURE"

    const-string/jumbo v19, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v16

    .line 1161
    .end local v9    # "removePicURl":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->filterText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 1169
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getIco24()I

    move-result v18

    const/16 v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_c

    .line 1170
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getIco24()I

    move-result v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->convertIco24ToRGB(I)Ljava/lang/String;

    move-result-object v17

    .line 1175
    .local v17, "textColor":Ljava/lang/String;
    :goto_1
    if-eqz v17, :cond_d

    .line 1176
    invoke-static/range {v17 .. v17}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 1184
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v18, v0

    float-to-double v0, v6

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocWidth(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 1186
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isItalic()Z

    move-result v18

    if-eqz v18, :cond_5

    .line 1187
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->setTextItalics()V

    .line 1190
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isBold()Z

    move-result v18

    if-eqz v18, :cond_e

    .line 1191
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v10, v5, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setTypeface(Ljava/lang/String;Z)V

    .line 1198
    :goto_3
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    if-lez v18, :cond_6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getUnderlineCode()I

    move-result v18

    if-lez v18, :cond_6

    .line 1199
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setUnderlineText(Z)V

    .line 1202
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isCapitalized()Z

    move-result v18

    if-eqz v18, :cond_f

    .line 1203
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->setCAPLetterON()V

    .line 1208
    :cond_7
    :goto_4
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isStrikeThrough()Z

    move-result v18

    if-eqz v18, :cond_8

    .line 1209
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setStrikeThruText(Z)V

    .line 1212
    :cond_8
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isDoubleStrikeThrough()Z

    move-result v18

    if-eqz v18, :cond_9

    .line 1213
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setStrikeThruText(Z)V

    .line 1216
    :cond_9
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getSubSuperScriptIndex()S

    move-result v18

    packed-switch v18, :pswitch_data_0

    .line 1244
    :goto_5
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getShading()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v14

    .line 1245
    .local v14, "shadeDesc":Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
    if-eqz v14, :cond_a

    .line 1246
    invoke-virtual {v14}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->getCvBack()Lorg/apache/poi/hwpf/model/Colorref;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hwpf/model/Colorref;->getValue()I

    move-result v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->convertIco24ToRGB(I)Ljava/lang/String;

    move-result-object v13

    .line 1248
    .local v13, "shadeColor":Ljava/lang/String;
    if-eqz v13, :cond_a

    .line 1249
    invoke-virtual {v10, v13}, Lcom/samsung/thumbnail/customview/word/Run;->setShadingColor(Ljava/lang/String;)V

    .line 1254
    .end local v13    # "shadeColor":Ljava/lang/String;
    :cond_a
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isHighlighted()Z

    move-result v18

    if-eqz v18, :cond_b

    .line 1255
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getHighlightedColor()B

    move-result v4

    .line 1256
    .local v4, "byteHLColor":B
    invoke-static {v4}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->getColor(I)Ljava/lang/String;

    move-result-object v7

    .line 1257
    .local v7, "hlColor":Ljava/lang/String;
    invoke-virtual {v10, v7}, Lcom/samsung/thumbnail/customview/word/Run;->setHighLightedColor(Ljava/lang/String;)V

    .line 1264
    .end local v4    # "byteHLColor":B
    .end local v7    # "hlColor":Ljava/lang/String;
    :cond_b
    :goto_6
    const-string/jumbo v18, "\t"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_11

    .line 1266
    const-string/jumbo v18, "\t"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_10

    .line 1268
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->clone()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/thumbnail/customview/word/Run;

    .line 1269
    .local v15, "tabRun":Lcom/samsung/thumbnail/customview/word/Run;
    if-eqz v15, :cond_b

    .line 1270
    const-string/jumbo v18, ""

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    .line 1271
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setTabStatus(Z)V

    .line 1272
    invoke-virtual {v11, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1273
    const-string/jumbo v18, "\t"

    const-string/jumbo v19, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 1274
    goto :goto_6

    .line 1172
    .end local v14    # "shadeDesc":Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
    .end local v15    # "tabRun":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v17    # "textColor":Ljava/lang/String;
    :cond_c
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getColor()I

    move-result v18

    invoke-static/range {v18 .. v18}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->getHexCode(I)Ljava/lang/String;

    move-result-object v17

    .restart local v17    # "textColor":Ljava/lang/String;
    goto/16 :goto_1

    .line 1180
    :cond_d
    const/16 v18, 0x0

    invoke-static/range {v18 .. v18}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->getHexCode(I)Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    goto/16 :goto_2

    .line 1193
    :cond_e
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v10, v5, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setTypeface(Ljava/lang/String;Z)V

    goto/16 :goto_3

    .line 1204
    :cond_f
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isSmallCaps()Z

    move-result v18

    if-eqz v18, :cond_7

    .line 1205
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->setCAPLetterON()V

    goto/16 :goto_4

    .line 1219
    :pswitch_0
    sget-object v18, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUPERSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setVertivalAlignment(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;)V

    goto/16 :goto_5

    .line 1223
    :pswitch_1
    sget-object v18, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUBSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setVertivalAlignment(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;)V

    goto/16 :goto_5

    .line 1280
    .restart local v14    # "shadeDesc":Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
    :cond_10
    const/16 v18, 0x0

    const/16 v19, 0x9

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v19

    move-object/from16 v0, v16

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 1282
    .local v12, "runText":Ljava/lang/String;
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->clone()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/customview/word/Run;

    .line 1283
    .local v8, "newRun":Lcom/samsung/thumbnail/customview/word/Run;
    if-eqz v8, :cond_b

    .line 1284
    invoke-virtual {v8, v12}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    .line 1285
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1287
    const/16 v18, 0x9

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v18

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_6

    .line 1294
    .end local v8    # "newRun":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v12    # "runText":Ljava/lang/String;
    :cond_11
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v18

    if-lez v18, :cond_1

    .line 1295
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->clone()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/customview/word/Run;

    .line 1296
    .restart local v8    # "newRun":Lcom/samsung/thumbnail/customview/word/Run;
    if-eqz v8, :cond_1

    .line 1297
    invoke-static/range {v16 .. v16}, Lcom/samsung/thumbnail/util/Utils;->getDirectionalText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    .line 1299
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1216
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private readEMFPictures([BLjava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "rawData"    # [B
    .param p2, "imageName"    # Ljava/lang/String;
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 1541
    const/4 v2, 0x0

    .line 1543
    .local v2, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->folderPath:Ljava/io/File;

    invoke-direct {v6, v7, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v3, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1545
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .local v3, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v5, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;

    int-to-float v6, p3

    int-to-float v7, p4

    invoke-direct {v5, p1, v6, v7}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;-><init>([BFF)V

    .line 1546
    .local v5, "readEMFFile":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawImage()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1547
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 1548
    sget-object v6, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v7, 0x19

    invoke-virtual {v0, v6, v7, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1550
    :cond_0
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-object v2, v3

    .line 1557
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .end local v5    # "readEMFFile":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->folderPath:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1559
    .local v4, "path":Ljava/lang/String;
    invoke-static {v4, p3, p4}, Lcom/samsung/thumbnail/util/Utils;->decodeSampledBitmapFromFile(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v6

    return-object v6

    .line 1551
    .end local v4    # "path":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 1552
    .local v1, "e":Ljava/io/FileNotFoundException;
    :goto_1
    const-string/jumbo v6, "WordDocCustomParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1553
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 1554
    .local v1, "e":Ljava/io/IOException;
    :goto_2
    const-string/jumbo v6, "WordDocCustomParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1553
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v1

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 1551
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v1

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_1
.end method

.method private readHeaderAndFooter(ILorg/apache/poi/hwpf/model/PlexOfCps;Lorg/apache/poi/hwpf/usermodel/Range;Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)V
    .locals 16
    .param p1, "plcfHddIndex"    # I
    .param p2, "plcfHdd"    # Lorg/apache/poi/hwpf/model/PlexOfCps;
    .param p3, "headerStoriesRange"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .param p4, "paraType"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    .prologue
    .line 423
    move-object/from16 v0, p2

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v9

    .line 424
    .local v9, "prop":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    invoke-virtual {v9}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getEnd()I

    move-result v14

    invoke-virtual {v9}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v15

    if-le v14, v15, :cond_2

    .line 426
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Range;->text()Ljava/lang/String;

    move-result-object v10

    .line 427
    .local v10, "rawText":Ljava/lang/String;
    invoke-virtual {v9}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v14

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v15

    invoke-static {v14, v15}, Ljava/lang/Math;->min(II)I

    move-result v11

    .line 428
    .local v11, "start":I
    invoke-virtual {v9}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getEnd()I

    move-result v14

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v15

    invoke-static {v14, v15}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 430
    .local v4, "end":I
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Range;->numParagraphs()I

    move-result v6

    .line 431
    .local v6, "numHeaderPara":I
    const/4 v2, 0x0

    .line 432
    .local v2, "charCount":I
    const/4 v8, 0x1

    .line 434
    .local v8, "paraCount":I
    const/4 v5, 0x0

    .line 435
    .local v5, "inTable":Z
    const/4 v13, 0x0

    .local v13, "val":I
    :goto_0
    if-ge v13, v6, :cond_2

    .line 437
    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Lorg/apache/poi/hwpf/usermodel/Range;->getParagraph(I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v7

    .line 439
    .local v7, "para":Lorg/apache/poi/hwpf/usermodel/Paragraph;
    if-lt v2, v11, :cond_0

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->text()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int/2addr v14, v2

    if-gt v14, v4, :cond_0

    .line 442
    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->isInTable()Z

    move-result v14

    if-eqz v14, :cond_1

    .line 444
    if-nez v5, :cond_0

    .line 447
    move-object/from16 v0, p3

    invoke-virtual {v0, v7}, Lorg/apache/poi/hwpf/usermodel/Range;->getTable(Lorg/apache/poi/hwpf/usermodel/Paragraph;)Lorg/apache/poi/hwpf/usermodel/Table;

    move-result-object v12

    .line 448
    .local v12, "table":Lorg/apache/poi/hwpf/usermodel/Table;
    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getJustification()I

    move-result v14

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v12, v14, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->readTables(Lorg/apache/poi/hwpf/usermodel/Table;ILcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)V

    .line 449
    const/4 v5, 0x1

    .line 461
    .end local v12    # "table":Lorg/apache/poi/hwpf/usermodel/Table;
    :cond_0
    :goto_1
    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->text()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int/2addr v2, v14

    .line 435
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 452
    :cond_1
    const/4 v5, 0x0

    .line 453
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v7, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->readPara(Lorg/apache/poi/hwpf/usermodel/Paragraph;Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    move-result-object v3

    .line 454
    .local v3, "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setParaType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)V

    .line 455
    invoke-virtual {v3, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setParaNumber(I)V

    .line 456
    add-int/lit8 v8, v8, 0x1

    .line 457
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v14

    invoke-virtual {v14, v3}, Lcom/samsung/thumbnail/office/word/Page;->addElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto :goto_1

    .line 464
    .end local v2    # "charCount":I
    .end local v3    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v4    # "end":I
    .end local v5    # "inTable":Z
    .end local v6    # "numHeaderPara":I
    .end local v7    # "para":Lorg/apache/poi/hwpf/usermodel/Paragraph;
    .end local v8    # "paraCount":I
    .end local v10    # "rawText":Ljava/lang/String;
    .end local v11    # "start":I
    .end local v13    # "val":I
    :cond_2
    return-void
.end method

.method private readOtherPictures([BLjava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "rawData"    # [B
    .param p2, "imageName"    # Ljava/lang/String;
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 1565
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 1567
    .local v1, "is":Ljava/io/InputStream;
    :try_start_0
    iget-object v3, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->folderName:Ljava/lang/String;

    invoke-static {v1, v3, p2, v4}, Lorg/apache/poi/util/IOUtils;->writeToFile(Ljava/io/InputStream;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1573
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->folderPath:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1575
    .local v2, "path":Ljava/lang/String;
    invoke-static {v2, p3, p4}, Lcom/samsung/thumbnail/util/Utils;->decodeSampledBitmapFromFile(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v3

    return-object v3

    .line 1568
    .end local v2    # "path":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1569
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v3, "WordDocCustomParser"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private readPara(Lorg/apache/poi/hwpf/usermodel/Paragraph;Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .locals 44
    .param p1, "para"    # Lorg/apache/poi/hwpf/usermodel/Paragraph;
    .param p2, "paraType"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    .prologue
    .line 527
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 529
    const/16 v28, 0x0

    .line 531
    .local v28, "picture":Lorg/apache/poi/hwpf/usermodel/Picture;
    const/16 v21, 0x0

    .line 533
    .local v21, "listLabel":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->paraNumber:I

    move/from16 v40, v0

    add-int/lit8 v40, v40, 0x1

    move/from16 v0, v40

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->paraNumber:I

    .line 535
    new-instance v11, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-direct {v11}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    .line 537
    .local v11, "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lorg/apache/poi/hwpf/HWPFDocument;->getStyleSheet()Lorg/apache/poi/hwpf/model/StyleSheet;

    move-result-object v40

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getStyleIndex()S

    move-result v41

    invoke-virtual/range {v40 .. v41}, Lorg/apache/poi/hwpf/model/StyleSheet;->getStyleDescription(I)Lorg/apache/poi/hwpf/model/StyleDescription;

    move-result-object v33

    .line 539
    .local v33, "styleDescrip":Lorg/apache/poi/hwpf/model/StyleDescription;
    invoke-virtual/range {v33 .. v33}, Lorg/apache/poi/hwpf/model/StyleDescription;->getName()Ljava/lang/String;

    move-result-object v34

    .line 541
    .local v34, "styleName":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getShading()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v32

    .line 542
    .local v32, "shadeDesc":Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
    if-eqz v32, :cond_0

    .line 543
    invoke-virtual/range {v32 .. v32}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->getCvBack()Lorg/apache/poi/hwpf/model/Colorref;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Lorg/apache/poi/hwpf/model/Colorref;->getValue()I

    move-result v40

    move-object/from16 v0, p0

    move/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->convertIco24ToARGB(I)Ljava/lang/String;

    move-result-object v26

    .line 545
    .local v26, "paraBGColor":Ljava/lang/String;
    if-eqz v26, :cond_0

    .line 546
    move-object/from16 v0, v26

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setBackgroundColor(Ljava/lang/String;)V

    .line 550
    .end local v26    # "paraBGColor":Ljava/lang/String;
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getTabStopsPositions()[I

    move-result-object v35

    .line 552
    .local v35, "tabStops":[I
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_0
    move-object/from16 v0, v35

    array-length v0, v0

    move/from16 v40, v0

    move/from16 v0, v40

    if-ge v15, v0, :cond_1

    .line 553
    new-instance v36, Lcom/samsung/thumbnail/customview/hslf/TabValues;

    const-string/jumbo v40, "left"

    aget v41, v35, v15

    invoke-static/range {v41 .. v41}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v41

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v41, v0

    move-object/from16 v0, v36

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/TabValues;-><init>(Ljava/lang/String;I)V

    .line 555
    .local v36, "tabValues":Lcom/samsung/thumbnail/customview/hslf/TabValues;
    move-object/from16 v0, v36

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addTabContents(Lcom/samsung/thumbnail/customview/hslf/TabValues;)V

    .line 552
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 558
    .end local v36    # "tabValues":Lcom/samsung/thumbnail/customview/hslf/TabValues;
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v40, v0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getSpacingBefore()I

    move-result v41

    invoke-static/range {v41 .. v41}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v41

    move/from16 v0, v41

    float-to-double v0, v0

    move-wide/from16 v42, v0

    move-object/from16 v0, v40

    move-wide/from16 v1, v42

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocHeight(D)D

    move-result-wide v40

    move-wide/from16 v0, v40

    double-to-float v0, v0

    move/from16 v38, v0

    .line 560
    .local v38, "topSpacing":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v40, v0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getSpacingAfter()I

    move-result v41

    invoke-static/range {v41 .. v41}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v41

    move/from16 v0, v41

    float-to-double v0, v0

    move-wide/from16 v42, v0

    move-object/from16 v0, v40

    move-wide/from16 v1, v42

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocHeight(D)D

    move-result-wide v40

    move-wide/from16 v0, v40

    double-to-float v7, v0

    .line 563
    .local v7, "bottomSpacing":F
    invoke-virtual {v11, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setBottomSpacing(F)V

    .line 564
    move/from16 v0, v38

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setTopSpacing(F)V

    .line 566
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getIndentFromLeft()I

    move-result v40

    invoke-static/range {v40 .. v40}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v40

    move/from16 v0, v40

    float-to-int v0, v0

    move/from16 v18, v0

    .line 570
    .local v18, "leftIndentation":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v40, v0

    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v42, v0

    move-object/from16 v0, v40

    move-wide/from16 v1, v42

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocWidth(D)D

    move-result-wide v40

    move-wide/from16 v0, v40

    double-to-int v0, v0

    move/from16 v19, v0

    .line 571
    .local v19, "leftMargin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v40, v0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getIndentFromRight()I

    move-result v41

    invoke-static/range {v41 .. v41}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v41

    move/from16 v0, v41

    float-to-double v0, v0

    move-wide/from16 v42, v0

    move-object/from16 v0, v40

    move-wide/from16 v1, v42

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocWidth(D)D

    move-result-wide v40

    move-wide/from16 v0, v40

    double-to-int v0, v0

    move/from16 v29, v0

    .line 573
    .local v29, "rightMargin":I
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getFirstLineIndent()I

    move-result v40

    invoke-static/range {v40 .. v40}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v40

    move/from16 v0, v40

    float-to-int v12, v0

    .line 576
    .local v12, "firstLineIndentation":I
    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v40, v0

    move/from16 v0, v40

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    .line 577
    move/from16 v0, v29

    int-to-float v0, v0

    move/from16 v40, v0

    move/from16 v0, v40

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setRightMargin(F)V

    .line 578
    int-to-float v0, v12

    move/from16 v40, v0

    move/from16 v0, v40

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setFirstLineMargin(F)V

    .line 580
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getLineSpacing()Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->get_dyaLine()S

    move-result v40

    move/from16 v0, v40

    int-to-float v0, v0

    move/from16 v20, v0

    .line 581
    .local v20, "lineSpacing":F
    const/16 v40, 0x0

    cmpg-float v40, v20, v40

    if-gez v40, :cond_4

    .line 582
    const/high16 v40, -0x40800000    # -1.0f

    mul-float v40, v40, v20

    move/from16 v0, v40

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineSpacing(F)V

    .line 590
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getLeftBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v40

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->getXDocBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    move-result-object v40

    move-object/from16 v0, v40

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    .line 591
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getTopBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v40

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->getXDocBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    move-result-object v40

    move-object/from16 v0, v40

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    .line 592
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getRightBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v40

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->getXDocBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    move-result-object v40

    move-object/from16 v0, v40

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    .line 593
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getBottomBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v40

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->getXDocBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    move-result-object v40

    move-object/from16 v0, v40

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    .line 595
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getJustification()I

    move-result v27

    .line 596
    .local v27, "paraJustification":I
    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->getParaAlignment(I)Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-result-object v40

    move-object/from16 v0, v40

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    .line 599
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->isInList()Z

    move-result v40

    if-eqz v40, :cond_2

    .line 600
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getList()Lorg/apache/poi/hwpf/usermodel/HWPFList;

    move-result-object v14

    .line 601
    .local v14, "hwpfList":Lorg/apache/poi/hwpf/usermodel/HWPFList;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->numberingState:Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;

    move-object/from16 v40, v0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getIlvl()I

    move-result v41

    move/from16 v0, v41

    int-to-char v0, v0

    move/from16 v41, v0

    move-object/from16 v0, v40

    move/from16 v1, v41

    invoke-static {v0, v14, v1}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->getBulletText(Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;Lorg/apache/poi/hwpf/usermodel/HWPFList;C)Ljava/lang/String;

    move-result-object v21

    .line 603
    const/16 v40, 0x1

    move/from16 v0, v40

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->inList:Z

    .line 608
    .end local v14    # "hwpfList":Lorg/apache/poi/hwpf/usermodel/HWPFList;
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->numCharacterRuns()I

    move-result v24

    .line 610
    .local v24, "numCharRun":I
    const-string/jumbo v10, ""

    .line 611
    .local v10, "charText":Ljava/lang/String;
    const-string/jumbo v23, ""

    .line 614
    .local v23, "newCharText":Ljava/lang/String;
    const/16 v39, 0x0

    .local v39, "y":I
    :goto_2
    move/from16 v0, v39

    move/from16 v1, v24

    if-ge v0, v1, :cond_3

    .line 616
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-boolean v0, v0, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    move/from16 v40, v0

    const/16 v41, 0x1

    move/from16 v0, v40

    move/from16 v1, v41

    if-ne v0, v1, :cond_6

    .line 793
    :cond_3
    const/16 v40, 0x0

    move/from16 v0, v40

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->inList:Z

    .line 795
    return-object v11

    .line 583
    .end local v10    # "charText":Ljava/lang/String;
    .end local v23    # "newCharText":Ljava/lang/String;
    .end local v24    # "numCharRun":I
    .end local v27    # "paraJustification":I
    .end local v39    # "y":I
    :cond_4
    const/high16 v40, 0x43700000    # 240.0f

    cmpl-float v40, v20, v40

    if-ltz v40, :cond_5

    .line 584
    const/high16 v40, 0x43700000    # 240.0f

    div-float v40, v20, v40

    move/from16 v0, v40

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineSpacing(F)V

    goto/16 :goto_1

    .line 586
    :cond_5
    move/from16 v0, v20

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineSpacing(F)V

    goto/16 :goto_1

    .line 619
    .restart local v10    # "charText":Ljava/lang/String;
    .restart local v23    # "newCharText":Ljava/lang/String;
    .restart local v24    # "numCharRun":I
    .restart local v27    # "paraJustification":I
    .restart local v39    # "y":I
    :cond_6
    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getCharacterRun(I)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v9

    .line 621
    .local v9, "charRun":Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    if-eqz v9, :cond_c

    .line 623
    invoke-virtual {v9}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->text()Ljava/lang/String;

    move-result-object v10

    .line 637
    const-string/jumbo v8, ""

    .line 638
    .local v8, "bulletPrefix":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->inList:Z

    move/from16 v40, v0

    if-eqz v40, :cond_7

    if-eqz v21, :cond_7

    .line 639
    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v40

    const/16 v41, 0x2

    move/from16 v0, v40

    move/from16 v1, v41

    if-le v0, v1, :cond_9

    if-nez v39, :cond_9

    .line 640
    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v41, 0x0

    const-string/jumbo v42, "\t"

    move-object/from16 v0, v21

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v42

    move-object/from16 v0, v21

    move/from16 v1, v41

    move/from16 v2, v42

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string/jumbo v41, "\t"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 648
    :cond_7
    :goto_3
    invoke-virtual {v9}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->text()Ljava/lang/String;

    move-result-object v40

    const-string/jumbo v41, "\u0013"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_12

    .line 649
    const/16 v25, 0x0

    .line 650
    .local v25, "p":I
    const/16 v17, 0x0

    .line 651
    .local v17, "isHref":Z
    move/from16 v25, v39

    :goto_4
    move/from16 v0, v25

    move/from16 v1, v24

    if-ge v0, v1, :cond_b

    .line 652
    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getCharacterRun(I)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v22

    .line 654
    .local v22, "newCharRun":Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    if-eqz v22, :cond_8

    .line 656
    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->text()Ljava/lang/String;

    move-result-object v23

    .line 658
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->picTable:Lorg/apache/poi/hwpf/model/PicturesTable;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/PicturesTable;->hasPicture(Lorg/apache/poi/hwpf/usermodel/CharacterRun;)Z

    move-result v40

    if-eqz v40, :cond_a

    .line 660
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->picTable:Lorg/apache/poi/hwpf/model/PicturesTable;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    move-object/from16 v0, v40

    move-object/from16 v1, v22

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/model/PicturesTable;->extractPicture(Lorg/apache/poi/hwpf/usermodel/CharacterRun;Z)Lorg/apache/poi/hwpf/usermodel/Picture;

    move-result-object v28

    .line 663
    if-eqz v28, :cond_8

    .line 664
    const/16 v40, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    move/from16 v2, v18

    move/from16 v3, v40

    move/from16 v4, v27

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->readPicture(Lorg/apache/poi/hwpf/usermodel/Picture;III)Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    move-result-object v6

    .line 667
    .local v6, "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    if-eqz v6, :cond_8

    .line 669
    sget-object v40, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOC:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-object/from16 v0, v40

    invoke-virtual {v6, v0}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 671
    invoke-virtual {v11, v6}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 651
    .end local v6    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    :cond_8
    :goto_5
    add-int/lit8 v25, v25, 0x1

    goto :goto_4

    .line 643
    .end local v17    # "isHref":Z
    .end local v22    # "newCharRun":Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .end local v25    # "p":I
    :cond_9
    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v40

    const/16 v41, 0x2

    move/from16 v0, v40

    move/from16 v1, v41

    if-ne v0, v1, :cond_7

    if-nez v39, :cond_7

    .line 644
    const-string/jumbo v8, "\u2022\t"

    goto :goto_3

    .line 678
    .restart local v17    # "isHref":Z
    .restart local v22    # "newCharRun":Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .restart local v25    # "p":I
    :cond_a
    const-string/jumbo v40, "\u0015"

    move-object/from16 v0, v40

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_d

    .line 747
    .end local v22    # "newCharRun":Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    :cond_b
    move/from16 v39, v25

    .line 614
    .end local v8    # "bulletPrefix":Ljava/lang/String;
    .end local v17    # "isHref":Z
    .end local v25    # "p":I
    :cond_c
    :goto_6
    add-int/lit8 v39, v39, 0x1

    goto/16 :goto_2

    .line 688
    .restart local v8    # "bulletPrefix":Ljava/lang/String;
    .restart local v17    # "isHref":Z
    .restart local v22    # "newCharRun":Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .restart local v25    # "p":I
    :cond_d
    if-eqz v23, :cond_e

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v40

    const-string/jumbo v41, "HYPERLINK"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_e

    const/16 v40, 0x22

    move-object/from16 v0, v23

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v40

    const/16 v41, -0x1

    move/from16 v0, v40

    move/from16 v1, v41

    if-le v0, v1, :cond_e

    .line 692
    const/16 v17, 0x1

    .line 698
    const/16 v40, 0x22

    move-object/from16 v0, v23

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v40

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->text()Ljava/lang/String;

    move-result-object v41

    const/16 v42, 0x22

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->indexOf(I)I

    move-result v41

    sub-int v40, v40, v41

    if-lez v40, :cond_8

    .line 700
    const/16 v40, 0x22

    move-object/from16 v0, v23

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v40

    add-int/lit8 v40, v40, 0x1

    const/16 v41, 0x22

    move-object/from16 v0, v23

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v41

    move-object/from16 v0, v23

    move/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    goto/16 :goto_5

    .line 705
    :cond_e
    if-eqz v17, :cond_f

    const-string/jumbo v40, "TOC"

    move-object/from16 v0, v34

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v40

    if-nez v40, :cond_f

    .line 708
    const-string/jumbo v40, ""

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v40

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->readCharRun(Lorg/apache/poi/hwpf/usermodel/CharacterRun;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v31

    .line 710
    .local v31, "runList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Run;>;"
    if-eqz v31, :cond_8

    .line 711
    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :goto_7
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v40

    if-eqz v40, :cond_8

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/samsung/thumbnail/customview/word/Run;

    .line 712
    .local v30, "run":Lcom/samsung/thumbnail/customview/word/Run;
    sget-object v40, Lorg/apache/poi/java/awt/Color;->BLUE:Lorg/apache/poi/java/awt/Color;

    invoke-virtual/range {v40 .. v40}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v40

    move-object/from16 v0, v30

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 713
    const/16 v40, 0x1

    move-object/from16 v0, v30

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setUnderlineText(Z)V

    .line 714
    move-object/from16 v0, v30

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto :goto_7

    .line 717
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v30    # "run":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v31    # "runList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Run;>;"
    :cond_f
    if-eqz v23, :cond_8

    .line 719
    move-object/from16 v37, v23

    .line 720
    .local v37, "text":Ljava/lang/String;
    :goto_8
    const-string/jumbo v40, "\u0008"

    move-object/from16 v0, v37

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v40

    if-eqz v40, :cond_10

    .line 722
    const-string/jumbo v40, "\u0008"

    const-string/jumbo v41, ""

    move-object/from16 v0, v37

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    .line 724
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v11}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->drawOfficeDrawing(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    goto :goto_8

    .line 726
    :cond_10
    const-string/jumbo v40, ""

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v40

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->readCharRun(Lorg/apache/poi/hwpf/usermodel/CharacterRun;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v31

    .line 728
    .restart local v31    # "runList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Run;>;"
    if-eqz v31, :cond_11

    .line 729
    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .restart local v16    # "i$":Ljava/util/Iterator;
    :goto_9
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v40

    if-eqz v40, :cond_11

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/samsung/thumbnail/customview/word/Run;

    .line 730
    .restart local v30    # "run":Lcom/samsung/thumbnail/customview/word/Run;
    move-object/from16 v0, v30

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto :goto_9

    .line 733
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v30    # "run":Lcom/samsung/thumbnail/customview/word/Run;
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->picTable:Lorg/apache/poi/hwpf/model/PicturesTable;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    invoke-virtual {v0, v9}, Lorg/apache/poi/hwpf/model/PicturesTable;->hasHorizontalLine(Lorg/apache/poi/hwpf/usermodel/CharacterRun;)Z

    move-result v40

    if-eqz v40, :cond_8

    .line 734
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->picTable:Lorg/apache/poi/hwpf/model/PicturesTable;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    move-object/from16 v0, v40

    move/from16 v1, v41

    invoke-virtual {v0, v9, v1}, Lorg/apache/poi/hwpf/model/PicturesTable;->extractPicture(Lorg/apache/poi/hwpf/usermodel/CharacterRun;Z)Lorg/apache/poi/hwpf/usermodel/Picture;

    move-result-object v28

    .line 736
    if-eqz v28, :cond_8

    .line 737
    const/16 v40, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    move/from16 v2, v18

    move/from16 v3, v40

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->drawHorizontalLine(Lorg/apache/poi/hwpf/usermodel/Picture;II)Lcom/samsung/thumbnail/customview/word/ShapesController;

    move-result-object v13

    .line 739
    .local v13, "hrLineShape":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual {v11, v13}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_5

    .line 750
    .end local v13    # "hrLineShape":Lcom/samsung/thumbnail/customview/word/ShapesController;
    .end local v17    # "isHref":Z
    .end local v22    # "newCharRun":Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .end local v25    # "p":I
    .end local v31    # "runList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Run;>;"
    .end local v37    # "text":Ljava/lang/String;
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->picTable:Lorg/apache/poi/hwpf/model/PicturesTable;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    invoke-virtual {v0, v9}, Lorg/apache/poi/hwpf/model/PicturesTable;->hasPicture(Lorg/apache/poi/hwpf/usermodel/CharacterRun;)Z

    move-result v40

    if-eqz v40, :cond_13

    .line 752
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->picTable:Lorg/apache/poi/hwpf/model/PicturesTable;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    move-object/from16 v0, v40

    move/from16 v1, v41

    invoke-virtual {v0, v9, v1}, Lorg/apache/poi/hwpf/model/PicturesTable;->extractPicture(Lorg/apache/poi/hwpf/usermodel/CharacterRun;Z)Lorg/apache/poi/hwpf/usermodel/Picture;

    move-result-object v28

    .line 753
    if-eqz v28, :cond_c

    .line 754
    const/16 v40, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    move/from16 v2, v18

    move/from16 v3, v40

    move/from16 v4, v27

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->readPicture(Lorg/apache/poi/hwpf/usermodel/Picture;III)Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    move-result-object v6

    .line 757
    .restart local v6    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    if-eqz v6, :cond_c

    .line 758
    sget-object v40, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOC:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-object/from16 v0, v40

    invoke-virtual {v6, v0}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 761
    invoke-virtual {v11, v6}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_6

    .line 766
    .end local v6    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    :cond_13
    :goto_a
    const-string/jumbo v40, "\u0008"

    move-object/from16 v0, v40

    invoke-virtual {v10, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v40

    if-eqz v40, :cond_14

    .line 767
    const-string/jumbo v40, "\u0008"

    const-string/jumbo v41, ""

    move-object/from16 v0, v40

    move-object/from16 v1, v41

    invoke-virtual {v10, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 768
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v11}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->drawOfficeDrawing(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    goto :goto_a

    .line 771
    :cond_14
    move-object/from16 v0, p0

    invoke-direct {v0, v9, v8}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->readCharRun(Lorg/apache/poi/hwpf/usermodel/CharacterRun;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v31

    .line 774
    .restart local v31    # "runList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Run;>;"
    if-eqz v31, :cond_15

    .line 775
    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .restart local v16    # "i$":Ljava/util/Iterator;
    :goto_b
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v40

    if-eqz v40, :cond_15

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/samsung/thumbnail/customview/word/Run;

    .line 776
    .restart local v30    # "run":Lcom/samsung/thumbnail/customview/word/Run;
    move-object/from16 v0, v30

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto :goto_b

    .line 780
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v30    # "run":Lcom/samsung/thumbnail/customview/word/Run;
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->picTable:Lorg/apache/poi/hwpf/model/PicturesTable;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    invoke-virtual {v0, v9}, Lorg/apache/poi/hwpf/model/PicturesTable;->hasHorizontalLine(Lorg/apache/poi/hwpf/usermodel/CharacterRun;)Z

    move-result v40

    if-eqz v40, :cond_c

    .line 781
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->picTable:Lorg/apache/poi/hwpf/model/PicturesTable;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    move-object/from16 v0, v40

    move/from16 v1, v41

    invoke-virtual {v0, v9, v1}, Lorg/apache/poi/hwpf/model/PicturesTable;->extractPicture(Lorg/apache/poi/hwpf/usermodel/CharacterRun;Z)Lorg/apache/poi/hwpf/usermodel/Picture;

    move-result-object v28

    .line 782
    if-eqz v28, :cond_c

    .line 783
    const/16 v40, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    move/from16 v2, v18

    move/from16 v3, v40

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->drawHorizontalLine(Lorg/apache/poi/hwpf/usermodel/Picture;II)Lcom/samsung/thumbnail/customview/word/ShapesController;

    move-result-object v13

    .line 785
    .restart local v13    # "hrLineShape":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual {v11, v13}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_6
.end method

.method private readPicture(Lorg/apache/poi/hwpf/usermodel/Picture;III)Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .locals 24
    .param p1, "picture"    # Lorg/apache/poi/hwpf/usermodel/Picture;
    .param p2, "leftIndentation"    # I
    .param p3, "topIndentation"    # I
    .param p4, "paraJustification"    # I

    .prologue
    .line 1366
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Picture;->suggestFullFileName()Ljava/lang/String;

    move-result-object v5

    .line 1367
    .local v5, "imageName":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Picture;->suggestPictureType()Lorg/apache/poi/hwpf/usermodel/PictureType;

    .line 1369
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Picture;->suggestFullFileName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1370
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".bmp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1374
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Picture;->getHorizontalScalingFactor()I

    move-result v13

    .line 1375
    .local v13, "aspectRatioX":I
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Picture;->getVerticalScalingFactor()I

    move-result v14

    .line 1380
    .local v14, "aspectRatioY":I
    if-lez v13, :cond_2

    .line 1381
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDxaGoal()I

    move-result v2

    mul-int/2addr v2, v13

    div-int/lit16 v2, v2, 0x3e8

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v20

    .line 1387
    .local v20, "imageWidth":F
    :goto_0
    if-lez v14, :cond_3

    .line 1388
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDyaGoal()I

    move-result v2

    mul-int/2addr v2, v14

    div-int/lit16 v2, v2, 0x3e8

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v19

    .line 1394
    .local v19, "imageHeight":F
    :goto_1
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->WMF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Picture;->suggestPictureType()Lorg/apache/poi/hwpf/usermodel/PictureType;

    move-result-object v3

    if-ne v2, v3, :cond_5

    .line 1396
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Picture;->getBlipRecordList()Ljava/util/List;

    move-result-object v18

    .line 1399
    .local v18, "escherRecordList":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/poi/ddf/EscherRecord;>;"
    const/4 v15, 0x0

    .line 1400
    .local v15, "escherBSERecord":Lorg/apache/poi/ddf/EscherBSERecord;
    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lorg/apache/poi/ddf/EscherBSERecord;

    if-eqz v2, :cond_4

    .line 1402
    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    .end local v15    # "escherBSERecord":Lorg/apache/poi/ddf/EscherBSERecord;
    check-cast v15, Lorg/apache/poi/ddf/EscherBSERecord;

    .line 1406
    .restart local v15    # "escherBSERecord":Lorg/apache/poi/ddf/EscherBSERecord;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Picture;->getRawContent()[B

    move-result-object v3

    invoke-virtual {v15}, Lorg/apache/poi/ddf/EscherBSERecord;->getBlipRecord()Lorg/apache/poi/ddf/EscherBlipRecord;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/ddf/EscherMetafileBlip;

    move/from16 v0, v20

    float-to-int v6, v0

    move/from16 v0, v19

    float-to-int v7, v0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->readWMFPictures([BLorg/apache/poi/ddf/EscherMetafileBlip;Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 1419
    .end local v15    # "escherBSERecord":Lorg/apache/poi/ddf/EscherBSERecord;
    .end local v18    # "escherRecordList":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/poi/ddf/EscherRecord;>;"
    .local v7, "bitmap":Landroid/graphics/Bitmap;
    :goto_2
    const/16 v21, 0x0

    .line 1420
    .local v21, "rotation":I
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Picture;->getPICFAndOfficeArtData()Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;->getShape()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v16

    .line 1422
    .local v16, "escherCR":Lorg/apache/poi/ddf/EscherContainerRecord;
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 1423
    .local v17, "escherOptRcdList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    if-eqz v16, :cond_1

    .line 1424
    const/16 v2, -0xff5

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v2, v1}, Lorg/apache/poi/ddf/EscherContainerRecord;->getRecordsById(SLjava/util/List;)V

    .line 1426
    const/4 v2, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherOptRecord;

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/samsung/thumbnail/util/Utils;->getIntEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;S)I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->rotationAngleToDegree_97Format(I)I

    move-result v21

    .line 1432
    :cond_1
    new-instance v6, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    sget v2, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    add-int v8, v2, p2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move/from16 v0, v20

    float-to-double v10, v0

    invoke-virtual {v2, v10, v11}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocWidth(D)D

    move-result-wide v2

    double-to-int v10, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v22, v0

    move-wide/from16 v0, v22

    invoke-virtual {v2, v0, v1}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocHeight(D)D

    move-result-wide v2

    double-to-int v11, v2

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->paraNumber:I

    move/from16 v9, p3

    invoke-direct/range {v6 .. v12}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;-><init>(Landroid/graphics/Bitmap;IIIII)V

    .line 1436
    .local v6, "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->getParaAlignment(I)Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-result-object v2

    invoke-virtual {v6, v2}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    .line 1437
    move/from16 v0, v21

    invoke-virtual {v6, v0}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setRotationAngle(I)V

    .line 1439
    .end local v6    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .end local v7    # "bitmap":Landroid/graphics/Bitmap;
    .end local v16    # "escherCR":Lorg/apache/poi/ddf/EscherContainerRecord;
    .end local v17    # "escherOptRcdList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    .end local v21    # "rotation":I
    :goto_3
    return-object v6

    .line 1384
    .end local v19    # "imageHeight":F
    .end local v20    # "imageWidth":F
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDxaGoal()I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v20

    .restart local v20    # "imageWidth":F
    goto/16 :goto_0

    .line 1391
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDyaGoal()I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v19

    .restart local v19    # "imageHeight":F
    goto/16 :goto_1

    .line 1404
    .restart local v15    # "escherBSERecord":Lorg/apache/poi/ddf/EscherBSERecord;
    .restart local v18    # "escherRecordList":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_4
    const/4 v6, 0x0

    goto :goto_3

    .line 1410
    .end local v15    # "escherBSERecord":Lorg/apache/poi/ddf/EscherBSERecord;
    .end local v18    # "escherRecordList":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_5
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->EMF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Picture;->suggestPictureType()Lorg/apache/poi/hwpf/usermodel/PictureType;

    move-result-object v3

    if-ne v2, v3, :cond_6

    .line 1411
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Picture;->getRawContent()[B

    move-result-object v2

    move/from16 v0, v20

    float-to-int v3, v0

    move/from16 v0, v19

    float-to-int v4, v0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v5, v3, v4}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->readEMFPictures([BLjava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v7

    .restart local v7    # "bitmap":Landroid/graphics/Bitmap;
    goto/16 :goto_2

    .line 1414
    .end local v7    # "bitmap":Landroid/graphics/Bitmap;
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Picture;->getRawContent()[B

    move-result-object v2

    move/from16 v0, v20

    float-to-int v3, v0

    move/from16 v0, v19

    float-to-int v4, v0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v5, v3, v4}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->readOtherPictures([BLjava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v7

    .restart local v7    # "bitmap":Landroid/graphics/Bitmap;
    goto/16 :goto_2
.end method

.method private readPicture([BLorg/apache/poi/ddf/EscherRecord;Ljava/lang/String;IIII)Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .locals 7
    .param p1, "data"    # [B
    .param p2, "escherRecord"    # Lorg/apache/poi/ddf/EscherRecord;
    .param p3, "imageName"    # Ljava/lang/String;
    .param p4, "left"    # I
    .param p5, "top"    # I
    .param p6, "right"    # I
    .param p7, "bottom"    # I

    .prologue
    const/4 v0, 0x0

    .line 1445
    if-nez p1, :cond_1

    .line 1471
    :cond_0
    :goto_0
    return-object v0

    .line 1450
    :cond_1
    sub-int v4, p6, p4

    .line 1451
    .local v4, "width":I
    sub-int v5, p7, p5

    .line 1453
    .local v5, "height":I
    const/4 v1, 0x0

    .line 1454
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->WMF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    invoke-direct {p0, p2}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->suggestPictureType(Lorg/apache/poi/ddf/EscherRecord;)Lorg/apache/poi/hwpf/usermodel/PictureType;

    move-result-object v3

    if-ne v2, v3, :cond_2

    .line 1456
    if-eqz p2, :cond_0

    move-object v2, p2

    .line 1461
    check-cast v2, Lorg/apache/poi/ddf/EscherMetafileBlip;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->readWMFPictures([BLorg/apache/poi/ddf/EscherMetafileBlip;Ljava/lang/String;II)Landroid/graphics/Bitmap;

    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    move-result-object v1

    .line 1469
    .restart local v1    # "bitmap":Landroid/graphics/Bitmap;
    :goto_1
    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    iget v6, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->paraNumber:I

    move v2, p4

    move v3, p5

    invoke-direct/range {v0 .. v6}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;-><init>(Landroid/graphics/Bitmap;IIIII)V

    .line 1471
    .local v0, "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    goto :goto_0

    .line 1463
    .end local v0    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    :cond_2
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->EMF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    invoke-direct {p0, p2}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->suggestPictureType(Lorg/apache/poi/ddf/EscherRecord;)Lorg/apache/poi/hwpf/usermodel/PictureType;

    move-result-object v3

    if-ne v2, v3, :cond_3

    .line 1464
    invoke-direct {p0, p1, p3, v4, v5}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->readEMFPictures([BLjava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_1

    .line 1466
    :cond_3
    invoke-direct {p0, p1, p3, v4, v5}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->readOtherPictures([BLjava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_1
.end method

.method private readShape(Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;)Ljava/util/ArrayList;
    .locals 58
    .param p1, "ofcDrawing"    # Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/word/ShapesController;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1644
    new-instance v53, Ljava/util/ArrayList;

    invoke-direct/range {v53 .. v53}, Ljava/util/ArrayList;-><init>()V

    .line 1646
    .local v53, "shapeControllerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/ShapesController;>;"
    invoke-interface/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;->getOfficeArtSpContainerList()Ljava/util/List;

    move-result-object v44

    check-cast v44, Ljava/util/ArrayList;

    .line 1652
    .local v44, "escherContainerRecordList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/ddf/EscherRecord;>;"
    new-instance v48, Lorg/apache/poi/hwpf/model/FSPA;

    invoke-direct/range {v48 .. v48}, Lorg/apache/poi/hwpf/model/FSPA;-><init>()V

    .line 1653
    .local v48, "fspaObj":Lorg/apache/poi/hwpf/model/FSPA;
    invoke-interface/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;->getRectangleLeft()I

    move-result v6

    move-object/from16 v0, v48

    invoke-virtual {v0, v6}, Lorg/apache/poi/hwpf/model/FSPA;->setXaLeft(I)V

    .line 1654
    invoke-interface/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;->getRectangleRight()I

    move-result v6

    move-object/from16 v0, v48

    invoke-virtual {v0, v6}, Lorg/apache/poi/hwpf/model/FSPA;->setXaRight(I)V

    .line 1655
    invoke-interface/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;->getRectangleTop()I

    move-result v6

    move-object/from16 v0, v48

    invoke-virtual {v0, v6}, Lorg/apache/poi/hwpf/model/FSPA;->setYaTop(I)V

    .line 1656
    invoke-interface/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;->getRectangleBottom()I

    move-result v6

    move-object/from16 v0, v48

    invoke-virtual {v0, v6}, Lorg/apache/poi/hwpf/model/FSPA;->setYaBottom(I)V

    .line 1657
    invoke-interface/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;->getShapeId()I

    move-result v6

    move-object/from16 v0, v48

    invoke-virtual {v0, v6}, Lorg/apache/poi/hwpf/model/FSPA;->setSpid(I)V

    .line 1659
    const/16 v54, 0x0

    .line 1660
    .local v54, "shapeName":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1662
    .local v4, "shapeCategory":Ljava/lang/String;
    const/16 v57, 0x0

    .line 1664
    .local v57, "zOrder":I
    if-eqz v44, :cond_7

    .line 1666
    invoke-virtual/range {v44 .. v44}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v50

    .local v50, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface/range {v50 .. v50}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-interface/range {v50 .. v50}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v46

    check-cast v46, Lorg/apache/poi/ddf/EscherRecord;

    .line 1667
    .local v46, "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    const/4 v3, 0x0

    .line 1669
    .local v3, "spCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    new-instance v45, Ljava/util/ArrayList;

    invoke-direct/range {v45 .. v45}, Ljava/util/ArrayList;-><init>()V

    .line 1670
    .local v45, "escherOptRcdList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    new-instance v47, Ljava/util/ArrayList;

    invoke-direct/range {v47 .. v47}, Ljava/util/ArrayList;-><init>()V

    .line 1671
    .local v47, "escherSpRcdList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    new-instance v40, Ljava/util/ArrayList;

    invoke-direct/range {v40 .. v40}, Ljava/util/ArrayList;-><init>()V

    .line 1672
    .local v40, "escherChildAnchorRcdList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    new-instance v42, Ljava/util/ArrayList;

    invoke-direct/range {v42 .. v42}, Ljava/util/ArrayList;-><init>()V

    .local v42, "escherClientDataRcdList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    move-object/from16 v43, v46

    .line 1674
    check-cast v43, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 1675
    .local v43, "escherContainerRecord":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v6, -0xff5

    move-object/from16 v0, v43

    move-object/from16 v1, v45

    invoke-virtual {v0, v6, v1}, Lorg/apache/poi/ddf/EscherContainerRecord;->getRecordsById(SLjava/util/List;)V

    .line 1677
    const/16 v6, -0xff6

    move-object/from16 v0, v43

    move-object/from16 v1, v47

    invoke-virtual {v0, v6, v1}, Lorg/apache/poi/ddf/EscherContainerRecord;->getRecordsById(SLjava/util/List;)V

    .line 1679
    const/16 v6, -0xff1

    move-object/from16 v0, v43

    move-object/from16 v1, v40

    invoke-virtual {v0, v6, v1}, Lorg/apache/poi/ddf/EscherContainerRecord;->getRecordsById(SLjava/util/List;)V

    .line 1682
    const/16 v6, -0xfef

    move-object/from16 v0, v43

    move-object/from16 v1, v42

    invoke-virtual {v0, v6, v1}, Lorg/apache/poi/ddf/EscherContainerRecord;->getRecordsById(SLjava/util/List;)V

    .line 1686
    new-instance v5, Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    invoke-direct {v5}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;-><init>()V

    .line 1687
    .local v5, "shapeInfo":Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    move-object/from16 v0, v48

    invoke-virtual {v5, v0}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->setFSPA(Lorg/apache/poi/hwpf/model/FSPA;)V

    .line 1688
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->setHorizontalLine(Z)V

    .line 1690
    if-eqz v45, :cond_0

    invoke-interface/range {v45 .. v45}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_0

    .line 1691
    const/4 v6, 0x0

    move-object/from16 v0, v45

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/poi/ddf/EscherOptRecord;

    invoke-virtual {v5, v6}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->setEscherOptRecord(Lorg/apache/poi/ddf/EscherOptRecord;)V

    .line 1697
    if-eqz v47, :cond_0

    invoke-interface/range {v47 .. v47}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_0

    .line 1698
    const/4 v6, 0x0

    move-object/from16 v0, v47

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/poi/ddf/EscherSpRecord;

    invoke-virtual {v5, v6}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->setSpRecord(Lorg/apache/poi/ddf/EscherSpRecord;)V

    .line 1703
    const/4 v6, 0x0

    move-object/from16 v0, v45

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/poi/ddf/EscherOptRecord;

    const/4 v7, 0x4

    invoke-static {v6, v7}, Lcom/samsung/thumbnail/util/Utils;->getIntEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;S)I

    move-result v6

    invoke-static {v6}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->rotationAngleToDegree_97Format(I)I

    move-result v52

    .line 1712
    .local v52, "rotation":I
    if-eqz v40, :cond_5

    invoke-interface/range {v40 .. v40}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_5

    .line 1714
    const/4 v6, 0x0

    move-object/from16 v0, v40

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lorg/apache/poi/ddf/EscherChildAnchorRecord;

    .line 1717
    .local v41, "escherClientAnchorRecord":Lorg/apache/poi/ddf/EscherChildAnchorRecord;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual/range {v41 .. v41}, Lorg/apache/poi/ddf/EscherChildAnchorRecord;->getDy2()I

    move-result v7

    invoke-virtual/range {v41 .. v41}, Lorg/apache/poi/ddf/EscherChildAnchorRecord;->getDy1()I

    move-result v12

    sub-int/2addr v7, v12

    invoke-static {v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v7

    float-to-double v12, v7

    invoke-virtual {v6, v12, v13}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocHeight(D)D

    move-result-wide v6

    double-to-float v11, v6

    .line 1720
    .local v11, "height":F
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual/range {v41 .. v41}, Lorg/apache/poi/ddf/EscherChildAnchorRecord;->getDx2()I

    move-result v7

    invoke-virtual/range {v41 .. v41}, Lorg/apache/poi/ddf/EscherChildAnchorRecord;->getDx1()I

    move-result v12

    sub-int/2addr v7, v12

    invoke-static {v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v7

    float-to-double v12, v7

    invoke-virtual {v6, v12, v13}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocWidth(D)D

    move-result-wide v6

    double-to-float v10, v6

    .line 1723
    .local v10, "width":F
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual/range {v41 .. v41}, Lorg/apache/poi/ddf/EscherChildAnchorRecord;->getDy1()I

    move-result v7

    invoke-static {v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v7

    float-to-double v12, v7

    invoke-virtual {v6, v12, v13}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocHeight(D)D

    move-result-wide v6

    double-to-float v9, v6

    .line 1725
    .local v9, "topMargin":F
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual/range {v41 .. v41}, Lorg/apache/poi/ddf/EscherChildAnchorRecord;->getDx1()I

    move-result v7

    invoke-static {v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v7

    float-to-double v12, v7

    invoke-virtual {v6, v12, v13}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocWidth(D)D

    move-result-wide v6

    double-to-float v8, v6

    .line 1746
    .end local v41    # "escherClientAnchorRecord":Lorg/apache/poi/ddf/EscherChildAnchorRecord;
    .local v8, "leftMargin":F
    :goto_1
    sget-object v6, Lcom/samsung/thumbnail/office/word/WordDocCustomParser$1;->$SwitchMap$org$apache$poi$hwpf$usermodel$OfficeDrawing$HorizontalPositioning:[I

    invoke-interface/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;->getHorizontalPositioning()Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 1768
    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->LEFT:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    .line 1773
    .local v16, "horizontalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;
    :goto_2
    sget-object v6, Lcom/samsung/thumbnail/office/word/WordDocCustomParser$1;->$SwitchMap$org$apache$poi$hwpf$usermodel$OfficeDrawing$HorizontalRelativeElement:[I

    invoke-interface/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;->getHorizontalRelative()Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalRelativeElement;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalRelativeElement;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_1

    .line 1785
    sget-object v17, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    .line 1789
    .local v17, "horizontalRelativeElement":Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;
    :goto_3
    sget-object v6, Lcom/samsung/thumbnail/office/word/WordDocCustomParser$1;->$SwitchMap$org$apache$poi$hwpf$usermodel$OfficeDrawing$VerticalPositioning:[I

    invoke-interface/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;->getVerticalPositioning()Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_2

    .line 1812
    sget-object v18, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->TOP:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    .line 1817
    .local v18, "verticalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    :goto_4
    sget-object v6, Lcom/samsung/thumbnail/office/word/WordDocCustomParser$1;->$SwitchMap$org$apache$poi$hwpf$usermodel$OfficeDrawing$VerticalRelativeElement:[I

    invoke-interface/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;->getVerticalRelativeElement()Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalRelativeElement;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalRelativeElement;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_3

    .line 1829
    sget-object v19, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    .line 1835
    .local v19, "verticalRelativeElement":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;
    :goto_5
    const/4 v6, 0x0

    cmpg-float v6, v8, v6

    if-gez v6, :cond_1

    .line 1836
    sget v6, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    int-to-float v6, v6

    add-float/2addr v8, v6

    .line 1838
    :cond_1
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getShapeName()Ljava/lang/String;

    move-result-object v54

    .line 1840
    const/4 v6, 0x0

    cmpl-float v6, v8, v6

    if-nez v6, :cond_6

    const/4 v6, 0x0

    cmpl-float v6, v9, v6

    if-nez v6, :cond_6

    .line 1841
    const/16 v57, 0x0

    .line 1846
    :goto_6
    if-eqz v54, :cond_4

    .line 1847
    invoke-static/range {v54 .. v54}, Lcom/samsung/thumbnail/util/Utils;->getShapeCategory(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1850
    const-string/jumbo v55, ""

    .line 1852
    .local v55, "text":Ljava/lang/String;
    const/16 v6, -0xff5

    :try_start_0
    move-object/from16 v0, v43

    invoke-virtual {v0, v6}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v51

    check-cast v51, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 1854
    .local v51, "officeArtFOPT":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v6, 0xc0

    move-object/from16 v0, v51

    invoke-virtual {v0, v6}, Lorg/apache/poi/ddf/EscherOptRecord;->lookup(I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v49

    check-cast v49, Lorg/apache/poi/ddf/EscherComplexProperty;

    .line 1856
    .local v49, "gtextUNICODE":Lorg/apache/poi/ddf/EscherComplexProperty;
    if-eqz v49, :cond_2

    .line 1857
    new-instance v56, Ljava/lang/String;

    invoke-virtual/range {v49 .. v49}, Lorg/apache/poi/ddf/EscherComplexProperty;->getComplexData()[B

    move-result-object v6

    const-string/jumbo v7, "UTF-16LE"

    move-object/from16 v0, v56

    invoke-direct {v0, v6, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .end local v55    # "text":Ljava/lang/String;
    .local v56, "text":Ljava/lang/String;
    move-object/from16 v55, v56

    .line 1868
    .end local v49    # "gtextUNICODE":Lorg/apache/poi/ddf/EscherComplexProperty;
    .end local v51    # "officeArtFOPT":Lorg/apache/poi/ddf/EscherOptRecord;
    .end local v56    # "text":Ljava/lang/String;
    .restart local v55    # "text":Ljava/lang/String;
    :cond_2
    :goto_7
    invoke-virtual/range {v55 .. v55}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_3

    .line 1869
    const-string/jumbo v4, "Rect"

    .line 1870
    const/16 v57, -0x1

    .line 1871
    const-string/jumbo v6, "[^A-Za-z0-9]+"

    const-string/jumbo v7, ""

    move-object/from16 v0, v55

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->setShapeText(Ljava/lang/String;)V

    .line 1874
    :cond_3
    new-instance v3, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v3    # "spCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    const/4 v6, 0x1

    const/4 v7, 0x1

    move/from16 v0, v57

    int-to-long v12, v0

    move/from16 v0, v52

    int-to-long v14, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->folderPath:Ljava/io/File;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v21, v0

    invoke-direct/range {v3 .. v21}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/word/WordShapeInfo;IIFFFFJJLcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;Ljava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1879
    .restart local v3    # "spCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v6, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOC:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v3, v6}, Lcom/samsung/thumbnail/customview/word/ShapesController;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 1911
    .end local v55    # "text":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, v53

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1728
    .end local v8    # "leftMargin":F
    .end local v9    # "topMargin":F
    .end local v10    # "width":F
    .end local v11    # "height":F
    .end local v16    # "horizontalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;
    .end local v17    # "horizontalRelativeElement":Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;
    .end local v18    # "verticalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    .end local v19    # "verticalRelativeElement":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;
    :cond_5
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getHeight()F

    move-result v7

    float-to-int v7, v7

    mul-int/lit16 v7, v7, 0x240

    div-int/lit8 v7, v7, 0x48

    invoke-static {v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v7

    float-to-double v12, v7

    invoke-virtual {v6, v12, v13}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocHeight(D)D

    move-result-wide v6

    double-to-float v11, v6

    .line 1731
    .restart local v11    # "height":F
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getWidth()F

    move-result v7

    float-to-int v7, v7

    mul-int/lit16 v7, v7, 0x240

    div-int/lit8 v7, v7, 0x48

    invoke-static {v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v7

    float-to-double v12, v7

    invoke-virtual {v6, v12, v13}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocWidth(D)D

    move-result-wide v6

    double-to-float v10, v6

    .line 1734
    .restart local v10    # "width":F
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getTop()F

    move-result v7

    float-to-int v7, v7

    mul-int/lit16 v7, v7, 0x240

    div-int/lit8 v7, v7, 0x48

    invoke-static {v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v7

    float-to-double v12, v7

    invoke-virtual {v6, v12, v13}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocHeight(D)D

    move-result-wide v6

    double-to-float v9, v6

    .line 1736
    .restart local v9    # "topMargin":F
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getLeft()F

    move-result v7

    float-to-int v7, v7

    mul-int/lit16 v7, v7, 0x240

    div-int/lit8 v7, v7, 0x48

    invoke-static {v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v7

    float-to-double v12, v7

    invoke-virtual {v6, v12, v13}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocWidth(D)D

    move-result-wide v6

    double-to-float v8, v6

    .restart local v8    # "leftMargin":F
    goto/16 :goto_1

    .line 1749
    :pswitch_0
    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->CENTERED:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    .line 1750
    .restart local v16    # "horizontalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;
    goto/16 :goto_2

    .line 1752
    .end local v16    # "horizontalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;
    :pswitch_1
    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->RIGHT:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    .line 1753
    .restart local v16    # "horizontalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;
    goto/16 :goto_2

    .line 1756
    .end local v16    # "horizontalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;
    :pswitch_2
    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->ABSOLUTE:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    .line 1757
    .restart local v16    # "horizontalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;
    goto/16 :goto_2

    .line 1760
    .end local v16    # "horizontalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;
    :pswitch_3
    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->LEFT:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    .line 1761
    .restart local v16    # "horizontalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;
    goto/16 :goto_2

    .line 1763
    .end local v16    # "horizontalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;
    :pswitch_4
    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->RIGHT:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    .line 1764
    .restart local v16    # "horizontalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;
    goto/16 :goto_2

    .line 1775
    :pswitch_5
    sget-object v17, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->CHARACTER:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    .line 1776
    .restart local v17    # "horizontalRelativeElement":Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;
    goto/16 :goto_3

    .line 1778
    .end local v17    # "horizontalRelativeElement":Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;
    :pswitch_6
    sget-object v17, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    .line 1779
    .restart local v17    # "horizontalRelativeElement":Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;
    goto/16 :goto_3

    .line 1781
    .end local v17    # "horizontalRelativeElement":Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;
    :pswitch_7
    sget-object v17, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->PAGE:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    .line 1782
    .restart local v17    # "horizontalRelativeElement":Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;
    goto/16 :goto_3

    .line 1792
    :pswitch_8
    sget-object v18, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->CENTERED:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    .line 1793
    .restart local v18    # "verticalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    goto/16 :goto_4

    .line 1795
    .end local v18    # "verticalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    :pswitch_9
    sget-object v18, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->BOTTOM:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    .line 1796
    .restart local v18    # "verticalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    goto/16 :goto_4

    .line 1799
    .end local v18    # "verticalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    :pswitch_a
    sget-object v18, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->INSIDE:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    .line 1800
    .restart local v18    # "verticalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    goto/16 :goto_4

    .line 1802
    .end local v18    # "verticalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    :pswitch_b
    sget-object v18, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->OUTSIDE:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    .line 1803
    .restart local v18    # "verticalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    goto/16 :goto_4

    .line 1805
    .end local v18    # "verticalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    :pswitch_c
    sget-object v18, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->TOP:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    .line 1806
    .restart local v18    # "verticalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    goto/16 :goto_4

    .line 1809
    .end local v18    # "verticalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    :pswitch_d
    sget-object v18, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->ABSOLUTE:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    .line 1810
    .restart local v18    # "verticalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    goto/16 :goto_4

    .line 1819
    :pswitch_e
    sget-object v19, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->LINE:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    .line 1820
    .restart local v19    # "verticalRelativeElement":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;
    goto/16 :goto_5

    .line 1822
    .end local v19    # "verticalRelativeElement":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;
    :pswitch_f
    sget-object v19, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    .line 1823
    .restart local v19    # "verticalRelativeElement":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;
    goto/16 :goto_5

    .line 1825
    .end local v19    # "verticalRelativeElement":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;
    :pswitch_10
    sget-object v19, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->PAGE:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    .line 1826
    .restart local v19    # "verticalRelativeElement":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;
    goto/16 :goto_5

    .line 1843
    :cond_6
    const/16 v57, -0x1

    goto/16 :goto_6

    .line 1860
    .restart local v55    # "text":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 1861
    .local v2, "e":Ljava/io/UnsupportedEncodingException;
    const-string/jumbo v6, "WordDocCustomParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Exception: "

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 1862
    .end local v2    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v2

    .line 1865
    .local v2, "e":Ljava/lang/NullPointerException;
    const-string/jumbo v6, "WordDocCustomParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Exception: "

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 1915
    .end local v2    # "e":Ljava/lang/NullPointerException;
    .end local v3    # "spCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    .end local v5    # "shapeInfo":Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .end local v8    # "leftMargin":F
    .end local v9    # "topMargin":F
    .end local v10    # "width":F
    .end local v11    # "height":F
    .end local v16    # "horizontalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;
    .end local v17    # "horizontalRelativeElement":Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;
    .end local v18    # "verticalPosition":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    .end local v19    # "verticalRelativeElement":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;
    .end local v40    # "escherChildAnchorRcdList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    .end local v42    # "escherClientDataRcdList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    .end local v43    # "escherContainerRecord":Lorg/apache/poi/ddf/EscherContainerRecord;
    .end local v45    # "escherOptRcdList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    .end local v46    # "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    .end local v47    # "escherSpRcdList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    .end local v50    # "i$":Ljava/util/Iterator;
    .end local v52    # "rotation":I
    .end local v55    # "text":Ljava/lang/String;
    :cond_7
    new-instance v5, Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    invoke-direct {v5}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;-><init>()V

    .line 1916
    .restart local v5    # "shapeInfo":Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    move-object/from16 v0, v48

    invoke-virtual {v5, v0}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->setFSPA(Lorg/apache/poi/hwpf/model/FSPA;)V

    .line 1918
    const-string/jumbo v54, "Rectangle"

    .line 1920
    invoke-static/range {v54 .. v54}, Lcom/samsung/thumbnail/util/Utils;->getShapeCategory(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1922
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getHeight()F

    move-result v7

    float-to-int v7, v7

    mul-int/lit16 v7, v7, 0x240

    div-int/lit8 v7, v7, 0x48

    invoke-static {v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v7

    float-to-double v12, v7

    invoke-virtual {v6, v12, v13}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocHeight(D)D

    move-result-wide v6

    double-to-float v11, v6

    .line 1924
    .restart local v11    # "height":F
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getWidth()F

    move-result v7

    float-to-int v7, v7

    mul-int/lit16 v7, v7, 0x240

    div-int/lit8 v7, v7, 0x48

    invoke-static {v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v7

    float-to-double v12, v7

    invoke-virtual {v6, v12, v13}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocWidth(D)D

    move-result-wide v6

    double-to-float v10, v6

    .line 1926
    .restart local v10    # "width":F
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getTop()F

    move-result v7

    float-to-int v7, v7

    mul-int/lit16 v7, v7, 0x240

    div-int/lit8 v7, v7, 0x48

    invoke-static {v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v7

    float-to-double v12, v7

    invoke-virtual {v6, v12, v13}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocHeight(D)D

    move-result-wide v6

    double-to-float v9, v6

    .line 1928
    .restart local v9    # "topMargin":F
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getLeft()F

    move-result v7

    float-to-int v7, v7

    mul-int/lit16 v7, v7, 0x240

    div-int/lit8 v7, v7, 0x48

    invoke-static {v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v7

    float-to-double v12, v7

    invoke-virtual {v6, v12, v13}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocWidth(D)D

    move-result-wide v6

    double-to-float v8, v6

    .line 1936
    .restart local v8    # "leftMargin":F
    new-instance v3, Lcom/samsung/thumbnail/customview/word/ShapesController;

    const/16 v24, 0x1

    const/16 v25, 0x1

    move/from16 v0, v57

    int-to-long v0, v0

    move-wide/from16 v30, v0

    const-wide/16 v32, 0x0

    sget-object v34, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->ABSOLUTE:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    sget-object v35, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->LEFT_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    sget-object v36, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->ABSOLUTE:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    sget-object v37, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->TOP_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->folderPath:Ljava/io/File;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v39, v0

    move-object/from16 v21, v3

    move-object/from16 v22, v4

    move-object/from16 v23, v5

    move/from16 v26, v8

    move/from16 v27, v9

    move/from16 v28, v10

    move/from16 v29, v11

    invoke-direct/range {v21 .. v39}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/word/WordShapeInfo;IIFFFFJJLcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;Ljava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1943
    .restart local v3    # "spCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v6, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOC:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v3, v6}, Lcom/samsung/thumbnail/customview/word/ShapesController;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 1945
    move-object/from16 v0, v53

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1949
    .end local v3    # "spCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    .end local v5    # "shapeInfo":Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .end local v8    # "leftMargin":F
    .end local v9    # "topMargin":F
    .end local v10    # "width":F
    .end local v11    # "height":F
    :cond_8
    return-object v53

    .line 1746
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 1773
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 1789
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 1817
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method private readTables(Lorg/apache/poi/hwpf/usermodel/Table;ILcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)V
    .locals 40
    .param p1, "table"    # Lorg/apache/poi/hwpf/usermodel/Table;
    .param p2, "tableJustification"    # I
    .param p3, "paraType"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    .prologue
    .line 833
    const/16 v31, 0x0

    .line 834
    .local v31, "row":Lorg/apache/poi/hwpf/usermodel/TableRow;
    const/4 v11, 0x0

    .line 835
    .local v11, "cell":Lorg/apache/poi/hwpf/usermodel/TableCell;
    const/4 v6, 0x0

    .line 837
    .local v6, "align":I
    const/16 v32, 0x1

    .line 843
    .local v32, "rowSpan":I
    const/16 v37, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/Table;->getRow(I)Lorg/apache/poi/hwpf/usermodel/TableRow;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getRowJustification()I

    move-result v8

    .line 847
    .local v8, "alignRowKey":I
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->getRowAndCellAlignment(I)I

    move-result v6

    .line 849
    new-instance v26, Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    const/16 v37, 0x0

    move-object/from16 v0, v26

    move-object/from16 v1, v37

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;-><init>(Ljava/lang/String;)V

    .line 851
    .local v26, "mCurrentTable":Lcom/samsung/thumbnail/customview/tables/CanvasTable;
    const/4 v6, 0x0

    .line 852
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Table;->numRows()I

    move-result v28

    .line 854
    .local v28, "numOfRows":I
    const/16 v16, 0x0

    .line 855
    .local v16, "curCell":I
    const/4 v13, 0x0

    .line 856
    .local v13, "cellNo":I
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 862
    .local v15, "cellWidthLst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Ljava/lang/String;>;"
    const/16 v25, 0x0

    .local v25, "m":I
    :goto_0
    move/from16 v0, v25

    move/from16 v1, v28

    if-ge v0, v1, :cond_4

    .line 863
    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/Table;->getRow(I)Lorg/apache/poi/hwpf/usermodel/TableRow;

    move-result-object v31

    .line 864
    invoke-virtual/range {v31 .. v31}, Lorg/apache/poi/hwpf/usermodel/TableRow;->numCells()I

    move-result v16

    .line 866
    if-eqz v25, :cond_0

    move/from16 v0, v16

    if-ge v13, v0, :cond_1

    .line 867
    :cond_0
    move/from16 v13, v16

    .line 870
    :cond_1
    invoke-virtual/range {v31 .. v31}, Lorg/apache/poi/hwpf/usermodel/TableRow;->numCells()I

    move-result v37

    move/from16 v0, v37

    new-array v14, v0, [Ljava/lang/String;

    .line 872
    .local v14, "cellWidth":[Ljava/lang/String;
    const/16 v22, 0x0

    .local v22, "j":I
    :goto_1
    invoke-virtual/range {v31 .. v31}, Lorg/apache/poi/hwpf/usermodel/TableRow;->numCells()I

    move-result v37

    move/from16 v0, v22

    move/from16 v1, v37

    if-ge v0, v1, :cond_3

    .line 873
    move-object/from16 v0, v31

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getCell(I)Lorg/apache/poi/hwpf/usermodel/TableCell;

    move-result-object v11

    .line 876
    if-nez v25, :cond_2

    .line 877
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->totalCellWidth:F

    move/from16 v37, v0

    invoke-virtual {v11}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getWidth()I

    move-result v38

    move/from16 v0, v38

    int-to-float v0, v0

    move/from16 v38, v0

    add-float v37, v37, v38

    move/from16 v0, v37

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->totalCellWidth:F

    .line 878
    :cond_2
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getWidth()I

    move-result v37

    invoke-static/range {v37 .. v37}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v37

    aput-object v37, v14, v22

    .line 872
    add-int/lit8 v22, v22, 0x1

    goto :goto_1

    .line 887
    :cond_3
    invoke-virtual {v15, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 862
    add-int/lit8 v25, v25, 0x1

    goto :goto_0

    .line 890
    .end local v14    # "cellWidth":[Ljava/lang/String;
    .end local v22    # "j":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->totalCellWidth:F

    move/from16 v38, v0

    move/from16 v0, v38

    float-to-int v0, v0

    move/from16 v38, v0

    invoke-static/range {v38 .. v38}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v38

    move/from16 v0, v38

    float-to-double v0, v0

    move-wide/from16 v38, v0

    invoke-virtual/range {v37 .. v39}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v37

    move/from16 v0, v37

    int-to-double v0, v0

    move-wide/from16 v38, v0

    move-object/from16 v0, p0

    move/from16 v1, p2

    move-wide/from16 v2, v38

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->calculateTableXValue(ID)F

    move-result v35

    .line 894
    .local v35, "tableXValue":F
    move-object/from16 v0, v26

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->setTableX(F)V

    .line 896
    const/16 v23, 0x0

    .local v23, "k":I
    :goto_2
    move/from16 v0, v23

    move/from16 v1, v28

    if-ge v0, v1, :cond_a

    .line 897
    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/Table;->getRow(I)Lorg/apache/poi/hwpf/usermodel/TableRow;

    move-result-object v31

    .line 903
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->createNewRow()V

    .line 904
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getCurrentRow()Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-result-object v17

    .line 906
    .local v17, "currentRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    invoke-virtual/range {v31 .. v31}, Lorg/apache/poi/hwpf/usermodel/TableRow;->numCells()I

    move-result v27

    .line 908
    .local v27, "numOfCells":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v37, v0

    invoke-virtual/range {v31 .. v31}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getRowHeight()I

    move-result v38

    invoke-static/range {v38 .. v38}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v38

    move/from16 v0, v38

    float-to-double v0, v0

    move-wide/from16 v38, v0

    invoke-virtual/range {v37 .. v39}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v32

    .line 910
    new-array v0, v13, [Ljava/lang/String;

    move-object/from16 v21, v0

    .line 912
    .local v21, "gg":[Ljava/lang/String;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 914
    .local v12, "cellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    const/16 v22, 0x0

    .restart local v22    # "j":I
    :goto_3
    move/from16 v0, v22

    move/from16 v1, v27

    if-ge v0, v1, :cond_9

    .line 917
    move-object/from16 v0, v31

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getCell(I)Lorg/apache/poi/hwpf/usermodel/TableCell;

    move-result-object v11

    .line 919
    new-instance v10, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-direct {v10}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;-><init>()V

    .line 924
    .local v10, "canvasTableCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    const-string/jumbo v37, "top"

    move-object/from16 v0, v37

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setVerticalAlignment(Ljava/lang/String;)V

    .line 930
    move/from16 v0, v27

    if-ge v0, v13, :cond_6

    .line 931
    if-nez v22, :cond_5

    .line 933
    move/from16 v18, v23

    .line 934
    .local v18, "curruntRow":I
    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v28

    invoke-direct {v0, v1, v2, v15, v13}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->findRowForColspan(IILjava/util/ArrayList;I)[Ljava/lang/String;

    move-result-object v21

    .line 938
    .end local v18    # "curruntRow":I
    :cond_5
    aget-object v37, v21, v22

    invoke-virtual {v11}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getWidth()I

    move-result v38

    invoke-static/range {v38 .. v38}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v38

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-nez v37, :cond_6

    .line 939
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getWidth()I

    move-result v37

    move-object/from16 v0, p0

    move/from16 v1, v37

    move-object/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v27

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->calColspan(I[Ljava/lang/String;II)I

    .line 948
    :cond_6
    const-string/jumbo v37, "normal"

    move-object/from16 v0, v37

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTextDirection(Ljava/lang/String;)V

    .line 956
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getDescriptor()Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->getShd()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v34

    .line 957
    .local v34, "shadeDesc":Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
    sget-object v20, Lorg/apache/poi/java/awt/Color;->WHITE:Lorg/apache/poi/java/awt/Color;

    .line 958
    .local v20, "fillStyle":Lorg/apache/poi/java/awt/Color;
    if-eqz v34, :cond_7

    .line 959
    invoke-virtual/range {v34 .. v34}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->getCvBack()Lorg/apache/poi/hwpf/model/Colorref;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Lorg/apache/poi/hwpf/model/Colorref;->getValue()I

    move-result v37

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->convertIco24ToARGB(I)Ljava/lang/String;

    move-result-object v33

    .line 961
    .local v33, "shadeColor":Ljava/lang/String;
    if-eqz v33, :cond_7

    .line 962
    invoke-static/range {v33 .. v33}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->stringToColor(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v20

    .line 965
    .end local v33    # "shadeColor":Ljava/lang/String;
    :cond_7
    const/16 v37, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v37

    invoke-virtual {v10, v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellFillStyle(Lorg/apache/poi/java/awt/Color;Z)V

    .line 968
    const/16 v37, 0x0

    move/from16 v0, v37

    invoke-virtual {v11, v0}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getParagraph(I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getJustification()I

    move-result v7

    .line 969
    .local v7, "alignCellKey":I
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->getRowAndCellAlignment(I)I

    move-result v6

    .line 987
    const/high16 v9, 0x3f800000    # 1.0f

    .line 989
    .local v9, "borderWidth":F
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getBorderType()I

    move-result v37

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->getTableBorderStyle(I)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v37

    invoke-virtual {v11}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getColor()S

    move-result v38

    move-object/from16 v0, p0

    move/from16 v1, v38

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->getTableBorderColor(I)Lorg/apache/poi/java/awt/Color;

    move-result-object v38

    move-object/from16 v0, v37

    move-object/from16 v1, v38

    invoke-virtual {v10, v0, v1, v9}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 994
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getBorderType()I

    move-result v37

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->getTableBorderStyle(I)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v37

    invoke-virtual {v11}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getColor()S

    move-result v38

    move-object/from16 v0, p0

    move/from16 v1, v38

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->getTableBorderColor(I)Lorg/apache/poi/java/awt/Color;

    move-result-object v38

    move-object/from16 v0, v37

    move-object/from16 v1, v38

    invoke-virtual {v10, v0, v1, v9}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 998
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getBorderType()I

    move-result v37

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->getTableBorderStyle(I)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v37

    invoke-virtual {v11}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getColor()S

    move-result v38

    move-object/from16 v0, p0

    move/from16 v1, v38

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->getTableBorderColor(I)Lorg/apache/poi/java/awt/Color;

    move-result-object v38

    move-object/from16 v0, v37

    move-object/from16 v1, v38

    invoke-virtual {v10, v0, v1, v9}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 1003
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getBorderType()I

    move-result v37

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->getTableBorderStyle(I)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v37

    invoke-virtual {v11}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getColor()S

    move-result v38

    move-object/from16 v0, p0

    move/from16 v1, v38

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->getTableBorderColor(I)Lorg/apache/poi/java/awt/Color;

    move-result-object v38

    move-object/from16 v0, v37

    move-object/from16 v1, v38

    invoke-virtual {v10, v0, v1, v9}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 1008
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v37, v0

    invoke-virtual {v11}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getWidth()I

    move-result v38

    invoke-static/range {v38 .. v38}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v38

    move/from16 v0, v38

    float-to-int v0, v0

    move/from16 v38, v0

    invoke-virtual/range {v37 .. v38}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v37

    move/from16 v0, v37

    int-to-float v0, v0

    move/from16 v36, v0

    .line 1010
    .local v36, "width":F
    move/from16 v0, v36

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellWidth(F)V

    .line 1045
    invoke-static {v6}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->getAlign(I)Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setHorizontalAlignment(Ljava/lang/String;)V

    .line 1047
    move/from16 v0, v32

    int-to-float v0, v0

    move/from16 v37, v0

    move-object/from16 v0, v17

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->setRowHeight(F)V

    .line 1067
    new-instance v30, Ljava/util/ArrayList;

    invoke-direct/range {v30 .. v30}, Ljava/util/ArrayList;-><init>()V

    .line 1069
    .local v30, "paraList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    const/16 v24, 0x0

    .local v24, "l":I
    :goto_4
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/usermodel/TableCell;->numParagraphs()I

    move-result v37

    move/from16 v0, v24

    move/from16 v1, v37

    if-ge v0, v1, :cond_8

    .line 1070
    move/from16 v0, v24

    invoke-virtual {v11, v0}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getParagraph(I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v29

    .line 1072
    .local v29, "para":Lorg/apache/poi/hwpf/usermodel/Paragraph;
    sget-object v37, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->NORMAL:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    move-object/from16 v2, v37

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->readPara(Lorg/apache/poi/hwpf/usermodel/Paragraph;Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    move-result-object v19

    .line 1074
    .local v19, "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    sget-object v37, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->NORMAL:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-object/from16 v0, v19

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setParaType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)V

    .line 1075
    move-object/from16 v0, v30

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1069
    add-int/lit8 v24, v24, 0x1

    goto :goto_4

    .line 1078
    .end local v19    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v29    # "para":Lorg/apache/poi/hwpf/usermodel/Paragraph;
    :cond_8
    move-object/from16 v0, v30

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->addDocParaList(Ljava/util/ArrayList;)V

    .line 1079
    invoke-virtual {v12, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 914
    add-int/lit8 v22, v22, 0x1

    goto/16 :goto_3

    .line 1082
    .end local v7    # "alignCellKey":I
    .end local v9    # "borderWidth":F
    .end local v10    # "canvasTableCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .end local v20    # "fillStyle":Lorg/apache/poi/java/awt/Color;
    .end local v24    # "l":I
    .end local v30    # "paraList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    .end local v34    # "shadeDesc":Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
    .end local v36    # "width":F
    :cond_9
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->setCellList(Ljava/util/ArrayList;)V

    .line 896
    add-int/lit8 v23, v23, 0x1

    goto/16 :goto_2

    .line 1116
    .end local v12    # "cellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    .end local v17    # "currentRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    .end local v21    # "gg":[Ljava/lang/String;
    .end local v22    # "j":I
    .end local v27    # "numOfCells":I
    :cond_a
    move-object/from16 v0, v26

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->setTableType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)V

    .line 1117
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v37

    move-object/from16 v0, v37

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/word/Page;->addElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 1120
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mContext:Landroid/content/Context;

    move-object/from16 v38, v0

    invoke-virtual/range {v37 .. v38}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->drawOnCanvas(Landroid/content/Context;)Z

    .line 1121
    return-void
.end method

.method private readWMFPictures([BLorg/apache/poi/ddf/EscherMetafileBlip;Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 21
    .param p1, "rawData"    # [B
    .param p2, "escherMetafileBlip"    # Lorg/apache/poi/ddf/EscherMetafileBlip;
    .param p3, "imageName"    # Ljava/lang/String;
    .param p4, "width"    # I
    .param p5, "height"    # I

    .prologue
    .line 1486
    const/4 v5, 0x0

    .line 1487
    .local v5, "left":I
    const/4 v6, 0x0

    .line 1488
    .local v6, "right":I
    const/4 v7, 0x0

    .line 1489
    .local v7, "top":I
    const/4 v8, 0x0

    .line 1491
    .local v8, "bottom":I
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/ddf/EscherMetafileBlip;->getLeft()I

    move-result v5

    .line 1492
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/ddf/EscherMetafileBlip;->getRight()I

    move-result v6

    .line 1493
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/ddf/EscherMetafileBlip;->getTop()I

    move-result v7

    .line 1494
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/ddf/EscherMetafileBlip;->getBottom()I

    move-result v8

    .line 1496
    const/16 v19, 0x0

    .line 1498
    .local v19, "wmfFos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v20, Ljava/io/FileOutputStream;

    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->folderPath:Ljava/io/File;

    move-object/from16 v0, p3

    invoke-direct {v4, v10, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-direct {v0, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1500
    .end local v19    # "wmfFos":Ljava/io/FileOutputStream;
    .local v20, "wmfFos":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v3, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;

    move-object/from16 v4, p1

    invoke-direct/range {v3 .. v8}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;-><init>([BIIII)V

    .line 1502
    .local v3, "readWMFFile":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
    move/from16 v0, p4

    int-to-float v4, v0

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->setActWidth(F)V

    .line 1503
    move/from16 v0, p5

    int-to-float v4, v0

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->setActHeight(F)V

    .line 1504
    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawImageDoc()Landroid/graphics/Bitmap;

    move-result-object v9

    .line 1506
    .local v9, "wmfBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->flipTheImage()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1507
    if-eqz v9, :cond_0

    .line 1508
    new-instance v14, Landroid/graphics/Matrix;

    invoke-direct {v14}, Landroid/graphics/Matrix;-><init>()V

    .line 1509
    .local v14, "matrix":Landroid/graphics/Matrix;
    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v10, -0x40800000    # -1.0f

    invoke-virtual {v14, v4, v10}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 1510
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    const/4 v15, 0x1

    invoke-static/range {v9 .. v15}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v17

    .line 1514
    .local v17, "flippedBitmap":Landroid/graphics/Bitmap;
    if-eqz v17, :cond_0

    .line 1515
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v10, 0x19

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v4, v10, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1525
    .end local v14    # "matrix":Landroid/graphics/Matrix;
    .end local v17    # "flippedBitmap":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    if-eqz v20, :cond_4

    .line 1527
    :try_start_2
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move-object/from16 v19, v20

    .line 1533
    .end local v3    # "readWMFFile":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
    .end local v9    # "wmfBitmap":Landroid/graphics/Bitmap;
    .end local v20    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v19    # "wmfFos":Ljava/io/FileOutputStream;
    :cond_1
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->folderPath:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v10, "/"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 1535
    .local v18, "path":Ljava/lang/String;
    move-object/from16 v0, v18

    move/from16 v1, p4

    move/from16 v2, p5

    invoke-static {v0, v1, v2}, Lcom/samsung/thumbnail/util/Utils;->decodeSampledBitmapFromFile(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v4

    return-object v4

    .line 1519
    .end local v18    # "path":Ljava/lang/String;
    .end local v19    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v3    # "readWMFFile":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
    .restart local v9    # "wmfBitmap":Landroid/graphics/Bitmap;
    .restart local v20    # "wmfFos":Ljava/io/FileOutputStream;
    :cond_2
    if-eqz v9, :cond_0

    .line 1520
    :try_start_3
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v10, 0x19

    move-object/from16 v0, v20

    invoke-virtual {v9, v4, v10, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 1522
    .end local v3    # "readWMFFile":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
    .end local v9    # "wmfBitmap":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v16

    move-object/from16 v19, v20

    .line 1523
    .end local v20    # "wmfFos":Ljava/io/FileOutputStream;
    .local v16, "e":Ljava/io/FileNotFoundException;
    .restart local v19    # "wmfFos":Ljava/io/FileOutputStream;
    :goto_2
    :try_start_4
    const-string/jumbo v4, "WordDocCustomParser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Exception: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v16 .. v16}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v4, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1525
    if-eqz v19, :cond_1

    .line 1527
    :try_start_5
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 1528
    :catch_1
    move-exception v16

    .line 1529
    .local v16, "e":Ljava/io/IOException;
    const-string/jumbo v4, "WordDocCustomParser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "IOException: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v4, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1528
    .end local v16    # "e":Ljava/io/IOException;
    .end local v19    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v3    # "readWMFFile":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
    .restart local v9    # "wmfBitmap":Landroid/graphics/Bitmap;
    .restart local v20    # "wmfFos":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v16

    .line 1529
    .restart local v16    # "e":Ljava/io/IOException;
    const-string/jumbo v4, "WordDocCustomParser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "IOException: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v4, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v19, v20

    .line 1530
    .end local v20    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v19    # "wmfFos":Ljava/io/FileOutputStream;
    goto/16 :goto_1

    .line 1525
    .end local v3    # "readWMFFile":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
    .end local v9    # "wmfBitmap":Landroid/graphics/Bitmap;
    .end local v16    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_3
    if-eqz v19, :cond_3

    .line 1527
    :try_start_6
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 1530
    :cond_3
    :goto_4
    throw v4

    .line 1528
    :catch_3
    move-exception v16

    .line 1529
    .restart local v16    # "e":Ljava/io/IOException;
    const-string/jumbo v10, "WordDocCustomParser"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "IOException: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 1525
    .end local v16    # "e":Ljava/io/IOException;
    .end local v19    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v20    # "wmfFos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v4

    move-object/from16 v19, v20

    .end local v20    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v19    # "wmfFos":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 1522
    :catch_4
    move-exception v16

    goto/16 :goto_2

    .end local v19    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v3    # "readWMFFile":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
    .restart local v9    # "wmfBitmap":Landroid/graphics/Bitmap;
    .restart local v20    # "wmfFos":Ljava/io/FileOutputStream;
    :cond_4
    move-object/from16 v19, v20

    .end local v20    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v19    # "wmfFos":Ljava/io/FileOutputStream;
    goto/16 :goto_1
.end method

.method private setCustomViewProp()V
    .locals 22

    .prologue
    .line 291
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    .line 293
    .local v8, "dm":Landroid/util/DisplayMetrics;
    iget v7, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 294
    .local v7, "deviceWidth":I
    iget v6, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 296
    .local v6, "deviceHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v18, v0

    int-to-double v0, v6

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setSlideHeight(D)V

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v18, v0

    int-to-double v0, v7

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setSlideWidth(D)V

    .line 304
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hwpf/HWPFDocument;->getSectionTable()Lorg/apache/poi/hwpf/model/SectionTable;

    move-result-object v14

    .line 306
    .local v14, "sectTable":Lorg/apache/poi/hwpf/model/SectionTable;
    invoke-virtual {v14}, Lorg/apache/poi/hwpf/model/SectionTable;->getSections()Ljava/util/ArrayList;

    move-result-object v16

    .line 311
    .local v16, "sectionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hwpf/model/SEPX;>;"
    new-instance v9, Lcom/samsung/thumbnail/office/word/Page;

    invoke-direct {v9}, Lcom/samsung/thumbnail/office/word/Page;-><init>()V

    .line 314
    .local v9, "firstPage":Lcom/samsung/thumbnail/office/word/Page;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ge v11, v0, :cond_1

    .line 315
    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/apache/poi/hwpf/model/SEPX;

    .line 316
    .local v15, "section":Lorg/apache/poi/hwpf/model/SEPX;
    invoke-virtual {v15}, Lorg/apache/poi/hwpf/model/SEPX;->getSectionProperties()Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    move-result-object v13

    .line 318
    .local v13, "sectProp":Lorg/apache/poi/hwpf/usermodel/SectionProperties;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v18, v0

    invoke-virtual {v13}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getXaPage()I

    move-result v19

    invoke-static/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setWordWidth(J)V

    .line 320
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v18, v0

    invoke-virtual {v13}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getYaPage()I

    move-result v19

    invoke-static/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setWordHeight(J)V

    .line 324
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getWordWidth()J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-float v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getWordHeight()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v19, v0

    div-float v12, v18, v19

    .line 330
    .local v12, "ratioWidthToHeight":F
    int-to-float v0, v7

    move/from16 v18, v0

    int-to-float v0, v6

    move/from16 v19, v0

    div-float v18, v18, v19

    cmpg-float v18, v18, v12

    if-gez v18, :cond_0

    .line 331
    move/from16 v17, v7

    .line 332
    .local v17, "width":I
    int-to-float v0, v7

    move/from16 v18, v0

    div-float v18, v18, v12

    move/from16 v0, v18

    float-to-int v10, v0

    .line 338
    .local v10, "height":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v18, v0

    move/from16 v0, v17

    int-to-double v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setCustomViewWidth(D)V

    .line 339
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v18, v0

    int-to-double v0, v10

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setCustomViewHeight(D)V

    .line 342
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v18, v0

    invoke-virtual {v13}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDyaHdrTop()I

    move-result v19

    invoke-static/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v19

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/office/word/Page;->setHeaderStartPoint(F)V

    .line 345
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v18, v0

    invoke-virtual {v13}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDyaHdrBottom()I

    move-result v19

    invoke-static/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v19

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/office/word/Page;->setFooterStartPoint(F)V

    .line 356
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v18, v0

    invoke-virtual {v13}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDxaLeft()I

    move-result v19

    invoke-static/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/office/word/Page;->setLeftMargin(F)V

    .line 359
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v18, v0

    invoke-virtual {v13}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDxaRight()I

    move-result v19

    invoke-static/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/office/word/Page;->setRightMargin(F)V

    .line 362
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v18, v0

    invoke-virtual {v13}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDyaTop()I

    move-result v19

    invoke-static/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-double v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/office/word/Page;->setTopMargin(F)V

    .line 365
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v18, v0

    invoke-virtual {v13}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDyaBottom()I

    move-result v19

    invoke-static/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-double v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/office/word/Page;->setBottomMargin(F)V

    .line 369
    sget-object v18, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v10, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 372
    .local v4, "bitmap":Landroid/graphics/Bitmap;
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 373
    .local v5, "canvas":Landroid/graphics/Canvas;
    const/16 v18, -0x1

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 375
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->setCanvas(Landroid/graphics/Canvas;)V

    .line 376
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->setCanvasBitmap(Landroid/graphics/Bitmap;)V

    .line 378
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->addPage(Lcom/samsung/thumbnail/office/word/Page;)V

    .line 314
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_0

    .line 334
    .end local v4    # "bitmap":Landroid/graphics/Bitmap;
    .end local v5    # "canvas":Landroid/graphics/Canvas;
    .end local v10    # "height":I
    .end local v17    # "width":I
    :cond_0
    const-wide v18, 0x3feccccccccccccdL    # 0.9

    int-to-double v0, v6

    move-wide/from16 v20, v0

    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-int v10, v0

    .line 335
    .restart local v10    # "height":I
    const-wide v18, 0x3feccccccccccccdL    # 0.9

    int-to-double v0, v6

    move-wide/from16 v20, v0

    mul-double v18, v18, v20

    float-to-double v0, v12

    move-wide/from16 v20, v0

    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v17, v0

    .restart local v17    # "width":I
    goto/16 :goto_1

    .line 380
    .end local v10    # "height":I
    .end local v12    # "ratioWidthToHeight":F
    .end local v13    # "sectProp":Lorg/apache/poi/hwpf/usermodel/SectionProperties;
    .end local v15    # "section":Lorg/apache/poi/hwpf/model/SEPX;
    .end local v17    # "width":I
    :cond_1
    return-void
.end method

.method private stripFields(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/16 v10, 0x15

    const/4 v9, 0x0

    const/16 v8, 0x13

    const/4 v7, -0x1

    .line 2420
    invoke-virtual {p1, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    if-ne v5, v7, :cond_2

    move-object v4, p1

    .line 2459
    .end local p1    # "text":Ljava/lang/String;
    .local v4, "text":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 2445
    .end local v4    # "text":Ljava/lang/String;
    .local v0, "first13":I
    .local v1, "first14":I
    .local v2, "last15":I
    .local v3, "next13":I
    .restart local p1    # "text":Ljava/lang/String;
    :cond_0
    if-eq v1, v7, :cond_5

    if-lt v1, v3, :cond_1

    if-ne v3, v7, :cond_5

    .line 2446
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v9, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {p1, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, v2, 0x1

    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 2425
    .end local v0    # "first13":I
    .end local v1    # "first14":I
    .end local v2    # "last15":I
    .end local v3    # "next13":I
    :cond_2
    :goto_1
    invoke-virtual {p1, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    if-le v5, v7, :cond_3

    invoke-virtual {p1, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    if-le v5, v7, :cond_3

    .line 2426
    invoke-virtual {p1, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 2427
    .restart local v0    # "first13":I
    add-int/lit8 v5, v0, 0x1

    invoke-virtual {p1, v8, v5}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    .line 2428
    .restart local v3    # "next13":I
    const/16 v5, 0x14

    add-int/lit8 v6, v0, 0x1

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2429
    .restart local v1    # "first14":I
    invoke-virtual {p1, v10}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 2432
    .restart local v2    # "last15":I
    if-ge v2, v0, :cond_4

    .end local v0    # "first13":I
    .end local v1    # "first14":I
    .end local v2    # "last15":I
    .end local v3    # "next13":I
    :cond_3
    :goto_2
    move-object v4, p1

    .line 2459
    .end local p1    # "text":Ljava/lang/String;
    .restart local v4    # "text":Ljava/lang/String;
    goto :goto_0

    .line 2437
    .end local v4    # "text":Ljava/lang/String;
    .restart local v0    # "first13":I
    .restart local v1    # "first14":I
    .restart local v2    # "last15":I
    .restart local v3    # "next13":I
    .restart local p1    # "text":Ljava/lang/String;
    :cond_4
    if-ne v3, v7, :cond_0

    if-ne v1, v7, :cond_0

    .line 2438
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v9, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, v2, 0x1

    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 2439
    goto :goto_2

    .line 2455
    :cond_5
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v9, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, v2, 0x1

    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 2456
    goto :goto_1
.end method

.method private suggestPictureType(Lorg/apache/poi/ddf/EscherRecord;)Lorg/apache/poi/hwpf/usermodel/PictureType;
    .locals 2
    .param p1, "escherRecord"    # Lorg/apache/poi/ddf/EscherRecord;

    .prologue
    .line 2361
    if-nez p1, :cond_0

    .line 2362
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->UNKNOWN:Lorg/apache/poi/hwpf/usermodel/PictureType;

    .line 2410
    :goto_0
    return-object v1

    .line 2365
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 2410
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->UNKNOWN:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    :sswitch_0
    move-object v0, p1

    .line 2367
    check-cast v0, Lorg/apache/poi/ddf/EscherBSERecord;

    .line 2368
    .local v0, "bseRecord":Lorg/apache/poi/ddf/EscherBSERecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherBSERecord;->getBlipTypeWin32()B

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 2390
    :pswitch_0
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->UNKNOWN:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 2370
    :pswitch_1
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->UNKNOWN:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 2372
    :pswitch_2
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->UNKNOWN:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 2374
    :pswitch_3
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->EMF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 2376
    :pswitch_4
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->WMF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 2378
    :pswitch_5
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->PICT:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 2380
    :pswitch_6
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->JPEG:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 2382
    :pswitch_7
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->PNG:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 2384
    :pswitch_8
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->BMP:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 2386
    :pswitch_9
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->TIFF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 2388
    :pswitch_a
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->JPEG:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 2394
    .end local v0    # "bseRecord":Lorg/apache/poi/ddf/EscherBSERecord;
    :sswitch_1
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->EMF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 2396
    :sswitch_2
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->WMF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 2398
    :sswitch_3
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->PICT:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 2400
    :sswitch_4
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->JPEG:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 2402
    :sswitch_5
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->PNG:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 2404
    :sswitch_6
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->BMP:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 2406
    :sswitch_7
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->TIFF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 2408
    :sswitch_8
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->JPEG:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 2365
    :sswitch_data_0
    .sparse-switch
        -0xff9 -> :sswitch_0
        -0xfe6 -> :sswitch_1
        -0xfe5 -> :sswitch_2
        -0xfe4 -> :sswitch_3
        -0xfe3 -> :sswitch_4
        -0xfe2 -> :sswitch_5
        -0xfe1 -> :sswitch_6
        -0xfd7 -> :sswitch_7
        -0xfd6 -> :sswitch_8
    .end sparse-switch

    .line 2368
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method


# virtual methods
.method public getCanvas()Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;
    .locals 1

    .prologue
    .line 1961
    iget-object v0, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    return-object v0
.end method

.method public getElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
    .locals 1

    .prologue
    .line 1957
    iget-object v0, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    return-object v0
.end method

.method public getFolderName()Ljava/io/File;
    .locals 1

    .prologue
    .line 1953
    iget-object v0, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->folderPath:Ljava/io/File;

    return-object v0
.end method

.method public readAllParagraphs()V
    .locals 8

    .prologue
    .line 480
    iget-object v6, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->range:Lorg/apache/poi/hwpf/usermodel/Range;

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/usermodel/Range;->numParagraphs()I

    move-result v3

    .line 481
    .local v3, "numPara":I
    const/16 v6, 0x12c

    if-le v3, v6, :cond_0

    .line 482
    const/16 v3, 0x12c

    .line 486
    :cond_0
    const/4 v2, 0x0

    .line 487
    .local v2, "inTable":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_1

    .line 489
    iget-object v6, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iget-boolean v6, v6, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    const/4 v7, 0x1

    if-ne v6, v7, :cond_2

    .line 524
    :cond_1
    return-void

    .line 492
    :cond_2
    iget v6, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->paraNumber:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->paraNumber:I

    .line 502
    iget-object v6, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->range:Lorg/apache/poi/hwpf/usermodel/Range;

    invoke-virtual {v6, v1}, Lorg/apache/poi/hwpf/usermodel/Range;->getParagraph(I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v4

    .line 504
    .local v4, "para":Lorg/apache/poi/hwpf/usermodel/Paragraph;
    invoke-virtual {v4}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->isInTable()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 506
    if-nez v2, :cond_3

    .line 508
    iget-object v6, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->range:Lorg/apache/poi/hwpf/usermodel/Range;

    invoke-virtual {v6, v4}, Lorg/apache/poi/hwpf/usermodel/Range;->getTable(Lorg/apache/poi/hwpf/usermodel/Paragraph;)Lorg/apache/poi/hwpf/usermodel/Table;

    move-result-object v5

    .line 509
    .local v5, "table":Lorg/apache/poi/hwpf/usermodel/Table;
    invoke-virtual {v4}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getJustification()I

    move-result v6

    sget-object v7, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->NORMAL:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    invoke-direct {p0, v5, v6, v7}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->readTables(Lorg/apache/poi/hwpf/usermodel/Table;ILcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)V

    .line 511
    const/4 v2, 0x1

    .line 487
    .end local v5    # "table":Lorg/apache/poi/hwpf/usermodel/Table;
    :cond_3
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 514
    :cond_4
    const/4 v2, 0x0

    .line 515
    sget-object v6, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->NORMAL:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    invoke-direct {p0, v4, v6}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->readPara(Lorg/apache/poi/hwpf/usermodel/Paragraph;Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    move-result-object v0

    .line 517
    .local v0, "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    sget-object v6, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->NORMAL:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setParaType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)V

    .line 518
    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setParaNumber(I)V

    .line 520
    iget-object v6, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/samsung/thumbnail/office/word/Page;->addElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 521
    iget-object v6, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    iget-object v7, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->drawOnCanvas(Landroid/content/Context;)Z

    goto :goto_1
.end method

.method public readDocument(Ljava/io/FileInputStream;)V
    .locals 5
    .param p1, "fileInputStream"    # Ljava/io/FileInputStream;

    .prologue
    .line 182
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->init(Ljava/io/FileInputStream;)V

    .line 184
    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    if-nez v2, :cond_1

    .line 233
    :cond_0
    :goto_0
    return-void

    .line 187
    :cond_1
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->setCustomViewProp()V

    .line 213
    const/4 v1, 0x1

    .line 216
    .local v1, "pageNumber":I
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->readHeaderAndFooter(I)V

    .line 222
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->readAllParagraphs()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    if-eqz v2, :cond_0

    .line 231
    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/HWPFDocument;->closeStreams()V

    goto :goto_0

    .line 223
    :catch_0
    move-exception v0

    .line 224
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "WordDocCustomParser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public readHeaderAndFooter(I)V
    .locals 5
    .param p1, "pageNumber"    # I

    .prologue
    .line 390
    :try_start_0
    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->plcfHdd:Lorg/apache/poi/hwpf/model/PlexOfCps;

    if-nez v2, :cond_1

    .line 419
    :cond_0
    :goto_0
    return-void

    .line 395
    :cond_1
    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->headerStoriesRange:Lorg/apache/poi/hwpf/usermodel/Range;

    if-eqz v2, :cond_0

    .line 398
    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->headerStories:Lorg/apache/poi/hwpf/usermodel/HeaderStories;

    invoke-virtual {v2, p1}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getHeader(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->headerStories:Lorg/apache/poi/hwpf/usermodel/HeaderStories;

    invoke-virtual {v2, p1}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getHeader(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_2

    .line 401
    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->headerStories:Lorg/apache/poi/hwpf/usermodel/HeaderStories;

    invoke-virtual {v2, p1}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getHeaderPageType(I)I

    move-result v1

    .line 402
    .local v1, "plcfHddIndex":I
    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->plcfHdd:Lorg/apache/poi/hwpf/model/PlexOfCps;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->headerStoriesRange:Lorg/apache/poi/hwpf/usermodel/Range;

    sget-object v4, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->HEADER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->readHeaderAndFooter(ILorg/apache/poi/hwpf/model/PlexOfCps;Lorg/apache/poi/hwpf/usermodel/Range;Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)V

    .line 406
    .end local v1    # "plcfHddIndex":I
    :cond_2
    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->headerStories:Lorg/apache/poi/hwpf/usermodel/HeaderStories;

    invoke-virtual {v2, p1}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getFooter(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->headerStories:Lorg/apache/poi/hwpf/usermodel/HeaderStories;

    invoke-virtual {v2, p1}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getFooter(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 410
    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->headerStories:Lorg/apache/poi/hwpf/usermodel/HeaderStories;

    invoke-virtual {v2, p1}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getFooterPageType(I)I

    move-result v1

    .line 411
    .restart local v1    # "plcfHddIndex":I
    iget-object v2, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->plcfHdd:Lorg/apache/poi/hwpf/model/PlexOfCps;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->headerStoriesRange:Lorg/apache/poi/hwpf/usermodel/Range;

    sget-object v4, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->FOOTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->readHeaderAndFooter(ILorg/apache/poi/hwpf/model/PlexOfCps;Lorg/apache/poi/hwpf/usermodel/Range;Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 416
    .end local v1    # "plcfHddIndex":I
    :catch_0
    move-exception v0

    .line 417
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "WordDocCustomParser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setEmailPreview(Z)V
    .locals 0
    .param p1, "isPreview"    # Z

    .prologue
    .line 1967
    return-void
.end method
