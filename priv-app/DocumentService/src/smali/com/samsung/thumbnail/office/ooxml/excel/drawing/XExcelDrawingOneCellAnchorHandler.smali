.class public Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingOneCellAnchorHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelDrawingOneCellAnchorHandler.java"


# instance fields
.field private oneCellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/OneCellAnchor;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 21
    const/16 v0, 0xc9

    const-string/jumbo v1, "oneCellAnchor"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 23
    return-void
.end method

.method private init()V
    .locals 8

    .prologue
    .line 26
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicFromRowColHandler;

    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingOneCellAnchorHandler;->oneCellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/OneCellAnchor;

    invoke-direct {v2, v6}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicFromRowColHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;)V

    .line 28
    .local v2, "fromhandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicFromRowColHandler;
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingOneCellAnchorHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v7, "from"

    invoke-virtual {v6, v7, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicExtHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicExtHandler;-><init>()V

    .line 33
    .local v1, "exthandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicExtHandler;
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingOneCellAnchorHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v7, "ext"

    invoke-virtual {v6, v7, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicHandler;

    invoke-direct {v4}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicHandler;-><init>()V

    .line 37
    .local v4, "pichandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicHandler;
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingOneCellAnchorHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v7, "pic"

    invoke-virtual {v6, v7, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingSPHandler;

    invoke-direct {v5}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingSPHandler;-><init>()V

    .line 39
    .local v5, "sphandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingSPHandler;
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingOneCellAnchorHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v7, "sp"

    invoke-virtual {v6, v7, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingCxnSPHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingCxnSPHandler;-><init>()V

    .line 45
    .local v0, "cxnsphandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingCxnSPHandler;
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingOneCellAnchorHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v7, "cxnSp"

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XSSFChartGraphicFrameHandler;

    invoke-direct {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XSSFChartGraphicFrameHandler;-><init>()V

    .line 51
    .local v3, "graphicFrame":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XSSFChartGraphicFrameHandler;
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingOneCellAnchorHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v7, "graphicFrame"

    invoke-virtual {v6, v7, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 57
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 59
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/OneCellAnchor;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/OneCellAnchor;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingOneCellAnchorHandler;->oneCellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/OneCellAnchor;

    .line 60
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingOneCellAnchorHandler;->oneCellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/OneCellAnchor;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->addCellAnchor(Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;)V

    .line 61
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingOneCellAnchorHandler;->init()V

    .line 62
    return-void
.end method
