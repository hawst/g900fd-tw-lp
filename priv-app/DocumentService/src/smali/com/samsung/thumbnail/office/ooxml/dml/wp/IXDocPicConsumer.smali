.class public interface abstract Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;
.super Ljava/lang/Object;
.source "IXDocPicConsumer.java"


# virtual methods
.method public abstract addDataModelId(Ljava/lang/String;)V
.end method

.method public abstract addDiagramId(Ljava/lang/String;)V
.end method

.method public abstract addGroupShapeAnchor(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract addGroupStyle(Ljava/lang/String;)V
.end method

.method public abstract addShape(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;J)V
.end method

.method public abstract endGroupStyle()V
.end method

.method public abstract endPicture()V
.end method

.method public abstract endShape()V
.end method

.method public abstract setDocPr(ILjava/lang/String;)V
.end method

.method public abstract setDrSpeNme(Ljava/lang/String;)V
.end method

.method public abstract setEffectExtent(IIII)V
.end method

.method public abstract setImageRelId(Ljava/lang/String;)V
.end method

.method public abstract setPictAnchorProp(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract setPictPosOffset(Ljava/lang/String;J)V
.end method

.method public abstract setPictureExtent(II)V
.end method

.method public abstract setPictureType(Ljava/lang/String;)V
.end method

.method public abstract setRotation(I)V
.end method

.method public abstract setWrapSquareProp(Ljava/lang/String;)V
.end method

.method public abstract startPicture()V
.end method
