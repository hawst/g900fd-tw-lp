.class public Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DMLChartPtVHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;
    }
.end annotation


# instance fields
.field private ptConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;)V
    .locals 2
    .param p1, "ptConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;

    .prologue
    .line 22
    const/16 v0, 0x23

    const-string/jumbo v1, "v"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 23
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler;->ptConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;

    .line 24
    return-void
.end method


# virtual methods
.method public charactersHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;[CII)V
    .locals 4
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "content"    # [C
    .param p3, "start"    # I
    .param p4, "end"    # I

    .prologue
    .line 29
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p2, p3, p4}, Ljava/lang/String;-><init>([CII)V

    .line 32
    .local v1, "val":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    if-eqz v2, :cond_1

    .line 33
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler;->ptConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;

    invoke-interface {v2, p1, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;->setPtVal(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 60
    :cond_0
    :goto_0
    return-void

    .line 36
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .line 37
    .local v0, "chart":Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getExChartVAl()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "Num"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_2

    .line 39
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler;->ptConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;

    invoke-interface {v2, p1, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;->chartNumVal(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 41
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getExChartVAl()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "String"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_3

    .line 43
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler;->ptConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;

    invoke-interface {v2, p1, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;->chartStrVal(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 45
    :cond_3
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler;->ptConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;

    if-eqz v2, :cond_0

    .line 46
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler;->ptConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;

    invoke-interface {v2, p1, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;->setPtVal(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 65
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 67
    return-void
.end method
