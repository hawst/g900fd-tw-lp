.class public final enum Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;
.super Ljava/lang/Enum;
.source "MapMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

.field public static final enum MM_ANISOTROPIC:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

.field public static final enum MM_HIENGLISH:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

.field public static final enum MM_HIMETRIC:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

.field public static final enum MM_ISOTROPIC:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

.field public static final enum MM_LOENGLISH:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

.field public static final enum MM_LOMETRIC:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

.field public static final enum MM_TEXT:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

.field public static final enum MM_TWIPS:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;


# instance fields
.field private MapModeType:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 5
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    const-string/jumbo v1, "MM_TEXT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;->MM_TEXT:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    const-string/jumbo v1, "MM_LOMETRIC"

    invoke-direct {v0, v1, v4, v5}, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;->MM_LOMETRIC:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    const-string/jumbo v1, "MM_HIMETRIC"

    invoke-direct {v0, v1, v5, v6}, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;->MM_HIMETRIC:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    const-string/jumbo v1, "MM_LOENGLISH"

    invoke-direct {v0, v1, v6, v7}, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;->MM_LOENGLISH:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    const-string/jumbo v1, "MM_HIENGLISH"

    invoke-direct {v0, v1, v7, v8}, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;->MM_HIENGLISH:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    const-string/jumbo v1, "MM_TWIPS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;->MM_TWIPS:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    .line 6
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    const-string/jumbo v1, "MM_ISOTROPIC"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;->MM_ISOTROPIC:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    const-string/jumbo v1, "MM_ANISOTROPIC"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;->MM_ANISOTROPIC:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    .line 3
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    const/4 v1, 0x0

    sget-object v2, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;->MM_TEXT:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    aput-object v2, v0, v1

    sget-object v1, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;->MM_LOMETRIC:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;->MM_HIMETRIC:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;->MM_LOENGLISH:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;->MM_HIENGLISH:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;->MM_TWIPS:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;->MM_ISOTROPIC:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;->MM_ANISOTROPIC:Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;->$VALUES:[Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 11
    iput p3, p0, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;->MapModeType:I

    .line 12
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;->$VALUES:[Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;

    return-object v0
.end method


# virtual methods
.method public getMapModeType()I
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfEnums/MapMode;->MapModeType:I

    return v0
.end method
