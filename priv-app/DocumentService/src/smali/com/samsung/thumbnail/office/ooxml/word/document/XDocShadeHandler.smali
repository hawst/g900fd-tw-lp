.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocShadeHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;
.source "XDocShadeHandler.java"


# instance fields
.field private valueConsumer:Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V
    .locals 1
    .param p1, "valueConsumer"    # Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;

    .prologue
    .line 20
    const-string/jumbo v0, "shd"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;-><init>(Ljava/lang/String;)V

    .line 21
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocShadeHandler;->valueConsumer:Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;

    .line 22
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 27
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;-><init>()V

    .line 29
    .local v2, "shadingProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;
    const-string/jumbo v4, "val"

    invoke-virtual {p0, p3, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocShadeHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v3

    .line 30
    .local v3, "val":Ljava/lang/String;
    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->setPattern(Ljava/lang/String;)V

    .line 32
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 33
    .local v0, "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    const-string/jumbo v4, "color"

    invoke-virtual {p0, p3, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocShadeHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v3

    .line 34
    const-string/jumbo v4, "auto"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 35
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setAuto(Z)V

    .line 40
    :goto_0
    const-string/jumbo v4, "themeColor"

    invoke-virtual {p0, p3, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocShadeHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v3

    .line 41
    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setThemeColor(Ljava/lang/String;)V

    .line 42
    const-string/jumbo v4, "themeTint"

    invoke-virtual {p0, p3, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocShadeHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v3

    .line 43
    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setTint(Ljava/lang/String;)V

    .line 44
    const-string/jumbo v4, "themeShade"

    invoke-virtual {p0, p3, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocShadeHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v3

    .line 45
    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setShade(Ljava/lang/String;)V

    .line 46
    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->setColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    .line 48
    const-string/jumbo v4, "fill"

    invoke-virtual {p0, p3, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocShadeHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v3

    .line 49
    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->setFill(Ljava/lang/String;)V

    .line 51
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadeValue;

    invoke-direct {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadeValue;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;)V

    .line 52
    .local v1, "shadeVal":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadeValue;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocShadeHandler;->valueConsumer:Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;

    invoke-interface {v4, v1}, Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;->consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V

    .line 53
    return-void

    .line 37
    .end local v1    # "shadeVal":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadeValue;
    :cond_0
    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    goto :goto_0
.end method
