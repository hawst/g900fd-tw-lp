.class public Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingSPHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XExcelDrawingSPHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 26
    const/16 v0, 0xc9

    const-string/jumbo v1, "sp"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 28
    return-void
.end method

.method private init(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 11
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    const/16 v10, 0xc9

    .line 31
    const/4 v9, 0x4

    new-array v1, v9, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 33
    .local v1, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingNvSpPrHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingNvSpPrHandler;-><init>()V

    .line 34
    .local v0, "nvspprhandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingNvSpPrHandler;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XdrSeqDescriptor;

    const-string/jumbo v9, "nvSpPr"

    invoke-direct {v6, v9, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XdrSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 36
    .local v6, "xdr1Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XdrSeqDescriptor;
    const/4 v9, 0x0

    aput-object v6, v1, v9

    .line 38
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;

    invoke-direct {v2, v10, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 40
    .local v2, "spPrHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XdrSeqDescriptor;

    const-string/jumbo v9, "spPr"

    invoke-direct {v7, v9, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XdrSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 42
    .local v7, "xdr2Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XdrSeqDescriptor;
    const/4 v9, 0x1

    aput-object v7, v1, v9

    .line 44
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapeStyleHandler;

    invoke-direct {v3, v10, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapeStyleHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 45
    .local v3, "styleHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapeStyleHandler;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XdrSeqDescriptor;

    const-string/jumbo v9, "style"

    invoke-direct {v8, v9, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XdrSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 47
    .local v8, "xdr3Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XdrSeqDescriptor;
    const/4 v9, 0x2

    aput-object v8, v1, v9

    .line 49
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelTextBodyHandler;

    const/4 v9, 0x0

    invoke-direct {v4, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelTextBodyHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;)V

    .line 50
    .local v4, "txtBodyHandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelTextBodyHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XdrSeqDescriptor;

    const-string/jumbo v9, "txBody"

    invoke-direct {v5, v9, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XdrSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 52
    .local v5, "txtSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XdrSeqDescriptor;
    const/4 v9, 0x3

    aput-object v5, v1, v9

    .line 54
    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingSPHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 55
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 60
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 62
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getCurrentCellAnchor()Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    move-result-object v0

    .line 63
    .local v0, "cellAnchor":Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;->SHAPE:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->setType(Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;)V

    .line 65
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;-><init>()V

    .line 66
    .local v1, "shapeInfo":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->setShapePropery(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 68
    invoke-direct {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingSPHandler;->init(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 69
    return-void
.end method
