.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "GradientFillHandler.java"


# instance fields
.field private gradientFill:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    const-string/jumbo v0, "gradientFill"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method private init()V
    .locals 3

    .prologue
    .line 23
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillHandler;->gradientFill:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;

    .line 24
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillStopHandler;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillHandler;->gradientFill:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillStopHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;)V

    .line 26
    .local v0, "fillstophandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillStopHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "stop"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 74
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 76
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillHandler;->gradientFill:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addFill(Lcom/samsung/thumbnail/office/ooxml/excel/IFill;)V

    .line 77
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 10
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 32
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 33
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillHandler;->init()V

    .line 35
    const-string/jumbo v6, "degree"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 37
    .local v1, "gradientDegree":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 38
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillHandler;->gradientFill:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->setDegree(D)V

    .line 41
    :cond_0
    const-string/jumbo v6, "type"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    .line 44
    .local v2, "gradientType":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 45
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillHandler;->gradientFill:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;->PATH:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->setGradientType(Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;)V

    .line 48
    :cond_1
    const-string/jumbo v6, "left"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v3

    .line 50
    .local v3, "left":Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 51
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillHandler;->gradientFill:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;

    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->setLeft(D)V

    .line 54
    :cond_2
    const-string/jumbo v6, "right"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v4

    .line 56
    .local v4, "right":Ljava/lang/String;
    if-eqz v4, :cond_3

    .line 57
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillHandler;->gradientFill:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->setRight(D)V

    .line 60
    :cond_3
    const-string/jumbo v6, "top"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v5

    .line 61
    .local v5, "top":Ljava/lang/String;
    if-eqz v5, :cond_4

    .line 62
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillHandler;->gradientFill:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->setTop(D)V

    .line 65
    :cond_4
    const-string/jumbo v6, "bottom"

    invoke-virtual {p0, p3, v6, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 67
    .local v0, "bottom":Ljava/lang/String;
    if-eqz v0, :cond_5

    .line 68
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillHandler;->gradientFill:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->setBottom(D)V

    .line 70
    :cond_5
    return-void
.end method
