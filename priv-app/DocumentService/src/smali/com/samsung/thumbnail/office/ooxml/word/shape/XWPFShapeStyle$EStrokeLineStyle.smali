.class public final enum Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;
.super Ljava/lang/Enum;
.source "XWPFShapeStyle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EStrokeLineStyle"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

.field public static final enum SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

.field public static final enum THICK_BETWEEN_THIN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

.field public static final enum THICK_THIN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

.field public static final enum THIN_THICK:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

.field public static final enum THIN_THIN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 62
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    const-string/jumbo v1, "SINGLE"

    const-string/jumbo v2, "single"

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    .line 65
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    const-string/jumbo v1, "THIN_THIN"

    const-string/jumbo v2, "thinThin"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;->THIN_THIN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    .line 68
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    const-string/jumbo v1, "THIN_THICK"

    const-string/jumbo v2, "thinThick"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;->THIN_THICK:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    .line 71
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    const-string/jumbo v1, "THICK_THIN"

    const-string/jumbo v2, "thickThin"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;->THICK_THIN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    .line 74
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    const-string/jumbo v1, "THICK_BETWEEN_THIN"

    const-string/jumbo v2, "thickBetweenThin"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;->THICK_BETWEEN_THIN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    .line 58
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;->THIN_THIN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;->THIN_THICK:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;->THICK_THIN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;->THICK_BETWEEN_THIN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    aput-object v1, v0, v7

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 78
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;->value:Ljava/lang/String;

    .line 79
    return-void
.end method

.method public static convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 86
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;->values()[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 87
    .local v2, "inst":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 91
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;
    :goto_1
    return-object v2

    .line 86
    .restart local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 91
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 58
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    return-object v0
.end method


# virtual methods
.method public xmlValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;->value:Ljava/lang/String;

    return-object v0
.end method
