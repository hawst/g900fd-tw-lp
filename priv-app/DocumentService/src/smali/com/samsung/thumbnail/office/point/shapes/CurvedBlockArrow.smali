.class public Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;
.super Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;
.source "CurvedBlockArrow.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mDrawOnCanvas:Z

.field private mLeftMarg:I

.field private mTopMarg:I


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "folderPath"    # Ljava/io/File;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;-><init>()V

    .line 29
    const-string/jumbo v0, "CurvedBlockArrow"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->TAG:Ljava/lang/String;

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mDrawOnCanvas:Z

    .line 35
    iput-object p1, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->folderPath:Ljava/io/File;

    .line 36
    return-void
.end method


# virtual methods
.method protected draw(Landroid/graphics/Canvas;I)V
    .locals 40
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeCount"    # I

    .prologue
    .line 162
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    const v5, 0x46a8c000    # 21600.0f

    div-float v21, v4, v5

    .line 163
    .local v21, "mulFactor_Width":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    const v5, 0x46a8c000    # 21600.0f

    div-float v20, v4, v5

    .line 165
    .local v20, "mulFactor_Height":F
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "CurvedRightArrow"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 177
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval0:I

    if-nez v4, :cond_0

    .line 178
    const/16 v4, 0x3c45

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval0:I

    .line 179
    :cond_0
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval1:I

    if-nez v4, :cond_1

    .line 180
    const/16 v4, 0x4e59

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval1:I

    .line 181
    :cond_1
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval2:I

    if-nez v4, :cond_2

    .line 182
    const/16 v4, 0x3f48

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval2:I

    .line 184
    :cond_2
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval0:I

    int-to-float v4, v4

    mul-float v11, v4, v20

    .line 185
    .local v11, "adj0":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval1:I

    int-to-float v4, v4

    mul-float v12, v4, v20

    .line 186
    .local v12, "adj1":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval2:I

    int-to-float v4, v4

    mul-float v13, v4, v21

    .line 188
    .local v13, "adj2":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    add-float/2addr v4, v11

    sub-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-double v4, v4

    const/high16 v6, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    sub-float/2addr v7, v13

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    div-float/2addr v7, v8

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    sub-float/2addr v8, v13

    mul-float/2addr v7, v8

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    div-float/2addr v7, v8

    sub-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-float v14, v4

    .line 191
    .local v14, "at10":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    float-to-double v4, v4

    const/high16 v6, 0x3f800000    # 1.0f

    const/high16 v7, 0x40000000    # 2.0f

    mul-float/2addr v7, v12

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    sub-float/2addr v7, v8

    sub-float/2addr v7, v11

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    add-float/2addr v8, v11

    sub-float/2addr v8, v12

    div-float/2addr v7, v8

    const/high16 v8, 0x40000000    # 2.0f

    mul-float/2addr v8, v12

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    sub-float/2addr v8, v10

    sub-float/2addr v8, v11

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    add-float/2addr v10, v11

    sub-float/2addr v10, v12

    div-float/2addr v8, v10

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-float v0, v4

    move/from16 v16, v0

    .line 195
    .local v16, "at25":F
    new-instance v23, Landroid/graphics/Path;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/Path;-><init>()V

    .line 197
    .local v23, "path":Landroid/graphics/Path;
    new-instance v24, Landroid/graphics/RectF;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    mul-float/2addr v6, v7

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    add-float/2addr v7, v11

    sub-float/2addr v7, v12

    move-object/from16 v0, v24

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 198
    .local v24, "rectF":Landroid/graphics/RectF;
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v32, v0

    .line 199
    .local v32, "xc":D
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v38, v0

    .line 200
    .local v38, "yc":D
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    float-to-double v0, v4

    move-wide/from16 v28, v0

    .line 201
    .local v28, "x1":D
    const-wide/16 v34, 0x0

    .line 202
    .local v34, "y1":D
    sub-double v4, v34, v38

    sub-double v6, v28, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v26, v0

    .line 204
    .local v26, "startA":F
    const-wide/16 v30, 0x0

    .line 205
    .local v30, "x2":D
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    add-float/2addr v4, v11

    sub-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-double v0, v4

    move-wide/from16 v36, v0

    .line 206
    .local v36, "y2":D
    sub-double v4, v36, v38

    sub-double v6, v30, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v19, v0

    .line 207
    .local v19, "endA":F
    sub-float v27, v19, v26

    .line 208
    .local v27, "sweepA":F
    const/4 v4, 0x0

    cmpl-float v4, v27, v4

    if-lez v4, :cond_3

    .line 209
    const/high16 v4, 0x43b40000    # 360.0f

    sub-float v27, v27, v4

    .line 210
    :cond_3
    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 213
    new-instance v24, Landroid/graphics/RectF;

    .end local v24    # "rectF":Landroid/graphics/RectF;
    const/4 v4, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    sub-float/2addr v5, v6

    sub-float/2addr v5, v11

    const/high16 v6, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    mul-float/2addr v6, v7

    move-object/from16 v0, v24

    invoke-direct {v0, v4, v5, v6, v12}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 214
    .restart local v24    # "rectF":Landroid/graphics/RectF;
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v32, v0

    .line 215
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v38, v0

    .line 216
    const-wide/16 v28, 0x0

    .line 217
    const/high16 v4, 0x40400000    # 3.0f

    mul-float/2addr v4, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    sub-float/2addr v4, v5

    sub-float/2addr v4, v11

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-double v0, v4

    move-wide/from16 v34, v0

    .line 218
    sub-double v4, v34, v38

    sub-double v6, v28, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v26, v0

    .line 219
    float-to-double v0, v13

    move-wide/from16 v30, v0

    .line 220
    const/high16 v4, 0x40400000    # 3.0f

    mul-float/2addr v4, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    sub-float/2addr v4, v5

    sub-float/2addr v4, v11

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float/2addr v4, v14

    float-to-double v0, v4

    move-wide/from16 v36, v0

    .line 221
    sub-double v4, v36, v38

    sub-double v6, v30, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v19, v0

    .line 222
    sub-float v27, v19, v26

    .line 223
    const/4 v4, 0x0

    cmpl-float v4, v27, v4

    if-lez v4, :cond_4

    .line 224
    const/high16 v4, 0x43b40000    # 360.0f

    sub-float v27, v27, v4

    .line 225
    :cond_4
    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 227
    move-wide/from16 v0, v30

    double-to-float v4, v0

    move-wide/from16 v0, v36

    double-to-float v5, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 230
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    add-float/2addr v4, v12

    sub-float/2addr v4, v11

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float/2addr v4, v14

    move-object/from16 v0, v23

    invoke-virtual {v0, v13, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 231
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    add-float/2addr v5, v11

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 232
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    sub-float v4, v11, v4

    add-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float/2addr v4, v14

    move-object/from16 v0, v23

    invoke-virtual {v0, v13, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 234
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    add-float/2addr v4, v11

    sub-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float/2addr v4, v14

    move-object/from16 v0, v23

    invoke-virtual {v0, v13, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 237
    new-instance v24, Landroid/graphics/RectF;

    .end local v24    # "rectF":Landroid/graphics/RectF;
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    mul-float/2addr v6, v7

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    add-float/2addr v7, v11

    sub-float/2addr v7, v12

    move-object/from16 v0, v24

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 238
    .restart local v24    # "rectF":Landroid/graphics/RectF;
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v32, v0

    .line 239
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v38, v0

    .line 240
    float-to-double v0, v13

    move-wide/from16 v28, v0

    .line 241
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    add-float/2addr v4, v11

    sub-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float/2addr v4, v14

    float-to-double v0, v4

    move-wide/from16 v34, v0

    .line 242
    sub-double v4, v34, v38

    sub-double v6, v28, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v26, v0

    .line 243
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    sub-float v4, v4, v16

    float-to-double v0, v4

    move-wide/from16 v30, v0

    .line 244
    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v12, v4

    float-to-double v0, v4

    move-wide/from16 v36, v0

    .line 245
    sub-double v4, v36, v38

    sub-double v6, v30, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v19, v0

    .line 246
    sub-float v27, v19, v26

    .line 247
    const/4 v4, 0x0

    cmpg-float v4, v27, v4

    if-gez v4, :cond_5

    .line 248
    const/high16 v4, 0x43b40000    # 360.0f

    add-float v27, v27, v4

    .line 249
    :cond_5
    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 252
    new-instance v24, Landroid/graphics/RectF;

    .end local v24    # "rectF":Landroid/graphics/RectF;
    const/4 v4, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    sub-float/2addr v5, v6

    sub-float/2addr v5, v11

    const/high16 v6, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    mul-float/2addr v6, v7

    move-object/from16 v0, v24

    invoke-direct {v0, v4, v5, v6, v12}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 253
    .restart local v24    # "rectF":Landroid/graphics/RectF;
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v32, v0

    .line 254
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v38, v0

    .line 255
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    sub-float v4, v4, v16

    float-to-double v0, v4

    move-wide/from16 v28, v0

    .line 256
    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v12, v4

    float-to-double v0, v4

    move-wide/from16 v34, v0

    .line 257
    sub-double v4, v34, v38

    sub-double v6, v28, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v26, v0

    .line 258
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    float-to-double v0, v4

    move-wide/from16 v30, v0

    .line 259
    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v4, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    sub-float/2addr v4, v5

    sub-float/2addr v4, v11

    float-to-double v0, v4

    move-wide/from16 v36, v0

    .line 260
    sub-double v4, v36, v38

    sub-double v6, v30, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v19, v0

    .line 261
    sub-float v27, v19, v26

    .line 262
    const/4 v4, 0x0

    cmpg-float v4, v27, v4

    if-gez v4, :cond_6

    .line 263
    const/high16 v4, 0x43b40000    # 360.0f

    add-float v27, v27, v4

    .line 264
    :cond_6
    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 266
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Path;->close()V

    .line 268
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_7

    .line 269
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 271
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_8

    .line 272
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 275
    :cond_8
    new-instance v24, Landroid/graphics/RectF;

    .end local v24    # "rectF":Landroid/graphics/RectF;
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    mul-float/2addr v6, v7

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    add-float/2addr v7, v11

    sub-float/2addr v7, v12

    move-object/from16 v0, v24

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 276
    .restart local v24    # "rectF":Landroid/graphics/RectF;
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v32, v0

    .line 277
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v38, v0

    .line 278
    const-wide/16 v28, 0x0

    .line 279
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    add-float/2addr v4, v11

    sub-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-double v0, v4

    move-wide/from16 v34, v0

    .line 280
    sub-double v4, v34, v38

    sub-double v6, v28, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v26, v0

    .line 281
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    sub-float v4, v4, v16

    float-to-double v0, v4

    move-wide/from16 v30, v0

    .line 282
    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v12, v4

    float-to-double v0, v4

    move-wide/from16 v36, v0

    .line 283
    sub-double v4, v36, v38

    sub-double v6, v30, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v19, v0

    .line 284
    sub-float v27, v19, v26

    .line 285
    const/4 v4, 0x0

    cmpl-float v4, v27, v4

    if-lez v4, :cond_9

    .line 286
    const/high16 v4, 0x43b40000    # 360.0f

    sub-float v27, v27, v4

    .line 287
    :cond_9
    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 289
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_a

    .line 290
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 294
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->textArea:Landroid/graphics/RectF;

    const v5, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 295
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->textArea:Landroid/graphics/RectF;

    const v5, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 296
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->textArea:Landroid/graphics/RectF;

    const v5, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 297
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->textArea:Landroid/graphics/RectF;

    const v5, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    .line 703
    .end local v11    # "adj0":F
    .end local v12    # "adj1":F
    .end local v13    # "adj2":F
    .end local v14    # "at10":F
    .end local v16    # "at25":F
    .end local v19    # "endA":F
    .end local v23    # "path":Landroid/graphics/Path;
    .end local v24    # "rectF":Landroid/graphics/RectF;
    .end local v26    # "startA":F
    .end local v27    # "sweepA":F
    .end local v28    # "x1":D
    .end local v30    # "x2":D
    .end local v32    # "xc":D
    .end local v34    # "y1":D
    .end local v36    # "y2":D
    .end local v38    # "yc":D
    :cond_b
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mDrawOnCanvas:Z

    if-nez v4, :cond_c

    .line 704
    new-instance v9, Landroid/graphics/Matrix;

    invoke-direct {v9}, Landroid/graphics/Matrix;-><init>()V

    .line 707
    .local v9, "mtx":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->rotation:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    invoke-virtual {v9, v4, v5, v6}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 709
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->bitmap:Landroid/graphics/Bitmap;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->bitmapWidth:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->bitmapHight:I

    const/4 v10, 0x1

    invoke-static/range {v4 .. v10}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v25

    .line 714
    .local v25, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v22, Ljava/io/FileOutputStream;

    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->folderPath:Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "pic_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->shapeName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p2

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ".png"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-direct {v0, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 718
    .local v22, "out":Ljava/io/FileOutputStream;
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    move-object/from16 v0, v25

    move-object/from16 v1, v22

    invoke-virtual {v0, v4, v5, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 719
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 724
    .end local v9    # "mtx":Landroid/graphics/Matrix;
    .end local v22    # "out":Ljava/io/FileOutputStream;
    .end local v25    # "rotatedBMP":Landroid/graphics/Bitmap;
    :cond_c
    :goto_1
    return-void

    .line 300
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "CurvedLeftArrow"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_16

    .line 312
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval0:I

    if-nez v4, :cond_e

    .line 313
    const/16 v4, 0x32a0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval0:I

    .line 314
    :cond_e
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval1:I

    if-nez v4, :cond_f

    .line 315
    const/16 v4, 0x4bf0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval1:I

    .line 316
    :cond_f
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval2:I

    if-nez v4, :cond_10

    .line 317
    const/16 v4, 0x1c20

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval2:I

    .line 319
    :cond_10
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval0:I

    int-to-float v4, v4

    mul-float v11, v4, v20

    .line 320
    .restart local v11    # "adj0":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval1:I

    int-to-float v4, v4

    mul-float v12, v4, v20

    .line 321
    .restart local v12    # "adj1":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval2:I

    int-to-float v4, v4

    mul-float v13, v4, v21

    .line 323
    .restart local v13    # "adj2":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    add-float/2addr v4, v11

    sub-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-double v4, v4

    const/high16 v6, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    div-float v7, v13, v7

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    div-float v8, v13, v8

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-float v0, v4

    move/from16 v17, v0

    .line 326
    .local v17, "at9":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    float-to-double v4, v4

    const/high16 v6, 0x3f800000    # 1.0f

    const/high16 v7, 0x40000000    # 2.0f

    mul-float/2addr v7, v12

    sub-float/2addr v7, v11

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    sub-float/2addr v7, v8

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    add-float/2addr v8, v11

    sub-float/2addr v8, v12

    div-float/2addr v7, v8

    const/high16 v8, 0x40000000    # 2.0f

    mul-float/2addr v8, v12

    sub-float/2addr v8, v11

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    sub-float/2addr v8, v10

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    add-float/2addr v10, v11

    sub-float/2addr v10, v12

    div-float/2addr v8, v10

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-float v15, v4

    .line 330
    .local v15, "at24":F
    new-instance v23, Landroid/graphics/Path;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/Path;-><init>()V

    .line 333
    .restart local v23    # "path":Landroid/graphics/Path;
    new-instance v24, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    neg-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    add-float/2addr v7, v11

    sub-float/2addr v7, v12

    move-object/from16 v0, v24

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 334
    .restart local v24    # "rectF":Landroid/graphics/RectF;
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v32, v0

    .line 335
    .restart local v32    # "xc":D
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v38, v0

    .line 336
    .restart local v38    # "yc":D
    const-wide/16 v28, 0x0

    .line 337
    .restart local v28    # "x1":D
    const-wide/16 v34, 0x0

    .line 338
    .restart local v34    # "y1":D
    sub-double v4, v34, v38

    sub-double v6, v28, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v26, v0

    .line 340
    .restart local v26    # "startA":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    float-to-double v0, v4

    move-wide/from16 v30, v0

    .line 341
    .restart local v30    # "x2":D
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    add-float/2addr v4, v11

    sub-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-double v0, v4

    move-wide/from16 v36, v0

    .line 342
    .restart local v36    # "y2":D
    sub-double v4, v36, v38

    sub-double v6, v30, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v19, v0

    .line 343
    .restart local v19    # "endA":F
    sub-float v27, v19, v26

    .line 344
    .restart local v27    # "sweepA":F
    const/4 v4, 0x0

    cmpg-float v4, v27, v4

    if-gez v4, :cond_11

    .line 345
    const/high16 v4, 0x43b40000    # 360.0f

    add-float v27, v27, v4

    .line 346
    :cond_11
    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 349
    new-instance v24, Landroid/graphics/RectF;

    .end local v24    # "rectF":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    neg-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    sub-float/2addr v5, v6

    sub-float/2addr v5, v11

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    move-object/from16 v0, v24

    invoke-direct {v0, v4, v5, v6, v12}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 350
    .restart local v24    # "rectF":Landroid/graphics/RectF;
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v32, v0

    .line 351
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v38, v0

    .line 352
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    float-to-double v0, v4

    move-wide/from16 v28, v0

    .line 353
    const/high16 v4, 0x40400000    # 3.0f

    mul-float/2addr v4, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    sub-float/2addr v4, v5

    sub-float/2addr v4, v11

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-double v0, v4

    move-wide/from16 v34, v0

    .line 354
    sub-double v4, v34, v38

    sub-double v6, v28, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v26, v0

    .line 355
    float-to-double v0, v13

    move-wide/from16 v30, v0

    .line 356
    const/high16 v4, 0x40400000    # 3.0f

    mul-float/2addr v4, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    sub-float/2addr v4, v5

    sub-float/2addr v4, v11

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float v4, v4, v17

    float-to-double v0, v4

    move-wide/from16 v36, v0

    .line 357
    sub-double v4, v36, v38

    sub-double v6, v30, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v19, v0

    .line 358
    sub-float v27, v19, v26

    .line 359
    const/4 v4, 0x0

    cmpg-float v4, v27, v4

    if-gez v4, :cond_12

    .line 360
    const/high16 v4, 0x43b40000    # 360.0f

    add-float v27, v27, v4

    .line 361
    :cond_12
    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 364
    move-wide/from16 v0, v30

    double-to-float v4, v0

    move-wide/from16 v0, v36

    double-to-float v5, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 367
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    add-float/2addr v4, v11

    sub-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float v4, v4, v17

    add-float/2addr v4, v12

    sub-float/2addr v4, v11

    move-object/from16 v0, v23

    invoke-virtual {v0, v13, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 368
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    add-float/2addr v5, v11

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 369
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    add-float/2addr v4, v11

    sub-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float v4, v4, v17

    add-float/2addr v4, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v13, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 372
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    add-float/2addr v4, v11

    sub-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float v4, v4, v17

    move-object/from16 v0, v23

    invoke-virtual {v0, v13, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 375
    new-instance v24, Landroid/graphics/RectF;

    .end local v24    # "rectF":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    neg-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    add-float/2addr v7, v11

    sub-float/2addr v7, v12

    move-object/from16 v0, v24

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 376
    .restart local v24    # "rectF":Landroid/graphics/RectF;
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v32, v0

    .line 377
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v38, v0

    .line 378
    float-to-double v0, v13

    move-wide/from16 v28, v0

    .line 379
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    add-float/2addr v4, v11

    sub-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float v4, v4, v17

    float-to-double v0, v4

    move-wide/from16 v34, v0

    .line 380
    sub-double v4, v34, v38

    sub-double v6, v28, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v26, v0

    .line 381
    float-to-double v0, v15

    move-wide/from16 v30, v0

    .line 382
    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v12, v4

    float-to-double v0, v4

    move-wide/from16 v36, v0

    .line 383
    sub-double v4, v36, v38

    sub-double v6, v30, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v19, v0

    .line 384
    sub-float v27, v19, v26

    .line 385
    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 388
    new-instance v24, Landroid/graphics/RectF;

    .end local v24    # "rectF":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    neg-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    sub-float/2addr v5, v6

    sub-float/2addr v5, v11

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    move-object/from16 v0, v24

    invoke-direct {v0, v4, v5, v6, v12}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 389
    .restart local v24    # "rectF":Landroid/graphics/RectF;
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v32, v0

    .line 390
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v38, v0

    .line 391
    float-to-double v0, v15

    move-wide/from16 v28, v0

    .line 392
    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v12, v4

    float-to-double v0, v4

    move-wide/from16 v34, v0

    .line 393
    sub-double v4, v34, v38

    sub-double v6, v28, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v26, v0

    .line 394
    const-wide/16 v30, 0x0

    .line 395
    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v4, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    sub-float/2addr v4, v5

    sub-float/2addr v4, v11

    float-to-double v0, v4

    move-wide/from16 v36, v0

    .line 396
    sub-double v4, v36, v38

    sub-double v6, v30, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v19, v0

    .line 397
    sub-float v27, v19, v26

    .line 398
    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 400
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Path;->close()V

    .line 402
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_13

    .line 403
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 405
    :cond_13
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_14

    .line 406
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 409
    :cond_14
    new-instance v23, Landroid/graphics/Path;

    .end local v23    # "path":Landroid/graphics/Path;
    invoke-direct/range {v23 .. v23}, Landroid/graphics/Path;-><init>()V

    .line 410
    .restart local v23    # "path":Landroid/graphics/Path;
    new-instance v24, Landroid/graphics/RectF;

    .end local v24    # "rectF":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    neg-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v5, v12

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    sub-float/2addr v5, v6

    sub-float/2addr v5, v11

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    move-object/from16 v0, v24

    invoke-direct {v0, v4, v5, v6, v12}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 411
    .restart local v24    # "rectF":Landroid/graphics/RectF;
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v32, v0

    .line 412
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v38, v0

    .line 413
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    float-to-double v0, v4

    move-wide/from16 v28, v0

    .line 414
    const/high16 v4, 0x40400000    # 3.0f

    mul-float/2addr v4, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    sub-float/2addr v4, v5

    sub-float/2addr v4, v11

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-double v0, v4

    move-wide/from16 v34, v0

    .line 415
    sub-double v4, v34, v38

    sub-double v6, v28, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v26, v0

    .line 416
    float-to-double v0, v15

    move-wide/from16 v30, v0

    .line 417
    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v12, v4

    float-to-double v0, v4

    move-wide/from16 v36, v0

    .line 418
    sub-double v4, v36, v38

    sub-double v6, v30, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v19, v0

    .line 419
    sub-float v27, v19, v26

    .line 420
    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 422
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_15

    .line 423
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 427
    :cond_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->textArea:Landroid/graphics/RectF;

    const v5, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 428
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->textArea:Landroid/graphics/RectF;

    const v5, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 429
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->textArea:Landroid/graphics/RectF;

    const v5, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 430
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->textArea:Landroid/graphics/RectF;

    const v5, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 433
    .end local v11    # "adj0":F
    .end local v12    # "adj1":F
    .end local v13    # "adj2":F
    .end local v15    # "at24":F
    .end local v17    # "at9":F
    .end local v19    # "endA":F
    .end local v23    # "path":Landroid/graphics/Path;
    .end local v24    # "rectF":Landroid/graphics/RectF;
    .end local v26    # "startA":F
    .end local v27    # "sweepA":F
    .end local v28    # "x1":D
    .end local v30    # "x2":D
    .end local v32    # "xc":D
    .end local v34    # "y1":D
    .end local v36    # "y2":D
    .end local v38    # "yc":D
    :cond_16
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "CurvedUpArrow"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1f

    .line 445
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval0:I

    if-nez v4, :cond_17

    .line 446
    const/16 v4, 0x32a0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval0:I

    .line 447
    :cond_17
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval1:I

    if-nez v4, :cond_18

    .line 448
    const/16 v4, 0x4bf0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval1:I

    .line 449
    :cond_18
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval2:I

    if-nez v4, :cond_19

    .line 450
    const/16 v4, 0x1c20

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval2:I

    .line 452
    :cond_19
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval0:I

    int-to-float v4, v4

    mul-float v11, v4, v21

    .line 453
    .restart local v11    # "adj0":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval1:I

    int-to-float v4, v4

    mul-float v12, v4, v21

    .line 454
    .restart local v12    # "adj1":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval2:I

    int-to-float v4, v4

    mul-float v13, v4, v20

    .line 456
    .restart local v13    # "adj2":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    add-float/2addr v4, v11

    sub-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-double v4, v4

    const/high16 v6, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    div-float v7, v13, v7

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    div-float v8, v13, v8

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-float v0, v4

    move/from16 v17, v0

    .line 459
    .restart local v17    # "at9":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    float-to-double v4, v4

    const/high16 v6, 0x3f800000    # 1.0f

    const/high16 v7, 0x40000000    # 2.0f

    mul-float/2addr v7, v12

    sub-float/2addr v7, v11

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    sub-float/2addr v7, v8

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    add-float/2addr v8, v11

    sub-float/2addr v8, v12

    div-float/2addr v7, v8

    const/high16 v8, 0x40000000    # 2.0f

    mul-float/2addr v8, v12

    sub-float/2addr v8, v11

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    sub-float/2addr v8, v10

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    add-float/2addr v10, v11

    sub-float/2addr v10, v12

    div-float/2addr v8, v10

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-float v15, v4

    .line 463
    .restart local v15    # "at24":F
    new-instance v23, Landroid/graphics/Path;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/Path;-><init>()V

    .line 466
    .restart local v23    # "path":Landroid/graphics/Path;
    new-instance v24, Landroid/graphics/RectF;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    neg-float v5, v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    add-float/2addr v6, v11

    sub-float/2addr v6, v12

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    move-object/from16 v0, v24

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 467
    .restart local v24    # "rectF":Landroid/graphics/RectF;
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v32, v0

    .line 468
    .restart local v32    # "xc":D
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v38, v0

    .line 469
    .restart local v38    # "yc":D
    const-wide/16 v28, 0x0

    .line 470
    .restart local v28    # "x1":D
    const-wide/16 v34, 0x0

    .line 471
    .restart local v34    # "y1":D
    sub-double v4, v34, v38

    sub-double v6, v28, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v26, v0

    .line 473
    .restart local v26    # "startA":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    add-float/2addr v4, v11

    sub-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-double v0, v4

    move-wide/from16 v30, v0

    .line 474
    .restart local v30    # "x2":D
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    float-to-double v0, v4

    move-wide/from16 v36, v0

    .line 475
    .restart local v36    # "y2":D
    sub-double v4, v36, v38

    sub-double v6, v30, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v19, v0

    .line 476
    .restart local v19    # "endA":F
    sub-float v27, v19, v26

    .line 477
    .restart local v27    # "sweepA":F
    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 480
    new-instance v24, Landroid/graphics/RectF;

    .end local v24    # "rectF":Landroid/graphics/RectF;
    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v4, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    sub-float/2addr v4, v5

    sub-float/2addr v4, v11

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    neg-float v5, v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    move-object/from16 v0, v24

    invoke-direct {v0, v4, v5, v12, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 481
    .restart local v24    # "rectF":Landroid/graphics/RectF;
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v32, v0

    .line 482
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v38, v0

    .line 483
    const/high16 v4, 0x40400000    # 3.0f

    mul-float/2addr v4, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    sub-float/2addr v4, v5

    sub-float/2addr v4, v11

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-double v0, v4

    move-wide/from16 v28, v0

    .line 484
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    float-to-double v0, v4

    move-wide/from16 v34, v0

    .line 485
    sub-double v4, v34, v38

    sub-double v6, v28, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v26, v0

    .line 486
    const/high16 v4, 0x40400000    # 3.0f

    mul-float/2addr v4, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    sub-float/2addr v4, v5

    sub-float/2addr v4, v11

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float v4, v4, v17

    float-to-double v0, v4

    move-wide/from16 v30, v0

    .line 487
    float-to-double v0, v13

    move-wide/from16 v36, v0

    .line 488
    sub-double v4, v36, v38

    sub-double v6, v30, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v19, v0

    .line 489
    sub-float v27, v19, v26

    .line 490
    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 492
    move-wide/from16 v0, v30

    double-to-float v4, v0

    move-wide/from16 v0, v36

    double-to-float v5, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 495
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    add-float/2addr v4, v11

    sub-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float v4, v4, v17

    add-float/2addr v4, v12

    sub-float/2addr v4, v11

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v13}, Landroid/graphics/Path;->lineTo(FF)V

    .line 496
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    add-float/2addr v4, v11

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 497
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    add-float/2addr v4, v11

    sub-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float v4, v4, v17

    add-float/2addr v4, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v13}, Landroid/graphics/Path;->lineTo(FF)V

    .line 499
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    add-float/2addr v4, v11

    sub-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float v4, v4, v17

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v13}, Landroid/graphics/Path;->lineTo(FF)V

    .line 502
    new-instance v24, Landroid/graphics/RectF;

    .end local v24    # "rectF":Landroid/graphics/RectF;
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    neg-float v5, v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    add-float/2addr v6, v11

    sub-float/2addr v6, v12

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    move-object/from16 v0, v24

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 503
    .restart local v24    # "rectF":Landroid/graphics/RectF;
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v32, v0

    .line 504
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v38, v0

    .line 505
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    add-float/2addr v4, v11

    sub-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float v4, v4, v17

    float-to-double v0, v4

    move-wide/from16 v28, v0

    .line 506
    float-to-double v0, v13

    move-wide/from16 v34, v0

    .line 507
    sub-double v4, v34, v38

    sub-double v6, v28, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v26, v0

    .line 508
    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v12, v4

    float-to-double v0, v4

    move-wide/from16 v30, v0

    .line 509
    float-to-double v0, v15

    move-wide/from16 v36, v0

    .line 510
    sub-double v4, v36, v38

    sub-double v6, v30, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v19, v0

    .line 511
    sub-float v27, v19, v26

    .line 512
    const/4 v4, 0x0

    cmpg-float v4, v27, v4

    if-gez v4, :cond_1a

    .line 513
    const/high16 v4, 0x43b40000    # 360.0f

    add-float v27, v27, v4

    .line 514
    :cond_1a
    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 517
    new-instance v24, Landroid/graphics/RectF;

    .end local v24    # "rectF":Landroid/graphics/RectF;
    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v4, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    sub-float/2addr v4, v5

    sub-float/2addr v4, v11

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    neg-float v5, v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    move-object/from16 v0, v24

    invoke-direct {v0, v4, v5, v12, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 518
    .restart local v24    # "rectF":Landroid/graphics/RectF;
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v32, v0

    .line 519
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v38, v0

    .line 520
    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v12, v4

    float-to-double v0, v4

    move-wide/from16 v28, v0

    .line 521
    float-to-double v0, v15

    move-wide/from16 v34, v0

    .line 522
    sub-double v4, v34, v38

    sub-double v6, v28, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v26, v0

    .line 523
    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v4, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    sub-float/2addr v4, v5

    sub-float/2addr v4, v11

    float-to-double v0, v4

    move-wide/from16 v30, v0

    .line 524
    const-wide/16 v36, 0x0

    .line 525
    sub-double v4, v36, v38

    sub-double v6, v30, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v19, v0

    .line 526
    sub-float v27, v19, v26

    .line 527
    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 529
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Path;->close()V

    .line 531
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_1b

    .line 532
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 534
    :cond_1b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_1c

    .line 535
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 538
    :cond_1c
    new-instance v23, Landroid/graphics/Path;

    .end local v23    # "path":Landroid/graphics/Path;
    invoke-direct/range {v23 .. v23}, Landroid/graphics/Path;-><init>()V

    .line 539
    .restart local v23    # "path":Landroid/graphics/Path;
    new-instance v24, Landroid/graphics/RectF;

    .end local v24    # "rectF":Landroid/graphics/RectF;
    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v4, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    sub-float/2addr v4, v5

    sub-float/2addr v4, v11

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    neg-float v5, v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    move-object/from16 v0, v24

    invoke-direct {v0, v4, v5, v12, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 540
    .restart local v24    # "rectF":Landroid/graphics/RectF;
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v32, v0

    .line 541
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v38, v0

    .line 542
    const/high16 v4, 0x40400000    # 3.0f

    mul-float/2addr v4, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    sub-float/2addr v4, v5

    sub-float/2addr v4, v11

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-double v0, v4

    move-wide/from16 v28, v0

    .line 543
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    float-to-double v0, v4

    move-wide/from16 v34, v0

    .line 544
    sub-double v4, v34, v38

    sub-double v6, v28, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v26, v0

    .line 545
    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v12, v4

    float-to-double v0, v4

    move-wide/from16 v30, v0

    .line 546
    float-to-double v0, v15

    move-wide/from16 v36, v0

    .line 547
    sub-double v4, v36, v38

    sub-double v6, v30, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v19, v0

    .line 548
    sub-float v27, v19, v26

    .line 549
    const/4 v4, 0x0

    cmpg-float v4, v27, v4

    if-gez v4, :cond_1d

    .line 550
    const/high16 v4, 0x43b40000    # 360.0f

    add-float v27, v27, v4

    .line 551
    :cond_1d
    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 553
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_1e

    .line 554
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 558
    :cond_1e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->textArea:Landroid/graphics/RectF;

    const v5, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 559
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->textArea:Landroid/graphics/RectF;

    const v5, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 560
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->textArea:Landroid/graphics/RectF;

    const v5, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 561
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->textArea:Landroid/graphics/RectF;

    const v5, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 564
    .end local v11    # "adj0":F
    .end local v12    # "adj1":F
    .end local v13    # "adj2":F
    .end local v15    # "at24":F
    .end local v17    # "at9":F
    .end local v19    # "endA":F
    .end local v23    # "path":Landroid/graphics/Path;
    .end local v24    # "rectF":Landroid/graphics/RectF;
    .end local v26    # "startA":F
    .end local v27    # "sweepA":F
    .end local v28    # "x1":D
    .end local v30    # "x2":D
    .end local v32    # "xc":D
    .end local v34    # "y1":D
    .end local v36    # "y2":D
    .end local v38    # "yc":D
    :cond_1f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->shapeName:Ljava/lang/String;

    const-string/jumbo v5, "CurvedDownArrow"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 576
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval0:I

    if-nez v4, :cond_20

    .line 577
    const/16 v4, 0x32a0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval0:I

    .line 578
    :cond_20
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval1:I

    if-nez v4, :cond_21

    .line 579
    const/16 v4, 0x4bf0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval1:I

    .line 580
    :cond_21
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval2:I

    if-nez v4, :cond_22

    .line 581
    const/16 v4, 0x3840

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval2:I

    .line 583
    :cond_22
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval0:I

    int-to-float v4, v4

    mul-float v11, v4, v21

    .line 584
    .restart local v11    # "adj0":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval1:I

    int-to-float v4, v4

    mul-float v12, v4, v21

    .line 585
    .restart local v12    # "adj1":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->adjval2:I

    int-to-float v4, v4

    mul-float v13, v4, v20

    .line 587
    .restart local v13    # "adj2":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    add-float/2addr v4, v11

    sub-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-double v4, v4

    const/high16 v6, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    sub-float/2addr v7, v13

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    div-float/2addr v7, v8

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    sub-float/2addr v8, v13

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    div-float/2addr v8, v10

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-float v14, v4

    .line 590
    .restart local v14    # "at10":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    float-to-double v4, v4

    const/high16 v6, 0x3f800000    # 1.0f

    const/high16 v7, 0x40000000    # 2.0f

    mul-float/2addr v7, v12

    sub-float/2addr v7, v11

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    sub-float/2addr v7, v8

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    add-float/2addr v8, v11

    sub-float/2addr v8, v12

    div-float/2addr v7, v8

    const/high16 v8, 0x40000000    # 2.0f

    mul-float/2addr v8, v12

    sub-float/2addr v8, v11

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    sub-float/2addr v8, v10

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    add-float/2addr v10, v11

    sub-float/2addr v10, v12

    div-float/2addr v8, v10

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-float v0, v4

    move/from16 v16, v0

    .line 594
    .restart local v16    # "at25":F
    new-instance v23, Landroid/graphics/Path;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/Path;-><init>()V

    .line 597
    .restart local v23    # "path":Landroid/graphics/Path;
    new-instance v24, Landroid/graphics/RectF;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    add-float/2addr v6, v11

    sub-float/2addr v6, v12

    const/high16 v7, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    mul-float/2addr v7, v8

    move-object/from16 v0, v24

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 598
    .restart local v24    # "rectF":Landroid/graphics/RectF;
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v32, v0

    .line 599
    .restart local v32    # "xc":D
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v38, v0

    .line 600
    .restart local v38    # "yc":D
    const-wide/16 v28, 0x0

    .line 601
    .restart local v28    # "x1":D
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    float-to-double v0, v4

    move-wide/from16 v34, v0

    .line 602
    .restart local v34    # "y1":D
    sub-double v4, v34, v38

    sub-double v6, v28, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v26, v0

    .line 604
    .restart local v26    # "startA":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    add-float/2addr v4, v11

    sub-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-double v0, v4

    move-wide/from16 v30, v0

    .line 605
    .restart local v30    # "x2":D
    const-wide/16 v36, 0x0

    .line 606
    .restart local v36    # "y2":D
    sub-double v4, v36, v38

    sub-double v6, v30, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v19, v0

    .line 607
    .restart local v19    # "endA":F
    sub-float v27, v19, v26

    .line 608
    .restart local v27    # "sweepA":F
    const/4 v4, 0x0

    cmpg-float v4, v27, v4

    if-gez v4, :cond_23

    .line 609
    const/high16 v4, 0x43b40000    # 360.0f

    add-float v27, v27, v4

    .line 610
    :cond_23
    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 613
    new-instance v24, Landroid/graphics/RectF;

    .end local v24    # "rectF":Landroid/graphics/RectF;
    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v4, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    sub-float/2addr v4, v5

    sub-float/2addr v4, v11

    const/4 v5, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    mul-float/2addr v6, v7

    move-object/from16 v0, v24

    invoke-direct {v0, v4, v5, v12, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 614
    .restart local v24    # "rectF":Landroid/graphics/RectF;
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v32, v0

    .line 615
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v38, v0

    .line 616
    const/high16 v4, 0x40400000    # 3.0f

    mul-float/2addr v4, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    sub-float/2addr v4, v5

    sub-float/2addr v4, v11

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-double v0, v4

    move-wide/from16 v28, v0

    .line 617
    const-wide/16 v34, 0x0

    .line 618
    sub-double v4, v34, v38

    sub-double v6, v28, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v26, v0

    .line 619
    const/high16 v4, 0x40400000    # 3.0f

    mul-float/2addr v4, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    sub-float/2addr v4, v5

    sub-float/2addr v4, v11

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float/2addr v4, v14

    float-to-double v0, v4

    move-wide/from16 v30, v0

    .line 620
    float-to-double v0, v13

    move-wide/from16 v36, v0

    .line 621
    sub-double v4, v36, v38

    sub-double v6, v30, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v19, v0

    .line 622
    sub-float v27, v19, v26

    .line 623
    const/4 v4, 0x0

    cmpg-float v4, v27, v4

    if-gez v4, :cond_24

    .line 624
    const/high16 v4, 0x43b40000    # 360.0f

    add-float v27, v27, v4

    .line 625
    :cond_24
    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 627
    move-wide/from16 v0, v30

    double-to-float v4, v0

    move-wide/from16 v0, v36

    double-to-float v5, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 630
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    add-float/2addr v4, v11

    sub-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float/2addr v4, v14

    add-float/2addr v4, v12

    sub-float/2addr v4, v11

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v13}, Landroid/graphics/Path;->lineTo(FF)V

    .line 631
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    add-float/2addr v4, v11

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 632
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    add-float/2addr v4, v11

    sub-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float/2addr v4, v14

    add-float/2addr v4, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    sub-float/2addr v4, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v13}, Landroid/graphics/Path;->lineTo(FF)V

    .line 634
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    add-float/2addr v4, v11

    sub-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float/2addr v4, v14

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v13}, Landroid/graphics/Path;->lineTo(FF)V

    .line 637
    new-instance v24, Landroid/graphics/RectF;

    .end local v24    # "rectF":Landroid/graphics/RectF;
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    add-float/2addr v6, v11

    sub-float/2addr v6, v12

    const/high16 v7, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    mul-float/2addr v7, v8

    move-object/from16 v0, v24

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 638
    .restart local v24    # "rectF":Landroid/graphics/RectF;
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v32, v0

    .line 639
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v38, v0

    .line 640
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    add-float/2addr v4, v11

    sub-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float/2addr v4, v14

    float-to-double v0, v4

    move-wide/from16 v28, v0

    .line 641
    float-to-double v0, v13

    move-wide/from16 v34, v0

    .line 642
    sub-double v4, v34, v38

    sub-double v6, v28, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v26, v0

    .line 643
    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v12, v4

    float-to-double v0, v4

    move-wide/from16 v30, v0

    .line 644
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    sub-float v4, v4, v16

    float-to-double v0, v4

    move-wide/from16 v36, v0

    .line 645
    sub-double v4, v36, v38

    sub-double v6, v30, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v19, v0

    .line 646
    sub-float v27, v19, v26

    .line 647
    const/4 v4, 0x0

    cmpl-float v4, v27, v4

    if-lez v4, :cond_25

    .line 648
    const/high16 v4, 0x43b40000    # 360.0f

    sub-float v27, v27, v4

    .line 649
    :cond_25
    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 652
    new-instance v24, Landroid/graphics/RectF;

    .end local v24    # "rectF":Landroid/graphics/RectF;
    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v4, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    sub-float/2addr v4, v5

    sub-float/2addr v4, v11

    const/4 v5, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    mul-float/2addr v6, v7

    move-object/from16 v0, v24

    invoke-direct {v0, v4, v5, v12, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 653
    .restart local v24    # "rectF":Landroid/graphics/RectF;
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v32, v0

    .line 654
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v38, v0

    .line 655
    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v12, v4

    float-to-double v0, v4

    move-wide/from16 v28, v0

    .line 656
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    sub-float v4, v4, v16

    float-to-double v0, v4

    move-wide/from16 v34, v0

    .line 657
    sub-double v4, v34, v38

    sub-double v6, v28, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v26, v0

    .line 658
    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v4, v12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    sub-float/2addr v4, v5

    sub-float/2addr v4, v11

    float-to-double v0, v4

    move-wide/from16 v30, v0

    .line 659
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    float-to-double v0, v4

    move-wide/from16 v36, v0

    .line 660
    sub-double v4, v36, v38

    sub-double v6, v30, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v19, v0

    .line 661
    sub-float v27, v19, v26

    .line 662
    const/4 v4, 0x0

    cmpl-float v4, v27, v4

    if-lez v4, :cond_26

    .line 663
    const/high16 v4, 0x43b40000    # 360.0f

    sub-float v27, v27, v4

    .line 664
    :cond_26
    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 666
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Path;->close()V

    .line 668
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_27

    .line 669
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 671
    :cond_27
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_28

    .line 672
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 675
    :cond_28
    new-instance v23, Landroid/graphics/Path;

    .end local v23    # "path":Landroid/graphics/Path;
    invoke-direct/range {v23 .. v23}, Landroid/graphics/Path;-><init>()V

    .line 676
    .restart local v23    # "path":Landroid/graphics/Path;
    new-instance v24, Landroid/graphics/RectF;

    .end local v24    # "rectF":Landroid/graphics/RectF;
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    add-float/2addr v6, v11

    sub-float/2addr v6, v12

    const/high16 v7, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    mul-float/2addr v7, v8

    move-object/from16 v0, v24

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 677
    .restart local v24    # "rectF":Landroid/graphics/RectF;
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v32, v0

    .line 678
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v38, v0

    .line 679
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    add-float/2addr v4, v11

    sub-float/2addr v4, v12

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-double v0, v4

    move-wide/from16 v28, v0

    .line 680
    const-wide/16 v34, 0x0

    .line 681
    sub-double v4, v34, v38

    sub-double v6, v28, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v26, v0

    .line 682
    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v12, v4

    float-to-double v0, v4

    move-wide/from16 v30, v0

    .line 683
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    sub-float v4, v4, v16

    float-to-double v0, v4

    move-wide/from16 v36, v0

    .line 684
    sub-double v4, v36, v38

    sub-double v6, v30, v32

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v19, v0

    .line 685
    sub-float v27, v19, v26

    .line 686
    const/4 v4, 0x0

    cmpg-float v4, v27, v4

    if-gez v4, :cond_29

    .line 687
    const/high16 v4, 0x43b40000    # 360.0f

    add-float v27, v27, v4

    .line 688
    :cond_29
    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 690
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_2a

    .line 691
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 695
    :cond_2a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->textArea:Landroid/graphics/RectF;

    const v5, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 696
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->textArea:Landroid/graphics/RectF;

    const v5, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 697
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->textArea:Landroid/graphics/RectF;

    const v5, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 698
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->textArea:Landroid/graphics/RectF;

    const v5, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    mul-float/2addr v5, v6

    const v6, 0x46a8c000    # 21600.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 720
    .end local v11    # "adj0":F
    .end local v12    # "adj1":F
    .end local v13    # "adj2":F
    .end local v14    # "at10":F
    .end local v16    # "at25":F
    .end local v19    # "endA":F
    .end local v23    # "path":Landroid/graphics/Path;
    .end local v24    # "rectF":Landroid/graphics/RectF;
    .end local v26    # "startA":F
    .end local v27    # "sweepA":F
    .end local v28    # "x1":D
    .end local v30    # "x2":D
    .end local v32    # "xc":D
    .end local v34    # "y1":D
    .end local v36    # "y2":D
    .end local v38    # "yc":D
    .restart local v9    # "mtx":Landroid/graphics/Matrix;
    .restart local v25    # "rotatedBMP":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v18

    .line 721
    .local v18, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception while writing pictures to out file"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F

    .prologue
    .line 81
    invoke-super {p0, p3, p4, p7, p8}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IFF)V

    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mDrawOnCanvas:Z

    .line 83
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mLeftMarg:I

    .line 84
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mTopMarg:I

    .line 85
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 86
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->draw(Landroid/graphics/Canvas;I)V

    .line 87
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mDrawOnCanvas:Z

    .line 89
    return-void
.end method

.method public drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p2, "shapeCount"    # I

    .prologue
    .line 72
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V

    .line 74
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->draw(I)V

    .line 75
    return-void
.end method

.method public drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 147
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;I)V

    .line 148
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mDrawOnCanvas:Z

    .line 149
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mLeftMarg:I

    .line 150
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mTopMarg:I

    .line 151
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    .line 152
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    .line 153
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 154
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->draw(Landroid/graphics/Canvas;I)V

    .line 155
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 156
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mDrawOnCanvas:Z

    .line 157
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V
    .locals 0
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 40
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 41
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->draw(I)V

    .line 42
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V
    .locals 2
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I

    .prologue
    .line 46
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mDrawOnCanvas:Z

    .line 48
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mLeftMarg:I

    .line 49
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mTopMarg:I

    .line 50
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 51
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->draw(Landroid/graphics/Canvas;I)V

    .line 52
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mDrawOnCanvas:Z

    .line 54
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 102
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mDrawOnCanvas:Z

    .line 104
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mLeftMarg:I

    .line 105
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mTopMarg:I

    .line 106
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 107
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->draw(Landroid/graphics/Canvas;I)V

    .line 108
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 109
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mDrawOnCanvas:Z

    .line 110
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 114
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V

    .line 115
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mDrawOnCanvas:Z

    .line 116
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mLeftMarg:I

    .line 117
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mTopMarg:I

    .line 118
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 119
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->draw(Landroid/graphics/Canvas;I)V

    .line 120
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 121
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mDrawOnCanvas:Z

    .line 122
    return-void
.end method

.method public drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .prologue
    .line 96
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 97
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->draw(I)V

    .line 98
    return-void
.end method

.method public drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 59
    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p7

    move v4, p8

    move-object v5, p9

    invoke-super/range {v0 .. v5}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSSExcelShape(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mDrawOnCanvas:Z

    .line 62
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mLeftMarg:I

    .line 63
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mTopMarg:I

    .line 64
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 65
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->draw(Landroid/graphics/Canvas;I)V

    .line 66
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mDrawOnCanvas:Z

    .line 68
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;I)V
    .locals 1
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 125
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 126
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->draw(I)V

    .line 127
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 131
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 132
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mDrawOnCanvas:Z

    .line 133
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mLeftMarg:I

    .line 134
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mTopMarg:I

    .line 135
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->width:F

    .line 136
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->height:F

    .line 137
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 138
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->draw(Landroid/graphics/Canvas;I)V

    .line 139
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 140
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->mDrawOnCanvas:Z

    .line 141
    return-void
.end method
