.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLStrictSequenceHandler;
.source "PartTableStyleHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblCellBordersHandler$ICellBrdrConsumer;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ITcTxStylesFntClr;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler$IPartTableStyleConsumer;
    }
.end annotation


# instance fields
.field private partStyle:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

.field private partTblStylConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler$IPartTableStyleConsumer;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler$IPartTableStyleConsumer;)V
    .locals 1
    .param p1, "ele"    # Ljava/lang/String;
    .param p2, "partTblStylConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler$IPartTableStyleConsumer;

    .prologue
    .line 33
    const/16 v0, 0x1f

    invoke-direct {p0, v0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStrictSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 34
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;->partTblStylConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler$IPartTableStyleConsumer;

    .line 35
    return-void
.end method


# virtual methods
.method public consumeFill(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 1
    .param p1, "fillProperties"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;->partStyle:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->setFillProperties(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V

    .line 100
    return-void
.end method

.method public handleBold(Z)V
    .locals 1
    .param p1, "state"    # Z

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;->partStyle:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->setFontBold(Z)V

    .line 110
    return-void
.end method

.method public handleBorder(Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;)V
    .locals 1
    .param p1, "tcBrdr"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;->partStyle:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->setTcBrdr(Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;)V

    .line 94
    return-void
.end method

.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 68
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStrictSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;->partStyle:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;->partStyle:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getTcBrdr()Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->addTblPartStyle(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;)V

    .line 71
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;->partStyle:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;->partStyle:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->addFillProps(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V

    .line 73
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;->partStyle:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillPropsLst()Ljava/util/HashMap;

    move-result-object v0

    const-string/jumbo v1, "firstRow"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;->partStyle:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillPropsLst()Ljava/util/HashMap;

    move-result-object v0

    const-string/jumbo v1, "firstRow"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;->partStyle:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getTint()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;->partTblStylConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler$IPartTableStyleConsumer;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;->partStyle:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-interface {v0, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler$IPartTableStyleConsumer;->handlePartStyle(Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V

    .line 89
    return-void
.end method

.method public handleFontColor(Ljava/lang/String;)V
    .locals 1
    .param p1, "color"    # Ljava/lang/String;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;->partStyle:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->setTxStyFntClr(Ljava/lang/String;)V

    .line 104
    return-void
.end method

.method public handleItalic(Z)V
    .locals 1
    .param p1, "state"    # Z

    .prologue
    .line 114
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;->partStyle:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->setFontItalic(Z)V

    .line 116
    return-void
.end method

.method protected init()V
    .locals 6

    .prologue
    .line 41
    const/4 v5, 0x2

    new-array v0, v5, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 43
    .local v0, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcStyleHandler;

    invoke-direct {v2, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcStyleHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;)V

    .line 44
    .local v2, "tcStyleHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcStyleHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v5, "tcStyle"

    invoke-direct {v1, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 46
    .local v1, "tcSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v5, 0x0

    aput-object v1, v0, v5

    .line 48
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler;

    invoke-direct {v4, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler$ITcTxStylesFntClr;)V

    .line 49
    .local v4, "tctxStyleHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcTxStyleHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v5, "tcTxStyle"

    invoke-direct {v3, v5, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 51
    .local v3, "tctxSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v5, 0x1

    aput-object v3, v0, v5

    .line 53
    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 54
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 59
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStrictSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 60
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;->partStyle:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    .line 62
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;->init()V

    .line 64
    return-void
.end method
