.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;
.super Ljava/lang/Object;
.source "XSLFTableStyles.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ILineChoiceConsumer;


# static fields
.field private static final TAG:Ljava/lang/String; = "XSLFTableStyles"


# instance fields
.field private lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

.field private linePropsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;",
            ">;"
        }
    .end annotation
.end field

.field private tableFormats:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;",
            ">;"
        }
    .end annotation
.end field

.field private tableFormatsList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;",
            ">;>;"
        }
    .end annotation
.end field

.field private tblBgcolor:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->linePropsList:Ljava/util/ArrayList;

    .line 20
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->tableFormatsList:Ljava/util/Map;

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->tblBgcolor:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public addLinePropsList(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V
    .locals 1
    .param p1, "lineProps"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->linePropsList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    return-void
.end method

.method public addTblBgColor(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "styleID"    # Ljava/lang/String;
    .param p2, "tblBgColor"    # Ljava/lang/String;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->tblBgcolor:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    return-void
.end method

.method public addTblFormatList(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;)V
    .locals 2
    .param p1, "styleID"    # Ljava/lang/String;
    .param p2, "tblPartStyle"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;

    .prologue
    .line 44
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->tableFormatsList:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 45
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->tableFormatsList:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    :goto_0
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->tableFormats:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    return-void

    .line 47
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 48
    .local v0, "partList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;>;"
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->tableFormatsList:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public consumeLine(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V
    .locals 0
    .param p1, "lineProps"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .line 77
    return-void
.end method

.method public getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    return-object v0
.end method

.method public getLinePropsList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->linePropsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTblBgColor(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "styleId"    # Ljava/lang/String;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->tblBgcolor:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getTblFormat()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->tableFormats:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTblFormatList(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "styleId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->tableFormatsList:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public parseTableStyles(Ljava/io/InputStream;)V
    .locals 5
    .param p1, "inputStream"    # Ljava/io/InputStream;

    .prologue
    .line 25
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;->tableFormats:Ljava/util/ArrayList;

    .line 26
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStylesParser;

    invoke-direct {v1, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStylesParser;-><init>(Ljava/lang/Object;)V

    .line 27
    .local v1, "tableStylesParser":Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStylesParser;
    invoke-virtual {v1, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStylesParser;->parse(Ljava/io/InputStream;)V

    .line 29
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 33
    :goto_0
    return-void

    .line 30
    :catch_0
    move-exception v0

    .line 31
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v2, "XSLFTableStyles"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "IOException : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
