.class public Lcom/samsung/thumbnail/office/ooxml/pptx/PowerPointBuilder;
.super Ljava/lang/Object;
.source "PowerPointBuilder.java"


# instance fields
.field protected slideShow:Lorg/apache/poi/hslf/usermodel/SlideShow;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addShapeToSlide()V
    .locals 0

    .prologue
    .line 24
    return-void
.end method

.method public createSlide()V
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/PowerPointBuilder;->slideShow:Lorg/apache/poi/hslf/usermodel/SlideShow;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/usermodel/SlideShow;->createSlide()Lorg/apache/poi/hslf/model/Slide;

    .line 18
    return-void
.end method

.method public createSlideShow()V
    .locals 1

    .prologue
    .line 13
    new-instance v0, Lorg/apache/poi/hslf/usermodel/SlideShow;

    invoke-direct {v0}, Lorg/apache/poi/hslf/usermodel/SlideShow;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/PowerPointBuilder;->slideShow:Lorg/apache/poi/hslf/usermodel/SlideShow;

    .line 14
    return-void
.end method
