.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$ColorConsumer;
.super Ljava/lang/Object;
.source "GsHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "ColorConsumer"
.end annotation


# instance fields
.field gsConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;

.field pos:F


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;)V
    .locals 0
    .param p1, "gsConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$ColorConsumer;->gsConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;

    .line 39
    return-void
.end method


# virtual methods
.method public consumeColor(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 2
    .param p1, "clrScheme"    # Ljava/lang/String;
    .param p2, "val"    # Ljava/lang/String;
    .param p3, "fillProperties"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    .param p4, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$ColorConsumer;->gsConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;

    iget v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$ColorConsumer;->pos:F

    invoke-interface {v0, p1, v1, p2, p4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;->consumeGS(Ljava/lang/String;FLjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V

    .line 45
    return-void
.end method
