.class public final enum Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;
.super Ljava/lang/Enum;
.source "XWPFShapeStyle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EStrokeArrowType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

.field public static final enum BLOCK:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

.field public static final enum CLASSIC:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

.field public static final enum DIAMOND:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

.field public static final enum NONE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

.field public static final enum OPEN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

.field public static final enum OVAL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 167
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    const-string/jumbo v1, "NONE"

    const-string/jumbo v2, "none"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    .line 170
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    const-string/jumbo v1, "BLOCK"

    const-string/jumbo v2, "block"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;->BLOCK:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    .line 173
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    const-string/jumbo v1, "CLASSIC"

    const-string/jumbo v2, "classic"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;->CLASSIC:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    .line 176
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    const-string/jumbo v1, "OVAL"

    const-string/jumbo v2, "oval"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;->OVAL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    .line 179
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    const-string/jumbo v1, "DIAMOND"

    const-string/jumbo v2, "diamond"

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;->DIAMOND:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    .line 182
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    const-string/jumbo v1, "OPEN"

    const/4 v2, 0x5

    const-string/jumbo v3, "open"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;->OPEN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    .line 163
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;->BLOCK:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;->CLASSIC:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;->OVAL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;->DIAMOND:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;->OPEN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 185
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 186
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;->value:Ljava/lang/String;

    .line 187
    return-void
.end method

.method public static convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 194
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;->values()[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 195
    .local v2, "inst":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 199
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;
    :goto_1
    return-object v2

    .line 194
    .restart local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 199
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 163
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;
    .locals 1

    .prologue
    .line 163
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    return-object v0
.end method


# virtual methods
.method public xmlValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;->value:Ljava/lang/String;

    return-object v0
.end method
