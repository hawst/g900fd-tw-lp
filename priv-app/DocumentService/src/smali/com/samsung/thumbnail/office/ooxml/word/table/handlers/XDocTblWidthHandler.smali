.class public Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XDocTblWidthHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler$IXTblWOberver;
    }
.end annotation


# instance fields
.field private oberver:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler$IXTblWOberver;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler$IXTblWOberver;)V
    .locals 0
    .param p1, "elementName"    # Ljava/lang/String;
    .param p2, "oberver"    # Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler$IXTblWOberver;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 18
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;->oberver:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler$IXTblWOberver;

    .line 19
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 24
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 25
    const-string/jumbo v2, "w"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 26
    .local v0, "width":Ljava/lang/String;
    const-string/jumbo v2, "type"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 27
    .local v1, "widthType":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;->oberver:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler$IXTblWOberver;

    if-eqz v2, :cond_0

    .line 28
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;->oberver:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler$IXTblWOberver;

    invoke-virtual {p0, p2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler$IXTblWOberver;->setElement(Ljava/lang/String;)V

    .line 29
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;->oberver:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler$IXTblWOberver;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, p1, v3}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler$IXTblWOberver;->setTblWidth(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;I)V

    .line 30
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler;->oberver:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler$IXTblWOberver;

    invoke-static {v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    move-result-object v3

    invoke-interface {v2, p1, v3}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblWidthHandler$IXTblWOberver;->setWidthType(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;)V

    .line 33
    :cond_0
    return-void
.end method
