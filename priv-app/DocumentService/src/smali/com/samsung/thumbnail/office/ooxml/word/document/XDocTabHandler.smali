.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XDocTabHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabHandler$ITabContObserver;
    }
.end annotation


# instance fields
.field mTabContentObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabHandler$ITabContObserver;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabHandler$ITabContObserver;)V
    .locals 1
    .param p1, "tabContObserver"    # Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabHandler$ITabContObserver;

    .prologue
    .line 19
    const-string/jumbo v0, "tab"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 20
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabHandler;->mTabContentObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabHandler$ITabContObserver;

    .line 21
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 27
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    const-string/jumbo v2, "val"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;-><init>(Ljava/lang/String;)V

    .line 29
    .local v1, "val":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;

    const-string/jumbo v2, "pos"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;-><init>(Ljava/lang/String;)V

    .line 31
    .local v0, "pos":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 32
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabHandler;->mTabContentObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabHandler$ITabContObserver;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;->getValue()I

    move-result v4

    invoke-interface {v2, p1, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocTabHandler$ITabContObserver;->setTabContent(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;I)V

    .line 34
    :cond_0
    return-void
.end method
