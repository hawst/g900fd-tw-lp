.class public Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
.super Ljava/lang/Object;
.source "XDocParaProperties.java"


# static fields
.field public static final B_VAL:[I

.field public static final LOWER:I = 0xa

.field public static final ROMANLC:[Ljava/lang/String;

.field public static final ROMANUC:[Ljava/lang/String;

.field public static final UPPER:I = 0x14


# instance fields
.field private borders:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

.field private buAutoNum:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field private buChar:Ljava/lang/String;

.field private charProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

.field private defTabSz:I

.field private indentPPT:I

.field private indentation:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

.field private isBuNone:Z

.field private isBullet:Z

.field private isContextualSpace:Z

.field private justify:Ljava/lang/String;

.field private keepNxtStatus:Z

.field private keeplinesStatus:Z

.field private level:I

.field private lineSpacing:F

.field private marginLeft:I

.field private marginRight:I

.field private numbering:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;

.field private pgBrBefStatus:Z

.field private shadeProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

.field protected sldNumber:I

.field private spaceAfter:F

.field private spacing:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

.field private styleId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v3, 0xd

    .line 12
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "M"

    aput-object v1, v0, v4

    const-string/jumbo v1, "CM"

    aput-object v1, v0, v5

    const-string/jumbo v1, "D"

    aput-object v1, v0, v6

    const-string/jumbo v1, "CD"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string/jumbo v2, "C"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "XC"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "L"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "XL"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "X"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "IX"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "V"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "IV"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "I"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->ROMANUC:[Ljava/lang/String;

    .line 14
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "m"

    aput-object v1, v0, v4

    const-string/jumbo v1, "cm"

    aput-object v1, v0, v5

    const-string/jumbo v1, "d"

    aput-object v1, v0, v6

    const-string/jumbo v1, "cd"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string/jumbo v2, "c"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "xc"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "l"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "xl"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "x"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "ix"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "v"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "iv"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "i"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->ROMANLC:[Ljava/lang/String;

    .line 16
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->B_VAL:[I

    return-void

    :array_0
    .array-data 4
        0x3e8
        0x384
        0x1f4
        0x190
        0x64
        0x5a
        0x32
        0x28
        0xa
        0x9
        0x5
        0x4
        0x1
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->level:I

    .line 29
    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBullet:Z

    .line 30
    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBuNone:Z

    .line 31
    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->marginLeft:I

    .line 32
    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->marginRight:I

    .line 36
    iput v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->lineSpacing:F

    .line 37
    iput v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->spaceAfter:F

    .line 38
    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->indentPPT:I

    .line 39
    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->defTabSz:I

    .line 69
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V
    .locals 2
    .param p1, "paraProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->level:I

    .line 29
    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBullet:Z

    .line 30
    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBuNone:Z

    .line 31
    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->marginLeft:I

    .line 32
    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->marginRight:I

    .line 36
    iput v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->lineSpacing:F

    .line 37
    iput v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->spaceAfter:F

    .line 38
    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->indentPPT:I

    .line 39
    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->defTabSz:I

    .line 72
    if-nez p1, :cond_0

    .line 76
    :goto_0
    return-void

    .line 75
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->assign(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V

    goto :goto_0
.end method

.method public static getApha(II)Ljava/lang/String;
    .locals 4
    .param p0, "count"    # I
    .param p1, "alphaCase"    # I

    .prologue
    .line 351
    const-string/jumbo v0, ""

    .line 353
    .local v0, "alpha":Ljava/lang/String;
    sparse-switch p1, :sswitch_data_0

    .line 363
    :goto_0
    return-object v0

    .line 355
    :sswitch_0
    rem-int/lit8 v3, p0, 0x1a

    add-int/lit8 v3, v3, 0x61

    int-to-char v2, v3

    .line 356
    .local v2, "ch":C
    invoke-static {v2}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    .line 357
    goto :goto_0

    .line 359
    .end local v2    # "ch":C
    :sswitch_1
    rem-int/lit8 v3, p0, 0x1a

    add-int/lit8 v3, v3, 0x41

    int-to-char v1, v3

    .line 360
    .local v1, "b":C
    invoke-static {v1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 353
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
    .end sparse-switch
.end method

.method public static getRomanDigit(II)Ljava/lang/String;
    .locals 5
    .param p0, "count"    # I
    .param p1, "romanCase"    # I

    .prologue
    .line 322
    const-string/jumbo v2, ""

    .line 323
    .local v2, "roman":Ljava/lang/String;
    if-lez p0, :cond_0

    const/16 v4, 0xfa0

    if-lt p0, v4, :cond_1

    :cond_0
    move-object v3, v2

    .line 347
    .end local v2    # "roman":Ljava/lang/String;
    .local v3, "roman":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 332
    .end local v3    # "roman":Ljava/lang/String;
    .restart local v2    # "roman":Ljava/lang/String;
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v4, 0x100

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 333
    .local v0, "concater":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->ROMANUC:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_3

    .line 334
    :goto_2
    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->B_VAL:[I

    aget v4, v4, v1

    if-lt p0, v4, :cond_2

    .line 335
    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->B_VAL:[I

    aget v4, v4, v1

    sub-int/2addr p0, v4

    .line 336
    sparse-switch p1, :sswitch_data_0

    goto :goto_2

    .line 338
    :sswitch_0
    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->ROMANLC:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 341
    :sswitch_1
    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->ROMANUC:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 333
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 346
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 347
    .end local v2    # "roman":Ljava/lang/String;
    .restart local v3    # "roman":Ljava/lang/String;
    goto :goto_0

    .line 336
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public assign(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V
    .locals 1
    .param p1, "paraProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    .prologue
    .line 308
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->borders:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->borders:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    .line 309
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->indentation:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->indentation:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    .line 310
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->justify:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->justify:Ljava/lang/String;

    .line 311
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->numbering:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->numbering:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;

    .line 312
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->shadeProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->shadeProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    .line 313
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->spacing:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->spacing:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    .line 314
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->buAutoNum:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->buAutoNum:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 315
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->buChar:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->buChar:Ljava/lang/String;

    .line 316
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBullet:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBullet:Z

    .line 317
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBuNone:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBuNone:Z

    .line 318
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->marginLeft:I

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->marginLeft:I

    .line 319
    return-void
.end method

.method public getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->borders:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    return-object v0
.end method

.method public getBuAutoNum()Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->buAutoNum:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    return-object v0
.end method

.method public getBulletChar()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->buChar:Ljava/lang/String;

    return-object v0
.end method

.method public getDefTabSize()I
    .locals 1

    .prologue
    .line 214
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->defTabSz:I

    return v0
.end method

.method public getIndentPPT()I
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->indentPPT:I

    return v0
.end method

.method public getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->indentation:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    return-object v0
.end method

.method public getJustify()Ljava/lang/String;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->justify:Ljava/lang/String;

    return-object v0
.end method

.method public getKeepLinesStatus()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->keeplinesStatus:Z

    return v0
.end method

.method public getKeepNxtStatus()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->keepNxtStatus:Z

    return v0
.end method

.method public getLevel()I
    .locals 1

    .prologue
    .line 134
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->level:I

    return v0
.end method

.method public getLineSpacing()F
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->lineSpacing:F

    return v0
.end method

.method public getMarginLeft()I
    .locals 1

    .prologue
    .line 182
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->marginLeft:I

    return v0
.end method

.method public getMarginRight()I
    .locals 1

    .prologue
    .line 206
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->marginRight:I

    return v0
.end method

.method public getNumbering()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->numbering:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;

    return-object v0
.end method

.method public getParaCharProperties()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->charProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    return-object v0
.end method

.method public getPgBrBefStatus()Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->pgBrBefStatus:Z

    return v0
.end method

.method public getShadeColor()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->shadeProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    return-object v0
.end method

.method public getSldNumber()I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->sldNumber:I

    return v0
.end method

.method public getSpaceAfter()F
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->spaceAfter:F

    return v0
.end method

.method public getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->spacing:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    return-object v0
.end method

.method public getStyleId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->styleId:Ljava/lang/String;

    return-object v0
.end method

.method public isBuNone()Z
    .locals 1

    .prologue
    .line 158
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBuNone:Z

    return v0
.end method

.method public isBullet()Z
    .locals 1

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBullet:Z

    return v0
.end method

.method public isContextualSpace()Z
    .locals 1

    .prologue
    .line 202
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isContextualSpace:Z

    return v0
.end method

.method public mergeParaProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V
    .locals 2
    .param p1, "paraProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    .prologue
    .line 250
    if-nez p1, :cond_1

    .line 288
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->borders:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    if-nez v0, :cond_2

    .line 256
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->borders:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->borders:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    .line 257
    :cond_2
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->indentation:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    if-nez v0, :cond_c

    .line 258
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->indentation:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->indentation:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    .line 262
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->justify:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 263
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->justify:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->justify:Ljava/lang/String;

    .line 264
    :cond_4
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->numbering:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;

    if-nez v0, :cond_5

    .line 265
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->numbering:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->numbering:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;

    .line 266
    :cond_5
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->shadeProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    if-nez v0, :cond_6

    .line 267
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->shadeProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->shadeProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    .line 268
    :cond_6
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->charProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    if-nez v0, :cond_7

    .line 269
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->charProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->charProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .line 270
    :cond_7
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->spacing:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    if-nez v0, :cond_d

    .line 271
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->spacing:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->spacing:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    .line 275
    :cond_8
    :goto_2
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->buAutoNum:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    if-nez v0, :cond_9

    .line 276
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->buAutoNum:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->buAutoNum:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 277
    :cond_9
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->buChar:Ljava/lang/String;

    if-nez v0, :cond_a

    .line 278
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->buChar:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->buChar:Ljava/lang/String;

    .line 279
    :cond_a
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBullet:Z

    if-nez v0, :cond_b

    .line 280
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBullet:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBullet:Z

    .line 282
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBuNone:Z

    if-nez v0, :cond_b

    .line 283
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBuNone:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBuNone:Z

    .line 286
    :cond_b
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->marginLeft:I

    if-nez v0, :cond_0

    .line 287
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->marginLeft:I

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->marginLeft:I

    goto :goto_0

    .line 259
    :cond_c
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->indentation:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    if-eqz v0, :cond_3

    .line 260
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->indentation:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    iget-object v1, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->indentation:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->merge(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;)V

    goto :goto_1

    .line 272
    :cond_d
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->spacing:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    if-eqz v0, :cond_8

    .line 273
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->spacing:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    iget-object v1, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->spacing:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->merge(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;)V

    goto :goto_2
.end method

.method public setBorders(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V
    .locals 1
    .param p1, "borderType"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    .param p2, "border"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .prologue
    .line 107
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->borders:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    if-nez v0, :cond_0

    .line 108
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->borders:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->borders:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->setBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    .line 111
    return-void
.end method

.method public setBuAutoNum(Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;)V
    .locals 0
    .param p1, "buAutoNum"    # Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->buAutoNum:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 163
    return-void
.end method

.method public setBuNone()V
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBuNone:Z

    .line 155
    return-void
.end method

.method public setBullet()V
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBullet:Z

    .line 147
    return-void
.end method

.method public setBulletChar(Ljava/lang/String;)V
    .locals 0
    .param p1, "buChar"    # Ljava/lang/String;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->buChar:Ljava/lang/String;

    .line 139
    return-void
.end method

.method public setContextualSpacing()V
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isContextualSpace:Z

    .line 199
    return-void
.end method

.method public setDefTabSize(I)V
    .locals 0
    .param p1, "defTabSz"    # I

    .prologue
    .line 194
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->defTabSz:I

    .line 195
    return-void
.end method

.method public setIndent_PPT(I)V
    .locals 0
    .param p1, "indentVal"    # I

    .prologue
    .line 190
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->indentPPT:I

    .line 191
    return-void
.end method

.method public setIndentation(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;)V
    .locals 0
    .param p1, "indentation"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->indentation:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    .line 84
    return-void
.end method

.method public setJustify(Ljava/lang/String;)V
    .locals 0
    .param p1, "justify"    # Ljava/lang/String;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->justify:Ljava/lang/String;

    .line 96
    return-void
.end method

.method public setKeepLinesStatus(Z)V
    .locals 0
    .param p1, "keeplinesStatus"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->keeplinesStatus:Z

    .line 66
    return-void
.end method

.method public setKeepNxtStatus(Z)V
    .locals 0
    .param p1, "keepNxtStatus"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->keepNxtStatus:Z

    .line 54
    return-void
.end method

.method public setLevel(I)V
    .locals 0
    .param p1, "level"    # I

    .prologue
    .line 114
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->level:I

    .line 115
    return-void
.end method

.method public setLineSpacing(F)V
    .locals 0
    .param p1, "lineSpacing"    # F

    .prologue
    .line 118
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->lineSpacing:F

    .line 119
    return-void
.end method

.method public setMarginLeft(I)V
    .locals 0
    .param p1, "marginLeft"    # I

    .prologue
    .line 178
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->marginLeft:I

    .line 179
    return-void
.end method

.method public setMarginRight(I)V
    .locals 0
    .param p1, "marginRight"    # I

    .prologue
    .line 186
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->marginRight:I

    .line 187
    return-void
.end method

.method public setNumbering(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;)V
    .locals 0
    .param p1, "numbering"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->numbering:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;

    .line 88
    return-void
.end method

.method public setParaCharProperties(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V
    .locals 0
    .param p1, "charProperties"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->charProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .line 104
    return-void
.end method

.method public setPgBrBefStatus(Z)V
    .locals 0
    .param p1, "pgBrStatus"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->pgBrBefStatus:Z

    .line 46
    return-void
.end method

.method public setShadeColor(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;)V
    .locals 0
    .param p1, "shadeProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->shadeProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    .line 100
    return-void
.end method

.method public setSldNumber(I)V
    .locals 0
    .param p1, "sldNumber"    # I

    .prologue
    .line 170
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->sldNumber:I

    .line 171
    return-void
.end method

.method public setSpaceAfter(F)V
    .locals 0
    .param p1, "spaceAfter"    # F

    .prologue
    .line 126
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->spaceAfter:F

    .line 127
    return-void
.end method

.method public setSpacing(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;)V
    .locals 0
    .param p1, "spacing"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->spacing:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    .line 80
    return-void
.end method

.method public setStyleId(Ljava/lang/String;)V
    .locals 0
    .param p1, "styleId"    # Ljava/lang/String;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->styleId:Ljava/lang/String;

    .line 92
    return-void
.end method
