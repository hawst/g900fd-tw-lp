.class public Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleParser;
.super Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;
.source "XDocStyleParser.java"


# instance fields
.field protected styles:Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;)V
    .locals 0
    .param p1, "styles"    # Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;
    .param p2, "consumer"    # Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    .prologue
    .line 17
    invoke-direct {p0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;)V

    .line 18
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleParser;->styles:Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;

    .line 19
    return-void
.end method


# virtual methods
.method public createRootElementHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStylesHandler;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleParser;->styles:Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStylesHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;)V

    .line 24
    .local v0, "stylesHandler":Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStylesHandler;
    return-object v0
.end method

.method public getMainNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .locals 1

    .prologue
    .line 29
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v0

    return-object v0
.end method
