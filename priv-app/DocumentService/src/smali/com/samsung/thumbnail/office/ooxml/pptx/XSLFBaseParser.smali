.class public abstract Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBaseParser;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
.source "XSLFBaseParser.java"


# static fields
.field protected static pptxNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 16
    const/4 v6, 0x6

    new-array v6, v6, [Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    sput-object v6, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBaseParser;->pptxNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    .line 18
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v6, "http://schemas.openxmlformats.org/presentationml/2006/main"

    const/16 v7, 0x33

    invoke-direct {v4, v6, v7}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 20
    .local v4, "pptxMLMain":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    sget-object v6, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBaseParser;->pptxNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    .line 22
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v6, "http://schemas.openxmlformats.org/officeDocument/2006/relationships"

    const/16 v7, 0xa

    invoke-direct {v5, v6, v7}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 24
    .local v5, "relationship":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    sget-object v6, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBaseParser;->pptxNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const/4 v7, 0x1

    aput-object v5, v6, v7

    .line 26
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v6, "http://schemas.openxmlformats.org/drawingml/2006/main"

    const/16 v7, 0x1f

    invoke-direct {v3, v6, v7}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 28
    .local v3, "drawingML":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    sget-object v6, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBaseParser;->pptxNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const/4 v7, 0x2

    aput-object v3, v6, v7

    .line 30
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v6, "http://schemas.openxmlformats.org/drawingml/2006/chart"

    const/16 v7, 0x23

    invoke-direct {v0, v6, v7}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 32
    .local v0, "chartML":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    sget-object v6, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBaseParser;->pptxNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const/4 v7, 0x3

    aput-object v0, v6, v7

    .line 34
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v6, "http://schemas.openxmlformats.org/drawingml/2006/diagram"

    const/16 v7, 0xca

    invoke-direct {v1, v6, v7}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 36
    .local v1, "diagram":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const-string/jumbo v6, "drawingml"

    invoke-virtual {v1, v6}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setXMLNS(Ljava/lang/String;)V

    .line 37
    const-string/jumbo v6, "diagram"

    invoke-virtual {v1, v6}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setXMLCatagory(Ljava/lang/String;)V

    .line 38
    sget-object v6, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBaseParser;->pptxNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const/4 v7, 0x4

    aput-object v1, v6, v7

    .line 40
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v6, "http://schemas.microsoft.com/office/drawing/2008/diagram"

    const/16 v7, 0xcb

    invoke-direct {v2, v6, v7}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 43
    .local v2, "diagramShape":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    const-string/jumbo v6, "drawingml"

    invoke-virtual {v2, v6}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setXMLNS(Ljava/lang/String;)V

    .line 44
    const-string/jumbo v6, "diagram"

    invoke-virtual {v2, v6}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->setXMLCatagory(Ljava/lang/String;)V

    .line 45
    sget-object v6, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBaseParser;->pptxNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const/4 v7, 0x5

    aput-object v2, v6, v7

    .line 46
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1
    .param p1, "consumer"    # Ljava/lang/Object;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;-><init>(Ljava/lang/Object;)V

    .line 50
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBaseParser;->pptxNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBaseParser;->setPredefinedNameSpaces([Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 51
    return-void
.end method
