.class public Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XDocFooterHandler;
.super Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;
.source "XDocFooterHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 13
    const-string/jumbo v0, "ftr"

    const/16 v1, 0x64

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;-><init>(Ljava/lang/String;I)V

    .line 14
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 26
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->endFooter()V

    .line 27
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 19
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->startDocument()V

    .line 20
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 21
    return-void
.end method
