.class public Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
.super Ljava/lang/Object;
.source "XWPFRun.java"


# instance fields
.field private br:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBreak;

.field private charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

.field private chartRId:Ljava/lang/String;

.field private dataModelId:Ljava/lang/String;

.field private diagramId:Ljava/lang/String;

.field private hasTab:Z

.field private isChart:Z

.field private isDiagram:Z

.field private isLastRenderedPgBr:Z

.field private isPgBreak:Z

.field private isPic:Z

.field private isShape:Z

.field private pictures:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;",
            ">;"
        }
    .end annotation
.end field

.field private shapes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;",
            ">;"
        }
    .end annotation
.end field

.field private text:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->shapes:Ljava/util/ArrayList;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->pictures:Ljava/util/ArrayList;

    .line 36
    return-void
.end method


# virtual methods
.method public addDiagramId(Ljava/lang/String;)V
    .locals 1
    .param p1, "rId"    # Ljava/lang/String;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->diagramId:Ljava/lang/String;

    .line 114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->isDiagram:Z

    .line 115
    return-void
.end method

.method public addPicture(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;)V
    .locals 1
    .param p1, "picture"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->pictures:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    return-void
.end method

.method public addShape(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V
    .locals 1
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->shapes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->isShape:Z

    .line 110
    return-void
.end method

.method public getBreak()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBreak;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->br:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBreak;

    return-object v0
.end method

.method public getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    return-object v0
.end method

.method public getChartRelId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->chartRId:Ljava/lang/String;

    return-object v0
.end method

.method public getDataModelId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->dataModelId:Ljava/lang/String;

    return-object v0
.end method

.method public getDiagramRelId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->diagramId:Ljava/lang/String;

    return-object v0
.end method

.method public getPictures()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->pictures:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getShapes()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->shapes:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->text:Ljava/lang/String;

    return-object v0
.end method

.method public hasTab()Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->hasTab:Z

    return v0
.end method

.method public isChart()Z
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->isChart:Z

    return v0
.end method

.method public isDiagram()Z
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->isDiagram:Z

    return v0
.end method

.method public isLastRenderedPgBr()Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->isLastRenderedPgBr:Z

    return v0
.end method

.method public isPageBreak()Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->isPgBreak:Z

    return v0
.end method

.method public isPict()Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->isPic:Z

    return v0
.end method

.method public isShape()Z
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->isShape:Z

    return v0
.end method

.method public setBreak(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBreak;)V
    .locals 0
    .param p1, "br"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBreak;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->br:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBreak;

    .line 69
    return-void
.end method

.method public setCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V
    .locals 0
    .param p1, "charProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .line 48
    return-void
.end method

.method public setChartRelId(Ljava/lang/String;)V
    .locals 1
    .param p1, "chartRId"    # Ljava/lang/String;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->chartRId:Ljava/lang/String;

    .line 140
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->isChart:Z

    .line 141
    return-void
.end method

.method public setDataModelRelId(Ljava/lang/String;)V
    .locals 1
    .param p1, "rId"    # Ljava/lang/String;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->dataModelId:Ljava/lang/String;

    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->isDiagram:Z

    .line 120
    return-void
.end method

.method public setLastRenderedPgBr(Z)V
    .locals 0
    .param p1, "isLastRenderedPgBr"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->isLastRenderedPgBr:Z

    .line 65
    return-void
.end method

.method public setPageBreak()V
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->isPgBreak:Z

    .line 73
    return-void
.end method

.method public setPic()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->isPic:Z

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->isShape:Z

    .line 61
    return-void
.end method

.method public setTabStatus(Z)V
    .locals 0
    .param p1, "hasTab"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->hasTab:Z

    .line 40
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->text:Ljava/lang/String;

    .line 52
    return-void
.end method
