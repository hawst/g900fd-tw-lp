.class public Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;
.super Ljava/lang/Object;
.source "PathProp.java"


# instance fields
.field private connectType:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    return-void
.end method


# virtual methods
.method public getConnectType()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectType;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;->connectType:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectType;

    return-object v0
.end method

.method public merge(Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;)V
    .locals 1
    .param p1, "pathProp"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;

    .prologue
    .line 23
    if-nez p1, :cond_1

    .line 28
    :cond_0
    :goto_0
    return-void

    .line 26
    :cond_1
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;->connectType:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectType;

    if-nez v0, :cond_0

    .line 27
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;->getConnectType()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectType;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;->connectType:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectType;

    goto :goto_0
.end method

.method public setConnectType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectType;)V
    .locals 0
    .param p1, "connectType"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectType;

    .prologue
    .line 15
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;->connectType:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectType;

    .line 16
    return-void
.end method
