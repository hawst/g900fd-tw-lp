.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualShapePropHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "NonVisualShapePropHandler.java"


# instance fields
.field private shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;


# direct methods
.method public constructor <init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 4
    .param p1, "ID"    # I
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "shapeProperties"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 24
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualShapePropHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 26
    const/4 v3, 0x1

    new-array v2, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 27
    .local v2, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualPropHandlers;

    invoke-direct {v0, p3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualPropHandlers;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 29
    .local v0, "nvDrawHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualPropHandlers;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    const-string/jumbo v3, "nvPr"

    invoke-direct {v1, p1, v3, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 33
    .local v1, "nvDrawSeq":Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    const/4 v3, 0x0

    aput-object v1, v2, v3

    .line 34
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualShapePropHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 35
    return-void
.end method


# virtual methods
.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 40
    invoke-virtual {p0, p2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualShapePropHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, "elementName":Ljava/lang/String;
    const-string/jumbo v3, "cNvPr"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 43
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualShapePropHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    const-string/jumbo v4, "name"

    invoke-virtual {p0, p3, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualShapePropHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setShapeName(Ljava/lang/String;)V

    .line 45
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualShapePropHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    const-string/jumbo v4, "id"

    invoke-virtual {p0, p3, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualShapePropHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setShapeId(I)V

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    const-string/jumbo v3, "cNvSpPr"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 49
    const-string/jumbo v3, "txBox"

    invoke-virtual {p0, p3, v3, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualShapePropHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 51
    .local v1, "txtBox":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 52
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;

    invoke-direct {v2, v1}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;-><init>(Ljava/lang/String;)V

    .line 53
    .local v2, "val":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualShapePropHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;->getValue()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setTextBox(Z)V

    goto :goto_0

    .line 58
    .end local v1    # "txtBox":Ljava/lang/String;
    .end local v2    # "val":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    goto :goto_0
.end method
