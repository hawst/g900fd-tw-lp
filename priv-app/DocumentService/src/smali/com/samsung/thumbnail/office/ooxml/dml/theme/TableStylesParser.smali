.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStylesParser;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
.source "TableStylesParser.java"


# static fields
.field protected static pptxNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;


# instance fields
.field private tableStyleLst:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 25
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    sput-object v1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStylesParser;->pptxNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    .line 26
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v1, "http://schemas.openxmlformats.org/drawingml/2006/main"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 28
    .local v0, "drawingML":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStylesParser;->pptxNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    .line 29
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1
    .param p1, "consumer"    # Ljava/lang/Object;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;-><init>(Ljava/lang/Object;)V

    .line 33
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStylesParser;->pptxNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStylesParser;->setPredefinedNameSpaces([Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStylesParser;->tableStyleLst:Ljava/util/ArrayList;

    .line 35
    return-void
.end method


# virtual methods
.method public addTableStyle(Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;)V
    .locals 1
    .param p1, "tableStyle"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStylesParser;->tableStyleLst:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    return-void
.end method

.method public createRootElementHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStylesListHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStylesListHandler;-><init>()V

    return-object v0
.end method

.method public getMainNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .locals 1

    .prologue
    .line 44
    const/16 v0, 0x1f

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStylesParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v0

    return-object v0
.end method

.method public getTableStyleList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/XSLFTableStyles;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TableStylesParser;->tableStyleLst:Ljava/util/ArrayList;

    return-object v0
.end method
