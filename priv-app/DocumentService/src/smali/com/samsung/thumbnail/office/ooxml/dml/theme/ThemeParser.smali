.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeParser;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
.source "ThemeParser.java"


# static fields
.field protected static pptxNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 16
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    sput-object v1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeParser;->pptxNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    .line 17
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const-string/jumbo v1, "http://schemas.openxmlformats.org/drawingml/2006/main"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;-><init>(Ljava/lang/String;I)V

    .line 19
    .local v0, "drawingML":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeParser;->pptxNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    .line 20
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1
    .param p1, "consumer"    # Ljava/lang/Object;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;-><init>(Ljava/lang/Object;)V

    .line 24
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeParser;->pptxNameSpaces:[Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeParser;->setPredefinedNameSpaces([Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)V

    .line 25
    return-void
.end method


# virtual methods
.method public createRootElementHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeHandler;-><init>()V

    return-object v0
.end method

.method public getMainNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .locals 1

    .prologue
    .line 34
    const/16 v0, 0x1f

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v0

    return-object v0
.end method
