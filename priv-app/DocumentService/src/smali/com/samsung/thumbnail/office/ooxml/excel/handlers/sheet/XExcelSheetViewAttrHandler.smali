.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetViewAttrHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelSheetViewAttrHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 16
    const-string/jumbo v1, "sheetView"

    invoke-direct {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 19
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSelectionHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSelectionHandler;-><init>()V

    .line 20
    .local v0, "selectionHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSelectionHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetViewAttrHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "selection"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 27
    const-string/jumbo v2, "showGridLines"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetViewAttrHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 29
    .local v0, "showGridLines":Ljava/lang/String;
    const-string/jumbo v2, "showRowColHeaders"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetViewAttrHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 32
    .local v1, "showHeaders":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string/jumbo v2, "0"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 33
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->setShowGridLines()V

    .line 35
    :cond_0
    if-eqz v1, :cond_1

    const-string/jumbo v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 36
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->setShowRowColHeaders()V

    .line 38
    :cond_1
    return-void
.end method
