.class public abstract Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;
.super Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFSimpleShape;
.source "XSLFTextShape.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;
    }
.end annotation


# instance fields
.field private bodyPr:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

.field private lstStyle:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;",
            ">;"
        }
    .end annotation
.end field

.field private paraLst:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;)V
    .locals 1
    .param p1, "shapeProperties"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "sheet"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFSimpleShape;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;)V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;->paraLst:Ljava/util/List;

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;->lstStyle:Ljava/util/Map;

    .line 28
    return-void
.end method


# virtual methods
.method public addLstStyle(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;)V
    .locals 1
    .param p1, "level"    # Ljava/lang/String;
    .param p2, "paraProp"    # Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;->lstStyle:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    return-void
.end method

.method public addParagraph(Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;)V
    .locals 1
    .param p1, "para"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;->paraLst:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32
    return-void
.end method

.method public clearParagraphList()V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;->paraLst:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 40
    return-void
.end method

.method public getBodyProp()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;->bodyPr:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    return-object v0
.end method

.method public getLstStyle()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;->lstStyle:Ljava/util/Map;

    return-object v0
.end method

.method public getParagraphLst()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;->paraLst:Ljava/util/List;

    return-object v0
.end method

.method public setBodyProp(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;)V
    .locals 0
    .param p1, "bodyPr"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;->bodyPr:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    .line 52
    return-void
.end method
