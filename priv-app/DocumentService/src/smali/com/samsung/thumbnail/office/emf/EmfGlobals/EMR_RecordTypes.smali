.class public abstract Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_RecordTypes;
.super Ljava/lang/Object;
.source "EMR_RecordTypes.java"


# static fields
.field public static final EMR_ABORTPATH:I = 0x44

.field public static final EMR_ALPHABLEND:I = 0x72

.field public static final EMR_ANGLEARC:I = 0x29

.field public static final EMR_ARC:I = 0x2d

.field public static final EMR_ARCTO:I = 0x37

.field public static final EMR_BEGINPATH:I = 0x3b

.field public static final EMR_BITBLT:I = 0x4c

.field public static final EMR_CHORD:I = 0x2e

.field public static final EMR_CLOSEFIGURE:I = 0x3d

.field public static final EMR_COLORCORRECTPALETTE:I = 0x6f

.field public static final EMR_COLORMATCHTOTARGETW:I = 0x79

.field public static final EMR_COMMENT:I = 0x46

.field public static final EMR_CREATEBRUSHINDIRECT:I = 0x27

.field public static final EMR_CREATECOLORSPACE:I = 0x63

.field public static final EMR_CREATECOLORSPACEW:I = 0x7a

.field public static final EMR_CREATEDIBPATTERNBRUSHPT:I = 0x5e

.field public static final EMR_CREATEMONOBRUSH:I = 0x5d

.field public static final EMR_CREATEPALETTE:I = 0x31

.field public static final EMR_CREATEPEN:I = 0x26

.field public static final EMR_DELETECOLORSPACE:I = 0x65

.field public static final EMR_DELETEOBJECT:I = 0x28

.field public static final EMR_DRAWESCAPE:I = 0x69

.field public static final EMR_ELLIPSE:I = 0x2a

.field public static final EMR_ENDPATH:I = 0x3c

.field public static final EMR_EOF:I = 0xe

.field public static final EMR_EXCLUDECLIPRECT:I = 0x1d

.field public static final EMR_EXTCREATEFONTINDIRECTW:I = 0x52

.field public static final EMR_EXTCREATEPEN:I = 0x5f

.field public static final EMR_EXTESCAPE:I = 0x6a

.field public static final EMR_EXTFLOODFILL:I = 0x35

.field public static final EMR_EXTSELECTCLIPRGN:I = 0x4b

.field public static final EMR_EXTTEXTOUTA:I = 0x53

.field public static final EMR_EXTTEXTOUTW:I = 0x54

.field public static final EMR_FILLPATH:I = 0x3e

.field public static final EMR_FILLRGN:I = 0x47

.field public static final EMR_FLATTENPATH:I = 0x41

.field public static final EMR_FORCEUFIMAPPING:I = 0x6d

.field public static final EMR_FRAMERGN:I = 0x48

.field public static final EMR_GLSBOUNDEDRECORD:I = 0x67

.field public static final EMR_GLSRECORD:I = 0x66

.field public static final EMR_GRADIENTFILL:I = 0x76

.field public static final EMR_HEADER:I = 0x1

.field public static final EMR_INTERSECTCLIPRECT:I = 0x1e

.field public static final EMR_INVERTRGN:I = 0x49

.field public static final EMR_LINETO:I = 0x36

.field public static final EMR_MASKBLT:I = 0x4e

.field public static final EMR_MODIFYWORLDTRANSFORM:I = 0x24

.field public static final EMR_MOVETOEX:I = 0x1b

.field public static final EMR_NAMEDESCAPE:I = 0x6e

.field public static final EMR_OFFSETCLIPRGN:I = 0x1a

.field public static final EMR_PAINTRGN:I = 0x4a

.field public static final EMR_PIE:I = 0x2f

.field public static final EMR_PIXELFORMAT:I = 0x68

.field public static final EMR_PLGBLT:I = 0x4f

.field public static final EMR_POLYBEZIER:I = 0x2

.field public static final EMR_POLYBEZIER16:I = 0x55

.field public static final EMR_POLYBEZIERTO:I = 0x5

.field public static final EMR_POLYBEZIERTO16:I = 0x58

.field public static final EMR_POLYDRAW:I = 0x38

.field public static final EMR_POLYDRAW16:I = 0x5c

.field public static final EMR_POLYGON:I = 0x3

.field public static final EMR_POLYGON16:I = 0x56

.field public static final EMR_POLYLINE:I = 0x4

.field public static final EMR_POLYLINE16:I = 0x57

.field public static final EMR_POLYLINETO:I = 0x6

.field public static final EMR_POLYLINETO16:I = 0x59

.field public static final EMR_POLYPOLYGON:I = 0x8

.field public static final EMR_POLYPOLYGON16:I = 0x5b

.field public static final EMR_POLYPOLYLINE:I = 0x7

.field public static final EMR_POLYPOLYLINE16:I = 0x5a

.field public static final EMR_POLYTEXTOUTA:I = 0x60

.field public static final EMR_POLYTEXTOUTW:I = 0x61

.field public static final EMR_REALIZEPALETTE:I = 0x34

.field public static final EMR_RECTANGLE:I = 0x2b

.field public static final EMR_RESIZEPALETTE:I = 0x33

.field public static final EMR_RESTOREDC:I = 0x22

.field public static final EMR_ROUNDRECT:I = 0x2c

.field public static final EMR_SAVEDC:I = 0x21

.field public static final EMR_SCALEVIEWPORTEXTEX:I = 0x1f

.field public static final EMR_SCALEWINDOWEXTEX:I = 0x20

.field public static final EMR_SELECTCLIPPATH:I = 0x43

.field public static final EMR_SELECTOBJECT:I = 0x25

.field public static final EMR_SELECTPALETTE:I = 0x30

.field public static final EMR_SETARCDIRECTION:I = 0x39

.field public static final EMR_SETBKCOLOR:I = 0x19

.field public static final EMR_SETBKMODE:I = 0x12

.field public static final EMR_SETBRUSHORGEX:I = 0xd

.field public static final EMR_SETCOLORADJUSTMENT:I = 0x17

.field public static final EMR_SETCOLORSPACE:I = 0x64

.field public static final EMR_SETDIBITSTODEVICE:I = 0x50

.field public static final EMR_SETICMMODE:I = 0x62

.field public static final EMR_SETICMPROFILEA:I = 0x70

.field public static final EMR_SETICMPROFILEW:I = 0x71

.field public static final EMR_SETLAYOUT:I = 0x73

.field public static final EMR_SETLINKEDUFIS:I = 0x77

.field public static final EMR_SETMAPMODE:I = 0x11

.field public static final EMR_SETMAPPERFLAGS:I = 0x10

.field public static final EMR_SETMETARGN:I = 0x1c

.field public static final EMR_SETMITERLIMIT:I = 0x3a

.field public static final EMR_SETPALETTEENTRIES:I = 0x32

.field public static final EMR_SETPIXELV:I = 0xf

.field public static final EMR_SETPOLYFILLMODE:I = 0x13

.field public static final EMR_SETROP2:I = 0x14

.field public static final EMR_SETSTRETCHBLTMODE:I = 0x15

.field public static final EMR_SETTEXTALIGN:I = 0x16

.field public static final EMR_SETTEXTCOLOR:I = 0x18

.field public static final EMR_SETTEXTJUSTIFICATION:I = 0x78

.field public static final EMR_SETVIEWPORTEXTEX:I = 0xb

.field public static final EMR_SETVIEWPORTORGEX:I = 0xc

.field public static final EMR_SETWINDOWEXTEX:I = 0x9

.field public static final EMR_SETWINDOWORGEX:I = 0xa

.field public static final EMR_SETWORLDTRANSFORM:I = 0x23

.field public static final EMR_SMALLTEXTOUT:I = 0x6c

.field public static final EMR_STRETCHBLT:I = 0x4d

.field public static final EMR_STRETCHDIBITS:I = 0x51

.field public static final EMR_STROKEANDFILLPATH:I = 0x3f

.field public static final EMR_STROKEPATH:I = 0x40

.field public static final EMR_TRANSPARENTBLT:I = 0x74

.field public static final EMR_WIDENPATH:I = 0x42


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
