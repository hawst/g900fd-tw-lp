.class public Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramGradFillHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DiagramGradFillHandler.java"


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V
    .locals 6
    .param p1, "consumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;
    .param p2, "theme"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .prologue
    const/16 v5, 0x1f

    .line 17
    const-string/jumbo v4, "gradFill"

    invoke-direct {p0, v5, v4}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 19
    const/4 v4, 0x1

    new-array v3, v4, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 21
    .local v3, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler;

    const/4 v4, 0x0

    invoke-direct {v1, p1, v4, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 22
    .local v1, "gsHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLArrayHandler;

    const-string/jumbo v4, "gsLst"

    invoke-direct {v0, v5, v4, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLArrayHandler;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 24
    .local v0, "gsArrayHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLArrayHandler;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v4, "gsLst"

    invoke-direct {v2, v4, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 26
    .local v2, "gsSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v4, 0x0

    aput-object v2, v3, v4

    .line 28
    iput-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramGradFillHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 29
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 40
    return-void
.end method
