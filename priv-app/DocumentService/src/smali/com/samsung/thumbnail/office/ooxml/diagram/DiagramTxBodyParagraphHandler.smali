.class public Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyParagraphHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DiagramTxBodyParagraphHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;


# instance fields
.field private para:Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

.field private paraList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p1, "paraList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;>;"
    const/16 v0, 0x1f

    const-string/jumbo v1, "p"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 32
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyParagraphHandler;->paraList:Ljava/util/ArrayList;

    .line 33
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 72
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyParagraphHandler;->paraList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyParagraphHandler;->para:Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    return-void
.end method

.method protected init()V
    .locals 11

    .prologue
    .line 36
    const/4 v9, 0x4

    new-array v6, v9, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 38
    .local v6, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextRunHandler;

    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyParagraphHandler;->para:Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-direct {v8, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextRunHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;)V

    .line 39
    .local v8, "txtRunHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextRunHandler;
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v9, "r"

    invoke-direct {v7, v9, v8}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 41
    .local v7, "textRunSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v9, 0x0

    aput-object v7, v6, v9

    .line 43
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;

    const-string/jumbo v9, "pPr"

    const/4 v10, 0x0

    invoke-direct {v4, v9, v10, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;)V

    .line 45
    .local v4, "paraPropHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v9, "pPr"

    invoke-direct {v5, v9, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 47
    .local v5, "paraPropSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v9, 0x1

    aput-object v5, v6, v9

    .line 49
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextLineBreakHandler;

    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyParagraphHandler;->para:Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-direct {v0, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextLineBreakHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;)V

    .line 50
    .local v0, "breakHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextLineBreakHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v9, "br"

    invoke-direct {v1, v9, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 52
    .local v1, "breakPropSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v9, 0x2

    aput-object v1, v6, v9

    .line 54
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FieldHandler;

    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyParagraphHandler;->para:Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-direct {v2, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FieldHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;)V

    .line 55
    .local v2, "fldHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FieldHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v9, "fld"

    invoke-direct {v3, v9, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 57
    .local v3, "fldSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v9, 0x3

    aput-object v3, v6, v9

    .line 59
    iput-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyParagraphHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 60
    return-void
.end method

.method public setPropertiesForLvl(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 2
    .param p1, "level"    # Ljava/lang/String;
    .param p2, "paraProps"    # Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;
    .param p3, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyParagraphHandler;->para:Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyParagraphHandler;->para:Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->setParaProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V

    .line 82
    :cond_0
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 65
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 66
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyParagraphHandler;->para:Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    .line 67
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxBodyParagraphHandler;->init()V

    .line 68
    return-void
.end method
