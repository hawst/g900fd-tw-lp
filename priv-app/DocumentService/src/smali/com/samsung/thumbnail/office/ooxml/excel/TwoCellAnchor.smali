.class public Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;
.super Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;
.source "TwoCellAnchor.java"


# instance fields
.field private toCol:I

.field private toColOff:J

.field private toRow:I

.field private toRowOff:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;-><init>()V

    return-void
.end method


# virtual methods
.method public getToCol()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;->toCol:I

    return v0
.end method

.method public getToColOff()J
    .locals 2

    .prologue
    .line 40
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;->toColOff:J

    return-wide v0
.end method

.method public getToRow()I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;->toRow:I

    return v0
.end method

.method public getToRowOff()J
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;->toRowOff:J

    return-wide v0
.end method

.method public setToCol(I)V
    .locals 0
    .param p1, "toCol"    # I

    .prologue
    .line 20
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;->toCol:I

    .line 21
    return-void
.end method

.method public setToColOff(J)V
    .locals 1
    .param p1, "toColOff"    # J

    .prologue
    .line 36
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;->toColOff:J

    .line 37
    return-void
.end method

.method public setToRow(I)V
    .locals 0
    .param p1, "toRow"    # I

    .prologue
    .line 28
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;->toRow:I

    .line 29
    return-void
.end method

.method public setToRowOff(J)V
    .locals 1
    .param p1, "toRowOff"    # J

    .prologue
    .line 44
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;->toRowOff:J

    .line 45
    return-void
.end method
