.class public Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;
.super Ljava/lang/Object;
.source "XSSFExcelWorkbook.java"


# instance fields
.field private exshtid:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private exshtidList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private exshtnameList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private shtcount:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;->exshtid:Ljava/util/ArrayList;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;->exshtnameList:Ljava/util/ArrayList;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;->exshtidList:Ljava/util/ArrayList;

    .line 66
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Landroid/content/Context;)V
    .locals 1
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "mContext"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;->exshtid:Ljava/util/ArrayList;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;->exshtnameList:Ljava/util/ArrayList;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;->exshtidList:Ljava/util/ArrayList;

    .line 61
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;->onWorkbookRead(Ljava/io/InputStream;)V

    .line 62
    return-void
.end method

.method private parseExcel(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "is"    # Ljava/io/InputStream;

    .prologue
    .line 72
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelWorkbookParser;

    invoke-direct {v0, p0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelWorkbookParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;)V

    .line 73
    .local v0, "excelParser":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelWorkbookParser;
    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelWorkbookParser;->parse(Ljava/io/InputStream;)V

    .line 74
    return-void
.end method


# virtual methods
.method public addExSheetIrdList(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;->exshtid:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    return-void
.end method

.method public addExSheetNamelist(Ljava/lang/String;)V
    .locals 1
    .param p1, "exshtnameList"    # Ljava/lang/String;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;->exshtnameList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 102
    return-void
.end method

.method public addExSheetcount(I)V
    .locals 0
    .param p1, "sheetcount"    # I

    .prologue
    .line 117
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;->shtcount:I

    .line 118
    return-void
.end method

.method public addExSheetidList(Ljava/lang/String;)V
    .locals 1
    .param p1, "exshtidList"    # Ljava/lang/String;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;->exshtidList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    return-void
.end method

.method public getExSheetIrdList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;->exshtid:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExSheetNamelist()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;->exshtnameList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExSheetcount()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;->shtcount:I

    return v0
.end method

.method public getExSheetidList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;->exshtidList:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected onWorkbookRead(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;->parseExcel(Ljava/io/InputStream;)V

    .line 86
    return-void
.end method
