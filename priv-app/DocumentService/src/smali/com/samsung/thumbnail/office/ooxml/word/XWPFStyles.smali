.class public Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;
.super Lorg/apache/poi/POIXMLDocumentPart;
.source "XWPFStyles.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles$1;
    }
.end annotation


# static fields
.field public static BULLET:I = 0x0

.field public static final COLORRGBLENGTH:I = 0x6

.field public static DEFAULT:I


# instance fields
.field private defCharProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

.field private defParaProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

.field private defParaStyle:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

.field private defTableStyle:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

.field private excelfontsizechk:Z

.field private isPpt:Z

.field private lHOnlyForBody:Z

.field private styles:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const/16 v0, 0x6f

    sput v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->DEFAULT:I

    .line 55
    const/16 v0, 0x70

    sput v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->BULLET:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Lorg/apache/poi/POIXMLDocumentPart;-><init>()V

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->isPpt:Z

    .line 94
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V
    .locals 1
    .param p1, "part"    # Lorg/apache/poi/openxml4j/opc/PackagePart;
    .param p2, "rel"    # Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/POIXMLDocumentPart;-><init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->isPpt:Z

    .line 84
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->onDocumentRead()V

    .line 85
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "isCustomView"    # Z

    .prologue
    .line 96
    invoke-direct {p0}, Lorg/apache/poi/POIXMLDocumentPart;-><init>()V

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->isPpt:Z

    .line 99
    return-void
.end method


# virtual methods
.method public addMargin(Ljava/lang/StringBuilder;)V
    .locals 0
    .param p1, "style"    # Ljava/lang/StringBuilder;

    .prologue
    .line 340
    return-void
.end method

.method public addStyle(Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;)V
    .locals 2
    .param p1, "style"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    .prologue
    .line 991
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->styles:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getStyleId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 992
    return-void
.end method

.method public addStyleProp(Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;)Ljava/lang/String;
    .locals 8
    .param p1, "style"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    .prologue
    .line 239
    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x100

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 240
    .local v5, "value":Ljava/lang/StringBuilder;
    iget-object v4, p1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->styleType:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    .line 241
    .local v4, "type":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;
    if-nez v4, :cond_0

    .line 242
    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->TABLE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    .line 244
    :cond_0
    const/4 v1, 0x0

    .line 245
    .local v1, "paraProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    const/4 v0, 0x0

    .line 246
    .local v0, "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    const/4 v3, 0x0

    .line 247
    .local v3, "tblProp":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;
    const/4 v2, 0x0

    .line 251
    .local v2, "tblCellProp":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;
    sget-object v6, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$XWPFStyle$EStyleType:[I

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 299
    :goto_0
    :pswitch_0
    if-eqz v0, :cond_1

    .line 300
    invoke-virtual {p0, v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->createCharacterStyle(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Ljava/lang/StringBuilder;)Ljava/lang/String;

    .line 303
    :cond_1
    if-eqz v1, :cond_2

    .line 304
    invoke-virtual {p0, v1, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->createParagraphStyle(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;Ljava/lang/StringBuilder;)Ljava/lang/String;

    .line 307
    :cond_2
    if-eqz v3, :cond_3

    .line 308
    invoke-virtual {p0, v3, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->createTableStyle(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;Ljava/lang/StringBuilder;)Ljava/lang/String;

    .line 311
    :cond_3
    if-eqz v2, :cond_4

    .line 312
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getShade()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getShade()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_4

    .line 321
    :cond_4
    invoke-virtual {p0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->addMargin(Ljava/lang/StringBuilder;)V

    .line 323
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 253
    :pswitch_1
    iget-object v1, p1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->paraProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    .line 254
    goto :goto_0

    .line 256
    :pswitch_2
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .line 257
    goto :goto_0

    .line 259
    :pswitch_3
    iget-object v1, p1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->paraProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    .line 260
    iget-object v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .line 261
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getTblProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    move-result-object v3

    .line 262
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCellProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    move-result-object v2

    .line 264
    goto :goto_0

    .line 251
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method public concatStyleAttr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 974
    const-string/jumbo v0, ""

    .line 975
    .local v0, "val":Ljava/lang/String;
    invoke-virtual {v0, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 976
    invoke-virtual {v0, p2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 977
    const-string/jumbo v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 978
    return-object v0
.end method

.method public createCharacterStyle(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;Ljava/lang/StringBuilder;)Ljava/lang/String;
    .locals 3
    .param p1, "charProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .param p2, "paraShading"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;
    .param p3, "style"    # Ljava/lang/StringBuilder;

    .prologue
    .line 389
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->createCharacterStyle(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v1

    .line 391
    .local v1, "retStyle":Ljava/lang/String;
    if-eqz p1, :cond_0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    if-nez v2, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->color:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 396
    :cond_0
    if-eqz p2, :cond_1

    .line 397
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->getParaShadingColor(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;)Ljava/lang/String;

    move-result-object v0

    .line 399
    .local v0, "bgColor":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 415
    .end local v0    # "bgColor":Ljava/lang/String;
    :cond_1
    return-object v1
.end method

.method public createCharacterStyle(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Ljava/lang/StringBuilder;)Ljava/lang/String;
    .locals 12
    .param p1, "charProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .param p2, "style"    # Ljava/lang/StringBuilder;

    .prologue
    .line 445
    if-nez p1, :cond_0

    .line 446
    const-string/jumbo v10, ""

    .line 832
    :goto_0
    return-object v10

    .line 449
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getBold()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 455
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getItalic()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 478
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v1

    .line 479
    .local v1, "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    if-eqz v1, :cond_3

    .line 480
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_15

    .line 511
    :cond_3
    :goto_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v10

    if-lez v10, :cond_16

    iget-boolean v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->excelfontsizechk:Z

    if-nez v10, :cond_16

    iget-boolean v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->isPpt:Z

    if-nez v10, :cond_16

    .line 512
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v10

    int-to-float v10, v10

    const/high16 v11, 0x40000000    # 2.0f

    div-float v6, v10, v11

    .line 515
    .local v6, "size":F
    const/high16 v10, 0x42ac0000    # 86.0f

    mul-float/2addr v10, v6

    const/high16 v11, 0x42c80000    # 100.0f

    div-float v6, v10, v11

    .line 535
    .end local v6    # "size":F
    :cond_4
    :goto_2
    iget-boolean v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->excelfontsizechk:Z

    if-eqz v10, :cond_5

    iget-boolean v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->lHOnlyForBody:Z

    if-eqz v10, :cond_5

    .line 537
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v10

    int-to-float v6, v10

    .line 539
    .restart local v6    # "size":F
    const/4 v10, 0x0

    cmpl-float v10, v6, v10

    if-lez v10, :cond_17

    .line 563
    .end local v6    # "size":F
    :cond_5
    :goto_3
    iget-boolean v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->isPpt:Z

    if-eqz v10, :cond_6

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getIsBaseline()Z

    move-result v10

    if-nez v10, :cond_6

    .line 564
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v10

    int-to-float v10, v10

    const/high16 v11, 0x40000000    # 2.0f

    div-float v6, v10, v11

    .line 565
    .restart local v6    # "size":F
    const/high16 v10, 0x42ac0000    # 86.0f

    mul-float/2addr v10, v6

    const/high16 v11, 0x42c80000    # 100.0f

    div-float/2addr v10, v11

    .line 573
    .end local v6    # "size":F
    :cond_6
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getIsBaseline()Z

    move-result v10

    if-eqz v10, :cond_7

    .line 574
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getBaseline()I

    move-result v10

    if-eqz v10, :cond_19

    .line 575
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getBaseline()I

    move-result v0

    .line 577
    .local v0, "baselineVal":I
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v2

    .line 578
    .local v2, "fontSizeVal":I
    mul-int v10, v0, v2

    div-int/lit8 v4, v10, 0x64

    .line 579
    .local v4, "prcntBaseFont":I
    if-lez v4, :cond_18

    .line 609
    .end local v0    # "baselineVal":I
    .end local v2    # "fontSizeVal":I
    .end local v4    # "prcntBaseFont":I
    :cond_7
    :goto_4
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getHighlightColor()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_8

    .line 616
    :cond_8
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getShadeColor()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    move-result-object v5

    .line 617
    .local v5, "shade":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;
    if-eqz v5, :cond_9

    .line 618
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->getFill()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_9

    .line 640
    :cond_9
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getUnderlineProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    move-result-object v9

    .line 641
    .local v9, "underline":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;
    const/4 v7, 0x0

    .line 642
    .local v7, "textDec":Ljava/lang/String;
    if-eqz v9, :cond_1a

    .line 643
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->createUnderlineStyle()Ljava/lang/String;

    move-result-object v7

    .line 662
    :cond_a
    :goto_5
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getStrike()Z

    move-result v10

    if-nez v10, :cond_b

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getDStrike()Z

    move-result v10

    if-eqz v10, :cond_1b

    .line 701
    :cond_b
    :goto_6
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getSmallCaps()Z

    move-result v10

    if-eqz v10, :cond_c

    .line 707
    :cond_c
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getCaps()Z

    move-result v10

    if-eqz v10, :cond_d

    .line 721
    :cond_d
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getVertAlign()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-result-object v10

    if-eqz v10, :cond_e

    .line 755
    :cond_e
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getOutline()Z

    move-result v10

    if-eqz v10, :cond_f

    .line 770
    :cond_f
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getShadow()Z

    move-result v10

    if-eqz v10, :cond_10

    .line 784
    :cond_10
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getEmboss()Z

    move-result v10

    if-eqz v10, :cond_11

    .line 797
    :cond_11
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getEngrave()Z

    move-result v10

    if-eqz v10, :cond_12

    .line 804
    :cond_12
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getVanish()Z

    move-result v10

    if-eqz v10, :cond_13

    .line 811
    :cond_13
    iget-boolean v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->isPpt:Z

    if-eqz v10, :cond_1c

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getSpacing()F

    move-result v10

    const/4 v11, 0x0

    cmpl-float v10, v10, v11

    if-eqz v10, :cond_1c

    .line 832
    :cond_14
    :goto_7
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 485
    .end local v5    # "shade":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;
    .end local v7    # "textDec":Ljava/lang/String;
    .end local v9    # "underline":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;
    :cond_15
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getThemeColor()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_3

    .line 486
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_3

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, "0"

    invoke-virtual {v10, v11}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v10

    if-eqz v10, :cond_3

    .line 488
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getThemeColor()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->displayRgbfromThemeandtint(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v3

    .line 490
    .local v3, "output":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v10

    const v11, 0xffffff

    and-int/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    .line 492
    .local v8, "tintcolor":Ljava/lang/String;
    const-string/jumbo v10, "ffffff"

    invoke-virtual {v8, v10}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v10

    if-eqz v10, :cond_3

    .line 493
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    const/4 v11, 0x6

    if-ge v10, v11, :cond_3

    .line 494
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "0"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto/16 :goto_1

    .line 527
    .end local v3    # "output":Lorg/apache/poi/java/awt/Color;
    .end local v8    # "tintcolor":Ljava/lang/String;
    :cond_16
    iget-boolean v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->lHOnlyForBody:Z

    if-nez v10, :cond_4

    iget-boolean v10, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->isPpt:Z

    if-nez v10, :cond_4

    goto/16 :goto_2

    .line 550
    .restart local v6    # "size":F
    :cond_17
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v10

    int-to-float v10, v10

    const/high16 v11, 0x40000000    # 2.0f

    div-float v6, v10, v11

    .line 553
    const/high16 v10, 0x42ac0000    # 86.0f

    mul-float/2addr v10, v6

    const/high16 v11, 0x42c80000    # 100.0f

    div-float/2addr v10, v11

    goto/16 :goto_3

    .line 587
    .end local v6    # "size":F
    .restart local v0    # "baselineVal":I
    .restart local v2    # "fontSizeVal":I
    .restart local v4    # "prcntBaseFont":I
    :cond_18
    if-gez v4, :cond_7

    goto/16 :goto_4

    .line 599
    .end local v0    # "baselineVal":I
    .end local v2    # "fontSizeVal":I
    .end local v4    # "prcntBaseFont":I
    :cond_19
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getBaseline()I

    move-result v10

    if-nez v10, :cond_7

    .line 600
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v10

    int-to-float v10, v10

    const/high16 v11, 0x40000000    # 2.0f

    div-float v6, v10, v11

    .line 601
    .restart local v6    # "size":F
    const/high16 v10, 0x42ac0000    # 86.0f

    mul-float/2addr v10, v6

    const/high16 v11, 0x42c80000    # 100.0f

    div-float/2addr v10, v11

    goto/16 :goto_4

    .line 649
    .end local v6    # "size":F
    .restart local v5    # "shade":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;
    .restart local v7    # "textDec":Ljava/lang/String;
    .restart local v9    # "underline":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;
    :cond_1a
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getUnderline()Z

    move-result v10

    if-eqz v10, :cond_a

    goto/16 :goto_5

    .line 673
    :cond_1b
    if-eqz v7, :cond_b

    .line 674
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, ";"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 818
    :cond_1c
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getSpacing()F

    move-result v10

    const/4 v11, 0x0

    cmpl-float v10, v10, v11

    if-eqz v10, :cond_14

    goto/16 :goto_7
.end method

.method public createCharacterStyleForCanvas(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/customview/word/Run;)V
    .locals 15
    .param p1, "charProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .param p2, "textRun"    # Lcom/samsung/thumbnail/customview/word/Run;

    .prologue
    .line 1278
    if-nez p1, :cond_1

    .line 1610
    :cond_0
    :goto_0
    return-void

    .line 1282
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getBold()Z

    move-result v13

    if-eqz v13, :cond_2

    .line 1283
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/Run;->setTextBold()V

    .line 1286
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getItalic()Z

    move-result v13

    if-eqz v13, :cond_3

    .line 1287
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/Run;->setTextItalics()V

    .line 1300
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontName()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_4

    .line 1301
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getBold()Z

    move-result v14

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Lcom/samsung/thumbnail/customview/word/Run;->setTypeface(Ljava/lang/String;Z)V

    .line 1304
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    .line 1305
    .local v2, "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    if-eqz v2, :cond_6

    .line 1306
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_1a

    .line 1307
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v10

    .line 1309
    .local v10, "temp":Ljava/lang/String;
    const/4 v13, 0x0

    invoke-virtual {v10, v13}, Ljava/lang/String;->charAt(I)C

    move-result v13

    const/16 v14, 0x23

    if-ne v13, v14, :cond_19

    .line 1310
    const/4 v13, 0x1

    invoke-virtual {v10, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 1316
    :cond_5
    :goto_1
    const/16 v13, 0x10

    invoke-static {v10, v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    .line 1318
    .local v3, "colorvalue":I
    const/high16 v13, -0x1000000

    or-int/2addr v3, v13

    .line 1320
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 1350
    .end local v3    # "colorvalue":I
    .end local v10    # "temp":Ljava/lang/String;
    :cond_6
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v13

    if-lez v13, :cond_1d

    iget-boolean v13, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->excelfontsizechk:Z

    if-nez v13, :cond_1d

    iget-boolean v13, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->isPpt:Z

    if-nez v13, :cond_1d

    .line 1351
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v13

    int-to-float v9, v13

    .line 1354
    .local v9, "size":F
    const/high16 v13, 0x42ac0000    # 86.0f

    mul-float/2addr v13, v9

    const/high16 v14, 0x42c80000    # 100.0f

    div-float v9, v13, v14

    .line 1356
    float-to-int v13, v9

    int-to-float v13, v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 1373
    .end local v9    # "size":F
    :cond_7
    :goto_3
    iget-boolean v13, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->excelfontsizechk:Z

    if-eqz v13, :cond_8

    iget-boolean v13, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->lHOnlyForBody:Z

    if-eqz v13, :cond_8

    .line 1375
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v13

    int-to-float v9, v13

    .line 1378
    .restart local v9    # "size":F
    const/4 v13, 0x0

    cmpl-float v13, v9, v13

    if-lez v13, :cond_1e

    .line 1379
    float-to-int v13, v9

    int-to-float v13, v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 1396
    .end local v9    # "size":F
    :cond_8
    :goto_4
    iget-boolean v13, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->isPpt:Z

    if-eqz v13, :cond_9

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getIsBaseline()Z

    move-result v13

    if-nez v13, :cond_9

    .line 1397
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v13

    int-to-float v13, v13

    const/high16 v14, 0x40000000    # 2.0f

    div-float v9, v13, v14

    .line 1398
    .restart local v9    # "size":F
    const/high16 v13, 0x42ac0000    # 86.0f

    mul-float/2addr v13, v9

    const/high16 v14, 0x42c80000    # 100.0f

    div-float v9, v13, v14

    .line 1400
    float-to-int v13, v9

    int-to-float v13, v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 1404
    .end local v9    # "size":F
    :cond_9
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getIsBaseline()Z

    move-result v13

    if-eqz v13, :cond_a

    .line 1405
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getBaseline()I

    move-result v13

    if-eqz v13, :cond_20

    .line 1406
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getBaseline()I

    move-result v1

    .line 1408
    .local v1, "baselineVal":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v4

    .line 1409
    .local v4, "fontSizeVal":I
    mul-int v13, v1, v4

    div-int/lit8 v7, v13, 0x64

    .line 1410
    .local v7, "prcntBaseFont":I
    if-lez v7, :cond_1f

    .line 1437
    .end local v1    # "baselineVal":I
    .end local v4    # "fontSizeVal":I
    .end local v7    # "prcntBaseFont":I
    :cond_a
    :goto_5
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getHighlightColor()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_b

    .line 1445
    :cond_b
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getShadeColor()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    move-result-object v8

    .line 1447
    .local v8, "shade":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;
    if-eqz v8, :cond_c

    .line 1448
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->getFill()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_c

    .line 1477
    :cond_c
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getUnderlineProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    move-result-object v12

    .line 1479
    .local v12, "underline":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;
    if-eqz v12, :cond_21

    .line 1486
    const/4 v13, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/samsung/thumbnail/customview/word/Run;->setUnderlineText(Z)V

    .line 1494
    :cond_d
    :goto_6
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getStrike()Z

    move-result v13

    if-nez v13, :cond_e

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getDStrike()Z

    move-result v13

    if-eqz v13, :cond_f

    .line 1505
    :cond_e
    const/4 v13, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/samsung/thumbnail/customview/word/Run;->setStrikeThruText(Z)V

    .line 1509
    :cond_f
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getSmallCaps()Z

    move-result v13

    if-eqz v13, :cond_10

    .line 1510
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/Run;->setSMALLLetterON()V

    .line 1513
    :cond_10
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getCaps()Z

    move-result v13

    if-eqz v13, :cond_11

    .line 1514
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/Run;->setCAPLetterON()V

    .line 1517
    :cond_11
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getVertAlign()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-result-object v13

    if-eqz v13, :cond_12

    .line 1543
    :cond_12
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getOutline()Z

    move-result v13

    if-eqz v13, :cond_13

    .line 1558
    :cond_13
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getShadow()Z

    move-result v13

    if-eqz v13, :cond_14

    .line 1570
    :cond_14
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getEmboss()Z

    move-result v13

    if-eqz v13, :cond_15

    .line 1583
    :cond_15
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getEngrave()Z

    move-result v13

    if-eqz v13, :cond_16

    .line 1589
    :cond_16
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getVanish()Z

    move-result v13

    if-eqz v13, :cond_17

    .line 1590
    const v13, 0xffffff

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 1595
    :cond_17
    iget-boolean v13, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->isPpt:Z

    if-eqz v13, :cond_18

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getSpacing()F

    move-result v13

    const/4 v14, 0x0

    cmpl-float v13, v13, v14

    if-nez v13, :cond_0

    .line 1600
    :cond_18
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getSpacing()F

    move-result v13

    const/4 v14, 0x0

    cmpl-float v13, v13, v14

    if-eqz v13, :cond_0

    goto/16 :goto_0

    .line 1311
    .end local v8    # "shade":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;
    .end local v12    # "underline":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;
    .restart local v10    # "temp":Ljava/lang/String;
    :cond_19
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v13

    const/4 v14, 0x6

    if-le v13, v14, :cond_5

    .line 1312
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v5

    .line 1313
    .local v5, "lenColor":I
    add-int/lit8 v13, v5, -0x6

    invoke-virtual {v10, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_1

    .line 1321
    .end local v5    # "lenColor":I
    .end local v10    # "temp":Ljava/lang/String;
    :cond_1a
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getThemeColor()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_6

    .line 1322
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_1b

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v13

    const-string/jumbo v14, "0"

    invoke-virtual {v13, v14}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v13

    if-eqz v13, :cond_1b

    .line 1324
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getThemeColor()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->displayRgbfromThemeandtint(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v6

    .line 1327
    .local v6, "output":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v11

    .line 1329
    .local v11, "tintcolor":I
    const/high16 v13, -0x1000000

    or-int/2addr v11, v13

    .line 1331
    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    goto/16 :goto_2

    .line 1334
    .end local v6    # "output":Lorg/apache/poi/java/awt/Color;
    .end local v11    # "tintcolor":I
    :cond_1b
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getThemeColor()Ljava/lang/String;

    move-result-object v10

    .line 1336
    .restart local v10    # "temp":Ljava/lang/String;
    const/4 v13, 0x0

    invoke-virtual {v10, v13}, Ljava/lang/String;->charAt(I)C

    move-result v13

    const/16 v14, 0x23

    if-ne v13, v14, :cond_1c

    .line 1337
    const/4 v13, 0x1

    invoke-virtual {v10, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 1340
    :cond_1c
    const/16 v13, 0x10

    invoke-static {v10, v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    .line 1342
    .restart local v3    # "colorvalue":I
    const/high16 v13, -0x1000000

    or-int/2addr v3, v13

    .line 1344
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    goto/16 :goto_2

    .line 1365
    .end local v3    # "colorvalue":I
    .end local v10    # "temp":Ljava/lang/String;
    :cond_1d
    iget-boolean v13, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->lHOnlyForBody:Z

    if-nez v13, :cond_7

    iget-boolean v13, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->isPpt:Z

    if-nez v13, :cond_7

    .line 1368
    const/high16 v9, 0x41000000    # 8.0f

    .line 1370
    .restart local v9    # "size":F
    float-to-int v13, v9

    int-to-float v13, v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    goto/16 :goto_3

    .line 1385
    :cond_1e
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v13

    int-to-float v13, v13

    const/high16 v14, 0x40000000    # 2.0f

    div-float v9, v13, v14

    .line 1388
    const/high16 v13, 0x42ac0000    # 86.0f

    mul-float/2addr v13, v9

    const/high16 v14, 0x42c80000    # 100.0f

    div-float v9, v13, v14

    .line 1390
    float-to-int v13, v9

    int-to-float v13, v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    goto/16 :goto_4

    .line 1418
    .end local v9    # "size":F
    .restart local v1    # "baselineVal":I
    .restart local v4    # "fontSizeVal":I
    .restart local v7    # "prcntBaseFont":I
    :cond_1f
    if-gez v7, :cond_a

    goto/16 :goto_5

    .line 1428
    .end local v1    # "baselineVal":I
    .end local v4    # "fontSizeVal":I
    .end local v7    # "prcntBaseFont":I
    :cond_20
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getBaseline()I

    move-result v13

    if-nez v13, :cond_a

    .line 1429
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v13

    int-to-float v13, v13

    const/high16 v14, 0x40000000    # 2.0f

    div-float v9, v13, v14

    .line 1430
    .restart local v9    # "size":F
    const/high16 v13, 0x42ac0000    # 86.0f

    mul-float/2addr v13, v9

    const/high16 v14, 0x42c80000    # 100.0f

    div-float v9, v13, v14

    .line 1432
    float-to-int v13, v9

    int-to-float v13, v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    goto/16 :goto_5

    .line 1488
    .end local v9    # "size":F
    .restart local v8    # "shade":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;
    .restart local v12    # "underline":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;
    :cond_21
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getUnderline()Z

    move-result v13

    if-eqz v13, :cond_d

    .line 1490
    const/4 v13, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/samsung/thumbnail/customview/word/Run;->setUnderlineText(Z)V

    goto/16 :goto_6
.end method

.method public createParagraphStyle(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;Ljava/lang/StringBuilder;)Ljava/lang/String;
    .locals 6
    .param p1, "paraProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    .param p2, "val"    # Ljava/lang/StringBuilder;

    .prologue
    const/4 v5, -0x1

    .line 850
    if-nez p1, :cond_0

    .line 851
    const-string/jumbo v4, ""

    .line 970
    :goto_0
    return-object v4

    .line 854
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getJustify()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 867
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v3

    .line 868
    .local v3, "spaceProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;
    if-eqz v3, :cond_3

    .line 879
    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getAfter()I

    move-result v4

    if-eq v4, v5, :cond_2

    .line 887
    :cond_2
    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getBefore()I

    move-result v4

    if-eq v4, v5, :cond_3

    .line 897
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v1

    .line 898
    .local v1, "bordersProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;
    const/4 v0, 0x0

    .line 899
    .local v0, "border":Z
    if-eqz v1, :cond_4

    .line 900
    invoke-virtual {v1, p2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->createBorderStyle(Ljava/lang/StringBuilder;)V

    .line 901
    const/4 v0, 0x1

    .line 904
    :cond_4
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v2

    .line 905
    .local v2, "indProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;
    if-eqz v2, :cond_8

    .line 906
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getLeftInd()I

    move-result v4

    if-eqz v4, :cond_5

    .line 919
    :cond_5
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getRightInd()I

    move-result v4

    if-eqz v4, :cond_6

    .line 927
    :cond_6
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getFirstInd()I

    move-result v4

    if-eqz v4, :cond_7

    .line 935
    :cond_7
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getHangingInd()I

    move-result v4

    if-eqz v4, :cond_8

    .line 947
    :cond_8
    if-eqz v0, :cond_9

    .line 953
    const/4 v0, 0x0

    .line 970
    :cond_9
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public createTableStyle(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;Ljava/lang/StringBuilder;)Ljava/lang/String;
    .locals 1
    .param p1, "tableProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;
    .param p2, "tableStyle"    # Ljava/lang/StringBuilder;

    .prologue
    .line 420
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 421
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->createBorderStyle(Ljava/lang/StringBuilder;)V

    .line 424
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getShade()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 428
    :cond_1
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDefParaStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->defParaStyle:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    return-object v0
.end method

.method public getDefTableStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->defTableStyle:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    return-object v0
.end method

.method getParaShadingColor(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;)Ljava/lang/String;
    .locals 6
    .param p1, "paraShading"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    .prologue
    .line 346
    const-string/jumbo v0, "000000"

    .line 347
    .local v0, "autoColor":Ljava/lang/String;
    const-string/jumbo v3, "ffffff"

    .line 348
    .local v3, "defaultFill":Ljava/lang/String;
    const/4 v1, 0x0

    .line 359
    .local v1, "bgColor":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->getPattern()Ljava/lang/String;

    move-result-object v4

    .line 360
    .local v4, "wVal":Ljava/lang/String;
    const-string/jumbo v5, "clear"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 362
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->getFill()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 363
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->getFill()Ljava/lang/String;

    move-result-object v1

    .line 384
    :cond_0
    :goto_0
    return-object v1

    .line 365
    :cond_1
    move-object v1, v3

    goto :goto_0

    .line 368
    :cond_2
    const-string/jumbo v5, "solid"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 370
    const/4 v2, 0x0

    .line 371
    .local v2, "color":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 372
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v5

    iget-object v2, v5, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->color:Ljava/lang/String;

    .line 374
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->isAuto()Z

    move-result v5

    if-nez v5, :cond_5

    :cond_4
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 377
    :cond_5
    move-object v1, v0

    goto :goto_0

    .line 379
    :cond_6
    move-object v1, v2

    goto :goto_0
.end method

.method public getStyle(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    .locals 1
    .param p1, "styleID"    # Ljava/lang/String;

    .prologue
    .line 1133
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->styles:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    return-object v0
.end method

.method public getStyles()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 995
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->styles:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method protected onDocumentRead()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->styles:Ljava/util/LinkedHashMap;

    .line 139
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleParser;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;)V

    .line 141
    .local v1, "styleParser":Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleParser;
    const/4 v0, 0x0

    .line 143
    .local v0, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 144
    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleParser;->parse(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    if-eqz v0, :cond_0

    .line 148
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 161
    :cond_0
    return-void

    .line 147
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_1

    .line 148
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    :cond_1
    throw v2
.end method

.method public setCurrStyleLineHeight(Ljava/lang/String;)V
    .locals 0
    .param p1, "currStyleLineHight"    # Ljava/lang/String;

    .prologue
    .line 129
    return-void
.end method

.method public setDefaultCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V
    .locals 0
    .param p1, "defCharProperties"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->defCharProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .line 109
    return-void
.end method

.method public setDefaultParaProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V
    .locals 0
    .param p1, "defParaProperties"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->defParaProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    .line 104
    return-void
.end method

.method public setIsPPt(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 844
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->isPpt:Z

    .line 845
    return-void
.end method

.method public setexcelfontsizechk(Z)V
    .locals 1
    .param p1, "val"    # Z

    .prologue
    .line 839
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->lHOnlyForBody:Z

    .line 840
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->excelfontsizechk:Z

    .line 841
    return-void
.end method

.method public updateStyleTree()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1001
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->styles:Ljava/util/LinkedHashMap;

    invoke-virtual {v6}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v5

    .line 1002
    .local v5, "styleCollection":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;>;"
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1007
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1008
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    .line 1010
    .local v4, "style":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    if-eqz v4, :cond_0

    .line 1016
    :goto_0
    iget-object v6, v4, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->basedOn:Ljava/lang/String;

    if-eqz v6, :cond_0

    .line 1017
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->styles:Ljava/util/LinkedHashMap;

    iget-object v7, v4, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->basedOn:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    .line 1020
    .local v3, "parentStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    iput-object v3, v4, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->parentStyle:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    .line 1022
    if-eqz v3, :cond_0

    .line 1026
    move-object v4, v3

    .line 1027
    goto :goto_0

    .line 1029
    .end local v3    # "parentStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    .end local v4    # "style":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    :cond_1
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1031
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;>;"
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1032
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    .line 1034
    .restart local v4    # "style":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    if-eqz v4, :cond_2

    .line 1038
    sget-object v6, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$XWPFStyle$EStyleType:[I

    iget-object v7, v4, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->styleType:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    .line 1042
    :pswitch_1
    iget-boolean v6, v4, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->isDefault:Z

    if-eqz v6, :cond_3

    .line 1044
    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->defParaStyle:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    .line 1046
    :cond_3
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->defCharProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->defParaProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v4, v6, v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->mergeWithParent(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V

    goto :goto_1

    .line 1056
    :pswitch_2
    iget-boolean v6, v4, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->isDefault:Z

    if-eqz v6, :cond_4

    .line 1058
    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->defTableStyle:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    .line 1059
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->getDefParaStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v0

    .line 1060
    .local v0, "defParaStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    iget-object v6, v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    iget-object v7, v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->paraProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v4, v6, v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->mergeWithParent(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V

    goto :goto_1

    .line 1063
    .end local v0    # "defParaStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    :cond_4
    invoke-virtual {v4, v8, v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->mergeWithParent(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V

    goto :goto_1

    .line 1079
    .end local v4    # "style":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    :cond_5
    return-void

    .line 1038
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public writeStyles()V
    .locals 10

    .prologue
    const/16 v9, 0xc

    const/4 v8, 0x1

    .line 165
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->styles:Ljava/util/LinkedHashMap;

    invoke-virtual {v6}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v5

    .line 166
    .local v5, "styleCollection":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;>;"
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 168
    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;>;"
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 169
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    .line 174
    .local v4, "style":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    invoke-virtual {p0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->addStyleProp(Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;)Ljava/lang/String;

    .line 178
    iget-object v6, v4, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->styleType:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->TABLE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 179
    new-array v1, v9, [Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    const/4 v6, 0x0

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->FIRST_ROW:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v7, v1, v6

    sget-object v6, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->LAST_ROW:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v6, v1, v8

    const/4 v6, 0x2

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->FIRST_COL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v7, v1, v6

    const/4 v6, 0x3

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->LAST_COL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v7, v1, v6

    const/4 v6, 0x4

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->BAND1_VERT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v7, v1, v6

    const/4 v6, 0x5

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->BAND2_VERT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v7, v1, v6

    const/4 v6, 0x6

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->BAND1_HORZ:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v7, v1, v6

    const/4 v6, 0x7

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->BAND2_HORZ:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v7, v1, v6

    const/16 v6, 0x8

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->NE_CELL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v7, v1, v6

    const/16 v6, 0x9

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->NW_CELL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v7, v1, v6

    const/16 v6, 0xa

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->SE_CELL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v7, v1, v6

    const/16 v6, 0xb

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->SW_CELL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v7, v1, v6

    .line 193
    .local v1, "eTblStyleOverrideTypes":[Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v9, :cond_0

    .line 194
    aget-object v6, v1, v2

    invoke-virtual {v4, v6}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getTblStyleProp(Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 195
    aget-object v6, v1, v2

    invoke-virtual {v4, v6}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getTblStyleProp(Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v0

    .line 197
    .local v0, "currentStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->addStyleProp(Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;)Ljava/lang/String;

    .line 193
    .end local v0    # "currentStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 235
    .end local v1    # "eTblStyleOverrideTypes":[Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;
    .end local v2    # "i":I
    .end local v4    # "style":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    :cond_2
    iput-boolean v8, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->lHOnlyForBody:Z

    .line 236
    return-void
.end method
