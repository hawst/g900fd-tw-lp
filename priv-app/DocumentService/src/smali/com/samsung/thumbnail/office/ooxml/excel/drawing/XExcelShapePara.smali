.class public Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;
.super Ljava/lang/Object;
.source "XExcelShapePara.java"


# instance fields
.field private bodyProp:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

.field private paraLst:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;->paraLst:Ljava/util/List;

    .line 11
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;->bodyProp:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    .line 13
    return-void
.end method


# virtual methods
.method public addPara(Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;)V
    .locals 1
    .param p1, "para"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;->paraLst:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 21
    return-void
.end method

.method public getBodyProp()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;->bodyProp:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    return-object v0
.end method

.method public getParaList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;",
            ">;"
        }
    .end annotation

    .prologue
    .line 16
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;->paraLst:Ljava/util/List;

    return-object v0
.end method

.method public setBodyProp(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;)V
    .locals 0
    .param p1, "bpr"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;->bodyProp:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    .line 25
    return-void
.end method
