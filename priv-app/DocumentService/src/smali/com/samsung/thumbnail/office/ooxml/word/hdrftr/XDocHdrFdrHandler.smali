.class public Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XDocHdrFdrHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;
.source "XDocHdrFdrHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XDocHdrFdrHandler$IHdrFdrRef;
    }
.end annotation


# instance fields
.field private consumer:Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XDocHdrFdrHandler$IHdrFdrRef;

.field private header:Z


# direct methods
.method public constructor <init>(ZLcom/samsung/thumbnail/office/ooxml/word/hdrftr/XDocHdrFdrHandler$IHdrFdrRef;Ljava/lang/String;)V
    .locals 0
    .param p1, "header"    # Z
    .param p2, "consumer"    # Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XDocHdrFdrHandler$IHdrFdrRef;
    .param p3, "eleName"    # Ljava/lang/String;

    .prologue
    .line 15
    invoke-direct {p0, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;-><init>(Ljava/lang/String;)V

    .line 16
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XDocHdrFdrHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XDocHdrFdrHandler$IHdrFdrRef;

    .line 17
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XDocHdrFdrHandler;->header:Z

    .line 18
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 23
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 24
    const/4 v0, 0x0

    .line 25
    .local v0, "type":Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;
    const-string/jumbo v2, "type"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XDocHdrFdrHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 26
    .local v1, "val":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 27
    invoke-static {v1}, Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;

    move-result-object v0

    .line 29
    :cond_0
    const-string/jumbo v2, "id"

    const/16 v3, 0xa

    invoke-virtual {p0, p3, v2, v3, p1}, Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XDocHdrFdrHandler;->getAttrForNS(Lorg/xml/sax/Attributes;Ljava/lang/String;ILcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 31
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XDocHdrFdrHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XDocHdrFdrHandler$IHdrFdrRef;

    iget-boolean v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XDocHdrFdrHandler;->header:Z

    invoke-interface {v2, v3, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XDocHdrFdrHandler$IHdrFdrRef;->setHdrFdrRef(ZLcom/samsung/thumbnail/office/ooxml/word/hdrftr/EHdrFtrType;Ljava/lang/String;)V

    .line 32
    return-void
.end method
