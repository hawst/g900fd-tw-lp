.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocParaHandler;
.super Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPContent;
.source "XDocParaHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$IXPPrObserver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    const-string/jumbo v0, "p"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPContent;-><init>(Ljava/lang/String;)V

    .line 19
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->endPara()V

    .line 44
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPContent;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 37
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPContent;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 38
    return-void
.end method

.method protected init()V
    .locals 3

    .prologue
    .line 22
    invoke-super {p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPContent;->init()V

    .line 23
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;

    invoke-direct {v0, p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$IXPPrObserver;)V

    .line 24
    .local v0, "pprHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocParaHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "pPr"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    return-void
.end method

.method public onPPrFinished(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p1, "paraProperties"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    .param p2, "charProperties"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .param p3, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 55
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    .line 56
    .local v0, "xDocConsumer":Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;
    invoke-interface {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->setParagraphProperty(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V

    .line 57
    invoke-interface {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->setParaCharProperty(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V

    .line 58
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 30
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPContent;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 31
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->startPara()V

    .line 32
    return-void
.end method
