.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "GradFillHandler.java"


# instance fields
.field mShInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V
    .locals 10
    .param p1, "consumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;
    .param p2, "shInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "theme"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .prologue
    const/16 v9, 0x1f

    .line 24
    const-string/jumbo v8, "gradFill"

    invoke-direct {p0, v9, v8}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 26
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;->mShInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 29
    const/4 v8, 0x2

    new-array v7, v8, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 31
    .local v7, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler;

    invoke-direct {v1, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 32
    .local v1, "gsHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLArrayHandler;

    const-string/jumbo v8, "gsLst"

    invoke-direct {v0, v9, v8, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLArrayHandler;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 34
    .local v0, "gsArrayHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLArrayHandler;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v8, "gsLst"

    invoke-direct {v2, v8, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 36
    .local v2, "gsSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v8, 0x0

    aput-object v2, v7, v8

    .line 38
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    invoke-direct {v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;-><init>()V

    .line 40
    .local v3, "pathDesc":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathHandler;

    invoke-direct {v4, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V

    .line 41
    .local v4, "pathHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/OOXMLArrayHandler;

    const-string/jumbo v8, "path"

    invoke-direct {v5, v9, v8, v4}, Lcom/samsung/thumbnail/office/ooxml/OOXMLArrayHandler;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 43
    .local v5, "pathLst":Lcom/samsung/thumbnail/office/ooxml/OOXMLArrayHandler;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v8, "path"

    invoke-direct {v6, v8, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 45
    .local v6, "pathSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v8, 0x1

    aput-object v6, v7, v8

    .line 47
    iput-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 48
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 62
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 64
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->addGradColorLst()V

    .line 69
    :cond_0
    return-void
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 53
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 54
    const-string/jumbo v1, "path"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 55
    .local v0, "path":Ljava/lang/String;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;->mShInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    if-eqz v1, :cond_0

    .line 56
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;->mShInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setGradFillPathName(Ljava/lang/String;)V

    .line 58
    :cond_0
    return-void
.end method
