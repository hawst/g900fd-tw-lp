.class public Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XDocTableStylePrHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$IXPPrObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$IXRPrObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler$ITblPropObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$ITblCellPropObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler$ITblRowPropObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler$ITblStylePrObserver;
    }
.end annotation


# instance fields
.field private observer:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler$ITblStylePrObserver;

.field private style:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler$ITblStylePrObserver;)V
    .locals 1
    .param p1, "observer"    # Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler$ITblStylePrObserver;

    .prologue
    .line 35
    const-string/jumbo v0, "tblStylePr"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 36
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler;->observer:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler$ITblStylePrObserver;

    .line 37
    return-void
.end method

.method private init()V
    .locals 12

    .prologue
    .line 40
    const/4 v11, 0x5

    new-array v4, v11, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 42
    .local v4, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;

    invoke-direct {v7, p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$ITblCellPropObserver;)V

    .line 43
    .local v7, "tblTcHandler":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v11, "tcPr"

    invoke-direct {v8, v11, v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 45
    .local v8, "tblTcSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v11, 0x0

    aput-object v8, v4, v11

    .line 47
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler;

    invoke-direct {v9, p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler$ITblRowPropObserver;)V

    .line 48
    .local v9, "tblTrHandler":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler;
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v11, "trPr"

    invoke-direct {v10, v11, v9}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 50
    .local v10, "tblTrSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v11, 0x1

    aput-object v10, v4, v11

    .line 52
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;

    invoke-direct {v0, p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler$IXPPrObserver;)V

    .line 53
    .local v0, "pPrHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPPrHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v11, "pPr"

    invoke-direct {v1, v11, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 55
    .local v1, "pPrSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v11, 0x2

    aput-object v1, v4, v11

    .line 57
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;

    invoke-direct {v2, p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler$IXRPrObserver;)V

    .line 58
    .local v2, "rPrHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocRPRHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v11, "rPr"

    invoke-direct {v3, v11, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 60
    .local v3, "rPrSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v11, 0x3

    aput-object v3, v4, v11

    .line 62
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;

    invoke-direct {v5, p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler$ITblPropObserver;)V

    .line 63
    .local v5, "tablePr":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v11, "tblPr"

    invoke-direct {v6, v11, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 65
    .local v6, "tblPropSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v11, 0x4

    aput-object v6, v4, v11

    .line 67
    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 68
    return-void
.end method


# virtual methods
.method public onPPrFinished(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p1, "paraProperties"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    .param p2, "charProperties"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .param p3, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler;->style:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->setParaProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V

    .line 102
    return-void
.end method

.method public onRPrFinsihed(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p1, "charProperties"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .param p2, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler;->style:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->setCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V

    .line 96
    return-void
.end method

.method public setTblCellProp(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "cellProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    .prologue
    .line 107
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler;->style:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->setTblCellProp(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;)V

    .line 108
    return-void
.end method

.method public setTblProp(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "tblProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler;->style:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->setTblProp(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;)V

    .line 90
    return-void
.end method

.method public setTblRowProp(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "rowProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler;->style:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->setTblRowProp(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;)V

    .line 114
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 73
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 74
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler;->init()V

    .line 76
    const-string/jumbo v2, "type"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 77
    .local v1, "type":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 85
    :goto_0
    return-void

    .line 81
    :cond_0
    invoke-static {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    move-result-object v0

    .line 83
    .local v0, "tblStyleType":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    invoke-direct {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;)V

    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler;->style:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    .line 84
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler;->observer:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler$ITblStylePrObserver;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler;->style:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    invoke-interface {v2, p1, v0, v3}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableStylePrHandler$ITblStylePrObserver;->setTblStylePr(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;)V

    goto :goto_0
.end method
