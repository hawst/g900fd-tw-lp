.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocAlternateContentHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XDocAlternateContentHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 14
    const/4 v0, 0x3

    const-string/jumbo v1, "AlternateContent"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 15
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    .line 18
    const/4 v3, 0x1

    new-array v2, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 20
    .local v2, "seqDescriptors":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocFallBackHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocFallBackHandler;-><init>()V

    .line 21
    .local v1, "fallBackHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocFallBackHandler;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/document/MCMLSequenceDescriptor;

    const-string/jumbo v3, "Fallback"

    invoke-direct {v0, v3, v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/MCMLSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 23
    .local v0, "descriptor":Lcom/samsung/thumbnail/office/ooxml/word/document/MCMLSequenceDescriptor;
    const/4 v3, 0x0

    aput-object v0, v2, v3

    .line 25
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocAlternateContentHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 26
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 44
    return-void
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 38
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 39
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 31
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 32
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocAlternateContentHandler;->init()V

    .line 33
    return-void
.end method
