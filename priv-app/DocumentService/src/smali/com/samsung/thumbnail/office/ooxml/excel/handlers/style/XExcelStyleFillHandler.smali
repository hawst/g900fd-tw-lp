.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFillHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelStyleFillHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 11
    const-string/jumbo v2, "fill"

    invoke-direct {p0, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 13
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/PatternFillHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/PatternFillHandler;-><init>()V

    .line 14
    .local v1, "patternfillhandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/PatternFillHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFillHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "patternFill"

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillHandler;-><init>()V

    .line 18
    .local v0, "gradientFillHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/GradientFillHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFillHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "gradientFill"

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    return-void
.end method
