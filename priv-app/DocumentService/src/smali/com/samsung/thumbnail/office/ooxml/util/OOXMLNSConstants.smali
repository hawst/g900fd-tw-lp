.class public Lcom/samsung/thumbnail/office/ooxml/util/OOXMLNSConstants;
.super Ljava/lang/Object;
.source "OOXMLNSConstants.java"


# static fields
.field public static final CHART:Ljava/lang/String; = "http://schemas.openxmlformats.org/drawingml/2006/chart"

.field public static final CHART_ID:I = 0x23

.field public static final CHART_PREFIX:Ljava/lang/String; = "c"

.field public static final DIAGRAM:Ljava/lang/String; = "http://schemas.openxmlformats.org/drawingml/2006/diagram"

.field public static final DIAGRAM_ID:I = 0xca

.field public static final DIAGRAM_PREFIX:Ljava/lang/String; = "dgm"

.field public static final DIAGRAM_SHAPE:Ljava/lang/String; = "http://schemas.microsoft.com/office/drawing/2008/diagram"

.field public static final DIAGRAM_SHAPE_ID:I = 0xcb

.field public static final DIAGRAM_SHAPE_PREFIX:Ljava/lang/String; = "dsp"

.field public static final DML:Ljava/lang/String; = "http://schemas.openxmlformats.org/drawingml/2006/main"

.field public static final DML_ID:I = 0x1f

.field public static final DML_PICT:Ljava/lang/String; = "http://schemas.openxmlformats.org/drawingml/2006/picture"

.field public static final DML_PICT_ID:I = 0x20

.field public static final DML_PICT_PREFIX:Ljava/lang/String; = "pic"

.field public static final DML_PREFIX:Ljava/lang/String; = "a"

.field public static final EXCEL_DML_MAIN:Ljava/lang/String; = "http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing"

.field public static final EXCEL_DML_MAIN_ID:I = 0xc9

.field public static final EXCEL_DML_MAIN_PREFIX:Ljava/lang/String; = "xdr"

.field public static final EXCEL_SHEET_MAIN:Ljava/lang/String; = "http://schemas.openxmlformats.org/spreadsheetml/2006/main"

.field public static final EXCEL_SHEET_MAIN_ID:I = 0xc8

.field public static final MARKUP_COMPATIBILITY:Ljava/lang/String; = "http://schemas.openxmlformats.org/markup-compatibility/2006"

.field public static final MARKUP_COMPATIBILITY_PREFIX:Ljava/lang/String; = "ve"

.field public static final MATH:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/math"

.field public static final MATH_PREFIX:Ljava/lang/String; = "m"

.field public static final MC:Ljava/lang/String; = "http://schemas.openxmlformats.org/markup-compatibility/2006"

.field public static final MC_ID:I = 0x3

.field public static final MC_PREFIX:Ljava/lang/String; = "mc"

.field public static final OFFICE:Ljava/lang/String; = "urn:schemas-microsoft-com:office:office"

.field public static final OFFICE_ID:I = 0x32

.field public static final OFFICE_PREFIX:Ljava/lang/String; = "o"

.field public static final PPTX_ML_MAIN:Ljava/lang/String; = "http://schemas.openxmlformats.org/presentationml/2006/main"

.field public static final PPTX_ML_MAIN_ID:I = 0x33

.field public static final PPTX_ML_MAIN_PREFIX:Ljava/lang/String; = "p"

.field public static final RELATIONSHIP:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships"

.field public static final RELATIONSHIP_ID:I = 0xa

.field public static final RELATIONSHIP_PREFIX:Ljava/lang/String; = "r"

.field public static final VML:Ljava/lang/String; = "urn:schemas-microsoft-com:vml"

.field public static final VML_ID:I = 0x28

.field public static final VML_PREFIX:Ljava/lang/String; = "v"

.field public static final WORD_DML:Ljava/lang/String; = "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"

.field public static final WORD_DML_ID:I = 0x1e

.field public static final WORD_DML_PREFIX:Ljava/lang/String; = "wp"

.field public static final WORD_ML:Ljava/lang/String; = "http://schemas.openxmlformats.org/wordprocessingml/2006/main"

.field public static final WORD_ML_ID:I = 0x14

.field public static final WORD_ML_OFFICE:Ljava/lang/String; = "http://schemas.microsoft.com/office/word/2006/wordml"

.field public static final WORD_ML_OFFICE_PREFIX:Ljava/lang/String; = "wne"

.field public static final WORD_ML_PREFIX:Ljava/lang/String; = "w"

.field public static final WORD_OFFICE_URN:Ljava/lang/String; = "urn:schemas-microsoft-com:office:word"

.field public static final WORD_OFFICE_URN_ID:I = 0x29

.field public static final WORD_OFFICE_URN_PREFIX:Ljava/lang/String; = "w10"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
