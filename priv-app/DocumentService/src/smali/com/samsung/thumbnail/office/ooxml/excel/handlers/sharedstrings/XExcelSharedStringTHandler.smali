.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringTHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedTagNoChildrenHandler;
.source "XExcelSharedStringTHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    const-string/jumbo v0, "t"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedTagNoChildrenHandler;-><init>(Ljava/lang/String;)V

    .line 20
    return-void
.end method


# virtual methods
.method public charactersHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;[CII)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "content"    # [C
    .param p3, "start"    # I
    .param p4, "end"    # I

    .prologue
    .line 25
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2, p3, p4}, Ljava/lang/String;-><init>([CII)V

    .line 26
    .local v0, "text":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v1

    .line 28
    .local v1, "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    iget-object v2, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tempString:Ljava/lang/String;

    invoke-virtual {p0, v2, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringTHandler;->concat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tempString:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public concat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "str1"    # Ljava/lang/String;
    .param p2, "str2"    # Ljava/lang/String;

    .prologue
    .line 32
    const-string/jumbo v0, ""

    .line 33
    .local v0, "concatstr":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 34
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 38
    :goto_0
    return-object v0

    .line 36
    :cond_0
    move-object v0, p2

    goto :goto_0
.end method
