.class public Lcom/samsung/thumbnail/office/ooxml/OOXMLSingleElementHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "OOXMLSingleElementHandler.java"


# instance fields
.field private childHandler:Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;


# direct methods
.method public constructor <init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "elementName"    # Ljava/lang/String;
    .param p3, "elementHandler"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 24
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLSingleElementHandler;->childHandler:Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    .line 25
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "elementName"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V
    .locals 0
    .param p1, "elementName"    # Ljava/lang/String;
    .param p2, "elementHandler"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 18
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLSingleElementHandler;->childHandler:Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    .line 19
    return-void
.end method


# virtual methods
.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLSingleElementHandler;->childHandler:Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    if-nez v0, :cond_0

    .line 49
    :goto_0
    return-void

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLSingleElementHandler;->childHandler:Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    invoke-virtual {v0, p2, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->checkElement(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 44
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLSingleElementHandler;->childHandler:Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    invoke-virtual {p1, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->pushHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 45
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLSingleElementHandler;->childHandler:Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    goto :goto_0

    .line 47
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->startUnknownElement()V

    goto :goto_0
.end method
