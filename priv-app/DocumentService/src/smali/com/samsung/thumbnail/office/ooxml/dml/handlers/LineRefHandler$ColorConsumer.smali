.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ColorConsumer;
.super Ljava/lang/Object;
.source "LineRefHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "ColorConsumer"
.end annotation


# instance fields
.field lineConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ILineChoiceConsumer;

.field lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ILineChoiceConsumer;)V
    .locals 1
    .param p1, "lineConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ILineChoiceConsumer;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ColorConsumer;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .line 49
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ColorConsumer;->lineConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ILineChoiceConsumer;

    .line 50
    return-void
.end method


# virtual methods
.method public consumeColor(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 2
    .param p1, "clrScheme"    # Ljava/lang/String;
    .param p2, "val"    # Ljava/lang/String;
    .param p3, "fillProp"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    .param p4, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ColorConsumer;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->setColor(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/LineRefHandler$ColorConsumer;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    invoke-direct {v1, p3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->setFillProp(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V

    .line 72
    return-void
.end method
