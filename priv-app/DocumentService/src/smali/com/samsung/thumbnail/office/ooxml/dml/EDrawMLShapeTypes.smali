.class public final enum Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;
.super Ljava/lang/Enum;
.source "EDrawMLShapeTypes.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ACCENT_BORDER_CALLOUT1:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ACCENT_BORDER_CALLOUT2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ACCENT_BORDER_CALLOUT3:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ACCENT_CALLOUT1:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ACCENT_CALLOUT2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ACCENT_CALLOUT3:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ACTION_BUTTON_BACK_PREVIOUS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ACTION_BUTTON_BEGINNING:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ACTION_BUTTON_BLANK:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ACTION_BUTTON_DOCUMENT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ACTION_BUTTON_END:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ACTION_BUTTON_FORWARD_NEXT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ACTION_BUTTON_HELP:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ACTION_BUTTON_HOME:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ACTION_BUTTON_INFORMATION:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ACTION_BUTTON_MOVIE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ACTION_BUTTON_RETURN:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ACTION_BUTTON_SOUND:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ARC:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ARCCONNECTOR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum BENT_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum BENT_CONNECTOR2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum BENT_CONNECTOR3:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum BENT_CONNECTOR4:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum BENT_CONNECTOR5:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum BENT_UP_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum BEVEL:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum BLOCK_ARC:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum BORDER_CALLOUT1:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum BORDER_CALLOUT2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum BORDER_CALLOUT3:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum BRACE_PAIR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum BRACKET_PAIR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum CALLOUT1:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum CALLOUT2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum CALLOUT3:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum CAN:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum CHART_PLUS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum CHART_STAR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum CHART_X:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum CHEVRON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum CHORD:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum CIRCULAR_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum CLOUD:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum CLOUD_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum CORNER:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum CORNER_TABS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum CUBE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum CURVED_CONNECTOR2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum CURVED_CONNECTOR3:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum CURVED_CONNECTOR4:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum CURVED_CONNECTOR5:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum CURVED_DOWN_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum CURVED_LEFT_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum CURVED_RIGHT_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum CURVED_UP_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum DECAGON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum DIAG_STRIPE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum DIAMOND:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum DODECAGON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum DONUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum DOUBLE_WAVE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum DOWN_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum DOWN_ARROW_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ELLIPSE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ELLIPSE_RIBBON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ELLIPSE_RIBBON2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_ALTERNATE_PROCESS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_COLLATE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_CONNECTOR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_DECISION:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_DELAY:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_DISPLAY:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_DOCUMENT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_EXTRACT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_INPUT_OUTPUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_INTERNAL_STORAGE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_MAGNETIC_DISK:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_MAGNETIC_DRUM:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_MAGNETIC_TAPE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_MANUAL_INPUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_MANUAL_OPERATION:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_MERGE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_MULTIDOCUMENT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_OFFLINE_STORAGE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_OFFPAGE_CONNECTOR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_ONLINE_STORAGE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_OR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_PREDEFINED_PROCESS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_PREPARATION:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_PROCESS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_PUNCHED_CARD:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_PUNCHED_TAPE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_SORT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_SUMMING_JUNCTION:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FLOW_CHART_TERMINATOR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FOLDED_CORNER:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FRAME:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum FUNNEL:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum GEAR6:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum GEAR9:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum HALF_FRAME:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum HEART:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum HEPTAGON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum HEXAGON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum HOME_PLATE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum HORIZONTAL_SCROLL:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum IRREGULAR_SEAL1:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum IRREGULAR_SEAL2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum LEFT_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum LEFT_ARROW_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum LEFT_BRACE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum LEFT_BRACKET:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum LEFT_CIRCULAR_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum LEFT_RIGHT_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum LEFT_RIGHT_ARROW_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum LEFT_RIGHT_CIRCULAR_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum LEFT_RIGHT_RIBBON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum LEFT_RIGHT_UP_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum LEFT_UP_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum LIGHTNING_BOLT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum LINE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum LINECONNECTOR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum LINE_INV:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum MATH_DIVIDE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum MATH_EQUAL:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum MATH_MINUS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum MATH_MULTIPLY:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum MATH_NOT_EQUAL:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum MATH_PLUS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum MOON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum NON_ISOSCELES_TRAPEZOID:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum NOTCHED_RIGHT_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum NO_SMOKING:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum OCTAGON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum PARALLELOGRAM:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum PENTAGON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum PIE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum PIE_WEDGE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum PLAQUE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum PLAQUE_TABS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum PLUS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum QUAD_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum QUAD_ARROW_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum RIBBON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum RIBBON2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum RIGHT_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum RIGHT_ARROW_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum RIGHT_BRACE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum RIGHT_BRACKET:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ROUND1_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ROUND2_DIAG_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ROUND2_SAME_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum ROUND_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum RT_TRIANGLE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum SMILEY_FACE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum SNIP1_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum SNIP2_DIAG_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum SNIP2_SAME_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum SNIP_ROUND_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum SQUARE_TABS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum STAR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum STAR10:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum STAR12:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum STAR16:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum STAR24:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum STAR32:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum STAR4:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum STAR5:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum STAR6:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum STAR7:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum STAR8:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum STARSHAPES:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum STRAIGHT_CONNECTOR1:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum STRIPED_RIGHT_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum SUN:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum SWOOSH_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum TEARDROP:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum TRAPEZIUM:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum TRAPEZOID:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum TRIANGLE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum UP_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum UP_ARROW_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum UP_DOWN_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum UP_DOWN_ARROW_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum UTURN_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum VERTICAL_SCROLL:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum WAVE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum WEDGE_ELLIPSE_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum WEDGE_RECT_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

.field public static final enum WEDGE_ROUND_RECT_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 10
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "LINE"

    const-string/jumbo v2, "line"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LINE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 13
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "LINE_INV"

    const-string/jumbo v2, "lineInv"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LINE_INV:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 16
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "TRIANGLE"

    const-string/jumbo v2, "triangle"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->TRIANGLE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 19
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "RT_TRIANGLE"

    const-string/jumbo v2, "rtTriangle"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->RT_TRIANGLE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 22
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "RECT"

    const-string/jumbo v2, "rect"

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 25
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "DIAMOND"

    const/4 v2, 0x5

    const-string/jumbo v3, "diamond"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->DIAMOND:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 28
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "PARALLELOGRAM"

    const/4 v2, 0x6

    const-string/jumbo v3, "parallelogram"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->PARALLELOGRAM:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 31
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "TRAPEZOID"

    const/4 v2, 0x7

    const-string/jumbo v3, "trapezoid"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->TRAPEZOID:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 32
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "TRAPEZIUM"

    const/16 v2, 0x8

    const-string/jumbo v3, "trapezium"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->TRAPEZIUM:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 35
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "NON_ISOSCELES_TRAPEZOID"

    const/16 v2, 0x9

    const-string/jumbo v3, "nonIsoscelesTrapezoid"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->NON_ISOSCELES_TRAPEZOID:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 38
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "PENTAGON"

    const/16 v2, 0xa

    const-string/jumbo v3, "pentagon"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->PENTAGON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 41
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "HEXAGON"

    const/16 v2, 0xb

    const-string/jumbo v3, "hexagon"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->HEXAGON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 44
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "HEPTAGON"

    const/16 v2, 0xc

    const-string/jumbo v3, "heptagon"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->HEPTAGON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 47
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "OCTAGON"

    const/16 v2, 0xd

    const-string/jumbo v3, "octagon"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->OCTAGON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 50
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "DECAGON"

    const/16 v2, 0xe

    const-string/jumbo v3, "decagon"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->DECAGON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 53
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "DODECAGON"

    const/16 v2, 0xf

    const-string/jumbo v3, "dodecagon"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->DODECAGON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 56
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "STAR4"

    const/16 v2, 0x10

    const-string/jumbo v3, "star4"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STAR4:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 59
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "STAR5"

    const/16 v2, 0x11

    const-string/jumbo v3, "star5"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STAR5:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 62
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "STAR6"

    const/16 v2, 0x12

    const-string/jumbo v3, "star6"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STAR6:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 65
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "STAR7"

    const/16 v2, 0x13

    const-string/jumbo v3, "star7"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STAR7:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 68
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "STAR8"

    const/16 v2, 0x14

    const-string/jumbo v3, "star8"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STAR8:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 71
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "STAR10"

    const/16 v2, 0x15

    const-string/jumbo v3, "star10"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STAR10:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 74
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "STAR12"

    const/16 v2, 0x16

    const-string/jumbo v3, "star12"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STAR12:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 77
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "STAR16"

    const/16 v2, 0x17

    const-string/jumbo v3, "star16"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STAR16:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 80
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "STAR24"

    const/16 v2, 0x18

    const-string/jumbo v3, "star24"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STAR24:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 83
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "STAR32"

    const/16 v2, 0x19

    const-string/jumbo v3, "star32"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STAR32:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 86
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "STAR"

    const/16 v2, 0x1a

    const-string/jumbo v3, "star"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STAR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 89
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "STARSHAPES"

    const/16 v2, 0x1b

    const-string/jumbo v3, "starshapes"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STARSHAPES:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 92
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ROUND_RECT"

    const/16 v2, 0x1c

    const-string/jumbo v3, "roundRect"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ROUND_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 95
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ROUND1_RECT"

    const/16 v2, 0x1d

    const-string/jumbo v3, "round1Rect"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ROUND1_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 98
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ROUND2_SAME_RECT"

    const/16 v2, 0x1e

    const-string/jumbo v3, "round2SameRect"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ROUND2_SAME_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 102
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ROUND2_DIAG_RECT"

    const/16 v2, 0x1f

    const-string/jumbo v3, "round2DiagRect"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ROUND2_DIAG_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 106
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "SNIP_ROUND_RECT"

    const/16 v2, 0x20

    const-string/jumbo v3, "snipRoundRect"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->SNIP_ROUND_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 109
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "SNIP1_RECT"

    const/16 v2, 0x21

    const-string/jumbo v3, "snip1Rect"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->SNIP1_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 112
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "SNIP2_SAME_RECT"

    const/16 v2, 0x22

    const-string/jumbo v3, "snip2SameRect"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->SNIP2_SAME_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 116
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "SNIP2_DIAG_RECT"

    const/16 v2, 0x23

    const-string/jumbo v3, "snip2DiagRect"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->SNIP2_DIAG_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 119
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "PLAQUE"

    const/16 v2, 0x24

    const-string/jumbo v3, "plaque"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->PLAQUE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 122
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ELLIPSE"

    const/16 v2, 0x25

    const-string/jumbo v3, "ellipse"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ELLIPSE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 125
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "TEARDROP"

    const/16 v2, 0x26

    const-string/jumbo v3, "teardrop"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->TEARDROP:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 128
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "HOME_PLATE"

    const/16 v2, 0x27

    const-string/jumbo v3, "homePlate"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->HOME_PLATE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 131
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "CHEVRON"

    const/16 v2, 0x28

    const-string/jumbo v3, "chevron"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CHEVRON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 134
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "PIE_WEDGE"

    const/16 v2, 0x29

    const-string/jumbo v3, "pieWedge"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->PIE_WEDGE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 137
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "PIE"

    const/16 v2, 0x2a

    const-string/jumbo v3, "pie"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->PIE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 140
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "BLOCK_ARC"

    const/16 v2, 0x2b

    const-string/jumbo v3, "blockArc"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BLOCK_ARC:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 143
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "DONUT"

    const/16 v2, 0x2c

    const-string/jumbo v3, "donut"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->DONUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 146
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "NO_SMOKING"

    const/16 v2, 0x2d

    const-string/jumbo v3, "noSmoking"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->NO_SMOKING:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 149
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "RIGHT_ARROW"

    const/16 v2, 0x2e

    const-string/jumbo v3, "rightArrow"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->RIGHT_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 152
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "LEFT_ARROW"

    const/16 v2, 0x2f

    const-string/jumbo v3, "leftArrow"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LEFT_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 155
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "UP_ARROW"

    const/16 v2, 0x30

    const-string/jumbo v3, "upArrow"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->UP_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 158
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "DOWN_ARROW"

    const/16 v2, 0x31

    const-string/jumbo v3, "downArrow"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->DOWN_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 161
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "STRIPED_RIGHT_ARROW"

    const/16 v2, 0x32

    const-string/jumbo v3, "stripedRightArrow"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STRIPED_RIGHT_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 164
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "NOTCHED_RIGHT_ARROW"

    const/16 v2, 0x33

    const-string/jumbo v3, "notchedRightArrow"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->NOTCHED_RIGHT_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 167
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "BENT_UP_ARROW"

    const/16 v2, 0x34

    const-string/jumbo v3, "bentUpArrow"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BENT_UP_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 170
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "LEFT_RIGHT_ARROW"

    const/16 v2, 0x35

    const-string/jumbo v3, "leftRightArrow"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LEFT_RIGHT_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 173
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "UP_DOWN_ARROW"

    const/16 v2, 0x36

    const-string/jumbo v3, "upDownArrow"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->UP_DOWN_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 176
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "LEFT_UP_ARROW"

    const/16 v2, 0x37

    const-string/jumbo v3, "leftUpArrow"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LEFT_UP_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 179
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "LEFT_RIGHT_UP_ARROW"

    const/16 v2, 0x38

    const-string/jumbo v3, "leftRightUpArrow"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LEFT_RIGHT_UP_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 182
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "QUAD_ARROW"

    const/16 v2, 0x39

    const-string/jumbo v3, "quadArrow"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->QUAD_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 185
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "LEFT_ARROW_CALLOUT"

    const/16 v2, 0x3a

    const-string/jumbo v3, "leftArrowCallout"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LEFT_ARROW_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 188
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "RIGHT_ARROW_CALLOUT"

    const/16 v2, 0x3b

    const-string/jumbo v3, "rightArrowCallout"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->RIGHT_ARROW_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 191
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "UP_ARROW_CALLOUT"

    const/16 v2, 0x3c

    const-string/jumbo v3, "upArrowCallout"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->UP_ARROW_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 194
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "DOWN_ARROW_CALLOUT"

    const/16 v2, 0x3d

    const-string/jumbo v3, "downArrowCallout"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->DOWN_ARROW_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 197
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "LEFT_RIGHT_ARROW_CALLOUT"

    const/16 v2, 0x3e

    const-string/jumbo v3, "leftRightArrowCallout"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LEFT_RIGHT_ARROW_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 201
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "UP_DOWN_ARROW_CALLOUT"

    const/16 v2, 0x3f

    const-string/jumbo v3, "upDownArrowCallout"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->UP_DOWN_ARROW_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 204
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "QUAD_ARROW_CALLOUT"

    const/16 v2, 0x40

    const-string/jumbo v3, "quadArrowCallout"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->QUAD_ARROW_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 207
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "BENT_ARROW"

    const/16 v2, 0x41

    const-string/jumbo v3, "bentArrow"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BENT_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 210
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "UTURN_ARROW"

    const/16 v2, 0x42

    const-string/jumbo v3, "uturnArrow"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->UTURN_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 213
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "CIRCULAR_ARROW"

    const/16 v2, 0x43

    const-string/jumbo v3, "circularArrow"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CIRCULAR_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 216
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "LEFT_CIRCULAR_ARROW"

    const/16 v2, 0x44

    const-string/jumbo v3, "leftCircularArrow"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LEFT_CIRCULAR_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 219
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "LEFT_RIGHT_CIRCULAR_ARROW"

    const/16 v2, 0x45

    const-string/jumbo v3, "leftRightCircularArrow"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LEFT_RIGHT_CIRCULAR_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 223
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "CURVED_RIGHT_ARROW"

    const/16 v2, 0x46

    const-string/jumbo v3, "curvedRightArrow"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CURVED_RIGHT_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 226
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "CURVED_LEFT_ARROW"

    const/16 v2, 0x47

    const-string/jumbo v3, "curvedLeftArrow"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CURVED_LEFT_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 229
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "CURVED_UP_ARROW"

    const/16 v2, 0x48

    const-string/jumbo v3, "curvedUpArrow"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CURVED_UP_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 232
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "CURVED_DOWN_ARROW"

    const/16 v2, 0x49

    const-string/jumbo v3, "curvedDownArrow"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CURVED_DOWN_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 235
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "SWOOSH_ARROW"

    const/16 v2, 0x4a

    const-string/jumbo v3, "swooshArrow"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->SWOOSH_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 238
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "CUBE"

    const/16 v2, 0x4b

    const-string/jumbo v3, "cube"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CUBE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 241
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "CAN"

    const/16 v2, 0x4c

    const-string/jumbo v3, "can"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CAN:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 244
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "LIGHTNING_BOLT"

    const/16 v2, 0x4d

    const-string/jumbo v3, "lightningBolt"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LIGHTNING_BOLT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 247
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "HEART"

    const/16 v2, 0x4e

    const-string/jumbo v3, "heart"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->HEART:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 250
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "SUN"

    const/16 v2, 0x4f

    const-string/jumbo v3, "sun"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->SUN:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 253
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "MOON"

    const/16 v2, 0x50

    const-string/jumbo v3, "moon"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->MOON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 256
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "SMILEY_FACE"

    const/16 v2, 0x51

    const-string/jumbo v3, "smileyFace"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->SMILEY_FACE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 259
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "IRREGULAR_SEAL1"

    const/16 v2, 0x52

    const-string/jumbo v3, "irregularSeal1"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->IRREGULAR_SEAL1:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 262
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "IRREGULAR_SEAL2"

    const/16 v2, 0x53

    const-string/jumbo v3, "irregularSeal2"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->IRREGULAR_SEAL2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 265
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FOLDED_CORNER"

    const/16 v2, 0x54

    const-string/jumbo v3, "foldedCorner"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FOLDED_CORNER:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 268
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "BEVEL"

    const/16 v2, 0x55

    const-string/jumbo v3, "bevel"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BEVEL:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 271
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FRAME"

    const/16 v2, 0x56

    const-string/jumbo v3, "frame"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FRAME:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 274
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "HALF_FRAME"

    const/16 v2, 0x57

    const-string/jumbo v3, "halfFrame"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->HALF_FRAME:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 277
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "CORNER"

    const/16 v2, 0x58

    const-string/jumbo v3, "corner"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CORNER:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 280
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "DIAG_STRIPE"

    const/16 v2, 0x59

    const-string/jumbo v3, "diagStripe"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->DIAG_STRIPE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 283
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "CHORD"

    const/16 v2, 0x5a

    const-string/jumbo v3, "chord"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CHORD:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 286
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ARC"

    const/16 v2, 0x5b

    const-string/jumbo v3, "arc"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ARC:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "LINECONNECTOR"

    const/16 v2, 0x5c

    const-string/jumbo v3, "lineConnector"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LINECONNECTOR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ARCCONNECTOR"

    const/16 v2, 0x5d

    const-string/jumbo v3, "arcConnector"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ARCCONNECTOR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 290
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "LEFT_BRACKET"

    const/16 v2, 0x5e

    const-string/jumbo v3, "leftBracket"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LEFT_BRACKET:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 293
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "RIGHT_BRACKET"

    const/16 v2, 0x5f

    const-string/jumbo v3, "rightBracket"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->RIGHT_BRACKET:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 296
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "LEFT_BRACE"

    const/16 v2, 0x60

    const-string/jumbo v3, "leftBrace"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LEFT_BRACE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 299
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "RIGHT_BRACE"

    const/16 v2, 0x61

    const-string/jumbo v3, "rightBrace"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->RIGHT_BRACE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 302
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "BRACKET_PAIR"

    const/16 v2, 0x62

    const-string/jumbo v3, "bracketPair"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BRACKET_PAIR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 305
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "BRACE_PAIR"

    const/16 v2, 0x63

    const-string/jumbo v3, "bracePair"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BRACE_PAIR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 308
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "STRAIGHT_CONNECTOR1"

    const/16 v2, 0x64

    const-string/jumbo v3, "straightConnector1"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STRAIGHT_CONNECTOR1:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 311
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "BENT_CONNECTOR2"

    const/16 v2, 0x65

    const-string/jumbo v3, "bentConnector2"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BENT_CONNECTOR2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 314
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "BENT_CONNECTOR3"

    const/16 v2, 0x66

    const-string/jumbo v3, "bentConnector3"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BENT_CONNECTOR3:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 317
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "BENT_CONNECTOR4"

    const/16 v2, 0x67

    const-string/jumbo v3, "bentConnector4"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BENT_CONNECTOR4:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 320
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "BENT_CONNECTOR5"

    const/16 v2, 0x68

    const-string/jumbo v3, "bentConnector5"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BENT_CONNECTOR5:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 323
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "CURVED_CONNECTOR2"

    const/16 v2, 0x69

    const-string/jumbo v3, "curvedConnector2"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CURVED_CONNECTOR2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 326
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "CURVED_CONNECTOR3"

    const/16 v2, 0x6a

    const-string/jumbo v3, "curvedConnector3"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CURVED_CONNECTOR3:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 329
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "CURVED_CONNECTOR4"

    const/16 v2, 0x6b

    const-string/jumbo v3, "curvedConnector4"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CURVED_CONNECTOR4:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 332
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "CURVED_CONNECTOR5"

    const/16 v2, 0x6c

    const-string/jumbo v3, "curvedConnector5"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CURVED_CONNECTOR5:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 335
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "CALLOUT1"

    const/16 v2, 0x6d

    const-string/jumbo v3, "callout1"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CALLOUT1:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 338
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "CALLOUT2"

    const/16 v2, 0x6e

    const-string/jumbo v3, "callout2"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CALLOUT2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 341
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "CALLOUT3"

    const/16 v2, 0x6f

    const-string/jumbo v3, "callout3"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CALLOUT3:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 344
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ACCENT_CALLOUT1"

    const/16 v2, 0x70

    const-string/jumbo v3, "accentCallout1"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACCENT_CALLOUT1:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 347
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ACCENT_CALLOUT2"

    const/16 v2, 0x71

    const-string/jumbo v3, "accentCallout2"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACCENT_CALLOUT2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 350
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ACCENT_CALLOUT3"

    const/16 v2, 0x72

    const-string/jumbo v3, "accentCallout3"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACCENT_CALLOUT3:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 353
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "BORDER_CALLOUT1"

    const/16 v2, 0x73

    const-string/jumbo v3, "borderCallout1"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BORDER_CALLOUT1:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 356
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "BORDER_CALLOUT2"

    const/16 v2, 0x74

    const-string/jumbo v3, "borderCallout2"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BORDER_CALLOUT2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 359
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "BORDER_CALLOUT3"

    const/16 v2, 0x75

    const-string/jumbo v3, "borderCallout3"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BORDER_CALLOUT3:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 362
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ACCENT_BORDER_CALLOUT1"

    const/16 v2, 0x76

    const-string/jumbo v3, "accentBorderCallout1"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACCENT_BORDER_CALLOUT1:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 366
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ACCENT_BORDER_CALLOUT2"

    const/16 v2, 0x77

    const-string/jumbo v3, "accentBorderCallout2"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACCENT_BORDER_CALLOUT2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 370
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ACCENT_BORDER_CALLOUT3"

    const/16 v2, 0x78

    const-string/jumbo v3, "accentBorderCallout3"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACCENT_BORDER_CALLOUT3:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 374
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "WEDGE_RECT_CALLOUT"

    const/16 v2, 0x79

    const-string/jumbo v3, "wedgeRectCallout"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->WEDGE_RECT_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 378
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "WEDGE_ROUND_RECT_CALLOUT"

    const/16 v2, 0x7a

    const-string/jumbo v3, "wedgeRoundRectCallout"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->WEDGE_ROUND_RECT_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 382
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "WEDGE_ELLIPSE_CALLOUT"

    const/16 v2, 0x7b

    const-string/jumbo v3, "wedgeEllipseCallout"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->WEDGE_ELLIPSE_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 385
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "CLOUD_CALLOUT"

    const/16 v2, 0x7c

    const-string/jumbo v3, "cloudCallout"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CLOUD_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 388
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "CLOUD"

    const/16 v2, 0x7d

    const-string/jumbo v3, "cloud"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CLOUD:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 391
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "RIBBON"

    const/16 v2, 0x7e

    const-string/jumbo v3, "ribbon"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->RIBBON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 394
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "RIBBON2"

    const/16 v2, 0x7f

    const-string/jumbo v3, "ribbon2"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->RIBBON2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 397
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ELLIPSE_RIBBON"

    const/16 v2, 0x80

    const-string/jumbo v3, "ellipseRibbon"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ELLIPSE_RIBBON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 400
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ELLIPSE_RIBBON2"

    const/16 v2, 0x81

    const-string/jumbo v3, "ellipseRibbon2"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ELLIPSE_RIBBON2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 403
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "LEFT_RIGHT_RIBBON"

    const/16 v2, 0x82

    const-string/jumbo v3, "leftRightRibbon"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LEFT_RIGHT_RIBBON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 406
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "VERTICAL_SCROLL"

    const/16 v2, 0x83

    const-string/jumbo v3, "verticalScroll"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->VERTICAL_SCROLL:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 409
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "HORIZONTAL_SCROLL"

    const/16 v2, 0x84

    const-string/jumbo v3, "horizontalScroll"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->HORIZONTAL_SCROLL:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 412
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "WAVE"

    const/16 v2, 0x85

    const-string/jumbo v3, "wave"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->WAVE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 415
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "DOUBLE_WAVE"

    const/16 v2, 0x86

    const-string/jumbo v3, "doubleWave"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->DOUBLE_WAVE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 418
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "PLUS"

    const/16 v2, 0x87

    const-string/jumbo v3, "plus"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->PLUS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 421
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_PROCESS"

    const/16 v2, 0x88

    const-string/jumbo v3, "flowChartProcess"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_PROCESS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 424
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_DECISION"

    const/16 v2, 0x89

    const-string/jumbo v3, "flowChartDecision"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_DECISION:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 427
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_INPUT_OUTPUT"

    const/16 v2, 0x8a

    const-string/jumbo v3, "flowChartInputOutput"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_INPUT_OUTPUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 431
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_PREDEFINED_PROCESS"

    const/16 v2, 0x8b

    const-string/jumbo v3, "flowChartPredefinedProcess"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_PREDEFINED_PROCESS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 435
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_INTERNAL_STORAGE"

    const/16 v2, 0x8c

    const-string/jumbo v3, "flowChartInternalStorage"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_INTERNAL_STORAGE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 439
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_DOCUMENT"

    const/16 v2, 0x8d

    const-string/jumbo v3, "flowChartDocument"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_DOCUMENT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 442
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_MULTIDOCUMENT"

    const/16 v2, 0x8e

    const-string/jumbo v3, "flowChartMultidocument"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_MULTIDOCUMENT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 445
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_TERMINATOR"

    const/16 v2, 0x8f

    const-string/jumbo v3, "flowChartTerminator"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_TERMINATOR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 448
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_PREPARATION"

    const/16 v2, 0x90

    const-string/jumbo v3, "flowChartPreparation"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_PREPARATION:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 451
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_MANUAL_INPUT"

    const/16 v2, 0x91

    const-string/jumbo v3, "flowChartManualInput"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_MANUAL_INPUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 455
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_MANUAL_OPERATION"

    const/16 v2, 0x92

    const-string/jumbo v3, "flowChartManualOperation"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_MANUAL_OPERATION:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 459
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_CONNECTOR"

    const/16 v2, 0x93

    const-string/jumbo v3, "flowChartConnector"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_CONNECTOR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 462
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_PUNCHED_CARD"

    const/16 v2, 0x94

    const-string/jumbo v3, "flowChartPunchedCard"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_PUNCHED_CARD:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 465
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_PUNCHED_TAPE"

    const/16 v2, 0x95

    const-string/jumbo v3, "flowChartPunchedTape"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_PUNCHED_TAPE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 469
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_SUMMING_JUNCTION"

    const/16 v2, 0x96

    const-string/jumbo v3, "flowChartSummingJunction"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_SUMMING_JUNCTION:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 472
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_OR"

    const/16 v2, 0x97

    const-string/jumbo v3, "flowChartOr"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_OR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 475
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_COLLATE"

    const/16 v2, 0x98

    const-string/jumbo v3, "flowChartCollate"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_COLLATE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 478
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_SORT"

    const/16 v2, 0x99

    const-string/jumbo v3, "flowChartSort"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_SORT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 481
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_EXTRACT"

    const/16 v2, 0x9a

    const-string/jumbo v3, "flowChartExtract"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_EXTRACT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 484
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_MERGE"

    const/16 v2, 0x9b

    const-string/jumbo v3, "flowChartMerge"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_MERGE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 487
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_OFFLINE_STORAGE"

    const/16 v2, 0x9c

    const-string/jumbo v3, "flowChartOfflineStorage"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_OFFLINE_STORAGE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 491
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_ONLINE_STORAGE"

    const/16 v2, 0x9d

    const-string/jumbo v3, "flowChartOnlineStorage"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_ONLINE_STORAGE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 495
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_MAGNETIC_TAPE"

    const/16 v2, 0x9e

    const-string/jumbo v3, "flowChartMagneticTape"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_MAGNETIC_TAPE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 499
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_MAGNETIC_DISK"

    const/16 v2, 0x9f

    const-string/jumbo v3, "flowChartMagneticDisk"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_MAGNETIC_DISK:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 503
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_MAGNETIC_DRUM"

    const/16 v2, 0xa0

    const-string/jumbo v3, "flowChartMagneticDrum"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_MAGNETIC_DRUM:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 506
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_DISPLAY"

    const/16 v2, 0xa1

    const-string/jumbo v3, "flowChartDisplay"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_DISPLAY:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 509
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_DELAY"

    const/16 v2, 0xa2

    const-string/jumbo v3, "flowChartDelay"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_DELAY:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 512
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_ALTERNATE_PROCESS"

    const/16 v2, 0xa3

    const-string/jumbo v3, "flowChartAlternateProcess"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_ALTERNATE_PROCESS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 516
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FLOW_CHART_OFFPAGE_CONNECTOR"

    const/16 v2, 0xa4

    const-string/jumbo v3, "flowChartOffpageConnector"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_OFFPAGE_CONNECTOR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 520
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ACTION_BUTTON_BLANK"

    const/16 v2, 0xa5

    const-string/jumbo v3, "actionButtonBlank"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACTION_BUTTON_BLANK:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 523
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ACTION_BUTTON_HOME"

    const/16 v2, 0xa6

    const-string/jumbo v3, "actionButtonHome"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACTION_BUTTON_HOME:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 526
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ACTION_BUTTON_HELP"

    const/16 v2, 0xa7

    const-string/jumbo v3, "actionButtonHelp"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACTION_BUTTON_HELP:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 529
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ACTION_BUTTON_INFORMATION"

    const/16 v2, 0xa8

    const-string/jumbo v3, "actionButtonInformation"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACTION_BUTTON_INFORMATION:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 533
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ACTION_BUTTON_FORWARD_NEXT"

    const/16 v2, 0xa9

    const-string/jumbo v3, "actionButtonForwardNext"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACTION_BUTTON_FORWARD_NEXT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 537
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ACTION_BUTTON_BACK_PREVIOUS"

    const/16 v2, 0xaa

    const-string/jumbo v3, "actionButtonBackPrevious"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACTION_BUTTON_BACK_PREVIOUS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 540
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ACTION_BUTTON_END"

    const/16 v2, 0xab

    const-string/jumbo v3, "actionButtonEnd"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACTION_BUTTON_END:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 543
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ACTION_BUTTON_BEGINNING"

    const/16 v2, 0xac

    const-string/jumbo v3, "actionButtonBeginning"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACTION_BUTTON_BEGINNING:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 546
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ACTION_BUTTON_RETURN"

    const/16 v2, 0xad

    const-string/jumbo v3, "actionButtonReturn"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACTION_BUTTON_RETURN:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 549
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ACTION_BUTTON_DOCUMENT"

    const/16 v2, 0xae

    const-string/jumbo v3, "actionButtonDocument"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACTION_BUTTON_DOCUMENT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 552
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ACTION_BUTTON_SOUND"

    const/16 v2, 0xaf

    const-string/jumbo v3, "actionButtonSound"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACTION_BUTTON_SOUND:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 555
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "ACTION_BUTTON_MOVIE"

    const/16 v2, 0xb0

    const-string/jumbo v3, "actionButtonMovie"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACTION_BUTTON_MOVIE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 558
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "GEAR6"

    const/16 v2, 0xb1

    const-string/jumbo v3, "gear6"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->GEAR6:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 561
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "GEAR9"

    const/16 v2, 0xb2

    const-string/jumbo v3, "gear9"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->GEAR9:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 564
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "FUNNEL"

    const/16 v2, 0xb3

    const-string/jumbo v3, "funnel"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FUNNEL:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 567
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "MATH_PLUS"

    const/16 v2, 0xb4

    const-string/jumbo v3, "mathPlus"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->MATH_PLUS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 570
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "MATH_MINUS"

    const/16 v2, 0xb5

    const-string/jumbo v3, "mathMinus"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->MATH_MINUS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 573
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "MATH_MULTIPLY"

    const/16 v2, 0xb6

    const-string/jumbo v3, "mathMultiply"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->MATH_MULTIPLY:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 576
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "MATH_DIVIDE"

    const/16 v2, 0xb7

    const-string/jumbo v3, "mathDivide"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->MATH_DIVIDE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 579
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "MATH_EQUAL"

    const/16 v2, 0xb8

    const-string/jumbo v3, "mathEqual"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->MATH_EQUAL:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 582
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "MATH_NOT_EQUAL"

    const/16 v2, 0xb9

    const-string/jumbo v3, "mathNotEqual"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->MATH_NOT_EQUAL:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 585
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "CORNER_TABS"

    const/16 v2, 0xba

    const-string/jumbo v3, "cornerTabs"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CORNER_TABS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 588
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "SQUARE_TABS"

    const/16 v2, 0xbb

    const-string/jumbo v3, "squareTabs"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->SQUARE_TABS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 591
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "PLAQUE_TABS"

    const/16 v2, 0xbc

    const-string/jumbo v3, "plaqueTabs"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->PLAQUE_TABS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 594
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "CHART_X"

    const/16 v2, 0xbd

    const-string/jumbo v3, "chartX"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CHART_X:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 597
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "CHART_STAR"

    const/16 v2, 0xbe

    const-string/jumbo v3, "chartStar"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CHART_STAR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 600
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    const-string/jumbo v1, "CHART_PLUS"

    const/16 v2, 0xbf

    const-string/jumbo v3, "chartPlus"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CHART_PLUS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    .line 6
    const/16 v0, 0xc0

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LINE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LINE_INV:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->TRIANGLE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->RT_TRIANGLE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->DIAMOND:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->PARALLELOGRAM:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->TRAPEZOID:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->TRAPEZIUM:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->NON_ISOSCELES_TRAPEZOID:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->PENTAGON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->HEXAGON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->HEPTAGON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->OCTAGON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->DECAGON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->DODECAGON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STAR4:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STAR5:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STAR6:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STAR7:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STAR8:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STAR10:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STAR12:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STAR16:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STAR24:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STAR32:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STAR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STARSHAPES:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ROUND_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ROUND1_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ROUND2_SAME_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ROUND2_DIAG_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->SNIP_ROUND_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->SNIP1_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->SNIP2_SAME_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->SNIP2_DIAG_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->PLAQUE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ELLIPSE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->TEARDROP:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->HOME_PLATE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CHEVRON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->PIE_WEDGE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->PIE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BLOCK_ARC:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->DONUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->NO_SMOKING:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->RIGHT_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LEFT_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->UP_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->DOWN_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STRIPED_RIGHT_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->NOTCHED_RIGHT_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BENT_UP_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LEFT_RIGHT_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->UP_DOWN_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LEFT_UP_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LEFT_RIGHT_UP_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->QUAD_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LEFT_ARROW_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->RIGHT_ARROW_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->UP_ARROW_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->DOWN_ARROW_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LEFT_RIGHT_ARROW_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->UP_DOWN_ARROW_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->QUAD_ARROW_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BENT_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->UTURN_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CIRCULAR_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LEFT_CIRCULAR_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LEFT_RIGHT_CIRCULAR_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CURVED_RIGHT_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CURVED_LEFT_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CURVED_UP_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CURVED_DOWN_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->SWOOSH_ARROW:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CUBE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CAN:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LIGHTNING_BOLT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->HEART:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->SUN:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->MOON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->SMILEY_FACE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->IRREGULAR_SEAL1:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->IRREGULAR_SEAL2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FOLDED_CORNER:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BEVEL:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FRAME:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->HALF_FRAME:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CORNER:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->DIAG_STRIPE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CHORD:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ARC:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LINECONNECTOR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ARCCONNECTOR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LEFT_BRACKET:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->RIGHT_BRACKET:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x60

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LEFT_BRACE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x61

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->RIGHT_BRACE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x62

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BRACKET_PAIR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x63

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BRACE_PAIR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->STRAIGHT_CONNECTOR1:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x65

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BENT_CONNECTOR2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x66

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BENT_CONNECTOR3:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x67

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BENT_CONNECTOR4:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x68

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BENT_CONNECTOR5:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x69

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CURVED_CONNECTOR2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CURVED_CONNECTOR3:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CURVED_CONNECTOR4:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CURVED_CONNECTOR5:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CALLOUT1:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CALLOUT2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CALLOUT3:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x70

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACCENT_CALLOUT1:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x71

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACCENT_CALLOUT2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x72

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACCENT_CALLOUT3:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x73

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BORDER_CALLOUT1:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x74

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BORDER_CALLOUT2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x75

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->BORDER_CALLOUT3:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x76

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACCENT_BORDER_CALLOUT1:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x77

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACCENT_BORDER_CALLOUT2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x78

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACCENT_BORDER_CALLOUT3:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x79

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->WEDGE_RECT_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->WEDGE_ROUND_RECT_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->WEDGE_ELLIPSE_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CLOUD_CALLOUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CLOUD:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->RIBBON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->RIBBON2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x80

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ELLIPSE_RIBBON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x81

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ELLIPSE_RIBBON2:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x82

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->LEFT_RIGHT_RIBBON:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x83

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->VERTICAL_SCROLL:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x84

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->HORIZONTAL_SCROLL:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x85

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->WAVE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x86

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->DOUBLE_WAVE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x87

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->PLUS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x88

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_PROCESS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x89

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_DECISION:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_INPUT_OUTPUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_PREDEFINED_PROCESS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_INTERNAL_STORAGE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_DOCUMENT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_MULTIDOCUMENT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_TERMINATOR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x90

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_PREPARATION:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x91

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_MANUAL_INPUT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x92

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_MANUAL_OPERATION:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x93

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_CONNECTOR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x94

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_PUNCHED_CARD:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x95

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_PUNCHED_TAPE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x96

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_SUMMING_JUNCTION:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x97

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_OR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x98

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_COLLATE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x99

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_SORT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_EXTRACT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_MERGE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_OFFLINE_STORAGE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_ONLINE_STORAGE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_MAGNETIC_TAPE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_MAGNETIC_DISK:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_MAGNETIC_DRUM:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_DISPLAY:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_DELAY:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_ALTERNATE_PROCESS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FLOW_CHART_OFFPAGE_CONNECTOR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACTION_BUTTON_BLANK:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACTION_BUTTON_HOME:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACTION_BUTTON_HELP:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACTION_BUTTON_INFORMATION:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACTION_BUTTON_FORWARD_NEXT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACTION_BUTTON_BACK_PREVIOUS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xab

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACTION_BUTTON_END:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xac

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACTION_BUTTON_BEGINNING:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xad

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACTION_BUTTON_RETURN:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xae

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACTION_BUTTON_DOCUMENT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACTION_BUTTON_SOUND:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ACTION_BUTTON_MOVIE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->GEAR6:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->GEAR9:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->FUNNEL:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->MATH_PLUS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->MATH_MINUS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->MATH_MULTIPLY:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->MATH_DIVIDE:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->MATH_EQUAL:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->MATH_NOT_EQUAL:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xba

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CORNER_TABS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->SQUARE_TABS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->PLAQUE_TABS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CHART_X:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CHART_STAR:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->CHART_PLUS:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 604
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 605
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->value:Ljava/lang/String;

    .line 606
    return-void
.end method

.method public static convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 613
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->values()[Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 614
    .local v2, "inst":Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 618
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;
    :goto_1
    return-object v2

    .line 613
    .restart local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 618
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 6
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    return-object v0
.end method


# virtual methods
.method public xmlValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 609
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->value:Ljava/lang/String;

    return-object v0
.end method
