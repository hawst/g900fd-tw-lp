.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelSheetsAttriHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelSheetsAttriHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    const-string/jumbo v0, "sheet"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 17
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 22
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 23
    const-string/jumbo v4, "name"

    invoke-virtual {p0, p3, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelSheetsAttriHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 24
    .local v1, "name":Ljava/lang/String;
    const-string/jumbo v4, "sheetId"

    invoke-virtual {p0, p3, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelSheetsAttriHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v3

    .line 26
    .local v3, "shtid":Ljava/lang/String;
    const-string/jumbo v4, "r:id"

    invoke-virtual {p0, p3, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelSheetsAttriHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 27
    .local v0, "id":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;

    invoke-virtual {v4, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;->addExSheetIrdList(Ljava/lang/String;)V

    .line 28
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;

    invoke-virtual {v4, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;->addExSheetNamelist(Ljava/lang/String;)V

    .line 29
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;

    invoke-virtual {v4, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;->addExSheetidList(Ljava/lang/String;)V

    .line 30
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;->getExSheetIrdList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 32
    .local v2, "sheetcount":I
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;

    invoke-virtual {v4, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelWorkbook;->addExSheetcount(I)V

    .line 34
    return-void
.end method
