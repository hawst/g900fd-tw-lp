.class public Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;
.super Ljava/lang/Object;
.source "GradientFill.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Stop"
.end annotation


# instance fields
.field private color:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

.field private pos:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;->color:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    return-object v0
.end method

.method public getPosition()D
    .locals 2

    .prologue
    .line 98
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;->pos:D

    return-wide v0
.end method

.method public setColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V
    .locals 0
    .param p1, "color"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;->color:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .line 95
    return-void
.end method

.method public setPosition(D)V
    .locals 1
    .param p1, "pos"    # D

    .prologue
    .line 90
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;->pos:D

    .line 91
    return-void
.end method
