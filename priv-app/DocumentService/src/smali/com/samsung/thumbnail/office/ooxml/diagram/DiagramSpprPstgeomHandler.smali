.class public Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprPstgeomHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DiagramSpprPstgeomHandler.java"


# instance fields
.field private pstGeom:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IPstgeomHandler;


# direct methods
.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IPstgeomHandler;)V
    .locals 4
    .param p1, "nsID"    # I
    .param p2, "pstGeom"    # Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IPstgeomHandler;

    .prologue
    .line 23
    const-string/jumbo v3, "prstGeom"

    invoke-direct {p0, p1, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 24
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprPstgeomHandler;->pstGeom:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IPstgeomHandler;

    .line 26
    const/4 v3, 0x1

    new-array v2, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 28
    .local v2, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPrstGeomAvLstHandler;

    const/16 v3, 0x1f

    invoke-direct {v0, v3, p2}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPrstGeomAvLstHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IPstgeomHandler;)V

    .line 30
    .local v0, "avlstHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPrstGeomAvLstHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v3, "avLst"

    invoke-direct {v1, v3, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 33
    .local v1, "avlstSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v3, 0x0

    aput-object v1, v2, v3

    .line 35
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprPstgeomHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 36
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 41
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 43
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    const-string/jumbo v1, "prst"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprPstgeomHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;-><init>(Ljava/lang/String;)V

    .line 48
    .local v0, "prst":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprPstgeomHandler;->pstGeom:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IPstgeomHandler;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpPrHandler$IPstgeomHandler;->setPrst(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 49
    return-void
.end method
