.class public Lcom/samsung/thumbnail/office/ooxml/dml/charts/XSSFExcelChartParser;
.super Ljava/lang/Object;
.source "XSSFExcelChartParser.java"


# instance fields
.field private chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;Ljava/io/InputStream;Landroid/content/Context;)V
    .locals 0
    .param p1, "chartobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;
    .param p2, "is"    # Ljava/io/InputStream;
    .param p3, "mContext"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/XSSFExcelChartParser;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .line 55
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/XSSFExcelChartParser;->onDrawingRead(Ljava/io/InputStream;)V

    .line 57
    return-void
.end method

.method private parseExcel(Ljava/io/InputStream;)V
    .locals 2
    .param p1, "is"    # Ljava/io/InputStream;

    .prologue
    .line 61
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStartParser;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/XSSFExcelChartParser;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStartParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;)V

    .line 63
    .local v0, "chartParser":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStartParser;
    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStartParser;->parse(Ljava/io/InputStream;)V

    .line 64
    return-void
.end method


# virtual methods
.method protected onDrawingRead(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/XSSFExcelChartParser;->parseExcel(Ljava/io/InputStream;)V

    .line 75
    return-void
.end method
