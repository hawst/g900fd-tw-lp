.class public final Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;
.super Lorg/apache/poi/POIXMLRelation;
.source "XSSFRelation.java"


# static fields
.field public static final ACTIVEX_BINS:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final ACTIVEX_CONTROLS:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final CHART:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final DRAWINGS:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final IMAGES:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final IMAGE_DIB:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final IMAGE_EMF:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final IMAGE_JPEG:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final IMAGE_PICT:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final IMAGE_PNG:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final IMAGE_WMF:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final MACROS_WORKBOOK:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final MACRO_ADDIN_WORKBOOK:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final MACRO_TEMPLATE_WORKBOOK:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final OLEEMBEDDINGS:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final PACKEMBEDDINGS:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final PRINTER_SETTINGS:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final SHARED_STRINGS:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final SHEET_HYPERLINKS:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final STYLES:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final TABLE:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final TEMPLATE_WORKBOOK:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final THEME:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final VBA_MACROS:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final WORKBOOK:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field public static final WORKSHEET:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

.field protected static _table:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;",
            ">;"
        }
    .end annotation
.end field

.field private static log:Lorg/apache/poi/util/POILogger;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 44
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->log:Lorg/apache/poi/util/POILogger;

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->_table:Ljava/util/Map;

    .line 51
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/workbook"

    const-string/jumbo v3, "/xl/workbook.xml"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->WORKBOOK:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 55
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "application/vnd.ms-excel.sheet.macroEnabled.main+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"

    const-string/jumbo v3, "/xl/workbook.xml"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->MACROS_WORKBOOK:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 59
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.spreadsheetml.template.main+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"

    const-string/jumbo v3, "/xl/workbook.xml"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->TEMPLATE_WORKBOOK:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 63
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "application/vnd.ms-excel.template.macroEnabled.main+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"

    const-string/jumbo v3, "/xl/workbook.xml"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->MACRO_TEMPLATE_WORKBOOK:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 67
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "application/vnd.ms-excel.addin.macroEnabled.main+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"

    const-string/jumbo v3, "/xl/workbook.xml"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->MACRO_ADDIN_WORKBOOK:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 71
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/worksheet"

    const-string/jumbo v3, "/xl/worksheets/sheet#.xml"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->WORKSHEET:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 78
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.theme+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme"

    const-string/jumbo v3, "/xl/theme/theme#.xml"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->THEME:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 82
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.spreadsheetml.sharedStrings+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/sharedStrings"

    const-string/jumbo v3, "/xl/sharedStrings.xml"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->SHARED_STRINGS:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 87
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.spreadsheetml.styles+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles"

    const-string/jumbo v3, "/xl/styles.xml"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->STYLES:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 93
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.drawing+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/drawing"

    const-string/jumbo v3, "/xl/drawings/drawing#.xml"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->DRAWINGS:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 100
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.spreadsheetml.table+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/table"

    const-string/jumbo v3, "/xl/tables/table#.xml"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->TABLE:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 105
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.drawingml.chart+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/chart"

    const-string/jumbo v3, "/xl/charts/chart#.xml"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->CHART:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 136
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    const-class v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;

    invoke-direct {v0, v5, v1, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->IMAGES:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 140
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "image/x-emf"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    const-string/jumbo v3, "/xl/media/image#.emf"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->IMAGE_EMF:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 144
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "image/x-wmf"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    const-string/jumbo v3, "/xl/media/image#.wmf"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->IMAGE_WMF:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 148
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "image/pict"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    const-string/jumbo v3, "/xl/media/image#.pict"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->IMAGE_PICT:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 152
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "image/jpeg"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    const-string/jumbo v3, "/xl/media/image#.jpeg"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->IMAGE_JPEG:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 156
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "image/png"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    const-string/jumbo v3, "/xl/media/image#.png"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->IMAGE_PNG:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 160
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "image/dib"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    const-string/jumbo v3, "/xl/media/image#.dib"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->IMAGE_DIB:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 172
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink"

    invoke-direct {v0, v5, v1, v5, v5}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->SHEET_HYPERLINKS:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 176
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/oleObject"

    invoke-direct {v0, v5, v1, v5, v5}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->OLEEMBEDDINGS:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 178
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/package"

    invoke-direct {v0, v5, v1, v5, v5}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->PACKEMBEDDINGS:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 181
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "application/vnd.ms-office.vbaProject"

    const-string/jumbo v2, "http://schemas.microsoft.com/office/2006/relationships/vbaProject"

    const-string/jumbo v3, "/xl/vbaProject.bin"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->VBA_MACROS:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 185
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "application/vnd.ms-office.activeX+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/control"

    const-string/jumbo v3, "/xl/activeX/activeX#.xml"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->ACTIVEX_CONTROLS:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 189
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "application/vnd.ms-office.activeX"

    const-string/jumbo v2, "http://schemas.microsoft.com/office/2006/relationships/activeXControlBinary"

    const-string/jumbo v3, "/xl/activeX/activeX#.bin"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->ACTIVEX_BINS:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    .line 201
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.spreadsheetml.printerSettings"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/printerSettings"

    const-string/jumbo v3, "/xl/printerSettings/printerSettings#.bin"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->PRINTER_SETTINGS:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V
    .locals 1
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "rel"    # Ljava/lang/String;
    .param p3, "defaultName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/poi/POIXMLDocumentPart;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 208
    .local p4, "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/poi/POIXMLDocumentPart;>;"
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/poi/POIXMLRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    .line 210
    if-eqz p4, :cond_0

    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->_table:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 211
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->_table:Ljava/util/Map;

    invoke-interface {v0, p2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    :cond_0
    return-void
.end method

.method public static getInstance(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;
    .locals 1
    .param p0, "rel"    # Ljava/lang/String;

    .prologue
    .line 245
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->_table:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    return-object v0
.end method


# virtual methods
.method public getContents(Lorg/apache/poi/openxml4j/opc/PackagePart;)Ljava/io/InputStream;
    .locals 9
    .param p1, "corePart"    # Lorg/apache/poi/openxml4j/opc/PackagePart;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 220
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->_relation:Ljava/lang/String;

    invoke-virtual {p1, v5}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getRelationshipsByType(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    move-result-object v2

    .line 222
    .local v2, "prc":Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;
    invoke-virtual {v2}, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 223
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/openxml4j/opc/PackageRelationship;>;"
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 224
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    .line 225
    .local v3, "rel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    invoke-virtual {v3}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/poi/openxml4j/opc/PackagingURIHelper;->createPartName(Ljava/net/URI;)Lorg/apache/poi/openxml4j/opc/PackagePartName;

    move-result-object v4

    .line 227
    .local v4, "relName":Lorg/apache/poi/openxml4j/opc/PackagePartName;
    invoke-virtual {p1}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getPackage()Lorg/apache/poi/openxml4j/opc/OPCPackage;

    move-result-object v5

    invoke-virtual {v5, v4}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->getPart(Lorg/apache/poi/openxml4j/opc/PackagePartName;)Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v1

    .line 229
    .local v1, "part":Lorg/apache/poi/openxml4j/opc/PackagePart;
    if-eqz v1, :cond_0

    .line 230
    invoke-virtual {v1}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 233
    .end local v1    # "part":Lorg/apache/poi/openxml4j/opc/PackagePart;
    .end local v3    # "rel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .end local v4    # "relName":Lorg/apache/poi/openxml4j/opc/PackagePartName;
    :goto_0
    return-object v5

    .line 232
    :cond_0
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->log:Lorg/apache/poi/util/POILogger;

    const/4 v6, 0x5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "No part "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->_defaultName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " found"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 233
    const/4 v5, 0x0

    goto :goto_0
.end method
