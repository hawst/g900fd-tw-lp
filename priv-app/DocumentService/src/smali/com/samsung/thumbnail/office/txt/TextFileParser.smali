.class public Lcom/samsung/thumbnail/office/txt/TextFileParser;
.super Ljava/lang/Object;
.source "TextFileParser.java"


# static fields
.field private static final MAX_CHAR_COUNT:I = 0x600

.field private static final TEXT_FONT_SIZE:I = 0x28


# instance fields
.field cnsEleCr:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

.field private docPara:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

.field private firstPgCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

.field private run:Lcom/samsung/thumbnail/customview/word/Run;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    new-instance v1, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-direct {v1}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;-><init>()V

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;-><init>(Lcom/samsung/thumbnail/customview/hslf/SlideDimention;)V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/txt/TextFileParser;->cnsEleCr:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .line 41
    new-instance v0, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/txt/TextFileParser;->cnsEleCr:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;-><init>(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/txt/TextFileParser;->firstPgCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    .line 42
    return-void
.end method

.method private createPage(Landroid/content/Context;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v12, 0x41f00000    # 30.0f

    .line 149
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 151
    .local v3, "metrics":Landroid/util/DisplayMetrics;
    iget-object v7, p0, Lcom/samsung/thumbnail/office/txt/TextFileParser;->cnsEleCr:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v5

    .line 152
    .local v5, "slideDimention":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    iget v7, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-double v8, v7

    invoke-virtual {v5, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setSlideWidth(D)V

    .line 153
    iget v7, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v8, v7

    invoke-virtual {v5, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setSlideHeight(D)V

    .line 156
    const/16 v7, 0x2fd0

    invoke-static {v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v7

    float-to-int v7, v7

    int-to-long v8, v7

    invoke-virtual {v5, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setWordWidth(J)V

    .line 158
    const/16 v7, 0x3de0

    invoke-static {v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v7

    float-to-int v7, v7

    int-to-long v8, v7

    invoke-virtual {v5, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setWordHeight(J)V

    .line 160
    const/4 v6, 0x0

    .line 161
    .local v6, "width":I
    const/4 v2, 0x0

    .line 163
    .local v2, "height":I
    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getSlideWidth()D

    move-result-wide v8

    double-to-int v7, v8

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getSlideHeight()D

    move-result-wide v8

    double-to-int v8, v8

    if-ge v7, v8, :cond_0

    .line 165
    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getSlideWidth()D

    move-result-wide v8

    double-to-int v6, v8

    .line 166
    int-to-long v8, v6

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getWordHeight()J

    move-result-wide v10

    mul-long/2addr v8, v10

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getWordWidth()J

    move-result-wide v10

    div-long/2addr v8, v10

    long-to-int v2, v8

    .line 174
    :goto_0
    int-to-double v8, v6

    invoke-virtual {v5, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setCustomViewWidth(D)V

    .line 175
    int-to-double v8, v2

    invoke-virtual {v5, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setCustomViewHeight(D)V

    .line 177
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v2, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 180
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 181
    .local v1, "canvas":Landroid/graphics/Canvas;
    const/4 v7, -0x1

    invoke-virtual {v1, v7}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 183
    new-instance v4, Lcom/samsung/thumbnail/office/word/Page;

    invoke-direct {v4}, Lcom/samsung/thumbnail/office/word/Page;-><init>()V

    .line 184
    .local v4, "page":Lcom/samsung/thumbnail/office/word/Page;
    invoke-virtual {v4, v12}, Lcom/samsung/thumbnail/office/word/Page;->setLeftMargin(F)V

    .line 185
    invoke-virtual {v4, v12}, Lcom/samsung/thumbnail/office/word/Page;->setTopMargin(F)V

    .line 186
    invoke-virtual {v4, v12}, Lcom/samsung/thumbnail/office/word/Page;->setRightMargin(F)V

    .line 187
    invoke-virtual {v4, v12}, Lcom/samsung/thumbnail/office/word/Page;->setBottomMargin(F)V

    .line 188
    iget-object v7, p0, Lcom/samsung/thumbnail/office/txt/TextFileParser;->cnsEleCr:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v7, v4}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->addPage(Lcom/samsung/thumbnail/office/word/Page;)V

    .line 190
    iget-object v7, p0, Lcom/samsung/thumbnail/office/txt/TextFileParser;->firstPgCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    invoke-virtual {v7, v1}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->setCanvas(Landroid/graphics/Canvas;)V

    .line 191
    iget-object v7, p0, Lcom/samsung/thumbnail/office/txt/TextFileParser;->firstPgCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->setCanvasBitmap(Landroid/graphics/Bitmap;)V

    .line 192
    return-void

    .line 169
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    .end local v4    # "page":Lcom/samsung/thumbnail/office/word/Page;
    :cond_0
    const-wide v8, 0x3fe999999999999aL    # 0.8

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getSlideHeight()D

    move-result-wide v10

    mul-double/2addr v8, v10

    double-to-int v2, v8

    .line 170
    int-to-long v8, v2

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getWordWidth()J

    move-result-wide v10

    mul-long/2addr v8, v10

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getWordHeight()J

    move-result-wide v10

    div-long/2addr v8, v10

    long-to-int v6, v8

    goto :goto_0
.end method

.method private drawTextOnCanvas(Ljava/lang/String;Landroid/content/Context;)V
    .locals 8
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 109
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 110
    .local v2, "is":Ljava/io/InputStream;
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 113
    .local v0, "br":Ljava/io/BufferedReader;
    :goto_0
    :try_start_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .local v3, "strLine":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 115
    iget-object v4, p0, Lcom/samsung/thumbnail/office/txt/TextFileParser;->cnsEleCr:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iget-boolean v4, v4, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v4, :cond_2

    .line 133
    :cond_0
    if-eqz v2, :cond_1

    .line 134
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 139
    .end local v3    # "strLine":Ljava/lang/String;
    :cond_1
    :goto_1
    return-void

    .line 118
    .restart local v3    # "strLine":Ljava/lang/String;
    :cond_2
    :try_start_2
    new-instance v4, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-direct {v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    iput-object v4, p0, Lcom/samsung/thumbnail/office/txt/TextFileParser;->docPara:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .line 119
    new-instance v4, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-direct {v4}, Lcom/samsung/thumbnail/customview/word/Run;-><init>()V

    iput-object v4, p0, Lcom/samsung/thumbnail/office/txt/TextFileParser;->run:Lcom/samsung/thumbnail/customview/word/Run;

    .line 121
    iget-object v4, p0, Lcom/samsung/thumbnail/office/txt/TextFileParser;->run:Lcom/samsung/thumbnail/customview/word/Run;

    invoke-virtual {v4, v3}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    .line 122
    iget-object v4, p0, Lcom/samsung/thumbnail/office/txt/TextFileParser;->run:Lcom/samsung/thumbnail/customview/word/Run;

    const/high16 v5, 0x42200000    # 40.0f

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 124
    iget-object v4, p0, Lcom/samsung/thumbnail/office/txt/TextFileParser;->docPara:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    iget-object v5, p0, Lcom/samsung/thumbnail/office/txt/TextFileParser;->run:Lcom/samsung/thumbnail/customview/word/Run;

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 125
    iget-object v4, p0, Lcom/samsung/thumbnail/office/txt/TextFileParser;->docPara:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    sget-object v5, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->TXT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 126
    iget-object v4, p0, Lcom/samsung/thumbnail/office/txt/TextFileParser;->cnsEleCr:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/thumbnail/office/txt/TextFileParser;->docPara:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/office/word/Page;->addElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 127
    iget-object v4, p0, Lcom/samsung/thumbnail/office/txt/TextFileParser;->firstPgCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    invoke-virtual {v4, p2}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->drawOnCanvas(Landroid/content/Context;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 129
    .end local v3    # "strLine":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 130
    .local v1, "e":Ljava/io/IOException;
    :try_start_3
    const-string/jumbo v4, "DocumentService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 133
    if-eqz v2, :cond_1

    .line 134
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 135
    :catch_1
    move-exception v1

    .line 136
    const-string/jumbo v4, "DocumentService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 135
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v3    # "strLine":Ljava/lang/String;
    :catch_2
    move-exception v1

    .line 136
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v4, "DocumentService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 132
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "strLine":Ljava/lang/String;
    :catchall_0
    move-exception v4

    .line 133
    if-eqz v2, :cond_3

    .line 134
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 137
    :cond_3
    :goto_2
    throw v4

    .line 135
    :catch_3
    move-exception v1

    .line 136
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v5, "DocumentService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "IOException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method


# virtual methods
.method public getFirstPageCanvas()Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/thumbnail/office/txt/TextFileParser;->firstPgCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    return-object v0
.end method

.method public parseTxtFile(Ljava/io/File;Landroid/content/Context;)Z
    .locals 11
    .param p1, "file"    # Ljava/io/File;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    .line 60
    invoke-direct {p0, p2}, Lcom/samsung/thumbnail/office/txt/TextFileParser;->createPage(Landroid/content/Context;)V

    .line 61
    const/4 v4, 0x0

    .line 63
    .local v4, "fstream":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    .end local v4    # "fstream":Ljava/io/FileInputStream;
    .local v5, "fstream":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/InputStreamReader;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/index/Utils;->getCharsetFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v5, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v0, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 67
    .local v0, "br":Ljava/io/BufferedReader;
    const/16 v8, 0x600

    new-array v2, v8, [C

    .line 74
    .local v2, "dataArr":[C
    const/4 v8, 0x0

    const/16 v9, 0x600

    invoke-virtual {v0, v2, v8, v9}, Ljava/io/BufferedReader;->read([CII)I

    move-result v1

    .line 76
    .local v1, "currentLen":I
    if-lez v1, :cond_0

    .line 77
    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v8, 0x601

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 78
    .local v6, "strBuilder":Ljava/lang/StringBuilder;
    const/4 v8, 0x0

    invoke-virtual {v6, v2, v8, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 79
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8, p2}, Lcom/samsung/thumbnail/office/txt/TextFileParser;->drawTextOnCanvas(Ljava/lang/String;Landroid/content/Context;)V

    .line 80
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->setLength(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 87
    .end local v6    # "strBuilder":Ljava/lang/StringBuilder;
    :cond_0
    if-eqz v5, :cond_1

    .line 88
    :try_start_2
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 94
    :cond_1
    :goto_0
    const/4 v7, 0x1

    move-object v4, v5

    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v1    # "currentLen":I
    .end local v2    # "dataArr":[C
    .end local v5    # "fstream":Ljava/io/FileInputStream;
    .restart local v4    # "fstream":Ljava/io/FileInputStream;
    :cond_2
    :goto_1
    return v7

    .line 89
    .end local v4    # "fstream":Ljava/io/FileInputStream;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "currentLen":I
    .restart local v2    # "dataArr":[C
    .restart local v5    # "fstream":Ljava/io/FileInputStream;
    :catch_0
    move-exception v3

    .line 90
    .local v3, "e":Ljava/io/IOException;
    const-string/jumbo v7, "DocumentService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "IOException: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 82
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v1    # "currentLen":I
    .end local v2    # "dataArr":[C
    .end local v3    # "e":Ljava/io/IOException;
    .end local v5    # "fstream":Ljava/io/FileInputStream;
    .restart local v4    # "fstream":Ljava/io/FileInputStream;
    :catch_1
    move-exception v3

    .line 83
    .local v3, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 87
    if-eqz v4, :cond_2

    .line 88
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 89
    :catch_2
    move-exception v3

    .line 90
    .local v3, "e":Ljava/io/IOException;
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "IOException: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 86
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 87
    :goto_3
    if-eqz v4, :cond_3

    .line 88
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 91
    :cond_3
    :goto_4
    throw v7

    .line 89
    :catch_3
    move-exception v3

    .line 90
    .restart local v3    # "e":Ljava/io/IOException;
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "IOException: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 86
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "fstream":Ljava/io/FileInputStream;
    .restart local v5    # "fstream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v7

    move-object v4, v5

    .end local v5    # "fstream":Ljava/io/FileInputStream;
    .restart local v4    # "fstream":Ljava/io/FileInputStream;
    goto :goto_3

    .line 82
    .end local v4    # "fstream":Ljava/io/FileInputStream;
    .restart local v5    # "fstream":Ljava/io/FileInputStream;
    :catch_4
    move-exception v3

    move-object v4, v5

    .end local v5    # "fstream":Ljava/io/FileInputStream;
    .restart local v4    # "fstream":Ljava/io/FileInputStream;
    goto :goto_2
.end method
