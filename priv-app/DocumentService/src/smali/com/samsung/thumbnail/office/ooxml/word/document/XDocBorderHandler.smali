.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XDocBorderHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;
    }
.end annotation


# instance fields
.field private borderObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;)V
    .locals 0
    .param p1, "elementName"    # Ljava/lang/String;
    .param p2, "borderObserver"    # Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 21
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;->borderObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;

    .line 22
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 6
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 27
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;-><init>()V

    .line 28
    .local v0, "border":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    invoke-virtual {p0, p2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v3

    .line 29
    .local v3, "ele":Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    move-result-object v1

    .line 31
    .local v1, "borderType":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    const-string/jumbo v5, "val"

    invoke-virtual {p0, p3, v5, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v4

    .line 32
    .local v4, "value":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 33
    invoke-static {v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->setBorderStyle(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;)V

    .line 36
    :cond_0
    const-string/jumbo v5, "color"

    invoke-virtual {p0, p3, v5, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v4

    .line 37
    if-eqz v4, :cond_1

    .line 38
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 39
    .local v2, "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    const-string/jumbo v5, "auto"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 40
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setAuto(Z)V

    .line 45
    :goto_0
    const-string/jumbo v5, "themeColor"

    invoke-virtual {p0, p3, v5, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v4

    .line 46
    invoke-virtual {v2, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setThemeColor(Ljava/lang/String;)V

    .line 47
    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->setBorderColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    .line 50
    .end local v2    # "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    :cond_1
    const-string/jumbo v5, "sz"

    invoke-virtual {p0, p3, v5, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v4

    .line 51
    if-eqz v4, :cond_2

    .line 52
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->setBorderSize(I)V

    .line 55
    :cond_2
    const-string/jumbo v5, "space"

    invoke-virtual {p0, p3, v5, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v4

    .line 56
    if-eqz v4, :cond_3

    .line 57
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->setBorderSpace(I)V

    .line 60
    :cond_3
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler;->borderObserver:Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;

    invoke-interface {v5, v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBorderHandler$IBorderObserver;->setBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    .line 61
    return-void

    .line 42
    .restart local v2    # "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    :cond_4
    invoke-virtual {v2, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    goto :goto_0
.end method
