.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/ColorSchemeHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "ColorSchemeHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;


# direct methods
.method public constructor <init>()V
    .locals 29

    .prologue
    .line 19
    const-string/jumbo v28, "clrScheme"

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 21
    const/16 v28, 0xc

    move/from16 v0, v28

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    move-object/from16 v27, v0

    .line 23
    .local v27, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v15, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;

    const-string/jumbo v28, "dk1"

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    invoke-direct {v15, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;)V

    .line 25
    .local v15, "dk1Handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;
    new-instance v16, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v28, "dk1"

    move-object/from16 v0, v16

    move-object/from16 v1, v28

    invoke-direct {v0, v1, v15}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 27
    .local v16, "dk1Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v28, 0x0

    aput-object v16, v27, v28

    .line 29
    new-instance v23, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;

    const-string/jumbo v28, "lt1"

    move-object/from16 v0, v23

    move-object/from16 v1, v28

    move-object/from16 v2, p0

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;)V

    .line 31
    .local v23, "lt1Handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;
    new-instance v24, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v28, "lt1"

    move-object/from16 v0, v24

    move-object/from16 v1, v28

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 33
    .local v24, "lt1Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v28, 0x1

    aput-object v24, v27, v28

    .line 35
    new-instance v17, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;

    const-string/jumbo v28, "dk2"

    move-object/from16 v0, v17

    move-object/from16 v1, v28

    move-object/from16 v2, p0

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;)V

    .line 37
    .local v17, "dk2Handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v28, "dk2"

    move-object/from16 v0, v18

    move-object/from16 v1, v28

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 39
    .local v18, "dk2Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v28, 0x2

    aput-object v18, v27, v28

    .line 41
    new-instance v25, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;

    const-string/jumbo v28, "lt2"

    move-object/from16 v0, v25

    move-object/from16 v1, v28

    move-object/from16 v2, p0

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;)V

    .line 43
    .local v25, "lt2Handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;
    new-instance v26, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v28, "lt2"

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 45
    .local v26, "lt2Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v28, 0x3

    aput-object v26, v27, v28

    .line 47
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;

    const-string/jumbo v28, "accent1"

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    invoke-direct {v3, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;)V

    .line 49
    .local v3, "accent1Handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v28, "accent1"

    move-object/from16 v0, v28

    invoke-direct {v4, v0, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 51
    .local v4, "accent1Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v28, 0x4

    aput-object v4, v27, v28

    .line 53
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;

    const-string/jumbo v28, "accent2"

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    invoke-direct {v5, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;)V

    .line 55
    .local v5, "accent2Handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v28, "accent2"

    move-object/from16 v0, v28

    invoke-direct {v6, v0, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 57
    .local v6, "accent2Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v28, 0x5

    aput-object v6, v27, v28

    .line 59
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;

    const-string/jumbo v28, "accent3"

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    invoke-direct {v7, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;)V

    .line 61
    .local v7, "accent3Handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v28, "accent3"

    move-object/from16 v0, v28

    invoke-direct {v8, v0, v7}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 63
    .local v8, "accent3Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v28, 0x6

    aput-object v8, v27, v28

    .line 65
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;

    const-string/jumbo v28, "accent4"

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    invoke-direct {v9, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;)V

    .line 67
    .local v9, "accent4Handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v28, "accent4"

    move-object/from16 v0, v28

    invoke-direct {v10, v0, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 69
    .local v10, "accent4Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v28, 0x7

    aput-object v10, v27, v28

    .line 71
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;

    const-string/jumbo v28, "accent5"

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    invoke-direct {v11, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;)V

    .line 73
    .local v11, "accent5Handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v28, "accent5"

    move-object/from16 v0, v28

    invoke-direct {v12, v0, v11}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 75
    .local v12, "accent5Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v28, 0x8

    aput-object v12, v27, v28

    .line 77
    new-instance v13, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;

    const-string/jumbo v28, "accent6"

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    invoke-direct {v13, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;)V

    .line 79
    .local v13, "accent6Handler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;
    new-instance v14, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v28, "accent6"

    move-object/from16 v0, v28

    invoke-direct {v14, v0, v13}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 81
    .local v14, "accent6Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v28, 0x9

    aput-object v14, v27, v28

    .line 83
    new-instance v21, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;

    const-string/jumbo v28, "hlink"

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    move-object/from16 v2, p0

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;)V

    .line 85
    .local v21, "hLinkHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;
    new-instance v22, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v28, "hlink"

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 87
    .local v22, "hLinkSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v28, 0xa

    aput-object v22, v27, v28

    .line 89
    new-instance v19, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;

    const-string/jumbo v28, "folHlink"

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    move-object/from16 v2, p0

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;)V

    .line 91
    .local v19, "folHLinkHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler;
    new-instance v20, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v28, "folHlink"

    move-object/from16 v0, v20

    move-object/from16 v1, v28

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 93
    .local v20, "folHLinkSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v28, 0xb

    aput-object v20, v27, v28

    .line 95
    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ColorSchemeHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 96
    return-void
.end method


# virtual methods
.method public consumeColor(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p1, "clrScheme"    # Ljava/lang/String;
    .param p2, "val"    # Ljava/lang/String;
    .param p3, "fillProp"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    .param p4, "Parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 101
    invoke-virtual {p4}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->addColorSchema(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    invoke-virtual {p4}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->addExcelColorSchema(Ljava/lang/String;)V

    .line 103
    return-void
.end method
