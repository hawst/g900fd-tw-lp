.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualPropHandlers;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "NonVisualPropHandlers.java"


# instance fields
.field private shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 1
    .param p1, "shapeProperties"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 21
    const-string/jumbo v0, "nvPr"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 22
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualPropHandlers;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 23
    return-void
.end method


# virtual methods
.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 8
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 28
    invoke-virtual {p0, p2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualPropHandlers;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 30
    .local v0, "elementName":Ljava/lang/String;
    const-string/jumbo v5, "ph"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 31
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;-><init>()V

    .line 33
    .local v2, "placeHolder":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;
    const-string/jumbo v5, "type"

    invoke-virtual {p0, p3, v5, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualPropHandlers;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v3

    .line 35
    .local v3, "placeHolderType":Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    move-result-object v5

    iput-object v5, v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;->phType:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$EPlaceHolderType;

    .line 37
    const-string/jumbo v5, "idx"

    invoke-virtual {p0, p3, v5, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualPropHandlers;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 38
    .local v1, "phIndex":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 41
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;->phIndex:J

    .line 44
    :cond_0
    const-string/jumbo v5, "sz"

    invoke-virtual {p0, p3, v5, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualPropHandlers;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v4

    .line 46
    .local v4, "size":Ljava/lang/String;
    const-string/jumbo v5, "half"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 47
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder$Size;->HALF:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder$Size;

    iput-object v5, v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;->size:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder$Size;

    .line 76
    :cond_1
    :goto_0
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/NonVisualPropHandlers;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v5, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setPlaceHolder(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;)V

    .line 78
    .end local v1    # "phIndex":Ljava/lang/String;
    .end local v2    # "placeHolder":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;
    .end local v3    # "placeHolderType":Ljava/lang/String;
    .end local v4    # "size":Ljava/lang/String;
    :cond_2
    return-void

    .line 48
    .restart local v1    # "phIndex":Ljava/lang/String;
    .restart local v2    # "placeHolder":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;
    .restart local v3    # "placeHolderType":Ljava/lang/String;
    .restart local v4    # "size":Ljava/lang/String;
    :cond_3
    const-string/jumbo v5, "full"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 49
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder$Size;->FULL:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder$Size;

    iput-object v5, v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;->size:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder$Size;

    goto :goto_0

    .line 50
    :cond_4
    const-string/jumbo v5, "quarter"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 51
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder$Size;->QUARTER:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder$Size;

    iput-object v5, v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;->size:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder$Size;

    goto :goto_0
.end method
