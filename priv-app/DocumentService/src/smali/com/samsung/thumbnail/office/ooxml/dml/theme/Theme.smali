.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
.super Lorg/apache/poi/POIXMLDocumentPart;
.source "Theme.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;


# instance fields
.field protected bgFill:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;",
            ">;"
        }
    .end annotation
.end field

.field private bgFillProp:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

.field private bgRelIdLst:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private bgTileRel:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field protected colorMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected colorSchema:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private file:Ljava/io/File;

.field private fillProp:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;",
            ">;"
        }
    .end annotation
.end field

.field private fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

.field protected majorFont:Ljava/lang/String;

.field protected minorFont:Ljava/lang/String;

.field private packPart:Lorg/apache/poi/openxml4j/opc/PackagePart;

.field private parsed:Z

.field private relId:Ljava/lang/String;

.field private themeName:Ljava/lang/String;

.field private themergbclr:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lorg/apache/poi/POIXMLDocumentPart;-><init>()V

    .line 52
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->init()V

    .line 53
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/POIXMLDocumentPart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V
    .locals 1
    .param p1, "part"    # Lorg/apache/poi/POIXMLDocumentPart;
    .param p2, "rel"    # Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    invoke-virtual {p1}, Lorg/apache/poi/POIXMLDocumentPart;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/poi/POIXMLDocumentPart;-><init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V

    move-object v0, p1

    .line 64
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->bgFill:Ljava/util/List;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->bgFill:Ljava/util/List;

    move-object v0, p1

    .line 65
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->colorSchema:Ljava/util/Map;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->colorSchema:Ljava/util/Map;

    move-object v0, p1

    .line 66
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->colorMap:Ljava/util/Map;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->colorMap:Ljava/util/Map;

    move-object v0, p1

    .line 67
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->majorFont:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->majorFont:Ljava/lang/String;

    move-object v0, p1

    .line 68
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->minorFont:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->minorFont:Ljava/lang/String;

    .line 69
    check-cast p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .end local p1    # "part":Lorg/apache/poi/POIXMLDocumentPart;
    iget-boolean v0, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->parsed:Z

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->parsed:Z

    .line 70
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->init()V

    .line 71
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V
    .locals 0
    .param p1, "part"    # Lorg/apache/poi/openxml4j/opc/PackagePart;
    .param p2, "rel"    # Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/POIXMLDocumentPart;-><init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V

    .line 57
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->packPart:Lorg/apache/poi/openxml4j/opc/PackagePart;

    .line 58
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->init()V

    .line 59
    return-void
.end method

.method private init()V
    .locals 1

    .prologue
    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->bgFill:Ljava/util/List;

    .line 75
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->colorSchema:Ljava/util/Map;

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->fillProp:Ljava/util/ArrayList;

    .line 77
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->bgTileRel:Ljava/util/HashMap;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->bgRelIdLst:Ljava/util/ArrayList;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->themergbclr:Ljava/util/ArrayList;

    .line 80
    return-void
.end method


# virtual methods
.method public addBGFill(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 1
    .param p1, "fillProps"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 203
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->bgFill:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    return-void
.end method

.method public addColorSchema(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "clrSchma"    # Ljava/lang/String;
    .param p2, "val"    # Ljava/lang/String;

    .prologue
    .line 175
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->colorSchema:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    return-void
.end method

.method public addExcelColorSchema(Ljava/lang/String;)V
    .locals 1
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 167
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->themergbclr:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    return-void
.end method

.method public addFillProp(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 1
    .param p1, "fillProps"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 211
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->fillProp:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 212
    return-void
.end method

.method public consumeFill(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 0
    .param p1, "fillProperties"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 274
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .line 275
    return-void
.end method

.method public getBGFill()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;",
            ">;"
        }
    .end annotation

    .prologue
    .line 207
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->bgFill:Ljava/util/List;

    return-object v0
.end method

.method public getBgBlipFillIdLst()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->bgRelIdLst:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getBgFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->bgFillProp:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    return-object v0
.end method

.method public getBgPictureData(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 88
    const/4 v0, 0x0

    .line 90
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->packPart:Lorg/apache/poi/openxml4j/opc/PackagePart;

    if-eqz v3, :cond_0

    .line 94
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getRelationById(Ljava/lang/String;)Lorg/apache/poi/POIXMLDocumentPart;

    move-result-object v1

    .line 95
    .local v1, "docPart":Lorg/apache/poi/POIXMLDocumentPart;
    instance-of v3, v1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    if-eqz v3, :cond_0

    move-object v2, v1

    .line 96
    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    .line 97
    .local v2, "picData":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->file:Ljava/io/File;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->setFile(Ljava/io/File;)V

    .line 98
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->getData()[B

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->getData()[B

    move-result-object v5

    array-length v5, v5

    invoke-static {v3, v4, v5}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 103
    .end local v1    # "docPart":Lorg/apache/poi/POIXMLDocumentPart;
    .end local v2    # "picData":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;
    :cond_0
    return-object v0
.end method

.method public getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "color"    # Ljava/lang/String;

    .prologue
    .line 192
    move-object v0, p1

    .line 193
    .local v0, "str":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->colorMap:Ljava/util/Map;

    if-eqz v1, :cond_0

    .line 194
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->colorMap:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "str":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 195
    .restart local v0    # "str":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 196
    move-object v0, p1

    .line 199
    :cond_0
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->colorSchema:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    return-object v1
.end method

.method public getColorSchema()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 179
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->colorSchema:Ljava/util/Map;

    return-object v0
.end method

.method public getExcelColorSchema()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 171
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->themergbclr:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    return-object v0
.end method

.method public getFillPropsLst()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;",
            ">;"
        }
    .end annotation

    .prologue
    .line 215
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->fillProp:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getFontName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "font"    # Ljava/lang/String;

    .prologue
    .line 261
    const/4 v0, 0x0

    .line 262
    .local v0, "fontName":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 263
    const-string/jumbo v1, "minor"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 264
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->minorFont:Ljava/lang/String;

    .line 269
    :cond_0
    :goto_0
    return-object v0

    .line 265
    :cond_1
    const-string/jumbo v1, "major"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 266
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->majorFont:Ljava/lang/String;

    goto :goto_0
.end method

.method public getMajorFont()Ljava/lang/String;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->majorFont:Ljava/lang/String;

    return-object v0
.end method

.method public getMinorFont()Ljava/lang/String;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->minorFont:Ljava/lang/String;

    return-object v0
.end method

.method public getPicture()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x0

    return-object v0
.end method

.method public getThemeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->themeName:Ljava/lang/String;

    return-object v0
.end method

.method public isBgBlipTile(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 152
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->bgTileRel:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->bgTileRel:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 155
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public parseTheme()V
    .locals 6

    .prologue
    .line 107
    iget-boolean v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->parsed:Z

    if-eqz v2, :cond_1

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    const/4 v1, 0x0

    .line 113
    .local v1, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 115
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeParser;

    invoke-direct {v2, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeParser;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeParser;->parse(Ljava/io/InputStream;)V

    .line 116
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->parsed:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    if-eqz v1, :cond_0

    .line 122
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 123
    :catch_0
    move-exception v0

    .line 124
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 117
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 118
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_2
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 120
    if-eqz v1, :cond_0

    .line 122
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 123
    :catch_2
    move-exception v0

    .line 124
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 120
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    if-eqz v1, :cond_2

    .line 122
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 125
    :cond_2
    :goto_1
    throw v2

    .line 123
    :catch_3
    move-exception v0

    .line 124
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setBgBlipFillId(Ljava/lang/String;)V
    .locals 1
    .param p1, "relId"    # Ljava/lang/String;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->bgRelIdLst:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    return-void
.end method

.method public setBgFillProp(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 0
    .param p1, "bgFillProp"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 219
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->bgFillProp:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .line 220
    return-void
.end method

.method public setBlipFill(Ljava/lang/String;)V
    .locals 0
    .param p1, "relId"    # Ljava/lang/String;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->relId:Ljava/lang/String;

    .line 141
    return-void
.end method

.method public setColorMap(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 183
    .local p1, "colorMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->colorMap:Ljava/util/Map;

    .line 184
    return-void
.end method

.method public setFile(Ljava/io/File;)V
    .locals 0
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->file:Ljava/io/File;

    .line 84
    return-void
.end method

.method public setIsBgBlipTile(Z)V
    .locals 3
    .param p1, "isBgTile"    # Z

    .prologue
    .line 159
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->bgTileRel:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->bgRelIdLst:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->bgRelIdLst:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    return-void
.end method

.method public setMajorFont(Ljava/lang/String;)V
    .locals 0
    .param p1, "fontTypeface"    # Ljava/lang/String;

    .prologue
    .line 253
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->majorFont:Ljava/lang/String;

    .line 254
    return-void
.end method

.method public setMinorFont(Ljava/lang/String;)V
    .locals 0
    .param p1, "fontTypeface"    # Ljava/lang/String;

    .prologue
    .line 257
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->minorFont:Ljava/lang/String;

    .line 258
    return-void
.end method

.method public setThemeName(Ljava/lang/String;)V
    .locals 0
    .param p1, "themeName"    # Ljava/lang/String;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->themeName:Ljava/lang/String;

    .line 133
    return-void
.end method
