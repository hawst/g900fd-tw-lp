.class public final enum Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;
.super Ljava/lang/Enum;
.source "XDocTblCellProperties.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VMerge"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

.field public static final enum CONTINUE:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

.field public static final enum RESTART:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 144
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    const-string/jumbo v1, "CONTINUE"

    const-string/jumbo v2, "continue"

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;->CONTINUE:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    const-string/jumbo v1, "RESTART"

    const-string/jumbo v2, "restart"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;->RESTART:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    .line 143
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;->CONTINUE:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;->RESTART:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 148
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 149
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;->value:Ljava/lang/String;

    .line 150
    return-void
.end method

.method public static convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 157
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;->values()[Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 158
    .local v2, "inst":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 162
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;
    :goto_1
    return-object v2

    .line 157
    .restart local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 162
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 143
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;
    .locals 1

    .prologue
    .line 143
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    return-object v0
.end method


# virtual methods
.method public xmlValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;->value:Ljava/lang/String;

    return-object v0
.end method
