.class public Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XPPTGraphicHandler.java"


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;)V
    .locals 9
    .param p1, "graphicFrame"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;

    .prologue
    .line 23
    const/16 v7, 0x1f

    const-string/jumbo v8, "graphicData"

    invoke-direct {p0, v7, v8}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 25
    const/4 v7, 0x3

    new-array v4, v7, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 27
    .local v4, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableHandler;

    invoke-direct {v5, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;)V

    .line 28
    .local v5, "tableHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableHandler;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v7, "tbl"

    invoke-direct {v6, v7, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 30
    .local v6, "tableSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v7, 0x0

    aput-object v6, v4, v7

    .line 32
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTChartHandler;

    invoke-direct {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTChartHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;)V

    .line 33
    .local v0, "chartHandler":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTChartHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    const/16 v7, 0x23

    const-string/jumbo v8, "chart"

    invoke-direct {v1, v7, v8, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 36
    .local v1, "chartSeq":Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    const/4 v7, 0x1

    aput-object v1, v4, v7

    .line 38
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramRelIdsHandler;

    const/16 v7, 0xca

    invoke-direct {v2, v7, p1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramRelIdsHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;)V

    .line 40
    .local v2, "dgmHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramRelIdsHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DgmSeqDescriptor;

    const-string/jumbo v7, "relIds"

    invoke-direct {v3, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DgmSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 42
    .local v3, "dgmSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DgmSeqDescriptor;
    const/4 v7, 0x2

    aput-object v3, v4, v7

    .line 44
    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 45
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 50
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 51
    return-void
.end method
