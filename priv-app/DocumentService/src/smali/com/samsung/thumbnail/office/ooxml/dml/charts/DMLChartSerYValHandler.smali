.class public Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerYValHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DMLChartSerYValHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;


# instance fields
.field private seriesVal:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    .line 28
    const/16 v3, 0x23

    const-string/jumbo v4, "yVal"

    invoke-direct {p0, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 30
    const/4 v3, 0x1

    new-array v2, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 32
    .local v2, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumRefHandler;

    invoke-direct {v0, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumRefHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;)V

    .line 33
    .local v0, "numrefHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumRefHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v3, "numRef"

    invoke-direct {v1, v3, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 35
    .local v1, "numrefSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/4 v3, 0x0

    aput-object v1, v2, v3

    .line 37
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerYValHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 38
    return-void
.end method


# virtual methods
.method public chartNumVal(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "val"    # Ljava/lang/String;

    .prologue
    .line 61
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->addChartYVal(Ljava/lang/String;)V

    .line 62
    return-void
.end method

.method public chartStrVal(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "str"    # Ljava/lang/String;

    .prologue
    .line 68
    return-void
.end method

.method public setFormatCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "formatCode"    # Ljava/lang/String;

    .prologue
    .line 86
    return-void
.end method

.method public setPtVal(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "strVal"    # Ljava/lang/String;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerYValHandler;->seriesVal:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->addPtVal(Ljava/lang/String;)V

    .line 74
    return-void
.end method

.method public setRefType(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;I)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "refType"    # I

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerYValHandler;->seriesVal:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->setRefType(I)V

    .line 79
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 42
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 45
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    if-eqz v0, :cond_0

    .line 46
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerYValHandler;->seriesVal:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    .line 47
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getCurSeries()Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    move-result-object v0

    const-string/jumbo v1, "val"

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerYValHandler;->seriesVal:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->putSeriesVal(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;)V

    .line 52
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    if-eqz v0, :cond_1

    .line 53
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerYValHandler;->seriesVal:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    .line 54
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getCurSeries()Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    move-result-object v0

    const-string/jumbo v1, "val"

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerYValHandler;->seriesVal:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->putSeriesVal(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;)V

    .line 57
    :cond_1
    return-void
.end method
