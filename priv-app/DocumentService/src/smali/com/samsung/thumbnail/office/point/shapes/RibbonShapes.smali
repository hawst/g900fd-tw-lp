.class public Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;
.super Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;
.source "RibbonShapes.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mDrawOnCanvas:Z

.field private mLeftMarg:I

.field private mTopMarg:I

.field private rectF:Landroid/graphics/RectF;

.field private startA:F

.field private sweepA:F


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 2
    .param p1, "folderPath"    # Ljava/io/File;

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;-><init>()V

    .line 29
    const-string/jumbo v0, "RibbonShapes"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->TAG:Ljava/lang/String;

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mDrawOnCanvas:Z

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->rectF:Landroid/graphics/RectF;

    .line 35
    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->startA:F

    .line 36
    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    .line 39
    iput-object p1, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->folderPath:Ljava/io/File;

    .line 40
    return-void
.end method

.method private initArc(FFFFFFFF)V
    .locals 22
    .param p1, "left"    # F
    .param p2, "top"    # F
    .param p3, "right"    # F
    .param p4, "bottom"    # F
    .param p5, "xStart"    # F
    .param p6, "yStart"    # F
    .param p7, "xEnd"    # F
    .param p8, "yEnd"    # F

    .prologue
    .line 719
    new-instance v5, Landroid/graphics/RectF;

    move/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    invoke-direct {v5, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->rectF:Landroid/graphics/RectF;

    .line 720
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->rectF:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->centerX()F

    move-result v5

    float-to-double v10, v5

    .line 721
    .local v10, "xc":D
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->rectF:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    float-to-double v0, v5

    move-wide/from16 v16, v0

    .line 722
    .local v16, "yc":D
    move/from16 v0, p5

    float-to-double v6, v0

    .line 723
    .local v6, "x1":D
    move/from16 v0, p6

    float-to-double v12, v0

    .line 724
    .local v12, "y1":D
    sub-double v18, v12, v16

    sub-double v20, v6, v10

    invoke-static/range {v18 .. v21}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-float v5, v0

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->startA:F

    .line 725
    move/from16 v0, p7

    float-to-double v8, v0

    .line 726
    .local v8, "x2":D
    move/from16 v0, p8

    float-to-double v14, v0

    .line 727
    .local v14, "y2":D
    sub-double v18, v14, v16

    sub-double v20, v8, v10

    invoke-static/range {v18 .. v21}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-float v4, v0

    .line 728
    .local v4, "endA":F
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->startA:F

    sub-float v5, v4, v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    .line 729
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    const/16 v18, 0x0

    cmpl-float v5, v5, v18

    if-lez v5, :cond_0

    .line 730
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    const/high16 v18, 0x43b40000    # 360.0f

    sub-float v5, v5, v18

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    .line 732
    :cond_0
    return-void
.end method

.method private initClockwiseArc(FFFFFFFF)V
    .locals 22
    .param p1, "left"    # F
    .param p2, "top"    # F
    .param p3, "right"    # F
    .param p4, "bottom"    # F
    .param p5, "xStart"    # F
    .param p6, "yStart"    # F
    .param p7, "xEnd"    # F
    .param p8, "yEnd"    # F

    .prologue
    .line 702
    new-instance v5, Landroid/graphics/RectF;

    move/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    invoke-direct {v5, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->rectF:Landroid/graphics/RectF;

    .line 703
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->rectF:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->centerX()F

    move-result v5

    float-to-double v10, v5

    .line 704
    .local v10, "xc":D
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->rectF:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    float-to-double v0, v5

    move-wide/from16 v16, v0

    .line 705
    .local v16, "yc":D
    move/from16 v0, p5

    float-to-double v6, v0

    .line 706
    .local v6, "x1":D
    move/from16 v0, p6

    float-to-double v12, v0

    .line 707
    .local v12, "y1":D
    sub-double v18, v12, v16

    sub-double v20, v6, v10

    invoke-static/range {v18 .. v21}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-float v5, v0

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->startA:F

    .line 708
    move/from16 v0, p7

    float-to-double v8, v0

    .line 709
    .local v8, "x2":D
    move/from16 v0, p8

    float-to-double v14, v0

    .line 710
    .local v14, "y2":D
    sub-double v18, v14, v16

    sub-double v20, v8, v10

    invoke-static/range {v18 .. v21}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-float v4, v0

    .line 711
    .local v4, "endA":F
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->startA:F

    sub-float v5, v4, v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    .line 712
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    const/16 v18, 0x0

    cmpg-float v5, v5, v18

    if-gez v5, :cond_0

    .line 713
    const/high16 v5, 0x43b40000    # 360.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    move/from16 v18, v0

    add-float v5, v5, v18

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    .line 715
    :cond_0
    return-void
.end method


# virtual methods
.method protected draw(Landroid/graphics/Canvas;I)V
    .locals 119
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeCount"    # I

    .prologue
    .line 160
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    const v9, 0x46a8c000    # 21600.0f

    div-float v115, v4, v9

    .line 161
    .local v115, "mulFactor_Width":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const v9, 0x46a8c000    # 21600.0f

    div-float v114, v4, v9

    .line 163
    .local v114, "mulFactor_Height":F
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v9, "Ribbon2"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 173
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval0:I

    if-nez v4, :cond_0

    .line 174
    const/16 v4, 0x1518

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval0:I

    .line 175
    :cond_0
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval1:I

    if-nez v4, :cond_1

    .line 176
    const/16 v4, 0x49d4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval1:I

    .line 178
    :cond_1
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval0:I

    int-to-float v0, v4

    move/from16 v101, v0

    .line 179
    .local v101, "at0":F
    const v4, 0x4428c000    # 675.0f

    add-float v102, v101, v4

    .line 180
    .local v102, "at1":F
    const v4, 0x4428c000    # 675.0f

    add-float v107, v102, v4

    .line 181
    .local v107, "at2":F
    const v4, 0x4428c000    # 675.0f

    add-float v28, v107, v4

    .line 182
    .local v28, "at3":F
    const v4, 0x4428c000    # 675.0f

    add-float v48, v28, v4

    .line 183
    .local v48, "at4":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval1:I

    int-to-float v0, v4

    move/from16 v78, v0

    .line 184
    .local v78, "at10":F
    const/high16 v4, 0x3f800000    # 1.0f

    mul-float v4, v4, v78

    const/high16 v9, 0x40800000    # 4.0f

    div-float v17, v4, v9

    .line 185
    .local v17, "at11":F
    const/high16 v4, 0x3f800000    # 1.0f

    mul-float v4, v4, v78

    const/high16 v9, 0x40000000    # 2.0f

    div-float v103, v4, v9

    .line 186
    .local v103, "at12":F
    const/high16 v4, 0x40400000    # 3.0f

    mul-float v4, v4, v78

    const/high16 v9, 0x40800000    # 4.0f

    div-float v69, v4, v9

    .line 188
    .local v69, "at13":F
    new-instance v117, Landroid/graphics/Path;

    invoke-direct/range {v117 .. v117}, Landroid/graphics/Path;-><init>()V

    .line 192
    .local v117, "path":Landroid/graphics/Path;
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 193
    mul-float v4, v28, v115

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 194
    mul-float v4, v48, v115

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float v13, v48, v115

    mul-float v14, v17, v114

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const/high16 v16, 0x40400000    # 3.0f

    mul-float v15, v15, v16

    const/high16 v16, 0x40800000    # 4.0f

    div-float v15, v15, v16

    add-float/2addr v14, v15

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13, v14}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 196
    mul-float v4, v48, v115

    mul-float v9, v78, v114

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 197
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v48, v115

    sub-float/2addr v4, v9

    mul-float v9, v78, v114

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 199
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v48, v115

    sub-float/2addr v4, v9

    mul-float v9, v17, v114

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const/high16 v14, 0x40400000    # 3.0f

    mul-float/2addr v13, v14

    const/high16 v14, 0x40800000    # 4.0f

    div-float/2addr v13, v14

    add-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 201
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v48, v115

    sub-float/2addr v4, v9

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v14, v28, v115

    sub-float/2addr v13, v14

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13, v14}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 203
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 204
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    const v9, 0x4528c000    # 2700.0f

    mul-float v9, v9, v115

    sub-float/2addr v4, v9

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const/high16 v13, 0x40400000    # 3.0f

    mul-float/2addr v9, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v9, v13

    mul-float v13, v103, v114

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    add-float/2addr v13, v14

    sub-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 206
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float v13, v78, v114

    sub-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 207
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v101, v115

    sub-float/2addr v4, v9

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float v13, v78, v114

    sub-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 209
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v101, v115

    sub-float/2addr v4, v9

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float v13, v78, v114

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const/high16 v15, 0x40400000    # 3.0f

    mul-float/2addr v14, v15

    const/high16 v15, 0x40800000    # 4.0f

    div-float/2addr v14, v15

    add-float/2addr v13, v14

    sub-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 211
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v101, v115

    sub-float/2addr v4, v9

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v14, v102, v115

    sub-float/2addr v13, v14

    const/4 v14, 0x0

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13, v14}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 213
    mul-float v4, v102, v115

    const/4 v9, 0x0

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 214
    mul-float v4, v101, v115

    const/4 v9, 0x0

    mul-float v13, v101, v115

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float v15, v78, v114

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    move/from16 v16, v0

    const/high16 v18, 0x40400000    # 3.0f

    mul-float v16, v16, v18

    const/high16 v18, 0x40800000    # 4.0f

    div-float v16, v16, v18

    add-float v15, v15, v16

    sub-float/2addr v14, v15

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13, v14}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 216
    mul-float v4, v101, v115

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float v13, v78, v114

    sub-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 218
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float v13, v78, v114

    sub-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 219
    const v4, 0x4528c000    # 2700.0f

    mul-float v4, v4, v115

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const/high16 v13, 0x40400000    # 3.0f

    mul-float/2addr v9, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v9, v13

    mul-float v13, v103, v114

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    add-float/2addr v13, v14

    sub-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 221
    invoke-virtual/range {v117 .. v117}, Landroid/graphics/Path;->close()V

    .line 222
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_2

    .line 223
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v117

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 224
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_3

    .line 225
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v117

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 228
    :cond_3
    mul-float v4, v48, v115

    mul-float v9, v17, v114

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const/high16 v14, 0x40400000    # 3.0f

    mul-float/2addr v13, v14

    const/high16 v14, 0x40800000    # 4.0f

    div-float/2addr v13, v14

    add-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 230
    mul-float v4, v48, v115

    mul-float v9, v103, v114

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    add-float/2addr v9, v13

    mul-float v13, v28, v115

    mul-float v14, v103, v114

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const/high16 v16, 0x40000000    # 2.0f

    div-float v15, v15, v16

    add-float/2addr v14, v15

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13, v14}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 233
    mul-float v4, v102, v115

    mul-float v9, v103, v114

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    add-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 235
    mul-float v4, v101, v115

    mul-float v9, v103, v114

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    add-float/2addr v9, v13

    mul-float v13, v101, v115

    mul-float v14, v69, v114

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const/high16 v16, 0x40800000    # 4.0f

    div-float v15, v15, v16

    add-float/2addr v14, v15

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13, v14}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 238
    mul-float v4, v101, v115

    mul-float v9, v78, v114

    mul-float v13, v102, v115

    mul-float v14, v78, v114

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13, v14}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 240
    mul-float v4, v48, v115

    mul-float v9, v78, v114

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 243
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v48, v115

    sub-float/2addr v4, v9

    mul-float v9, v17, v114

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const/high16 v14, 0x40400000    # 3.0f

    mul-float/2addr v13, v14

    const/high16 v14, 0x40800000    # 4.0f

    div-float/2addr v13, v14

    add-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 245
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v48, v115

    sub-float/2addr v4, v9

    mul-float v9, v103, v114

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    add-float/2addr v9, v13

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v14, v28, v115

    sub-float/2addr v13, v14

    mul-float v14, v103, v114

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const/high16 v16, 0x40000000    # 2.0f

    div-float v15, v15, v16

    add-float/2addr v14, v15

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13, v14}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 249
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v102, v115

    sub-float/2addr v4, v9

    mul-float v9, v103, v114

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    add-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 251
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v101, v115

    sub-float/2addr v4, v9

    mul-float v9, v103, v114

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    add-float/2addr v9, v13

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v14, v101, v115

    sub-float/2addr v13, v14

    mul-float v14, v69, v114

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const/high16 v16, 0x40800000    # 4.0f

    div-float v15, v15, v16

    add-float/2addr v14, v15

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13, v14}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 255
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v101, v115

    sub-float/2addr v4, v9

    mul-float v9, v78, v114

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v14, v102, v115

    sub-float/2addr v13, v14

    mul-float v14, v78, v114

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13, v14}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 258
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v48, v115

    sub-float/2addr v4, v9

    mul-float v9, v78, v114

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 262
    mul-float v4, v101, v115

    mul-float v9, v69, v114

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const/high16 v14, 0x40800000    # 4.0f

    div-float/2addr v13, v14

    add-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 264
    mul-float v4, v101, v115

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float v13, v78, v114

    sub-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 265
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v101, v115

    sub-float/2addr v4, v9

    mul-float v9, v69, v114

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const/high16 v14, 0x40800000    # 4.0f

    div-float/2addr v13, v14

    add-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 267
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v101, v115

    sub-float/2addr v4, v9

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float v13, v78, v114

    sub-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 270
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_4

    .line 271
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v117

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 275
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->textArea:Landroid/graphics/RectF;

    const v9, 0x45bdd800    # 6075.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float/2addr v9, v13

    const v13, 0x46a8c000    # 21600.0f

    div-float/2addr v9, v13

    iput v9, v4, Landroid/graphics/RectF;->left:F

    .line 276
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->textArea:Landroid/graphics/RectF;

    const v9, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float/2addr v9, v13

    const v13, 0x46a8c000    # 21600.0f

    div-float/2addr v9, v13

    iput v9, v4, Landroid/graphics/RectF;->top:F

    .line 277
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->textArea:Landroid/graphics/RectF;

    const v9, 0x46729400    # 15525.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float/2addr v9, v13

    const v13, 0x46a8c000    # 21600.0f

    div-float/2addr v9, v13

    iput v9, v4, Landroid/graphics/RectF;->right:F

    .line 278
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->textArea:Landroid/graphics/RectF;

    const v9, 0x46729400    # 15525.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float/2addr v9, v13

    const v13, 0x46a8c000    # 21600.0f

    div-float/2addr v9, v13

    iput v9, v4, Landroid/graphics/RectF;->bottom:F

    .line 673
    .end local v17    # "at11":F
    .end local v28    # "at3":F
    .end local v48    # "at4":F
    .end local v69    # "at13":F
    .end local v78    # "at10":F
    .end local v101    # "at0":F
    .end local v102    # "at1":F
    .end local v103    # "at12":F
    .end local v107    # "at2":F
    .end local v117    # "path":Landroid/graphics/Path;
    :cond_5
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mDrawOnCanvas:Z

    if-nez v4, :cond_6

    .line 674
    new-instance v26, Landroid/graphics/Matrix;

    invoke-direct/range {v26 .. v26}, Landroid/graphics/Matrix;-><init>()V

    .line 677
    .local v26, "mtx":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->rotation:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v9, v13

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v9, v13}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 679
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->bitmap:Landroid/graphics/Bitmap;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->bitmapWidth:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->bitmapHight:I

    move/from16 v25, v0

    const/16 v27, 0x1

    invoke-static/range {v21 .. v27}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v118

    .line 684
    .local v118, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v116, Ljava/io/FileOutputStream;

    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->folderPath:Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "pic_"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->shapeName:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, ".png"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v4, v9, v13}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v116

    invoke-direct {v0, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 690
    .local v116, "out":Ljava/io/FileOutputStream;
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v9, 0x64

    move-object/from16 v0, v118

    move-object/from16 v1, v116

    invoke-virtual {v0, v4, v9, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 691
    invoke-virtual/range {v116 .. v116}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 697
    .end local v26    # "mtx":Landroid/graphics/Matrix;
    .end local v116    # "out":Ljava/io/FileOutputStream;
    .end local v118    # "rotatedBMP":Landroid/graphics/Bitmap;
    :cond_6
    :goto_1
    return-void

    .line 281
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v9, "Ribbon"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 291
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval0:I

    if-nez v4, :cond_8

    .line 292
    const/16 v4, 0x1518

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval0:I

    .line 293
    :cond_8
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval1:I

    if-nez v4, :cond_9

    .line 294
    const/16 v4, 0xa8c

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval1:I

    .line 296
    :cond_9
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval0:I

    int-to-float v0, v4

    move/from16 v101, v0

    .line 297
    .restart local v101    # "at0":F
    const v4, 0x4428c000    # 675.0f

    add-float v102, v101, v4

    .line 298
    .restart local v102    # "at1":F
    const v4, 0x4428c000    # 675.0f

    add-float v107, v102, v4

    .line 299
    .restart local v107    # "at2":F
    const v4, 0x4428c000    # 675.0f

    add-float v28, v107, v4

    .line 300
    .restart local v28    # "at3":F
    const v4, 0x4428c000    # 675.0f

    add-float v48, v28, v4

    .line 301
    .restart local v48    # "at4":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval1:I

    int-to-float v0, v4

    move/from16 v78, v0

    .line 302
    .restart local v78    # "at10":F
    const/high16 v4, 0x3f800000    # 1.0f

    mul-float v4, v4, v78

    const/high16 v9, 0x40800000    # 4.0f

    div-float v17, v4, v9

    .line 303
    .restart local v17    # "at11":F
    const/high16 v4, 0x40000000    # 2.0f

    mul-float v103, v17, v4

    .line 304
    .restart local v103    # "at12":F
    const/high16 v4, 0x40400000    # 3.0f

    mul-float v69, v17, v4

    .line 306
    .restart local v69    # "at13":F
    new-instance v117, Landroid/graphics/Path;

    invoke-direct/range {v117 .. v117}, Landroid/graphics/Path;-><init>()V

    .line 310
    .restart local v117    # "path":Landroid/graphics/Path;
    const/4 v4, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 311
    mul-float v4, v28, v115

    const/4 v9, 0x0

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 312
    mul-float v4, v28, v115

    mul-float v9, v17, v114

    mul-float v13, v48, v115

    mul-float v14, v17, v114

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13, v14}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 314
    mul-float v4, v48, v115

    mul-float v9, v78, v114

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 315
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v48, v115

    sub-float/2addr v4, v9

    mul-float v9, v78, v114

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 316
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v48, v115

    sub-float/2addr v4, v9

    mul-float v9, v17, v114

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 317
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v28, v115

    sub-float/2addr v4, v9

    mul-float v9, v17, v114

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v14, v28, v115

    sub-float/2addr v13, v14

    const/4 v14, 0x0

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13, v14}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 319
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    const/4 v9, 0x0

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 320
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    const v9, 0x4528c000    # 2700.0f

    mul-float v9, v9, v115

    sub-float/2addr v4, v9

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v9, v13

    mul-float v13, v103, v114

    sub-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 322
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float v13, v78, v114

    sub-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 323
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v101, v115

    sub-float/2addr v4, v9

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float v13, v78, v114

    sub-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 325
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v101, v115

    sub-float/2addr v4, v9

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float v13, v17, v114

    sub-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 327
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v101, v115

    sub-float/2addr v4, v9

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v14, v102, v115

    sub-float/2addr v13, v14

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13, v14}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 329
    mul-float v4, v102, v115

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 330
    mul-float v4, v101, v115

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float v13, v101, v115

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float v15, v17, v114

    sub-float/2addr v14, v15

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13, v14}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 332
    mul-float v4, v101, v115

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float v13, v78, v114

    sub-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 333
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float v13, v78, v114

    sub-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 334
    const v4, 0x4528c000    # 2700.0f

    mul-float v4, v4, v115

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v9, v13

    mul-float v13, v103, v114

    sub-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 336
    invoke-virtual/range {v117 .. v117}, Landroid/graphics/Path;->close()V

    .line 337
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_a

    .line 338
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v117

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 339
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_b

    .line 340
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v117

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 343
    :cond_b
    mul-float v4, v48, v115

    mul-float v9, v17, v114

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 344
    mul-float v4, v48, v115

    mul-float v9, v103, v114

    mul-float v13, v28, v115

    mul-float v14, v103, v114

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13, v14}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 346
    mul-float v4, v102, v115

    mul-float v9, v103, v114

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 347
    mul-float v4, v102, v115

    mul-float v9, v69, v114

    mul-float v13, v101, v115

    mul-float v14, v69, v114

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13, v14}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 349
    mul-float v4, v102, v115

    mul-float v9, v78, v114

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 350
    mul-float v4, v48, v115

    mul-float v9, v78, v114

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 353
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v48, v115

    sub-float/2addr v4, v9

    mul-float v9, v17, v114

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 354
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v28, v115

    sub-float/2addr v4, v9

    mul-float v9, v102, v114

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v14, v28, v115

    sub-float/2addr v13, v14

    mul-float v14, v103, v114

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13, v14}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 356
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v102, v115

    sub-float/2addr v4, v9

    mul-float v9, v103, v114

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 357
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v101, v115

    sub-float/2addr v4, v9

    mul-float v9, v103, v114

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v14, v101, v115

    sub-float/2addr v13, v14

    mul-float v14, v69, v114

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13, v14}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 359
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v102, v115

    sub-float/2addr v4, v9

    mul-float v9, v78, v114

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 360
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v48, v115

    sub-float/2addr v4, v9

    mul-float v9, v78, v114

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 363
    mul-float v4, v101, v115

    mul-float v9, v69, v114

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 364
    mul-float v4, v101, v115

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float v13, v78, v114

    sub-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 365
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v101, v115

    sub-float/2addr v4, v9

    mul-float v9, v69, v114

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 366
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float v9, v101, v115

    sub-float/2addr v4, v9

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float v13, v78, v114

    sub-float/2addr v9, v13

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 369
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_c

    .line 370
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v117

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 374
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->textArea:Landroid/graphics/RectF;

    const v9, 0x45bdd800    # 6075.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float/2addr v9, v13

    const v13, 0x46a8c000    # 21600.0f

    div-float/2addr v9, v13

    iput v9, v4, Landroid/graphics/RectF;->left:F

    .line 375
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->textArea:Landroid/graphics/RectF;

    const v9, 0x4593a800    # 4725.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float/2addr v9, v13

    const v13, 0x46a8c000    # 21600.0f

    div-float/2addr v9, v13

    iput v9, v4, Landroid/graphics/RectF;->top:F

    .line 376
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->textArea:Landroid/graphics/RectF;

    const v9, 0x46729400    # 15525.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float/2addr v9, v13

    const v13, 0x46a8c000    # 21600.0f

    div-float/2addr v9, v13

    iput v9, v4, Landroid/graphics/RectF;->right:F

    .line 377
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->textArea:Landroid/graphics/RectF;

    const v9, 0x4698ee00    # 19575.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float/2addr v9, v13

    const v13, 0x46a8c000    # 21600.0f

    div-float/2addr v9, v13

    iput v9, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 380
    .end local v17    # "at11":F
    .end local v28    # "at3":F
    .end local v48    # "at4":F
    .end local v69    # "at13":F
    .end local v78    # "at10":F
    .end local v101    # "at0":F
    .end local v102    # "at1":F
    .end local v103    # "at12":F
    .end local v107    # "at2":F
    .end local v117    # "path":Landroid/graphics/Path;
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v9, "EllipseRibbon2"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 382
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval0:I

    if-nez v4, :cond_e

    .line 383
    const/16 v4, 0x1518

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval0:I

    .line 384
    :cond_e
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval1:I

    if-nez v4, :cond_f

    .line 385
    const/16 v4, 0x3f48

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval1:I

    .line 386
    :cond_f
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval2:I

    if-nez v4, :cond_10

    .line 387
    const/16 v4, 0xa8c

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval2:I

    .line 389
    :cond_10
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval0:I

    int-to-float v4, v4

    mul-float v11, v4, v115

    .line 390
    .local v11, "adj0":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval1:I

    int-to-float v4, v4

    mul-float v34, v4, v114

    .line 391
    .local v34, "adj1":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval2:I

    int-to-float v4, v4

    mul-float v99, v4, v114

    .line 393
    .local v99, "adj2":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    move/from16 v28, v0

    .line 394
    .restart local v28    # "at3":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    move/from16 v48, v0

    .line 395
    .restart local v48    # "at4":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    const/high16 v9, 0x41000000    # 8.0f

    div-float v110, v4, v9

    .line 396
    .local v110, "at5":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    const/high16 v9, 0x40000000    # 2.0f

    div-float v111, v4, v9

    .line 397
    .local v111, "at6":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    const/high16 v9, 0x40e00000    # 7.0f

    mul-float/2addr v4, v9

    const/high16 v9, 0x41000000    # 8.0f

    div-float v112, v4, v9

    .line 398
    .local v112, "at7":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    const/high16 v9, 0x40400000    # 3.0f

    mul-float/2addr v4, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float v7, v4, v9

    .line 399
    .local v7, "at8":F
    move/from16 v0, v111

    neg-float v5, v0

    .line 400
    .local v5, "at9":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval2:I

    int-to-float v4, v4

    mul-float v4, v4, v114

    const v9, 0x46eeda00    # 30573.0f

    mul-float v9, v9, v114

    mul-float/2addr v4, v9

    const/high16 v9, 0x45800000    # 4096.0f

    mul-float v9, v9, v114

    div-float v78, v4, v9

    .line 405
    .restart local v78    # "at10":F
    const/high16 v4, 0x40000000    # 2.0f

    mul-float v17, v78, v4

    .line 406
    .restart local v17    # "at11":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    add-float v4, v4, v78

    sub-float v103, v4, v99

    .line 407
    .restart local v103    # "at12":F
    add-float v69, v78, v34

    .line 408
    .restart local v69    # "at13":F
    const/high16 v4, 0x40000000    # 2.0f

    div-float v104, v34, v4

    .line 409
    .local v104, "at14":F
    add-float v105, v78, v104

    .line 410
    .local v105, "at15":F
    sub-float v106, v103, v34

    .line 411
    .local v106, "at16":F
    add-float v39, v11, v110

    .line 412
    .local v39, "at17":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    sub-float v30, v4, v39

    .line 413
    .local v30, "at18":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    sub-float v20, v4, v11

    .line 414
    .local v20, "at19":F
    sub-float v89, v111, v11

    .line 415
    .local v89, "at20":F
    move/from16 v0, v78

    float-to-double v14, v0

    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    div-float v9, v89, v9

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    div-float v13, v89, v13

    mul-float/2addr v9, v13

    sub-float/2addr v4, v9

    float-to-double v0, v4

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v22

    mul-double v14, v14, v22

    double-to-float v0, v14

    move/from16 v81, v0

    .line 418
    .local v81, "at21":F
    sub-float v19, v78, v81

    .line 419
    .local v19, "at22":F
    add-float v4, v19, v106

    sub-float v12, v4, v78

    .line 420
    .local v12, "at23":F
    add-float v4, v99, v106

    sub-float v10, v4, v78

    .line 421
    .local v10, "at24":F
    const v4, 0x45157000    # 2391.0f

    mul-float v4, v4, v114

    mul-float v4, v4, v78

    const/high16 v9, 0x47000000    # 32768.0f

    mul-float v9, v9, v114

    div-float v73, v4, v9

    .line 423
    .local v73, "at25":F
    sub-float v66, v111, v39

    .line 424
    .local v66, "at26":F
    move/from16 v0, v78

    float-to-double v14, v0

    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    div-float v9, v66, v9

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    div-float v13, v66, v13

    mul-float/2addr v9, v13

    sub-float/2addr v4, v9

    float-to-double v0, v4

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v22

    mul-double v14, v14, v22

    double-to-float v0, v14

    move/from16 v64, v0

    .line 427
    .local v64, "at27":F
    add-float v4, v78, v34

    sub-float v38, v4, v64

    .line 428
    .local v38, "at28":F
    add-float v57, v19, v34

    .line 429
    .local v57, "at29":F
    sub-float v31, v103, v64

    .line 430
    .local v31, "at30":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    sub-float v42, v4, v99

    .line 431
    .local v42, "at31":F
    add-float v44, v78, v103

    .line 432
    .local v44, "at32":F
    add-float v4, v44, v78

    sub-float v36, v4, v106

    .line 433
    .local v36, "at33":F
    add-float v4, v42, v78

    sub-float v6, v4, v69

    .line 434
    .local v6, "at34":F
    add-float v4, v44, v78

    sub-float v8, v4, v69

    .line 435
    .local v8, "at35":F
    add-float v4, v73, v103

    sub-float v84, v4, v105

    .line 438
    .local v84, "at36":F
    const/high16 v100, 0x40000000    # 2.0f

    .line 441
    .local v100, "adjVal":F
    new-instance v117, Landroid/graphics/Path;

    invoke-direct/range {v117 .. v117}, Landroid/graphics/Path;-><init>()V

    .line 444
    .restart local v117    # "path":Landroid/graphics/Path;
    const/4 v9, 0x0

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v12}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->initClockwiseArc(FFFFFFFF)V

    .line 445
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->startA:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 448
    const/4 v15, 0x0

    move-object/from16 v13, p0

    move v14, v5

    move/from16 v16, v7

    move/from16 v18, v11

    move/from16 v21, v19

    invoke-direct/range {v13 .. v21}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->initClockwiseArc(FFFFFFFF)V

    .line 449
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->startA:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    move-object/from16 v21, p0

    move/from16 v22, v5

    move/from16 v23, v6

    move/from16 v24, v7

    move/from16 v25, v8

    move/from16 v26, v20

    move/from16 v27, v12

    move/from16 v29, v10

    .line 452
    invoke-direct/range {v21 .. v29}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->initClockwiseArc(FFFFFFFF)V

    .line 453
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->startA:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 456
    move-object/from16 v0, v117

    move/from16 v1, v28

    invoke-virtual {v0, v1, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 460
    move-object/from16 v0, v117

    move/from16 v1, v112

    move/from16 v2, v84

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 461
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    move-object/from16 v23, p0

    move/from16 v24, v5

    move/from16 v25, v42

    move/from16 v26, v7

    move/from16 v27, v44

    move/from16 v29, v48

    .line 464
    invoke-direct/range {v23 .. v31}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->initArc(FFFFFFFF)V

    .line 465
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->startA:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    move-object/from16 v32, p0

    move/from16 v33, v5

    move/from16 v35, v7

    move/from16 v37, v30

    move/from16 v40, v38

    .line 468
    invoke-direct/range {v32 .. v40}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->initArc(FFFFFFFF)V

    .line 469
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->startA:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 472
    const/16 v47, 0x0

    move-object/from16 v40, p0

    move/from16 v41, v5

    move/from16 v43, v7

    move/from16 v45, v39

    move/from16 v46, v31

    invoke-direct/range {v40 .. v48}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->initArc(FFFFFFFF)V

    .line 473
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->startA:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 476
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 480
    move-object/from16 v0, v117

    move/from16 v1, v110

    move/from16 v2, v84

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 483
    const/4 v4, 0x0

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 485
    invoke-virtual/range {v117 .. v117}, Landroid/graphics/Path;->close()V

    .line 487
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_11

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_11

    .line 488
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v117

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 489
    :cond_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_12

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_12

    .line 490
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v117

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_12
    move-object/from16 v49, p0

    move/from16 v50, v5

    move/from16 v51, v34

    move/from16 v52, v7

    move/from16 v53, v36

    move/from16 v54, v39

    move/from16 v55, v38

    move/from16 v56, v11

    .line 494
    invoke-direct/range {v49 .. v57}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->initArc(FFFFFFFF)V

    .line 495
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->startA:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 499
    move-object/from16 v0, v117

    move/from16 v1, v39

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    move-object/from16 v49, p0

    move/from16 v50, v5

    move/from16 v51, v34

    move/from16 v52, v7

    move/from16 v53, v36

    move/from16 v54, v30

    move/from16 v55, v38

    move/from16 v56, v20

    .line 501
    invoke-direct/range {v49 .. v57}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->initClockwiseArc(FFFFFFFF)V

    .line 502
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->startA:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 504
    move-object/from16 v0, v117

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 506
    sub-float v4, v11, v100

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v12}, Landroid/graphics/Path;->moveTo(FF)V

    .line 507
    sub-float v4, v11, v100

    move-object/from16 v0, v117

    move/from16 v1, v57

    invoke-virtual {v0, v4, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 508
    add-float v4, v20, v100

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v12}, Landroid/graphics/Path;->moveTo(FF)V

    .line 509
    add-float v4, v20, v100

    move-object/from16 v0, v117

    move/from16 v1, v57

    invoke-virtual {v0, v4, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 512
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_13

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_13

    .line 513
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v117

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 517
    :cond_13
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->textArea:Landroid/graphics/RectF;

    const v9, 0x45bdd800    # 6075.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float/2addr v9, v13

    const v13, 0x46a8c000    # 21600.0f

    div-float/2addr v9, v13

    iput v9, v4, Landroid/graphics/RectF;->left:F

    .line 518
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->textArea:Landroid/graphics/RectF;

    const v9, 0x44fd2000    # 2025.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float/2addr v9, v13

    const v13, 0x46a8c000    # 21600.0f

    div-float/2addr v9, v13

    iput v9, v4, Landroid/graphics/RectF;->top:F

    .line 519
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->textArea:Landroid/graphics/RectF;

    const v9, 0x466de400    # 15225.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float/2addr v9, v13

    const v13, 0x46a8c000    # 21600.0f

    div-float/2addr v9, v13

    iput v9, v4, Landroid/graphics/RectF;->right:F

    .line 520
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->textArea:Landroid/graphics/RectF;

    const v9, 0x46729400    # 15525.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float/2addr v9, v13

    const v13, 0x46a8c000    # 21600.0f

    div-float/2addr v9, v13

    iput v9, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 523
    .end local v5    # "at9":F
    .end local v6    # "at34":F
    .end local v7    # "at8":F
    .end local v8    # "at35":F
    .end local v10    # "at24":F
    .end local v11    # "adj0":F
    .end local v12    # "at23":F
    .end local v17    # "at11":F
    .end local v19    # "at22":F
    .end local v20    # "at19":F
    .end local v28    # "at3":F
    .end local v30    # "at18":F
    .end local v31    # "at30":F
    .end local v34    # "adj1":F
    .end local v36    # "at33":F
    .end local v38    # "at28":F
    .end local v39    # "at17":F
    .end local v42    # "at31":F
    .end local v44    # "at32":F
    .end local v48    # "at4":F
    .end local v57    # "at29":F
    .end local v64    # "at27":F
    .end local v66    # "at26":F
    .end local v69    # "at13":F
    .end local v73    # "at25":F
    .end local v78    # "at10":F
    .end local v81    # "at21":F
    .end local v84    # "at36":F
    .end local v89    # "at20":F
    .end local v99    # "adj2":F
    .end local v100    # "adjVal":F
    .end local v103    # "at12":F
    .end local v104    # "at14":F
    .end local v105    # "at15":F
    .end local v106    # "at16":F
    .end local v110    # "at5":F
    .end local v111    # "at6":F
    .end local v112    # "at7":F
    .end local v117    # "path":Landroid/graphics/Path;
    :cond_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->shapeName:Ljava/lang/String;

    const-string/jumbo v9, "EllipseRibbon"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 525
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval0:I

    if-nez v4, :cond_15

    .line 526
    const/16 v4, 0x1518

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval0:I

    .line 527
    :cond_15
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval1:I

    if-nez v4, :cond_16

    .line 528
    const/16 v4, 0x1518

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval1:I

    .line 529
    :cond_16
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval2:I

    if-nez v4, :cond_17

    .line 530
    const/16 v4, 0x49d4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval2:I

    .line 532
    :cond_17
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval0:I

    int-to-float v4, v4

    mul-float v11, v4, v115

    .line 533
    .restart local v11    # "adj0":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval1:I

    int-to-float v4, v4

    mul-float v34, v4, v114

    .line 534
    .restart local v34    # "adj1":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->adjval2:I

    int-to-float v4, v4

    mul-float v99, v4, v114

    .line 536
    .restart local v99    # "adj2":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    move/from16 v28, v0

    .line 537
    .restart local v28    # "at3":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    move/from16 v48, v0

    .line 538
    .restart local v48    # "at4":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    const/high16 v9, 0x41000000    # 8.0f

    div-float v110, v4, v9

    .line 539
    .restart local v110    # "at5":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    const/high16 v9, 0x40000000    # 2.0f

    div-float v111, v4, v9

    .line 540
    .restart local v111    # "at6":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    const/high16 v9, 0x40e00000    # 7.0f

    mul-float/2addr v4, v9

    const/high16 v9, 0x41000000    # 8.0f

    div-float v112, v4, v9

    .line 541
    .restart local v112    # "at7":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    const/high16 v9, 0x40400000    # 3.0f

    mul-float/2addr v4, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float v7, v4, v9

    .line 542
    .restart local v7    # "at8":F
    move/from16 v0, v111

    neg-float v5, v0

    .line 543
    .restart local v5    # "at9":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    sub-float v78, v4, v99

    .line 544
    .restart local v78    # "at10":F
    const v4, 0x46eeda00    # 30573.0f

    mul-float v4, v4, v114

    mul-float v4, v4, v78

    const/high16 v9, 0x45800000    # 4096.0f

    mul-float v9, v9, v114

    div-float v17, v4, v9

    .line 546
    .restart local v17    # "at11":F
    const/high16 v4, 0x40000000    # 2.0f

    mul-float v103, v17, v4

    .line 547
    .restart local v103    # "at12":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    sub-float v69, v4, v103

    .line 548
    .restart local v69    # "at13":F
    add-float v104, v17, v99

    .line 549
    .restart local v104    # "at14":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    add-float v4, v4, v17

    sub-float v105, v4, v34

    .line 550
    .restart local v105    # "at15":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    sub-float v106, v4, v34

    .line 551
    .restart local v106    # "at16":F
    const/high16 v4, 0x40000000    # 2.0f

    div-float v39, v106, v4

    .line 552
    .restart local v39    # "at17":F
    add-float v30, v17, v39

    .line 553
    .restart local v30    # "at18":F
    add-float v4, v104, v34

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    sub-float v20, v4, v9

    .line 554
    .restart local v20    # "at19":F
    add-float v89, v11, v110

    .line 555
    .restart local v89    # "at20":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    sub-float v81, v4, v89

    .line 556
    .restart local v81    # "at21":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    sub-float v19, v4, v11

    .line 557
    .restart local v19    # "at22":F
    sub-float v12, v111, v11

    .line 559
    .restart local v12    # "at23":F
    move/from16 v0, v17

    float-to-double v14, v0

    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    div-float v9, v12, v9

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    div-float v13, v12, v13

    mul-float/2addr v9, v13

    sub-float/2addr v4, v9

    float-to-double v0, v4

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v22

    mul-double v14, v14, v22

    double-to-float v10, v14

    .line 562
    .restart local v10    # "at24":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    add-float/2addr v4, v10

    sub-float v73, v4, v17

    .line 563
    .restart local v73    # "at25":F
    add-float v4, v73, v17

    sub-float v66, v4, v20

    .line 564
    .restart local v66    # "at26":F
    add-float v4, v99, v17

    sub-float v64, v4, v20

    .line 565
    .restart local v64    # "at27":F
    const v4, 0x45157000    # 2391.0f

    mul-float v4, v4, v114

    mul-float v4, v4, v17

    const/high16 v9, 0x47000000    # 32768.0f

    mul-float v9, v9, v114

    div-float v38, v4, v9

    .line 567
    .restart local v38    # "at28":F
    sub-float v57, v111, v89

    .line 569
    .restart local v57    # "at29":F
    move/from16 v0, v17

    float-to-double v14, v0

    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    div-float v9, v57, v9

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    div-float v13, v57, v13

    mul-float/2addr v9, v13

    sub-float/2addr v4, v9

    float-to-double v0, v4

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v22

    mul-double v14, v14, v22

    double-to-float v0, v14

    move/from16 v31, v0

    .line 572
    .restart local v31    # "at30":F
    add-float v4, v34, v31

    sub-float v42, v4, v17

    .line 573
    .restart local v42    # "at31":F
    add-float v4, v73, v34

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    sub-float v44, v4, v9

    .line 574
    .restart local v44    # "at32":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    add-float v4, v4, v31

    sub-float v36, v4, v104

    .line 575
    .restart local v36    # "at33":F
    add-float v6, v17, v104

    .line 576
    .restart local v6    # "at34":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    sub-float v8, v4, v6

    .line 577
    .restart local v8    # "at35":F
    add-float v4, v8, v20

    sub-float v84, v4, v17

    .line 578
    .restart local v84    # "at36":F
    add-float v4, v78, v105

    sub-float v62, v4, v17

    .line 580
    .local v62, "at37":F
    add-float v4, v8, v105

    sub-float v60, v4, v17

    .line 581
    .local v60, "at38":F
    add-float v4, v38, v104

    sub-float v108, v4, v30

    .line 582
    .local v108, "at39":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    sub-float v109, v4, v108

    .line 585
    .local v109, "at40":F
    const/high16 v100, 0x40000000    # 2.0f

    .line 588
    .restart local v100    # "adjVal":F
    new-instance v117, Landroid/graphics/Path;

    invoke-direct/range {v117 .. v117}, Landroid/graphics/Path;-><init>()V

    .line 591
    .restart local v117    # "path":Landroid/graphics/Path;
    const/16 v63, 0x0

    move-object/from16 v58, p0

    move/from16 v59, v5

    move/from16 v61, v7

    move/from16 v65, v11

    invoke-direct/range {v58 .. v66}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->initArc(FFFFFFFF)V

    .line 592
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->startA:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    move-object/from16 v67, p0

    move/from16 v68, v5

    move/from16 v70, v7

    move/from16 v71, v48

    move/from16 v72, v11

    move/from16 v74, v19

    move/from16 v75, v73

    .line 595
    invoke-direct/range {v67 .. v75}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->initArc(FFFFFFFF)V

    .line 596
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->startA:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    move-object/from16 v21, p0

    move/from16 v22, v5

    move/from16 v23, v60

    move/from16 v24, v7

    move/from16 v25, v62

    move/from16 v26, v19

    move/from16 v27, v66

    move/from16 v29, v64

    .line 599
    invoke-direct/range {v21 .. v29}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->initArc(FFFFFFFF)V

    .line 600
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->startA:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 603
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    move-object/from16 v0, v117

    move/from16 v1, v64

    invoke-virtual {v0, v4, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 607
    move-object/from16 v0, v117

    move/from16 v1, v112

    move/from16 v2, v109

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 608
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    const/4 v9, 0x0

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 611
    const/16 v80, 0x0

    move-object/from16 v74, p0

    move/from16 v75, v5

    move/from16 v76, v8

    move/from16 v77, v7

    move/from16 v79, v28

    move/from16 v82, v36

    invoke-direct/range {v74 .. v82}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->initClockwiseArc(FFFFFFFF)V

    .line 612
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->startA:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    move-object/from16 v82, p0

    move/from16 v83, v5

    move/from16 v85, v7

    move/from16 v86, v34

    move/from16 v87, v81

    move/from16 v88, v42

    move/from16 v90, v42

    .line 615
    invoke-direct/range {v82 .. v90}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->initClockwiseArc(FFFFFFFF)V

    .line 616
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->startA:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 619
    const/16 v97, 0x0

    const/16 v98, 0x0

    move-object/from16 v90, p0

    move/from16 v91, v5

    move/from16 v92, v8

    move/from16 v93, v7

    move/from16 v94, v78

    move/from16 v95, v89

    move/from16 v96, v36

    invoke-direct/range {v90 .. v98}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->initClockwiseArc(FFFFFFFF)V

    .line 620
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->startA:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 623
    const/4 v4, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 627
    move-object/from16 v0, v117

    move/from16 v1, v110

    move/from16 v2, v109

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 630
    const/4 v4, 0x0

    move-object/from16 v0, v117

    move/from16 v1, v64

    invoke-virtual {v0, v4, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 633
    invoke-virtual/range {v117 .. v117}, Landroid/graphics/Path;->close()V

    .line 635
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_18

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->paintFill:Landroid/graphics/Paint;

    if-eqz v4, :cond_18

    .line 636
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v117

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 637
    :cond_18
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_19

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_19

    .line 638
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v117

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_19
    move-object/from16 v90, p0

    move/from16 v91, v5

    move/from16 v92, v84

    move/from16 v93, v7

    move/from16 v94, v34

    move/from16 v95, v89

    move/from16 v96, v42

    move/from16 v97, v11

    move/from16 v98, v44

    .line 642
    invoke-direct/range {v90 .. v98}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->initClockwiseArc(FFFFFFFF)V

    .line 643
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->startA:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 647
    move-object/from16 v0, v117

    move/from16 v1, v89

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    move-object/from16 v90, p0

    move/from16 v91, v5

    move/from16 v92, v84

    move/from16 v93, v7

    move/from16 v94, v34

    move/from16 v95, v81

    move/from16 v96, v42

    move/from16 v97, v19

    move/from16 v98, v44

    .line 649
    invoke-direct/range {v90 .. v98}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->initArc(FFFFFFFF)V

    .line 650
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->rectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->startA:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->sweepA:F

    move-object/from16 v0, v117

    invoke-virtual {v0, v4, v9, v13}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 652
    move-object/from16 v0, v117

    move/from16 v1, v81

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 654
    sub-float v4, v11, v100

    move-object/from16 v0, v117

    move/from16 v1, v66

    invoke-virtual {v0, v4, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 655
    sub-float v4, v11, v100

    move-object/from16 v0, v117

    move/from16 v1, v44

    invoke-virtual {v0, v4, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 656
    add-float v4, v19, v100

    move-object/from16 v0, v117

    move/from16 v1, v66

    invoke-virtual {v0, v4, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 657
    add-float v4, v19, v100

    move-object/from16 v0, v117

    move/from16 v1, v44

    invoke-virtual {v0, v4, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 660
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->paintLine:Landroid/graphics/Paint;

    if-eqz v4, :cond_1a

    .line 661
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v117

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 665
    :cond_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->textArea:Landroid/graphics/RectF;

    const v9, 0x45bdd800    # 6075.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float/2addr v9, v13

    const v13, 0x46a8c000    # 21600.0f

    div-float/2addr v9, v13

    iput v9, v4, Landroid/graphics/RectF;->left:F

    .line 666
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->textArea:Landroid/graphics/RectF;

    const v9, 0x45bdd800    # 6075.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float/2addr v9, v13

    const v13, 0x46a8c000    # 21600.0f

    div-float/2addr v9, v13

    iput v9, v4, Landroid/graphics/RectF;->top:F

    .line 667
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->textArea:Landroid/graphics/RectF;

    const v9, 0x466de400    # 15225.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    mul-float/2addr v9, v13

    const v13, 0x46a8c000    # 21600.0f

    div-float/2addr v9, v13

    iput v9, v4, Landroid/graphics/RectF;->right:F

    .line 668
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->textArea:Landroid/graphics/RectF;

    const v9, 0x4698ee00    # 19575.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    mul-float/2addr v9, v13

    const v13, 0x46a8c000    # 21600.0f

    div-float/2addr v9, v13

    iput v9, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 692
    .end local v5    # "at9":F
    .end local v6    # "at34":F
    .end local v7    # "at8":F
    .end local v8    # "at35":F
    .end local v10    # "at24":F
    .end local v11    # "adj0":F
    .end local v12    # "at23":F
    .end local v17    # "at11":F
    .end local v19    # "at22":F
    .end local v20    # "at19":F
    .end local v28    # "at3":F
    .end local v30    # "at18":F
    .end local v31    # "at30":F
    .end local v34    # "adj1":F
    .end local v36    # "at33":F
    .end local v38    # "at28":F
    .end local v39    # "at17":F
    .end local v42    # "at31":F
    .end local v44    # "at32":F
    .end local v48    # "at4":F
    .end local v57    # "at29":F
    .end local v60    # "at38":F
    .end local v62    # "at37":F
    .end local v64    # "at27":F
    .end local v66    # "at26":F
    .end local v69    # "at13":F
    .end local v73    # "at25":F
    .end local v78    # "at10":F
    .end local v81    # "at21":F
    .end local v84    # "at36":F
    .end local v89    # "at20":F
    .end local v99    # "adj2":F
    .end local v100    # "adjVal":F
    .end local v103    # "at12":F
    .end local v104    # "at14":F
    .end local v105    # "at15":F
    .end local v106    # "at16":F
    .end local v108    # "at39":F
    .end local v109    # "at40":F
    .end local v110    # "at5":F
    .end local v111    # "at6":F
    .end local v112    # "at7":F
    .end local v117    # "path":Landroid/graphics/Path;
    .restart local v26    # "mtx":Landroid/graphics/Matrix;
    .restart local v118    # "rotatedBMP":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v113

    .line 693
    .local v113, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "Exception while writing pictures to out file"

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v113

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F

    .prologue
    .line 115
    invoke-super {p0, p3, p4, p7, p8}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IFF)V

    .line 116
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mDrawOnCanvas:Z

    .line 117
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mLeftMarg:I

    .line 118
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mTopMarg:I

    .line 119
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 120
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 121
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 122
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mDrawOnCanvas:Z

    .line 123
    return-void
.end method

.method public drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p2, "shapeCount"    # I

    .prologue
    .line 106
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V

    .line 108
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->draw(I)V

    .line 109
    return-void
.end method

.method public drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 92
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;I)V

    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mDrawOnCanvas:Z

    .line 94
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mLeftMarg:I

    .line 95
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mTopMarg:I

    .line 96
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    .line 97
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    .line 98
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 99
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 100
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mDrawOnCanvas:Z

    .line 102
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V
    .locals 0
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 44
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 45
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->draw(I)V

    .line 46
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V
    .locals 2
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I

    .prologue
    .line 50
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mDrawOnCanvas:Z

    .line 52
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mLeftMarg:I

    .line 53
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mTopMarg:I

    .line 54
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 55
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 56
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mDrawOnCanvas:Z

    .line 58
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 136
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 137
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mDrawOnCanvas:Z

    .line 138
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mLeftMarg:I

    .line 139
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mTopMarg:I

    .line 140
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 141
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 142
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 143
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mDrawOnCanvas:Z

    .line 144
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 148
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V

    .line 149
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mDrawOnCanvas:Z

    .line 150
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mLeftMarg:I

    .line 151
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mTopMarg:I

    .line 152
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 153
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 154
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 155
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mDrawOnCanvas:Z

    .line 156
    return-void
.end method

.method public drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .prologue
    .line 130
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 131
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->draw(I)V

    .line 132
    return-void
.end method

.method public drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 63
    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p7

    move v4, p8

    move-object v5, p9

    invoke-super/range {v0 .. v5}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSSExcelShape(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mDrawOnCanvas:Z

    .line 66
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mLeftMarg:I

    .line 67
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mTopMarg:I

    .line 68
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 69
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 70
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mDrawOnCanvas:Z

    .line 72
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 76
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mDrawOnCanvas:Z

    .line 78
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mLeftMarg:I

    .line 79
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mTopMarg:I

    .line 80
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->width:F

    .line 81
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->height:F

    .line 82
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 83
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->draw(Landroid/graphics/Canvas;I)V

    .line 84
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 85
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->mDrawOnCanvas:Z

    .line 86
    return-void
.end method
