.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelWorkSheetsHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 29
    const-string/jumbo v10, "worksheet"

    invoke-direct {p0, v10}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 31
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelDimensionHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelDimensionHandler;-><init>()V

    .line 32
    .local v1, "dimensionHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelDimensionHandler;
    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v11, "dimension"

    invoke-virtual {v10, v11, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetViewsHandler;

    invoke-direct {v8}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetViewsHandler;-><init>()V

    .line 35
    .local v8, "sheetViewsHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetViewsHandler;
    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v11, "sheetViews"

    invoke-virtual {v10, v11, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetFormatPrHandler;

    invoke-direct {v7}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetFormatPrHandler;-><init>()V

    .line 38
    .local v7, "sheetFormatPrHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetFormatPrHandler;
    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v11, "sheetFormatPr"

    invoke-virtual {v10, v11, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExceWorksheetColsHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExceWorksheetColsHandler;-><init>()V

    .line 42
    .local v0, "colsHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExceWorksheetColsHandler;
    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v11, "cols"

    invoke-virtual {v10, v11, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExceSheetDataHandler;

    invoke-direct {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExceSheetDataHandler;-><init>()V

    .line 45
    .local v9, "sheetdataHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExceSheetDataHandler;
    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v11, "sheetData"

    invoke-virtual {v10, v11, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelMergeCellsHandler;

    invoke-direct {v4}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelMergeCellsHandler;-><init>()V

    .line 48
    .local v4, "mergecellHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelMergeCellsHandler;
    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v11, "mergeCells"

    invoke-virtual {v10, v11, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelPageMarginsHandler;

    invoke-direct {v5}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelPageMarginsHandler;-><init>()V

    .line 51
    .local v5, "pageMarginsHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelPageMarginsHandler;
    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v11, "pageMargins"

    invoke-virtual {v10, v11, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelPageSetupHandler;

    invoke-direct {v6}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelPageSetupHandler;-><init>()V

    .line 55
    .local v6, "pageSetupHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelPageSetupHandler;
    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v11, "pageSetup"

    invoke-virtual {v10, v11, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelDrawingHandler;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelDrawingHandler;-><init>()V

    .line 58
    .local v2, "drawingHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelDrawingHandler;
    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v11, "drawing"

    invoke-virtual {v10, v11, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorksheetHyperlinksHandler;

    invoke-direct {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorksheetHyperlinksHandler;-><init>()V

    .line 61
    .local v3, "hyperlinkHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorksheetHyperlinksHandler;
    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelWorkSheetsHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v11, "hyperlinks"

    invoke-virtual {v10, v11, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 69
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 70
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v0

    .line 72
    .local v0, "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    iget-boolean v1, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->customCols:Z

    if-nez v1, :cond_0

    .line 73
    const/16 v1, 0x10

    iput v1, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->maxCol:I

    .line 75
    :cond_0
    return-void
.end method
