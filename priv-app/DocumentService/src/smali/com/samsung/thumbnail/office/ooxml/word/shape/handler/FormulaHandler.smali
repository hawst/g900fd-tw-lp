.class public Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/FormulaHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "FormulaHandler.java"


# instance fields
.field private shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V
    .locals 2
    .param p1, "shapePRop"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    .prologue
    .line 18
    const/16 v0, 0x28

    const-string/jumbo v1, "f"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 20
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/FormulaHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    .line 21
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 26
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 28
    const-string/jumbo v1, "eqn"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/FormulaHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 29
    .local v0, "val":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 30
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/FormulaHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->addFormulas(Ljava/lang/String;)V

    .line 32
    :cond_0
    return-void
.end method
