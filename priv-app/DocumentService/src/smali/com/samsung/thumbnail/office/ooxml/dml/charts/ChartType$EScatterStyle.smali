.class public final enum Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;
.super Ljava/lang/Enum;
.source "ChartType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EScatterStyle"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

.field public static final enum LINE:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

.field public static final enum LINE_MARKER:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

.field public static final enum MARKER:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

.field public static final enum NONE:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

.field public static final enum SMOOTH:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

.field public static final enum SMOOTH_MARKER:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 97
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    const-string/jumbo v1, "NONE"

    const-string/jumbo v2, "none"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;->NONE:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    .line 100
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    const-string/jumbo v1, "LINE"

    const-string/jumbo v2, "line"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;->LINE:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    .line 103
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    const-string/jumbo v1, "LINE_MARKER"

    const-string/jumbo v2, "lineMarker"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;->LINE_MARKER:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    .line 106
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    const-string/jumbo v1, "MARKER"

    const-string/jumbo v2, "marker"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;->MARKER:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    .line 109
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    const-string/jumbo v1, "SMOOTH"

    const-string/jumbo v2, "smooth"

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;->SMOOTH:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    .line 112
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    const-string/jumbo v1, "SMOOTH_MARKER"

    const/4 v2, 0x5

    const-string/jumbo v3, "smoothMarker"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;->SMOOTH_MARKER:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    .line 93
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;->NONE:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;->LINE:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;->LINE_MARKER:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;->MARKER:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;->SMOOTH:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;->SMOOTH_MARKER:Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 115
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 116
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;->value:Ljava/lang/String;

    .line 117
    return-void
.end method

.method public static convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 124
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;->values()[Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 125
    .local v2, "inst":Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 129
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;
    :goto_1
    return-object v2

    .line 124
    .restart local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 129
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 93
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;

    return-object v0
.end method


# virtual methods
.method public xmlValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartType$EScatterStyle;->value:Ljava/lang/String;

    return-object v0
.end method
