.class public Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPlotAreaHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DMLChartPlotAreaHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 21
    const/16 v0, 0x23

    const-string/jumbo v1, "plotArea"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 22
    return-void
.end method

.method private init()V
    .locals 37

    .prologue
    .line 25
    const/16 v36, 0x10

    move/from16 v0, v36

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    move-object/from16 v29, v0

    .line 27
    .local v29, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartBarChartHandler;

    invoke-direct {v9}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartBarChartHandler;-><init>()V

    .line 28
    .local v9, "barchartHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartBarChartHandler;
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v36, "barChart"

    move-object/from16 v0, v36

    invoke-direct {v10, v0, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 30
    .local v10, "barchartSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v36, 0x0

    aput-object v10, v29, v36

    .line 32
    new-instance v23, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPieChartHandler;

    invoke-direct/range {v23 .. v23}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPieChartHandler;-><init>()V

    .line 33
    .local v23, "piechartHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPieChartHandler;
    new-instance v24, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v36, "pieChart"

    move-object/from16 v0, v24

    move-object/from16 v1, v36

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 35
    .local v24, "piechartSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v36, 0x1

    aput-object v24, v29, v36

    .line 37
    new-instance v17, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartLineChartHandler;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartLineChartHandler;-><init>()V

    .line 38
    .local v17, "linechartHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartLineChartHandler;
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v36, "lineChart"

    move-object/from16 v0, v18

    move-object/from16 v1, v36

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 40
    .local v18, "linechartSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v36, 0x2

    aput-object v18, v29, v36

    .line 42
    new-instance v27, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartScatterChartHandler;

    invoke-direct/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartScatterChartHandler;-><init>()V

    .line 43
    .local v27, "scatterchartHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartScatterChartHandler;
    new-instance v28, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v36, "scatterChart"

    move-object/from16 v0, v28

    move-object/from16 v1, v36

    move-object/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 45
    .local v28, "scatterchartSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v36, 0x3

    aput-object v28, v29, v36

    .line 47
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartBubbleChartHandler;

    invoke-direct {v11}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartBubbleChartHandler;-><init>()V

    .line 48
    .local v11, "bubbleChartHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartBubbleChartHandler;
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v36, "bubbleChart"

    move-object/from16 v0, v36

    invoke-direct {v12, v0, v11}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 50
    .local v12, "bubbleChartSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v36, 0x4

    aput-object v12, v29, v36

    .line 52
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartAreaChartHandler;

    invoke-direct {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartAreaChartHandler;-><init>()V

    .line 53
    .local v5, "areaChartHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartAreaChartHandler;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v36, "areaChart"

    move-object/from16 v0, v36

    invoke-direct {v6, v0, v5}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 55
    .local v6, "areaChartSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v36, 0x5

    aput-object v6, v29, v36

    .line 57
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartBarChartHandler;

    invoke-direct {v7}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartBarChartHandler;-><init>()V

    .line 58
    .local v7, "bar3dchartHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartBarChartHandler;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v36, "bar3DChart"

    move-object/from16 v0, v36

    invoke-direct {v8, v0, v7}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 60
    .local v8, "bar3dchartSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v36, 0x6

    aput-object v8, v29, v36

    .line 62
    new-instance v13, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartDoughnutHandler;

    invoke-direct {v13}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartDoughnutHandler;-><init>()V

    .line 63
    .local v13, "doughchartHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartDoughnutHandler;
    new-instance v14, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v36, "doughnutChart"

    move-object/from16 v0, v36

    invoke-direct {v14, v0, v13}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 65
    .local v14, "doughchartSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v36, 0x7

    aput-object v14, v29, v36

    .line 67
    new-instance v25, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLRadarChartHandler;

    invoke-direct/range {v25 .. v25}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLRadarChartHandler;-><init>()V

    .line 68
    .local v25, "radarHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLRadarChartHandler;
    new-instance v26, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v36, "radarChart"

    move-object/from16 v0, v26

    move-object/from16 v1, v36

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 70
    .local v26, "radarSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v36, 0x8

    aput-object v26, v29, v36

    .line 72
    new-instance v30, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStockHandler;

    invoke-direct/range {v30 .. v30}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStockHandler;-><init>()V

    .line 73
    .local v30, "stockChartHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStockHandler;
    new-instance v31, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v36, "stockChart"

    move-object/from16 v0, v31

    move-object/from16 v1, v36

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 75
    .local v31, "stockSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v36, 0x9

    aput-object v31, v29, v36

    .line 77
    new-instance v15, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLLine3DChartHandler;

    invoke-direct {v15}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLLine3DChartHandler;-><init>()V

    .line 78
    .local v15, "line3DChartHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLLine3DChartHandler;
    new-instance v16, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v36, "line3DChart"

    move-object/from16 v0, v16

    move-object/from16 v1, v36

    invoke-direct {v0, v1, v15}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 80
    .local v16, "line3DChartSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v36, 0xa

    aput-object v16, v29, v36

    .line 82
    new-instance v21, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLPie3DChartHandler;

    invoke-direct/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLPie3DChartHandler;-><init>()V

    .line 83
    .local v21, "pie3DChartHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLPie3DChartHandler;
    new-instance v22, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v36, "pie3DChart"

    move-object/from16 v0, v22

    move-object/from16 v1, v36

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 85
    .local v22, "pie3DChartSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v36, 0xb

    aput-object v22, v29, v36

    .line 87
    new-instance v19, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLOfPieChartHandler;

    invoke-direct/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLOfPieChartHandler;-><init>()V

    .line 88
    .local v19, "ofPieChartHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLOfPieChartHandler;
    new-instance v20, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v36, "ofPieChart"

    move-object/from16 v0, v20

    move-object/from16 v1, v36

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 90
    .local v20, "ofPieChartSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v36, 0xc

    aput-object v20, v29, v36

    .line 92
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLArea3DChartHandler;

    invoke-direct {v3}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLArea3DChartHandler;-><init>()V

    .line 93
    .local v3, "area3DChartHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLArea3DChartHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v36, "area3DChart"

    move-object/from16 v0, v36

    invoke-direct {v4, v0, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 95
    .local v4, "area3DChartSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v36, 0xd

    aput-object v4, v29, v36

    .line 97
    new-instance v32, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLSurface3DChartHandler;

    invoke-direct/range {v32 .. v32}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLSurface3DChartHandler;-><init>()V

    .line 98
    .local v32, "surface3DChartHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLSurface3DChartHandler;
    new-instance v33, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v36, "surface3DChart"

    move-object/from16 v0, v33

    move-object/from16 v1, v36

    move-object/from16 v2, v32

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 100
    .local v33, "surface3DChartSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v36, 0xe

    aput-object v33, v29, v36

    .line 102
    new-instance v34, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLSurfaceChartHandler;

    invoke-direct/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLSurfaceChartHandler;-><init>()V

    .line 103
    .local v34, "surfaceChartHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLSurfaceChartHandler;
    new-instance v35, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v36, "surfaceChart"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move-object/from16 v2, v34

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 105
    .local v35, "surfaceChartSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v36, 0xf

    aput-object v35, v29, v36

    .line 107
    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPlotAreaHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 108
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 113
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 114
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPlotAreaHandler;->init()V

    .line 115
    return-void
.end method
