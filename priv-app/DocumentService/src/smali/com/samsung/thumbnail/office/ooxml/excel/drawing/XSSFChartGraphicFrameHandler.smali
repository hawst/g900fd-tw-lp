.class public Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XSSFChartGraphicFrameHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XSSFChartGraphicFrameHandler.java"


# instance fields
.field graphicFrame:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;


# direct methods
.method public constructor <init>()V
    .locals 9

    .prologue
    const/16 v8, 0x1f

    .line 25
    const/16 v6, 0xc9

    const-string/jumbo v7, "graphicFrame"

    invoke-direct {p0, v6, v7}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 28
    const/4 v6, 0x2

    new-array v4, v6, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 30
    .local v4, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingNvGraphicFramePrHandler;

    invoke-direct {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingNvGraphicFramePrHandler;-><init>()V

    .line 31
    .local v3, "nvspprhandler":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingNvGraphicFramePrHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XdrSeqDescriptor;

    const-string/jumbo v6, "nvGraphicFramePr"

    invoke-direct {v5, v6, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XdrSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 33
    .local v5, "xdr1Seq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XdrSeqDescriptor;
    const/4 v6, 0x0

    aput-object v5, v4, v6

    .line 35
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;

    invoke-direct {v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;-><init>()V

    iput-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XSSFChartGraphicFrameHandler;->graphicFrame:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;

    .line 36
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicHandler;

    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XSSFChartGraphicFrameHandler;->graphicFrame:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;

    invoke-direct {v0, v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;)V

    .line 38
    .local v0, "graphicDataHandler":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/shapes/XPPTGraphicHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/OOXMLSingleElementHandler;

    const-string/jumbo v6, "graphic"

    invoke-direct {v1, v8, v6, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSingleElementHandler;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 42
    .local v1, "graphicDataSingleHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLSingleElementHandler;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    const-string/jumbo v6, "graphic"

    invoke-direct {v2, v8, v6, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 45
    .local v2, "graphicHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    const/4 v6, 0x1

    aput-object v2, v4, v6

    .line 47
    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XSSFChartGraphicFrameHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 48
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 60
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XSSFChartGraphicFrameHandler;->graphicFrame:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;->getIsChart()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 61
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    .line 62
    .local v0, "drawing":Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XSSFChartGraphicFrameHandler;->graphicFrame:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFGraphicFrame;->getRelId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->addChartRelId(Ljava/lang/String;)V

    .line 63
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getCurrentCellAnchor()Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    move-result-object v1

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;->CHART:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->setType(Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;)V

    .line 66
    .end local v0    # "drawing":Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    :cond_0
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 53
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 54
    return-void
.end method
