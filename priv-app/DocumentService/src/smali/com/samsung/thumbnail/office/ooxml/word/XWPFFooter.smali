.class public Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;
.super Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;
.source "XWPFFooter.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/IBodyElement;


# instance fields
.field private _footerName:Ljava/lang/String;

.field private relArray:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;-><init>()V

    .line 48
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;)V
    .locals 0
    .param p1, "doc"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/POIXMLDocumentPart;Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V
    .locals 1
    .param p1, "parent"    # Lorg/apache/poi/POIXMLDocumentPart;
    .param p2, "part"    # Lorg/apache/poi/openxml4j/opc/PackagePart;
    .param p3, "rel"    # Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;-><init>(Lorg/apache/poi/POIXMLDocumentPart;Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V

    .line 66
    invoke-virtual {p2}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getPartName()Lorg/apache/poi/openxml4j/opc/PackagePartName;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;->_footerName:Ljava/lang/String;

    .line 67
    invoke-virtual {p2}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getRelArray()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;->relArray:Ljava/util/List;

    .line 68
    return-void
.end method


# virtual methods
.method public getElementType()Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;
    .locals 1

    .prologue
    .line 110
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->FOOTER:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    return-object v0
.end method

.method public getFooterName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;->_footerName:Ljava/lang/String;

    return-object v0
.end method

.method public getRelList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;->relArray:Ljava/util/List;

    return-object v0
.end method

.method protected onDocumentRead(Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;)V
    .locals 2
    .param p1, "consumer"    # Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    invoke-super {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;->onDocumentRead()V

    .line 78
    const/4 v0, 0x0

    .line 80
    .local v0, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 81
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XDocFooterParser;

    invoke-direct {v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XDocFooterParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;)V

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/hdrftr/XDocFooterParser;->parse(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    if-eqz v0, :cond_0

    .line 85
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 101
    :cond_0
    return-void

    .line 84
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    .line 85
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    :cond_1
    throw v1
.end method

.method public setFooterName(Ljava/lang/String;)V
    .locals 0
    .param p1, "footerName"    # Ljava/lang/String;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;->_footerName:Ljava/lang/String;

    .line 40
    return-void
.end method
