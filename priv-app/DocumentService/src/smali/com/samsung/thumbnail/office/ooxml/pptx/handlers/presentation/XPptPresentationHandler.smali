.class public Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/XPptPresentationHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XPptPresentationHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    const/16 v11, 0x33

    .line 14
    const-string/jumbo v10, "presentation"

    invoke-direct {p0, v10}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 16
    const/4 v10, 0x4

    new-array v3, v10, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 17
    .local v3, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/SlideSizeHandler;

    invoke-direct {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/SlideSizeHandler;-><init>()V

    .line 18
    .local v4, "sizeHandler":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/SlideSizeHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;

    const-string/jumbo v10, "sldSz"

    invoke-direct {v5, v11, v10, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 21
    .local v5, "sizeSeq":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;
    const/4 v10, 0x0

    aput-object v5, v3, v10

    .line 23
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/SlideIdsListHandler;

    invoke-direct {v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/SlideIdsListHandler;-><init>()V

    .line 24
    .local v6, "slideIdLstHandler":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/SlideIdsListHandler;
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;

    const-string/jumbo v10, "sldIdLst"

    invoke-direct {v7, v11, v10, v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 27
    .local v7, "slideIdSeq":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;
    const/4 v10, 0x1

    aput-object v7, v3, v10

    .line 29
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/SlideMasterIdHandler;

    invoke-direct {v8}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/SlideMasterIdHandler;-><init>()V

    .line 30
    .local v8, "slideMasterIdLstHandler":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/SlideMasterIdHandler;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/OOXMLArrayHandler;

    const-string/jumbo v10, "sldMasterIdLst"

    invoke-direct {v2, v10, v8}, Lcom/samsung/thumbnail/office/ooxml/OOXMLArrayHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 32
    .local v2, "masterIDLst":Lcom/samsung/thumbnail/office/ooxml/OOXMLArrayHandler;
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;

    const-string/jumbo v10, "sldMasterIdLst"

    invoke-direct {v9, v11, v10, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 35
    .local v9, "slideMasterIdSeq":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;
    const/4 v10, 0x2

    aput-object v9, v3, v10

    .line 37
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;

    const-string/jumbo v10, "defaultTextStyle"

    invoke-direct {v0, v10}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;-><init>(Ljava/lang/String;)V

    .line 39
    .local v0, "defTextStyleHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;

    const-string/jumbo v10, "defaultTextStyle"

    invoke-direct {v1, v11, v10, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 42
    .local v1, "defTextStyleSeq":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;
    const/4 v10, 0x3

    aput-object v1, v3, v10

    .line 44
    iput-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/XPptPresentationHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 45
    return-void
.end method


# virtual methods
.method protected init()V
    .locals 3

    .prologue
    .line 48
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/SlideSizeHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/SlideSizeHandler;-><init>()V

    .line 49
    .local v0, "sizeHandler":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/SlideSizeHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/presentation/XPptPresentationHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "sldSz"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    return-void
.end method
