.class public Lcom/samsung/thumbnail/office/ooxml/dml/wp/EffectExtentHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "EffectExtentHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/wp/EffectExtentHandler$IEffectExtentObserver;
    }
.end annotation


# instance fields
.field private extentOberserver:Lcom/samsung/thumbnail/office/ooxml/dml/wp/EffectExtentHandler$IEffectExtentObserver;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/wp/EffectExtentHandler$IEffectExtentObserver;)V
    .locals 2
    .param p1, "extentOberserver"    # Lcom/samsung/thumbnail/office/ooxml/dml/wp/EffectExtentHandler$IEffectExtentObserver;

    .prologue
    .line 19
    const/16 v0, 0x1e

    const-string/jumbo v1, "effectExtent"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 20
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/EffectExtentHandler;->extentOberserver:Lcom/samsung/thumbnail/office/ooxml/dml/wp/EffectExtentHandler$IEffectExtentObserver;

    .line 21
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 10
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 26
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;

    const-string/jumbo v0, "l"

    invoke-virtual {p0, p3, v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/EffectExtentHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;-><init>(Ljava/lang/String;)V

    .line 28
    .local v7, "l":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;

    const-string/jumbo v0, "r"

    invoke-virtual {p0, p3, v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/EffectExtentHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;-><init>(Ljava/lang/String;)V

    .line 30
    .local v8, "r":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;

    const-string/jumbo v0, "b"

    invoke-virtual {p0, p3, v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/EffectExtentHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;-><init>(Ljava/lang/String;)V

    .line 32
    .local v6, "b":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;

    const-string/jumbo v0, "t"

    invoke-virtual {p0, p3, v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/EffectExtentHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v9, v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;-><init>(Ljava/lang/String;)V

    .line 34
    .local v9, "t":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/EffectExtentHandler;->extentOberserver:Lcom/samsung/thumbnail/office/ooxml/dml/wp/EffectExtentHandler$IEffectExtentObserver;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;->getValue()I

    move-result v1

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;->getValue()I

    move-result v2

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;->getValue()I

    move-result v3

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;->getValue()I

    move-result v4

    move-object v5, p1

    invoke-interface/range {v0 .. v5}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/EffectExtentHandler$IEffectExtentObserver;->setEffectExtent(IIIILcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V

    .line 36
    return-void
.end method
