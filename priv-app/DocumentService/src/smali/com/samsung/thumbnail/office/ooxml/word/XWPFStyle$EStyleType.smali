.class public final enum Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;
.super Ljava/lang/Enum;
.source "XWPFStyle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EStyleType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

.field public static final enum CHARACTER:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

.field public static final enum NUMBERING:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

.field public static final enum PARAGRAPH:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

.field public static final enum TABLE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 204
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    const-string/jumbo v1, "CHARACTER"

    const-string/jumbo v2, "character"

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->CHARACTER:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    const-string/jumbo v1, "NUMBERING"

    const-string/jumbo v2, "numbering"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->NUMBERING:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    const-string/jumbo v1, "PARAGRAPH"

    const-string/jumbo v2, "paragraph"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->PARAGRAPH:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    const-string/jumbo v1, "TABLE"

    const-string/jumbo v2, "table"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->TABLE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    .line 203
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->CHARACTER:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->NUMBERING:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->PARAGRAPH:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->TABLE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 209
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 210
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->value:Ljava/lang/String;

    .line 211
    return-void
.end method

.method public static convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 218
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->values()[Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 219
    .local v2, "inst":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 223
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;
    :goto_1
    return-object v2

    .line 218
    .restart local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 223
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 203
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;
    .locals 1

    .prologue
    .line 203
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    return-object v0
.end method


# virtual methods
.method public xmlValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->value:Ljava/lang/String;

    return-object v0
.end method
