.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStrStyleUnderlineHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelSharedStrStyleUnderlineHandler.java"


# instance fields
.field underline:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    const-string/jumbo v0, "u"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 25
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 30
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 31
    const-string/jumbo v1, "val"

    invoke-interface {p3, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStrStyleUnderlineHandler;->underline:Ljava/lang/String;

    .line 32
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStrStyleUnderlineHandler;->underline:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 33
    const-string/jumbo v1, "underline"

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStrStyleUnderlineHandler;->underline:Ljava/lang/String;

    .line 35
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v0

    .line 37
    .local v0, "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStrStyleUnderlineHandler;->underline:Ljava/lang/String;

    iget v3, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    invoke-virtual {v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->addExSharedStringStyleUnderline(Ljava/lang/String;I)V

    .line 41
    return-void
.end method
