.class public Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;
.super Ljava/lang/Object;
.source "EMR_Eof.java"


# instance fields
.field private Size:I

.field private Sizelast:I

.field private Type:I

.field fileContent:[B

.field private nPalEntries:I

.field private offPalEntries:I

.field private offset:I


# direct methods
.method public constructor <init>([BI)V
    .locals 0
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->fileContent:[B

    .line 18
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->offset:I

    .line 19
    return-void
.end method


# virtual methods
.method public getRecordSize()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->Size:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->Type:I

    return v0
.end method

.method public getSizelast()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->Sizelast:I

    return v0
.end method

.method public getnPalEntries()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->nPalEntries:I

    return v0
.end method

.method public getoffPalEntries()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->offPalEntries:I

    return v0
.end method

.method public readEMR_Eof()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x0

    .line 62
    const/4 v3, 0x0

    .line 64
    .local v3, "i":I
    const/4 v7, 0x4

    new-array v4, v7, [B

    .line 66
    .local v4, "intConvert":[B
    const/4 v3, 0x0

    :goto_0
    if-gt v3, v11, :cond_0

    .line 67
    rsub-int/lit8 v7, v3, 0x3

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->fileContent:[B

    iget v9, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->offset:I

    add-int/2addr v9, v3

    aget-byte v8, v8, v9

    aput-byte v8, v4, v7

    .line 66
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 68
    :cond_0
    invoke-static {v4, v10}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v2

    .line 69
    .local v2, "Type":I
    iget v7, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->offset:I

    add-int/2addr v7, v3

    iput v7, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->offset:I

    .line 70
    invoke-virtual {p0, v2}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->setRecordType(I)V

    .line 72
    const/4 v3, 0x0

    :goto_1
    if-gt v3, v11, :cond_1

    .line 73
    rsub-int/lit8 v7, v3, 0x3

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->fileContent:[B

    iget v9, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->offset:I

    add-int/2addr v9, v3

    aget-byte v8, v8, v9

    aput-byte v8, v4, v7

    .line 72
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 74
    :cond_1
    invoke-static {v4, v10}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 75
    .local v0, "Size":I
    iget v7, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->offset:I

    add-int/2addr v7, v3

    iput v7, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->offset:I

    .line 76
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->setRecordSize(I)V

    .line 78
    const/4 v3, 0x0

    :goto_2
    if-gt v3, v11, :cond_2

    .line 79
    rsub-int/lit8 v7, v3, 0x3

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->fileContent:[B

    iget v9, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->offset:I

    add-int/2addr v9, v3

    aget-byte v8, v8, v9

    aput-byte v8, v4, v7

    .line 78
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 80
    :cond_2
    invoke-static {v4, v10}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v5

    .line 81
    .local v5, "nPalEntries":I
    iget v7, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->offset:I

    add-int/2addr v7, v3

    iput v7, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->offset:I

    .line 82
    invoke-virtual {p0, v5}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->setnPalEntries(I)V

    .line 84
    const/4 v3, 0x0

    :goto_3
    if-gt v3, v11, :cond_3

    .line 85
    rsub-int/lit8 v7, v3, 0x3

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->fileContent:[B

    iget v9, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->offset:I

    add-int/2addr v9, v3

    aget-byte v8, v8, v9

    aput-byte v8, v4, v7

    .line 84
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 86
    :cond_3
    invoke-static {v4, v10}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v6

    .line 87
    .local v6, "offPalEntries":I
    iget v7, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->offset:I

    add-int/2addr v7, v3

    iput v7, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->offset:I

    .line 88
    invoke-virtual {p0, v6}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->setoffPalEntries(I)V

    .line 90
    const/4 v3, 0x0

    :goto_4
    if-gt v3, v11, :cond_4

    .line 91
    rsub-int/lit8 v7, v3, 0x3

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->fileContent:[B

    iget v9, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->offset:I

    add-int/2addr v9, v3

    aget-byte v8, v8, v9

    aput-byte v8, v4, v7

    .line 90
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 92
    :cond_4
    invoke-static {v4, v10}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 93
    .local v1, "Sizelast":I
    iget v7, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->offset:I

    add-int/2addr v7, v3

    iput v7, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->offset:I

    .line 94
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->setSizelast(I)V

    .line 95
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "RecrdSize"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->Size:I

    .line 35
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "RecrdType"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->Type:I

    .line 27
    return-void
.end method

.method public setSizelast(I)V
    .locals 0
    .param p1, "Szelast"    # I

    .prologue
    .line 58
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->Sizelast:I

    .line 59
    return-void
.end method

.method public setnPalEntries(I)V
    .locals 0
    .param p1, "nPlEntries"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->nPalEntries:I

    .line 43
    return-void
.end method

.method public setoffPalEntries(I)V
    .locals 0
    .param p1, "offPlEntries"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfControlRecords/EMR_Eof;->offPalEntries:I

    .line 51
    return-void
.end method
