.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/PatternFillHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "PatternFillHandler.java"


# instance fields
.field private patternFill:Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    const-string/jumbo v0, "patternFill"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 21
    return-void
.end method

.method private init()V
    .locals 3

    .prologue
    .line 24
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/PatternFillHandler;->patternFill:Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;

    .line 25
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/FillFGColorHandler;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/PatternFillHandler;->patternFill:Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/FillFGColorHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;)V

    .line 26
    .local v0, "fgcolorhandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/FillFGColorHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/PatternFillHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "fgColor"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/PatternFillHandler;->patternFill:Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addFill(Lcom/samsung/thumbnail/office/ooxml/excel/IFill;)V

    .line 45
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 33
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 34
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/PatternFillHandler;->init()V

    .line 38
    return-void
.end method
