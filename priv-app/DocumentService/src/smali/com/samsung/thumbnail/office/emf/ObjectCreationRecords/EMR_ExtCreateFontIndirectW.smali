.class public Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;
.super Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;
.source "EMR_ExtCreateFontIndirectW.java"


# instance fields
.field private FontBrush:I

.field private Size:I

.field private Type:I

.field fileContent:[B

.field private ihFonts:I

.field private logFontEx:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;

.field private offset:I

.field private sizeOfLogFont:I

.field private sizeOfLogFontEx:I

.field private sizeOfLogFontPanose:I


# direct methods
.method public constructor <init>([BI)V
    .locals 1
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;-><init>()V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->logFontEx:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;

    .line 12
    const/16 v0, 0x140

    iput v0, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->sizeOfLogFontPanose:I

    .line 13
    const/16 v0, 0x52

    iput v0, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->sizeOfLogFont:I

    .line 14
    const/16 v0, 0x152

    iput v0, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->sizeOfLogFontEx:I

    .line 20
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->fileContent:[B

    .line 21
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->offset:I

    .line 22
    return-void
.end method


# virtual methods
.method public getFontBrush()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->FontBrush:I

    return v0
.end method

.method public getIhFonts()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->ihFonts:I

    return v0
.end method

.method public getLogFontEx()Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->logFontEx:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;

    return-object v0
.end method

.method public getOffset()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->offset:I

    return v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->Size:I

    return v0
.end method

.method public getSizeOfLogFontPanose()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->sizeOfLogFontPanose:I

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->Type:I

    return v0
.end method

.method public readEmfExtCreateFontIndirectW()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x0

    .line 81
    const/4 v3, 0x0

    .line 83
    .local v3, "i":I
    const/4 v7, 0x4

    new-array v5, v7, [B

    .line 85
    .local v5, "intConvert":[B
    const/4 v3, 0x0

    :goto_0
    if-gt v3, v11, :cond_0

    .line 86
    rsub-int/lit8 v7, v3, 0x3

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->fileContent:[B

    iget v9, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->offset:I

    add-int/2addr v9, v3

    aget-byte v8, v8, v9

    aput-byte v8, v5, v7

    .line 85
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 87
    :cond_0
    invoke-static {v5, v10}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 88
    .local v1, "Type":I
    iget v7, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->offset:I

    add-int/2addr v7, v3

    iput v7, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->offset:I

    .line 89
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->setType(I)V

    .line 90
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->setObjectType(I)V

    .line 92
    const/4 v3, 0x0

    :goto_1
    if-gt v3, v11, :cond_1

    .line 93
    rsub-int/lit8 v7, v3, 0x3

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->fileContent:[B

    iget v9, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->offset:I

    add-int/2addr v9, v3

    aget-byte v8, v8, v9

    aput-byte v8, v5, v7

    .line 92
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 94
    :cond_1
    invoke-static {v5, v10}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 95
    .local v0, "Size":I
    iget v7, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->offset:I

    add-int/2addr v7, v3

    iput v7, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->offset:I

    .line 96
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->setSize(I)V

    .line 98
    const/4 v3, 0x0

    :goto_2
    if-gt v3, v11, :cond_2

    .line 99
    rsub-int/lit8 v7, v3, 0x3

    iget-object v8, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->fileContent:[B

    iget v9, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->offset:I

    add-int/2addr v9, v3

    aget-byte v8, v8, v9

    aput-byte v8, v5, v7

    .line 98
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 100
    :cond_2
    invoke-static {v5, v10}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v4

    .line 101
    .local v4, "ihFonts":I
    iget v7, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->offset:I

    add-int/2addr v7, v3

    iput v7, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->offset:I

    .line 102
    invoke-virtual {p0, v4}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->setIhFonts(I)V

    .line 103
    invoke-virtual {p0, v4}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->sethandleNumber(I)V

    .line 105
    add-int/lit8 v2, v0, -0xc

    .line 107
    .local v2, "buffsize":I
    iget v7, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->sizeOfLogFontPanose:I

    if-le v2, v7, :cond_3

    .line 108
    new-instance v6, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->fileContent:[B

    iget v8, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->offset:I

    invoke-direct {v6, v7, v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;-><init>([BI)V

    .line 109
    .local v6, "logFontEx":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->readEmrLogFontEx()V

    .line 110
    iget v7, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->offset:I

    iget v8, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->sizeOfLogFontEx:I

    add-int/2addr v7, v8

    iput v7, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->offset:I

    .line 111
    invoke-virtual {p0, v6}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->setLogFontEx(Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;)V

    .line 118
    :goto_3
    return-void

    .line 113
    .end local v6    # "logFontEx":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;
    :cond_3
    new-instance v6, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->fileContent:[B

    iget v8, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->offset:I

    invoke-direct {v6, v7, v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;-><init>([BI)V

    .line 114
    .restart local v6    # "logFontEx":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;->readEmrLogFont()V

    .line 115
    iget v7, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->offset:I

    iget v8, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->sizeOfLogFont:I

    add-int/2addr v7, v8

    iput v7, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->offset:I

    .line 116
    invoke-virtual {p0, v6}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->setLogFontEx(Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;)V

    goto :goto_3
.end method

.method public setFontBrush(I)V
    .locals 0
    .param p1, "Brsh"    # I

    .prologue
    .line 77
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->FontBrush:I

    .line 78
    return-void
.end method

.method public setIhFonts(I)V
    .locals 0
    .param p1, "ihFonts"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->ihFonts:I

    .line 54
    return-void
.end method

.method public setLogFontEx(Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;)V
    .locals 0
    .param p1, "logFontEx"    # Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->logFontEx:Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMF_LogFontEx;

    .line 62
    return-void
.end method

.method public setOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->offset:I

    .line 30
    return-void
.end method

.method public setSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 45
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->Size:I

    .line 46
    return-void
.end method

.method public setSizeOfLogFontPanose(I)V
    .locals 0
    .param p1, "sizeOfLogFontPanose"    # I

    .prologue
    .line 69
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->sizeOfLogFontPanose:I

    .line 70
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 37
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_ExtCreateFontIndirectW;->Type:I

    .line 38
    return-void
.end method
