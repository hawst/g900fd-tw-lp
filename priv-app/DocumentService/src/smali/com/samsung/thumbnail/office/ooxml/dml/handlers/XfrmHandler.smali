.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XfrmHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$ChSizeConsumer;,
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$ChPointConsumer;,
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$SizeConsumer;,
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$PointConsumer;
    }
.end annotation


# instance fields
.field private shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;


# direct methods
.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 15
    .param p1, "nsID"    # I
    .param p2, "shapeProperties"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 23
    const-string/jumbo v14, "xfrm"

    move/from16 v0, p1

    invoke-direct {p0, v0, v14}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 25
    move-object/from16 v0, p2

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 26
    const/4 v14, 0x4

    new-array v13, v14, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 28
    .local v13, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$PointConsumer;

    move-object/from16 v0, p2

    invoke-direct {v12, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$PointConsumer;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 29
    .local v12, "pConsumer":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$PointConsumer;
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler;

    const-string/jumbo v14, "off"

    invoke-direct {v10, v14, v12}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler$IPointConsumer;)V

    .line 31
    .local v10, "offPointHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler;
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v14, "off"

    invoke-direct {v11, v14, v10}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 33
    .local v11, "offSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v14, 0x0

    aput-object v11, v13, v14

    .line 35
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$SizeConsumer;

    move-object/from16 v0, p2

    invoke-direct {v9, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$SizeConsumer;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 36
    .local v9, "extSzConsumer":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$SizeConsumer;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Size2DHandler;

    const-string/jumbo v14, "ext"

    invoke-direct {v8, v14, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Size2DHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Size2DHandler$ISizeConsumer;)V

    .line 38
    .local v8, "extSz2DHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Size2DHandler;
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v14, "ext"

    invoke-direct {v7, v14, v8}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 40
    .local v7, "extSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v14, 0x1

    aput-object v7, v13, v14

    .line 42
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$ChPointConsumer;

    move-object/from16 v0, p2

    invoke-direct {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$ChPointConsumer;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 43
    .local v1, "chPointConsumer":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$ChPointConsumer;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler;

    const-string/jumbo v14, "chOff"

    invoke-direct {v5, v14, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler$IPointConsumer;)V

    .line 45
    .local v5, "choffPointHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Point2DHandler;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v14, "chOff"

    invoke-direct {v6, v14, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 47
    .local v6, "choffSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v14, 0x2

    aput-object v6, v13, v14

    .line 49
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$ChSizeConsumer;

    move-object/from16 v0, p2

    invoke-direct {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$ChSizeConsumer;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 50
    .local v2, "chSzConsumer":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler$ChSizeConsumer;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Size2DHandler;

    const-string/jumbo v14, "chExt"

    invoke-direct {v4, v14, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Size2DHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Size2DHandler$ISizeConsumer;)V

    .line 52
    .local v4, "chextSzHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/Size2DHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v14, "chExt"

    invoke-direct {v3, v14, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 54
    .local v3, "chextSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v14, 0x3

    aput-object v3, v13, v14

    .line 56
    iput-object v13, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 57
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 62
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 64
    const-string/jumbo v3, "rot"

    invoke-virtual {p0, p3, v3, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    .line 65
    .local v2, "val":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 66
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setRotation(I)V

    .line 69
    :cond_0
    const-string/jumbo v3, "flipH"

    invoke-virtual {p0, p3, v3, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    .line 70
    if-eqz v2, :cond_1

    .line 71
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;

    invoke-direct {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;-><init>(Ljava/lang/String;)V

    .line 72
    .local v0, "flipH":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;->getValue()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setFlipH(Z)V

    .line 75
    .end local v0    # "flipH":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;
    :cond_1
    const-string/jumbo v3, "flipV"

    invoke-virtual {p0, p3, v3, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    .line 76
    if-eqz v2, :cond_2

    .line 77
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;

    invoke-direct {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;-><init>(Ljava/lang/String;)V

    .line 78
    .local v1, "flipV":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;->getValue()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setFlipV(Z)V

    .line 80
    .end local v1    # "flipV":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;
    :cond_2
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/XfrmHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setTransform()V

    .line 81
    return-void
.end method
