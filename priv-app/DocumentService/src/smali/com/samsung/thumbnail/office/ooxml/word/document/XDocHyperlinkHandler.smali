.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocHyperlinkHandler;
.super Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPContent;
.source "XDocHyperlinkHandler.java"


# instance fields
.field private hyperLinkRun:Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlinkRun;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    const-string/jumbo v0, "hyperlink"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPContent;-><init>(Ljava/lang/String;)V

    .line 19
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPContent;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 45
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->endHyperLink()V

    .line 46
    return-void
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 39
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPContent;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 40
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 24
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPContent;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 25
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlinkRun;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlinkRun;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocHyperlinkHandler;->hyperLinkRun:Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlinkRun;

    .line 27
    const-string/jumbo v1, "anchor"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocHyperlinkHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 28
    .local v0, "value":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocHyperlinkHandler;->hyperLinkRun:Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlinkRun;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlinkRun;->setAnchor(Ljava/lang/String;)V

    .line 29
    const-string/jumbo v1, "id"

    const/16 v2, 0xa

    invoke-virtual {p0, p3, v1, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocHyperlinkHandler;->getAttrForNS(Lorg/xml/sax/Attributes;Ljava/lang/String;ILcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 31
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocHyperlinkHandler;->hyperLinkRun:Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlinkRun;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlinkRun;->setHyperlinkId(Ljava/lang/String;)V

    .line 33
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocHyperlinkHandler;->hyperLinkRun:Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlinkRun;

    invoke-interface {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->startHyperLink(Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlinkRun;)V

    .line 34
    return-void
.end method
