.class public final enum Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;
.super Ljava/lang/Enum;
.source "TextAlignmentModeFlags.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

.field public static final enum TA_BASELINE:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

.field public static final enum TA_BOTTOM:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

.field public static final enum TA_CENTER:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

.field public static final enum TA_LEFT:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

.field public static final enum TA_NOUPDATECP:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

.field public static final enum TA_RIGHT:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

.field public static final enum TA_RTLREADING:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

.field public static final enum TA_TOP:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

.field public static final enum TA_UPDATECP:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;


# instance fields
.field private HorizontalTextAlignmentType:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x6

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 4
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    const-string/jumbo v1, "TA_NOUPDATECP"

    invoke-direct {v0, v1, v4, v4}, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->TA_NOUPDATECP:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    const-string/jumbo v1, "TA_LEFT"

    invoke-direct {v0, v1, v5, v4}, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->TA_LEFT:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    const-string/jumbo v1, "TA_TOP"

    invoke-direct {v0, v1, v6, v4}, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->TA_TOP:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    const-string/jumbo v1, "TA_UPDATECP"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2, v5}, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->TA_UPDATECP:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    const-string/jumbo v1, "TA_RIGHT"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2, v6}, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->TA_RIGHT:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    const-string/jumbo v1, "TA_CENTER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v7}, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->TA_CENTER:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    .line 5
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    const-string/jumbo v1, "TA_BOTTOM"

    invoke-direct {v0, v1, v7, v8}, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->TA_BOTTOM:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    const-string/jumbo v1, "TA_BASELINE"

    const/4 v2, 0x7

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->TA_BASELINE:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    const-string/jumbo v1, "TA_RTLREADING"

    const/16 v2, 0x100

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->TA_RTLREADING:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    .line 3
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    sget-object v1, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->TA_NOUPDATECP:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->TA_LEFT:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->TA_TOP:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    aput-object v1, v0, v6

    const/4 v1, 0x3

    sget-object v2, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->TA_UPDATECP:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->TA_RIGHT:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->TA_CENTER:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    aput-object v2, v0, v1

    sget-object v1, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->TA_BOTTOM:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    aput-object v1, v0, v7

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->TA_BASELINE:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    aput-object v2, v0, v1

    sget-object v1, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->TA_RTLREADING:Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    aput-object v1, v0, v8

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->$VALUES:[Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 10
    iput p3, p0, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->HorizontalTextAlignmentType:I

    .line 11
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->$VALUES:[Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;

    return-object v0
.end method


# virtual methods
.method public getTextAlignmentModeFlag()I
    .locals 1

    .prologue
    .line 14
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfEnums/TextAlignmentModeFlags;->HorizontalTextAlignmentType:I

    return v0
.end method
