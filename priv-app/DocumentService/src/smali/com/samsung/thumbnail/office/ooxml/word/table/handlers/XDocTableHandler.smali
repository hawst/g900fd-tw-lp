.class public Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XDocTableHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblGridHandler$ITblGridObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler$ITblPropObserver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    const-string/jumbo v0, "tbl"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 23
    return-void
.end method

.method private init()V
    .locals 5

    .prologue
    .line 26
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;

    invoke-direct {v0, p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler$ITblPropObserver;)V

    .line 27
    .local v0, "tablePr":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v4, "tblPr"

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRHandler;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRHandler;-><init>()V

    .line 29
    .local v2, "tblRowHanlder":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRHandler;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v4, "tr"

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblGridHandler;

    invoke-direct {v1, p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblGridHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblGridHandler$ITblGridObserver;)V

    .line 31
    .local v1, "tblGridHandler":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblGridHandler;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v4, "tblGrid"

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 52
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->endTable()V

    .line 53
    return-void
.end method

.method public setTblGridProp(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    .local p2, "cellWidthList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->setTblGridColWidth(Ljava/util/ArrayList;)V

    .line 65
    return-void
.end method

.method public setTblProp(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "tblProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    .prologue
    .line 57
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->setTblProperty(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;)V

    .line 58
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 37
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 38
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTableHandler;->init()V

    .line 39
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->startTable()V

    .line 40
    return-void
.end method
