.class public Lcom/samsung/thumbnail/office/ooxml/pptx/XPPTChartParser;
.super Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBaseParser;
.source "XPPTChartParser.java"


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .param p1, "consumer"    # Ljava/lang/Object;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBaseParser;-><init>(Ljava/lang/Object;)V

    .line 12
    return-void
.end method


# virtual methods
.method public createRootElementHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartHandler;-><init>()V

    return-object v0
.end method

.method public getMainNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .locals 1

    .prologue
    .line 24
    const/16 v0, 0x23

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XPPTChartParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v0

    return-object v0
.end method

.method public startParse()V
    .locals 0

    .prologue
    .line 15
    return-void
.end method
