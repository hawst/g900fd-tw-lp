.class public Lcom/samsung/thumbnail/office/ooxml/excel/table/XExceTableColumnHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExceTableColumnHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    const/16 v0, 0xc8

    const-string/jumbo v1, "tableColumn"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 26
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 31
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 32
    const-string/jumbo v2, "id"

    invoke-interface {p3, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 33
    .local v0, "id":Ljava/lang/String;
    const-string/jumbo v2, "name"

    invoke-interface {p3, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 35
    .local v1, "name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/table/XWPFExcelTableParser;

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/table/XWPFExcelTableParser;->addExTableColumnID(Ljava/lang/String;)V

    .line 36
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/table/XWPFExcelTableParser;

    invoke-virtual {v2, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/table/XWPFExcelTableParser;->addExTableColumnName(Ljava/lang/String;)V

    .line 40
    return-void
.end method
