.class public Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
.super Ljava/lang/Object;
.source "XWPFShape.java"


# instance fields
.field private grpZorder:J

.field private mParagraphArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/hslf/DocParagraph;",
            ">;"
        }
    .end annotation
.end field

.field private mTxtBxContents:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/hslf/DocParagraph;",
            ">;"
        }
    .end annotation
.end field

.field private mVertAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

.field private orientParam:Ljava/lang/String;

.field private sId:Ljava/lang/String;

.field private shapeContents:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/IBodyElement;",
            ">;"
        }
    .end annotation
.end field

.field private shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V
    .locals 2
    .param p1, "shapeProp"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->grpZorder:J

    .line 24
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->shapeContents:Ljava/util/ArrayList;

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->mParagraphArray:Ljava/util/ArrayList;

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->mTxtBxContents:Ljava/util/ArrayList;

    .line 28
    return-void
.end method

.method public static getColor(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "style"    # Ljava/lang/String;

    .prologue
    .line 106
    if-eqz p0, :cond_0

    const-string/jumbo v0, "#"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x7

    if-lt v0, v1, :cond_0

    .line 107
    const-string/jumbo v0, "#"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const-string/jumbo v1, "#"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x7

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 112
    :cond_0
    return-object p0
.end method

.method public static getValuesFromStyle(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 5
    .param p0, "style"    # Ljava/lang/String;
    .param p1, "doTwipsConversion"    # Z

    .prologue
    const/4 v4, 0x0

    .line 117
    const-string/jumbo v2, ";"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 118
    const-string/jumbo v2, ":"

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    const-string/jumbo v3, ";"

    invoke-virtual {p0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 123
    :cond_0
    :goto_0
    const-string/jumbo v2, "pt"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 124
    const-string/jumbo v2, "pt"

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 145
    :goto_1
    return-object p0

    .line 119
    :cond_1
    const-string/jumbo v2, ":"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 120
    const-string/jumbo v2, ":"

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 125
    :cond_2
    const-string/jumbo v2, "in"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 126
    const-string/jumbo v2, "in"

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 128
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    const/high16 v4, 0x42900000    # 72.0f

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    .line 129
    :cond_3
    if-eqz p1, :cond_4

    .line 131
    :try_start_0
    invoke-static {p0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    float-to-int v2, v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v1

    .line 133
    .local v1, "val":F
    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    goto :goto_1

    .line 134
    .end local v1    # "val":F
    :catch_0
    move-exception v0

    .line 135
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string/jumbo p0, "0"

    .line 136
    goto :goto_1

    .line 139
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :cond_4
    :try_start_1
    invoke-static {p0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints1(F)F

    move-result v2

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float v1, v2, v3

    .line 140
    .restart local v1    # "val":F
    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object p0

    goto :goto_1

    .line 141
    .end local v1    # "val":F
    :catch_1
    move-exception v0

    .line 142
    .restart local v0    # "e":Ljava/lang/NumberFormatException;
    const-string/jumbo p0, "0"

    goto :goto_1
.end method

.method public static parseFloat(Ljava/lang/String;)F
    .locals 2
    .param p0, "style"    # Ljava/lang/String;

    .prologue
    .line 149
    if-nez p0, :cond_0

    .line 150
    const/4 v1, 0x0

    .line 157
    :goto_0
    return v1

    .line 151
    :cond_0
    const/4 v1, 0x0

    .line 153
    .local v1, "val":F
    :try_start_0
    invoke-static {p0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 154
    :catch_0
    move-exception v0

    .line 155
    .local v0, "e":Ljava/lang/NumberFormatException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static parseString(Ljava/lang/String;)F
    .locals 3
    .param p0, "val"    # Ljava/lang/String;

    .prologue
    .line 98
    const/high16 v0, 0x3f800000    # 1.0f

    .line 99
    .local v0, "width":F
    const-string/jumbo v1, "pt"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 100
    const/4 v1, 0x0

    const-string/jumbo v2, "pt"

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    .line 102
    :cond_0
    return v0
.end method


# virtual methods
.method public addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V
    .locals 1
    .param p1, "paragraph"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->mParagraphArray:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->mParagraphArray:Ljava/util/ArrayList;

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->mParagraphArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    return-void
.end method

.method public addTxtBoxContent(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V
    .locals 1
    .param p1, "content"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->mTxtBxContents:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    return-void
.end method

.method public addTxtBoxContent(Lcom/samsung/thumbnail/office/ooxml/word/IBodyElement;)V
    .locals 1
    .param p1, "content"    # Lcom/samsung/thumbnail/office/ooxml/word/IBodyElement;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->shapeContents:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    return-void
.end method

.method public getAllParagraph()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/hslf/DocParagraph;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->mParagraphArray:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getContent()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/IBodyElement;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->shapeContents:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getGrpZorder()J
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->grpZorder:J

    return-wide v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->sId:Ljava/lang/String;

    return-object v0
.end method

.method public getOrientParam()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->orientParam:Ljava/lang/String;

    return-object v0
.end method

.method public getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    return-object v0
.end method

.method public getTxtBxContents()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/hslf/DocParagraph;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->mTxtBxContents:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getVerticalAlignmentElement()Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->mVertAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    return-object v0
.end method

.method public merge(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V
    .locals 2
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    .prologue
    .line 92
    if-eqz p1, :cond_0

    .line 93
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->merge(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 95
    :cond_0
    return-void
.end method

.method public setGrpZorder(J)V
    .locals 1
    .param p1, "grpZorder"    # J

    .prologue
    .line 52
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->grpZorder:J

    .line 53
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "sId"    # Ljava/lang/String;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->sId:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public setOrientParam(Ljava/lang/String;)V
    .locals 0
    .param p1, "orientParam"    # Ljava/lang/String;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->orientParam:Ljava/lang/String;

    .line 77
    return-void
.end method

.method public setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V
    .locals 0
    .param p1, "verAlignEle"    # Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->mVertAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    .line 162
    return-void
.end method
