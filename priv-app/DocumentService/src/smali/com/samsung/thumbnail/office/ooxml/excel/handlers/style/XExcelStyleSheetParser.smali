.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleSheetParser;
.super Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;
.source "XExcelStyleSheetParser.java"


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;)V
    .locals 0
    .param p1, "styleobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XExcelStreamParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;)V

    .line 17
    return-void
.end method


# virtual methods
.method public createRootElementHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleSheetHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleSheetHandler;-><init>()V

    return-object v0
.end method

.method public getMainNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .locals 1

    .prologue
    .line 26
    const/16 v0, 0xc8

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleSheetParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v0

    return-object v0
.end method
