.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextRunHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "TextRunHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler$ITxtCharPropConsumer;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextHandler$ITextConsumer;


# instance fields
.field private para:Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

.field private run:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

.field protected text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;)V
    .locals 2
    .param p1, "para"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    .prologue
    .line 28
    const/16 v0, 0x1f

    const-string/jumbo v1, "r"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 29
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextRunHandler;->para:Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    .line 30
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;Ljava/lang/String;)V
    .locals 1
    .param p1, "para"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;
    .param p2, "ele"    # Ljava/lang/String;

    .prologue
    .line 33
    const/16 v0, 0x1f

    invoke-direct {p0, v0, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 34
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextRunHandler;->para:Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    .line 35
    return-void
.end method


# virtual methods
.method public consumeCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V
    .locals 1
    .param p1, "charProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextRunHandler;->run:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextRunHandler;->run:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->setCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V

    .line 78
    :cond_0
    return-void
.end method

.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextRunHandler;->para:Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextRunHandler;->para:Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextRunHandler;->run:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->addRun(Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;)V

    .line 67
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextRunHandler;->run:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextRunHandler;->run:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextRunHandler;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->setText(Ljava/lang/String;)V

    .line 71
    :cond_0
    return-void
.end method

.method protected init()V
    .locals 7

    .prologue
    .line 38
    const/4 v5, 0x2

    new-array v0, v5, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 40
    .local v0, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextHandler;

    invoke-direct {v4, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextHandler$ITextConsumer;)V

    .line 41
    .local v4, "txtHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v5, "t"

    invoke-direct {v1, v5, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 43
    .local v1, "textSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v5, 0x0

    aput-object v1, v0, v5

    .line 45
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;

    const-string/jumbo v5, "rPr"

    const/4 v6, 0x0

    invoke-direct {v2, v5, v6, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler$ITxtCharPropConsumer;)V

    .line 47
    .local v2, "txtCharPropsHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v5, "rPr"

    invoke-direct {v3, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 49
    .local v3, "txtCharPropsSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v5, 0x1

    aput-object v3, v0, v5

    .line 51
    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextRunHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 52
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextRunHandler;->text:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 57
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 58
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextRunHandler;->run:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    .line 59
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextRunHandler;->init()V

    .line 60
    return-void
.end method
