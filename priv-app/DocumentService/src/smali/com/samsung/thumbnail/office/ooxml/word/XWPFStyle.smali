.class public Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
.super Ljava/lang/Object;
.source "XWPFStyle.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;,
        Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;
    }
.end annotation


# instance fields
.field protected basedOn:Ljava/lang/String;

.field protected charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

.field protected isDefault:Z

.field private merged:Z

.field protected paraProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

.field protected parentStyle:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

.field protected styleId:Ljava/lang/String;

.field protected styleType:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

.field private tableProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

.field private tblCellProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

.field private tblRowProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;

.field private tblStyleType:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

.field private tblStyles:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;)V
    .locals 0
    .param p1, "type"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->tblStyleType:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    .line 65
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "styleId"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->styleId:Ljava/lang/String;

    .line 51
    invoke-static {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->styleType:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    .line 53
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->styleType:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->TABLE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    if-ne v0, v1, :cond_0

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->tblStyles:Ljava/util/HashMap;

    .line 56
    :cond_0
    return-void
.end method


# virtual methods
.method public getBasedOn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->basedOn:Ljava/lang/String;

    return-object v0
.end method

.method public getCellProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->tblCellProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    return-object v0
.end method

.method public getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    return-object v0
.end method

.method public getDefault()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->isDefault:Z

    return v0
.end method

.method public getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->paraProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    return-object v0
.end method

.method public getParentStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->parentStyle:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    return-object v0
.end method

.method public getRowProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->tblRowProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;

    return-object v0
.end method

.method public getStyleId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->styleId:Ljava/lang/String;

    return-object v0
.end method

.method public getStyleType()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->styleType:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    return-object v0
.end method

.method public getTblProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->tableProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    return-object v0
.end method

.method public getTblStyleProp(Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    .locals 1
    .param p1, "type"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->tblStyles:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    return-object v0
.end method

.method public getTblStyleType()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->tblStyleType:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    return-object v0
.end method

.method public mergeWithParent(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V
    .locals 4
    .param p1, "defCharProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .param p2, "defParaProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    .prologue
    .line 157
    iget-boolean v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->merged:Z

    if-eqz v2, :cond_0

    .line 201
    :goto_0
    return-void

    .line 162
    :cond_0
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->parentStyle:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    if-nez v2, :cond_3

    .line 165
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    if-nez v2, :cond_1

    .line 166
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-direct {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V

    .line 168
    .local v0, "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .line 172
    .end local v0    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    :goto_1
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->paraProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    if-nez v2, :cond_2

    .line 173
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-direct {v1, p2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V

    .line 175
    .local v1, "paraProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->paraProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    .line 179
    .end local v1    # "paraProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    :goto_2
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->merged:Z

    goto :goto_0

    .line 170
    :cond_1
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->mergeCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V

    goto :goto_1

    .line 177
    :cond_2
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->paraProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v2, p2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->mergeParaProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V

    goto :goto_2

    .line 183
    :cond_3
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    if-nez v2, :cond_4

    .line 184
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;-><init>()V

    .line 185
    .restart local v0    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .line 187
    .end local v0    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    :cond_4
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->paraProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    if-nez v2, :cond_5

    .line 188
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;-><init>()V

    .line 189
    .restart local v1    # "paraProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->paraProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    .line 191
    .end local v1    # "paraProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    :cond_5
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->parentStyle:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    invoke-virtual {v2, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->mergeWithParent(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V

    .line 194
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->parentStyle:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->mergeCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V

    .line 195
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->paraProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->parentStyle:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->paraProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->mergeParaProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V

    goto :goto_0
.end method

.method public setBasedOn(Ljava/lang/String;)V
    .locals 0
    .param p1, "basedOn"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->basedOn:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public setCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V
    .locals 0
    .param p1, "charProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .line 81
    return-void
.end method

.method public setDefault(Z)V
    .locals 0
    .param p1, "isDefault"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->isDefault:Z

    .line 93
    return-void
.end method

.method public setParaProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V
    .locals 0
    .param p1, "paraProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->paraProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    .line 89
    return-void
.end method

.method public setParentStyle(Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;)V
    .locals 0
    .param p1, "parentStyle"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->parentStyle:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    .line 153
    return-void
.end method

.method public setTableStyleProp(Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;)V
    .locals 1
    .param p1, "type"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;
    .param p2, "style"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->tblStyles:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    return-void
.end method

.method public setTblCellProp(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;)V
    .locals 0
    .param p1, "tblCellProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->tblCellProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    .line 133
    return-void
.end method

.method public setTblProp(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;)V
    .locals 0
    .param p1, "tableProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->tableProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    .line 113
    return-void
.end method

.method public setTblRowProp(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;)V
    .locals 0
    .param p1, "tblRowProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->tblRowProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;

    .line 141
    return-void
.end method
