.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/EffectLstHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "EffectLstHandler.java"


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/OuterShadow$IEffectConsumer;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;)V
    .locals 8
    .param p1, "effectConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/OuterShadow$IEffectConsumer;
    .param p2, "gsConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;

    .prologue
    const/4 v7, 0x0

    .line 17
    const/16 v5, 0x1f

    const-string/jumbo v6, "effectLst"

    invoke-direct {p0, v5, v6}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 19
    const/4 v5, 0x2

    new-array v4, v5, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 21
    .local v4, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/OuterShadow;

    invoke-direct {v2, p1, v7, v7}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/OuterShadow;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/OuterShadow$IEffectConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 23
    .local v2, "otShadowHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/OuterShadow;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v5, "outerShdw"

    invoke-direct {v3, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 25
    .local v3, "otShadowSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v5, 0x0

    aput-object v3, v4, v5

    .line 27
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FillOverLayHandler;

    invoke-direct {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FillOverLayHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;)V

    .line 28
    .local v0, "filOverLayHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FillOverLayHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v5, "fillOverlay"

    invoke-direct {v1, v5, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 30
    .local v1, "fillOverLaySeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v5, 0x1

    aput-object v1, v4, v5

    .line 32
    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/EffectLstHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 34
    return-void
.end method


# virtual methods
.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 39
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 40
    return-void
.end method
