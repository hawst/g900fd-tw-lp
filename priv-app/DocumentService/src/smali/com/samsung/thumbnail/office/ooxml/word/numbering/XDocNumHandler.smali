.class public Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XDocNumHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler$ILvlOverride;


# instance fields
.field private num:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    const-string/jumbo v0, "num"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 26
    return-void
.end method


# virtual methods
.method public addLvlOverride(Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;)V
    .locals 1
    .param p1, "lvlOverride"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumHandler;->num:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;->addLvlOverride(Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;)V

    .line 66
    return-void
.end method

.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 60
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumHandler;->num:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;->addNum(Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;)V

    .line 61
    return-void
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 42
    invoke-virtual {p0, p2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 44
    .local v0, "elementName":Ljava/lang/String;
    const-string/jumbo v3, "abstractNumId"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 45
    const-string/jumbo v3, "val"

    invoke-virtual {p0, p3, v3, p1}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    .line 47
    .local v2, "val":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumHandler;->num:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;->setAbstractNumId(I)V

    .line 54
    .end local v2    # "val":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    const-string/jumbo v3, "lvlOverride"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 49
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;

    invoke-direct {v1, p0}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler$ILvlOverride;)V

    .line 51
    .local v1, "lvlOverrideHandler":Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;
    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->pushHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 52
    invoke-virtual {v1, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocLvlOverrideHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    goto :goto_0
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 31
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 32
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumHandler;->num:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;

    .line 34
    const-string/jumbo v1, "numId"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 35
    .local v0, "val":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumHandler;->num:Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;->setNumId(I)V

    .line 36
    return-void
.end method
