.class public Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;
.super Ljava/lang/Object;
.source "ExcelChartDisplay.java"


# instance fields
.field private chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

.field private chartclr:[Ljava/lang/String;

.field private elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

.field private mMaxDataXValue:D

.field private mMaxDataYValue:D

.field private mMinDataXValue:D

.field private mMinDataYValue:D


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 3
    .param p1, "elementsCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "#4169e1"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "#a52a2a"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "#6b8e23"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "#8a2be2"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "#00bfff"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "#cd853f"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "#87cefa"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "#b8860b"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "#bdb76b"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "#9370db"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartclr:[Ljava/lang/String;

    .line 49
    iput-object p1, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .line 50
    return-void
.end method

.method private getAreaChartRenderer(IILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 12
    .param p1, "i"    # I
    .param p2, "numcnt"    # I
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;

    .prologue
    const-wide/16 v10, 0x0

    const/4 v8, 0x1

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    .line 223
    iget-object v4, p3, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartSeriesNames:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 224
    .local v2, "seriescount":I
    new-instance v1, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v1}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 225
    .local v1, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v4, 0x41800000    # 16.0f

    invoke-virtual {v1, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 226
    const/high16 v4, 0x41a00000    # 20.0f

    invoke-virtual {v1, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 227
    const/high16 v4, 0x41200000    # 10.0f

    invoke-virtual {v1, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 228
    const/high16 v4, 0x41700000    # 15.0f

    invoke-virtual {v1, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 229
    const/high16 v4, 0x40a00000    # 5.0f

    invoke-virtual {v1, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setPointSize(F)V

    .line 230
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    cmpg-double v4, v4, v10

    if-gez v4, :cond_0

    .line 231
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    .line 232
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    .line 233
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    .line 235
    :cond_0
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 237
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    cmpg-double v4, v4, v10

    if-gez v4, :cond_1

    .line 238
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    .line 239
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    .line 240
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    .line 242
    :cond_1
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMax(D)V

    .line 244
    const/4 v4, 0x4

    new-array v4, v4, [I

    fill-array-data v4, :array_0

    invoke-virtual {v1, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 246
    const/4 v3, 0x0

    .local v3, "x":I
    :goto_0
    if-ge v3, v2, :cond_2

    .line 247
    new-instance v0, Lorg/achartengine/renderer/XYSeriesRenderer;

    invoke-direct {v0}, Lorg/achartengine/renderer/XYSeriesRenderer;-><init>()V

    .line 248
    .local v0, "r":Lorg/achartengine/renderer/XYSeriesRenderer;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartclr:[Ljava/lang/String;

    sub-int v5, v2, v3

    add-int/lit8 v5, v5, -0x1

    rem-int/lit8 v5, v5, 0xa

    aget-object v4, v4, v5

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v4}, Lorg/achartengine/renderer/XYSeriesRenderer;->setColor(I)V

    .line 249
    invoke-virtual {v0, v8}, Lorg/achartengine/renderer/XYSeriesRenderer;->setFillBelowLine(Z)V

    .line 250
    iget-object v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartclr:[Ljava/lang/String;

    sub-int v5, v2, v3

    add-int/lit8 v5, v5, -0x1

    aget-object v4, v4, v5

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v4}, Lorg/achartengine/renderer/XYSeriesRenderer;->setFillBelowLineColor(I)V

    .line 252
    invoke-virtual {v0, v8}, Lorg/achartengine/renderer/XYSeriesRenderer;->setFillPoints(Z)V

    .line 253
    invoke-virtual {v1, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 246
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 255
    .end local v0    # "r":Lorg/achartengine/renderer/XYSeriesRenderer;
    :cond_2
    const/4 v3, 0x0

    :goto_1
    if-ge v3, p2, :cond_3

    .line 256
    add-int/lit8 v4, v3, 0x1

    int-to-double v6, v4

    iget-object v4, p3, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartCategoryNames:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v1, v6, v7, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    .line 255
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 259
    :cond_3
    const v4, -0xbbbbbc

    invoke-virtual {v1, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxesColor(I)V

    .line 260
    const v4, -0x333334

    invoke-virtual {v1, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsColor(I)V

    .line 261
    invoke-virtual {v1, v8}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setShowGrid(Z)V

    .line 263
    const/4 v4, -0x1

    invoke-virtual {v1, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setBackgroundColor(I)V

    .line 264
    invoke-virtual {v1, v8}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setApplyBackgroundColor(Z)V

    .line 265
    return-object v1

    .line 244
    nop

    :array_0
    .array-data 4
        0x14
        0x1e
        0xf
        0x1e
    .end array-data
.end method

.method private getBarChartRenderer(ILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 12
    .param p1, "numCount"    # I
    .param p2, "drawingobj"    # Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;

    .prologue
    const v11, -0x777778

    const/4 v10, 0x1

    const-wide/16 v8, 0x0

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    .line 524
    new-instance v2, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 525
    .local v2, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v4, 0x41800000    # 16.0f

    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 526
    const/high16 v4, 0x41a00000    # 20.0f

    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 527
    const/high16 v4, 0x40e00000    # 7.0f

    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 528
    const/high16 v4, 0x41700000    # 15.0f

    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 530
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_0

    .line 531
    new-instance v1, Lorg/achartengine/renderer/SimpleSeriesRenderer;

    invoke-direct {v1}, Lorg/achartengine/renderer/SimpleSeriesRenderer;-><init>()V

    .line 532
    .local v1, "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartclr:[Ljava/lang/String;

    rem-int/lit8 v5, v0, 0xa

    aget-object v4, v4, v5

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v1, v4}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setColor(I)V

    .line 533
    invoke-virtual {v2, v1}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 530
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 536
    .end local v1    # "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    :cond_0
    sget-object v4, Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;->VERTICAL:Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;

    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setOrientation(Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;)V

    .line 538
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    cmpg-double v4, v4, v8

    if-gez v4, :cond_1

    .line 539
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    .line 540
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    .line 541
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    .line 543
    :cond_1
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 545
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    cmpg-double v4, v4, v8

    if-gez v4, :cond_2

    .line 546
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    .line 547
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    .line 548
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    .line 551
    :cond_2
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 553
    invoke-virtual {v2, v8, v9}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 554
    add-int/lit8 v4, p1, 0x1

    int-to-double v4, v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMax(D)V

    .line 556
    invoke-virtual {v2, v11}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxesColor(I)V

    .line 557
    const v4, -0x333334

    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsColor(I)V

    .line 558
    const/4 v0, 0x0

    :goto_1
    if-ge v0, p1, :cond_3

    .line 559
    add-int/lit8 v4, v0, 0x1

    int-to-double v6, v4

    iget-object v4, p2, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartCategoryNames:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v2, v6, v7, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    .line 558
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 561
    :cond_3
    invoke-virtual {v2, v11}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setGridColor(I)V

    .line 562
    invoke-virtual {v2, v10}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setShowGridX(Z)V

    .line 563
    const/4 v4, 0x4

    new-array v4, v4, [I

    fill-array-data v4, :array_0

    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 565
    const-wide v4, 0x3fd3333333333333L    # 0.3

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setBarSpacing(D)V

    .line 566
    const/4 v0, 0x0

    :goto_2
    if-ge v0, p1, :cond_4

    .line 567
    invoke-virtual {v2, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getSeriesRendererAt(I)Lorg/achartengine/renderer/SimpleSeriesRenderer;

    move-result-object v3

    .line 569
    .local v3, "seriesRenderer":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    invoke-virtual {v3, v10}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setDisplayChartValues(Z)V

    .line 566
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 572
    .end local v3    # "seriesRenderer":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    :cond_4
    const/4 v4, -0x1

    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setBackgroundColor(I)V

    .line 573
    invoke-virtual {v2, v10}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setApplyBackgroundColor(Z)V

    .line 574
    return-object v2

    .line 563
    nop

    :array_0
    .array-data 4
        0x14
        0x28
        0x32
        0xa
    .end array-data
.end method

.method private getLineChartRenderer(ILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 12
    .param p1, "numcnt"    # I
    .param p2, "drawingobj"    # Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;

    .prologue
    const v11, -0x333334

    const/4 v10, 0x1

    const-wide/16 v8, 0x0

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    .line 318
    new-instance v2, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 319
    .local v2, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v3, 0x41800000    # 16.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 320
    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 321
    const/high16 v3, 0x41200000    # 10.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 322
    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 323
    const/high16 v3, 0x40a00000    # 5.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setPointSize(F)V

    .line 324
    invoke-virtual {v2, v8, v9}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 325
    int-to-double v4, p1

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 327
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_0

    .line 328
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    .line 329
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    .line 330
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    .line 332
    :cond_0
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 334
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_1

    .line 335
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    .line 336
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    .line 337
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    .line 340
    :cond_1
    invoke-virtual {v2, v11}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setGridColor(I)V

    .line 341
    invoke-virtual {v2, v10}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setShowGridX(Z)V

    .line 342
    const/4 v3, 0x4

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 343
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_2

    .line 344
    new-instance v1, Lorg/achartengine/renderer/XYSeriesRenderer;

    invoke-direct {v1}, Lorg/achartengine/renderer/XYSeriesRenderer;-><init>()V

    .line 345
    .local v1, "r":Lorg/achartengine/renderer/XYSeriesRenderer;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartclr:[Ljava/lang/String;

    rem-int/lit8 v4, v0, 0xa

    aget-object v3, v3, v4

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v3}, Lorg/achartengine/renderer/XYSeriesRenderer;->setColor(I)V

    .line 346
    sget-object v3, Lorg/achartengine/chart/PointStyle;->SQUARE:Lorg/achartengine/chart/PointStyle;

    invoke-virtual {v1, v3}, Lorg/achartengine/renderer/XYSeriesRenderer;->setPointStyle(Lorg/achartengine/chart/PointStyle;)V

    .line 347
    invoke-virtual {v1, v10}, Lorg/achartengine/renderer/XYSeriesRenderer;->setFillPoints(Z)V

    .line 348
    invoke-virtual {v2, v1}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 343
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 351
    .end local v1    # "r":Lorg/achartengine/renderer/XYSeriesRenderer;
    :cond_2
    const/4 v0, 0x0

    :goto_1
    if-ge v0, p1, :cond_3

    .line 352
    add-int/lit8 v3, v0, 0x1

    int-to-double v4, v3

    iget-object v3, p2, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartCategoryNames:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v4, v5, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    .line 351
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 356
    :cond_3
    const v3, -0xbbbbbc

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxesColor(I)V

    .line 357
    invoke-virtual {v2, v11}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsColor(I)V

    .line 358
    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setBackgroundColor(I)V

    .line 359
    invoke-virtual {v2, v10}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setApplyBackgroundColor(Z)V

    .line 360
    return-object v2

    .line 342
    nop

    :array_0
    .array-data 4
        0x14
        0x1e
        0xf
        0x1e
    .end array-data
.end method

.method private getPieChartRenderer()Lorg/achartengine/renderer/DefaultRenderer;
    .locals 12

    .prologue
    const/high16 v6, 0x41700000    # 15.0f

    const-wide/16 v10, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 689
    new-instance v5, Lorg/achartengine/renderer/DefaultRenderer;

    invoke-direct {v5}, Lorg/achartengine/renderer/DefaultRenderer;-><init>()V

    .line 690
    .local v5, "renderer":Lorg/achartengine/renderer/DefaultRenderer;
    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setLabelsTextSize(F)V

    .line 691
    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setLegendTextSize(F)V

    .line 692
    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_0

    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setMargins([I)V

    .line 694
    iget-object v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartclr:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 695
    .local v1, "color":Ljava/lang/String;
    new-instance v4, Lorg/achartengine/renderer/SimpleSeriesRenderer;

    invoke-direct {v4}, Lorg/achartengine/renderer/SimpleSeriesRenderer;-><init>()V

    .line 696
    .local v4, "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v6}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setColor(I)V

    .line 697
    invoke-virtual {v5, v4}, Lorg/achartengine/renderer/DefaultRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 694
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 699
    .end local v1    # "color":Ljava/lang/String;
    .end local v4    # "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    :cond_0
    invoke-virtual {v5, v7}, Lorg/achartengine/renderer/DefaultRenderer;->setZoomButtonsVisible(Z)V

    .line 700
    invoke-virtual {v5, v7}, Lorg/achartengine/renderer/DefaultRenderer;->setZoomEnabled(Z)V

    .line 701
    const/high16 v6, 0x41a00000    # 20.0f

    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setChartTitleTextSize(F)V

    .line 702
    invoke-virtual {v5, v8}, Lorg/achartengine/renderer/DefaultRenderer;->setDisplayValues(Z)V

    .line 703
    invoke-virtual {v5, v8}, Lorg/achartengine/renderer/DefaultRenderer;->setShowLabels(Z)V

    .line 704
    invoke-virtual {v5, v8}, Lorg/achartengine/renderer/DefaultRenderer;->getSeriesRendererAt(I)Lorg/achartengine/renderer/SimpleSeriesRenderer;

    move-result-object v4

    .line 705
    .restart local v4    # "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    invoke-virtual {v4, v7}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setGradientEnabled(Z)V

    .line 706
    const v6, -0xffff01

    invoke-virtual {v4, v10, v11, v6}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setGradientStart(DI)V

    .line 707
    const v6, -0xff0100

    invoke-virtual {v4, v10, v11, v6}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setGradientStop(DI)V

    .line 708
    invoke-virtual {v4, v7}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setHighlighted(Z)V

    .line 709
    const/4 v6, -0x1

    invoke-virtual {v5, v6}, Lorg/achartengine/renderer/DefaultRenderer;->setBackgroundColor(I)V

    .line 710
    invoke-virtual {v5, v7}, Lorg/achartengine/renderer/DefaultRenderer;->setApplyBackgroundColor(Z)V

    .line 712
    return-object v5

    .line 692
    nop

    :array_0
    .array-data 4
        0x14
        0x1e
        0xf
        0x3c
    .end array-data
.end method

.method private getScatterChartDatasetForCanvas(ILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)Lorg/achartengine/model/XYMultipleSeriesDataset;
    .locals 16
    .param p1, "i"    # I
    .param p2, "drawingobj"    # Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;

    .prologue
    .line 589
    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataXValue:D

    .line 590
    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataXValue:D

    .line 591
    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    .line 592
    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    .line 594
    new-instance v6, Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-direct {v6}, Lorg/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    .line 595
    .local v6, "dataset":Lorg/achartengine/model/XYMultipleSeriesDataset;
    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartSeriesNames:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 596
    .local v11, "seriescount":I
    const/4 v9, 0x0

    .local v9, "order":I
    :goto_0
    if-ge v9, v11, :cond_6

    .line 598
    const/4 v13, 0x1

    if-le v11, v13, :cond_4

    .line 599
    new-instance v10, Lorg/achartengine/model/XYSeries;

    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartSeriesNames:Ljava/util/ArrayList;

    invoke-virtual {v13, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-direct {v10, v13}, Lorg/achartengine/model/XYSeries;-><init>(Ljava/lang/String;)V

    .line 603
    .local v10, "series":Lorg/achartengine/model/XYSeries;
    :goto_1
    const/4 v7, 0x1

    .line 604
    .local v7, "k":I
    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartCategoryNames:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 605
    .local v12, "total":I
    const/4 v8, 0x0

    .line 606
    .local v8, "num":I
    :goto_2
    if-ge v8, v12, :cond_5

    .line 607
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, ""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 608
    .local v2, "dXVal":D
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataXValue:D

    cmpl-double v13, v2, v14

    if-lez v13, :cond_0

    .line 609
    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataXValue:D

    .line 611
    :cond_0
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataXValue:D

    cmpg-double v13, v2, v14

    if-gez v13, :cond_1

    const-wide/16 v14, 0x0

    cmpg-double v13, v2, v14

    if-gez v13, :cond_1

    .line 612
    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataXValue:D

    .line 615
    :cond_1
    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartValues:Ljava/util/ArrayList;

    invoke-virtual {v13, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Ljava/lang/String;

    aget-object v13, v13, v8

    invoke-static {v13}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 617
    .local v4, "dYVal":D
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    cmpl-double v13, v4, v14

    if-lez v13, :cond_2

    .line 618
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    .line 620
    :cond_2
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    cmpg-double v13, v4, v14

    if-gez v13, :cond_3

    const-wide/16 v14, 0x0

    cmpg-double v13, v4, v14

    if-gez v13, :cond_3

    .line 621
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    .line 624
    :cond_3
    invoke-virtual {v10, v2, v3, v4, v5}, Lorg/achartengine/model/XYSeries;->add(DD)V

    .line 626
    add-int/lit8 v8, v8, 0x1

    .line 627
    add-int/lit8 v7, v7, 0x1

    .line 628
    goto :goto_2

    .line 601
    .end local v2    # "dXVal":D
    .end local v4    # "dYVal":D
    .end local v7    # "k":I
    .end local v8    # "num":I
    .end local v10    # "series":Lorg/achartengine/model/XYSeries;
    .end local v12    # "total":I
    :cond_4
    new-instance v10, Lorg/achartengine/model/XYSeries;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v14, v9, 0x1

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, ""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v10, v13}, Lorg/achartengine/model/XYSeries;-><init>(Ljava/lang/String;)V

    .restart local v10    # "series":Lorg/achartengine/model/XYSeries;
    goto/16 :goto_1

    .line 629
    .restart local v7    # "k":I
    .restart local v8    # "num":I
    .restart local v12    # "total":I
    :cond_5
    invoke-virtual {v6, v10}, Lorg/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lorg/achartengine/model/XYSeries;)V

    .line 596
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 632
    .end local v7    # "k":I
    .end local v8    # "num":I
    .end local v10    # "series":Lorg/achartengine/model/XYSeries;
    .end local v12    # "total":I
    :cond_6
    return-object v6
.end method

.method private getScatterChartRenderer(I)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 11
    .param p1, "numCount"    # I

    .prologue
    const/high16 v4, 0x41700000    # 15.0f

    const/4 v10, 0x1

    const-wide/16 v8, 0x0

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    .line 636
    new-instance v2, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 637
    .local v2, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v3, 0x41800000    # 16.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 638
    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 639
    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 640
    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 641
    const/high16 v3, 0x40a00000    # 5.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setPointSize(F)V

    .line 643
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataXValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_0

    .line 644
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataXValue:D

    .line 645
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataXValue:D

    .line 646
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataXValue:D

    .line 648
    :cond_0
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 650
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataXValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_1

    .line 651
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataXValue:D

    .line 652
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataXValue:D

    .line 653
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataXValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataXValue:D

    .line 655
    :cond_1
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataXValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 657
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_2

    .line 658
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    .line 659
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    .line 660
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    .line 662
    :cond_2
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 664
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    cmpg-double v3, v4, v8

    if-gez v3, :cond_3

    .line 665
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    .line 666
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    .line 667
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    .line 670
    :cond_3
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMax(D)V

    .line 671
    const/4 v3, 0x4

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 673
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_4

    .line 674
    new-instance v1, Lorg/achartengine/renderer/XYSeriesRenderer;

    invoke-direct {v1}, Lorg/achartengine/renderer/XYSeriesRenderer;-><init>()V

    .line 675
    .local v1, "r":Lorg/achartengine/renderer/XYSeriesRenderer;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartclr:[Ljava/lang/String;

    rem-int/lit8 v4, v0, 0xa

    aget-object v3, v3, v4

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v3}, Lorg/achartengine/renderer/XYSeriesRenderer;->setColor(I)V

    .line 676
    invoke-virtual {v1, v10}, Lorg/achartengine/renderer/XYSeriesRenderer;->setFillPoints(Z)V

    .line 677
    invoke-virtual {v2, v1}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 673
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 680
    .end local v1    # "r":Lorg/achartengine/renderer/XYSeriesRenderer;
    :cond_4
    invoke-virtual {v2, v10}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setShowGrid(Z)V

    .line 681
    const v3, -0xbbbbbc

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxesColor(I)V

    .line 682
    const v3, -0x333334

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsColor(I)V

    .line 683
    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setBackgroundColor(I)V

    .line 684
    invoke-virtual {v2, v10}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setApplyBackgroundColor(Z)V

    .line 685
    return-object v2

    .line 671
    nop

    :array_0
    .array-data 4
        0x14
        0x1e
        0xf
        0x1e
    .end array-data
.end method

.method public static declared-synchronized isNum(Ljava/lang/String;)Z
    .locals 3
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 114
    const-class v2, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;

    monitor-enter v2

    :try_start_0
    invoke-static {p0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    const/4 v1, 0x1

    :goto_0
    monitor-exit v2

    return v1

    .line 115
    :catch_0
    move-exception v0

    .line 116
    .local v0, "nfe":Ljava/lang/NumberFormatException;
    const/4 v1, 0x0

    goto :goto_0

    .line 114
    .end local v0    # "nfe":Ljava/lang/NumberFormatException;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method


# virtual methods
.method public areaChartDisplayForCanvas(ILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)V
    .locals 4
    .param p1, "i"    # I
    .param p2, "drawingobj"    # Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;

    .prologue
    .line 158
    iget-object v2, p2, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartCategoryNames:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 159
    .local v1, "numcnt":I
    new-instance v0, Lorg/achartengine/chart/TimeChart;

    invoke-virtual {p0, p1, v1, p2}, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->getAreaChartDatasetForcanvas(IILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v2

    invoke-direct {p0, p1, v1, p2}, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->getAreaChartRenderer(IILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/achartengine/chart/TimeChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;)V

    .line 162
    .local v0, "areaChart":Lorg/achartengine/chart/TimeChart;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 163
    iget-object v2, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setAreaChart(Lorg/achartengine/chart/TimeChart;)V

    .line 164
    iget-object v2, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 165
    iget-object v2, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 168
    return-void
.end method

.method public barchartdisplayForCanvas(IILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)V
    .locals 4
    .param p1, "i"    # I
    .param p2, "j"    # I
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;

    .prologue
    .line 414
    iget-object v2, p3, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartCategoryNames:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 415
    .local v1, "numcnt":I
    new-instance v0, Lorg/achartengine/chart/ColumnChart;

    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->getColChartDatasetForcanvas(ILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v2

    invoke-direct {p0, v1, p3}, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->getBarChartRenderer(ILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/achartengine/chart/ColumnChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;)V

    .line 417
    .local v0, "barChart":Lorg/achartengine/chart/ColumnChart;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 418
    iget-object v2, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setHorizontalBarChart(Lorg/achartengine/chart/ColumnChart;)V

    .line 419
    iget-object v2, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 420
    iget-object v2, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 423
    return-void
.end method

.method public colchartdisplayForCanvas(ILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)V
    .locals 5
    .param p1, "i"    # I
    .param p2, "drawingobj"    # Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;

    .prologue
    .line 427
    iget-object v2, p2, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartCategoryNames:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 428
    .local v1, "numcnt":I
    new-instance v0, Lorg/achartengine/chart/BarChart;

    invoke-virtual {p0, p1, p2}, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->getColChartDatasetForcanvas(ILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v2

    invoke-virtual {p0, v1, p2}, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->getColChartRenderer(ILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v3

    sget-object v4, Lorg/achartengine/chart/BarChart$Type;->DEFAULT:Lorg/achartengine/chart/BarChart$Type;

    invoke-direct {v0, v2, v3, v4}, Lorg/achartengine/chart/BarChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;Lorg/achartengine/chart/BarChart$Type;)V

    .line 431
    .local v0, "barChart":Lorg/achartengine/chart/BarChart;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 432
    iget-object v2, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setColumnBarChart(Lorg/achartengine/chart/AbstractChart;)V

    .line 433
    iget-object v2, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 434
    iget-object v2, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 436
    return-void
.end method

.method public drawchartForCanvas(Ljava/util/ArrayList;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 56
    .local p1, "drawingobj":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 59
    .local v8, "totalchart":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v8, :cond_0

    .line 60
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;

    .line 61
    .local v7, "tempChartInfo":Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;
    invoke-virtual {p0, v7}, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->verifyChart(Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;)V

    .line 59
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 65
    .end local v7    # "tempChartInfo":Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;
    :cond_0
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v8, :cond_b

    .line 66
    new-instance v9, Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-direct {v9}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;-><init>()V

    iput-object v9, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    .line 67
    const-string/jumbo v4, "false"

    .line 69
    .local v4, "lines":Ljava/lang/String;
    const-string/jumbo v1, "false"

    .line 70
    .local v1, "bar":Ljava/lang/String;
    const-string/jumbo v5, "false"

    .line 71
    .local v5, "pie":Ljava/lang/String;
    const-string/jumbo v6, "false"

    .line 72
    .local v6, "scatter":Ljava/lang/String;
    const-string/jumbo v0, "false"

    .line 73
    .local v0, "area":Ljava/lang/String;
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;

    .line 74
    .restart local v7    # "tempChartInfo":Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;
    iget-object v9, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->getX()I

    move-result v10

    invoke-virtual {v9, v10}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setX(I)V

    .line 75
    iget-object v9, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->getY()I

    move-result v10

    invoke-virtual {v9, v10}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setY(I)V

    .line 76
    iget-object v9, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->getHeight()I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartHeight(F)V

    .line 77
    iget-object v9, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->getWidth()I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartWidth(F)V

    .line 78
    iget-object v2, v7, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartType:Ljava/lang/String;

    .line 80
    .local v2, "chartname":Ljava/lang/String;
    const-string/jumbo v9, "Bar"

    invoke-virtual {v2, v9}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_3

    .line 81
    const-string/jumbo v1, "true"

    .line 94
    :cond_1
    :goto_2
    const-string/jumbo v9, "true"

    invoke-virtual {v5, v9}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_7

    .line 95
    invoke-virtual {p0, v3, v7}, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->pieChartDisplayForCanvas(ILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)V

    .line 65
    :cond_2
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 82
    :cond_3
    const-string/jumbo v9, "Pie"

    invoke-virtual {v2, v9}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_4

    .line 83
    const-string/jumbo v5, "true"

    goto :goto_2

    .line 84
    :cond_4
    const-string/jumbo v9, "Area"

    invoke-virtual {v2, v9}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_5

    .line 85
    const-string/jumbo v0, "true"

    goto :goto_2

    .line 86
    :cond_5
    const-string/jumbo v9, "Line"

    invoke-virtual {v2, v9}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_6

    .line 87
    const-string/jumbo v4, "true"

    goto :goto_2

    .line 88
    :cond_6
    const-string/jumbo v9, "Scatter"

    invoke-virtual {v2, v9}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_1

    .line 89
    const-string/jumbo v6, "true"

    goto :goto_2

    .line 97
    :cond_7
    const-string/jumbo v9, "true"

    invoke-virtual {v1, v9}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_8

    .line 99
    invoke-virtual {p0, v3, v7}, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->colchartdisplayForCanvas(ILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)V

    goto :goto_3

    .line 100
    :cond_8
    const-string/jumbo v9, "true"

    invoke-virtual {v4, v9}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_9

    .line 101
    invoke-virtual {p0, v3, v7}, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->lineChartDisplayForCanvas(ILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)V

    goto :goto_3

    .line 102
    :cond_9
    const-string/jumbo v9, "true"

    invoke-virtual {v0, v9}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_a

    .line 103
    invoke-virtual {p0, v3, v7}, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->areaChartDisplayForCanvas(ILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)V

    goto :goto_3

    .line 104
    :cond_a
    const-string/jumbo v9, "true"

    invoke-virtual {v6, v9}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_2

    .line 105
    invoke-virtual {p0, v3, v7}, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->scatterChartDisplayForCanvas(ILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)V

    goto :goto_3

    .line 109
    .end local v0    # "area":Ljava/lang/String;
    .end local v1    # "bar":Ljava/lang/String;
    .end local v2    # "chartname":Ljava/lang/String;
    .end local v4    # "lines":Ljava/lang/String;
    .end local v5    # "pie":Ljava/lang/String;
    .end local v6    # "scatter":Ljava/lang/String;
    .end local v7    # "tempChartInfo":Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;
    :cond_b
    return-void
.end method

.method public getAreaChartDatasetForcanvas(IILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)Lorg/achartengine/model/XYMultipleSeriesDataset;
    .locals 18
    .param p1, "i"    # I
    .param p2, "numcnt"    # I
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;

    .prologue
    .line 173
    new-instance v4, Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-direct {v4}, Lorg/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    .line 174
    .local v4, "dataset":Lorg/achartengine/model/XYMultipleSeriesDataset;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 176
    .local v11, "valueList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Double;>;"
    move-object/from16 v0, p3

    iget-object v13, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartSeriesNames:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 177
    .local v9, "seriescount":I
    new-array v8, v9, [Lorg/achartengine/model/CategorySeries;

    .line 178
    .local v8, "seriesArray":[Lorg/achartengine/model/CategorySeries;
    const/4 v6, 0x0

    .local v6, "order":I
    :goto_0
    if-ge v6, v9, :cond_7

    .line 181
    const/4 v13, 0x1

    if-lt v9, v13, :cond_6

    .line 182
    new-instance v7, Lorg/achartengine/model/CategorySeries;

    move-object/from16 v0, p3

    iget-object v13, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartSeriesNames:Ljava/util/ArrayList;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_3

    move-object/from16 v0, p3

    iget-object v13, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartSeriesNames:Ljava/util/ArrayList;

    invoke-virtual {v13, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    :goto_1
    invoke-direct {v7, v13}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 185
    .local v7, "series":Lorg/achartengine/model/CategorySeries;
    move-object/from16 v0, p3

    iget-object v13, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartCategoryNames:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 186
    .local v10, "total":I
    const/4 v5, 0x0

    .line 187
    .local v5, "num":I
    :goto_2
    if-ge v5, v10, :cond_5

    .line 188
    const-wide/16 v2, 0x0

    .line 189
    .local v2, "addedValue":D
    if-nez v6, :cond_4

    .line 190
    move-object/from16 v0, p3

    iget-object v13, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartValues:Ljava/util/ArrayList;

    invoke-virtual {v13, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Ljava/lang/String;

    aget-object v13, v13, v5

    invoke-static {v13}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 196
    :goto_3
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-le v13, v5, :cond_0

    .line 197
    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 199
    :cond_0
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v13

    invoke-virtual {v11, v5, v13}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 200
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    cmpl-double v13, v2, v14

    if-lez v13, :cond_1

    .line 201
    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    .line 203
    :cond_1
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    cmpg-double v13, v2, v14

    if-gez v13, :cond_2

    const-wide/16 v14, 0x0

    cmpg-double v13, v2, v14

    if-gez v13, :cond_2

    .line 204
    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    .line 206
    :cond_2
    invoke-virtual {v7, v2, v3}, Lorg/achartengine/model/CategorySeries;->add(D)V

    .line 207
    add-int/lit8 v5, v5, 0x1

    .line 208
    goto :goto_2

    .line 182
    .end local v2    # "addedValue":D
    .end local v5    # "num":I
    .end local v7    # "series":Lorg/achartengine/model/CategorySeries;
    .end local v10    # "total":I
    :cond_3
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Series "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    add-int/lit8 v14, v6, 0x1

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto :goto_1

    .line 193
    .restart local v2    # "addedValue":D
    .restart local v5    # "num":I
    .restart local v7    # "series":Lorg/achartengine/model/CategorySeries;
    .restart local v10    # "total":I
    :cond_4
    move-object/from16 v0, p3

    iget-object v13, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartValues:Ljava/util/ArrayList;

    invoke-virtual {v13, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Ljava/lang/String;

    aget-object v13, v13, v5

    invoke-static {v13}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v14

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Double;

    invoke-virtual {v13}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v16

    add-double v2, v14, v16

    goto :goto_3

    .line 209
    .end local v2    # "addedValue":D
    :cond_5
    aput-object v7, v8, v6

    .line 178
    .end local v5    # "num":I
    .end local v7    # "series":Lorg/achartengine/model/CategorySeries;
    .end local v10    # "total":I
    :cond_6
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 214
    :cond_7
    array-length v13, v8

    add-int/lit8 v12, v13, -0x1

    .local v12, "x":I
    :goto_4
    if-ltz v12, :cond_8

    .line 215
    aget-object v13, v8, v12

    invoke-virtual {v13}, Lorg/achartengine/model/CategorySeries;->toXYSeries()Lorg/achartengine/model/XYSeries;

    move-result-object v13

    invoke-virtual {v4, v13}, Lorg/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lorg/achartengine/model/XYSeries;)V

    .line 214
    add-int/lit8 v12, v12, -0x1

    goto :goto_4

    .line 218
    :cond_8
    return-object v4
.end method

.method public getColChartDatasetForcanvas(ILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)Lorg/achartengine/model/XYMultipleSeriesDataset;
    .locals 12
    .param p1, "i"    # I
    .param p2, "drawingobj"    # Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;

    .prologue
    .line 486
    new-instance v2, Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-direct {v2}, Lorg/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    .line 487
    .local v2, "dataset":Lorg/achartengine/model/XYMultipleSeriesDataset;
    iget-object v9, p2, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartSeriesNames:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 488
    .local v7, "seriescount":I
    const/4 v5, 0x0

    .local v5, "order":I
    :goto_0
    if-ge v5, v7, :cond_4

    .line 489
    const-wide/16 v10, 0x0

    iput-wide v10, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    .line 490
    const-wide/16 v10, 0x0

    iput-wide v10, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    .line 492
    new-instance v6, Lorg/achartengine/model/CategorySeries;

    iget-object v9, p2, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartSeriesNames:Ljava/util/ArrayList;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_2

    iget-object v9, p2, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartSeriesNames:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    :goto_1
    invoke-direct {v6, v9}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 495
    .local v6, "series":Lorg/achartengine/model/CategorySeries;
    iget-object v9, p2, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartCategoryNames:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 496
    .local v8, "total":I
    const/4 v4, 0x0

    .line 497
    .local v4, "num":I
    :goto_2
    if-ge v4, v8, :cond_3

    .line 499
    :try_start_0
    iget-object v9, p2, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartValues:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/String;

    aget-object v9, v9, v4

    invoke-static {v9}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 501
    .local v0, "dVal":D
    iget-wide v10, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    cmpl-double v9, v0, v10

    if-lez v9, :cond_0

    .line 502
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    .line 504
    :cond_0
    iget-wide v10, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    cmpg-double v9, v0, v10

    if-gez v9, :cond_1

    const-wide/16 v10, 0x0

    cmpg-double v9, v0, v10

    if-gez v9, :cond_1

    .line 505
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    .line 507
    :cond_1
    invoke-virtual {v6, v0, v1}, Lorg/achartengine/model/CategorySeries;->add(D)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 512
    .end local v0    # "dVal":D
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 492
    .end local v4    # "num":I
    .end local v6    # "series":Lorg/achartengine/model/CategorySeries;
    .end local v8    # "total":I
    :cond_2
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Series "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    add-int/lit8 v10, v5, 0x1

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto :goto_1

    .line 508
    .restart local v4    # "num":I
    .restart local v6    # "series":Lorg/achartengine/model/CategorySeries;
    .restart local v8    # "total":I
    :catch_0
    move-exception v3

    .line 509
    .local v3, "ex":Ljava/lang/NumberFormatException;
    const-string/jumbo v9, "DocumentService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Exception: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    const-wide/16 v10, 0x0

    invoke-virtual {v6, v10, v11}, Lorg/achartengine/model/CategorySeries;->add(D)V

    goto :goto_3

    .line 516
    .end local v3    # "ex":Ljava/lang/NumberFormatException;
    :cond_3
    invoke-virtual {v6}, Lorg/achartengine/model/CategorySeries;->toXYSeries()Lorg/achartengine/model/XYSeries;

    move-result-object v9

    invoke-virtual {v2, v9}, Lorg/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lorg/achartengine/model/XYSeries;)V

    .line 488
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 519
    .end local v4    # "num":I
    .end local v6    # "series":Lorg/achartengine/model/CategorySeries;
    .end local v8    # "total":I
    :cond_4
    return-object v2
.end method

.method public getColChartRenderer(ILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 11
    .param p1, "numCount"    # I
    .param p2, "drawingobj"    # Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;

    .prologue
    const/4 v10, 0x1

    const/high16 v4, 0x41700000    # 15.0f

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    const-wide/16 v6, 0x0

    .line 440
    new-instance v2, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-direct {v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;-><init>()V

    .line 441
    .local v2, "renderer":Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    const/high16 v3, 0x41800000    # 16.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setAxisTitleTextSize(F)V

    .line 442
    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setChartTitleTextSize(F)V

    .line 443
    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLabelsTextSize(F)V

    .line 444
    invoke-virtual {v2, v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setLegendTextSize(F)V

    .line 445
    const/4 v3, 0x4

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setMargins([I)V

    .line 447
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_0

    .line 448
    new-instance v1, Lorg/achartengine/renderer/SimpleSeriesRenderer;

    invoke-direct {v1}, Lorg/achartengine/renderer/SimpleSeriesRenderer;-><init>()V

    .line 449
    .local v1, "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartclr:[Ljava/lang/String;

    rem-int/lit8 v4, v0, 0xa

    aget-object v3, v3, v4

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v3}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->setColor(I)V

    .line 450
    invoke-virtual {v2, v1}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addSeriesRenderer(Lorg/achartengine/renderer/SimpleSeriesRenderer;)V

    .line 447
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 453
    .end local v1    # "r":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    :cond_0
    invoke-virtual {v2, v6, v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 454
    add-int/lit8 v3, p1, 0x1

    int-to-double v4, v3

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 456
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    cmpg-double v3, v4, v6

    if-gez v3, :cond_1

    .line 457
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    mul-double/2addr v4, v8

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    .line 458
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    .line 459
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    mul-double/2addr v4, v8

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    .line 461
    :cond_1
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 463
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    cmpg-double v3, v4, v6

    if-gez v3, :cond_2

    .line 464
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    mul-double/2addr v4, v8

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    .line 465
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    .line 466
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    mul-double/2addr v4, v8

    iput-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    .line 469
    :cond_2
    iget-wide v4, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setYAxisMax(D)V

    .line 470
    const v3, -0x333334

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setGridColor(I)V

    .line 471
    invoke-virtual {v2, v10}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setShowGridX(Z)V

    .line 472
    const-wide v4, 0x3fd3333333333333L    # 0.3

    invoke-virtual {v2, v4, v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setBarSpacing(D)V

    .line 474
    const/4 v0, 0x0

    :goto_1
    if-ge v0, p1, :cond_3

    .line 475
    add-int/lit8 v3, v0, 0x1

    int-to-double v4, v3

    iget-object v3, p2, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartCategoryNames:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v4, v5, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    .line 474
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 477
    :cond_3
    iput-wide v6, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    .line 478
    iput-wide v6, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    .line 479
    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setBackgroundColor(I)V

    .line 480
    invoke-virtual {v2, v10}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->setApplyBackgroundColor(Z)V

    .line 481
    return-object v2

    .line 445
    :array_0
    .array-data 4
        0x1e
        0x1e
        0xf
        0x1e
    .end array-data
.end method

.method public getLineChartDatasetForcanvas(ILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)Lorg/achartengine/model/XYMultipleSeriesDataset;
    .locals 12
    .param p1, "i"    # I
    .param p2, "drawingobj"    # Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;

    .prologue
    .line 282
    new-instance v2, Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-direct {v2}, Lorg/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    .line 283
    .local v2, "dataset":Lorg/achartengine/model/XYMultipleSeriesDataset;
    iget-object v9, p2, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartSeriesNames:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 284
    .local v7, "seriescount":I
    const/4 v5, 0x0

    .local v5, "order":I
    :goto_0
    if-ge v5, v7, :cond_5

    .line 286
    const/4 v9, 0x1

    if-lt v7, v9, :cond_4

    .line 287
    new-instance v6, Lorg/achartengine/model/CategorySeries;

    iget-object v9, p2, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartSeriesNames:Ljava/util/ArrayList;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_2

    iget-object v9, p2, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartSeriesNames:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    :goto_1
    invoke-direct {v6, v9}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 290
    .local v6, "series":Lorg/achartengine/model/CategorySeries;
    iget-object v9, p2, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartCategoryNames:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 291
    .local v8, "total":I
    const/4 v4, 0x0

    .line 292
    .local v4, "num":I
    :goto_2
    if-ge v4, v8, :cond_3

    .line 294
    :try_start_0
    iget-object v9, p2, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartValues:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/String;

    aget-object v9, v9, v4

    invoke-static {v9}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 296
    .local v0, "dVal":D
    iget-wide v10, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    cmpl-double v9, v0, v10

    if-lez v9, :cond_0

    .line 297
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMaxDataYValue:D

    .line 299
    :cond_0
    iget-wide v10, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    cmpg-double v9, v0, v10

    if-gez v9, :cond_1

    const-wide/16 v10, 0x0

    cmpg-double v9, v0, v10

    if-gez v9, :cond_1

    .line 300
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->mMinDataYValue:D

    .line 302
    :cond_1
    invoke-virtual {v6, v0, v1}, Lorg/achartengine/model/CategorySeries;->add(D)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 307
    .end local v0    # "dVal":D
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 287
    .end local v4    # "num":I
    .end local v6    # "series":Lorg/achartengine/model/CategorySeries;
    .end local v8    # "total":I
    :cond_2
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Series "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    add-int/lit8 v10, v5, 0x1

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto :goto_1

    .line 303
    .restart local v4    # "num":I
    .restart local v6    # "series":Lorg/achartengine/model/CategorySeries;
    .restart local v8    # "total":I
    :catch_0
    move-exception v3

    .line 304
    .local v3, "ex":Ljava/lang/NumberFormatException;
    const-string/jumbo v9, "DocumentService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Exception: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    const-wide/16 v10, 0x0

    invoke-virtual {v6, v10, v11}, Lorg/achartengine/model/CategorySeries;->add(D)V

    goto :goto_3

    .line 309
    .end local v3    # "ex":Ljava/lang/NumberFormatException;
    :cond_3
    invoke-virtual {v6}, Lorg/achartengine/model/CategorySeries;->toXYSeries()Lorg/achartengine/model/XYSeries;

    move-result-object v9

    invoke-virtual {v2, v9}, Lorg/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lorg/achartengine/model/XYSeries;)V

    .line 284
    .end local v4    # "num":I
    .end local v6    # "series":Lorg/achartengine/model/CategorySeries;
    .end local v8    # "total":I
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 312
    :cond_5
    return-object v2
.end method

.method public getPieChartdatasetForCanvas(Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;I)Lorg/achartengine/model/CategorySeries;
    .locals 8
    .param p1, "drawingobj"    # Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;
    .param p2, "i"    # I

    .prologue
    .line 717
    iget-object v4, p1, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartCategoryNames:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 718
    .local v2, "totalcnt":I
    new-instance v1, Lorg/achartengine/model/CategorySeries;

    const-string/jumbo v4, ""

    invoke-direct {v1, v4}, Lorg/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 719
    .local v1, "series":Lorg/achartengine/model/CategorySeries;
    const/4 v3, 0x0

    .line 720
    .local v3, "x":I
    :goto_0
    if-ge v3, v2, :cond_0

    .line 723
    :try_start_0
    iget-object v4, p1, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartCategoryNames:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v5, p1, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartValues:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    aget-object v5, v5, v3

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-virtual {v1, v4, v6, v7}, Lorg/achartengine/model/CategorySeries;->add(Ljava/lang/String;D)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 729
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 725
    :catch_0
    move-exception v0

    .line 726
    .local v0, "ex":Ljava/lang/NumberFormatException;
    const-string/jumbo v4, "DocumentService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 727
    const-wide/16 v4, 0x0

    invoke-virtual {v1, v4, v5}, Lorg/achartengine/model/CategorySeries;->add(D)V

    goto :goto_1

    .line 731
    .end local v0    # "ex":Ljava/lang/NumberFormatException;
    :cond_0
    return-object v1
.end method

.method public lineChartDisplayForCanvas(ILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)V
    .locals 4
    .param p1, "i"    # I
    .param p2, "drawingobj"    # Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;

    .prologue
    .line 269
    iget-object v2, p2, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartCategoryNames:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 270
    .local v1, "numcnt":I
    new-instance v0, Lorg/achartengine/chart/LineChart;

    invoke-virtual {p0, p1, p2}, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->getLineChartDatasetForcanvas(ILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v2

    invoke-direct {p0, v1, p2}, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->getLineChartRenderer(ILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/achartengine/chart/LineChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;)V

    .line 272
    .local v0, "lineChart":Lorg/achartengine/chart/XYChart;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 273
    iget-object v2, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setLineChart(Lorg/achartengine/chart/XYChart;)V

    .line 274
    iget-object v2, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 275
    iget-object v2, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 278
    return-void
.end method

.method public pieChartDisplayForCanvas(ILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)V
    .locals 3
    .param p1, "i"    # I
    .param p2, "drawingobj"    # Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;

    .prologue
    .line 579
    new-instance v0, Lorg/achartengine/chart/PieChart;

    invoke-virtual {p0, p2, p1}, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->getPieChartdatasetForCanvas(Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;I)Lorg/achartengine/model/CategorySeries;

    move-result-object v1

    invoke-direct {p0}, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->getPieChartRenderer()Lorg/achartengine/renderer/DefaultRenderer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/achartengine/chart/PieChart;-><init>(Lorg/achartengine/model/CategorySeries;Lorg/achartengine/renderer/DefaultRenderer;)V

    .line 581
    .local v0, "pieChart":Lorg/achartengine/chart/PieChart;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 582
    iget-object v1, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setPieChart(Lorg/achartengine/chart/PieChart;)V

    .line 583
    iget-object v1, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    sget-object v2, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 584
    iget-object v1, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 585
    return-void
.end method

.method public scatterChartDisplayForCanvas(ILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)V
    .locals 4
    .param p1, "i"    # I
    .param p2, "drawingobj"    # Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;

    .prologue
    .line 145
    iget-object v2, p2, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartCategoryNames:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 146
    .local v0, "numcnt":I
    new-instance v1, Lorg/achartengine/chart/ScatterChart;

    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->getScatterChartDatasetForCanvas(ILcom/samsung/thumbnail/office/excel/ExcelChartInfo;)Lorg/achartengine/model/XYMultipleSeriesDataset;

    move-result-object v2

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->getScatterChartRenderer(I)Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/achartengine/chart/ScatterChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;)V

    .line 150
    .local v1, "scatterChart":Lorg/achartengine/chart/XYChart;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setChartType(I)V

    .line 151
    iget-object v2, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v2, v1}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setScatterChart(Lorg/achartengine/chart/XYChart;)V

    .line 152
    iget-object v2, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 153
    iget-object v2, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->chartContents:Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 155
    return-void
.end method

.method verifyChart(Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;)V
    .locals 8
    .param p1, "ChartInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;

    .prologue
    .line 122
    iget-object v0, p1, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartCategoryNames:Ljava/util/ArrayList;

    .line 123
    .local v0, "categoryNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v3, p1, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartSeriesNames:Ljava/util/ArrayList;

    .line 124
    .local v3, "seriesNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 125
    .local v6, "totalseriescount":I
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 126
    .local v5, "totalcategoryCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v6, :cond_4

    .line 127
    iget-object v7, p1, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartValues:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    .line 129
    .local v4, "tempSeries":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    if-ge v2, v5, :cond_3

    .line 130
    aget-object v7, v4, v2

    if-eqz v7, :cond_0

    aget-object v7, v4, v2

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_2

    .line 131
    :cond_0
    const-string/jumbo v7, "0"

    aput-object v7, v4, v2

    .line 129
    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 133
    :cond_2
    aget-object v7, v4, v2

    invoke-static {v7}, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->isNum(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 134
    const-string/jumbo v7, "0"

    aput-object v7, v4, v2

    goto :goto_2

    .line 126
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 141
    .end local v2    # "j":I
    .end local v4    # "tempSeries":[Ljava/lang/String;
    :cond_4
    return-void
.end method
