.class public Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;
.super Ljava/lang/Object;
.source "EMR_PolybezierTo16.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmfDrawInterface;


# instance fields
.field private Size:I

.field private Type:I

.field private aPoints:[B

.field private arrayOfPoint:[Landroid/graphics/Point;

.field private bottom:I

.field private count:I

.field private emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

.field fileContent:[B

.field private left:I

.field private localPath:Landroid/graphics/Path;

.field private offset:I

.field private pointsCount:I

.field private right:I

.field private top:I


# direct methods
.method public constructor <init>([BILcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V
    .locals 1
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I
    .param p3, "emrGlobals"    # Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .prologue
    const/4 v0, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->localPath:Landroid/graphics/Path;

    .line 59
    iput-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->arrayOfPoint:[Landroid/graphics/Point;

    .line 76
    iput-object p3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .line 77
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->fileContent:[B

    .line 78
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    .line 79
    return-void
.end method


# virtual methods
.method public getBottom()I
    .locals 1

    .prologue
    .line 201
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->bottom:I

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->count:I

    return v0
.end method

.method public getLeft()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->left:I

    return v0
.end method

.method public getPath()Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->localPath:Landroid/graphics/Path;

    return-object v0
.end method

.method public getRecordSize()I
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->Size:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->Type:I

    return v0
.end method

.method public getRight()I
    .locals 1

    .prologue
    .line 182
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->right:I

    return v0
.end method

.method public getTop()I
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->top:I

    return v0
.end method

.method public getaPoints()[B
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->aPoints:[B

    return-object v0
.end method

.method public prepare()V
    .locals 25

    .prologue
    .line 313
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->getCount()I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->setNumberOfPoints(I)V

    .line 314
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->getaPoints()[B

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->setPoints([B)V

    .line 315
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->pointsCount:I

    move/from16 v17, v0

    .line 317
    .local v17, "i":I
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->localPath:Landroid/graphics/Path;

    .line 319
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->arrayOfPoint:[Landroid/graphics/Point;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->arrayOfPoint:[Landroid/graphics/Point;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 320
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v9, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->currentX:F

    .line 321
    .local v9, "f1":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v10, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->currentY:F

    .line 323
    .local v10, "f2":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->localPath:Landroid/graphics/Path;

    invoke-virtual {v2, v9, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 325
    const/16 v18, 0x0

    .line 326
    .local v18, "j":I
    :goto_0
    move/from16 v0, v18

    move/from16 v1, v17

    if-ge v0, v1, :cond_0

    add-int/lit8 v2, v18, 0x2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->arrayOfPoint:[Landroid/graphics/Point;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 327
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v21, v2, v18

    .line 328
    .local v21, "localPoint2":Landroid/graphics/Point;
    add-int/lit8 v19, v18, 0x1

    .line 329
    .local v19, "k":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v22, v2, v19

    .line 330
    .local v22, "localPoint3":Landroid/graphics/Point;
    add-int/lit8 v20, v18, 0x2

    .line 331
    .local v20, "l":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v23, v2, v20

    .line 332
    .local v23, "localPoint4":Landroid/graphics/Point;
    move-object/from16 v0, v21

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v11, v2

    .line 333
    .local v11, "f3":F
    move-object/from16 v0, v21

    iget v2, v0, Landroid/graphics/Point;->y:I

    int-to-float v12, v2

    .line 334
    .local v12, "f4":F
    move-object/from16 v0, v22

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v13, v2

    .line 335
    .local v13, "f5":F
    move-object/from16 v0, v22

    iget v2, v0, Landroid/graphics/Point;->y:I

    int-to-float v14, v2

    .line 336
    .local v14, "f6":F
    move-object/from16 v0, v23

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v15, v2

    .line 337
    .local v15, "f7":F
    move-object/from16 v0, v23

    iget v2, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v2

    move/from16 v16, v0

    .line 339
    .local v16, "f8":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->localPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v3, v11, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v4, v12, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v5, v13, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v6, v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v6, v14, v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v7, v15, v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v8, v16, v8

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 344
    add-int/lit8 v18, v18, 0x3

    .line 345
    goto :goto_0

    .line 347
    .end local v11    # "f3":F
    .end local v12    # "f4":F
    .end local v13    # "f5":F
    .end local v14    # "f6":F
    .end local v15    # "f7":F
    .end local v16    # "f8":F
    .end local v19    # "k":I
    .end local v20    # "l":I
    .end local v21    # "localPoint2":Landroid/graphics/Point;
    .end local v22    # "localPoint3":Landroid/graphics/Point;
    .end local v23    # "localPoint4":Landroid/graphics/Point;
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v2, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    if-eqz v2, :cond_2

    .line 348
    const/16 v24, 0x0

    .line 350
    .local v24, "m":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v2, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->pathStart:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 351
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v2, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    invoke-virtual {v2, v9, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 352
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v3, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->pathStart:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->pathStart:I

    .line 355
    :cond_1
    :goto_1
    move/from16 v0, v24

    move/from16 v1, v17

    if-ge v0, v1, :cond_2

    add-int/lit8 v2, v24, 0x2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->arrayOfPoint:[Landroid/graphics/Point;

    array-length v3, v3

    if-ge v2, v3, :cond_2

    .line 356
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v21, v2, v24

    .line 357
    .restart local v21    # "localPoint2":Landroid/graphics/Point;
    add-int/lit8 v19, v24, 0x1

    .line 358
    .restart local v19    # "k":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v22, v2, v19

    .line 359
    .restart local v22    # "localPoint3":Landroid/graphics/Point;
    add-int/lit8 v20, v24, 0x2

    .line 360
    .restart local v20    # "l":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->arrayOfPoint:[Landroid/graphics/Point;

    aget-object v23, v2, v20

    .line 361
    .restart local v23    # "localPoint4":Landroid/graphics/Point;
    move-object/from16 v0, v21

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v11, v2

    .line 362
    .restart local v11    # "f3":F
    move-object/from16 v0, v21

    iget v2, v0, Landroid/graphics/Point;->y:I

    int-to-float v12, v2

    .line 363
    .restart local v12    # "f4":F
    move-object/from16 v0, v22

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v13, v2

    .line 364
    .restart local v13    # "f5":F
    move-object/from16 v0, v22

    iget v2, v0, Landroid/graphics/Point;->y:I

    int-to-float v14, v2

    .line 365
    .restart local v14    # "f6":F
    move-object/from16 v0, v23

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v15, v2

    .line 366
    .restart local v15    # "f7":F
    move-object/from16 v0, v23

    iget v2, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v2

    move/from16 v16, v0

    .line 368
    .restart local v16    # "f8":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v2, v2, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v3, v11, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v4, v12, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v5, v5, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v5, v13, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v6, v6, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v6, v14, v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v7, v7, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v7, v15, v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v8, v8, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v8, v16, v8

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 373
    add-int/lit8 v24, v24, 0x3

    .line 374
    goto :goto_1

    .line 376
    .end local v11    # "f3":F
    .end local v12    # "f4":F
    .end local v13    # "f5":F
    .end local v14    # "f6":F
    .end local v15    # "f7":F
    .end local v16    # "f8":F
    .end local v19    # "k":I
    .end local v20    # "l":I
    .end local v21    # "localPoint2":Landroid/graphics/Point;
    .end local v22    # "localPoint3":Landroid/graphics/Point;
    .end local v23    # "localPoint4":Landroid/graphics/Point;
    .end local v24    # "m":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->arrayOfPoint:[Landroid/graphics/Point;

    add-int/lit8 v3, v17, -0x1

    aget-object v2, v2, v3

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->arrayOfPoint:[Landroid/graphics/Point;

    add-int/lit8 v4, v17, -0x1

    aget-object v3, v3, v4

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->setCurrentXY(FF)V

    .line 379
    .end local v9    # "f1":F
    .end local v10    # "f2":F
    .end local v18    # "j":I
    :cond_3
    return-void
.end method

.method public readPolybezierTo16()V
    .locals 14

    .prologue
    .line 237
    const/4 v5, 0x0

    .line 239
    .local v5, "i":I
    const/4 v11, 0x4

    new-array v6, v11, [B

    .line 241
    .local v6, "intConvert":[B
    const/4 v5, 0x0

    :goto_0
    const/4 v11, 0x3

    if-gt v5, v11, :cond_0

    .line 242
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 241
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 243
    :cond_0
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 244
    .local v1, "Type":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    .line 245
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->setRecordType(I)V

    .line 247
    const/4 v5, 0x0

    :goto_1
    const/4 v11, 0x3

    if-gt v5, v11, :cond_1

    .line 248
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 247
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 249
    :cond_1
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 250
    .local v0, "Size":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    .line 251
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->setRecordSize(I)V

    .line 253
    const/4 v5, 0x0

    :goto_2
    const/4 v11, 0x3

    if-gt v5, v11, :cond_2

    .line 254
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 253
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 255
    :cond_2
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v7

    .line 256
    .local v7, "lef":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    .line 257
    invoke-virtual {p0, v7}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->setLeft(I)V

    .line 259
    const/4 v5, 0x0

    :goto_3
    const/4 v11, 0x3

    if-gt v5, v11, :cond_3

    .line 260
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 259
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 261
    :cond_3
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v10

    .line 262
    .local v10, "top":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    .line 263
    invoke-virtual {p0, v10}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->setTop(I)V

    .line 265
    const/4 v5, 0x0

    :goto_4
    const/4 v11, 0x3

    if-gt v5, v11, :cond_4

    .line 266
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 265
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 267
    :cond_4
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v9

    .line 268
    .local v9, "right":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    .line 269
    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->setRight(I)V

    .line 271
    const/4 v5, 0x0

    :goto_5
    const/4 v11, 0x3

    if-gt v5, v11, :cond_5

    .line 272
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 271
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 273
    :cond_5
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v2

    .line 274
    .local v2, "bot":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    .line 275
    invoke-virtual {p0, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->setBottom(I)V

    .line 277
    const/4 v5, 0x0

    :goto_6
    const/4 v11, 0x3

    if-gt v5, v11, :cond_6

    .line 278
    rsub-int/lit8 v11, v5, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    add-int/2addr v13, v5

    aget-byte v12, v12, v13

    aput-byte v12, v6, v11

    .line 277
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    .line 279
    :cond_6
    const/4 v11, 0x0

    invoke-static {v6, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v4

    .line 280
    .local v4, "count":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    .line 281
    invoke-virtual {p0, v4}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->setCount(I)V

    .line 283
    add-int/lit8 v3, v0, -0x1c

    .line 284
    .local v3, "buffsize":I
    new-array v8, v3, [B

    .line 285
    .local v8, "pointsBuffer":[B
    const/4 v5, 0x0

    :goto_7
    if-ge v5, v3, :cond_7

    .line 286
    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->fileContent:[B

    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    add-int/2addr v12, v5

    aget-byte v11, v11, v12

    aput-byte v11, v8, v5

    .line 285
    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    .line 288
    :cond_7
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    add-int/2addr v11, v5

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->offset:I

    .line 289
    invoke-virtual {p0, v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->setaPoints([B)V

    .line 290
    return-void
.end method

.method public render(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 390
    const/4 v2, 0x0

    .line 391
    .local v2, "i":I
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;-><init>()V

    .line 392
    .local v0, "PaintPolyBezier16PropertiesObject":Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 394
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 395
    .local v1, "emrSize":I
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->beginEndPath:Landroid/graphics/Path;

    if-nez v3, :cond_3

    .line 396
    if-lez v1, :cond_1

    .line 397
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 398
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedHandlePen:I

    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;->gethandleNumber()I

    move-result v3

    if-ne v4, v3, :cond_4

    .line 402
    :cond_0
    if-ge v2, v1, :cond_1

    .line 403
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getPenStyle()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/thumbnail/office/emf/Util/toStyleAndroid;->getStyle(I)Landroid/graphics/Paint$Style;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 407
    iget-object v3, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v3, v3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->EmrObjectArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreatePen;->getColor()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/thumbnail/office/emf/Util/toColorAndroid;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->setColor(I)V

    .line 411
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->getPath()Landroid/graphics/Path;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 416
    :cond_1
    if-eq v2, v1, :cond_2

    if-nez v1, :cond_3

    .line 418
    :cond_2
    const/16 v3, 0x8

    iget-object v4, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v4, v4, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->selectedStockObject:I

    if-ne v3, v4, :cond_5

    .line 427
    :cond_3
    :goto_1
    return-void

    .line 397
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 421
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->getPath()Landroid/graphics/Path;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/PaintProperties;->getMyPaintBrush()Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_1
.end method

.method public setBottom(I)V
    .locals 0
    .param p1, "b"    # I

    .prologue
    .line 211
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->bottom:I

    .line 212
    return-void
.end method

.method public setCount(I)V
    .locals 0
    .param p1, "cnt"    # I

    .prologue
    .line 135
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->count:I

    .line 136
    return-void
.end method

.method public setCurrentXY(FF)V
    .locals 2
    .param p1, "currX"    # F
    .param p2, "currY"    # F

    .prologue
    .line 301
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v1, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v1, p1, v1

    iput v1, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->currentX:F

    .line 302
    iget-object v0, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->emrGlobals:Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    iget v1, v1, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v1, p2, v1

    iput v1, v0, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->currentY:F

    .line 303
    return-void
.end method

.method public setLeft(I)V
    .locals 0
    .param p1, "l"    # I

    .prologue
    .line 154
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->left:I

    .line 155
    return-void
.end method

.method public setNumberOfPoints(I)V
    .locals 0
    .param p1, "pointCount"    # I

    .prologue
    .line 487
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->pointsCount:I

    .line 488
    return-void
.end method

.method public setPoints([B)V
    .locals 13
    .param p1, "points"    # [B

    .prologue
    const v12, 0xffff

    const/4 v11, 0x1

    const/4 v10, 0x0

    const v9, 0x8000

    .line 436
    const/4 v2, 0x0

    .line 437
    .local v2, "i":I
    const/4 v4, 0x0

    .line 438
    .local v4, "j":I
    const/4 v5, 0x0

    .line 439
    .local v5, "k":I
    const/4 v6, 0x0

    .line 441
    .local v6, "offset":I
    array-length v7, p1

    shr-int/lit8 v7, v7, 0x2

    new-array v7, v7, [Landroid/graphics/Point;

    iput-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->arrayOfPoint:[Landroid/graphics/Point;

    .line 443
    const/4 v7, 0x2

    new-array v3, v7, [B

    .line 445
    .local v3, "intConvert":[B
    :goto_0
    array-length v7, p1

    if-ge v6, v7, :cond_4

    .line 446
    const/4 v2, 0x0

    :goto_1
    if-gt v2, v11, :cond_0

    .line 447
    rsub-int/lit8 v7, v2, 0x1

    add-int v8, v6, v2

    aget-byte v8, p1, v8

    aput-byte v8, v3, v7

    .line 446
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 448
    :cond_0
    invoke-static {v3, v10}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getSmallInt([BI)I

    move-result v0

    .line 449
    .local v0, "X":I
    add-int/2addr v6, v2

    .line 451
    and-int v7, v0, v9

    if-ne v7, v9, :cond_1

    .line 452
    xor-int/lit8 v7, v0, -0x1

    and-int/2addr v7, v12

    add-int/lit8 v7, v7, 0x1

    mul-int/lit8 v0, v7, -0x1

    .line 455
    :cond_1
    const/4 v4, 0x0

    :goto_2
    if-gt v4, v11, :cond_2

    .line 456
    rsub-int/lit8 v7, v4, 0x1

    add-int v8, v6, v4

    aget-byte v8, p1, v8

    aput-byte v8, v3, v7

    .line 455
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 457
    :cond_2
    invoke-static {v3, v10}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getSmallInt([BI)I

    move-result v1

    .line 459
    .local v1, "Y":I
    and-int v7, v1, v9

    if-ne v7, v9, :cond_3

    .line 460
    xor-int/lit8 v7, v1, -0x1

    and-int/2addr v7, v12

    add-int/lit8 v7, v7, 0x1

    mul-int/lit8 v1, v7, -0x1

    .line 463
    :cond_3
    add-int/2addr v6, v4

    .line 464
    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->arrayOfPoint:[Landroid/graphics/Point;

    new-instance v8, Landroid/graphics/Point;

    invoke-direct {v8, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    aput-object v8, v7, v5

    .line 466
    add-int/lit8 v5, v5, 0x1

    .line 467
    goto :goto_0

    .line 469
    .end local v0    # "X":I
    .end local v1    # "Y":I
    :cond_4
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "RecrdSize"    # I

    .prologue
    .line 116
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->Size:I

    .line 117
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "RecrdType"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->Type:I

    .line 98
    return-void
.end method

.method public setRight(I)V
    .locals 0
    .param p1, "r"    # I

    .prologue
    .line 192
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->right:I

    .line 193
    return-void
.end method

.method public setTop(I)V
    .locals 0
    .param p1, "t"    # I

    .prologue
    .line 173
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->top:I

    .line 174
    return-void
.end method

.method public setaPoints([B)V
    .locals 0
    .param p1, "pt"    # [B

    .prologue
    .line 221
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/EMR_PolybezierTo16;->aPoints:[B

    .line 222
    return-void
.end method
