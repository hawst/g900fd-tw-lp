.class public Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStylesHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XDocStylesHandler.java"


# instance fields
.field protected styles:Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;)V
    .locals 4
    .param p1, "styles"    # Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;

    .prologue
    .line 15
    const-string/jumbo v2, "styles"

    invoke-direct {p0, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 17
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStylesHandler;->styles:Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;

    .line 19
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocDefaultHandler;

    invoke-direct {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocDefaultHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;)V

    .line 20
    .local v0, "defaultHandler":Lcom/samsung/thumbnail/office/ooxml/word/style/XDocDefaultHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;

    invoke-direct {v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;)V

    .line 21
    .local v1, "styleHandler":Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStylesHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "docDefaults"

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStylesHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "style"

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStylesHandler;->styles:Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;

    invoke-interface {v0}, Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;->updateStyleTree()V

    .line 37
    return-void
.end method
