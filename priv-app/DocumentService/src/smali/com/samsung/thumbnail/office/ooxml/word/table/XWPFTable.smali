.class public Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;
.super Ljava/lang/Object;
.source "XWPFTable.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/IBodyElement;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;
    }
.end annotation


# instance fields
.field private mGridColWidthArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private rowSpanCellProp:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;",
            ">;"
        }
    .end annotation
.end field

.field private tableRows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;",
            ">;"
        }
    .end annotation
.end field

.field private tblProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

.field private text:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->text:Ljava/lang/StringBuilder;

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->tableRows:Ljava/util/List;

    .line 75
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->rowSpanCellProp:Ljava/util/Map;

    .line 92
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 2
    .param p1, "isCanvas"    # Z

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->text:Ljava/lang/StringBuilder;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->tableRows:Ljava/util/List;

    .line 70
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->rowSpanCellProp:Ljava/util/Map;

    .line 71
    return-void
.end method


# virtual methods
.method public addMergedCellProperty(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;)V
    .locals 2
    .param p1, "mergedCellProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->rowSpanCellProp:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getCurrentRow()Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;

    move-result-object v1

    iget v1, v1, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;->curCellNo:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    return-void
.end method

.method public addRow(Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;)V
    .locals 1
    .param p1, "row"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->tableRows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    return-void
.end method

.method public getCurrentRow()Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->tableRows:Ljava/util/List;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->tableRows:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;

    return-object v0
.end method

.method public getElementType()Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;
    .locals 1

    .prologue
    .line 403
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->TABLE:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    return-object v0
.end method

.method public getGridColWidthArray()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->mGridColWidthArray:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getMergedCellProperty()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->rowSpanCellProp:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getCurrentRow()Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;

    move-result-object v1

    iget v1, v1, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;->curCellNo:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    return-object v0
.end method

.method public getPreviousRow()Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->tableRows:Ljava/util/List;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->tableRows:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;

    return-object v0
.end method

.method public getRows()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->tableRows:Ljava/util/List;

    return-object v0
.end method

.method public getTableProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->tblProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->text:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setGridColWidthArray(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 121
    .local p1, "gridColArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->mGridColWidthArray:Ljava/util/ArrayList;

    .line 122
    return-void
.end method

.method public setTableProp(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;)V
    .locals 0
    .param p1, "tblProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->tblProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    .line 96
    return-void
.end method
