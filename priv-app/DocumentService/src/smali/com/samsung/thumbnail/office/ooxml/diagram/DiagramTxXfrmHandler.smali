.class public Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DiagramTxXfrmHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler$IXfrmTxBodyHandler;
    }
.end annotation


# instance fields
.field private xfrmTxBody:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler$IXfrmTxBodyHandler;


# direct methods
.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler$IXfrmTxBodyHandler;)V
    .locals 1
    .param p1, "ID"    # I
    .param p2, "xfrmTxBody"    # Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler$IXfrmTxBodyHandler;

    .prologue
    .line 21
    const-string/jumbo v0, "txXfrm"

    invoke-direct {p0, p1, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 22
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler;->xfrmTxBody:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler$IXfrmTxBodyHandler;

    .line 23
    return-void
.end method

.method private init()V
    .locals 6

    .prologue
    .line 26
    const/4 v5, 0x2

    new-array v4, v5, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 28
    .local v4, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPointHandler;

    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler;->xfrmTxBody:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler$IXfrmTxBodyHandler;

    invoke-direct {v2, v5}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPointHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler$IXfrmTxBodyHandler;)V

    .line 29
    .local v2, "offHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramPointHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v5, "off"

    invoke-direct {v3, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 31
    .local v3, "offSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v5, 0x0

    aput-object v3, v4, v5

    .line 33
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSizeHandler;

    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler;->xfrmTxBody:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler$IXfrmTxBodyHandler;

    invoke-direct {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSizeHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler$IXfrmTxBodyHandler;)V

    .line 34
    .local v0, "extHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSizeHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v5, "ext"

    invoke-direct {v1, v5, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 36
    .local v1, "extSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v5, 0x1

    aput-object v1, v4, v5

    .line 38
    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 39
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 44
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 46
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler;->init()V

    .line 48
    const-string/jumbo v2, "rot"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "rot":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 50
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;

    invoke-direct {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;-><init>(Ljava/lang/String;)V

    .line 51
    .local v1, "rotation":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler;->xfrmTxBody:Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler$IXfrmTxBodyHandler;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;->getValue()I

    move-result v3

    invoke-interface {v2, p1, v3}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramTxXfrmHandler$IXfrmTxBodyHandler;->setTxXfrmRotation(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;I)V

    .line 53
    .end local v1    # "rotation":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;
    :cond_0
    return-void
.end method
