.class public final enum Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;
.super Ljava/lang/Enum;
.source "XDocUnderline.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EUnderline"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

.field public static final enum DASH:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

.field public static final enum DASHED_HEAVY:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

.field public static final enum DASH_DOT_DOT_HEAVY:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

.field public static final enum DASH_DOT_HEAVY:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

.field public static final enum DASH_LONG:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

.field public static final enum DASH_LONG_HEAVY:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

.field public static final enum DOTTED:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

.field public static final enum DOTTED_HEAVY:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

.field public static final enum DOT_DASH:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

.field public static final enum DOT_DOT_DASH:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

.field public static final enum DOUBLE:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

.field public static final enum NONE:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

.field public static final enum SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

.field public static final enum THICK:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

.field public static final enum WAVE:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

.field public static final enum WAVY_DOUBLE:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

.field public static final enum WAVY_HEAVY:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

.field public static final enum WORDS:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 100
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    const-string/jumbo v1, "SINGLE"

    const-string/jumbo v2, "single"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    .line 103
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    const-string/jumbo v1, "WORDS"

    const-string/jumbo v2, "words"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->WORDS:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    .line 106
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    const-string/jumbo v1, "DOUBLE"

    const-string/jumbo v2, "double"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->DOUBLE:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    .line 109
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    const-string/jumbo v1, "THICK"

    const-string/jumbo v2, "thick"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->THICK:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    .line 112
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    const-string/jumbo v1, "DOTTED"

    const-string/jumbo v2, "dotted"

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->DOTTED:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    .line 115
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    const-string/jumbo v1, "DOTTED_HEAVY"

    const/4 v2, 0x5

    const-string/jumbo v3, "dottedHeavy"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->DOTTED_HEAVY:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    .line 118
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    const-string/jumbo v1, "DASH"

    const/4 v2, 0x6

    const-string/jumbo v3, "dash"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->DASH:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    .line 121
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    const-string/jumbo v1, "DASHED_HEAVY"

    const/4 v2, 0x7

    const-string/jumbo v3, "dashedHeavy"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->DASHED_HEAVY:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    .line 124
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    const-string/jumbo v1, "DASH_LONG"

    const/16 v2, 0x8

    const-string/jumbo v3, "dashLong"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->DASH_LONG:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    .line 127
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    const-string/jumbo v1, "DASH_LONG_HEAVY"

    const/16 v2, 0x9

    const-string/jumbo v3, "dashLongHeavy"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->DASH_LONG_HEAVY:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    .line 130
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    const-string/jumbo v1, "DOT_DASH"

    const/16 v2, 0xa

    const-string/jumbo v3, "dotDash"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->DOT_DASH:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    .line 133
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    const-string/jumbo v1, "DASH_DOT_HEAVY"

    const/16 v2, 0xb

    const-string/jumbo v3, "dashDotHeavy"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->DASH_DOT_HEAVY:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    .line 136
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    const-string/jumbo v1, "DOT_DOT_DASH"

    const/16 v2, 0xc

    const-string/jumbo v3, "dotDotDash"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->DOT_DOT_DASH:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    .line 139
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    const-string/jumbo v1, "DASH_DOT_DOT_HEAVY"

    const/16 v2, 0xd

    const-string/jumbo v3, "dashDotDotHeavy"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->DASH_DOT_DOT_HEAVY:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    .line 142
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    const-string/jumbo v1, "WAVE"

    const/16 v2, 0xe

    const-string/jumbo v3, "wave"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->WAVE:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    .line 145
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    const-string/jumbo v1, "WAVY_HEAVY"

    const/16 v2, 0xf

    const-string/jumbo v3, "wavyHeavy"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->WAVY_HEAVY:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    .line 148
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    const-string/jumbo v1, "WAVY_DOUBLE"

    const/16 v2, 0x10

    const-string/jumbo v3, "wavyDouble"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->WAVY_DOUBLE:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    .line 151
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    const-string/jumbo v1, "NONE"

    const/16 v2, 0x11

    const-string/jumbo v3, "none"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    .line 96
    const/16 v0, 0x12

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->WORDS:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->DOUBLE:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->THICK:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->DOTTED:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->DOTTED_HEAVY:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->DASH:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->DASHED_HEAVY:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->DASH_LONG:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->DASH_LONG_HEAVY:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->DOT_DASH:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->DASH_DOT_HEAVY:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->DOT_DOT_DASH:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->DASH_DOT_DOT_HEAVY:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->WAVE:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->WAVY_HEAVY:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->WAVY_DOUBLE:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 155
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 156
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->value:Ljava/lang/String;

    .line 157
    return-void
.end method

.method public static convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 164
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->values()[Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 165
    .local v2, "inst":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 169
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;
    :goto_1
    return-object v2

    .line 164
    .restart local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 169
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 96
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    return-object v0
.end method


# virtual methods
.method public xmlValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->value:Ljava/lang/String;

    return-object v0
.end method
