.class public Lcom/samsung/thumbnail/office/ooxml/dml/wp/ExtentHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "ExtentHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/wp/ExtentHandler$IExtentObserver;
    }
.end annotation


# instance fields
.field private extentOberserver:Lcom/samsung/thumbnail/office/ooxml/dml/wp/ExtentHandler$IExtentObserver;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/wp/ExtentHandler$IExtentObserver;)V
    .locals 2
    .param p1, "extentOberserver"    # Lcom/samsung/thumbnail/office/ooxml/dml/wp/ExtentHandler$IExtentObserver;

    .prologue
    .line 19
    const/16 v0, 0x1e

    const-string/jumbo v1, "extent"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 20
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/ExtentHandler;->extentOberserver:Lcom/samsung/thumbnail/office/ooxml/dml/wp/ExtentHandler$IExtentObserver;

    .line 21
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 26
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;

    const-string/jumbo v2, "cx"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/ExtentHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;-><init>(Ljava/lang/String;)V

    .line 28
    .local v0, "cx":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;

    const-string/jumbo v2, "cy"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/ExtentHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;-><init>(Ljava/lang/String;)V

    .line 31
    .local v1, "cy":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/ExtentHandler;->extentOberserver:Lcom/samsung/thumbnail/office/ooxml/dml/wp/ExtentHandler$IExtentObserver;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;->getValue()I

    move-result v3

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLIntegerValue;->getValue()I

    move-result v4

    invoke-interface {v2, v3, v4, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/ExtentHandler$IExtentObserver;->setExtent(IILcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V

    .line 32
    return-void
.end method
