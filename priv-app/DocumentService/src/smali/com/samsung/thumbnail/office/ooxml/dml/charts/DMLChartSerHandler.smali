.class public Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DMLChartSerHandler.java"


# instance fields
.field private shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 27
    const/16 v0, 0x23

    const-string/jumbo v1, "ser"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 28
    return-void
.end method

.method private chartColor(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 4
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "shapeProperties"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 120
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getShapeProps()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 122
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getShapeProps()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 124
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getShapeProps()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v0

    .line 126
    .local v0, "color":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->addColorVal(Ljava/lang/String;)V

    .line 128
    .end local v0    # "color":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getShapeProps()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 130
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getShapeProps()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v1

    .line 132
    .local v1, "lumMod":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v2, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->addLumMod(Ljava/lang/String;)V

    .line 163
    .end local v1    # "lumMod":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 135
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->addLumMod(Ljava/lang/String;)V

    goto :goto_0

    .line 137
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getShapeProps()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 139
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getShapeProps()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 142
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getShapeProps()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 144
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getShapeProps()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v0

    .line 148
    .restart local v0    # "color":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->addColorVal(Ljava/lang/String;)V

    .line 150
    .end local v0    # "color":Ljava/lang/String;
    :cond_4
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getShapeProps()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 152
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getShapeProps()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v1

    .line 156
    .restart local v1    # "lumMod":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v2, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->addLumMod(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 159
    .end local v1    # "lumMod":Ljava/lang/String;
    :cond_5
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->addLumMod(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private init()V
    .locals 24

    .prologue
    .line 31
    const/16 v22, 0x9

    move/from16 v0, v22

    new-array v11, v0, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 33
    .local v11, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerCatHandler;

    invoke-direct {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerCatHandler;-><init>()V

    .line 34
    .local v5, "catHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerCatHandler;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v22, "cat"

    move-object/from16 v0, v22

    invoke-direct {v6, v0, v5}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 36
    .local v6, "catSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v22, 0x0

    aput-object v6, v11, v22

    .line 38
    new-instance v16, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerValHandler;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerValHandler;-><init>()V

    .line 39
    .local v16, "valHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerValHandler;
    new-instance v17, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v22, "val"

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 41
    .local v17, "valSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v22, 0x1

    aput-object v17, v11, v22

    .line 43
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerXValHandler;

    invoke-direct/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerXValHandler;-><init>()V

    .line 44
    .local v18, "xvalHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerXValHandler;
    new-instance v19, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v22, "xVal"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 46
    .local v19, "xvalSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v22, 0x2

    aput-object v19, v11, v22

    .line 48
    new-instance v20, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerYValHandler;

    invoke-direct/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerYValHandler;-><init>()V

    .line 49
    .local v20, "yvalHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerYValHandler;
    new-instance v21, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v22, "yVal"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 51
    .local v21, "yvalSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v22, 0x3

    aput-object v21, v11, v22

    .line 53
    new-instance v14, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerTxHandler;

    invoke-direct {v14}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerTxHandler;-><init>()V

    .line 54
    .local v14, "txHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerTxHandler;
    new-instance v15, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v22, "tx"

    move-object/from16 v0, v22

    invoke-direct {v15, v0, v14}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 56
    .local v15, "txSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v22, 0x4

    aput-object v15, v11, v22

    .line 58
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLBubbleSizeHandler;

    invoke-direct {v4}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLBubbleSizeHandler;-><init>()V

    .line 59
    .local v4, "bubbleHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLBubbleSizeHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v22, "bubbleSize"

    move-object/from16 v0, v22

    invoke-direct {v3, v0, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 61
    .local v3, "bubSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v22, 0x5

    aput-object v3, v11, v22

    .line 63
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLOrderHandler;

    invoke-direct {v9}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLOrderHandler;-><init>()V

    .line 64
    .local v9, "orderHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLOrderHandler;
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v22, "order"

    move-object/from16 v0, v22

    invoke-direct {v10, v0, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 66
    .local v10, "orderSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v22, 0x6

    aput-object v10, v11, v22

    .line 68
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;

    const/16 v22, 0x23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v23, v0

    move/from16 v0, v22

    move-object/from16 v1, v23

    invoke-direct {v12, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 70
    .local v12, "shapePropHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ShapePropertiesHandler;
    new-instance v13, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v22, "spPr"

    move-object/from16 v0, v22

    invoke-direct {v13, v0, v12}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 72
    .local v13, "shapePropSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v22, 0x7

    aput-object v13, v11, v22

    .line 74
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLMarkerColorHandler;

    const/16 v22, 0x23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v23, v0

    move/from16 v0, v22

    move-object/from16 v1, v23

    invoke-direct {v7, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLMarkerColorHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 76
    .local v7, "markerColorHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLMarkerColorHandler;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v22, "marker"

    move-object/from16 v0, v22

    invoke-direct {v8, v0, v7}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 78
    .local v8, "markerColorSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/16 v22, 0x8

    aput-object v8, v11, v22

    .line 80
    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 81
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 109
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 111
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    if-eqz v0, :cond_0

    .line 116
    :goto_0
    return-void

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-direct {p0, p1, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerHandler;->chartColor(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    goto :goto_0
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 85
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 86
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 87
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerHandler;->init()V

    .line 90
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;-><init>()V

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->addSeries(Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;)V

    .line 93
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->setShapeProps(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 105
    :goto_0
    return-void

    .line 98
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    if-eqz v0, :cond_1

    .line 99
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;-><init>()V

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->addSeries(Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;)V

    .line 101
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->setShapeProps(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 104
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->setShapeProps(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    goto :goto_0
.end method
