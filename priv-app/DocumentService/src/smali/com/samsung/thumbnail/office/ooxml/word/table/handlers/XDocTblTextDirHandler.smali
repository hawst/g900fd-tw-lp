.class public Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTextDirHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XDocTblTextDirHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTextDirHandler$ITxtDirObserver;
    }
.end annotation


# instance fields
.field private txtDirObserver:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTextDirHandler$ITxtDirObserver;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTextDirHandler$ITxtDirObserver;)V
    .locals 0
    .param p1, "elementName"    # Ljava/lang/String;
    .param p2, "txtDirObserver"    # Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTextDirHandler$ITxtDirObserver;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 16
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTextDirHandler;->txtDirObserver:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTextDirHandler$ITxtDirObserver;

    .line 17
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 22
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 23
    const-string/jumbo v1, "val"

    invoke-virtual {p0, p3, v1, p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTextDirHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 24
    .local v0, "txtDir":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTextDirHandler;->txtDirObserver:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTextDirHandler$ITxtDirObserver;

    if-eqz v1, :cond_0

    .line 25
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTextDirHandler;->txtDirObserver:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTextDirHandler$ITxtDirObserver;

    invoke-interface {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTextDirHandler$ITxtDirObserver;->setTextDirection(Ljava/lang/String;)V

    .line 27
    :cond_0
    return-void
.end method
