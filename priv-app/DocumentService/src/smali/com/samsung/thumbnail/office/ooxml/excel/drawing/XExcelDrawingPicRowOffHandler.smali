.class public Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowOffHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedTagNoChildrenHandler;
.source "XExcelDrawingPicRowOffHandler.java"


# instance fields
.field private cellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

.field private fromTo:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;)V
    .locals 2
    .param p1, "fromTo"    # Ljava/lang/String;
    .param p2, "cellAnchor"    # Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    .prologue
    .line 16
    const/16 v0, 0xc9

    const-string/jumbo v1, "rowOff"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedTagNoChildrenHandler;-><init>(ILjava/lang/String;)V

    .line 18
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowOffHandler;->fromTo:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowOffHandler;->cellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    .line 20
    return-void
.end method


# virtual methods
.method public charactersHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;[CII)V
    .locals 4
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "content"    # [C
    .param p3, "start"    # I
    .param p4, "end"    # I

    .prologue
    .line 25
    invoke-super {p0, p1, p2, p3, p4}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedTagNoChildrenHandler;->charactersHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;[CII)V

    .line 26
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2, p3, p4}, Ljava/lang/String;-><init>([CII)V

    .line 28
    .local v0, "rowOffset":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowOffHandler;->fromTo:Ljava/lang/String;

    const-string/jumbo v2, "from"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 29
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowOffHandler;->cellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->setFromRowOff(J)V

    .line 33
    :cond_0
    :goto_0
    return-void

    .line 30
    :cond_1
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowOffHandler;->cellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    instance-of v1, v1, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;

    if-eqz v1, :cond_0

    .line 31
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelDrawingPicRowOffHandler;->cellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;->setToRowOff(J)V

    goto :goto_0
.end method
