.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelStyleFontColorHandler.java"


# instance fields
.field auto:Ljava/lang/String;

.field rgb:Ljava/lang/String;

.field theme:Ljava/lang/String;

.field tint:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    const-string/jumbo v0, "color"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 31
    return-void
.end method


# virtual methods
.method public fillBorderbottom(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "rgb"    # Ljava/lang/String;
    .param p3, "theme"    # Ljava/lang/String;
    .param p4, "tint"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 183
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v0

    .line 185
    .local v0, "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    if-eqz p2, :cond_0

    .line 186
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    const/4 v2, 0x2

    invoke-virtual {p2, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    iget v3, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tblbordercount:I

    invoke-virtual {v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellBottomBorderRGBColor(Ljava/lang/String;I)V

    .line 195
    :goto_0
    if-eqz p3, :cond_1

    .line 196
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tblbordercount:I

    invoke-virtual {v1, p3, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellBottomBorderThemeColor(Ljava/lang/String;I)V

    .line 211
    :goto_1
    return-void

    .line 190
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tblbordercount:I

    invoke-virtual {v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellBottomBorderRGBColor(Ljava/lang/String;I)V

    goto :goto_0

    .line 200
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tblbordercount:I

    invoke-virtual {v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellBottomBorderThemeColor(Ljava/lang/String;I)V

    goto :goto_1
.end method

.method public fillBorderleft(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "rgb"    # Ljava/lang/String;
    .param p3, "theme"    # Ljava/lang/String;
    .param p4, "tint"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 124
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v0

    .line 126
    .local v0, "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    if-eqz p2, :cond_0

    .line 127
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    const/4 v2, 0x2

    invoke-virtual {p2, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    iget v3, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tblbordercount:I

    invoke-virtual {v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellLeftBorderRGBColor(Ljava/lang/String;I)V

    .line 135
    :goto_0
    if-eqz p3, :cond_1

    .line 136
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tblbordercount:I

    invoke-virtual {v1, p3, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellLeftBorderThemeColor(Ljava/lang/String;I)V

    .line 149
    :goto_1
    return-void

    .line 131
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tblbordercount:I

    invoke-virtual {v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellLeftBorderRGBColor(Ljava/lang/String;I)V

    goto :goto_0

    .line 140
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tblbordercount:I

    invoke-virtual {v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellLeftBorderThemeColor(Ljava/lang/String;I)V

    goto :goto_1
.end method

.method public fillBorderright(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "rgb"    # Ljava/lang/String;
    .param p3, "theme"    # Ljava/lang/String;
    .param p4, "tint"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 96
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v0

    .line 98
    .local v0, "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    if-eqz p2, :cond_0

    .line 99
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    const/4 v2, 0x2

    invoke-virtual {p2, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    iget v3, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tblbordercount:I

    invoke-virtual {v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellRightBorderRGBColor(Ljava/lang/String;I)V

    .line 107
    :goto_0
    if-eqz p3, :cond_1

    .line 108
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tblbordercount:I

    invoke-virtual {v1, p3, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellRightBorderThemeColor(Ljava/lang/String;I)V

    .line 120
    :goto_1
    return-void

    .line 103
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tblbordercount:I

    invoke-virtual {v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellRightBorderRGBColor(Ljava/lang/String;I)V

    goto :goto_0

    .line 112
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tblbordercount:I

    invoke-virtual {v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellRightBorderThemeColor(Ljava/lang/String;I)V

    goto :goto_1
.end method

.method public fillBordertop(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "rgb"    # Ljava/lang/String;
    .param p3, "theme"    # Ljava/lang/String;
    .param p4, "tint"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 153
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v0

    .line 155
    .local v0, "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    if-eqz p2, :cond_0

    .line 156
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    const/4 v2, 0x2

    invoke-virtual {p2, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    iget v3, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tblbordercount:I

    invoke-virtual {v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellTopBorderRGBColor(Ljava/lang/String;I)V

    .line 164
    :goto_0
    if-eqz p3, :cond_1

    .line 165
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tblbordercount:I

    invoke-virtual {v1, p3, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellTopBorderThemeColor(Ljava/lang/String;I)V

    .line 179
    :goto_1
    return-void

    .line 160
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tblbordercount:I

    invoke-virtual {v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellTopBorderRGBColor(Ljava/lang/String;I)V

    goto :goto_0

    .line 170
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tblbordercount:I

    invoke-virtual {v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellTopBorderThemeColor(Ljava/lang/String;I)V

    goto :goto_1
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 36
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 37
    const-string/jumbo v2, "theme"

    invoke-interface {p3, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->theme:Ljava/lang/String;

    .line 38
    const-string/jumbo v2, "rgb"

    invoke-interface {p3, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->rgb:Ljava/lang/String;

    .line 39
    const-string/jumbo v2, "tint"

    invoke-interface {p3, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->tint:Ljava/lang/String;

    .line 40
    const-string/jumbo v2, "auto"

    invoke-interface {p3, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->auto:Ljava/lang/String;

    .line 44
    const-string/jumbo v2, "indexed"

    invoke-interface {p3, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, "indexedColor":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->rgb:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 48
    if-eqz v0, :cond_0

    .line 49
    invoke-static {v0}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->getIndexedColorHexCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->rgb:Ljava/lang/String;

    .line 50
    :cond_0
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->auto:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 52
    const-string/jumbo v2, "0"

    invoke-static {v2}, Lcom/samsung/thumbnail/office/excel/HSSFExcelColorMap;->getIndexedColorHexCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->rgb:Ljava/lang/String;

    .line 56
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getStyle()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "borderStyleRight"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_2

    .line 58
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->rgb:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->theme:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->tint:Ljava/lang/String;

    invoke-virtual {p0, p1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->fillBorderright(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getStyle()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "borderStyleLeft"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_3

    .line 62
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->rgb:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->theme:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->tint:Ljava/lang/String;

    invoke-virtual {p0, p1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->fillBorderleft(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getStyle()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "borderStyleTop"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_4

    .line 66
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->rgb:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->theme:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->tint:Ljava/lang/String;

    invoke-virtual {p0, p1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->fillBordertop(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    :cond_4
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getStyle()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "borderStyleBottom"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_6

    .line 70
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->rgb:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->theme:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->tint:Ljava/lang/String;

    invoke-virtual {p0, p1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->fillBorderbottom(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    :cond_5
    :goto_0
    return-void

    .line 72
    :cond_6
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v1

    .line 74
    .local v1, "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->rgb:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 75
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->rgb:Ljava/lang/String;

    iget v4, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    invoke-virtual {v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleTextColorRGB(Ljava/lang/String;I)V

    .line 77
    :cond_7
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->theme:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 78
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->theme:Ljava/lang/String;

    iget v4, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    invoke-virtual {v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleTextColorTheme(Ljava/lang/String;I)V

    .line 80
    :cond_8
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->tint:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 81
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontColorHandler;->tint:Ljava/lang/String;

    iget v4, v1, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->fontid:I

    invoke-virtual {v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleTextColorTint(Ljava/lang/String;I)V

    goto :goto_0
.end method
