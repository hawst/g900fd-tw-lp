.class public Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeHandler;
.super Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;
.source "ShapeHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/StrokeHandler$IStrokeConsumer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    const/16 v0, 0x28

    const-string/jumbo v1, "shape"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;-><init>(ILjava/lang/String;)V

    .line 20
    return-void
.end method


# virtual methods
.method protected init()V
    .locals 3

    .prologue
    .line 23
    invoke-super {p0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->init()V

    .line 24
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ImageDataHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ImageDataHandler;-><init>()V

    .line 25
    .local v0, "imageDataHandler":Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ImageDataHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "imagedata"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    return-void
.end method

.method public setStroke(Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;)V
    .locals 1
    .param p1, "strokeProp"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setStrokeProp(Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;)V

    .line 53
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 31
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeElementsHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 33
    const-string/jumbo v1, "connectortype"

    const/16 v2, 0x32

    invoke-virtual {p0, p3, v1, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeHandler;->getAttrForNS(Lorg/xml/sax/Attributes;Ljava/lang/String;ILcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 35
    .local v0, "val":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 36
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setConnectorType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;)V

    .line 39
    :cond_0
    const-string/jumbo v1, "id"

    invoke-virtual {p0, p3, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 40
    if-eqz v0, :cond_1

    .line 41
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setId(Ljava/lang/String;)V

    .line 44
    :cond_1
    const-string/jumbo v1, "type"

    invoke-virtual {p0, p3, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 45
    if-eqz v0, :cond_2

    .line 46
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/ShapeHandler;->shapeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setType(Ljava/lang/String;)V

    .line 48
    :cond_2
    return-void
.end method
