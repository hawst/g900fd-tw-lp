.class public Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlinkRun;
.super Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
.source "XWPFHyperlinkRun.java"


# instance fields
.field private anchor:Ljava/lang/String;

.field private rId:Ljava/lang/String;

.field private runLst:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;-><init>()V

    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlinkRun;->runLst:Ljava/util/ArrayList;

    .line 16
    return-void
.end method


# virtual methods
.method public addRun(Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;)V
    .locals 1
    .param p1, "run"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlinkRun;->runLst:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 20
    return-void
.end method

.method public getAnchor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlinkRun;->anchor:Ljava/lang/String;

    return-object v0
.end method

.method public getHyperlinkId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlinkRun;->rId:Ljava/lang/String;

    return-object v0
.end method

.method public getRunList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;",
            ">;"
        }
    .end annotation

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlinkRun;->runLst:Ljava/util/ArrayList;

    return-object v0
.end method

.method public setAnchor(Ljava/lang/String;)V
    .locals 0
    .param p1, "anchor"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlinkRun;->anchor:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public setHyperlinkId(Ljava/lang/String;)V
    .locals 0
    .param p1, "rId"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlinkRun;->rId:Ljava/lang/String;

    .line 43
    return-void
.end method
