.class public Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLMainNSHandler;
.source "XDocTblTRPrHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;
.implements Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XTblRowHeightHandler$IRowHeightObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler$ITblRowPropObserver;
    }
.end annotation


# instance fields
.field private observer:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler$ITblRowPropObserver;

.field private rowProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler$ITblRowPropObserver;)V
    .locals 1
    .param p1, "observer"    # Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler$ITblRowPropObserver;

    .prologue
    .line 25
    const-string/jumbo v0, "trPr"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLMainNSHandler;-><init>(Ljava/lang/String;)V

    .line 26
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler;->observer:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler$ITblRowPropObserver;

    .line 27
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    .line 30
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XTblRowHeightHandler;

    invoke-direct {v1, p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XTblRowHeightHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XTblRowHeightHandler$IRowHeightObserver;)V

    .line 31
    .local v1, "heightHandler":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XTblRowHeightHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "trHeight"

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;

    const-string/jumbo v2, "cnfStyle"

    invoke-direct {v0, v2, p0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 35
    .local v0, "cnfHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "cnfStyle"

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    return-void
.end method


# virtual methods
.method public consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V
    .locals 3
    .param p1, "value"    # Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;

    .prologue
    .line 69
    instance-of v1, p1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 70
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    .line 71
    .local v0, "highlight":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler;->rowProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;->setCNFStyle(Ljava/lang/String;)V

    .line 73
    .end local v0    # "highlight":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    :cond_0
    return-void
.end method

.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLMainNSHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method public setHeightRule(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;)V
    .locals 1
    .param p1, "hRule"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler;->rowProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;->setHRule(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;)V

    .line 60
    return-void
.end method

.method public setRowHeight(I)V
    .locals 1
    .param p1, "val"    # I

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler;->rowProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;->setHeight(I)V

    .line 55
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 41
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLMainNSHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 42
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler;->init()V

    .line 43
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler;->rowProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;

    .line 44
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler;->observer:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler$ITblRowPropObserver;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler;->rowProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;

    invoke-interface {v0, p1, v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTRPrHandler$ITblRowPropObserver;->setTblRowProp(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;)V

    .line 45
    return-void
.end method
