.class public Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;
.super Ljava/lang/Object;
.source "XWPFPicture.java"


# instance fields
.field protected behindDoc:Z

.field protected enclosedSpName:Ljava/lang/String;

.field protected hOffsetPos:F

.field protected height:F

.field private isInLine:Z

.field protected relId:Ljava/lang/String;

.field protected relativeHeight:J

.field protected rotation:F

.field protected vOffsetPos:F

.field private vmlStyle:Z

.field protected width:F


# direct methods
.method public constructor <init>(FF)V
    .locals 3
    .param p1, "width"    # F
    .param p2, "height"    # F

    .prologue
    const/4 v2, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->relativeHeight:J

    .line 39
    iput-boolean v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->isInLine:Z

    .line 40
    iput-boolean v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->vmlStyle:Z

    .line 50
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->width:F

    .line 51
    iput p2, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->height:F

    .line 52
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V
    .locals 3
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    .prologue
    const/4 v2, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->relativeHeight:J

    .line 39
    iput-boolean v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->isInLine:Z

    .line 40
    iput-boolean v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->vmlStyle:Z

    .line 43
    if-eqz p1, :cond_0

    .line 45
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getStyle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->setVMLStyle(Ljava/lang/String;)V

    .line 47
    :cond_0
    return-void
.end method


# virtual methods
.method public getBehindDocState()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->behindDoc:Z

    return v0
.end method

.method public getEnclosedSpName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->enclosedSpName:Ljava/lang/String;

    return-object v0
.end method

.method public getHeight()F
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->height:F

    return v0
.end method

.method public getHorizontalOffsetPos()F
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->hOffsetPos:F

    return v0
.end method

.method public getImageRelId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->relId:Ljava/lang/String;

    return-object v0
.end method

.method public getRelId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->relId:Ljava/lang/String;

    return-object v0
.end method

.method public getRelativeHeight()J
    .locals 2

    .prologue
    .line 71
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->relativeHeight:J

    return-wide v0
.end method

.method public getRotation()F
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->rotation:F

    return v0
.end method

.method public getVerticalOffsetPos()F
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->vOffsetPos:F

    return v0
.end method

.method public getWidth()F
    .locals 1

    .prologue
    .line 145
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->width:F

    return v0
.end method

.method public isInLine()Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->isInLine:Z

    return v0
.end method

.method public isVMLType()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->vmlStyle:Z

    return v0
.end method

.method public setBehindDoc(Z)V
    .locals 0
    .param p1, "behindDoc"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->behindDoc:Z

    .line 68
    return-void
.end method

.method public setEffectExtent(IIII)V
    .locals 3
    .param p1, "l"    # I
    .param p2, "r"    # I
    .param p3, "b"    # I
    .param p4, "t"    # I

    .prologue
    .line 129
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->width:F

    int-to-float v1, p1

    invoke-static {v1}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints1(F)F

    move-result v1

    int-to-float v2, p2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints1(F)F

    move-result v2

    add-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->width:F

    .line 130
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->height:F

    int-to-float v1, p3

    invoke-static {v1}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints1(F)F

    move-result v1

    int-to-float v2, p4

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints1(F)F

    move-result v2

    add-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->height:F

    .line 131
    return-void
.end method

.method public setEnclosedSpName(Ljava/lang/String;)V
    .locals 0
    .param p1, "enclosedSpName"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->enclosedSpName:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public setExtent(II)V
    .locals 1
    .param p1, "cx"    # I
    .param p2, "cy"    # I

    .prologue
    .line 120
    int-to-float v0, p1

    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints1(F)F

    move-result v0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->width:F

    .line 121
    int-to-float v0, p2

    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints1(F)F

    move-result v0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->height:F

    .line 122
    return-void
.end method

.method public setImageRelId(Ljava/lang/String;)V
    .locals 0
    .param p1, "relId"    # Ljava/lang/String;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->relId:Ljava/lang/String;

    .line 117
    return-void
.end method

.method public setInLineStatus(Z)V
    .locals 0
    .param p1, "isInLine"    # Z

    .prologue
    .line 95
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->isInLine:Z

    .line 96
    return-void
.end method

.method public setPictPosOffset(Ljava/lang/String;F)V
    .locals 1
    .param p1, "offsetRef"    # Ljava/lang/String;
    .param p2, "offsetVal"    # F

    .prologue
    .line 134
    if-eqz p1, :cond_0

    .line 135
    const-string/jumbo v0, "column"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    iput p2, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->hOffsetPos:F

    .line 141
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->isInLine:Z

    .line 142
    return-void

    .line 137
    :cond_1
    const-string/jumbo v0, "paragraph"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    iput p2, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->vOffsetPos:F

    goto :goto_0
.end method

.method public setRelativeHeight(J)V
    .locals 1
    .param p1, "relativeHeight"    # J

    .prologue
    .line 75
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->relativeHeight:J

    .line 76
    return-void
.end method

.method public setRotation(I)V
    .locals 1
    .param p1, "rotation"    # I

    .prologue
    .line 125
    invoke-static {p1}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->rotationAngelToDegrees16_16(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->rotation:F

    .line 126
    return-void
.end method

.method public setVMLStyle(Ljava/lang/String;)V
    .locals 6
    .param p1, "style"    # Ljava/lang/String;

    .prologue
    .line 169
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->vmlStyle:Z

    .line 171
    :try_start_0
    const-string/jumbo v3, "width:"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 172
    const-string/jumbo v3, "width:"

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 174
    .local v2, "wid":Ljava/lang/String;
    const-string/jumbo v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    const-string/jumbo v4, "pt"

    invoke-virtual {v2, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 175
    invoke-static {v2}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->width:F

    .line 178
    .end local v2    # "wid":Ljava/lang/String;
    :cond_0
    const-string/jumbo v3, "height:"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 179
    const-string/jumbo v3, "height:"

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 181
    .local v1, "hei":Ljava/lang/String;
    const-string/jumbo v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    const-string/jumbo v4, "pt"

    invoke-virtual {v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 182
    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->height:F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 204
    .end local v1    # "hei":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 201
    :catch_0
    move-exception v0

    .line 202
    .local v0, "ex":Ljava/lang/Exception;
    const-string/jumbo v3, "XWPFPicture"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception in setting Picture Style : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setVMLType(Z)V
    .locals 0
    .param p1, "vmlStyle"    # Z

    .prologue
    .line 107
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->vmlStyle:Z

    .line 108
    return-void
.end method
