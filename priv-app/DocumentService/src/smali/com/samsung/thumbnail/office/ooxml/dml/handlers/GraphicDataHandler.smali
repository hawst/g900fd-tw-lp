.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GraphicDataHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "GraphicDataHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 21
    const/16 v0, 0x1f

    const-string/jumbo v1, "graphicData"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 22
    return-void
.end method

.method private init()V
    .locals 6

    .prologue
    .line 25
    const/4 v5, 0x2

    new-array v4, v5, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 27
    .local v4, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PictureHandler;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PictureHandler;-><init>()V

    .line 28
    .local v2, "picHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PictureHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PictureSeqDescriptor;

    const-string/jumbo v5, "pic"

    invoke-direct {v3, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PictureSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 30
    .local v3, "picSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/PictureSeqDescriptor;
    const/4 v5, 0x0

    aput-object v3, v4, v5

    .line 32
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramRelIdsHandler;

    const/16 v5, 0xca

    invoke-direct {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramRelIdsHandler;-><init>(I)V

    .line 34
    .local v0, "dgmHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramRelIdsHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DgmSeqDescriptor;

    const-string/jumbo v5, "relIds"

    invoke-direct {v1, v5, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DgmSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 36
    .local v1, "dgmSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DgmSeqDescriptor;
    const/4 v5, 0x1

    aput-object v1, v4, v5

    .line 38
    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GraphicDataHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 39
    return-void
.end method


# virtual methods
.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 51
    const/16 v2, 0x23

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v2

    invoke-static {p2, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GraphicDataHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)Ljava/lang/String;

    move-result-object v1

    .line 54
    .local v1, "elementName":Ljava/lang/String;
    const-string/jumbo v2, "chart"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 55
    const-string/jumbo v2, "id"

    const/16 v3, 0xa

    invoke-virtual {p0, p3, v2, v3, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GraphicDataHandler;->getAttrForNS(Lorg/xml/sax/Attributes;Ljava/lang/String;ILcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 57
    .local v0, "attr":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->addChart(Ljava/lang/String;)V

    .line 61
    .end local v0    # "attr":Ljava/lang/String;
    :goto_0
    return-void

    .line 59
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    goto :goto_0
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 44
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 45
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GraphicDataHandler;->init()V

    .line 46
    return-void
.end method
