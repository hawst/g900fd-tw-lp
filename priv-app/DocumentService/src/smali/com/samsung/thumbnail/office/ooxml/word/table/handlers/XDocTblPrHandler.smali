.class public Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;
.super Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;
.source "XDocTblPrHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPPrHandler$ITblPPrObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler$ITblPropObserver;,
        Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler$XTblStyleId;
    }
.end annotation


# instance fields
.field private observer:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler$ITblPropObserver;

.field private tableProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler$ITblPropObserver;)V
    .locals 1
    .param p1, "observer"    # Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler$ITblPropObserver;

    .prologue
    .line 25
    const-string/jumbo v0, "tblPr"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;-><init>(Ljava/lang/String;)V

    .line 26
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;->observer:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler$ITblPropObserver;

    .line 27
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;)Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;->tableProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    return-object v0
.end method


# virtual methods
.method protected init()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 30
    invoke-super {p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;->init()V

    .line 31
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    array-length v6, v6

    add-int/lit8 v6, v6, 0x2

    new-array v0, v6, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 33
    .local v0, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler$XTblStyleId;

    invoke-direct {v1, p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler$XTblStyleId;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;)V

    .line 34
    .local v1, "styleId":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler$XTblStyleId;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;

    const-string/jumbo v6, "tblStyle"

    invoke-direct {v2, v6, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 36
    .local v2, "styleIdHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLStringHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v6, "tblStyle"

    invoke-direct {v3, v6, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 38
    .local v3, "styleIdSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    aput-object v3, v0, v9

    .line 40
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPPrHandler;

    invoke-direct {v4, p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPPrHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPPrHandler$ITblPPrObserver;)V

    .line 41
    .local v4, "tblPpr":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPPrHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v6, "tblpPr"

    invoke-direct {v5, v6, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 43
    .local v5, "tblPprSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v6, 0x1

    aput-object v5, v0, v6

    .line 45
    iget-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    const/4 v7, 0x2

    iget-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    array-length v8, v8

    invoke-static {v6, v9, v0, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 47
    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 48
    return-void
.end method

.method public setTblPosition(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "tblXPos"    # Ljava/lang/String;
    .param p2, "tblYPos"    # Ljava/lang/String;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;->tableProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->setXPosition(Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;->tableProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->setYPosition(Ljava/lang/String;)V

    .line 78
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 53
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 54
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;->tableProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    .line 55
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;->tableProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrExHandler;->tableProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    .line 56
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;->observer:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler$ITblPropObserver;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler;->tableProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    invoke-interface {v0, p1, v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblPrHandler$ITblPropObserver;->setTblProp(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;)V

    .line 57
    return-void
.end method
