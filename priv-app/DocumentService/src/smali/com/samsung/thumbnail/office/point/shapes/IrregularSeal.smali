.class public Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;
.super Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;
.source "IrregularSeal.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mDrawOnCanvas:Z

.field private mLeftMarg:I

.field private mTopMarg:I


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "folderPath"    # Ljava/io/File;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;-><init>()V

    .line 24
    const-string/jumbo v0, "IrregularSeal"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->TAG:Ljava/lang/String;

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mDrawOnCanvas:Z

    .line 30
    iput-object p1, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->folderPath:Ljava/io/File;

    .line 31
    return-void
.end method


# virtual methods
.method protected draw(Landroid/graphics/Canvas;I)V
    .locals 13
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeCount"    # I

    .prologue
    .line 152
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->width:F

    const v1, 0x46a8c000    # 21600.0f

    div-float v9, v0, v1

    .line 153
    .local v9, "mulFactor_Width":F
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->height:F

    const v1, 0x46a8c000    # 21600.0f

    div-float v8, v0, v1

    .line 155
    .local v8, "mulFactor_Height":F
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->shapeName:Ljava/lang/String;

    const-string/jumbo v1, "IrregularSeal1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 156
    new-instance v11, Landroid/graphics/Path;

    invoke-direct {v11}, Landroid/graphics/Path;-><init>()V

    .line 157
    .local v11, "path":Landroid/graphics/Path;
    const v0, 0x4628c000    # 10800.0f

    mul-float/2addr v0, v9

    const v1, 0x45b54000    # 5800.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 158
    const v0, 0x46028000    # 8352.0f

    mul-float/2addr v0, v9

    const v1, 0x450f7000    # 2295.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 159
    const v0, 0x45e48000    # 7312.0f

    mul-float/2addr v0, v9

    const v1, 0x45c58000    # 6320.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 160
    const/high16 v0, 0x43b90000    # 370.0f

    mul-float/2addr v0, v9

    const v1, 0x450f7000    # 2295.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 161
    const v0, 0x45909800    # 4627.0f

    mul-float/2addr v0, v9

    const v1, 0x45ee0800    # 7617.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 162
    const/4 v0, 0x0

    mul-float/2addr v0, v9

    const v1, 0x46069c00    # 8615.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 163
    const v0, 0x4568a000    # 3722.0f

    mul-float/2addr v0, v9

    const v1, 0x45458000    # 3160.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 164
    const/high16 v0, 0x43070000    # 135.0f

    mul-float/2addr v0, v9

    const v1, 0x4663ec00    # 14587.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 165
    const v0, 0x45ace000    # 5532.0f

    mul-float/2addr v0, v9

    const v1, -0x3bdd8000    # -650.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 166
    const v0, 0x4594d000    # 4762.0f

    mul-float/2addr v0, v9

    const v1, 0x4689a200    # 17617.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 167
    const v0, 0x45f11800    # 7715.0f

    mul-float/2addr v0, v9

    const v1, 0x46742c00    # 15627.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 168
    const v0, 0x44408000    # 770.0f

    mul-float/2addr v0, v9

    const v1, 0x45baa800    # 5973.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 169
    const v0, 0x46249000    # 10532.0f

    mul-float/2addr v0, v9

    const v1, 0x46695c00    # 14935.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 170
    const v0, 0x4529b000    # 2715.0f

    mul-float/2addr v0, v9

    const v1, 0x45961000    # 4802.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 171
    const v0, 0x465b1000    # 14020.0f

    mul-float/2addr v0, v9

    const v1, 0x4661e400    # 14457.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 172
    const v0, 0x4580e800    # 4125.0f

    mul-float/2addr v0, v9

    const v1, 0x45636000    # 3638.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 173
    const v0, 0x46838a00    # 16837.0f

    mul-float/2addr v0, v9

    const v1, 0x464a3800    # 12942.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 174
    const v0, 0x4594d800    # 4763.0f

    mul-float/2addr v0, v9

    const/high16 v1, 0x43ae0000    # 348.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 175
    const v0, 0x46898e00    # 17607.0f

    mul-float/2addr v0, v9

    const v1, 0x4623ac00    # 10475.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 176
    const v0, 0x46a4d200    # 21097.0f

    mul-float/2addr v0, v9

    const v1, 0x45fe4800    # 8137.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 177
    const v0, 0x46827c00    # 16702.0f

    mul-float/2addr v0, v9

    const v1, 0x45e49800    # 7315.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 178
    const v0, 0x468f9800    # 18380.0f

    mul-float/2addr v0, v9

    const v1, 0x458b4800    # 4457.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 179
    const v0, -0x3a7bf800    # -4225.0f

    mul-float/2addr v0, v9

    const/high16 v1, 0x44590000    # 868.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 180
    const v0, 0x4662e800    # 14522.0f

    mul-float/2addr v0, v9

    const/4 v1, 0x0

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 181
    invoke-virtual {v11}, Landroid/graphics/Path;->close()V

    .line 183
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->paintFill:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {p1, v11, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->paintLine:Landroid/graphics/Paint;

    if-eqz v0, :cond_1

    .line 186
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->paintLine:Landroid/graphics/Paint;

    invoke-virtual {p1, v11, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 191
    :cond_1
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->textArea:Landroid/graphics/RectF;

    const v1, 0x45a8c000    # 5400.0f

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->width:F

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 192
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->textArea:Landroid/graphics/RectF;

    const v1, 0x45d2f000    # 6750.0f

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->height:F

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 193
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->textArea:Landroid/graphics/RectF;

    const v1, 0x467d2000    # 16200.0f

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->width:F

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 194
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->textArea:Landroid/graphics/RectF;

    const v1, 0x46680800    # 14850.0f

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->height:F

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 244
    .end local v11    # "path":Landroid/graphics/Path;
    :cond_2
    :goto_0
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mDrawOnCanvas:Z

    if-nez v0, :cond_3

    .line 245
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 248
    .local v5, "mtx":Landroid/graphics/Matrix;
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->rotation:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->width:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->height:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    invoke-virtual {v5, v0, v1, v2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 250
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->bitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget v3, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->bitmapWidth:I

    iget v4, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->bitmapHight:I

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 255
    .local v12, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v10, Ljava/io/FileOutputStream;

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->folderPath:Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "pic_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->shapeName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".png"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v10, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 261
    .local v10, "out":Ljava/io/FileOutputStream;
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v1, 0x64

    invoke-virtual {v12, v0, v1, v10}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 262
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 267
    .end local v5    # "mtx":Landroid/graphics/Matrix;
    .end local v10    # "out":Ljava/io/FileOutputStream;
    .end local v12    # "rotatedBMP":Landroid/graphics/Bitmap;
    :cond_3
    :goto_1
    return-void

    .line 197
    :cond_4
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->shapeName:Ljava/lang/String;

    const-string/jumbo v1, "IrregularSeal2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 198
    new-instance v11, Landroid/graphics/Path;

    invoke-direct {v11}, Landroid/graphics/Path;-><init>()V

    .line 199
    .restart local v11    # "path":Landroid/graphics/Path;
    const v0, 0x46331800    # 11462.0f

    mul-float/2addr v0, v9

    const v1, 0x4587b000    # 4342.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 200
    const v0, 0x4617e800    # 9722.0f

    mul-float/2addr v0, v9

    const v1, 0x44ebe000    # 1887.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 201
    const v0, 0x46059800    # 8550.0f

    mul-float/2addr v0, v9

    const v1, 0x45c77000    # 6382.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 202
    const v0, 0x458cb000    # 4502.0f

    mul-float/2addr v0, v9

    const v1, 0x45629000    # 3625.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 203
    const v0, 0x44598000    # 870.0f

    mul-float/2addr v0, v9

    const/high16 v1, 0x45830000    # 4192.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 204
    const v0, 0x44928000    # 1172.0f

    mul-float/2addr v0, v9

    const v1, 0x46013800    # 8270.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 205
    const v0, 0x452cb000    # 2763.0f

    mul-float/2addr v0, v9

    const v1, 0x454fa000    # 3322.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 206
    const/4 v0, 0x0

    mul-float/2addr v0, v9

    const v1, 0x46493400    # 12877.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 207
    const v0, 0x45502000    # 3330.0f

    mul-float/2addr v0, v9

    const v1, 0x451bd000    # 2493.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 208
    const v0, 0x44a0a000    # 1285.0f

    mul-float/2addr v0, v9

    const v1, 0x468b4200    # 17825.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 209
    const/high16 v0, 0x455c0000    # 3520.0f

    mul-float/2addr v0, v9

    const v1, 0x43cf8000    # 415.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 210
    const v0, 0x4599a800    # 4917.0f

    mul-float/2addr v0, v9

    const v1, 0x46a8c000    # 21600.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 211
    const v0, 0x45eb3800    # 7527.0f

    mul-float/2addr v0, v9

    const v1, 0x468d9a00    # 18125.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 212
    const v0, 0x4492a000    # 1173.0f

    mul-float/2addr v0, v9

    const v1, 0x44c66000    # 1587.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 213
    const v0, 0x461a4000    # 9872.0f

    mul-float/2addr v0, v9

    const v1, 0x4687b400    # 17370.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 214
    const v0, 0x44d98000    # 1740.0f

    mul-float/2addr v0, v9

    const/high16 v1, 0x44b80000    # 1472.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 215
    const v0, 0x463e5000    # 12180.0f

    mul-float/2addr v0, v9

    const v1, 0x4678fc00    # 15935.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 216
    const v0, 0x452ca000    # 2762.0f

    mul-float/2addr v0, v9

    const v1, 0x44b36000    # 1435.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 217
    const v0, 0x4664c000    # 14640.0f

    mul-float/2addr v0, v9

    const v1, 0x46603800    # 14350.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 218
    const v0, 0x45846800    # 4237.0f

    mul-float/2addr v0, v9

    const v1, 0x44a04000    # 1282.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 219
    const v0, 0x467ff000    # 16380.0f

    mul-float/2addr v0, v9

    const v1, 0x46405800    # 12310.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 220
    const v0, 0x44ec4000    # 1890.0f

    mul-float/2addr v0, v9

    const/high16 v1, -0x3b810000    # -1020.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 221
    const v0, 0x4684b200    # 16985.0f

    mul-float/2addr v0, v9

    const v1, 0x4612e800    # 9402.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 222
    const v0, 0x46a8c000    # 21600.0f

    mul-float/2addr v0, v9

    const v1, 0x45cfa800    # 6645.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 223
    const v0, 0x467ff000    # 16380.0f

    mul-float/2addr v0, v9

    const v1, 0x45cc2000    # 6532.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 224
    const v0, 0x468cae00    # 18007.0f

    mul-float/2addr v0, v9

    const v1, 0x45464000    # 3172.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 225
    const v0, 0x4662f400    # 14525.0f

    mul-float/2addr v0, v9

    const v1, 0x45b48800    # 5777.0f

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 226
    const v0, 0x46671800    # 14790.0f

    mul-float/2addr v0, v9

    const/4 v1, 0x0

    mul-float/2addr v1, v8

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 227
    invoke-virtual {v11}, Landroid/graphics/Path;->close()V

    .line 229
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->paintFill:Landroid/graphics/Paint;

    if-eqz v0, :cond_5

    .line 230
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->paintFill:Landroid/graphics/Paint;

    invoke-virtual {p1, v11, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 231
    :cond_5
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->paintLine:Landroid/graphics/Paint;

    if-eqz v0, :cond_6

    .line 232
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->paintLine:Landroid/graphics/Paint;

    invoke-virtual {p1, v11, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 237
    :cond_6
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->textArea:Landroid/graphics/RectF;

    const v1, 0x45bdd800    # 6075.0f

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->width:F

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 238
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->textArea:Landroid/graphics/RectF;

    const v1, 0x45c87800    # 6415.0f

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->height:F

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 239
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->textArea:Landroid/graphics/RectF;

    const v1, 0x465d7c00    # 14175.0f

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->width:F

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 240
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->textArea:Landroid/graphics/RectF;

    const v1, 0x466d4400    # 15185.0f

    iget v2, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->height:F

    mul-float/2addr v1, v2

    const v2, 0x46a8c000    # 21600.0f

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 263
    .end local v11    # "path":Landroid/graphics/Path;
    .restart local v5    # "mtx":Landroid/graphics/Matrix;
    .restart local v12    # "rotatedBMP":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v7

    .line 264
    .local v7, "e":Ljava/lang/Exception;
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Exception while writing pictures to out file"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F

    .prologue
    .line 106
    invoke-super {p0, p3, p4, p7, p8}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IFF)V

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mDrawOnCanvas:Z

    .line 108
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mLeftMarg:I

    .line 109
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mTopMarg:I

    .line 110
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 111
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->draw(Landroid/graphics/Canvas;I)V

    .line 112
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 113
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mDrawOnCanvas:Z

    .line 114
    return-void
.end method

.method public drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p2, "shapeCount"    # I

    .prologue
    .line 97
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V

    .line 99
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->draw(I)V

    .line 100
    return-void
.end method

.method public drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 83
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;I)V

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mDrawOnCanvas:Z

    .line 85
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mLeftMarg:I

    .line 86
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mTopMarg:I

    .line 87
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->width:F

    .line 88
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->height:F

    .line 89
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 90
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->draw(Landroid/graphics/Canvas;I)V

    .line 91
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 92
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mDrawOnCanvas:Z

    .line 93
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V
    .locals 0
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 35
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 36
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->draw(I)V

    .line 37
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V
    .locals 2
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I

    .prologue
    .line 41
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mDrawOnCanvas:Z

    .line 43
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mLeftMarg:I

    .line 44
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mTopMarg:I

    .line 45
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 46
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->draw(Landroid/graphics/Canvas;I)V

    .line 47
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mDrawOnCanvas:Z

    .line 49
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 127
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 128
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mDrawOnCanvas:Z

    .line 129
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mLeftMarg:I

    .line 130
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mTopMarg:I

    .line 131
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 132
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->draw(Landroid/graphics/Canvas;I)V

    .line 133
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 134
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mDrawOnCanvas:Z

    .line 135
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 139
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V

    .line 140
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mDrawOnCanvas:Z

    .line 141
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mLeftMarg:I

    .line 142
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mTopMarg:I

    .line 143
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 144
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->draw(Landroid/graphics/Canvas;I)V

    .line 145
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 146
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mDrawOnCanvas:Z

    .line 147
    return-void
.end method

.method public drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .prologue
    .line 121
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 122
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->draw(I)V

    .line 123
    return-void
.end method

.method public drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 54
    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p7

    move v4, p8

    move-object v5, p9

    invoke-super/range {v0 .. v5}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSSExcelShape(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mDrawOnCanvas:Z

    .line 57
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mLeftMarg:I

    .line 58
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mTopMarg:I

    .line 59
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 60
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->draw(Landroid/graphics/Canvas;I)V

    .line 61
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mDrawOnCanvas:Z

    .line 63
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 67
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mDrawOnCanvas:Z

    .line 69
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mLeftMarg:I

    .line 70
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mTopMarg:I

    .line 71
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->width:F

    .line 72
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->height:F

    .line 73
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 74
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->draw(Landroid/graphics/Canvas;I)V

    .line 75
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->mDrawOnCanvas:Z

    .line 77
    return-void
.end method
