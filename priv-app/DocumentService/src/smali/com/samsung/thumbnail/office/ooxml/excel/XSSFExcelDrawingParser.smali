.class public Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelDrawingParser;
.super Ljava/lang/Object;
.source "XSSFExcelDrawingParser.java"


# instance fields
.field private drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;Ljava/io/InputStream;Landroid/content/Context;)V
    .locals 0
    .param p1, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p2, "is"    # Ljava/io/InputStream;
    .param p3, "mContext"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelDrawingParser;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    .line 57
    return-void
.end method

.method private parseExcel(Ljava/io/InputStream;)V
    .locals 2
    .param p1, "is"    # Ljava/io/InputStream;

    .prologue
    .line 61
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelSheetDrawingParser;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelDrawingParser;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelSheetDrawingParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;)V

    .line 63
    .local v0, "excelParser":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelSheetDrawingParser;
    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelSheetDrawingParser;->parse(Ljava/io/InputStream;)V

    .line 64
    return-void
.end method


# virtual methods
.method protected onDrawingRead(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelDrawingParser;->parseExcel(Ljava/io/InputStream;)V

    .line 75
    return-void
.end method
