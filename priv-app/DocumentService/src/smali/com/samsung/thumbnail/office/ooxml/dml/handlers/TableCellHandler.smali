.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableCellHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLChoiceHandler;
.source "TableCellHandler.java"


# instance fields
.field private cell:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;

.field private shapeProps:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

.field private tableRow:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;)V
    .locals 2
    .param p1, "tableRow"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;

    .prologue
    .line 30
    const/16 v0, 0x1f

    const-string/jumbo v1, "tc"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLChoiceHandler;-><init>(ILjava/lang/String;)V

    .line 31
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableCellHandler;->tableRow:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;

    .line 32
    return-void
.end method

.method private init()V
    .locals 6

    .prologue
    const/16 v5, 0x1f

    .line 35
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 36
    .local v0, "handlerMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;>;"
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTTextBodyHandler;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableCellHandler;->cell:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;

    invoke-direct {v2, v5, v3}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTTextBodyHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape;)V

    .line 38
    .local v2, "txtBodyHandler":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTTextBodyHandler;
    const-string/jumbo v3, "txBody"

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableCellHandler;->choiceHandlerMap:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TblTCPrHandler;

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableCellHandler;->shapeProps:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-direct {v1, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TblTCPrHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 43
    .local v1, "tcPrHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TblTCPrHandler;
    const-string/jumbo v3, "tcPr"

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableCellHandler;->choiceHandlerMap:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 88
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLChoiceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 89
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 9
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 52
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLChoiceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 53
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    .line 54
    .local v4, "slide":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-direct {v7}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;-><init>()V

    iput-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableCellHandler;->shapeProps:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 55
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;

    iget-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableCellHandler;->shapeProps:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-direct {v7, v8, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;)V

    iput-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableCellHandler;->cell:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;

    .line 56
    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableCellHandler;->tableRow:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;

    iget-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableCellHandler;->cell:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableRow;->addCell(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTableCell;)V

    .line 58
    const-string/jumbo v7, "hMerge"

    invoke-virtual {p0, p3, v7, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableCellHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 60
    .local v1, "hMerge":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 61
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;

    invoke-direct {v2, v1}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;-><init>(Ljava/lang/String;)V

    .line 62
    .local v2, "hMergeValue":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;
    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableCellHandler;->shapeProps:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;->getValue()Z

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setHMerge(Z)V

    .line 65
    .end local v2    # "hMergeValue":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;
    :cond_0
    const-string/jumbo v7, "gridSpan"

    invoke-virtual {p0, p3, v7, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableCellHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 67
    .local v0, "gridSpan":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 68
    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableCellHandler;->shapeProps:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setGridSpan(I)V

    .line 71
    :cond_1
    const-string/jumbo v7, "vMerge"

    invoke-virtual {p0, p3, v7, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableCellHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v5

    .line 72
    .local v5, "vMerge":Ljava/lang/String;
    if-eqz v5, :cond_2

    .line 73
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;

    invoke-direct {v6, v1}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;-><init>(Ljava/lang/String;)V

    .line 74
    .local v6, "vMergeValue":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;
    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableCellHandler;->shapeProps:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;->getValue()Z

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setVMerge(Z)V

    .line 77
    .end local v6    # "vMergeValue":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;
    :cond_2
    const-string/jumbo v7, "rowSpan"

    invoke-virtual {p0, p3, v7, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableCellHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v3

    .line 79
    .local v3, "rowSpan":Ljava/lang/String;
    if-eqz v3, :cond_3

    .line 80
    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableCellHandler;->shapeProps:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setRowSpan(I)V

    .line 83
    :cond_3
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/TableCellHandler;->init()V

    .line 84
    return-void
.end method
