.class public Lcom/samsung/thumbnail/office/ooxml/contenttype/CTTypes;
.super Ljava/lang/Object;
.source "CTTypes.java"


# instance fields
.field private defaultCT:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/contenttype/CTDefault;",
            ">;"
        }
    .end annotation
.end field

.field private overrideCT:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/contenttype/CTOverride;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTTypes;->defaultCT:Ljava/util/List;

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTTypes;->overrideCT:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addDefault(Lcom/samsung/thumbnail/office/ooxml/contenttype/CTDefault;)V
    .locals 1
    .param p1, "def"    # Lcom/samsung/thumbnail/office/ooxml/contenttype/CTDefault;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTTypes;->defaultCT:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 29
    return-void
.end method

.method public addOverride(Lcom/samsung/thumbnail/office/ooxml/contenttype/CTOverride;)V
    .locals 1
    .param p1, "override"    # Lcom/samsung/thumbnail/office/ooxml/contenttype/CTOverride;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTTypes;->overrideCT:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    return-void
.end method

.method public getCTDefault()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/contenttype/CTDefault;",
            ">;"
        }
    .end annotation

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTTypes;->defaultCT:Ljava/util/List;

    return-object v0
.end method

.method public getCTOverride()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/contenttype/CTOverride;",
            ">;"
        }
    .end annotation

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTTypes;->overrideCT:Ljava/util/List;

    return-object v0
.end method
