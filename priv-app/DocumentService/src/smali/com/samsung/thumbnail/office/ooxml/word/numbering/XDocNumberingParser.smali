.class public Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumberingParser;
.super Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;
.source "XDocNumberingParser.java"


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocStreamParser;-><init>(Ljava/lang/Object;)V

    .line 22
    return-void
.end method


# virtual methods
.method public createRootElementHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumberingHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumberingHandler;-><init>()V

    return-object v0
.end method

.method public getMainNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .locals 1

    .prologue
    .line 31
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/XDocNumberingParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v0

    return-object v0
.end method
