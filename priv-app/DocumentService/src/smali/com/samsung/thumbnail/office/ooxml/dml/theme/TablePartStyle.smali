.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;
.super Ljava/lang/Object;
.source "TablePartStyle.java"


# static fields
.field public static final BOTTOM_BORDER:I = 0x3

.field public static final INSIDE_H_BORDER:I = 0x4

.field public static final INSIDE_V_BORDER:I = 0x5

.field public static final LEFT_BORDER:I = 0x0

.field public static final RIGHT_BORDER:I = 0x1

.field public static final TL_TO_BR_BORDER:I = 0x6

.field public static final TOP_BORDER:I = 0x2

.field public static final TR_TO_BL_BORDER:I = 0x7


# instance fields
.field private bold:Z

.field private fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

.field public fillProps:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;",
            ">;"
        }
    .end annotation
.end field

.field private fontRef:Ljava/lang/String;

.field private italic:Z

.field public tblPartStyle:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;",
            ">;"
        }
    .end annotation
.end field

.field public tcBrdr:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;

.field private txStylefontClr:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->tblPartStyle:Ljava/util/HashMap;

    .line 22
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->fillProps:Ljava/util/HashMap;

    .line 26
    iput-boolean v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->bold:Z

    .line 27
    iput-boolean v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->italic:Z

    .line 31
    return-void
.end method


# virtual methods
.method public addFillProps(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 1
    .param p1, "element"    # Ljava/lang/String;
    .param p2, "fillProp"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->fillProps:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    return-void
.end method

.method public addTblPartStyle(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;)V
    .locals 1
    .param p1, "element"    # Ljava/lang/String;
    .param p2, "tcBrdr"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->tblPartStyle:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    return-void
.end method

.method public getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    return-object v0
.end method

.method public getFillPropsLst()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->fillProps:Ljava/util/HashMap;

    return-object v0
.end method

.method public getFontBold()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->bold:Z

    return v0
.end method

.method public getFontItalic()Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->italic:Z

    return v0
.end method

.method public getFontRef()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->fontRef:Ljava/lang/String;

    return-object v0
.end method

.method public getTblPartStyle()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->tblPartStyle:Ljava/util/HashMap;

    return-object v0
.end method

.method public getTcBrdr()Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->tcBrdr:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;

    return-object v0
.end method

.method public getTxStyFntClr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->txStylefontClr:Ljava/lang/String;

    return-object v0
.end method

.method public setFillProperties(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 0
    .param p1, "fillProps"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .line 63
    return-void
.end method

.method public setFontBold(Z)V
    .locals 0
    .param p1, "bold"    # Z

    .prologue
    .line 86
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->bold:Z

    .line 87
    return-void
.end method

.method public setFontItalic(Z)V
    .locals 0
    .param p1, "italic"    # Z

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->italic:Z

    .line 95
    return-void
.end method

.method public setFontRef(Ljava/lang/String;)V
    .locals 0
    .param p1, "fontRef"    # Ljava/lang/String;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->fontRef:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public setTcBrdr(Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;)V
    .locals 0
    .param p1, "tcBrdr"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->tcBrdr:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;

    .line 51
    return-void
.end method

.method public setTxStyFntClr(Ljava/lang/String;)V
    .locals 0
    .param p1, "txStylefontClr"    # Ljava/lang/String;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TablePartStyle;->txStylefontClr:Ljava/lang/String;

    .line 79
    return-void
.end method
