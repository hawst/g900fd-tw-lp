.class public Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTTextStylesHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XPPTTextStylesHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 9

    .prologue
    const/16 v8, 0x33

    .line 12
    const-string/jumbo v7, "txStyles"

    invoke-direct {p0, v7}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 14
    const/4 v7, 0x3

    new-array v4, v7, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 15
    .local v4, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;

    const-string/jumbo v7, "titleStyle"

    invoke-direct {v5, v8, v7}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;-><init>(ILjava/lang/String;)V

    .line 17
    .local v5, "titleListHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;

    const-string/jumbo v7, "titleStyle"

    invoke-direct {v6, v8, v7, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 20
    .local v6, "titleStyleSeq":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;
    const/4 v7, 0x0

    aput-object v6, v4, v7

    .line 22
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;

    const-string/jumbo v7, "bodyStyle"

    invoke-direct {v0, v8, v7}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;-><init>(ILjava/lang/String;)V

    .line 24
    .local v0, "bodyListHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;

    const-string/jumbo v7, "bodyStyle"

    invoke-direct {v1, v8, v7, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 27
    .local v1, "bodyStyleSeq":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;
    const/4 v7, 0x1

    aput-object v1, v4, v7

    .line 29
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;

    const-string/jumbo v7, "otherStyle"

    invoke-direct {v2, v8, v7}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;-><init>(ILjava/lang/String;)V

    .line 31
    .local v2, "otherListHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ListStylesHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;

    const-string/jumbo v7, "otherStyle"

    invoke-direct {v3, v8, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 34
    .local v3, "otherStyleSeq":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSequenceDescriptor;
    const/4 v7, 0x2

    aput-object v3, v4, v7

    .line 36
    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTTextStylesHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 37
    return-void
.end method
