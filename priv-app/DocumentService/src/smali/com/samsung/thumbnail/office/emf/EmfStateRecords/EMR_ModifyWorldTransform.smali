.class public Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;
.super Ljava/lang/Object;
.source "EMR_ModifyWorldTransform.java"


# instance fields
.field private Size:I

.field private Type:I

.field fileContent:[B

.field private modifyWorldTransformMode:I

.field private offset:I

.field private xFormDx:F

.field private xFormDy:F

.field private xFormM11:F

.field private xFormM12:F

.field private xFormM21:F

.field private xFormM22:F


# direct methods
.method public constructor <init>([BI)V
    .locals 0
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->fileContent:[B

    .line 26
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    .line 27
    return-void
.end method


# virtual methods
.method public getRecordSize()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->Size:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->Type:I

    return v0
.end method

.method public getmodifyWorldTransformMode()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->modifyWorldTransformMode:I

    return v0
.end method

.method public getxFormDx()F
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->xFormDx:F

    return v0
.end method

.method public getxFormDy()F
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->xFormDy:F

    return v0
.end method

.method public getxFormM11()F
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->xFormM11:F

    return v0
.end method

.method public getxFormM12()F
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->xFormM12:F

    return v0
.end method

.method public getxFormM21()F
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->xFormM21:F

    return v0
.end method

.method public getxFormM22()F
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->xFormM22:F

    return v0
.end method

.method public readEmfModifyWorldTransform()V
    .locals 14

    .prologue
    .line 102
    const/4 v2, 0x0

    .line 104
    .local v2, "i":I
    const/4 v11, 0x4

    new-array v3, v11, [B

    .line 106
    .local v3, "intConvert":[B
    const/4 v2, 0x0

    :goto_0
    const/4 v11, 0x3

    if-gt v2, v11, :cond_0

    .line 107
    rsub-int/lit8 v11, v2, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    add-int/2addr v13, v2

    aget-byte v12, v12, v13

    aput-byte v12, v3, v11

    .line 106
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 108
    :cond_0
    const/4 v11, 0x0

    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 109
    .local v1, "Type":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    add-int/2addr v11, v2

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    .line 110
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->setRecordType(I)V

    .line 112
    const/4 v2, 0x0

    :goto_1
    const/4 v11, 0x3

    if-gt v2, v11, :cond_1

    .line 113
    rsub-int/lit8 v11, v2, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    add-int/2addr v13, v2

    aget-byte v12, v12, v13

    aput-byte v12, v3, v11

    .line 112
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 114
    :cond_1
    const/4 v11, 0x0

    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 115
    .local v0, "Size":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    add-int/2addr v11, v2

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    .line 116
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->setRecordSize(I)V

    .line 118
    const/4 v2, 0x0

    :goto_2
    const/4 v11, 0x3

    if-gt v2, v11, :cond_2

    .line 119
    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->fileContent:[B

    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    add-int/2addr v12, v2

    aget-byte v11, v11, v12

    aput-byte v11, v3, v2

    .line 118
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 120
    :cond_2
    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v11

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v7

    .line 122
    .local v7, "xFrmM11":F
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    add-int/2addr v11, v2

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    .line 123
    invoke-static {v7}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->setxFormM11(F)V

    .line 125
    const/4 v2, 0x0

    :goto_3
    const/4 v11, 0x3

    if-gt v2, v11, :cond_3

    .line 126
    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->fileContent:[B

    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    add-int/2addr v12, v2

    aget-byte v11, v11, v12

    aput-byte v11, v3, v2

    .line 125
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 127
    :cond_3
    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v11

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v8

    .line 129
    .local v8, "xFrmM12":F
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    add-int/2addr v11, v2

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    .line 130
    invoke-static {v8}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->setxFormM12(F)V

    .line 132
    const/4 v2, 0x0

    :goto_4
    const/4 v11, 0x3

    if-gt v2, v11, :cond_4

    .line 133
    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->fileContent:[B

    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    add-int/2addr v12, v2

    aget-byte v11, v11, v12

    aput-byte v11, v3, v2

    .line 132
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 134
    :cond_4
    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v11

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v9

    .line 136
    .local v9, "xFrmM21":F
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    add-int/2addr v11, v2

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    .line 137
    invoke-static {v9}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->setxFormM21(F)V

    .line 139
    const/4 v2, 0x0

    :goto_5
    const/4 v11, 0x3

    if-gt v2, v11, :cond_5

    .line 140
    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->fileContent:[B

    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    add-int/2addr v12, v2

    aget-byte v11, v11, v12

    aput-byte v11, v3, v2

    .line 139
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 141
    :cond_5
    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v11

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v10

    .line 143
    .local v10, "xFrmM22":F
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    add-int/2addr v11, v2

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    .line 144
    invoke-static {v10}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->setxFormM22(F)V

    .line 146
    const/4 v2, 0x0

    :goto_6
    const/4 v11, 0x3

    if-gt v2, v11, :cond_6

    .line 147
    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->fileContent:[B

    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    add-int/2addr v12, v2

    aget-byte v11, v11, v12

    aput-byte v11, v3, v2

    .line 146
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 148
    :cond_6
    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v11

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v5

    .line 150
    .local v5, "xFrmDx":F
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    add-int/2addr v11, v2

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    .line 151
    invoke-static {v5}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedWidth(F)F

    move-result v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->setxFormDx(F)V

    .line 153
    const/4 v2, 0x0

    :goto_7
    const/4 v11, 0x3

    if-gt v2, v11, :cond_7

    .line 154
    iget-object v11, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->fileContent:[B

    iget v12, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    add-int/2addr v12, v2

    aget-byte v11, v11, v12

    aput-byte v11, v3, v2

    .line 153
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 155
    :cond_7
    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v11

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v6

    .line 157
    .local v6, "xFrmDy":F
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    add-int/2addr v11, v2

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    .line 158
    invoke-static {v6}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->getResizedHeight(F)F

    move-result v11

    invoke-virtual {p0, v11}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->setxFormDy(F)V

    .line 160
    const/4 v2, 0x0

    :goto_8
    const/4 v11, 0x3

    if-gt v2, v11, :cond_8

    .line 161
    rsub-int/lit8 v11, v2, 0x3

    iget-object v12, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->fileContent:[B

    iget v13, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    add-int/2addr v13, v2

    aget-byte v12, v12, v13

    aput-byte v12, v3, v11

    .line 160
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 162
    :cond_8
    const/4 v11, 0x0

    invoke-static {v3, v11}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v4

    .line 163
    .local v4, "modifyWrldTransformMode":I
    iget v11, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    add-int/2addr v11, v2

    iput v11, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->offset:I

    .line 164
    invoke-virtual {p0, v4}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->setmodifyWorldTransformMode(I)V

    .line 165
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "RecrdSize"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->Size:I

    .line 43
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "RecrdType"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->Type:I

    .line 35
    return-void
.end method

.method public setmodifyWorldTransformMode(I)V
    .locals 0
    .param p1, "modifyWrldTransformMode"    # I

    .prologue
    .line 98
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->modifyWorldTransformMode:I

    .line 99
    return-void
.end method

.method public setxFormDx(F)V
    .locals 0
    .param p1, "xFrmDx"    # F

    .prologue
    .line 82
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->xFormDx:F

    .line 83
    return-void
.end method

.method public setxFormDy(F)V
    .locals 0
    .param p1, "xFrmDy"    # F

    .prologue
    .line 90
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->xFormDy:F

    .line 91
    return-void
.end method

.method public setxFormM11(F)V
    .locals 0
    .param p1, "xFrmM11"    # F

    .prologue
    .line 50
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->xFormM11:F

    .line 51
    return-void
.end method

.method public setxFormM12(F)V
    .locals 0
    .param p1, "xFrmM12"    # F

    .prologue
    .line 58
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->xFormM12:F

    .line 59
    return-void
.end method

.method public setxFormM21(F)V
    .locals 0
    .param p1, "xFrmM21"    # F

    .prologue
    .line 66
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->xFormM21:F

    .line 67
    return-void
.end method

.method public setxFormM22(F)V
    .locals 0
    .param p1, "xFrmM22"    # F

    .prologue
    .line 74
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_ModifyWorldTransform;->xFormM22:F

    .line 75
    return-void
.end method
