.class Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler$StyleBasedOn;
.super Ljava/lang/Object;
.source "XDocStyleHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "StyleBasedOn"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;


# direct methods
.method constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler$StyleBasedOn;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V
    .locals 3
    .param p1, "value"    # Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;

    .prologue
    .line 139
    instance-of v1, p1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 140
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    .line 141
    .local v0, "basedOn":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler$StyleBasedOn;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;

    iget-object v1, v1, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleHandler;->style:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->setBasedOn(Ljava/lang/String;)V

    .line 143
    .end local v0    # "basedOn":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    :cond_0
    return-void
.end method
