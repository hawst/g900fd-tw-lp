.class public final enum Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;
.super Ljava/lang/Enum;
.source "GradientFill.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EGradientType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

.field public static final enum LINEAR:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

.field public static final enum PATH:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 110
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

    const-string/jumbo v1, "LINEAR"

    const-string/jumbo v2, "linear"

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;->LINEAR:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

    .line 114
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

    const-string/jumbo v1, "PATH"

    const-string/jumbo v2, "path"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;->PATH:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

    .line 106
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;->LINEAR:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;->PATH:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 118
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 119
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;->value:Ljava/lang/String;

    .line 120
    return-void
.end method

.method public static convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 127
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;->values()[Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 128
    .local v2, "inst":Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 132
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;
    :goto_1
    return-object v2

    .line 127
    .restart local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 132
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 106
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

    return-object v0
.end method


# virtual methods
.method public xmlValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;->value:Ljava/lang/String;

    return-object v0
.end method
