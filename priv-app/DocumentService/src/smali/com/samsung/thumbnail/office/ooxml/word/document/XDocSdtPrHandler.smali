.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtPrHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XDocSdtPrHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    const-string/jumbo v0, "sdtPr"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 15
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    .line 18
    const/4 v3, 0x1

    new-array v2, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 20
    .local v2, "seqDescriptors":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPartObjHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPartObjHandler;-><init>()V

    .line 21
    .local v1, "docPartObjHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocPartObjHandler;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v3, "docPartObj"

    invoke-direct {v0, v3, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 23
    .local v0, "docPartObj":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v3, 0x0

    aput-object v0, v2, v3

    .line 25
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtPrHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 26
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 32
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 33
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 39
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 40
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtPrHandler;->init()V

    .line 41
    return-void
.end method
