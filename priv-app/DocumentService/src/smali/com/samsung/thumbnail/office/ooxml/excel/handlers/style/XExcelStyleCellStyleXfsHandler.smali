.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellStyleXfsHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelStyleCellStyleXfsHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    const-string/jumbo v0, "cellStyleXfs"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 20
    return-void
.end method


# virtual methods
.method protected init()V
    .locals 3

    .prologue
    .line 24
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleXFHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleXFHandler;-><init>()V

    .line 25
    .local v0, "XfHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleXFHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleCellStyleXfsHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "xf"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 34
    return-void
.end method
