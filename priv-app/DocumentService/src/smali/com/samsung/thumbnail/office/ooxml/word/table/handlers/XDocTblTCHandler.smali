.class public Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCHandler;
.super Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;
.source "XDocTblTCHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$ITblCellPropObserver;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 19
    const-string/jumbo v1, "tc"

    const/16 v2, 0x64

    invoke-direct {p0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;-><init>(Ljava/lang/String;I)V

    .line 21
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;

    invoke-direct {v0, p0}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$ITblCellPropObserver;)V

    .line 22
    .local v0, "tcPrHandler":Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "tcPr"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 42
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->endCell()V

    .line 43
    return-void
.end method

.method public setTblCellProp(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "cellProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    .prologue
    .line 48
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->setTblCellProperty(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;)V

    .line 50
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 28
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocBodyBlockLvlElts;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 29
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->startCell()V

    .line 30
    return-void
.end method
