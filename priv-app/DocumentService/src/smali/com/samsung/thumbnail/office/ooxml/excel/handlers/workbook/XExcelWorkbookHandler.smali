.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelWorkbookHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelWorkbookHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    .line 12
    const-string/jumbo v5, "workbook"

    invoke-direct {p0, v5}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 14
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelFileversionHandler;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelFileversionHandler;-><init>()V

    .line 15
    .local v2, "fileversionHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelFileversionHandler;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelWorkbookHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v6, "fileVersion"

    invoke-virtual {v5, v6, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelWoorkBookPrHandler;

    invoke-direct {v4}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelWoorkBookPrHandler;-><init>()V

    .line 18
    .local v4, "workbookPrHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelWoorkBookPrHandler;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelWorkbookHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v6, "workbookPr"

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelBookViewsHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelBookViewsHandler;-><init>()V

    .line 21
    .local v0, "bookviewsHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelBookViewsHandler;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelWorkbookHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v6, "bookViews"

    invoke-virtual {v5, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelSheetsHandler;

    invoke-direct {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelSheetsHandler;-><init>()V

    .line 25
    .local v3, "sheetsHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelSheetsHandler;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelWorkbookHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v6, "sheets"

    invoke-virtual {v5, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelCalcPrHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelCalcPrHandler;-><init>()V

    .line 28
    .local v1, "calcPrHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelCalcPrHandler;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/workbook/XExcelWorkbookHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v6, "calcPr"

    invoke-virtual {v5, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    return-void
.end method
