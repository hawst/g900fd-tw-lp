.class public Lcom/samsung/thumbnail/office/ooxml/excel/table/XWPFExcelTableParser;
.super Ljava/lang/Object;
.source "XWPFExcelTableParser.java"


# instance fields
.field private extableColumnid:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private extableColumnname:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private extableColumnscount:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Landroid/content/Context;)V
    .locals 1
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/table/XWPFExcelTableParser;->extableColumnid:Ljava/util/ArrayList;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/table/XWPFExcelTableParser;->extableColumnname:Ljava/util/ArrayList;

    .line 57
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/table/XWPFExcelTableParser;->onTableRead(Ljava/io/InputStream;)V

    .line 59
    return-void
.end method

.method private parseExcel(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "is"    # Ljava/io/InputStream;

    .prologue
    .line 63
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/table/XExcelSheetTableParser;

    invoke-direct {v0, p0}, Lcom/samsung/thumbnail/office/ooxml/excel/table/XExcelSheetTableParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/excel/table/XWPFExcelTableParser;)V

    .line 65
    .local v0, "excelParser":Lcom/samsung/thumbnail/office/ooxml/excel/table/XExcelSheetTableParser;
    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/table/XExcelSheetTableParser;->parse(Ljava/io/InputStream;)V

    .line 66
    return-void
.end method


# virtual methods
.method public addExTableColumnID(Ljava/lang/String;)V
    .locals 1
    .param p1, "extableColumnid"    # Ljava/lang/String;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/table/XWPFExcelTableParser;->extableColumnid:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    return-void
.end method

.method public addExTableColumnName(Ljava/lang/String;)V
    .locals 1
    .param p1, "extableColumnname"    # Ljava/lang/String;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/table/XWPFExcelTableParser;->extableColumnname:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    return-void
.end method

.method public addExTableColumnscount(I)V
    .locals 0
    .param p1, "extableColumnscount"    # I

    .prologue
    .line 84
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/table/XWPFExcelTableParser;->extableColumnscount:I

    .line 85
    return-void
.end method

.method public getExTableColumnID()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/table/XWPFExcelTableParser;->extableColumnid:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExTableColumnName()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/table/XWPFExcelTableParser;->extableColumnname:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExTableColumnscount()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/table/XWPFExcelTableParser;->extableColumnscount:I

    return v0
.end method

.method protected onTableRead(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/table/XWPFExcelTableParser;->parseExcel(Ljava/io/InputStream;)V

    .line 77
    return-void
.end method
