.class public final enum Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;
.super Ljava/lang/Enum;
.source "ENumberFormat.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum AIUEO:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum AIUEO_FULL_WIDTH:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum ARABIC_ABJAD:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum ARABIC_ALPHA:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum BULLET:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum CARDINAL_TEXT:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum CHICAGO:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum CHINESE_COUNTING:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum CHINESE_COUNTING_THOUSAND:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum CHINESE_LEGAL_SIMPLIFIED:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum CHOSUNG:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum DECIMAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum DECIMAL_ENCLOSED_CIRCLE:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum DECIMAL_ENCLOSED_CIRCLE_CHINESE:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum DECIMAL_ENCLOSED_FULLSTOP:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum DECIMAL_ENCLOSED_PAREN:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum DECIMAL_FULL_WIDTH:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum DECIMAL_FULL_WIDTH2:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum DECIMAL_HALF_WIDTH:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum DECIMAL_ZERO:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum GANADA:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum HEBREW1:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum HEBREW2:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum HEX:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum HINDI_CONSONANTS:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum HINDI_COUNTING:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum HINDI_NUMBERS:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum HINDI_VOWELS:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum IDEOGRAPH_DIGITAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum IDEOGRAPH_ENCLOSED_CIRCLE:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum IDEOGRAPH_LEGAL_TRADITIONAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum IDEOGRAPH_TRADITIONAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum IDEOGRAPH_ZODIAC:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum IDEOGRAPH_ZODIAC_TRADITIONAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum IROHA:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum IROHA_FULL_WIDTH:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum JAPANESE_COUNTING:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum JAPANESE_DIGITAL_TEN_THOUSAND:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum JAPANESE_LEGAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum KOREAN_COUNTING:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum KOREAN_DIGITAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum KOREAN_DIGITAL2:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum KOREAN_LEGAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum LOWER_LETTER:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum LOWER_ROMAN:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum NONE:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum NUMBER_IN_DASH:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum ORDINAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum ORDINAL_TEXT:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum RUSSIAN_LOWER:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum RUSSIAN_UPPER:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum TAIWANESE_COUNTING:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum TAIWANESE_COUNTING_THOUSAND:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum TAIWANESE_DIGITAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum THAI_COUNTING:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum THAI_LETTERS:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum THAI_NUMBERS:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum UPPER_LETTER:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum UPPER_ROMAN:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field public static final enum VIETNAMESE_COUNTING:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 7
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "DECIMAL"

    const-string/jumbo v2, "decimal"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->DECIMAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 10
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "UPPER_ROMAN"

    const-string/jumbo v2, "upperRoman"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->UPPER_ROMAN:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 13
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "LOWER_ROMAN"

    const-string/jumbo v2, "lowerRoman"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->LOWER_ROMAN:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 16
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "UPPER_LETTER"

    const-string/jumbo v2, "upperLetter"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->UPPER_LETTER:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 19
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "LOWER_LETTER"

    const-string/jumbo v2, "lowerLetter"

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->LOWER_LETTER:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 22
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "ORDINAL"

    const/4 v2, 0x5

    const-string/jumbo v3, "ordinal"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->ORDINAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 25
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "CARDINAL_TEXT"

    const/4 v2, 0x6

    const-string/jumbo v3, "cardinalText"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->CARDINAL_TEXT:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 28
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "ORDINAL_TEXT"

    const/4 v2, 0x7

    const-string/jumbo v3, "ordinalText"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->ORDINAL_TEXT:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 31
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "HEX"

    const/16 v2, 0x8

    const-string/jumbo v3, "hex"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->HEX:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 34
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "CHICAGO"

    const/16 v2, 0x9

    const-string/jumbo v3, "chicago"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->CHICAGO:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 37
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "IDEOGRAPH_DIGITAL"

    const/16 v2, 0xa

    const-string/jumbo v3, "ideographDigital"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->IDEOGRAPH_DIGITAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 40
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "JAPANESE_COUNTING"

    const/16 v2, 0xb

    const-string/jumbo v3, "japaneseCounting"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->JAPANESE_COUNTING:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 43
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "AIUEO"

    const/16 v2, 0xc

    const-string/jumbo v3, "aiueo"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->AIUEO:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 46
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "IROHA"

    const/16 v2, 0xd

    const-string/jumbo v3, "iroha"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->IROHA:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 49
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "DECIMAL_FULL_WIDTH"

    const/16 v2, 0xe

    const-string/jumbo v3, "decimalFullWidth"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->DECIMAL_FULL_WIDTH:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 52
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "DECIMAL_HALF_WIDTH"

    const/16 v2, 0xf

    const-string/jumbo v3, "decimalHalfWidth"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->DECIMAL_HALF_WIDTH:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 55
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "JAPANESE_LEGAL"

    const/16 v2, 0x10

    const-string/jumbo v3, "japaneseLegal"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->JAPANESE_LEGAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 59
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "JAPANESE_DIGITAL_TEN_THOUSAND"

    const/16 v2, 0x11

    const-string/jumbo v3, "japaneseDigitalTenThousand"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->JAPANESE_DIGITAL_TEN_THOUSAND:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 63
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "DECIMAL_ENCLOSED_CIRCLE"

    const/16 v2, 0x12

    const-string/jumbo v3, "decimalEnclosedCircle"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->DECIMAL_ENCLOSED_CIRCLE:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 67
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "DECIMAL_FULL_WIDTH2"

    const/16 v2, 0x13

    const-string/jumbo v3, "decimalFullWidth2"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->DECIMAL_FULL_WIDTH2:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 70
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "AIUEO_FULL_WIDTH"

    const/16 v2, 0x14

    const-string/jumbo v3, "aiueoFullWidth"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->AIUEO_FULL_WIDTH:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 73
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "IROHA_FULL_WIDTH"

    const/16 v2, 0x15

    const-string/jumbo v3, "irohaFullWidth"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->IROHA_FULL_WIDTH:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 76
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "DECIMAL_ZERO"

    const/16 v2, 0x16

    const-string/jumbo v3, "decimalZero"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->DECIMAL_ZERO:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 79
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "BULLET"

    const/16 v2, 0x17

    const-string/jumbo v3, "bullet"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->BULLET:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 82
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "GANADA"

    const/16 v2, 0x18

    const-string/jumbo v3, "ganada"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->GANADA:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 85
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "CHOSUNG"

    const/16 v2, 0x19

    const-string/jumbo v3, "chosung"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->CHOSUNG:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 88
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "DECIMAL_ENCLOSED_FULLSTOP"

    const/16 v2, 0x1a

    const-string/jumbo v3, "decimalEnclosedFullstop"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->DECIMAL_ENCLOSED_FULLSTOP:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 92
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "DECIMAL_ENCLOSED_PAREN"

    const/16 v2, 0x1b

    const-string/jumbo v3, "decimalEnclosedParen"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->DECIMAL_ENCLOSED_PAREN:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 96
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "DECIMAL_ENCLOSED_CIRCLE_CHINESE"

    const/16 v2, 0x1c

    const-string/jumbo v3, "decimalEnclosedCircleChinese"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->DECIMAL_ENCLOSED_CIRCLE_CHINESE:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 100
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "IDEOGRAPH_ENCLOSED_CIRCLE"

    const/16 v2, 0x1d

    const-string/jumbo v3, "ideographEnclosedCircle"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->IDEOGRAPH_ENCLOSED_CIRCLE:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 104
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "IDEOGRAPH_TRADITIONAL"

    const/16 v2, 0x1e

    const-string/jumbo v3, "ideographTraditional"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->IDEOGRAPH_TRADITIONAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 107
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "IDEOGRAPH_ZODIAC"

    const/16 v2, 0x1f

    const-string/jumbo v3, "ideographZodiac"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->IDEOGRAPH_ZODIAC:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 110
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "IDEOGRAPH_ZODIAC_TRADITIONAL"

    const/16 v2, 0x20

    const-string/jumbo v3, "ideographZodiacTraditional"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->IDEOGRAPH_ZODIAC_TRADITIONAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 114
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "TAIWANESE_COUNTING"

    const/16 v2, 0x21

    const-string/jumbo v3, "taiwaneseCounting"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->TAIWANESE_COUNTING:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 118
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "IDEOGRAPH_LEGAL_TRADITIONAL"

    const/16 v2, 0x22

    const-string/jumbo v3, "ideographLegalTraditional"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->IDEOGRAPH_LEGAL_TRADITIONAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 122
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "TAIWANESE_COUNTING_THOUSAND"

    const/16 v2, 0x23

    const-string/jumbo v3, "taiwaneseCountingThousand"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->TAIWANESE_COUNTING_THOUSAND:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 126
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "TAIWANESE_DIGITAL"

    const/16 v2, 0x24

    const-string/jumbo v3, "taiwaneseDigital"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->TAIWANESE_DIGITAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 129
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "CHINESE_COUNTING"

    const/16 v2, 0x25

    const-string/jumbo v3, "chineseCounting"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->CHINESE_COUNTING:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 132
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "CHINESE_LEGAL_SIMPLIFIED"

    const/16 v2, 0x26

    const-string/jumbo v3, "chineseLegalSimplified"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->CHINESE_LEGAL_SIMPLIFIED:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 136
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "CHINESE_COUNTING_THOUSAND"

    const/16 v2, 0x27

    const-string/jumbo v3, "chineseCountingThousand"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->CHINESE_COUNTING_THOUSAND:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 140
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "KOREAN_DIGITAL"

    const/16 v2, 0x28

    const-string/jumbo v3, "koreanDigital"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->KOREAN_DIGITAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 143
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "KOREAN_COUNTING"

    const/16 v2, 0x29

    const-string/jumbo v3, "koreanCounting"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->KOREAN_COUNTING:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 146
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "KOREAN_LEGAL"

    const/16 v2, 0x2a

    const-string/jumbo v3, "koreanLegal"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->KOREAN_LEGAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 149
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "KOREAN_DIGITAL2"

    const/16 v2, 0x2b

    const-string/jumbo v3, "koreanDigital2"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->KOREAN_DIGITAL2:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 152
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "VIETNAMESE_COUNTING"

    const/16 v2, 0x2c

    const-string/jumbo v3, "vietnameseCounting"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->VIETNAMESE_COUNTING:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 155
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "RUSSIAN_LOWER"

    const/16 v2, 0x2d

    const-string/jumbo v3, "russianLower"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->RUSSIAN_LOWER:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 158
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "RUSSIAN_UPPER"

    const/16 v2, 0x2e

    const-string/jumbo v3, "russianUpper"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->RUSSIAN_UPPER:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 161
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "NONE"

    const/16 v2, 0x2f

    const-string/jumbo v3, "none"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 164
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "NUMBER_IN_DASH"

    const/16 v2, 0x30

    const-string/jumbo v3, "numberInDash"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->NUMBER_IN_DASH:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 167
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "HEBREW1"

    const/16 v2, 0x31

    const-string/jumbo v3, "hebrew1"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->HEBREW1:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 170
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "HEBREW2"

    const/16 v2, 0x32

    const-string/jumbo v3, "hebrew2"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->HEBREW2:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 173
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "ARABIC_ALPHA"

    const/16 v2, 0x33

    const-string/jumbo v3, "arabicAlpha"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->ARABIC_ALPHA:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 176
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "ARABIC_ABJAD"

    const/16 v2, 0x34

    const-string/jumbo v3, "arabicAbjad"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->ARABIC_ABJAD:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 179
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "HINDI_VOWELS"

    const/16 v2, 0x35

    const-string/jumbo v3, "hindiVowels"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->HINDI_VOWELS:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 182
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "HINDI_CONSONANTS"

    const/16 v2, 0x36

    const-string/jumbo v3, "hindiConsonants"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->HINDI_CONSONANTS:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 185
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "HINDI_NUMBERS"

    const/16 v2, 0x37

    const-string/jumbo v3, "hindiNumbers"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->HINDI_NUMBERS:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 188
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "HINDI_COUNTING"

    const/16 v2, 0x38

    const-string/jumbo v3, "hindiCounting"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->HINDI_COUNTING:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 191
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "THAI_LETTERS"

    const/16 v2, 0x39

    const-string/jumbo v3, "thaiLetters"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->THAI_LETTERS:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 194
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "THAI_NUMBERS"

    const/16 v2, 0x3a

    const-string/jumbo v3, "thaiNumbers"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->THAI_NUMBERS:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 197
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    const-string/jumbo v1, "THAI_COUNTING"

    const/16 v2, 0x3b

    const-string/jumbo v3, "thaiCounting"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->THAI_COUNTING:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 3
    const/16 v0, 0x3c

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->DECIMAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->UPPER_ROMAN:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->LOWER_ROMAN:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->UPPER_LETTER:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->LOWER_LETTER:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->ORDINAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->CARDINAL_TEXT:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->ORDINAL_TEXT:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->HEX:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->CHICAGO:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->IDEOGRAPH_DIGITAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->JAPANESE_COUNTING:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->AIUEO:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->IROHA:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->DECIMAL_FULL_WIDTH:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->DECIMAL_HALF_WIDTH:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->JAPANESE_LEGAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->JAPANESE_DIGITAL_TEN_THOUSAND:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->DECIMAL_ENCLOSED_CIRCLE:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->DECIMAL_FULL_WIDTH2:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->AIUEO_FULL_WIDTH:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->IROHA_FULL_WIDTH:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->DECIMAL_ZERO:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->BULLET:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->GANADA:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->CHOSUNG:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->DECIMAL_ENCLOSED_FULLSTOP:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->DECIMAL_ENCLOSED_PAREN:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->DECIMAL_ENCLOSED_CIRCLE_CHINESE:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->IDEOGRAPH_ENCLOSED_CIRCLE:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->IDEOGRAPH_TRADITIONAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->IDEOGRAPH_ZODIAC:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->IDEOGRAPH_ZODIAC_TRADITIONAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->TAIWANESE_COUNTING:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->IDEOGRAPH_LEGAL_TRADITIONAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->TAIWANESE_COUNTING_THOUSAND:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->TAIWANESE_DIGITAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->CHINESE_COUNTING:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->CHINESE_LEGAL_SIMPLIFIED:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->CHINESE_COUNTING_THOUSAND:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->KOREAN_DIGITAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->KOREAN_COUNTING:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->KOREAN_LEGAL:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->KOREAN_DIGITAL2:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->VIETNAMESE_COUNTING:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->RUSSIAN_LOWER:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->RUSSIAN_UPPER:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->NUMBER_IN_DASH:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->HEBREW1:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->HEBREW2:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->ARABIC_ALPHA:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->ARABIC_ABJAD:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->HINDI_VOWELS:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->HINDI_CONSONANTS:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->HINDI_NUMBERS:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->HINDI_COUNTING:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->THAI_LETTERS:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->THAI_NUMBERS:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->THAI_COUNTING:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 200
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 201
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->value:Ljava/lang/String;

    .line 202
    return-void
.end method

.method public static convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 209
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->values()[Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 210
    .local v2, "inst":Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 214
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;
    :goto_1
    return-object v2

    .line 209
    .restart local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 214
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    return-object v0
.end method


# virtual methods
.method public xmlValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->value:Ljava/lang/String;

    return-object v0
.end method
