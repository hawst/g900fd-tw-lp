.class public Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;
.super Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;
.source "WmfHeader.java"


# instance fields
.field private bottom:I

.field private height:I

.field private inch:I

.field private left:I

.field private mDwIsAldus:I

.field private mFileContent:[B

.field private mIsAlbus:Z

.field private mtHeaderSize:I

.field private mtMaxRecord:I

.field private mtNoObjects:I

.field private mtNoParameters:I

.field private mtSize:I

.field private mtType:I

.field private mtVersion:I

.field private right:I

.field private top:I

.field private width:I


# direct methods
.method public constructor <init>([BIZI)V
    .locals 0
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I
    .param p3, "isAlbus"    # Z
    .param p4, "dwIsAldus"    # I

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/AbstractWMFReader;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mFileContent:[B

    .line 35
    iput p2, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mOffset:I

    .line 36
    iput-boolean p3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mIsAlbus:Z

    .line 37
    iput p4, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mDwIsAldus:I

    .line 38
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->init()V

    .line 39
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 42
    iput v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->left:I

    .line 43
    iput v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->top:I

    .line 44
    iput v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->width:I

    .line 45
    iput v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->height:I

    .line 46
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->left:I

    iget v1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->width:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->right:I

    .line 47
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->top:I

    iget v1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->height:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->bottom:I

    .line 48
    return-void
.end method


# virtual methods
.method public getBottom()I
    .locals 1

    .prologue
    .line 131
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->bottom:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->height:I

    return v0
.end method

.method public getInch()I
    .locals 1

    .prologue
    .line 139
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->inch:I

    return v0
.end method

.method public getLeft()I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->left:I

    return v0
.end method

.method public getMtHeaderSize()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mtHeaderSize:I

    return v0
.end method

.method public getMtMaxRecord()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mtMaxRecord:I

    return v0
.end method

.method public getMtNoObjects()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mtNoObjects:I

    return v0
.end method

.method public getMtNoParameters()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mtNoParameters:I

    return v0
.end method

.method public getMtSize()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mtSize:I

    return v0
.end method

.method public getMtType()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mtType:I

    return v0
.end method

.method public getMtVersion()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mtVersion:I

    return v0
.end method

.method public getRight()I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->right:I

    return v0
.end method

.method public getTop()I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->top:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->width:I

    return v0
.end method

.method public readWMFHeader()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 163
    iget-boolean v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mIsAlbus:Z

    if-eqz v3, :cond_2

    .line 164
    iget-object v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mFileContent:[B

    invoke-virtual {p0, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->readShort([B)S

    .line 165
    iget-object v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mFileContent:[B

    invoke-virtual {p0, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->readShort([B)S

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->left:I

    .line 166
    iget-object v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mFileContent:[B

    invoke-virtual {p0, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->readShort([B)S

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->top:I

    .line 167
    iget-object v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mFileContent:[B

    invoke-virtual {p0, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->readShort([B)S

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->right:I

    .line 168
    iget-object v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mFileContent:[B

    invoke-virtual {p0, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->readShort([B)S

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->bottom:I

    .line 169
    iget-object v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mFileContent:[B

    invoke-virtual {p0, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->readShort([B)S

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->inch:I

    .line 170
    iget-object v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mFileContent:[B

    invoke-virtual {p0, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->readInt([B)I

    move-result v2

    .line 171
    .local v2, "reserved":I
    iget-object v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mFileContent:[B

    invoke-virtual {p0, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->readShort([B)S

    move-result v1

    .line 173
    .local v1, "checksum":S
    iget v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->left:I

    iget v4, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->right:I

    if-le v3, v4, :cond_0

    .line 174
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->right:I

    .line 175
    .local v0, "_i":I
    iget v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->left:I

    iput v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->right:I

    .line 176
    iput v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->left:I

    .line 179
    .end local v0    # "_i":I
    :cond_0
    iget v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->top:I

    iget v4, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->bottom:I

    if-le v3, v4, :cond_1

    .line 180
    iget v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->bottom:I

    .line 181
    .restart local v0    # "_i":I
    iget v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->top:I

    iput v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->bottom:I

    .line 182
    iput v0, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->top:I

    .line 186
    .end local v0    # "_i":I
    :cond_1
    iget v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->right:I

    iget v4, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->left:I

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->width:I

    .line 187
    iget v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->bottom:I

    iget v4, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->top:I

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->height:I

    .line 189
    iget-object v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mFileContent:[B

    invoke-virtual {p0, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->readShort([B)S

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mtType:I

    .line 190
    iget-object v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mFileContent:[B

    invoke-virtual {p0, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->readShort([B)S

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mtHeaderSize:I

    .line 196
    .end local v1    # "checksum":S
    .end local v2    # "reserved":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mFileContent:[B

    invoke-virtual {p0, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->readShort([B)S

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mtVersion:I

    .line 197
    iget-object v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mFileContent:[B

    invoke-virtual {p0, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->readInt([B)I

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mtSize:I

    .line 198
    iget-object v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mFileContent:[B

    invoke-virtual {p0, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->readShort([B)S

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mtNoObjects:I

    .line 199
    iget-object v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mFileContent:[B

    invoke-virtual {p0, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->readInt([B)I

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mtMaxRecord:I

    .line 200
    iget-object v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mFileContent:[B

    invoke-virtual {p0, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->readShort([B)S

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mtNoParameters:I

    .line 201
    return-void

    .line 192
    :cond_2
    iget v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mDwIsAldus:I

    shl-int/lit8 v3, v3, 0x10

    shr-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mtType:I

    .line 193
    iget v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mDwIsAldus:I

    shr-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mtHeaderSize:I

    goto :goto_0
.end method

.method public setBottom(I)V
    .locals 0
    .param p1, "bottom"    # I

    .prologue
    .line 135
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->bottom:I

    .line 136
    return-void
.end method

.method public setHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 159
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->height:I

    .line 160
    return-void
.end method

.method public setInch(I)V
    .locals 0
    .param p1, "inch"    # I

    .prologue
    .line 143
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->inch:I

    .line 144
    return-void
.end method

.method public setLeft(I)V
    .locals 0
    .param p1, "left"    # I

    .prologue
    .line 111
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->left:I

    .line 112
    return-void
.end method

.method public setMtHeaderSize(I)V
    .locals 0
    .param p1, "mtHeaderSize"    # I

    .prologue
    .line 63
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mtHeaderSize:I

    .line 64
    return-void
.end method

.method public setMtMaxRecord(I)V
    .locals 0
    .param p1, "mtMaxRecord"    # I

    .prologue
    .line 95
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mtMaxRecord:I

    .line 96
    return-void
.end method

.method public setMtNoObjects(I)V
    .locals 0
    .param p1, "mtNoObjects"    # I

    .prologue
    .line 87
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mtNoObjects:I

    .line 88
    return-void
.end method

.method public setMtNoParameters(I)V
    .locals 0
    .param p1, "mtNoParameters"    # I

    .prologue
    .line 103
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mtNoParameters:I

    .line 104
    return-void
.end method

.method public setMtSize(I)V
    .locals 0
    .param p1, "mtSize"    # I

    .prologue
    .line 79
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mtSize:I

    .line 80
    return-void
.end method

.method public setMtType(I)V
    .locals 0
    .param p1, "mtType"    # I

    .prologue
    .line 55
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mtType:I

    .line 56
    return-void
.end method

.method public setMtVersion(I)V
    .locals 0
    .param p1, "mtVersion"    # I

    .prologue
    .line 71
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->mtVersion:I

    .line 72
    return-void
.end method

.method public setRight(I)V
    .locals 0
    .param p1, "right"    # I

    .prologue
    .line 119
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->right:I

    .line 120
    return-void
.end method

.method public setTop(I)V
    .locals 0
    .param p1, "top"    # I

    .prologue
    .line 127
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->top:I

    .line 128
    return-void
.end method

.method public setWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 151
    iput p1, p0, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/WmfHeader;->width:I

    .line 152
    return-void
.end method
