.class public Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;
.super Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty;
.source "XDocIndentationProperty.java"


# instance fields
.field private firstLineIndent:I

.field private hangingIndent:I

.field private leftIndent:I

.field private rightIndent:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty;-><init>()V

    return-void
.end method


# virtual methods
.method public getFirstInd()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->firstLineIndent:I

    return v0
.end method

.method public getHangingInd()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->hangingIndent:I

    return v0
.end method

.method public getLeftInd()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->leftIndent:I

    return v0
.end method

.method public getRightInd()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->rightIndent:I

    return v0
.end method

.method public merge(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;)V
    .locals 1
    .param p1, "prop"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    .prologue
    .line 41
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->firstLineIndent:I

    if-nez v0, :cond_0

    .line 42
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->firstLineIndent:I

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->firstLineIndent:I

    .line 44
    :cond_0
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->hangingIndent:I

    if-nez v0, :cond_1

    .line 45
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->hangingIndent:I

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->hangingIndent:I

    .line 47
    :cond_1
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->leftIndent:I

    if-nez v0, :cond_2

    .line 48
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->leftIndent:I

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->leftIndent:I

    .line 50
    :cond_2
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->rightIndent:I

    if-nez v0, :cond_3

    .line 51
    iget v0, p1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->rightIndent:I

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->rightIndent:I

    .line 53
    :cond_3
    return-void
.end method

.method public setIndentation(IIII)V
    .locals 0
    .param p1, "firstLineIndent"    # I
    .param p2, "hangingIndent"    # I
    .param p3, "leftIndent"    # I
    .param p4, "rightIndent"    # I

    .prologue
    .line 14
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->firstLineIndent:I

    .line 15
    iput p2, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->hangingIndent:I

    .line 16
    iput p3, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->leftIndent:I

    .line 17
    iput p4, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->rightIndent:I

    .line 18
    return-void
.end method

.method public setLeftIndent(I)V
    .locals 0
    .param p1, "leftIndent"    # I

    .prologue
    .line 21
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->leftIndent:I

    .line 22
    return-void
.end method
