.class public Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;
.super Ljava/lang/Object;
.source "XWPFShapeProp.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "XWPFShapeProp"


# instance fields
.field private adj1:I

.field private adj2:I

.field private adj3:I

.field private adj4:I

.field private adj5:I

.field private adj6:I

.field private adj7:I

.field private adj8:I

.field private adjVal:I

.field private bodyPr:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;

.field private connectorType:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

.field private fillAlpha:J

.field private fillAlphaOff:J

.field private fillColor:Ljava/lang/String;

.field private fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

.field public fillRefId:Ljava/lang/String;

.field private fillType:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

.field private filledState:Z

.field private formulas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private gradFillColor:Ljava/lang/String;

.field private hrAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

.field private id:Ljava/lang/String;

.field private isDiagramShape:Z

.field private isGroupShape:Z

.field private lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

.field private lumMod:J

.field private lumOff:J

.field private path:Ljava/lang/String;

.field private pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

.field private pathProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;

.field private rotation:I

.field private satMod:J

.field private satOff:J

.field private sdRatio:F

.field protected shH:J

.field protected shW:J

.field protected shX:J

.field protected shY:J

.field private shade:J

.field private shadowColor:Ljava/lang/String;

.field private shapeType:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

.field private stringText:Ljava/lang/String;

.field private strokeColor:Ljava/lang/String;

.field private strokeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;

.field private strokeStatus:Z

.field private strokeWeight:Ljava/lang/String;

.field private style:Ljava/lang/String;

.field private textRotation:I

.field private tint:J

.field private type:Ljava/lang/String;

.field private xAnchorRef:Ljava/lang/String;

.field private yAnchorRef:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-boolean v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->filledState:Z

    .line 52
    iput-boolean v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->strokeStatus:Z

    .line 53
    iput-boolean v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->isDiagramShape:Z

    .line 54
    iput-boolean v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->isGroupShape:Z

    .line 56
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->lumMod:J

    .line 57
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->lumOff:J

    .line 58
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->tint:J

    .line 59
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->shade:J

    .line 60
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->satMod:J

    .line 61
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->satOff:J

    .line 62
    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->fillAlpha:J

    .line 63
    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->fillAlphaOff:J

    .line 68
    iput v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->rotation:I

    .line 69
    iput v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->textRotation:I

    .line 74
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->sdRatio:F

    .line 75
    iput v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adjVal:I

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->formulas:Ljava/util/ArrayList;

    .line 84
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V
    .locals 6
    .param p1, "shapeType"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    .prologue
    const-wide/16 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-boolean v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->filledState:Z

    .line 52
    iput-boolean v3, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->strokeStatus:Z

    .line 53
    iput-boolean v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->isDiagramShape:Z

    .line 54
    iput-boolean v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->isGroupShape:Z

    .line 56
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->lumMod:J

    .line 57
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->lumOff:J

    .line 58
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->tint:J

    .line 59
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->shade:J

    .line 60
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->satMod:J

    .line 61
    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->satOff:J

    .line 62
    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->fillAlpha:J

    .line 63
    iput-wide v4, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->fillAlphaOff:J

    .line 68
    iput v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->rotation:I

    .line 69
    iput v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->textRotation:I

    .line 74
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->sdRatio:F

    .line 75
    iput v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adjVal:I

    .line 78
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->shapeType:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->formulas:Ljava/util/ArrayList;

    .line 80
    return-void
.end method


# virtual methods
.method public addFormulas(Ljava/lang/String;)V
    .locals 1
    .param p1, "formula"    # Ljava/lang/String;

    .prologue
    .line 304
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->formulas:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 305
    return-void
.end method

.method public getAdj1()I
    .locals 1

    .prologue
    .line 256
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj1:I

    return v0
.end method

.method public getAdj2()I
    .locals 1

    .prologue
    .line 244
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj2:I

    return v0
.end method

.method public getAdj3()I
    .locals 1

    .prologue
    .line 240
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj3:I

    return v0
.end method

.method public getAdj4()I
    .locals 1

    .prologue
    .line 236
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj4:I

    return v0
.end method

.method public getAdj5()I
    .locals 1

    .prologue
    .line 232
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj5:I

    return v0
.end method

.method public getAdj6()I
    .locals 1

    .prologue
    .line 228
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj6:I

    return v0
.end method

.method public getAdj7()I
    .locals 1

    .prologue
    .line 224
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj7:I

    return v0
.end method

.method public getAdj8()I
    .locals 1

    .prologue
    .line 220
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj8:I

    return v0
.end method

.method public getAdjVal()I
    .locals 1

    .prologue
    .line 451
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adjVal:I

    return v0
.end method

.method public getConnectorType()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->connectorType:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    return-object v0
.end method

.method public getFillAlpha()J
    .locals 2

    .prologue
    .line 543
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->fillAlpha:J

    return-wide v0
.end method

.method public getFillAlphaOff()J
    .locals 2

    .prologue
    .line 551
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->fillAlphaOff:J

    return-wide v0
.end method

.method public getFillColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->fillColor:Ljava/lang/String;

    return-object v0
.end method

.method public getFillColorLumMod()J
    .locals 2

    .prologue
    .line 495
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->lumMod:J

    return-wide v0
.end method

.method public getFillColorLumOff()J
    .locals 2

    .prologue
    .line 503
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->lumOff:J

    return-wide v0
.end method

.method public getFillColorSatMod()J
    .locals 2

    .prologue
    .line 527
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->satMod:J

    return-wide v0
.end method

.method public getFillColorSatOff()J
    .locals 2

    .prologue
    .line 535
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->satOff:J

    return-wide v0
.end method

.method public getFillColorShade()J
    .locals 2

    .prologue
    .line 519
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->shade:J

    return-wide v0
.end method

.method public getFillColorTint()J
    .locals 2

    .prologue
    .line 511
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->tint:J

    return-wide v0
.end method

.method public getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    .locals 1

    .prologue
    .line 627
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    return-object v0
.end method

.method public getFillType()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->fillType:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    return-object v0
.end method

.method public getFilledStatus()Z
    .locals 1

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->filledState:Z

    return v0
.end method

.method public getFormulas()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 308
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->formulas:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getGradFillColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->gradFillColor:Ljava/lang/String;

    return-object v0
.end method

.method public getHrAlign()Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->hrAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getLineColorLumMod()J
    .locals 2

    .prologue
    .line 583
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->lumMod:J

    return-wide v0
.end method

.method public getLineColorLumOff()J
    .locals 2

    .prologue
    .line 591
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->lumOff:J

    return-wide v0
.end method

.method public getLineColorSatMod()J
    .locals 2

    .prologue
    .line 615
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->satMod:J

    return-wide v0
.end method

.method public getLineColorSatOff()J
    .locals 2

    .prologue
    .line 623
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->satOff:J

    return-wide v0
.end method

.method public getLineColorShade()J
    .locals 2

    .prologue
    .line 607
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->shade:J

    return-wide v0
.end method

.method public getLineColorTint()J
    .locals 2

    .prologue
    .line 599
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->tint:J

    return-wide v0
.end method

.method public getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    .locals 1

    .prologue
    .line 631
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->path:Ljava/lang/String;

    return-object v0
.end method

.method public getPathDescriptor()Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;
    .locals 1

    .prologue
    .line 575
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    return-object v0
.end method

.method public getPathProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->pathProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;

    return-object v0
.end method

.method public getRefId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->fillRefId:Ljava/lang/String;

    return-object v0
.end method

.method public getRotation()I
    .locals 1

    .prologue
    .line 417
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->rotation:I

    return v0
.end method

.method public getSDRatio()F
    .locals 1

    .prologue
    .line 647
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->sdRatio:F

    return v0
.end method

.method public getShadow()Ljava/lang/String;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->shadowColor:Ljava/lang/String;

    return-object v0
.end method

.method public getShapeHeight()J
    .locals 2

    .prologue
    .line 456
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->shH:J

    return-wide v0
.end method

.method public getShapeType()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->shapeType:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    return-object v0
.end method

.method public getShapeWidth()J
    .locals 2

    .prologue
    .line 461
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->shW:J

    return-wide v0
.end method

.method public getShapeXvalue()J
    .locals 2

    .prologue
    .line 466
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->shX:J

    return-wide v0
.end method

.method public getShapeYvalue()J
    .locals 2

    .prologue
    .line 471
    iget-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->shY:J

    return-wide v0
.end method

.method public getStringText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->stringText:Ljava/lang/String;

    return-object v0
.end method

.method public getStrokeColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->strokeColor:Ljava/lang/String;

    return-object v0
.end method

.method public getStrokeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->strokeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;

    return-object v0
.end method

.method public getStrokeStatus()Z
    .locals 1

    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->strokeStatus:Z

    return v0
.end method

.method public getStrokeWeight()Ljava/lang/String;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->strokeWeight:Ljava/lang/String;

    return-object v0
.end method

.method public getStyle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->style:Ljava/lang/String;

    return-object v0
.end method

.method public getTextRotation()I
    .locals 1

    .prologue
    .line 425
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->textRotation:I

    return v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->type:Ljava/lang/String;

    return-object v0
.end method

.method public getXWPFBodyProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->bodyPr:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;

    return-object v0
.end method

.method public getxAnchorRef()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->xAnchorRef:Ljava/lang/String;

    return-object v0
.end method

.method public getyAnchorRef()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->yAnchorRef:Ljava/lang/String;

    return-object v0
.end method

.method public isDiagramShape()Z
    .locals 1

    .prologue
    .line 483
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->isDiagramShape:Z

    return v0
.end method

.method public isGroupShape()Z
    .locals 1

    .prologue
    .line 475
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->isGroupShape:Z

    return v0
.end method

.method public merge(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V
    .locals 2
    .param p1, "shapeProp"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    .prologue
    .line 328
    if-nez p1, :cond_0

    .line 380
    :goto_0
    return-void

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->connectorType:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    if-nez v0, :cond_1

    .line 336
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getConnectorType()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->connectorType:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    .line 339
    :cond_1
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->style:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 340
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getStyle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->style:Ljava/lang/String;

    .line 341
    :cond_2
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->fillColor:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 342
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getFillColor()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->fillColor:Ljava/lang/String;

    .line 343
    :cond_3
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->strokeColor:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 344
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getStrokeColor()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->strokeColor:Ljava/lang/String;

    .line 345
    :cond_4
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->strokeWeight:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 346
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getStrokeWeight()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->strokeWeight:Ljava/lang/String;

    .line 347
    :cond_5
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->shadowColor:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 348
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getShadow()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->shadowColor:Ljava/lang/String;

    .line 349
    :cond_6
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->path:Ljava/lang/String;

    if-nez v0, :cond_7

    .line 350
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->path:Ljava/lang/String;

    .line 351
    :cond_7
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj1:I

    if-nez v0, :cond_8

    .line 352
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getAdj1()I

    move-result v0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj1:I

    .line 353
    :cond_8
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj2:I

    if-nez v0, :cond_9

    .line 354
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getAdj2()I

    move-result v0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj2:I

    .line 355
    :cond_9
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj3:I

    if-nez v0, :cond_a

    .line 356
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getAdj3()I

    move-result v0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj3:I

    .line 357
    :cond_a
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj4:I

    if-nez v0, :cond_b

    .line 358
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getAdj4()I

    move-result v0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj4:I

    .line 359
    :cond_b
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj5:I

    if-nez v0, :cond_c

    .line 360
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getAdj5()I

    move-result v0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj5:I

    .line 361
    :cond_c
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj6:I

    if-nez v0, :cond_d

    .line 362
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getAdj6()I

    move-result v0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj6:I

    .line 363
    :cond_d
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj7:I

    if-nez v0, :cond_e

    .line 364
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getAdj7()I

    move-result v0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj7:I

    .line 365
    :cond_e
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj8:I

    if-nez v0, :cond_f

    .line 366
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getAdj8()I

    move-result v0

    iput v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj8:I

    .line 368
    :cond_f
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->formulas:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_10

    .line 369
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getFormulas()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->formulas:Ljava/util/ArrayList;

    .line 372
    :cond_10
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->strokeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;

    if-nez v0, :cond_11

    .line 373
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getStrokeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->strokeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;

    .line 376
    :goto_1
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->pathProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;

    if-nez v0, :cond_12

    .line 377
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getPathProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->pathProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;

    goto/16 :goto_0

    .line 375
    :cond_11
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->strokeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getStrokeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->merge(Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;)V

    goto :goto_1

    .line 379
    :cond_12
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->pathProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getPathProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;->merge(Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;)V

    goto/16 :goto_0
.end method

.method public setAdj(II)V
    .locals 0
    .param p1, "adj"    # I
    .param p2, "pos"    # I

    .prologue
    .line 181
    packed-switch p2, :pswitch_data_0

    .line 209
    :goto_0
    return-void

    .line 183
    :pswitch_0
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj1:I

    goto :goto_0

    .line 186
    :pswitch_1
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj2:I

    goto :goto_0

    .line 189
    :pswitch_2
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj3:I

    goto :goto_0

    .line 192
    :pswitch_3
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj4:I

    goto :goto_0

    .line 195
    :pswitch_4
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj5:I

    goto :goto_0

    .line 198
    :pswitch_5
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj6:I

    goto :goto_0

    .line 201
    :pswitch_6
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj7:I

    goto :goto_0

    .line 204
    :pswitch_7
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adj8:I

    goto :goto_0

    .line 181
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public setAdjVal(I)V
    .locals 0
    .param p1, "adjVal"    # I

    .prologue
    .line 447
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->adjVal:I

    .line 448
    return-void
.end method

.method public setConnectorType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;)V
    .locals 0
    .param p1, "connectorType"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->connectorType:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EConnectorType;

    .line 158
    return-void
.end method

.method public setDiagramShape(Z)V
    .locals 0
    .param p1, "isDiagramShape"    # Z

    .prologue
    .line 487
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->isDiagramShape:Z

    .line 488
    return-void
.end method

.method public setFillAlpha(J)V
    .locals 1
    .param p1, "alpha"    # J

    .prologue
    .line 539
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->fillAlpha:J

    .line 540
    return-void
.end method

.method public setFillAlphaOff(J)V
    .locals 1
    .param p1, "alphaOff"    # J

    .prologue
    .line 547
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->fillAlphaOff:J

    .line 548
    return-void
.end method

.method public setFillColor(Ljava/lang/String;)V
    .locals 0
    .param p1, "fillColor"    # Ljava/lang/String;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->fillColor:Ljava/lang/String;

    .line 138
    return-void
.end method

.method public setFillColorLumMod(J)V
    .locals 1
    .param p1, "lumMod"    # J

    .prologue
    .line 491
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->lumMod:J

    .line 492
    return-void
.end method

.method public setFillColorLumOff(J)V
    .locals 1
    .param p1, "lumOff"    # J

    .prologue
    .line 499
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->lumOff:J

    .line 500
    return-void
.end method

.method public setFillColorSatMod(J)V
    .locals 1
    .param p1, "satMod"    # J

    .prologue
    .line 523
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->satMod:J

    .line 524
    return-void
.end method

.method public setFillColorSatOff(J)V
    .locals 1
    .param p1, "satOff"    # J

    .prologue
    .line 531
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->satOff:J

    .line 532
    return-void
.end method

.method public setFillColorShade(J)V
    .locals 1
    .param p1, "shade"    # J

    .prologue
    .line 515
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->shade:J

    .line 516
    return-void
.end method

.method public setFillColorTint(J)V
    .locals 1
    .param p1, "tint"    # J

    .prologue
    .line 507
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->tint:J

    .line 508
    return-void
.end method

.method public setFillType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;)V
    .locals 0
    .param p1, "fillType"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    .prologue
    .line 312
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->fillType:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    .line 313
    return-void
.end method

.method public setFilledStatus(Ljava/lang/String;)V
    .locals 1
    .param p1, "status"    # Ljava/lang/String;

    .prologue
    .line 128
    if-eqz p1, :cond_0

    const-string/jumbo v0, "f"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->filledState:Z

    .line 130
    :cond_0
    return-void
.end method

.method public setGradFillColor(Ljava/lang/String;)V
    .locals 0
    .param p1, "gradFillColor"    # Ljava/lang/String;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->gradFillColor:Ljava/lang/String;

    .line 142
    return-void
.end method

.method public setGroupShape(Z)V
    .locals 0
    .param p1, "isGroupShape"    # Z

    .prologue
    .line 479
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->isGroupShape:Z

    .line 480
    return-void
.end method

.method public setHrAlign(Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;)V
    .locals 0
    .param p1, "hrAlign"    # Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->hrAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    .line 117
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 324
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->id:Ljava/lang/String;

    .line 325
    return-void
.end method

.method public setLineColorLumMod(J)V
    .locals 1
    .param p1, "lumMod"    # J

    .prologue
    .line 579
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->lumMod:J

    .line 580
    return-void
.end method

.method public setLineColorLumOff(J)V
    .locals 1
    .param p1, "lumOff"    # J

    .prologue
    .line 587
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->lumOff:J

    .line 588
    return-void
.end method

.method public setLineColorSatMod(J)V
    .locals 1
    .param p1, "satMod"    # J

    .prologue
    .line 611
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->satMod:J

    .line 612
    return-void
.end method

.method public setLineColorSatOff(J)V
    .locals 1
    .param p1, "satOff"    # J

    .prologue
    .line 619
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->satOff:J

    .line 620
    return-void
.end method

.method public setLineColorShade(J)V
    .locals 1
    .param p1, "shade"    # J

    .prologue
    .line 603
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->shade:J

    .line 604
    return-void
.end method

.method public setLineColorTint(J)V
    .locals 1
    .param p1, "tint"    # J

    .prologue
    .line 595
    iput-wide p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->tint:J

    .line 596
    return-void
.end method

.method public setLineProps(Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V
    .locals 0
    .param p1, "lineProps"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .prologue
    .line 635
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->lineProps:Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .line 636
    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 169
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->path:Ljava/lang/String;

    .line 170
    return-void
.end method

.method public setPathDescriptor(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;)V
    .locals 0
    .param p1, "pathDesc"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    .prologue
    .line 571
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->pathDesc:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    .line 572
    return-void
.end method

.method public setPathProp(Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;)V
    .locals 0
    .param p1, "pathProp"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->pathProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/PathProp;

    .line 166
    return-void
.end method

.method public setPoints(JJ)V
    .locals 3
    .param p1, "x"    # J
    .param p3, "y"    # J

    .prologue
    .line 429
    invoke-static {p1, p2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->toPoints(J)D

    move-result-wide v0

    double-to-long v0, v0

    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->shX:J

    .line 430
    invoke-static {p3, p4}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->toPoints(J)D

    move-result-wide v0

    double-to-long v0, v0

    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->shY:J

    .line 433
    return-void
.end method

.method public setPrstName(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V
    .locals 0
    .param p1, "shapeType"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    .prologue
    .line 443
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->shapeType:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    .line 444
    return-void
.end method

.method public setRefId(Ljava/lang/String;)V
    .locals 0
    .param p1, "fillRefId"    # Ljava/lang/String;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->fillRefId:Ljava/lang/String;

    .line 113
    return-void
.end method

.method public setRotation(I)V
    .locals 0
    .param p1, "rotation"    # I

    .prologue
    .line 413
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->rotation:I

    .line 414
    return-void
.end method

.method public setSDRatio(F)V
    .locals 0
    .param p1, "sdRatio"    # F

    .prologue
    .line 643
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->sdRatio:F

    .line 644
    return-void
.end method

.method public setShadow(Ljava/lang/String;)V
    .locals 0
    .param p1, "shadowColor"    # Ljava/lang/String;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->shadowColor:Ljava/lang/String;

    .line 154
    return-void
.end method

.method public setShapeFillProps(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 0
    .param p1, "fillProperties"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 639
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->fillProperties:Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .line 640
    return-void
.end method

.method public setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V
    .locals 0
    .param p1, "shapeType"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    .prologue
    .line 212
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->shapeType:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    .line 213
    return-void
.end method

.method public setSize(JJ)V
    .locals 3
    .param p1, "w"    # J
    .param p3, "h"    # J

    .prologue
    .line 436
    long-to-float v0, p1

    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints1(F)F

    move-result v0

    float-to-long v0, v0

    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->shW:J

    .line 437
    long-to-float v0, p3

    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints1(F)F

    move-result v0

    float-to-long v0, v0

    iput-wide v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->shH:J

    .line 440
    return-void
.end method

.method public setStringText(Ljava/lang/String;)V
    .locals 0
    .param p1, "stringText"    # Ljava/lang/String;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->stringText:Ljava/lang/String;

    .line 178
    return-void
.end method

.method public setStrokeColor(Ljava/lang/String;)V
    .locals 0
    .param p1, "strokeColor"    # Ljava/lang/String;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->strokeColor:Ljava/lang/String;

    .line 146
    return-void
.end method

.method public setStrokeProp(Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;)V
    .locals 0
    .param p1, "strokeProp"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->strokeProp:Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;

    .line 162
    return-void
.end method

.method public setStrokeStatus(Ljava/lang/String;)V
    .locals 1
    .param p1, "status"    # Ljava/lang/String;

    .prologue
    .line 87
    if-eqz p1, :cond_0

    const-string/jumbo v0, "f"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->strokeStatus:Z

    .line 89
    :cond_0
    return-void
.end method

.method public setStrokeWeight(Ljava/lang/String;)V
    .locals 0
    .param p1, "strokeWeight"    # Ljava/lang/String;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->strokeWeight:Ljava/lang/String;

    .line 150
    return-void
.end method

.method public setStyle(Ljava/lang/String;)V
    .locals 0
    .param p1, "style"    # Ljava/lang/String;

    .prologue
    .line 296
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->style:Ljava/lang/String;

    .line 297
    return-void
.end method

.method public setTextRotation(I)V
    .locals 0
    .param p1, "textRotation"    # I

    .prologue
    .line 421
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->textRotation:I

    .line 422
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 216
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->type:Ljava/lang/String;

    .line 217
    return-void
.end method

.method public setXWPFBodyProp(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;)V
    .locals 0
    .param p1, "bodyPr"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;

    .prologue
    .line 405
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->bodyPr:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp$XWPFBodyProperty;

    .line 406
    return-void
.end method

.method public setxAnchorRef(Ljava/lang/String;)V
    .locals 0
    .param p1, "xAnchorRef"    # Ljava/lang/String;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->xAnchorRef:Ljava/lang/String;

    .line 97
    return-void
.end method

.method public setyAnchorRef(Ljava/lang/String;)V
    .locals 0
    .param p1, "yAnchorRef"    # Ljava/lang/String;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->yAnchorRef:Ljava/lang/String;

    .line 105
    return-void
.end method
