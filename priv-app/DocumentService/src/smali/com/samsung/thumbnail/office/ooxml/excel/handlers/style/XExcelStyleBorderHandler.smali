.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelStyleBorderHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    .line 19
    const-string/jumbo v5, "border"

    invoke-direct {p0, v5}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 21
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderLeftHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderLeftHandler;-><init>()V

    .line 22
    .local v1, "leftHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderLeftHandler;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v6, "left"

    invoke-virtual {v5, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderRightHandler;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderRightHandler;-><init>()V

    .line 24
    .local v2, "rightHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderRightHandler;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v6, "right"

    invoke-virtual {v5, v6, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderTopHandler;

    invoke-direct {v4}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderTopHandler;-><init>()V

    .line 26
    .local v4, "topHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderTopHandler;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v6, "top"

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderBottomHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderBottomHandler;-><init>()V

    .line 28
    .local v0, "bottomHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderBottomHandler;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v6, "bottom"

    invoke-virtual {v5, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderDiagonalHandler;

    invoke-direct {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderDiagonalHandler;-><init>()V

    .line 30
    .local v3, "schemeHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderDiagonalHandler;
    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleBorderHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v6, "diagonal"

    invoke-virtual {v5, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 45
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v0

    .line 47
    .local v0, "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    iget v1, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tblbordercount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tblbordercount:I

    .line 48
    return-void
.end method
