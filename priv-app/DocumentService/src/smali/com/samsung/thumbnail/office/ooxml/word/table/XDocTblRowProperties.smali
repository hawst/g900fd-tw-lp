.class public Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;
.super Ljava/lang/Object;
.source "XDocTblRowProperties.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;
    }
.end annotation


# instance fields
.field private cnfStyle:Ljava/lang/String;

.field private hRule:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

.field private height:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    return-void
.end method


# virtual methods
.method public getCNFStyle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;->cnfStyle:Ljava/lang/String;

    return-object v0
.end method

.method public getHRule()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;->hRule:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;->height:I

    return v0
.end method

.method public setCNFStyle(Ljava/lang/String;)V
    .locals 0
    .param p1, "cnfStyle"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;->cnfStyle:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public setHRule(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;)V
    .locals 0
    .param p1, "hRule"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

    .prologue
    .line 16
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;->hRule:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

    .line 17
    return-void
.end method

.method public setHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 12
    iput p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;->height:I

    .line 13
    return-void
.end method
