.class public Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;
.super Ljava/lang/Object;
.source "StrokeProp.java"


# instance fields
.field private color:Ljava/lang/String;

.field private joinStyle:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

.field private lineEndType:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

.field private lineStyle:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

.field private strokWeight:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    return-void
.end method


# virtual methods
.method public getStrokeColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->color:Ljava/lang/String;

    return-object v0
.end method

.method public getStrokeEndType()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->lineEndType:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    return-object v0
.end method

.method public getStrokeJoinStyle()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->joinStyle:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

    return-object v0
.end method

.method public getStrokeStyle()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->lineStyle:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    return-object v0
.end method

.method public getStrokeWeight()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->strokWeight:Ljava/lang/String;

    return-object v0
.end method

.method public merge(Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;)V
    .locals 1
    .param p1, "strokeProp"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;

    .prologue
    .line 61
    if-nez p1, :cond_1

    .line 74
    :cond_0
    :goto_0
    return-void

    .line 64
    :cond_1
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->color:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 65
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->getStrokeColor()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->color:Ljava/lang/String;

    .line 66
    :cond_2
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->strokWeight:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 67
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->getStrokeWeight()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->strokWeight:Ljava/lang/String;

    .line 68
    :cond_3
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->lineStyle:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    if-nez v0, :cond_4

    .line 69
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->getStrokeStyle()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->lineStyle:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    .line 70
    :cond_4
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->lineEndType:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    if-nez v0, :cond_5

    .line 71
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->getStrokeEndType()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->lineEndType:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    .line 72
    :cond_5
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->joinStyle:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

    if-nez v0, :cond_0

    .line 73
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->getStrokeJoinStyle()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->joinStyle:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

    goto :goto_0
.end method

.method public setStrokeColor(Ljava/lang/String;)V
    .locals 0
    .param p1, "strokeColor"    # Ljava/lang/String;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->color:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public setStrokeEndType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;)V
    .locals 0
    .param p1, "lineEndType"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->lineEndType:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeArrowType;

    .line 34
    return-void
.end method

.method public setStrokeJoinStyle(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;)V
    .locals 0
    .param p1, "joinStyle"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->joinStyle:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeJoinStyle;

    .line 38
    return-void
.end method

.method public setStrokeStyle(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;)V
    .locals 0
    .param p1, "strokeStyle"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->lineStyle:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EStrokeLineStyle;

    .line 30
    return-void
.end method

.method public setStrokeWeight(Ljava/lang/String;)V
    .locals 0
    .param p1, "strokeWeight"    # Ljava/lang/String;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/StrokeProp;->strokWeight:Ljava/lang/String;

    .line 26
    return-void
.end method
