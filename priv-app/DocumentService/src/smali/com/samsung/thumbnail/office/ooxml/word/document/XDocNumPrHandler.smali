.class public Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XDocNumPrHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler$XNumId;,
        Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler$XIlvl;
    }
.end annotation


# instance fields
.field private numberPropery:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;

.field private propery:Lcom/samsung/thumbnail/office/ooxml/word/properties/IXDocPropery;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/properties/IXDocPropery;)V
    .locals 8
    .param p1, "propery"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/IXDocPropery;

    .prologue
    .line 27
    const-string/jumbo v7, "numPr"

    invoke-direct {p0, v7}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 28
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler;->propery:Lcom/samsung/thumbnail/office/ooxml/word/properties/IXDocPropery;

    .line 30
    const/4 v7, 0x2

    new-array v6, v7, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 32
    .local v6, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler$XIlvl;

    invoke-direct {v1, p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler$XIlvl;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler;)V

    .line 33
    .local v1, "ilvl":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler$XIlvl;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;

    const-string/jumbo v7, "ilvl"

    invoke-direct {v0, v7, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 35
    .local v0, "ilvelHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v7, "ilvl"

    invoke-direct {v2, v7, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 37
    .local v2, "ilvlSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v7, 0x0

    aput-object v2, v6, v7

    .line 40
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler$XNumId;

    invoke-direct {v3, p0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler$XNumId;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler;)V

    .line 41
    .local v3, "numId":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler$XNumId;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;

    const-string/jumbo v7, "numId"

    invoke-direct {v4, v7, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;)V

    .line 43
    .local v4, "numIdHandler":Lcom/samsung/thumbnail/office/ooxml/OOXMLIntegerHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;

    const-string/jumbo v7, "numId"

    invoke-direct {v5, v7, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 45
    .local v5, "numIdSeq":Lcom/samsung/thumbnail/office/ooxml/word/XDocSequenceDescriptor;
    const/4 v7, 0x1

    aput-object v5, v6, v7

    .line 48
    iput-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 49
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler;)Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler;->numberPropery:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;

    return-object v0
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler;->propery:Lcom/samsung/thumbnail/office/ooxml/word/properties/IXDocPropery;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler;->numberPropery:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;

    if-nez v0, :cond_1

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler;->propery:Lcom/samsung/thumbnail/office/ooxml/word/properties/IXDocPropery;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler;->numberPropery:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;

    invoke-interface {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/IXDocPropery;->consumePropery(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty;)V

    goto :goto_0
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 54
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocNumPrHandler;->numberPropery:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;

    .line 55
    return-void
.end method
