.class public Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillPropHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "XPPTSldBckFillPropHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;


# instance fields
.field private background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

.field private picShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

.field private shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;)V
    .locals 9
    .param p1, "background"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    .prologue
    const/4 v8, 0x0

    .line 30
    const-string/jumbo v7, "bgPr"

    invoke-direct {p0, v7}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 31
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillPropHandler;->background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    .line 33
    const/4 v7, 0x3

    new-array v2, v7, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 35
    .local v2, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;

    invoke-direct {v3, p0, v8, v8}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 37
    .local v3, "solidFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v7, "solidFill"

    invoke-direct {v4, v7, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 39
    .local v4, "solidFillSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v7, 0x0

    aput-object v4, v2, v7

    .line 41
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;

    invoke-direct {v5, v8}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 42
    .local v5, "spprNoFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v7, "noFill"

    invoke-direct {v6, v7, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 44
    .local v6, "spprNoFillSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v7, 0x1

    aput-object v6, v2, v7

    .line 46
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler;

    const/16 v7, 0x1f

    invoke-direct {v0, v7, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler$IBlipFill;)V

    .line 48
    .local v0, "blipFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/BlipFillHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v7, "blipFill"

    invoke-direct {v1, v7, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 50
    .local v1, "blipFillSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v7, 0x2

    aput-object v1, v2, v7

    .line 66
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillPropHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 67
    return-void
.end method


# virtual methods
.method public consumeFill(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 1
    .param p1, "fillProperties"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillPropHandler;->background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillPropHandler;->background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->setFillProperties(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V

    .line 97
    :cond_0
    return-void
.end method

.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 89
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 90
    return-void
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 72
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 74
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillPropHandler;->background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    if-nez v1, :cond_1

    .line 85
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    const/16 v1, 0x1f

    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v1

    invoke-static {p2, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillPropHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)Ljava/lang/String;

    move-result-object v0

    .line 80
    .local v0, "ele":Ljava/lang/String;
    const-string/jumbo v1, "blipFill"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 81
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillPropHandler;->background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->setFillType(I)V

    goto :goto_0

    .line 82
    :cond_2
    const-string/jumbo v1, "gradFill"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 83
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillPropHandler;->background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->setFillType(I)V

    goto :goto_0
.end method

.method public setBlipEmbed(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 3
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "blipEmbed"    # Ljava/lang/String;

    .prologue
    .line 101
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    .line 102
    .local v0, "slide":Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillPropHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 103
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillPropHandler;->shapeProperties:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-direct {v1, v2, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;)V

    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillPropHandler;->picShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

    .line 104
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillPropHandler;->picShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

    invoke-virtual {v1, p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->setBlipEmbed(Ljava/lang/String;)V

    .line 106
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillPropHandler;->background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    if-eqz v1, :cond_0

    .line 107
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillPropHandler;->background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillPropHandler;->picShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->setBlipFill(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;)V

    .line 109
    :cond_0
    return-void
.end method

.method public setTile(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "flip"    # Ljava/lang/String;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillPropHandler;->picShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillPropHandler;->picShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->setTile()V

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillPropHandler;->background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;->setFillType(I)V

    .line 117
    return-void
.end method
