.class public Lcom/samsung/thumbnail/office/point/shapes/Callout3;
.super Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;
.source "Callout3.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mDrawOnCanvas:Z

.field private mLeftMarg:I

.field private mTopMarg:I

.field public newBitmapHeight:I

.field public newBitmapWidth:I

.field public subFactor_Height:I

.field public subFactor_Width:I


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 2
    .param p1, "folderPath"    # Ljava/io/File;

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;-><init>()V

    .line 24
    const-string/jumbo v0, "Callout3"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->TAG:Ljava/lang/String;

    .line 25
    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    .line 26
    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    .line 27
    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->newBitmapHeight:I

    .line 28
    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->newBitmapWidth:I

    .line 29
    iput-boolean v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mDrawOnCanvas:Z

    .line 34
    iput-object p1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->folderPath:Ljava/io/File;

    .line 35
    return-void
.end method


# virtual methods
.method protected draw(I)V
    .locals 28
    .param p1, "shapeCount"    # I

    .prologue
    .line 158
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->shapeName:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 436
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->width:F

    const v4, 0x46a8c000    # 21600.0f

    div-float v24, v3, v4

    .line 162
    .local v24, "mulFactor_Width":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->height:F

    const v4, 0x46a8c000    # 21600.0f

    div-float v23, v3, v4

    .line 185
    .local v23, "mulFactor_Height":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval0:I

    if-nez v3, :cond_2

    .line 186
    const/16 v3, 0x5b68

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval0:I

    .line 187
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval1:I

    if-nez v3, :cond_3

    .line 188
    const/16 v3, 0x5f50

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval1:I

    .line 189
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval2:I

    if-nez v3, :cond_4

    .line 190
    const/16 v3, 0x6270

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval2:I

    .line 191
    :cond_4
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval3:I

    if-nez v3, :cond_5

    .line 192
    const/16 v3, 0x5460

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval3:I

    .line 193
    :cond_5
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval4:I

    if-nez v3, :cond_6

    .line 194
    const/16 v3, 0x6270

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval4:I

    .line 195
    :cond_6
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval5:I

    if-nez v3, :cond_7

    .line 196
    const/16 v3, 0xfd2

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval5:I

    .line 197
    :cond_7
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval6:I

    if-nez v3, :cond_8

    .line 198
    const/16 v3, 0x5b68

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval6:I

    .line 199
    :cond_8
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval7:I

    if-nez v3, :cond_9

    .line 200
    const/16 v3, 0xfd2

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval7:I

    .line 202
    :cond_9
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval0:I

    .line 203
    .local v9, "calloutadjval0":I
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval1:I

    .line 204
    .local v10, "calloutadjval1":I
    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval2:I

    .line 205
    .local v11, "calloutadjval2":I
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval3:I

    .line 206
    .local v12, "calloutadjval3":I
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval4:I

    .line 207
    .local v13, "calloutadjval4":I
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval5:I

    .line 208
    .local v14, "calloutadjval5":I
    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval6:I

    .line 209
    .local v15, "calloutadjval6":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval7:I

    move/from16 v16, v0

    .line 212
    .local v16, "calloutadjval7":I
    invoke-static {v9, v11}, Ljava/lang/Math;->min(II)I

    move-result v21

    .line 213
    .local v21, "minX":I
    move/from16 v0, v21

    invoke-static {v0, v13}, Ljava/lang/Math;->min(II)I

    move-result v21

    .line 214
    move/from16 v0, v21

    invoke-static {v0, v15}, Ljava/lang/Math;->min(II)I

    move-result v21

    .line 215
    const/4 v3, 0x0

    move/from16 v0, v21

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v21

    .line 216
    invoke-static {v9, v11}, Ljava/lang/Math;->max(II)I

    move-result v19

    .line 217
    .local v19, "maxX":I
    move/from16 v0, v19

    invoke-static {v0, v13}, Ljava/lang/Math;->max(II)I

    move-result v19

    .line 218
    move/from16 v0, v19

    invoke-static {v0, v15}, Ljava/lang/Math;->max(II)I

    move-result v19

    .line 219
    const/16 v3, 0x5460

    move/from16 v0, v19

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v19

    .line 220
    move/from16 v0, v21

    int-to-float v3, v0

    mul-float v3, v3, v24

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    .line 221
    move/from16 v0, v19

    int-to-float v3, v0

    mul-float v3, v3, v24

    move/from16 v0, v21

    int-to-float v4, v0

    mul-float v4, v4, v24

    sub-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->newBitmapWidth:I

    .line 224
    invoke-static {v10, v12}, Ljava/lang/Math;->min(II)I

    move-result v22

    .line 225
    .local v22, "minY":I
    move/from16 v0, v22

    invoke-static {v0, v14}, Ljava/lang/Math;->min(II)I

    move-result v22

    .line 226
    move/from16 v0, v22

    move/from16 v1, v16

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v22

    .line 227
    const/4 v3, 0x0

    move/from16 v0, v22

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v22

    .line 228
    invoke-static {v10, v12}, Ljava/lang/Math;->max(II)I

    move-result v20

    .line 229
    .local v20, "maxY":I
    move/from16 v0, v20

    invoke-static {v0, v14}, Ljava/lang/Math;->max(II)I

    move-result v20

    .line 230
    move/from16 v0, v20

    move/from16 v1, v16

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v20

    .line 231
    const/16 v3, 0x5460

    move/from16 v0, v20

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v20

    .line 232
    move/from16 v0, v22

    int-to-float v3, v0

    mul-float v3, v3, v23

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    .line 233
    move/from16 v0, v20

    int-to-float v3, v0

    mul-float v3, v3, v23

    move/from16 v0, v22

    int-to-float v4, v0

    mul-float v4, v4, v23

    sub-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->newBitmapHeight:I

    .line 235
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->newBitmapWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->newBitmapHeight:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 240
    .local v2, "_bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "Callout3"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 241
    new-instance v17, Landroid/graphics/Canvas;

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 243
    .local v17, "canvas":Landroid/graphics/Canvas;
    new-instance v26, Landroid/graphics/Path;

    invoke-direct/range {v26 .. v26}, Landroid/graphics/Path;-><init>()V

    .line 245
    .local v26, "path":Landroid/graphics/Path;
    int-to-float v3, v9

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v10

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 247
    int-to-float v3, v11

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v12

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 249
    int-to-float v3, v13

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v14

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 251
    int-to-float v3, v15

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    move/from16 v0, v16

    int-to-float v4, v0

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 253
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_a

    .line 254
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 256
    :cond_a
    new-instance v26, Landroid/graphics/Path;

    .end local v26    # "path":Landroid/graphics/Path;
    invoke-direct/range {v26 .. v26}, Landroid/graphics/Path;-><init>()V

    .line 257
    .restart local v26    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 259
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 261
    const/4 v3, 0x0

    mul-float v3, v3, v24

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v23

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 262
    const/4 v3, 0x0

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 264
    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Path;->close()V

    .line 266
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_b

    .line 267
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 271
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x4593a800    # 4725.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 272
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 273
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x46b89200    # 23625.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 274
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    .line 403
    .end local v17    # "canvas":Landroid/graphics/Canvas;
    .end local v26    # "path":Landroid/graphics/Path;
    :cond_c
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mDrawOnCanvas:Z

    if-nez v3, :cond_0

    .line 404
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 406
    .local v7, "mtx":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->flipHorizontal:Z

    if-eqz v3, :cond_19

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->flipVertical:Z

    if-nez v3, :cond_19

    .line 407
    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 416
    :cond_d
    :goto_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->rotation:I

    int-to-float v3, v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    invoke-virtual {v7, v3, v4, v5}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 419
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v27

    .line 423
    .local v27, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v25, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->folderPath:Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "pic_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->shapeName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ".png"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 429
    .local v25, "out":Ljava/io/FileOutputStream;
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    move-object/from16 v0, v27

    move-object/from16 v1, v25

    invoke-virtual {v0, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 430
    invoke-virtual/range {v25 .. v25}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 431
    .end local v25    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v18

    .line 432
    .local v18, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception while writing pictures to out file"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 277
    .end local v7    # "mtx":Landroid/graphics/Matrix;
    .end local v18    # "e":Ljava/lang/Exception;
    .end local v27    # "rotatedBMP":Landroid/graphics/Bitmap;
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "BorderCallout3"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 278
    new-instance v17, Landroid/graphics/Canvas;

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 280
    .restart local v17    # "canvas":Landroid/graphics/Canvas;
    new-instance v26, Landroid/graphics/Path;

    invoke-direct/range {v26 .. v26}, Landroid/graphics/Path;-><init>()V

    .line 282
    .restart local v26    # "path":Landroid/graphics/Path;
    int-to-float v3, v9

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v10

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 284
    int-to-float v3, v11

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v12

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 286
    int-to-float v3, v13

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v14

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 288
    int-to-float v3, v15

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    move/from16 v0, v16

    int-to-float v4, v0

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 290
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_f

    .line 291
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 293
    :cond_f
    new-instance v26, Landroid/graphics/Path;

    .end local v26    # "path":Landroid/graphics/Path;
    invoke-direct/range {v26 .. v26}, Landroid/graphics/Path;-><init>()V

    .line 294
    .restart local v26    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 296
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 298
    const/4 v3, 0x0

    mul-float v3, v3, v24

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v23

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 299
    const/4 v3, 0x0

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 301
    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Path;->close()V

    .line 303
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_10

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_10

    .line 304
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 305
    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_11

    .line 306
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 310
    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x4593a800    # 4725.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 311
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 312
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x46b89200    # 23625.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 313
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 316
    .end local v17    # "canvas":Landroid/graphics/Canvas;
    .end local v26    # "path":Landroid/graphics/Path;
    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "AccentCallout3"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 317
    new-instance v17, Landroid/graphics/Canvas;

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 319
    .restart local v17    # "canvas":Landroid/graphics/Canvas;
    new-instance v26, Landroid/graphics/Path;

    invoke-direct/range {v26 .. v26}, Landroid/graphics/Path;-><init>()V

    .line 322
    .restart local v26    # "path":Landroid/graphics/Path;
    int-to-float v3, v9

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v10

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 324
    int-to-float v3, v11

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v12

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 326
    int-to-float v3, v13

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v14

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 328
    int-to-float v3, v15

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    move/from16 v0, v16

    int-to-float v4, v0

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 330
    int-to-float v3, v15

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 332
    int-to-float v3, v15

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 334
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_13

    .line 335
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 337
    :cond_13
    new-instance v26, Landroid/graphics/Path;

    .end local v26    # "path":Landroid/graphics/Path;
    invoke-direct/range {v26 .. v26}, Landroid/graphics/Path;-><init>()V

    .line 338
    .restart local v26    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 340
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 342
    const/4 v3, 0x0

    mul-float v3, v3, v24

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v23

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 343
    const/4 v3, 0x0

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 345
    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Path;->close()V

    .line 347
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_14

    .line 348
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 352
    :cond_14
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x4593a800    # 4725.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 353
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 354
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x46b89200    # 23625.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 355
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 358
    .end local v17    # "canvas":Landroid/graphics/Canvas;
    .end local v26    # "path":Landroid/graphics/Path;
    :cond_15
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "AccentBorderCallout3"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 359
    new-instance v17, Landroid/graphics/Canvas;

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 361
    .restart local v17    # "canvas":Landroid/graphics/Canvas;
    new-instance v26, Landroid/graphics/Path;

    invoke-direct/range {v26 .. v26}, Landroid/graphics/Path;-><init>()V

    .line 364
    .restart local v26    # "path":Landroid/graphics/Path;
    int-to-float v3, v9

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v10

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 366
    int-to-float v3, v11

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v12

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 368
    int-to-float v3, v13

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v14

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 370
    int-to-float v3, v15

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    move/from16 v0, v16

    int-to-float v4, v0

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 372
    int-to-float v3, v15

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 374
    int-to-float v3, v15

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 376
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_16

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_16

    .line 377
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 379
    :cond_16
    new-instance v26, Landroid/graphics/Path;

    .end local v26    # "path":Landroid/graphics/Path;
    invoke-direct/range {v26 .. v26}, Landroid/graphics/Path;-><init>()V

    .line 380
    .restart local v26    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 382
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 384
    const/4 v3, 0x0

    mul-float v3, v3, v24

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v23

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 385
    const/4 v3, 0x0

    mul-float v3, v3, v24

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 387
    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Path;->close()V

    .line 389
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_17

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_17

    .line 390
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 391
    :cond_17
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_18

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_18

    .line 392
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 396
    :cond_18
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x4593a800    # 4725.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 397
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 398
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x46b89200    # 23625.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 399
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 409
    .end local v17    # "canvas":Landroid/graphics/Canvas;
    .end local v26    # "path":Landroid/graphics/Path;
    .restart local v7    # "mtx":Landroid/graphics/Matrix;
    :cond_19
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->flipVertical:Z

    if-eqz v3, :cond_1a

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->flipHorizontal:Z

    if-nez v3, :cond_1a

    .line 410
    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, -0x40800000    # -1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    goto/16 :goto_2

    .line 412
    :cond_1a
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->flipHorizontal:Z

    if-eqz v3, :cond_d

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->flipVertical:Z

    if-eqz v3, :cond_d

    .line 413
    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v4, -0x40800000    # -1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    goto/16 :goto_2
.end method

.method protected draw(Landroid/graphics/Canvas;I)V
    .locals 27
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeCount"    # I

    .prologue
    .line 440
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->shapeName:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 718
    :cond_0
    :goto_0
    return-void

    .line 443
    :cond_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->width:F

    const v4, 0x46a8c000    # 21600.0f

    div-float v23, v3, v4

    .line 444
    .local v23, "mulFactor_Width":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->height:F

    const v4, 0x46a8c000    # 21600.0f

    div-float v22, v3, v4

    .line 467
    .local v22, "mulFactor_Height":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval0:I

    if-nez v3, :cond_2

    .line 468
    const/16 v3, 0x5b68

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval0:I

    .line 469
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval1:I

    if-nez v3, :cond_3

    .line 470
    const/16 v3, 0x5f50

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval1:I

    .line 471
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval2:I

    if-nez v3, :cond_4

    .line 472
    const/16 v3, 0x6270

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval2:I

    .line 473
    :cond_4
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval3:I

    if-nez v3, :cond_5

    .line 474
    const/16 v3, 0x5460

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval3:I

    .line 475
    :cond_5
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval4:I

    if-nez v3, :cond_6

    .line 476
    const/16 v3, 0x6270

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval4:I

    .line 477
    :cond_6
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval5:I

    if-nez v3, :cond_7

    .line 478
    const/16 v3, 0xfd2

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval5:I

    .line 479
    :cond_7
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval6:I

    if-nez v3, :cond_8

    .line 480
    const/16 v3, 0x5b68

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval6:I

    .line 481
    :cond_8
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval7:I

    if-nez v3, :cond_9

    .line 482
    const/16 v3, 0xfd2

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval7:I

    .line 484
    :cond_9
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval0:I

    .line 485
    .local v9, "calloutadjval0":I
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval1:I

    .line 486
    .local v10, "calloutadjval1":I
    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval2:I

    .line 487
    .local v11, "calloutadjval2":I
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval3:I

    .line 488
    .local v12, "calloutadjval3":I
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval4:I

    .line 489
    .local v13, "calloutadjval4":I
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval5:I

    .line 490
    .local v14, "calloutadjval5":I
    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval6:I

    .line 491
    .local v15, "calloutadjval6":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->adjval7:I

    move/from16 v16, v0

    .line 494
    .local v16, "calloutadjval7":I
    invoke-static {v9, v11}, Ljava/lang/Math;->min(II)I

    move-result v20

    .line 495
    .local v20, "minX":I
    move/from16 v0, v20

    invoke-static {v0, v13}, Ljava/lang/Math;->min(II)I

    move-result v20

    .line 496
    move/from16 v0, v20

    invoke-static {v0, v15}, Ljava/lang/Math;->min(II)I

    move-result v20

    .line 497
    const/4 v3, 0x0

    move/from16 v0, v20

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v20

    .line 498
    invoke-static {v9, v11}, Ljava/lang/Math;->max(II)I

    move-result v18

    .line 499
    .local v18, "maxX":I
    move/from16 v0, v18

    invoke-static {v0, v13}, Ljava/lang/Math;->max(II)I

    move-result v18

    .line 500
    move/from16 v0, v18

    invoke-static {v0, v15}, Ljava/lang/Math;->max(II)I

    move-result v18

    .line 501
    const/16 v3, 0x5460

    move/from16 v0, v18

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v18

    .line 502
    move/from16 v0, v20

    int-to-float v3, v0

    mul-float v3, v3, v23

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    .line 503
    move/from16 v0, v18

    int-to-float v3, v0

    mul-float v3, v3, v23

    move/from16 v0, v20

    int-to-float v4, v0

    mul-float v4, v4, v23

    sub-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->newBitmapWidth:I

    .line 506
    invoke-static {v10, v12}, Ljava/lang/Math;->min(II)I

    move-result v21

    .line 507
    .local v21, "minY":I
    move/from16 v0, v21

    invoke-static {v0, v14}, Ljava/lang/Math;->min(II)I

    move-result v21

    .line 508
    move/from16 v0, v21

    move/from16 v1, v16

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v21

    .line 509
    const/4 v3, 0x0

    move/from16 v0, v21

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v21

    .line 510
    invoke-static {v10, v12}, Ljava/lang/Math;->max(II)I

    move-result v19

    .line 511
    .local v19, "maxY":I
    move/from16 v0, v19

    invoke-static {v0, v14}, Ljava/lang/Math;->max(II)I

    move-result v19

    .line 512
    move/from16 v0, v19

    move/from16 v1, v16

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v19

    .line 513
    const/16 v3, 0x5460

    move/from16 v0, v19

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v19

    .line 514
    move/from16 v0, v21

    int-to-float v3, v0

    mul-float v3, v3, v22

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    .line 515
    move/from16 v0, v19

    int-to-float v3, v0

    mul-float v3, v3, v22

    move/from16 v0, v21

    int-to-float v4, v0

    mul-float v4, v4, v22

    sub-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->newBitmapHeight:I

    .line 517
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->newBitmapWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->newBitmapHeight:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 522
    .local v2, "_bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "Callout3"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 525
    new-instance v25, Landroid/graphics/Path;

    invoke-direct/range {v25 .. v25}, Landroid/graphics/Path;-><init>()V

    .line 527
    .local v25, "path":Landroid/graphics/Path;
    int-to-float v3, v9

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v10

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 529
    int-to-float v3, v11

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v12

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 531
    int-to-float v3, v13

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v14

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 533
    int-to-float v3, v15

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    move/from16 v0, v16

    int-to-float v4, v0

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 535
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_a

    .line 536
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 538
    :cond_a
    new-instance v25, Landroid/graphics/Path;

    .end local v25    # "path":Landroid/graphics/Path;
    invoke-direct/range {v25 .. v25}, Landroid/graphics/Path;-><init>()V

    .line 539
    .restart local v25    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 541
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 543
    const/4 v3, 0x0

    mul-float v3, v3, v23

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v22

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 544
    const/4 v3, 0x0

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 546
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Path;->close()V

    .line 548
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_b

    .line 549
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 553
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x4593a800    # 4725.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 554
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 555
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x46b89200    # 23625.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 556
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    .line 685
    .end local v25    # "path":Landroid/graphics/Path;
    :cond_c
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mDrawOnCanvas:Z

    if-nez v3, :cond_0

    .line 686
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 688
    .local v7, "mtx":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->flipHorizontal:Z

    if-eqz v3, :cond_19

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->flipVertical:Z

    if-nez v3, :cond_19

    .line 689
    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 698
    :cond_d
    :goto_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->rotation:I

    int-to-float v3, v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    invoke-virtual {v7, v3, v4, v5}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 701
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v26

    .line 705
    .local v26, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v24, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->folderPath:Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "pic_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->shapeName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ".png"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 711
    .local v24, "out":Ljava/io/FileOutputStream;
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    move-object/from16 v0, v26

    move-object/from16 v1, v24

    invoke-virtual {v0, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 712
    invoke-virtual/range {v24 .. v24}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 713
    .end local v24    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v17

    .line 714
    .local v17, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception while writing pictures to out file"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 559
    .end local v7    # "mtx":Landroid/graphics/Matrix;
    .end local v17    # "e":Ljava/lang/Exception;
    .end local v26    # "rotatedBMP":Landroid/graphics/Bitmap;
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "BorderCallout3"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 562
    new-instance v25, Landroid/graphics/Path;

    invoke-direct/range {v25 .. v25}, Landroid/graphics/Path;-><init>()V

    .line 564
    .restart local v25    # "path":Landroid/graphics/Path;
    int-to-float v3, v9

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v10

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 566
    int-to-float v3, v11

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v12

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 568
    int-to-float v3, v13

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v14

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 570
    int-to-float v3, v15

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    move/from16 v0, v16

    int-to-float v4, v0

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 572
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_f

    .line 573
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 575
    :cond_f
    new-instance v25, Landroid/graphics/Path;

    .end local v25    # "path":Landroid/graphics/Path;
    invoke-direct/range {v25 .. v25}, Landroid/graphics/Path;-><init>()V

    .line 576
    .restart local v25    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 578
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 580
    const/4 v3, 0x0

    mul-float v3, v3, v23

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v22

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 581
    const/4 v3, 0x0

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 583
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Path;->close()V

    .line 585
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_10

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_10

    .line 586
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 587
    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_11

    .line 588
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 592
    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x4593a800    # 4725.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 593
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 594
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x46b89200    # 23625.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 595
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 598
    .end local v25    # "path":Landroid/graphics/Path;
    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "AccentCallout3"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 601
    new-instance v25, Landroid/graphics/Path;

    invoke-direct/range {v25 .. v25}, Landroid/graphics/Path;-><init>()V

    .line 604
    .restart local v25    # "path":Landroid/graphics/Path;
    int-to-float v3, v9

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v10

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 606
    int-to-float v3, v11

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v12

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 608
    int-to-float v3, v13

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v14

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 610
    int-to-float v3, v15

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    move/from16 v0, v16

    int-to-float v4, v0

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 612
    int-to-float v3, v15

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 614
    int-to-float v3, v15

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 616
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_13

    .line 617
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 619
    :cond_13
    new-instance v25, Landroid/graphics/Path;

    .end local v25    # "path":Landroid/graphics/Path;
    invoke-direct/range {v25 .. v25}, Landroid/graphics/Path;-><init>()V

    .line 620
    .restart local v25    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 622
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 624
    const/4 v3, 0x0

    mul-float v3, v3, v23

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v22

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 625
    const/4 v3, 0x0

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 627
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Path;->close()V

    .line 629
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_14

    .line 630
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 634
    :cond_14
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x4593a800    # 4725.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 635
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 636
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x46b89200    # 23625.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 637
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 640
    .end local v25    # "path":Landroid/graphics/Path;
    :cond_15
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->shapeName:Ljava/lang/String;

    const-string/jumbo v4, "AccentBorderCallout3"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 643
    new-instance v25, Landroid/graphics/Path;

    invoke-direct/range {v25 .. v25}, Landroid/graphics/Path;-><init>()V

    .line 646
    .restart local v25    # "path":Landroid/graphics/Path;
    int-to-float v3, v9

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v10

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 648
    int-to-float v3, v11

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v12

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 650
    int-to-float v3, v13

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    int-to-float v4, v14

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 652
    int-to-float v3, v15

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    move/from16 v0, v16

    int-to-float v4, v0

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 654
    int-to-float v3, v15

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 656
    int-to-float v3, v15

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 658
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_16

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_16

    .line 659
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 661
    :cond_16
    new-instance v25, Landroid/graphics/Path;

    .end local v25    # "path":Landroid/graphics/Path;
    invoke-direct/range {v25 .. v25}, Landroid/graphics/Path;-><init>()V

    .line 662
    .restart local v25    # "path":Landroid/graphics/Path;
    const/4 v3, 0x0

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 664
    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 666
    const/4 v3, 0x0

    mul-float v3, v3, v23

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v22

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 667
    const/4 v3, 0x0

    mul-float v3, v3, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Width:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    mul-float v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->subFactor_Height:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 669
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Path;->close()V

    .line 671
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_17

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintFill:Landroid/graphics/Paint;

    if-eqz v3, :cond_17

    .line 672
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 673
    :cond_17
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v3, :cond_18

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintLine:Landroid/graphics/Paint;

    if-eqz v3, :cond_18

    .line 674
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 678
    :cond_18
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x4593a800    # 4725.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 679
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x4428c000    # 675.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 680
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x46b89200    # 23625.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->width:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 681
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->textArea:Landroid/graphics/RectF;

    const v4, 0x46a37a00    # 20925.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->height:F

    mul-float/2addr v4, v5

    const v5, 0x46a8c000    # 21600.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 691
    .end local v25    # "path":Landroid/graphics/Path;
    .restart local v7    # "mtx":Landroid/graphics/Matrix;
    :cond_19
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->flipVertical:Z

    if-eqz v3, :cond_1a

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->flipHorizontal:Z

    if-nez v3, :cond_1a

    .line 692
    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, -0x40800000    # -1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    goto/16 :goto_2

    .line 694
    :cond_1a
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->flipHorizontal:Z

    if-eqz v3, :cond_d

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->flipVertical:Z

    if-eqz v3, :cond_d

    .line 695
    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v4, -0x40800000    # -1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    goto/16 :goto_2
.end method

.method public drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F

    .prologue
    .line 112
    invoke-super {p0, p3, p4, p7, p8}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IFF)V

    .line 113
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mDrawOnCanvas:Z

    .line 114
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mLeftMarg:I

    .line 115
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mTopMarg:I

    .line 116
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 117
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->draw(Landroid/graphics/Canvas;I)V

    .line 118
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 119
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mDrawOnCanvas:Z

    .line 120
    return-void
.end method

.method public drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p2, "shapeCount"    # I

    .prologue
    .line 103
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V

    .line 105
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->draw(I)V

    .line 106
    return-void
.end method

.method public drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 89
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;I)V

    .line 90
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mDrawOnCanvas:Z

    .line 91
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mLeftMarg:I

    .line 92
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mTopMarg:I

    .line 93
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->width:F

    .line 94
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->height:F

    .line 95
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 96
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->draw(Landroid/graphics/Canvas;I)V

    .line 97
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mDrawOnCanvas:Z

    .line 99
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V
    .locals 0
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 39
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 41
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->draw(I)V

    .line 43
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V
    .locals 2
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I

    .prologue
    .line 47
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mDrawOnCanvas:Z

    .line 49
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mLeftMarg:I

    .line 50
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mTopMarg:I

    .line 51
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 52
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->draw(Landroid/graphics/Canvas;I)V

    .line 53
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mDrawOnCanvas:Z

    .line 55
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 134
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 135
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mDrawOnCanvas:Z

    .line 136
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mLeftMarg:I

    .line 137
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mTopMarg:I

    .line 138
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 139
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->draw(Landroid/graphics/Canvas;I)V

    .line 140
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 141
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mDrawOnCanvas:Z

    .line 142
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 146
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V

    .line 147
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mDrawOnCanvas:Z

    .line 148
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mLeftMarg:I

    .line 149
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mTopMarg:I

    .line 150
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 151
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->draw(Landroid/graphics/Canvas;I)V

    .line 152
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 153
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mDrawOnCanvas:Z

    .line 154
    return-void
.end method

.method public drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .prologue
    .line 127
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 129
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->draw(I)V

    .line 130
    return-void
.end method

.method public drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 60
    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p7

    move v4, p8

    move-object v5, p9

    invoke-super/range {v0 .. v5}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSSExcelShape(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mDrawOnCanvas:Z

    .line 63
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mLeftMarg:I

    .line 64
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mTopMarg:I

    .line 65
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 66
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->draw(Landroid/graphics/Canvas;I)V

    .line 67
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mDrawOnCanvas:Z

    .line 69
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 73
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 74
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mDrawOnCanvas:Z

    .line 75
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mLeftMarg:I

    .line 76
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mTopMarg:I

    .line 77
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->width:F

    .line 78
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->height:F

    .line 79
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 80
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->draw(Landroid/graphics/Canvas;I)V

    .line 81
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 82
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->mDrawOnCanvas:Z

    .line 83
    return-void
.end method
