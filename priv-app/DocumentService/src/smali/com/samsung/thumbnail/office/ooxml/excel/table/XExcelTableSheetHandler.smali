.class public Lcom/samsung/thumbnail/office/ooxml/excel/table/XExcelTableSheetHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelTableSheetHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 15
    const/16 v1, 0xc8

    const-string/jumbo v2, "table"

    invoke-direct {p0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 18
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/table/XExceTableColumnsHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/table/XExceTableColumnsHandler;-><init>()V

    .line 19
    .local v0, "tableColumnsHandler":Lcom/samsung/thumbnail/office/ooxml/excel/table/XExceTableColumnsHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/table/XExcelTableSheetHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "tableColumns"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 27
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 44
    return-void
.end method
