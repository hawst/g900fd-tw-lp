.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringRprHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelSharedStringRprHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 10

    .prologue
    .line 26
    const-string/jumbo v8, "rPr"

    invoke-direct {p0, v8}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 28
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStrBoldHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStrBoldHandler;-><init>()V

    .line 29
    .local v0, "bHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStrBoldHandler;
    iget-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringRprHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v9, "b"

    invoke-virtual {v8, v9, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringVertAlignHandler;

    invoke-direct {v7}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringVertAlignHandler;-><init>()V

    .line 31
    .local v7, "vertAlignHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringVertAlignHandler;
    iget-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringRprHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v9, "vertAlign"

    invoke-virtual {v8, v9, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStrStyleSiZeHandler;

    invoke-direct {v6}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStrStyleSiZeHandler;-><init>()V

    .line 34
    .local v6, "szHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStrStyleSiZeHandler;
    iget-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringRprHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v9, "sz"

    invoke-virtual {v8, v9, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedstringFontColorHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedstringFontColorHandler;-><init>()V

    .line 36
    .local v1, "colorHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedstringFontColorHandler;
    iget-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringRprHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v9, "color"

    invoke-virtual {v8, v9, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelStyleRFontHandler;

    invoke-direct {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelStyleRFontHandler;-><init>()V

    .line 38
    .local v3, "rfontHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelStyleRFontHandler;
    iget-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringRprHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v9, "rFont"

    invoke-virtual {v8, v9, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStrStyleUnderlineHandler;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStrStyleUnderlineHandler;-><init>()V

    .line 40
    .local v2, "familyHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStrStyleUnderlineHandler;
    iget-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringRprHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v9, "u"

    invoke-virtual {v8, v9, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStrStyleItalicHandler;

    invoke-direct {v4}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStrStyleItalicHandler;-><init>()V

    .line 42
    .local v4, "schemeHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStrStyleItalicHandler;
    iget-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringRprHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v9, "i"

    invoke-virtual {v8, v9, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontStrikeHandler;

    invoke-direct {v5}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontStrikeHandler;-><init>()V

    .line 44
    .local v5, "strikeHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleFontStrikeHandler;
    iget-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sharedstrings/XExcelSharedStringRprHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v9, "strike"

    invoke-virtual {v8, v9, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 5
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 55
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 57
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v0

    .line 61
    .local v0, "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget v3, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    invoke-virtual {v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->addRprStyles(Ljava/lang/Boolean;I)V

    .line 65
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringStyleBold()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_0

    .line 67
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    invoke-virtual {v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->addExSharedStringStyleBold(Ljava/lang/String;I)V

    .line 72
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringStyleItalic()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_1

    .line 74
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    invoke-virtual {v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->addExSharedStringStyleItalic(Ljava/lang/String;I)V

    .line 79
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringStyleUnderline()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_2

    .line 81
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    invoke-virtual {v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->addExSharedStringStyleUnderline(Ljava/lang/String;I)V

    .line 87
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringStyleStrike()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_3

    .line 89
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    invoke-virtual {v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->addExSharedStringStyleStrike(Ljava/lang/String;I)V

    .line 93
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringStyleFontname()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_4

    .line 95
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    invoke-virtual {v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->addExSharedStringStyleFontname(Ljava/lang/String;I)V

    .line 99
    :cond_4
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringStyleFontSize()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_5

    .line 101
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    invoke-virtual {v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->addExSharedStringStyleFontSize(Ljava/lang/String;I)V

    .line 105
    :cond_5
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringTextColorRGB()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_6

    .line 107
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    invoke-virtual {v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->addExSharedStringTextColorRGB(Ljava/lang/String;I)V

    .line 111
    :cond_6
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringTextColorTheme()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_7

    .line 113
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    invoke-virtual {v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->addExSharedStringTextColorTheme(Ljava/lang/String;I)V

    .line 118
    :cond_7
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringTextColorTint()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_8

    .line 120
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    invoke-virtual {v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->addExSharedStringTextColorTint(Ljava/lang/String;I)V

    .line 124
    :cond_8
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getStyleTextVerticalAlign()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_9

    .line 127
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedString;->getCurrentSI()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v1

    iget v2, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->sharedstyleIndex:I

    invoke-virtual {v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->addStyleTextVerticalAlign(Ljava/lang/String;I)V

    .line 132
    :cond_9
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 51
    return-void
.end method
