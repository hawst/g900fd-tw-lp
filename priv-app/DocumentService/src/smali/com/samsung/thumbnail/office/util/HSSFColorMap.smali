.class public Lcom/samsung/thumbnail/office/util/HSSFColorMap;
.super Ljava/lang/Object;
.source "HSSFColorMap.java"


# static fields
.field private static final AQUA:I = 0x3

.field private static final AUTOMATIC:I = 0x1

.field private static final BLACK:I = 0x0

.field private static final BLACT_TEXT1_35PRCNT:I = 0x17

.field private static final BLACT_TEXT1_50PRCNT:I = 0x1c

.field private static final BLUE:I = 0x2

.field private static final BLUE_ACCENT:I = 0xf

.field private static final BLUE_LIGHT:I = 0xa

.field public static final CENTER_ACROSS_SELECTION_ALIGN:I = 0x6

.field public static final CENTER_ALIGN:I = 0x1

.field private static final DARK_BLUE:I = 0x9

.field private static final DARK_RED:I = 0xd

.field private static final DARK_SLATE:I = 0x15

.field public static final FILL_ALIGN:I = 0x4

.field private static final GREEN:I = 0xb

.field private static final GREEN_HONEYDEW:I = 0x2a

.field public static final JUSTIFY_ALIGN:I = 0x3

.field public static final JUSTIFY_LEFT_ALIGN:I = 0x5

.field public static final LEFT_ALIGN:I = 0x0

.field private static final LIGHT_BLUE:I = 0x28

.field private static final LIGHT_GRAY:I = 0x16

.field private static final MAROON_4:I = 0xc

.field private static final PALE_BLUE:I = 0x2c

.field private static final PURPLE:I = 0x5

.field private static final RED_ACCENT_2:I = 0x6

.field private static final RED_ACCENT_2_30PRCNT:I = 0x2d

.field private static final RED_ACCENT_2_40PRCNT:I = 0x1d

.field public static final RIGHT_ALIGN:I = 0x2

.field private static final SANDY_BROWN:I = 0x34

.field private static final SKY_LIGHT_BLUE:I = 0x1b

.field private static final SNOW:I = 0x37

.field private static final TABLE_CELL:I = 0x33

.field private static final TABLE_HEADER:I = 0x32

.field private static final TAN:I = 0x8

.field private static final TAN_1:I = 0x1a

.field private static final THISTLE:I = 0x2e

.field private static final WHEAT:I = 0x2f

.field private static final WHITE:I = 0x14

.field private static final YELLOW:I = 0x7

.field private static alignMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static colorMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 66
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    .line 67
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->alignMap:Ljava/util/Map;

    .line 70
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    .line 71
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    invoke-static {v5}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#0000FF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0xf

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#333399"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0xc

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#7D0552"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0xd

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#800000"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    invoke-static {v4}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#000000"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    invoke-static {v3}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#000000"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x8

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#EEECE1"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x9

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#1F497D"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/4 v1, 0x6

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#C0504D"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/4 v1, 0x7

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#FFFF00"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    invoke-static {v6}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#4BACC6"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0xb

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#00AC50"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0xa

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#0070C0"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    invoke-static {v7}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#7030A0"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x14

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#FFFFFF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x15

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#2f4f4f"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x16

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#d3d3d3"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x37

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#8b8989"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x17

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#5A5A5A"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x1a

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#EEECE1"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x1b

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#f0ffff"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x1c

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#808080"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x1d

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#D99793"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x28

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#00B0F0"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x2a

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#e0eee0"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x2c

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#8DB4E3"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x2d

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#FFC7CE"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x2f

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#f5deb3"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x34

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#f4a460"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x32

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#F7BE81"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x33

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#F5F6CE"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    const/16 v1, 0x2e

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "#d8bfd8"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->alignMap:Ljava/util/Map;

    .line 109
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->alignMap:Ljava/util/Map;

    invoke-static {v3}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "left"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->alignMap:Ljava/util/Map;

    invoke-static {v4}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "center"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->alignMap:Ljava/util/Map;

    invoke-static {v5}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "right"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->alignMap:Ljava/util/Map;

    invoke-static {v6}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "justify"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->alignMap:Ljava/util/Map;

    const/4 v1, 0x4

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "left"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->alignMap:Ljava/util/Map;

    invoke-static {v7}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "left"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->alignMap:Ljava/util/Map;

    const/4 v1, 0x6

    invoke-static {v1}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "center"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAlign(I)Ljava/lang/String;
    .locals 2
    .param p0, "justification"    # I

    .prologue
    .line 139
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->alignMap:Ljava/util/Map;

    invoke-static {p0}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public static getHexCode(I)Ljava/lang/String;
    .locals 2
    .param p0, "index"    # I

    .prologue
    .line 129
    sget-object v0, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->colorMap:Ljava/util/Map;

    invoke-static {p0}, Lcom/samsung/thumbnail/office/util/HSSFColorMap;->intToStr(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private static intToStr(I)Ljava/lang/String;
    .locals 1
    .param p0, "val"    # I

    .prologue
    .line 119
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
