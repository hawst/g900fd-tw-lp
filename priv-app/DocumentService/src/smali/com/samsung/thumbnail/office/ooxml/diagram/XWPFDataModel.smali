.class public Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;
.super Lorg/apache/poi/POIXMLDocumentPart;
.source "XWPFDataModel.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "XWPFDataModel"


# instance fields
.field private diagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

.field private diagramRelId:Ljava/lang/String;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lorg/apache/poi/POIXMLDocumentPart;-><init>()V

    .line 24
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V
    .locals 0
    .param p1, "part"    # Lorg/apache/poi/openxml4j/opc/PackagePart;
    .param p2, "rel"    # Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/POIXMLDocumentPart;-><init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V

    .line 28
    return-void
.end method


# virtual methods
.method public getDiagram()Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;->diagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    return-object v0
.end method

.method public getDiagramRelId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;->diagramRelId:Ljava/lang/String;

    return-object v0
.end method

.method public parseDataModel()V
    .locals 7

    .prologue
    .line 31
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelParser;

    invoke-direct {v0, p0}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;)V

    .line 33
    .local v0, "dataModelParser":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelParser;
    const/4 v2, 0x0

    .line 35
    .local v2, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 36
    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelParser;->parse(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    if-eqz v2, :cond_0

    .line 42
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 48
    :cond_0
    :goto_0
    return-void

    .line 43
    :catch_0
    move-exception v1

    .line 44
    .local v1, "e":Ljava/io/IOException;
    const-string/jumbo v3, "XWPFDataModel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "IOException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 37
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 38
    .restart local v1    # "e":Ljava/io/IOException;
    :try_start_2
    const-string/jumbo v3, "XWPFDataModel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 40
    if-eqz v2, :cond_0

    .line 42
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 43
    :catch_2
    move-exception v1

    .line 44
    const-string/jumbo v3, "XWPFDataModel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "IOException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 40
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_1

    .line 42
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 45
    :cond_1
    :goto_1
    throw v3

    .line 43
    :catch_3
    move-exception v1

    .line 44
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v4, "XWPFDataModel"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setDiagram(Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;)V
    .locals 0
    .param p1, "diagram"    # Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;->diagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    .line 60
    return-void
.end method

.method public setDiagramRelId(Ljava/lang/String;)V
    .locals 0
    .param p1, "diagramRelId"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;->diagramRelId:Ljava/lang/String;

    .line 52
    return-void
.end method
