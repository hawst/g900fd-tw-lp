.class public Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelExtLstHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DiagramDataModelExtLstHandler.java"


# direct methods
.method public constructor <init>(ILcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;)V
    .locals 4
    .param p1, "nsID"    # I
    .param p2, "dataModel"    # Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

    .prologue
    .line 16
    const-string/jumbo v3, "extLst"

    invoke-direct {p0, p1, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 18
    const/4 v3, 0x1

    new-array v2, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 20
    .local v2, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelExtHandler;

    const/16 v3, 0x1f

    invoke-direct {v0, v3, p2}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelExtHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;)V

    .line 22
    .local v0, "extHandler":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelExtHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v3, "ext"

    invoke-direct {v1, v3, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 24
    .local v1, "extSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v3, 0x0

    aput-object v1, v2, v3

    .line 26
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramDataModelExtLstHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 27
    return-void
.end method
